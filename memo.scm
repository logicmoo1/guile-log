(use-modules (logic guile-log))
(use-modules (logic guile-log iso-prolog))
(use-modules (logic guile-log memoize))
(use-modules (logic guile-log umatch))
(use-modules (logic guile-log vlist))
(use-modules (logic guile-log guile-prolog canon))
(use-modules (logic guile-log guile-prolog interleave))
(define s (fluid-ref *current-stack*))
(compile-prolog-string
"
-functorize(tabling).
ff(X) :- (X=[Y,A,B]),ff(A),ff(B),(Y=1;Y=2).

-functorize(with_canon).
f(X) :- ff(X).

-functorize(tabling).
f2(X) :- \\+f2(X).
-functorize(tabling).
f3(X) :- X=[1|Y],\\+\\+f3(Y).
") 

(compile-prolog-string
"
-functorize(tabling).
gg(X) :- gg(Y),(X=[1|Y];X=[2|Y]).

-functorize(with_canon).
g(X) :- gg(X).
")

