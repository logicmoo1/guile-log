(use-modules (logic guile-log iso-prolog))
(use-modules (logic guile-log guile-prolog zip))

(compile-prolog-string
"
c(X,A)   :- c(X,A,0). 
c(X,A,N) :- N < 10, (X = A ; (B is A + 1, M is N + 1, c(X,B,M))).

f(X,Y) :- zip(lane(X, c(X,0)    ),
              lane(Y, c(Y,100)  )).

g(X,Y) :- usr_zip(lane(Tag_a,X,c(X,0)),
                  lane(Tag_b,Y,c(Y,100)),
             (((X mod 2) =:= 0 -> update(Tag_a) ; true),
              ((Y mod 2) =:= 1 -> update(Tag_b) ; true))).
")

(pk 'f (prolog-run * (X Y) (f X Y)))
(pk 'g (prolog-run * (X Y) (g X Y)))
