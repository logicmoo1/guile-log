(use-modules (logic guile-log functional-database))
(use-modules (logic guile-log indexer))
(use-modules (logic guile-log umatch))
(use-modules (logic guile-log))

(define (try f n)
  (define (g n)
    (if (= n 0)
        (<run> 10 () (f 1 'a))
        (begin
          (<run> 10 () (f 1 'a))
          (g (- n 1)))))

  (let loop ((i 1000))
    (if (= i 0)
        (g 1)
        (begin
          (<clear>)
          (g n)
          (loop (- i 1))))))

(define (add x)
  (dynamic-push *current-stack* f x #f #f))
(define (get) 
  (dynamic-env-ref f))

(define (comp) 
  (dynamic-compile-index *current-stack* f))
(define-dynamic f)

(<define> (add-rules a)
  (<append-dynamic> f (<lambda-dyn> (,a 'b)))
  (<append-dynamic> f (<lambda-dyn> (,a 'c)))
  (<append-dynamic> f (<lambda-dyn> (,a 'd)))
  (<append-dynamic> f (<lambda-dyn> (,a 'e))))

(<run> 10 (x y) 
       (add-rules 1))


(comp)

(pk
(<run> 100 (x y) 
   (add-rules 2)
   (add-rules 3)
   (<=> x 1)
   (f x y)))

(pk
(<run> 100 (x y) 
   (<remove-dynamic> (f _ 'c))
   (<remove-dynamic> (f _ 'e))
   (<remove-dynamic> (f 2 'b))
   (add-rules 7)
   (f x y)))

(pk
(<run> 100 (x) (<or> (<and> (<with-dynamic-functions> f)
                            (add-rules 7)
                            (f 7 x))
                     (f 7 x))))
 

#|
(define n 33)

(for-each 
 (lambda (x)
   (for-each
    (lambda (y)
      (add `(,x ,y)))
    (iota n)))
 (iota n))

(define (find a)
  (let* ((e   (car (get)))
         (db  (vector-ref e 3))        
         (tag (vector-ref e 0)))
    (get-index-set *current-stack* a db)))

(define (finde a e)
  (let* ((db  (vector-ref e 3)))
    (get-index-test *current-stack* a db 1000)))

(define (try n)
  (let ((v  '(5 5) #;`(,(gp-var! *current-stack*) 4))
        (e  (car (get))))
    (let loop ((n n))
      (if (= n 0)
          (finde v e)
          (begin
            (finde v e)
            (loop (- n 1)))))))
|#