(define-module (logic guile-log minikanren)
  #:use-module (logic guile-log)
  #:use-module (logic guile-log vlist)
  #:use-module ((logic guile-log umatch)
		#:select (gp-attvar-raw? set-attribute-cstor! gp-lookup
					 gp-newframe-choice
					 gp-make-var
					 gp-attvar?))
  #:use-module (logic guile-log run)
  #:use-module (logic guile-log iinterleave)
  #:use-module (logic guile-log guile-prolog attribute)
  #:re-export (init-machines new-machine new-pot-machine delay)
  #:export
  (conde fresh freshs freshx == =/= symbolo numbero booleano
	 run run* absento condo conde conds condes alls
	 pko pkdyno similar conda condas condu condus condx condxn
	 project var true define-delay counter count-up once
	 validate))

(define-inlinable (var  x) (<lambda> () (when (attvar? x))))
(define-inlinable (true)   (<lambda> () <cc>))

(define touch (make-fluid (make-hash-table)))
(define (init) (fluid-set! touch (make-hash-table)))

(define-syntax-rule (once x) (<lambda> () (<once-ii> (x))))
(define-syntax-rule (project x) (<lookup> x))


;; consa and condu is subtle with no reordering of goals 
;; we could use the guile-log framework but we are not that lucky
(<define-guile-log-rule> (<and*> x ...)
  (<and-ii> (<and> (x) ((reschedule-1*))) ...))

(<define-guile-log-rule> (<and+*> x ...)
  (<and+> (<and> (x) (reschedule #f)) ...))

(define-syntax-rule (conda (x xx ...) ...)
  (<lambda> ()
    (<cond> (((once x)) (<and*> xx ...)) ...)))

(define-syntax-rule (condas (x xx ...) ...)
  (<lambda> ()
    (<cond> (((once x)) (<and+*> xx ...)) ...)))


(define-syntax condu
  (syntax-rules ()
    ((condu)   
     (<lambda> () <fail>))
    ((condu x) 
     (once (conda x)))
    ((condu . l)
     (once (conda . l)))))

(define-syntax condus
  (syntax-rules ()
    ((condus)   
     (<lambda> () <fail>))
    ((condus x) 
     (once (condas x)))
    ((condus . l)
     (once (condas . l)))))


(define-syntax condo
  (syntax-rules ()
    ((conde)
     (<lambda> () <fail>))
    ((conde (x ...))
     (<lambda> () (<and*> x ...)))
    ((conde (x ...) (xx ...) ...)
     (<lambda> ()
       (<or> (<and> (<and*> x ...)
		    (<and> ((reschedule-1**))
			   (<and*> xx ...))
		    ...))))))


(define-syntax conde
  (syntax-rules ()
    ((conde)
     (<lambda> () <fail>))
    ((conde (x ...))
     (<lambda> () (<and*> x ...)))
    ((conde (x ...) (xx ...) ...)
     (<lambda> ()
       (<or-ii> (<and*> x ...)
	   (<and> ((reschedule-1**))
		  (<and*> xx ...))
		...)))))

(define-guile-log <and-rev*>
  (lambda (x)
    (syntax-case x ()
      ((<and-rev*> w x ...)
       (with-syntax (((y ...) (reverse #'(x ...))))
	 #'(<and*> w y ...))))))


(define-syntax -attvar?
  (syntax-rules ()
    ((_ (#t a ...)) (and (attvar? (<lookup> a)) ...))
    ((_ x)          (attvar? (<lookup> x)))))

(define-guile-log ax  
  (syntax-rules ()
    ((_ w (h t) (p ...))
     (<and> w (p) ...))
    ((_ w (h t) (p ...) x)
     (<and> w (p) ... (x)))
    ((_ w (h t) (p ...) x ...)
    (<and> w (p) ... 
       (if (-attvar? h)
	   (if (-attvar? t)
	       (<and*> x ...)
	       (<and-rev*> x ...))
	   (if (-attvar? t)
	       (<and*> x ...)
	       (<and*> x ...)))))))
		 
	     
(define-syntax condx
  (syntax-rules ()
    ((conde)
     (<lambda> () <fail>))
    ((conde p (q x ...))
     (<lambda> () 
      (ax p q x ...)))
    ((conde p (q x ...) (qq xx ...) ...)
     (<lambda> ()
      (<or-ii> (ax p q x ...)
	       (<and> ((reschedule-1**))
		      (ax p qq xx ...))
	       ...)))))

(define-syntax condxn
  (syntax-rules ()
    ((conde)
     (<lambda> () <fail>))
    ((conde p (q x ...))
     (<lambda> () 
      (ax p q x ...)))
    ((conde p (q x ...) (qq xx ...) ...)
     (<lambda> ()
      (<or-ii> (ax p q x ...)
	       (<and> ((reschedule-1**))
		      (ax p qq xx ...))
	       ...)))))
		     

(define-syntax condes
  (syntax-rules ()
    ((condes)
     (<lambda> () <fail>))
    ((condes (x ...))
     (<lambda> () (<and+*> x ...)))
    ((condes (x ...) (xx ...) ...)
     (<lambda> ()
	       (<or-ii> 
		(<and+> (<and> (x) (reschedule #f)) ...)
		(<and> ((reschedule-1**))
		       (<and+> (<and> (xx) (reschedule #f))
			       ...))
		...)))))

(define-syntax conds
  (syntax-rules ()
    ((conde)
     (<lambda> () <fail>))
    ((conde (x ...))
     (<lambda> () (<and+> (<and> (x) (reschedule #f)) ...)))
    ((conde (x ...) (xx ...) ...)
     (<lambda> ()
	       (<or> (<and+> (<and> (x) (reschedule #f)) ...)
		     (<and> ((reschedule-1**))
			    (<and+> (<and> (xx) (reschedule #f))
				    ...))
		     ...)))))


(define-syntax-rule (fresh (v ...) x ...)
  (<lambda> ()
    (<var> (v ...) (<and*> x ...))))

(define-syntax-rule (freshs (v ...) x ...)
  (<lambda> ()
    (<var> (v ...) (<and+*> x ...))))

(define-syntax-rule (freshx (v ...) p q x ...)
  (<lambda> ()
    (<var> (v ...) (ax p q x ...))))

(define-syntax-rule (==  x y) (<lambda> () (<=> ,x ,y)))

(define (pko x)
  (<lambda> () (<pp> x)))

(define (pkdyno x y)
  (<lambda> () (<pp-dyn> x y)))

(define-inlinable (ground-<type>? pred)
  (lambda (u s)
    (pred (gp-lookup u s))))

(define ground-symbol?
  (ground-<type>? symbol?))

(define ground-number?
  (ground-<type>? number?))

(define ground-boolean?
  (ground-<type>? boolean?))

(<define> (abs->abs test u)
  (<let> ((u (<lookup> u)))
  (<var> (w ww) 
    (<if> (<get-attr> u isAbsent w)
      (<let*> ((w (<lookup> w)))
	(<recur> lp ((w w))
	  (if (pair? w)
	      (if (test (car w))
		  (<and> (=/=* u (car w)) (lp (cdr w)))
		  (lp (cdr w)))
	      (<del-attr> u isAbsent))))
      <cc>)
    (<if> (<get-attr> u isNot ww)
	  (<let> ((ww (<lookup> ww)))
	    (<recur> lp ((ww ww) (r '()))
	      (if (pair? ww)
		  (<let> ((k (car ww)))
		    (if (or (and (test (car k)) (eq? (<lookup> (cdr k)) u))
			    (and (test (cdr k)) (eq? (<lookup> (car k)) u)))
			(lp (cdr ww) (cons k r))
			(if (or (eq? (<lookup> (car k)) u) (eq? (<lookup> 
								 (cdr k)) u))
			    (lp (cdr ww) r)
			    (lp (cdr ww) (cons k r)))))
		  (if (pair? r)
		      (<put-attr> u isNot r)
		      (<del-attr> u isNot)))))
	  <cc>))))


(define (symbolo u)
  (<lambda> ()
    (abs->abs symbol? u)
    (<let> ((u (<lookup> u)))
      (if (attvar? u)
	  (if (gp-attvar-raw? u S)
	      (<var> (w)
	        (<if> (<get-attr> u isSymbol w)
		      <cc>
		      (<if> (<or> (<get-attr> u isNumber  w)
				  (<get-attr> u isBoolean w))
			    <fail>
			    (<put-attr> u isSymbol #t))))
	      (<put-attr> u isSymbol #t))
	  (when (ground-symbol? u S))))
    ((execute-delay u))))

(define (booleano u)
  (<lambda> ()
    (abs->abs boolean? u)
    (<let> ((u (<lookup> u)))
      (if (attvar? u)
	  (if (gp-attvar-raw? u S)
	      (<var> (w)
	        (<if> (<get-attr> u isBoolean w)
		      <cc>
		      (<if> (<or> (<get-attr> u isNumber w)
				  (<get-attr> u isSymbol w))
			    <fail>			    
			    (<put-attr> u isBoolean #t))))
	      (<put-attr> u isSymbol #t))
	  (when (ground-boolean? u S))))
    ((execute-delay u))))

(define (numbero u)
  (<lambda> ()
    (<let> ((u (<lookup> u)))
      (abs->abs number? u)
      (if (attvar? u)
	  (if (gp-attvar-raw? u S)
	      (<var> (w)
		(<if> (<get-attr> u isSymbol w)
		      <fail>
		      (<if> (<or> (<get-attr> u isNumber  w)
				  (<get-attr> u isBoolean w))
			    <cc>
			    (<put-attr> u isNumber #t))))
	      (<put-attr> u isNumber #t))
	  (when (ground-number? u S))))
    ((execute-delay u))))

(define (potential-var x)
  (lambda (s . p?)
    (define n 0)
    (<wrap-s>
     (<lambda> ()
        (<recur> lp ((x x))
	  (<<match>> (#:mode -) (x)
	    ((x . l) (<and> (lp x) (lp l)))
	    (x       
	     (<let> ((x (<lookup> x)))
		(if (attvar? x)
		    (<code> (set! n (+ n 1)))
		    <cc>))))))
     s)
    n))

(define (potential x)
  (lambda (s . p?)
    (define n 0)
    (define (p y) 
      (if (pair? p?)
	  (begin
	    (<wrap-s> (<lambda> () (<pp> x)) s)
	    (pk 'min y))
	  y))

    (<wrap-s> (<lambda> (x) 
		 (<recur> lp ((x x))
		   (<<match>> (#:mode -) (x)
		     ((x . l) (<and> (lp x) (lp l)))
		     (x       (<code> (set! n (+ n 1)))))))
	      s
	      x)
    (p (ash n 0))))

(define (potential-c x)
  (let ((a (potential-s x))
	(b (potential   x)))
    (lambda (s . l)
      (+ (apply a s l) (apply b s l)))))

(define (potential-s x)  
  (lambda (s . p?)
    (define (p x) 
      (if (pair? p?) (pk 'min x) x))

    (p
     (if (pair? s)
	 (if (vhash? (cdr s))
	     (vlist-length (cdr s))
	     (length s))
	 0))))

(define (potential-ss x)
  (lambda (s . l)
    (define h (make-hash-table))
    (define i 0)
    (let ((ss (if (pair? s) (cdr s) '())))
      (let lp ((l (if (null? ss) '() (if (pair? ss) ss (vhash->assoc ss)))))
	(if (pair? l)
	    (let ((v (caar l)))
	      (let ((r (hashq-ref h v #f)))
		(if r 
		    (lp (cdr l))
		    (begin
		      (hashq-set! h v #t)
		      (let lp2 ((ll (gp-lookup (cdar l) s)))
			(cond
			 ((pair? ll)
			  (let ((r (hashq-ref h ll #f)))
			    (if r
				#t
				(begin
				  (hashq-set! h ll #t)
				  (lp2 (gp-lookup (car ll) s))
				  (lp2 (gp-lookup (cdr ll) s))))))

			 ((gp-attvar? ll s)
			  (let ((r (hashq-ref h ll #f)))
			    (if r
				#t
				(begin
				  (set! i (+ i 1))
				  (hashq-set! h ll #t)))))
			 
			 (else
			  #t)))
		      (lp (cdr l)))))))))
    i))




(define-syntax-rule (run n (vs ...) c ...)
  (begin
    (<clear>)
    (with-fluids ((*gp-var-tr* '_.)
		  (*init-tr*   init))
      (<run> n (vs ...) 
	(<and>
	 (<logical++>)
	 (init-machines)
	 ;(init-pot-machines (potential-c (list vs ...)))
	 (init-schedule-data)
	 (<and> (<and> (c) ((reschedule-1*))) ...)
	 (reschedule #t))))))

(define-syntax-rule (run* (vs ...) c ...)
  (begin
    (<clear>)
    (with-fluids ((*gp-var-tr* '_.)
		  (*init-tr*   init))
      (<run> * (vs ...) 
	(<and>
	 (<logical++>)
	 ;(init-pot-machines (potential-c (list vs ...)))
	 (init-schedule-data)
	 (init-machines)
	 (<and>  (<and> (c) ((reschedule-1*))) ...)
	 (reschedule #t))))))
    
(define-syntax-rule (<lambda-is> (x y z) . code)
  (<lambda> (x y z)
    (if z
	(<and> . code)
	<fail>)))

(define isAbsent 
  (<lambda-is> (val newval z)
  (<let> ((val    (<lookup> val))
	  (newval (<lookup> newval)))
     (<recur> lp ((val val))
	(if (pair? val)
	    (<and>
	     (abse (car val) newval)
	     (lp (cdr val)))
	    <cc>)))))

(<define> (pisAbsent h t v)
  (<var> (w)
    (<get-attr> v isAbsent w)
    (<=> h ,`(,@(map (lambda (x) `(absento (,x ,v))) (<lookup> w)) . ,t))))

(set-attribute-cstor! isAbsent pisAbsent)

(<define> (abse u v)
  (<let> ((u (<lookup> u)))
    (<recur> lp ((v v))
      (<<match>> (#:mode -) (v)
        ((? attvar? v)
	 (<let> ((v       (<lookup> v))
		 (simple? #f))
	    (if (gp-attvar-raw? v S)
		(<var> (w ww)
		  (<cond>
		   ((<get-attr> v isSymbol w) 
		    (<code> (set! simple? #t))
		    (if (symbol? u)
			(=/=* v u)
			<cc>))

		   ((<get-attr> v isBoolean w)
		    (<code> (set! simple? #t)) 
		    (if (boolean? u)
			(=/=* v u)
			<cc>))
		
		   ((<get-attr> v isNumber w) 
		    (<code> (set! simple? #t))
		    (if (number? u)
			(=/=* v u) 
			<cc>))

		   (else <cc>))
		  
		   
		  (<if> (<get-attr> v isAbsent ww)
			(<let> ((ww (<lookup> ww)))
			  (if (member u ww)
			      <cc>
			      (if simple?
				  <cc>
				  (<put-attr> v isAbsent (cons u ww)))))
			(if simple?
			    <cc>
			    (<put-attr> v isAbsent (list u)))))
		(<put-attr> v isAbsent (list u)))))

	((x . y)
	 (<and> (lp x) (lp y)))

	(x (<not> (<==> x u)))))))


(define-inlinable (absento u v)
  (<lambda> ()
    (abse u v)))
      


(define-syntax-rule (mk-1 isSymbol symbol? symbolo)
(define isSymbol
  (<lambda-is> (x y z)
    (<let> ((y (<lookup> y)))
      (cond
        ((symbol? (<lookup> y))
	 <cc>)
	((gp-attvar-raw? y S)
	 ((symbolo y))))))))

(mk-1 isSymbol  symbol?  symbolo)
(mk-1 isNumber  number?  numbero)
(mk-1 isBoolean boolean? booleano)

(<define> (pisSymbol a tail x)
  (<=> a (,`(sym ,x) . tail)))

(set-attribute-cstor! isSymbol pisSymbol)

(<define> (pisNumber a tail x)
  (<=> a (,`(num ,x) . tail)))
    
(set-attribute-cstor! isNumber pisNumber)

(<define> (pisBoolean a tail x)
  (<=> a (,`(bool ,x) . tail)))

(set-attribute-cstor! isBoolean pisBoolean)


(define isNot 
  (<lambda-is> (val y z)
    (<let> ((y   (<lookup> y))
	    (val (<lookup> val)))
       (<recur> lp ((val val))
	 (if (pair? val)
	     (<and>
	      (=/=* (caar val) (cdar val))
	      (lp (cdr val)))
	     <cc>)))))

(<define> (pisNot a tail x)
  (<var> (r)
    (<get-attr> x isNot r)
    (<recur> lp ((r (<lookup> r)) (a a))
      (if (pair? r)
	  (<let*> ((d (<lookup> (car r)))
		   (q (hashq-ref (fluid-ref touch) d #f)))
	     (if q
		 (lp (cdr r) a)
		 (<var> (t)
		   (<code> (hashq-set! (fluid-ref touch) d #t))
		   (<=> a ,`((=/= ,(car d) ,(cdr d)) . ,t))
		   (lp (cdr r) t))))
	  (<=> a tail)))))

(set-attribute-cstor! isNot pisNot)

(<define> (add-test var test other true)
  (<let> ((pred (lambda (x) #t))
	  (code (<lambda> (v)
		   (<if> (<get-attr> var isNot v)
			 (<let> ((head (car test))
				 (tail (cdr test))
				 (v    (<lookup> v)))
			   (<recur> lp ((vv v))
			     (if (pair? vv)
				 (<if> 
				  (<and!> 
				   (<or>
				    (<and> (<==> ,(caar vv) head) 
					   (<==> ,(cdar vv) tail))
				    (<and> (<==> ,(cdar vv) head) 
					   (<==> ,(caar vv) tail))))
				  (<code> (true))
				  (lp (cdr vv)))
				 (<put-attr> var isNot (cons test v)))))
			 (<put-attr> var isNot (list test)))
		   (<cc> #f))))

    (<var> (v)
      (if (and (gp-attvar-raw? var S) (not (gp-attvar? var S)))
	  (<var> (q)
	    (<cond>
	     ((<get-attr> var isSymbol  #t) (<code> (set! pred symbol?)))
	     ((<get-attr> var isNumber  #t) (<code> (set! pred number?)))
	     ((<get-attr> var isBoolean #t) (<code> (set! pred boolean?)))
	     (else <cc>))
	    (if (not (pred v))
		(<cc> #t)
		(code v)))
	  (code v)))))    


(<define> (=/=* uu vv)
  (<let*> ((fr   (<newframe>))
	   (test (cons (<lookup> uu) (<lookup> vv)))
	   (true #f)
	   (ftrue (lambda () (set! true #t)))
	   (var  #f))

    (<recur> lp ((u uu) (v vv))
      (<let> ((u (<lookup> u))
	      (v (<lookup> v)))
       (if (attvar? u)
	   (<and>
	    (<code> (set! var #t))
	    (<values> (t) (add-test u test v ftrue))
	    (if t 
		(<code> (set! true #t))
		<cc>))
	   <cc>)

       (if (attvar? v)
	   (<and>
	    (<code> (set! var #t))
	    (<values> (t) (add-test v test u ftrue))
	    (if t 
		(<code> (set! true #t))
		<cc>))
	   <cc>)

       (if (not var)
	   (<<match>> (#:mode -) (u v)
	     ((x . xl) (y . yl)
	      (<and>
	       (lp x y) 
	       (if true
		   <cc>
		   (lp xl yl))))
	     (a b
	      (if (equal? (<lookup> u) (<lookup> v))
		  <cc>
		  (<code> (set! true #t)))))
	   <cc>)))
    (if true
	(<code> (<unwind-tail> fr))
	(when var))))
    
(define-syntax-rule (=/= x y) 
  (<lambda> () (=/=* x y)))

(define-syntax-rule (delayable ass)
  (and (and-map (lambda (x) (<var?> x)) ass)
       (let lp ((as ass))
	 (if (pair? as)
	     (let ((q (car as)))
	       (if (memq q (cdr as))
		   #f
		   (lp (cdr as))))
	     #t))))

(define-syntax-rule (define-u (head ...) code ...)
  (<define> (head ... p)
    (cond
     (p code ...)
     (else <fail>))))

(define-u (isDelay l item)
  (<var> (ll)
    (<cond>
     ((<get-attr> item isDelay ll)
      (<recur> l1 ((l l) (new '()))
        (<<match>> (#:mode -) (l)
	  (((and x (as-l _ f . args-l)) . l)
	   (<recur> l2 ((ll ll))
	     (<<match>> (#:mode -) (ll)
	       (((as-ll _ ,f . args-ll) . _)
		(<and>
		 (<=> args-l args-ll)
		 (l1 l new)))
	       ((_ . ll)
		(l2 ll))
	       (()
		(l1 l (cons x new))))))
	  (()
	   (if (pair? new)
	       (<put-attr> item isDelay (append new (<lookup> ll)))
	       <cc>)))))
     (else
      (<recur> lp ((l l))
	(<<match>> (#:mode -) (l)
	  (((as _ f . args) . l)
	   (<and>
	    (<recur> lpp ((as (<lookup> as)))
	     (if (pair? as)
		 (if (delayable (list (car as)))
		     (<var> (q)
		       (<if> (<get-attr> (car as) isDelay q)
			     (<recur> lp2 ((q (<lookup> q)) (new '()))
			       (<<match>> (#:mode -) (q)
                                 (((,as _ ,f . _) . q)
				  (lp2 q new))
				 ((x . q)
				  (lp2 q (cons x new)))
				 (()
				  (<and>
				   (if (null? new)
				       (<del-attr> (car as) isDelay)
				       (<put-attr> (car as) isDelay new))
				   (lpp (cdr as))))))
			     (lpp (cdr as))))
		     (lpp (cdr as)))
		 <cc>))				   
	    ((apply f args))
	    (lp l)))	  
	  (()
	   <cc>)))))))

(define-syntax-rule (define-delay (f args ...) (u ...) code ...)
  (define (f args ...)
    (let ((quode (begin code ...)))
      (<lambda> ()
        (<let> ((as (list (<lookup> u) ...)))
          (if (delayable as)
	      (<let> ((a (list as 'f f args ...)))
		(<var> (q)
		  (<if> (<get-attr> u isDelay q)
			(<put-attr> u isDelay (cons a (<lookup> q)))
			(<put-attr> u isDelay (cons a '())))
		  ...))
	      (quode)))))))

(define (execute-delay v)
  (<lambda> ()
    (<var> (l)
     (<if> (<get-attr> v isDelay l)
	   (<recur> lp ((l l))
	     (<<match>> (#:mode -) (l)
	       (((_ _ f . args) . l)
		(<and>
		 ((apply f args))
		 (lp l)))
	       (()
		<cc>)))
	   <cc>))))

(<define> (pDelay a tail x)
  (<var> (r)
    (<get-attr> x isDelay r)
    (<recur> lp ((r r) (a a))
      (<<match>> (#:mode -) (r)
	(((and d (as s _ . args)) . l)
	 (<and>
	  (<let*> ((d (<lookup> d))
		   (q (hashq-ref (fluid-ref touch) d #f)))
	     (if q
		 (lp l a)
		 (<var> (t)
		   (<code> (hashq-set! (fluid-ref touch) d #t))
		   (<=> a ,`((,s . ,args) . ,t))
		   (lp l t))))))
	(()
	 (<=> a tail))))))

(set-attribute-cstor! isDelay pDelay)

(define *counter* (gp-make-var 0))       	    
(define (count-up)
  (<lambda> () (<set> *counter* (+ 1 (<lookup> *counter*)))))
(define-syntax-rule (counter) (<lookup> *counter*))
(define (validate nr)
  (<lambda> ()
    (when (eq? (counter) nr))))
