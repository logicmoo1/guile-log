(define-module (logic guile-log guile-prolog engine)
  #:use-module (logic guile-log procedure-properties)
  #:use-module (ice-9 match)
  #:use-module (logic guile-log run)
  #:use-module (logic guile-log)
  #:use-module (logic guile-log guile-prolog dynamic-features)
  #:use-module (logic guile-log guile-prolog interpreter)
  #:use-module (logic guile-log dynamic-features)
  #:use-module (logic guile-log umatch)
  #:use-module (logic guile-log code-load)
  #:use-module (logic guile-log iso-prolog)
  #:export (engine_create
            engine_next
            engine_next_reified
            engine_post
            engine_yield
            engine_fetch
            is_engine
            engine_self
            current_engine
            stub))

(<define> (engine_yield term) (<ret> (list #:yield S P CC term)))

(define postbox (make-fluid #f))
(<wrap> add-fluid-dynamics postbox)
(define (<postbox-ref>) (fluid-ref postbox))

(<define> (engine_fetch term)
  (copy_term (variable-ref (car (<postbox-ref>))) term))

(define (generate-engine s size name goal)
  (let ((engine (gp-make-engine size)))
    (letrec ((state      #f)
             (first      #t)
             (guards     #f)
             (inhibit    #f)
             (path       (cons (list (cons s engine)) '()))
             (postbox    (make-variable #f))
             (on-result  
              (lambda ()
                (match state
                  (#:exit 
                   (set! engine #f)
                   state)                  
                  ((#:throw e)
                   (set! engine #f)
                   state)
                  (x
                   (if (eq? x 'stalled)
                       (begin
                         (set! state 
                           (list #:stalled
                                 (cons
                                  (@@ (logic 
                                       guile-log 
                                       guile-prolog
                                       interpreter)
                                      lold)
                                  (get-continuation))))
                         state)
                       state)))))
                   
             (start-f
              (lambda (s)
                (if engine
                    (begin
                      (set! start-f next)
                      (let ((s2   #f)
                            (pth  #f)
                            (g    #f))
                        (dynamic-wind
                          (lambda ()
                            (set! inhibit #t)
                            (set! g (gp-store-engine-guards))
                            (if (not first)
                                (begin
                                  (set! first #f)
                                  (set! pth
                                    (gp-set-engine path)))
                                (let ((s2.p (gp-new-engine engine)))
                                  (set! s2  (car s2.p))
                                  (set! pth (cdr s2.p))))
                            (if guards
                                (gp-restore-engine-guards guards)))
                          
                          (lambda ()
                            (set! state 
                              (goal
                               s2
                               (lambda () 
                                 (set! engine #f)
                                 #:exit)
                               (lambda (s p x)
                                 (list #:finish s p))
                               postbox
                               (lambda () ret)))
                            (on-result))
                          
                          (lambda ()
                            (set! guards (gp-store-engine-guards))
                            (set! path   (gp-set-engine pth))
                            (gp-restore-engine-guards g)
                            (set! inhibit #f)))))
                    #:exit)))
                                       
             (next
              (lambda (s)
                (let ((pth #f)
                      (g   #f))
                  (dynamic-wind
                    (lambda ()
                      (set! inhibit #t)
                      (set! g   (gp-store-engine-guards))
                      (set! pth (gp-set-engine path))
                      (gp-restore-engine-guards guards))

                    (lambda ()                 
                      (if engine
                          (match state
                            (#:exit
                             state)

                            ((#:throw e)
                             state)

                            ((#:stalled cc)
                             (set! state (continue (cdr cc)))
                             (on-result))

                            ((#:yield s0 p0 cc x)
                             (set! state (cc s0 p0))
                             (on-result))

                            ((#:finish _ p x)
                             (set! state (p))
                             (on-result))
                            
                            (x x))
                          #:exit))

                    (lambda ()
                      (set! guards (gp-store-engine-guards))
                      (set! path   (gp-set-engine pth))
                      (gp-restore-engine-guards g)
                      (set! inhibit #f))))))
             
             (ret
              (case-lambda
                (()
                 (when (not inhibit)
                   (let* ((pth   (gp-set-engine path))
                          (s     (<state-ref>)))
                     (gp-set-engine pth)
                     (list first start-f
                           s state path guards engine (variable-ref postbox)))))

                ((l)
                 (when (not inhibit)
                   (match l
                     ((first_ start-f_ s state_ path_ guards_ engine_ post_)
                      (set! start-f start-f_)
                      (set! first  first_)
                      (set! state  state_)
                      (set! path   path_)
                      (set! guards guards_)
                      (set! engine engine_)
                      (variable-set! postbox post_)
                      (let ((pth (gp-set-engine path)))
                        (<state-set!> s)
                        (gp-set-engine pth))))))

                ((kind s . term)
                 (match kind
                   (#:run    (start-f s))
                   (#:post   (apply variable-set! postbox term))
                   (#:finish
                    (set! engine #f))
                   (#:set-state
                    (set! state s))
                   (#:set-engine
                    (set! engine (cadr state)))
                   (#:exist
                    (and engine
                         (match state
                           ((#:throw . _) #f)
                           (#:exit        #f)
                           (_ #t)))))))))

      (<wrap> add-parameter-dynamics ret)
      (set-procedure-property! ret 'engine #t)
      (set-procedure-property!
       ret 'name (string->symbol (format #f "e<~a>" (name-it name))))
      ret)))

(<define> (is_engine term)
  (when (and (procedure?         (<lookup> term))
             (procedure-property (<lookup> term) 'engine))))

(<define> (engine_next engine term)
  (let* ((engine (<lookup> engine))
         (res    (engine #:run S)))
    (<recur> lp ((res res))
      (<<match>> (#:mode - #:name engine_next) (res)
         (#:exit 
          <fail>)

         ((#:throw e)
          (<var> (ee)
            (copy_term e ee)
            (throw ee)))

         ((#:finish s p x)
          (<=> x term))

         ((#:yield s p cc x)
          (<=> x term))
         
         ((#:stalled _)
          (<and>
           (nl)
           (write "stalled engine > ")
           (write engine)           
           (nl)
           (stall)
           (lp (engine #:run S))))
         
         (x (<ret> x))))))
           

(<define> (engine_next_reified engine term)
  (let* ((engine (<lookup> engine))
         (res    (engine #:run S)))
    (<recur> lp ((res res))
     (<<match>> (#:mode - #:name engine_next_reified) (res)
      (#:exit 
       (<=> "no" term))

      ((#:throw e)
       (copy_term (vector (list "exception" e))
                  term))

      ((#:finish s p x)
       (<=> ,(vector (list "the" x)) term))

      ((#:yield s p cc x)
       (<=> ,(vector (list "the" x)) term))

      ((#:stalled _)
       (<and>
        (write "stalled engine > ")
        (write engine)
        (nl)        
        (let ((state res))
          (stall)
          (<code> (engine #:set-state state))
          (lp (engine #:run S)))))
      
      (x (<ret> x))))))


(define engine_post 
  (<case-lambda>
    ((engine term)
     (let ((engine (<lookup> engine)))
       (<var> (t)
          (copy_term term t)
          (<code> (engine #:post S (<lookup> t))))))
    ((engine term reply)
     (engine_post engine term)
     (engine_next engine reply))))

(<define> (engine_self engine) (<=> engine ,((cdr (<postbox-ref>)))))

(<define> (current_engine engine)
 (is_engine engine)
 (when ((<lookup> engine) #:exist #f #f)))

(define (name-it x)
  (if (procedure? x)
      (procedure-name x)
      (if (string? x)
          (string->symbol x)
          x)))

(<define> (create-engine-scm t g name size engine)
  (let ((e (generate-engine S (<lookup> size) (<lookup> name)
                            (mk t g))))
    (<=> engine e)))
           

(<define> (ret_e e) (<ret> (list #:throw e)))
(<define> (ret_g t) (<ret> (list #:finish S P t)))

(define mod (current-module))
(define (mk t g)
  (lambda (s p cc a b)
    (stub s p cc t g a b)))



(compile-prolog-string "
 g(Term,Goal,A,B) :-
   do[(fluid-set! postbox (cons A B))], 
   Goal, 
   copy_term(Term,T),
   ret_g(T).
 
 f(Term,Goal,A,B) :- 
   with_fluid_guard_dynamic_object(scm[postbox],g(Term,Goal,A,B)).

 stub(Term,Goal,A,B) :-
   catch(f(Term,Goal,A,B),E,ret_e(E)).
  
 parse_ops([],Ops,[]) :- !.
 parse_ops([],Ops,Defaults) :- !,
   parse_ops(Defaults,Ops,[]).
 parse_ops([X|L],Ops,Defaults) :-
   (member(X,Ops) -> true ; true),
   parse_ops(L,Ops,Defaults).

 defaults([\"alias\"(noname),\"global\"(100),\"local\"(100),\"trail\"(100)]). 
 engine_create(Template, Goal, Engine) :-
   engine_create(Template, Goal, Engine, []).
 engine_create(Template, Goal, Engine, Ops) :-
   defaults(Defaults),
   parse_ops(Ops, [\"alias\"(Name),\"local\"(Size)], Defaults),
   copy_term([Template,Goal],[T,G]),
   'create-engine-scm'(T,G,Name,Size,Engine).    
")

