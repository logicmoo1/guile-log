(define-module (logic guile-log guile-prolog vm vm-goal2)
  #:use-module (logic guile-log)
  #:use-module (compat racket misc)
  #:use-module (logic guile-log iso-prolog)
  #:use-module (logic guile-log guile-prolog hash)
  #:use-module (logic guile-log guile-prolog ops)
  #:use-module (logic guile-log prolog swi)
  #:use-module (logic guile-log guile-prolog macros)
  #:use-module ((logic guile-log guile-prolog vm-compiler)
		#:select ())
  #:use-module (logic guile-log guile-prolog vm vm-pre)
  #:use-module (logic guile-log guile-prolog vm vm-var2)
  #:use-module (logic guile-log guile-prolog vm vm-args2)
  #:use-module (logic guile-log guile-prolog vm vm-unify2)
  #:use-module (logic guile-log guile-prolog vm vm-imprint2)
  #:use-module (logic guile-log guile-prolog vm vm-scm2)
  #:use-module (logic guile-log guile-prolog vm vm-disj2)
  #:use-module (logic guile-log guile-prolog vm vm-conj2)
  #:use-module (logic guile-log guile-prolog vm vm-handle2)
  #:use-module (system vm assembler)
  #:export (begin_att end_att recur verbatim_call with_cut pr
		      extended_off extended_on))
#;
(eval-when (compile)
(pk (prolog-run-rewind 1 (x) 
		   (dyntrace (@@ (logic guile-log guile-prolog vm vm-handle) 
				 handle)))))
(compile-prolog-string
"
- eval_when(compile).
the_tr2(X,[X]).

:- add_term_expansion_temp(the_tr2).
:- add_term_expansion_temp(extended_macro).
")


(eval-when (compile)
  (set! (@@ (logic guile-log slask) include-meta) #f))


(include-from-path "logic/guile-log/guile-prolog/vm/vm-goal-model.scm")

(eval-when (compile)
  (set! (@@ (logic guile-log slask) include-meta) #t))

