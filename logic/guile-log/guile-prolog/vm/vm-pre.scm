 (define-module (logic guile-log guile-prolog vm vm-pre)
  #:use-module (logic guile-log)
  #:use-module (logic guile-log iso-prolog)
  #:use-module (logic guile-log guile-prolog hash)
  #:use-module (logic guile-log guile-prolog ops)
  #:use-module (logic guile-log prolog swi)
  #:use-module (logic guile-log vlist)
  #:use-module (compat racket misc)
  #:use-module (system vm assembler)
  #:use-module (logic guile-log vm vm)
  #:replace (tr)
  #:export ())

(define unify_operators '())
(define ops
  (list
   (list op2+     op2+   )
   (list op2-     op2-   )
   (list op2*     op2*   )
   (list #{;}#    #{;}#  )
   (list #{,}#    #{,}#  )
   (list #{\\+}#  #{\\+}#)))

(define (combine_ops *extended*)
    (if *extended*
	(let lp ((l ops) (r '()))
	  (if (pair? l)
	      (let ((x (car l)))
		(if (assq (car x) *extended*)
		    (lp (cdr l) r)
		    (lp (cdr l) (cons x r))))
	      ((@ (guile) append) r *extended*)))
	'()))

(define *recurs*      (make-fluid vlist-null))
(define *tag*         (make-fluid 0))
(define *varn*        (make-fluid 4))
(define *svarn*       (make-fluid vlist-null))
(define *var-to-type* (make-fluid '()))
(<define> (get_recur x y n)
  (let ((a (vhash-ref (fluid-ref *recurs*) (<lookup> x) #f)))
    (if a
	(<=> (y n) (,(car a) ,(cdr a))))))
	
(<define> (add_recur x y n)
  (<code> (fluid-set! *recurs* 
		      (vhash-cons (<lookup> x) (cons y n)
				  (fluid-ref *recurs*)))))

(<define> (get_varn x) (<=> x ,(fluid-ref *varn*)))
(<define> (get_vart x) (<=> x ,(list->vector 
				(cons* 0 0 0
				       ((@ (guile) reverse)
					(fluid-ref *var-to-type*))))))

(<define> (new_tag x)
  (let ((tag (fluid-ref *tag*)))
    (<=> x tag)
    (<code> (fluid-set! *tag* (+ tag 1)))))

(<define> (init_vars) 
   (<code>
    (fluid-set! *recurs*       vlist-null)
    (fluid-set! *tag*          0)
    (fluid-set! *var-to-type* '())
    (fluid-set! *varn*         3)
    (fluid-set! *svarn*        vlist-null)))

(<define> (get_nsvars q n)
   (<=> n ,(vhashq-ref (fluid-ref *svarn*) (<lookup> q) 0)))
	   

(<define> (max_svarns x)
  (<=> x ,(let lp ((s 0) (l (vhash->assoc (fluid-ref *svarn*))))
	    (if (pair? l)
		(lp (max s (cdar l)) (cdr l))
		s))))

(define (get-varn)
  (let ((i (fluid-ref *varn*)))
    (fluid-set! *varn* (+ 1 i))
    i))

(define (get-svarn s q)
  (let* ((h (fluid-ref *svarn*))
	 (i (vhashq-ref h q 4)))
    (let ()
      (fluid-set! *svarn* (vhash-consq q (+ i 1) h))
      (cons i 0))))


(<define> (new_var v q s)
  (let ((v (<lookup> v))
	(q (<lookup> q))
	(s (<lookup> s)))
    (if (<var?> v)
	(let ((i (if q (get-svarn s q) (get-varn))))
	  (<=> v i)
	  (<code> 
	   (if (number? i)
	       (fluid-set! *var-to-type*
			   (cons (if (eq? s #t) 1 0) 
				 (fluid-ref *var-to-type*))))))
      <cc>)))

(define *const-i*  (make-fluid 0))
(define *regconst* (make-fluid vlist-null))
(define (init-const) 
  (fluid-set! *regconst* vlist-null)
  (fluid-set! *const-i*  0))
(define (get-consts)
  ((@ (guile) reverse) (vhash->assoc (fluid-ref *regconst*))))

(<define> (init_const) (<code> (init-const)))

(<define> (regconst x xx)
  (let ((h (fluid-ref *regconst*))
	(x (<lookup> x)))
    (<if> (constant x)
	  (let ((i (vhashq-ref h x #f)))
	    (if i
		(<=> xx i)
		(let ((i (fluid-ref *const-i*)))
		  (<code>
		   (fluid-set! *const-i*  (+ i 1))
		   (fluid-set! *regconst* 
			       (vhash-consq x i h)))
		  (<=> xx i))))
	  (<=> x xx))))


  (<define> (reset_e) (<code> (set! *e* 1)))

(<define> (init_e) (<code> (set! *e* 1)))
(define *e* 1)
(<define> (new_e E) 
   (<=> E *e*)
   (<code> (set! *e* (ash *e* 1))))

(define-inlinable (get-high-bit x) 
  (ash 1 (- (integer-length x) 1)))

(define-inlinable (get-rest-bits h x)
  (logand x (lognot h)))

(<define> (maskoff N E NN)
   (<let*> ((N1  (<lookup> N))
	    (E1  (get-high-bit     N1))
	    (NN1 (get-rest-bits E1 N1)))
     (<=> E  E1)
     (<=> NN NN1)))

(define mod_ (current-module))
(define (instr id)
  (let* ((s (module-ref mod_ id))
	 (y (prolog-run 1 (x) (trit s x))))
    (if (pair? y)
	(car y)
	(begin
	  (warn (format #f "could not lookup symbol ~a" id))
	  -1))))

(eval-when (compile load eval)
  (begin
    (define ncode 0)
    (<define> (inc x)
      (<=> x ncode)
      (<code> (set! ncode (+ ncode 1))))))
(define vartag (gensym "var"))
(<define> (gen x) (<=> x ,(cons vartag (gensym "id"))))
(<define> (var_p x)
   (let ((x (<lookup> x)))
     (<if> (var CUT SCUT x)
	   (gen x)
	   (if (and (pair? x) (eq? (car x) vartag))
	       <cc>))))

(compile-prolog-string
"
- eval_when(compile).
the_tr(t(X)  , [tr(X,N)])    :- inc(N).
the_tr((t(X,Y):-L),[tr(X,Y) :- L]).

binsi2.
binis2.
binss2.

the_tr(binss2_(X,Y) ,[tr(Y,N),binss2(X,Y)]) :- inc(N).
the_tr(binxx2_(X,Y) ,[tr(Y,N),binxx2(X,Y)]) :- inc(N).
the_tr(binxxu2_(X,Y),[tr(Y,N),binxxu2(X,Y)]) :- inc(N).

the_tr(binsi2_(X,Y) ,[tr(Y,N),binsi2(X,Y)]) :- inc(N).
the_tr(binxi2_(X,Y) ,[tr(Y,N),binxi2(X,Y)]) :- inc(N).
the_tr(binxiu2_(X,Y),[tr(Y,N),binxiu2(X,Y)]) :- inc(N).

the_tr(binis2_(X,Y) ,[tr(Y,N),binis2(X,Y)]) :- inc(N).
the_tr(binix2_(X,Y) ,[tr(Y,N),binix2(X,Y)]) :- inc(N).
the_tr(binixu2_(X,Y),[tr(Y,N),binixu2(X,Y)]) :- inc(N).

the_tr(bin(X), [binop(X,N),tr(X,N)]) :- inc(N).
the_tr((bin(X,Y):-L),[binop(X,Y) :- L]).

the_tr(bin1(X,O),   [binop1L(X,O),binop1R(X,O),tr(X,N0),tr(O,N)]) :- 
   inc(N0),inc(N).
the_tr(bin1(X,L,R), [binop1L(X,L),binop1R(X,R),tr(X,N),tr(L,NL),tr(R,NR)]) :- 
   inc(N),inc(NL),inc(NR).


the_tr((un(X,Y):-L),[unop(X,Y) :- L]).
the_tr(un(X) , [unop(X,N),tr(X,N)])  :- inc(N).

:- add_term_expansion_temp(the_tr).
")

(compile-prolog-string
"
tr(X,X) :- !.

print_error_if_fail :-
   b_getval(pretty,#t),!,
   write(tr_error(X)),nl,fail.

t(X,X) :- b_getval(pretty,#t),!.

t('store-state').
t(newframe).
t(newframe-ps).
t(newframe-pst).
t('newframe-light').

t(unwind).
t(unwind-ps).
t(unwind-psc).
t(unwind-pst).
t(unwind-psct).
t('unwind-light').
t('unwind-tail').
t('unwind-light-tail').

t(softie).
t('softie-psc').
t('softie-ps').
t('softie-pc').
t('softie-light').

t('post-s').
t('post-q').
t('post-c').
t('post-sc').

t('store-ps').
t('store-p').
t('restore-c').

t('fail-psc').
t('fail-pc').

t('newframe-negation').
t('unwind-negation').
t('post-negation').

t('set').
t('unify-variable').
t('unify-constant').
t('unify-instruction').
t('unify-constant-2').
t('unify-instruction-2').
t(icurly).
t(ifkn).
t(icons).

t('unify-2').
t('unify').
t('sp-move').
t('sp-move-s').

t('icurly!').
t('ifkn!').
t('icons!').

t('pre-unify').
t('post-unify').
t('post-unify-tail').

t('clear-sp').
t(false).

t(fail).
t(cc).
t('tail-cc').
t('call').
t('call-n').
t('tail-call').


t('goto-inst').
t('cut').
t('post-unicall').

t('push-constant').
t('push-instruction').
t('push-variable-s').
t('push-variable-v').
t('push-2variables-s').
t('push-2variables-x').
t('push-3variables-s').
t('push-3variables-x').
t('push-variable').
t('push-variable-scm').
t('pop-variable').
t('pushv').
t('mk-cons').
t('mk-fkn').
t('mk-curly').

t(seek).
t(pop).
t(dup).

t(=..).

t('ss-gt').
t('ss-lt').
t('ss-ge').
t('ss-le').
t('ss-e').
t('ss-ne').
t('xx-gt').
t('xx-lt').
t('xx-ge').
t('xx-le').
t('xx-e').
t('xx-ne').
bin1(>,gtL,gtR).
bin1(< ,ltL,ltR).
bin1(>=,geL,geR).
bin1(=<,leL,leR).
t(=:=).
t(=\\=).
t(@<).
t(@=<).
t(@>).
t(@>=).

t(dup).

t('equal-constant').
t('equal-instruction').

t(cutter).

t('unify-variable').
t(X,_) :- 
 print_error_if_fail.

bin(X,X) :- b_getval(pretty,#t),!.
bin1(min,min2_1).
bin1(max,max2_1).
bin1(+,plus2_1).
bin1(-,minus2_1).
bin1(*,mul2_1).
bin1(/,div2_1).
bin(∪).
bin(⊕).
bin(∖).
bin(∩).
bin(//).
bin(rdiv).
bin1(<<,shiftLL,shiftLR).
bin1(>>,shiftRL,shiftRR).
bin1(mod,modL,modR).
bin1(rem,remL,remR).
bin1(/\\,bitand).
bin1(\\/,bitor).
bin1(xor,xor1).
bin1(^,expL,expR).
bin1(**,exp2L,exp2R).

un(X,X) :- b_getval(pretty,#t),!.
un('op1_-').
un('op1_+').
un(\\).
un(ᶜ).
/*
binss(>    ,'ss-gt').
binss(<    ,'ss-lt').
binss(>=   ,'ss-ge').
binss(=<   ,'ss-le').
binss(=:=  ,'ss-e' ).
binss(=\\= ,'ss-ne').

binxx(>    ,'xx-gt').
binxx(<    ,'xx-lt').
binxx(>=   ,'xx-ge').
binxx(=<   ,'xx-le').
binxx(=:=  ,'xx-e' ).
binxx(=\\= ,'xx-ne').

binss2_(min,'ss-min-s').
binss2_(max,'ss-max-s').
binss2_(+,'ss-add-s').
binss2_(-,'ss-sub-s').
binss2_(*,'ss-mul-s').
binss2_(/,'ss-div-s').
binss2_(mod,'ss-mod-s').
binss2_(rem,'ss-rem-s').
binss2_(<<,'ss-lshift-s').
binss2_(>>,'ss-rshift-s').
binss2_(/\\,'ss-and-s').
binss2_(\\/,'ss-or-s').
binss2_(xor,'ss-xor-s').
binss2_(^,'ss-pow-s').
binss2_(**,'ss-pow-s').

binxx2_(min,'xx-min-x').
binxx2_(max,'xx-max-x').
binxx2_(+,'xx-add-x').
binxx2_(-,'xx-sub-x').
binxx2_(*,'xx-mul-x').
binxx2_(/,'xx-div-x').
binxx2_(mod,'xx-mod-x').
binxx2_(rem,'xx-rem-x').
binxx2_(<<,'xx-lshift-x').
binxx2_(>>,'xx-rshift-x').
binxx2_(/\\,'xx-and-x').
binxx2_(\\/,'xx-or-x').
binxx2_(xor,'xx-xor-x').
binxx2_(^,'xx-pow-x').
binxx2_(**,'xx-pow-x').

binsi2_(+,'is-addi-s').
binis2_(+,'is-addi-s').

binxi2_(+,'is-addi-x').
binix2_(+,'is-addi-x').
*/
")


(compile-prolog-string
"
trit(X,Y) :- b_setval(pretty,#f), tr(X,Y).
instruction(X) :- nonvar(X),(X=[] ; number(X) ; string(X)).
constant(X)    :- atom(X), \\+instruction(X).

link_l([L,LL],[L,L1],[L1,LL]).
link_l([L,LL],[L,L1],[L1,L2],[L2,LL]).
ff :- asserta((tr(X,X) :- !)).
")

(<wrap> ff)

(all-defined-out)
