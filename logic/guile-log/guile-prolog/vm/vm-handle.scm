(define-module (logic guile-log guile-prolog vm vm-handle)
  #:use-module (logic guile-log)
  #:use-module ((logic guile-log iso-prolog)
  		#:renamer
		(lambda (x)
		  (if (eq? x 'plus)
		      'swi:plus
		      x)))

  #:use-module (logic guile-log guile-prolog hash)
  #:use-module (logic guile-log guile-prolog ops)
  #:use-module ((logic guile-log prolog swi)
		#:renamer
		(lambda (x)
		  (if (eq? x 'plus)
		      'swi:plus
		      x)))
  #:use-module (compat racket misc)
  #:use-module (logic guile-log guile-prolog vm vm-pre)
  #:use-module (logic guile-log guile-prolog vm vm-var)
  #:use-module (logic guile-log vm vm)
  #:use-module (system vm assembler)
  #:export (handle_all))

(include-from-path "logic/guile-log/guile-prolog/vm/vm-handle-model.scm")
