(define-module (logic guile-log guile-prolog vm vm-conj2)
  #:use-module (logic guile-log)
  #:use-module (logic guile-log iso-prolog)
  #:use-module (logic guile-log guile-prolog hash)
  #:use-module (logic guile-log guile-prolog ops)
  #:use-module (logic guile-log prolog swi)
  #:use-module (compat racket misc)
  #:use-module (system vm assembler)
  #:use-module (logic guile-log guile-prolog macros)
  #:use-module ((logic guile-log guile-prolog vm-compiler)
		#:select ())
  #:use-module (logic guile-log guile-prolog vm vm-pre)
  #:use-module (logic guile-log guile-prolog vm vm-var2)
  #:export (compile_conj collect_conj))

#;
(eval-when (compile)
(prolog-run-rewind 1 (x) 
		   (dyntrace (@@ (logic guile-log guile-prolog vm vm-goal) 
				 compile_goal))))

(eval-when (compile)
  (set! (@@ (logic guile-log slask) include-meta) #f))

(compile-prolog-string
"
- eval_when(compile).
the_tr2(X,[X]).
:- add_term_expansion_temp(the_tr2).
:- add_term_expansion_temp(extended_macro).
")

(include-from-path "logic/guile-log/guile-prolog/vm/vm-conj-model.scm")

(eval-when (compile)
  (set! (@@ (logic guile-log slask) include-meta) #t))
