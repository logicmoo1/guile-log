(<define> (get-unify-operators x) (<=> x unify_operators))

(compile-prolog-string "
compile_imprint(Y,V,L,LL,M) :-
   var_p(Y) -> 
    (!,
     add_var(Y,V,Tag),
     push_v(-1,V),
     tr(unify,Unify),
     (isFirst(Tag) -> true ; touch_A(V)),
     L=[[Unify,Tag,M]|LL],!
    ).

compile_imprint({Y},V, L, LL, M) :- !, 
   touch_A(V),
   (M=#f -> tr('icurly',Curly) ; tr('icurly!',Curly)),
   L=[[Curly]|L1],
   compile_imprint(Y,V,L1,LL,M).

compile_imprint([Y|LY],V, L,LL,M) :- !,
   touch_A(V),
   (M=#f -> tr('icons',Icons) ; tr('icons!',Icons)),
   L=[[Icons]|L1],
   push_v(1,V),
   compile_imprint(Y ,V,L1,L2,M),
   compile_imprint(LY,V,L2,LL,M).

compile_imprint(Y(|LY),V,L,LL,M) :- !,
   'get-unify-operators'(Ops),
   (
     (constant(Y),member([Op,Y],Ops)) ->
        (
           Op=='+'   ->
             (LY=[X],compile_imprint(X,V,L,LL,#t));
           Op=='*'   ->
             (LY=[X],compile_imprint(X,V,L,LL,0));
           Op=='-'   ->
             (LY=[X],compile_imprint(X,V,L,LL,#f));
           Op==','   ->
             perform_and(LY,V,L,LL,M);
           Op==';'   ->
             perform_or(LY,V,L,LL,M);
           Op=='\\+' ->
             perform_not(LY,V,L,LL,M)
        ) ;
     compile_imprint_fkn(Y(|LY),V,L,LL,M)
   ).

compile_imprint(Y,V,L,LL,M) :-
  push_v(-1,V),
  (
    constant(Y) -> 
      tr('unify-constant',Unify);
    tr('unify-instruction',Unify)
  ),
  L=[[Unify,Y,M]|LL].


compile_imprint_fkn(Y(|LY),V,L,LL,M) :-
   touch_A(V),
   (M=#f -> tr(ifkn,Ifkn); tr('ifkn!',Ifkn)),
   L=[[Ifkn]|L1],
   compile_imprint([Y|LY],V,L1,LL,M).

mapimp([],[],M).
mapimp([X|Xs],[imprint(X,M)|Ys],M) :-
  mapimp(Xs,Ys,M).

perform_or([],V,L,LL,M) :- !,
    tr(false,False),
    L=[[False]|LL].

perform_or([Y],V,L,LL,M) :- !,
    compile_imprint(Y,V,L,LL,M).

perform_or(U,V,L,LL,M) :-
   mapimp(U,UU,M),
   compile_goal(once(';'(|UU)),#f,V,[L,LL]).

perform_and([],V,L,L,M).

perform_and([Y],V,L,LL,M) :- !,
    compile_imprint(Y,V,L,LL,M).

perform_and([Y|LY],V,L,LL,M) :-
  L=[[dup]|L1],
  compile_imprint(Y,V,L1,L2,M),
  perform_and(LY,V,L2,LL,M).

perform_not(Y,V,L,LL,M) :-
  compile_goal((\\+imprint(Y,M)),#f,V,[L,LL]).

")
