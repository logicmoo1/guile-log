(define (ele s x)
  ((@@ (logic guile-log guile-prolog set) ele)
   s (lambda () (error "wrong set construction")) (lambda (s p x) x) x))

(set-object-property! ele 'prolog-functor-type #:scm)

(compile-prolog-string 
"
rev(X,Y) :-
  rev(X,[],Y).

rev([],X,X).
rev([A|X],Y,Z) :-
  YY = [A|Y],
  rev(X,YY,Z).

-extended.
compile_scm(X,V,L,LL) :-
  var_p(X)    -> 
   ( !,
     add_var(X,V,Tag),tr('push-variable-scm',Push),
     push_v(1,V),
     L=[[Push,Tag]|LL]
   );
  constant(X)    -> (!,tr('push-constant',Atom),      L=[[Atom  ,XX]|LL], 
                     E=X,regconst(X,XX), push_v(1,V));
  instruction(X) ->  (!,throw(X)).

%(!,tr('push-instruction',Atomic), L=[[Atomic,X]|LL], 
%                     E=X,push_v(1,V)).

compile_scm((Op, ';'(max,min,+,-,*,/,<<,>>,\\/,/\\,mod))
                  (X,Y),V,L,LL) :- !,
  ifc(compile_scm(X,V,L,LX),EX,
      (
        (number(EX) -> true ; throw(EX)),
        ifc(compile_scm(Y,V,L,LY),EY,
            (
              (number(EY) -> true ; throw(EY)),
              call(E is Op(EX,EY)),
              throw(E)
            ),
            (
              LY=[['push-instruction',EX],[Op]|LL]
            ))
      ),
      ifc(compile_scm(Y,V,LX,LY),EY,
      (
         (number(EY) -> true ; throw(EY)),
         LX=[['push-instruction',EY],[Op]|LL]
      ),
      (
         push_v(-1,V),
         tr(Op,O),
         LY=[[Op]|LL]
      ))).

compile_scm((Op,(+ ; - ; \\))(X),V,L,LL) :- !,
  ifc(compile_scm(X,V,L,LX),EX,
     (
         (number(EX) -> true ; throw(EX)),
         call(E is Op(EX)),
         throw(E)
     ),
     (
         Op=='+' -> LX=LL ;
         (
           (
             Op=='-' -> unop('op1_-',O) ;
             unop(Op,O)
           ),
          LX=[[O]|LL]
        )
    )).

compile_scm({X},V,L,LL) :- !,
   scm_caller(ele,[X],#f,V,L,LL).

compile_scm(F(|X),V,L,LL) :- !,
   rev(X,XX),
   allcom(XX,V,L,L1),
   scm_caller2(F,X,#f,V,L1,LL).
    
allcom([A|U],V,L,LL) :- !,
  ifc(compile_scm(A,V,L,L1),EX,
     (
         write('EX'(EX)),nl,          
         (number(EX) -> true ; throw(EX)),
         L=[['push-instruction',EX]|L1]
     ),
     (
       true
    )),
   allcom(U,V,L1,LL).

allcom([],V,L,L).


")
