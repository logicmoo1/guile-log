(define-module (logic guile-log guile-prolog closure)
  #:use-module (logic guile-log code-load)
  #:use-module (logic guile-log)
  #:re-export (prolog-closure?
               make-prolog-closure
               prolog-closure-closure
               prolog-closure-parent
               prolog-closure-state
               prolog-closure-closed?)
  #:export (closure_p closure_state_ref closure_is_closed closure_code_ref))
  

(<define> (closure_p x) (when (prolog-closure? (<lookup> x))))
(<define> (closure_state_ref x l) (<=> l ,(prolog-closure-state (<lookup> x))))
(<define> (closure_is_closed x) 
          (when (and (prolog-closure?        (<lookup> x))
                     (prolog-closure-closed? (<lookup> x)))))
(<define> (closure_code_ref x l) (<=> l ,(prolog-closure-parent (<lookup> x))))
          