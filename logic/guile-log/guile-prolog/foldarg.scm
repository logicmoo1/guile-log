(define-module (logic guile-log guile-prolog foldarg)
  #:use-module (logic guile-log)
  #:use-module (logic guile-log prolog goal-functors)
  #:use-module (logic guile-log prolog util)
  #:use-module (logic guile-log tools)
  #:export (fold_arg fix_fold_arg))

(<define> (fold_arg geq nil x arg Code)
   (<var> (l)
    (<fold>
     (lambda (x s)
       (
        (<lambda> ()
	   (<let*> ((x  (<lookup> x))
		    (sx (car s))
		    (sl (cdr s))
		    (xx (car x))
		    (xa (cdr x)))
	      (<if> (goal-eval (vector (list geq xx sx)))
		    (<if> (goal-eval (vector (list geq sx xx)))
			  (<cc> (cons sx (cons xa sl)))
			  (<cc> (cons xx (cons xa '()))))
		    (<cc> s))))
	S (lambda () #f) (lambda (s p x) x)))
     (cons nil '())
     (<lambda> () (goal-eval Code))
     (cons x arg)
     l)
    (member arg l)))

(<define> (fix_fold_arg geq nil x arg y Code)
   (<var> (l)
    (<fix-fold>
     (lambda (x s)
       (
        (<lambda> ()
	   (<let*> ((x  (<lookup> x))
		    (sx (car s))
		    (sl (cdr s))
		    (xx (car x))
		    (xa (cdr x)))
	      (<if> (goal-eval (vector (list geq xx sx)))
		    (<if> (goal-eval (vector (list geq sx xx)))
			  (<cc> (cons sx (cons xa sl)))
			  (<cc> (cons xx (cons xa '()))))
		    (<cc> s))))
	S (lambda () #f) (lambda (s p x) x)))
     (cons nil '())
     (<lambda> () (goal-eval Code))
     (cons x arg)
     y
     l)
    (member arg l)))
