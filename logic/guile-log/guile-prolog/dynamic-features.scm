(define-module (logic guile-log guile-prolog dynamic-features)
  #:use-module (logic guile-log)
  #:use-module (logic guile-log iso-prolog)
  #:use-module (logic guile-log dynamic-features)
  #:use-module (logic guile-log prolog error)
  #:use-module (logic guile-log prolog parser)
  #:use-module (logic guile-log prolog char-conversion)
  #:use-module (logic guile-log prolog directives)
  #:use-module (logic guile-log prolog names)
  #:use-module (logic guile-log prolog goal-functors)
  #:re-export (get_prolog_operators_handle
               get_prolog_flags_handle
               get_prolog_conversion_handle)
  #:export(backtrack_dynamic_object
	   not_backtrack_dynamic_object
	   fluid_guard_dynamic_object
	   state_guard_dynamic_object
	   state_guard_dynamic_object_zip
	   always_state_guard_dynamic_object 

	   with_fluid_guard_dynamic_object
	   with_fluid_guard_dynamic_object_once
	   with_state_guard_dynamic_object
	   with_state_guard_dynamic_object_zip
	   with_always_state_guard_dynamic_object 
	   with_backtrack_dynamic_object
	   with_not_backtrack_dynamic_object

           copy_dynamic_object
           set_dynamic

           dynamic_feature

	   add_dynamic_feature
	   ))

(mk-sym dynamic_feature)

(<define> (add_dynamic_feature . l)
  (<recur> lp ((l l))
    (if (pair? l)
	(<and>
	 (add-dynamic-function-dynamics (<lookup> (car l)))
	 (lp (cdr l)))
	<cc>)))
	
(<define> (fail- h) (type_error dynamic_feature h))

(define-syntax-rule (mk backtrack_dynamic_object backtrack-dynamic-object)
  (<define> (backtrack_dynamic_object . h)
    (<recur> lp ((h h))
      (if (pair? h)
          (<and>
           (backtrack-dynamic-object (<lookup> (car h)) fail-)
           (lp (cdr h)))
          <cc>))))


(mk        backtrack_dynamic_object           backtrack-dynamic-object)
(mk    not_backtrack_dynamic_object       not-backtrack-dynamic-object)
(mk        fluid_guard_dynamic_object         fluid-guard-dynamic-object)
(mk        state_guard_dynamic_object         state-guard-dynamic-object)
(mk always_state_guard_dynamic_object  always-state-guard-dynamic-object)

(define-syntax-rule (mk-with a_b a-b)
  (define a_b
    (<case-lambda>
     ((h code)
      (a-b (<lookup> h) (<lambda> () (goal-eval code)) fail-))
     ((h  . l)
      (a-b (<lookup> h) (<lambda> () (<apply> a_b l)) fail-)))))

(mk-with with_fluid_guard_dynamic_object 
         with-fluid-guard-dynamic-object)

(mk-with with_fluid_guard_dynamic_object_once 
         with-fluid-guard-dynamic-object-once)

(mk-with with_state_guard_dynamic_object
         with-state-guard-dynamic-object)

(mk-with with_state_guard_dynamic_object_zip
         with-state-guard-dynamic-object-zip)

(mk-with with_always_state_guard_dynamic_object 
         with-always-state-guard-dynamic-object)

(mk-with with_backtrack_dynamic_object
         with-backtrack-dynamic-object)

(mk-with with_not_backtrack_dynamic_object
         with-not-backtrack-dynamic-object)

(<define> (copy_dynamic_object_ x y) (copy-dynamic-object x y #:fail fail-))
(compile-prolog-string 
"
copy_dynamic_object(X->Y) :- copy_dynamic_object_(X,Y).
")

(<define> (failxy s p cc x y tx ty) (fail- y))          
(<define> (set_dynamic_ x y) (set-dynamic x y #:fail fail #:failxy failxy))
(compile-prolog-string "set_dynamic(X->Y) :- set_dynamic_(X,Y).")

