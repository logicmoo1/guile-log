(define-module (logic guile-log guile-prolog project)
  #:use-module (logic guile-log) 
  #:use-module (logic guile-log iso-prolog)
  #:use-module (logic guile-log umatch)
  #:use-module (logic guile-log prolog error)
  #:use-module (logic guile-log prolog names)
  #:use-module (logic guile-log prolog goal-functors)
  #:use-module (logic guile-log attributed)
  #:use-module (logic guile-log guile-prolog attribute)
  #:use-module (logic guile-log dynamic-features)
  #:use-module (logic guile-log vlist)
  #:re-export (multibute)
  #:export (project_the_attributes))

(<define> (seen H X)
  (if (hash-ref H (<lookup> X) #f)
      <cc>
      <fail>))

(<define> (add H X)
  (<code> (hash-set! H (<lookup> X) #t)))

(compile-prolog-string
"
%k(A,B,C,H)  :- write(k(A,B,C)),nl,fail.
k(V,L,LL,H) :- (var(V),!) -> (attvar(V) -> L=[V|LL] ; L=LL).
k([],L,L,H) :- !.
k(X,L,LL,H) :- 
  X==[]    ->
    L=LL;
  X=[A|B] ->
    (
      seen(H,X) ->
         LL=L;            
      (
        add(H,X),
        k(A,LX,LL,H),
        k(B,L,LX,H)
      )
    );
  atomic(X) -> 
    L=LL; 
  X={Y} ->
    k(Y,L,LL,H);
  X=..[X|U] -> 
    k([X|U],L,LL,H) ; 
  L=LL. 
")

(<define> (project_the_attributes q)
  (<var> (l)
    (k q l '() (make-hash-table))
    (let lp ((v vlist-null) (l l))
       (<<match>> (#:mode -) (l)
         ((x . l)
	  (<let> ((x (<lookup> x)))
	    (if (gp-attvar-raw? x S)
		(<recur> lp2 ((v v) (d (gp-att-data x S)))
		  (<match> (#:mode -) (d)
		   (((a . _) . d)
		     (<let*> ((a (<lookup> a))
			      (f (ref-attribute-projector a)))
		       (if (and f (not (vhashq-ref v f #f)))
			   (lp2 (vhash-consq f #t v) d)
			   (lp2 v d))))
		    (()
		     (lp v l))))
		(lp v l))))
	 (()
	  (<recur> lp ((u  (map car (vhash->assoc v))))	    
	    (<<match>> (#:mode -) (u)
	     ((x . u)
	      (<and>
	       (<or> (<and> ((<lookup> x) q l) <cut>) <cc>)
	       (lp u)))
	     (() <cc>))))))))
