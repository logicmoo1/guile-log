(define-module (logic guile-log guile-prolog vm-compiler)
  #:use-module (logic guile-log)
  #:use-module (ice-9 pretty-print)
  #:use-module (logic guile-log iso-prolog)
  #:use-module (logic guile-log guile-prolog hash)
  #:use-module (logic guile-log guile-prolog ops)
  #:use-module (logic guile-log prolog swi)
  #:use-module (logic guile-log vlist)
  #:use-module (logic guile-log match)
  #:use-module (logic guile-log guile-prolog macros)
  #:use-module (logic guile-log guile-prolog vm vm-pre)
  #:use-module (logic guile-log guile-prolog vm vm-var)
  #:use-module (logic guile-log guile-prolog vm vm-goal)
  #:use-module (logic guile-log prolog compile)
  #:use-module ((logic guile-log umatch) #:select (gp-var! *current-stack*))
  #:use-module (system vm assembler)
  #:re-export (compile_goal begin_att end_att cc pr extended_off extended_on)
  #:export (compilable_scm 
	    collect_data define-prolog-fkn
	    make-vm-function
	    compile_to_fkn
	    instr define-prolog
	    generate-prolog-assembler
	    mockalambda))

#|
   This is the pre compiler that is compiled using scheme version of the prolog
   compiler, used in bootstrapping
|#

(include-from-path "logic/guile-log/guile-prolog/vm/vm-compiler.scm")
