(define-module (logic guile-log guile-prolog coroutine)
  #:use-module ((logic guile-log)
		#:select
		(<define> <var> <gc-call> <code> <case-lambda>
			  <lambda> <=> <scm> <wrap-s> <pp> <lookup>
			  <set> <let> <let*> <or> <attvar-raw?> <cp>
			  <modvar> <var?> <fail> <cc> S))
  #:use-module (logic guile-log iso-prolog)
  #:use-module (logic guile-log attributed)
  #:use-module (logic guile-log prolog goal-functors)
  #:use-module (logic guile-log prolog variant)
  #:use-module (logic guile-log guile-prolog attribute)
  #:use-module (logic guile-log guile-prolog hash)
  #:use-module (logic guile-log guile-prolog gc-call)
  #:use-module (logic guile-log guile-prolog attributator)
  #:use-module (logic guile-log umatch)
  #:replace (when)
  #:export (freeze frozen when dif for_all_goals 
		   with_coroutines freezeId whenId auto eq))


(define guard (@@ (logic guile-log umatch) doit-guard))
(define with_coroutines 
  (<case-lambda>
   ((V Constr Code)
    (goal-eval Constr)
    (<let> ((V (<lookup> V)))
     (<attvar-raw?> V)
     (<let> ((L  (<cp> (gp-att-data V S))))
       (with_attributate L '()
	  (<lambda> () 
	     (goal-eval Code))))))

   ((V U Constr Code)
    (goal-eval Constr)    
    (<var> (W Q)
      (get_var U '() W (list Q))   
      (<let> ((V (<lookup> V))
	      (W (<scm> W)))
	 (<attvar-raw?> V)
	 (<let> ((L  (<cp> (gp-att-data V S) W)))
	    (with_attributate L W
	       (<lambda> () 
		  (goal-eval Code)))))))))


(define doWhen           (gp-make-var))

(define (process_unwinder s p cc code)
  (let ((code (gp-scm-unpin code s)))
    ((@@ (logic guile-log umatch) dyn)
     (lambda x #f)
     (lambda x
       (gp-add-unwind-hook
	(lambda () 
	  (<wrap-s> 
	   (<lambda> ()
	    (<gc-call>  1 '() (<lambda> () (goal-eval code))))
	   s))))
		     
     s)
    (cc s p)))

(<define> (eq x y)
  (<let> ((x (<lookup> x))
	  (y (<lookup> y)))
    (if (eq? x y) <cc>)))


(compile-prolog-string
"
varx(X) :- var(X),\\+((attvar(X),test_attr(X,nonvar))).
nonvarx(X) :- \\+(varx(X)).
groundx(X) :-
   varx(X) -> 
     (!,fail);
   attvar(X) -> 
     (!,unop_attr(X,groundx));
   fail.

groundx([X|Y]) :- !,
   groundx(X),
   groundx(Y).

groundx(X(|Y)) :- !,
   groundx(X),
   groundx(Y).

groundx({X}) :- !, 
   groundx(X).

groundx(_).



unifiablex(X,Y) :- unifiable(X,Y,[_|_]).

do_all_atts([]).
do_all_atts([[]   ,G|L]) :- G, do_all_atts(L).
do_all_atts([[Unw],G|L]) :-          
   process_unwinder(Unw),G,!, do_all_atts(L).


% The unification algorithm fro freeze
% =
freezeId(Atts1, Val, #t) :- !,
  (
    get_attr(Val,freezeId,Atts2) ->
      (
        append(Atts1,Atts2,Atts),
        put_attr(Val,freezeId,Atts)
      );
    varx(Val) ->
       put_attr(Val,freezeId,Atts1);
    (
       do_all_atts(Atts1)
    )
  ).

% ==
freezeId(X,Val,#f)  :- !, freezeId(X,Val,Op).

freezeId(DX,Val,Op) :-
   get_attr(Val,freezeId,DY),
   Op(DX,DY).

% =@=
freezeId(V,Val,IX,IY,HX,HY,EX,EY,Op) :-
   raw_attvar(V,X),
   Op(X,Val,IX,IY,HX,HY,EX,EY).

% u
freezeId(V,W,L,Pred,H,Op) :- !,
   LL = [[V=W]|L],
   cc(LL).    

freeze(Var,Goal) :-
   ( 
      get_attr(Var, freezeId, Att),!,
      Att2 = [[],Goal|Att],
      put_attr(Var, freezeId, Att2)
   );
   (
      put_attr(Var, freezeId, [[],Goal]),!
   );
   Goal.

freeze(V,L,LL,Tok,get_var) :- LL=[V|L]. 

combine_atts([_,G,U |L], (G,GL)) :- combine_atts([U|L], GL).
combine_atts([_,G], G).
combine_atts([],true).

frozen(Var,Goal) :-
  get_attr(Var, freezeId, Atts) ->
     combine_atts(Atts,Goal);
     Goal = true.

get_var(X,V,VV,Tok1) :-
   get_attr(X,whenId,[Tok2|_]) ->
      (!,(Tok1==Tok2 -> VV=V ; (memqq(X,V) -> VV=V; VV=[X|V])),!);
   (nonvarx(X),attvar(X)) ->
      (!,resolve_attr(X,V,VV,Tok1,get_var),!);
   var(X) ->
      (!,(memqq(X,V) -> VV=V; VV=[X|V]),!);
   fail.

get_var([X|Y],V,VV,Tok) :- !,
  get_var(X,V,V1,Tok),
  get_var(Y,V1,VV,Tok).

get_var(F(|X),V,VV,Tok) :- !,
  get_var(F,V,V1,Tok),
  get_var(X,V1,VV,Tok).

get_var(_,V,V,_).


memqq(Y,[X|L]) :- X==Y -> true ; memqq(Y,L).

has_tok(Tok,[[T|_]|L]) :-
   T==Tok -> 
     (!);
   has_tok(Tok,L).

refine(_,_,[]).
refine(X,Tok,[V|VV]) :-
   (
     get_attr(V,whenId,L) ->
         (
            hasTok(Tok,L) -> 
               true;
            put_attr(V,whenId,[X|L])
         );
     varx(V) ->
         put_attr(V,whenId,[X]);
     true
   ),
   refine(X,Tok,VV).

do_all(_,[]).
do_all(Val,[[Tok,G,Action]|L]) :-
  Tok == [1] ->
     true;
  (
     G -> 
       (  !, 
          Action, 
          Tok=[1], 
          do_all(L,VV)
       ); 
     (
        get_var(Val,[],VV,Tok),!,
        refine([Tok,G,Action],Tok,VV),
        do_all(Val,L)
     )
  ).


%=
whenId(LX,Val,#t) :-  !,
  (
    get_attr(Val,whenId,LY) ->
      (
        append(LX,LY,L),
        put_attr(Val,whenId,L)
      );
    varx(Val) ->
       put_attr(Val,freezeId,LX);
    (
       do_all(Val,LX),!
    )
  ).

%==
whenId(Att,Val,#f) :-  !, whenId(Att,Val,==).

whenId(Att,Val,Op) :-  !,
  get_attr(Val,whenId,Att2),
  Op(Att1,Att2).

%=@=
whenId(Att,Val,IX,IY,HX,HY,EX,EY,Op) :-  !,
   raw_attvar(V,X),
   Op(X,Val,IX,IY,HX,HY,EX,EY).

%u
whenId(V,W,L,Pred,H,Op) :- !,
   LL = [[V=W]|L],
   cc(LL).

add(X,V,VV) :-
  memqq(X,V) -> VV=V; VV=[X|V].

addvars([],V,V).
addvars([X|L],V,VV) :- add(X,V,V1), addvars(L,V1,VV).

process_when((X,Y),(XX,YY),V,VV) :- !,
   process_when(X,XX,V,V1),
   process_when(Y,YY,V1,VV).

process_when((X;Y),(XX;YY),V,VV) :- !,
   process_when(X,XX,V,V1),
   process_when(Y,YY,V1,VV).

process_when(nonvar(X),Y,V,VV) :- !, 
  (
     varx(X) -> 
        (
          add(X,V,VV),
          Y=nonvarx(X)
        ) ; 
     (V=VV,Y=true)
  ).
   
process_when(ground(X),Y,V,VV) :- !, 
  (
    get_var(X,[],U,_), 
    append(V,U,VV),
    (
       U==[] ->
          Y=true;
       Y=groundx(X)
    )
  ).

process_when(F(X,Y),Z,V,VV) :- !, F=\"?=\",
  (
     unifiable(X,Y,L) ->
        (
           L=[] -> 
             (V=VV, Z=true);
           (get_var(L,V,VV,[_]),Z=(\\+unifiablex(X,Y)))
        );
     (V=VV, Z=true)
  ).

process_when(X,_,_,_) :- syntax_error(when(X)).

construct_when([],_).
construct_when([V|VV],X) :-
  (
     get_attr(V,whenId,L0) ->
       (
          L = [X|L0],
          put_attr(V,whenId,L)
       );
     put_attr(V,whenId,[X])
   ),
   construct_when(VV,X).

when_co(Cond,Action) :-
  Cond -> Action ;
  (
    process_when(Cond,Goal,[],VV),!,
    construct_when(VV,[[_],Goal,Action])
  ).

dif(X,Y) :- when_co(X?=Y, X\\==Y).
")

(define when when_co)


(<define> (auto x)
  (<let> ((x (<lookup> x)))
    (if (<var?> x)
	(<modvar> (y) (<set> x y))
	<fail>)))

(delayed-attribute freezeId)
(delayed-attribute whenId)
