(define-module (logic guile-log guile-prolog canon)
  #:use-module ((logic guile-log) #:select 
                (<clear> <define> <let> <let*> <=> <lookup> <match> <fail>
                         <cut> <wrap> <state-ref> <state-set!> <continue>
                         <code> <scm> <stall> <case-lambda> <cc> <var>
                         <newframe> <=> <and> <lambda> <apply> <pp>))
  #:use-module (logic guile-log iso-prolog)
  #:use-module (logic guile-log vlist)
  #:use-module (logic guile-log guile-prolog hash)
  #:use-module (logic guile-log guile-prolog dynamic-features)
  #:use-module (logic guile-log memoize)
  #:export (canonize recursive_canonize with_canon))

(<define> (bang H X)
  (<let> ((X (<lookup> X))
	  (H (<lookup> H)))
    (when (hashq-ref H X #f))))

(<define> (neq x y)
  (when (not (eq? (<lookup> x) (<lookup> y)))))
(<define> (eq x y)
  (when (eq? (<lookup> x) (<lookup> y))))

(<define> (hset h x)
   (<code> (hashq-set! (<lookup> h) (<lookup> x) #t)))

(<define> (trunc h)
  (<let> ((h (<lookup> h)))
    (<code> (vhash-truncate! (fluid-ref h)))))

(define ff #f)
(compile-prolog-string
"
find(X,O,H,W) :-
 copy_dynamic_object(W->WW), find0(X,O,H,WW).
 
find0(X,O,H,W):-
 (
   bang(H,X) -> 
    (vhashq_ref(W,X,_) -> fail ; (vhashq_cons(W,X,#t), ffind(X,O,H,W)));
   ffind(X,O,H,W)
 ).

ffind(X,O,H,W) :-
  var(X)  -> (!,fail);  
  X=[A|B] -> ( 
                 X=O;
                 find0(B,O,H,W); 
                 find0(A,O,H,W)
             ).
f(X,Z,H,W) :- 
 bang(H,X) -> (vhashq_ref(W,X,ZZ) -> Z=ZZ ; 
               (vhashq_cons(W,X,Z),ff(X,Z,H,W)));
 ff(X,Z,H,W).
 
ff(X,Z,H,W) :-
  var(X)  -> Z=X;
  X=[A|B] ->
    (!,(
      (find(B,U,H,W), neq(X,U), 'rec=='(U,X)) 
           -> (!,trunc(W),hset(H,U),f(U,Z,H,W));
      (find(A,U,H,W), neq(X,U), 'rec=='(U,X))
           -> (!,trunc(W),hset(H,U),f(U,Z,H,W));
      (
         Z=[AZ|BZ], 
         f(A,AZ,H,W), 
         f(B,BZ,H,W)
      )
    ));
  Z=X.

fstart(X,Z,H) :- make_vhash(W), f(X,Z,H,W),!.
")

(<define> (recursive_canonize x z)
  (<let> ((m (make-hash-table)))
    ((with-atomic-frec (<lambda> (x)
			(rec-action00
			 (<lambda> (x) (<code> (hashq-set! m (<lookup> x) #t)))
			 x)))
     x)		       		     
    (fstart x z m)))

(<define> (canonize f . x)
  (<var> (v)
    (recursive_canonize x v)
    (<apply> f v)))

(<define> (canonize2 x)
  (<var> (v)
    (recursive_canonize x v)
    (<cc> v)))

(define with_canon (with-canon canonize2))

