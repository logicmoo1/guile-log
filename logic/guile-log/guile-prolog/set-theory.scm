(define-module (logic guile-log guile-prolog set-theory)
  #:use-module (logic guile-log)
  #:use-module (logic guile-log vset)
  #:use-module (logic guile-log iso-prolog)
  #:use-module (logic guile-log guile-prolog set)
  #:use-module (logic guile-log inheritance)
  #:export (use_set_theory sets_to_theory))


(define (sets->theory sets s)
  (let lp ((l sets))
    (if (pair? l)
	(begin
	  (new-set (car l))
	  (lp (cdr l)))))
    
  (let lp1 ((l1 sets))
    (if (pair? l1)
	(begin
	  (let lp2 ((l2 (cdr l1)))
	    (if (pair? l2)
		(begin
		  (begin
		    (cond
		     ((<wrap-s> subset-scm s (car l1) (car l2))
		      (a->b (car l2) (car l1)))
		     ((<wrap-s> subset-scm s (car l2) (car l1))
		      (a->b (car l1) (car l2)))
		     (else #t))
		    (lp2 (cdr l2))))
		(lp1 (cdr l1))))))))

(<define> (use_set_theory theory)
  (<code> (fluid-set! *current-set-theory* (<lookup> theory))))

(<define> (sets_to_theory sets)
  (let lp ((l sets) (ll '()))
    (<match> (#:mode -) (l)
      ((x . l) (lp l (cons (<lookup> x) ll)))
      (()
       (<code> (sets->theory ll S))))))

