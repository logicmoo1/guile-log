(define-module (logic guile-log guile-prolog attribute)
  #:use-module (logic guile-log) 
  #:use-module (logic guile-log umatch)
  #:use-module (logic guile-log prolog error)
  #:use-module (logic guile-log prolog names)
  #:use-module (logic guile-log prolog goal-functors)
  #:use-module (logic guile-log attributed)
  #:use-module (logic guile-log dynamic-features)
  #:use-module (logic guile-log vlist)
  #:re-export (multibute)
  #:export (attvar put_attr put_attr_guarded put_attr_weak_guarded
		   put_attr_x put_attr_guarded_x put_attr_weak_guarded_x
		   get_attr get_attrs del_attr del_attr_x raw_attvar
		   construct_attr attribute_cstor attach_attribute_cstor
		   call_residue_vars build_attribut_representation
		   attribute_prefix del_attrs del_attrs_x 
		   verify_attributes
		   lookup get-att-var resolve_attr test_attr unop_attr
                   binop_attr value_attr))

(define *touched-attributes* (make-fluid vlist-null))
(<wrap> add-vhash-dynamics *touched-attributes*)
(fluid-set! *touched-attributes* #f)

(define-syntax-rule (attribute_unifyier x)
  (cons '() x))

(<define> (call_residue_vars goal vars)
   (with-fluid-guard-dynamic-object *touched-attributes*
      (<lambda> ()
        (with-backtrack-dynamic-object *touched-attributes*
	  (<lambda> ()
	    (with-state-guard-dynamic-object *touched-attributes*
	       (<lambda> ()
		  (<code> (fluid-set! *touched-attributes* vlist-null))
		  (goal-eval goal)
		  (<recur> lp ((r    '()) 
			       (atts (vhash->assoc 
				      (fluid-ref *touched-attributes*))))
		     (if (pair? atts)
			 (<let> ((x (<lookup> (caar atts))))
			    (if (gp-attvar-raw? x S)
				(lp (cons x r) (cdr atts))
				(lp r          (cdr atts))))
			 (<=> vars r))))))))))

(define (attribute_cstor cstor)  	  
  (<lambda> (x)
    (<var> (out)
       (cstor out '() x)
       (<recur> lp ((l out))
         (<<match>> (#:mode -) (l)
            ((x . l)
	     (<and>
	      (goal-eval x)
	      (lp l)))
	    (_ <cc>))))))

(define (attach_attribute_cstor f cstor)
  (set-attribute-cstor! f (cons cstor (attribute_cstor cstor)))
  f)

(define attribute_prefix 
  (case-lambda 
   ((f)
    (cons f #f))  
   ((f g)
    (cons g f))))

#|
(<define> (all_attvars V)
   (<let> ((v (vhash->assoc (fluid-ref attlist))))
|#
       
(<define> (attvar X) (when (gp-attvar-raw? (<lookup> X) S)))

(define-syntax-rule (mkput put_attr <put-attr>)
(<define> (put_attr Var Mod Val)  
  (<let> ((Mod (<lookup> Mod))
	  (Var (<lookup> Var)))
       (if (fluid-ref *touched-attributes*)
	   (if (vhashq-ref (fluid-ref *touched-attributes*) Var #f)
	       <cc>
	       (<code>
		(fluid-set! *touched-attributes*
			    (vhash-consq Var #t
					 (fluid-ref *touched-attributes*)))))
	   <cc>)
       (<if> (<attvar?> Var)
	     (<and>
	      (<if> (atom CUT S Mod)
		    (<put-attr> Var Mod Val)
		    (type_error Mod atom)))
	     (representation_error (cons* "attribute" Var Mod))))))
	     

(mkput put_attr                <put-attr>)
(mkput put_attr_guarded        <put-attr-guarded>)
(mkput put_attr_weak_guarded   <put-attr-weak-guarded>)
(mkput put_attr_x              <put-attr!>)
(mkput put_attr_guarded_x      <put-attr-guarded!>)
(mkput put_attr_weak_guarded_x <put-attr-weak-guarded!>)

(<define> (get_attr Var Mod Val)  
   (<let> ((Mod (<lookup> Mod))
	   (Var (<lookup> Var)))
      (<if> (atom CUT S Mod)
	    (<get-attr> Var Mod Val)
	    (type_error Mod atom))))


(<define> (construct_attr Y L)
  (<recur> lp ((L L))
    (<match> (#:mode -) (L)
      (((A . B) . L)
       (<cut>
	(<put-attr> Y A B)
	(lp L)))

      (() 
       (<cut> <cc>)))))

(<define> (get_attrs x m v) (<get-attrs> x m v))

(<define> (raw_attvar x z)
   (<let> ((x (<lookup> x)))
     (<if> (attvar x)
	   (<and>
	    (<values> (y) (<raw-attvar> x))
	    (doit_off)
	    (<r=> z y) 
	    (doit_on))
	   (type_error x attvar))))

(<define> (del_attr Var Mod)
   (<let> ((Mod (<lookup> Mod)))
      (<if> (atom CUT S Mod)
	    (<del-attr> Var Mod)
	    (type_error Mod atom))))

(<define> (del_attr_x Var Mod)
   (<let> ((Mod (<lookup> Mod)))
      (<if> (atom CUT S Mod)
	    (<del-attr!> Var Mod)
	    (type_error Mod atom))))

(<define> (del_attrs Var)
  (<let> ((x (<lookup> Var)))
    (if (gp-attvar-raw? x S)
	(<var> (v)
	   (<set> x v))
	<cc>)))

(<define> (del_attrs_x Var)
  (<let> ((x (<lookup> Var)))
    (if (gp-attvar-raw? x S)
	(<var> (v)
	   (<set!> x v))
	<cc>)))

(<define> (lookup x y) (<=> y ,(<lookup> x)))

(define verify_attributes 
  (case-lambda 
    ((verifyer)
     (cons (<lambda> (var val goals f)
       (<var> (l)
	      (verifyer var val l)
	      (<=> goals ,(<lambda> ()
			    (<recur> lp ((l l))
				     (<match> (#:mode -) (l)
                                   ((x . l)
				    (<and> (goal-eval x) (lp l)))
				   (()
				    (<cc>))))))))
	   '()))
    ((verifyer unifier)
     (let ((ret (verify_attributes verifyer)))
       (set-cdr! ret unifier)
       ret))))
			      
(set! (@@ (logic guile-log run) put_attr) put_attr) 

(<define> (resolve_attr x l ll . u)
  (<recur> lp ((data (<lookup> (gp-att-data (<lookup> x) S))) (l l))
     (if (pair? data)
         (<var> (lx)
           (<apply> (caar data) x l lx u)
           (lp (cdr data) lx))
         (<=> l ll))))

(<define> (test_attr x a)
  (<recur> lp ((data (<lookup> (gp-att-data (<lookup> x) S))))
     (if (pair? data)
	 (<if> ((caar data) a)
	       <cc>
	       (lp (cdr data)))
         <fail>)))

(<define> (unop_attr x a)
  (<recur> lp ((data (<lookup> (gp-att-data (<lookup> x) S))))
    (if (pair? data)
	(<and>
	 ((caar data) x a)
	 (lp (cdr data)))
	<cc>)))

(<define> (binop_attr x y a)
  (<recur> lp ((data (<lookup> (gp-att-data (<lookup> x) S))))
    (if (pair? data)
	(<and>
	 ((caar data) x y a)
	 (lp (cdr data)))
	<cc>)))
        
(<define> (value_attr x) (attvar x) (test_attr x "nonvar"))

    
