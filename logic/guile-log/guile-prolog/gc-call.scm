(define-module (logic guile-log guile-prolog gc-call)
  #:use-module (logic guile-log)
  #:use-module (logic guile-log prolog goal-functors)
  #:export (gc_call gc_scm_call gc_list_call))


(<define> (gc_call Var Code)
  (<gc-call> Var '() (<lambda> () (goal-eval Code))))

(<define> (gc_scm_call Var Code)
  (<gc-scm-call> Var (<lambda> () (goal-eval Code))))

(<define> (gc_list_call Var Code)
  (<gc-list-call> Var (<lambda> () (goal-eval Code))))

