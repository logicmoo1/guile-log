#|
Load this code in guile with clambda defined and it will generate a prolog-vm
stub that is linked into the guile-log shared library the good thing with this 
system is that the generated c-engine is synchronized with the scheme and 
prolog code. This makes the numbering of the labels quite robust and fault 
tolerant.

you need to load this file in guile with clambda and guile-log in the path.
Then you will generte a file called prolog-vm.c which should be placed in the
logic/guile-log/src directory and then make/make install to modify guile-log
The system for maximal reuse is to model the data as a cons list
(always midsle session . constant)

always    = st:  (inst)
            cc:  (inst ctrl-stack p0 . p)
             p:  (inst ctrl-stack) 
session   = #(cc p vars ...)

constant  = #(nlocals nstack constants code)
|#

(use-modules (language clambda clambda))
(use-modules (language clambda scm))
(use-modules (logic guile-log guile-prolog vm-compiler))
(eval-when (eval compile load)
  (define touch (make-hash-table)))

(init-clambda-scm)

(auto-defs)

(eval-when (compile eval load)
  (define debug #t))

(define-syntax DB
  (lambda (x)
    (syntax-case x ()
      ((_ code)
       (begin
	 (pk (syntax->datum #'code))
	 (if debug
	     #'code
	     #'(<scm> #t)))))))

(<global> (%array (void *) 1024) *operations*)
(<global> SCM *dls*      (<scm> #f))
(<global> SCM *delayers* (<scm> #f))
(<global> SCM *unwind-hooks*      (<scm> #f))	   
(<global> SCM *unwind-parameters* (<scm> #f))	   
(<global> SCM *true*              (<scm> #f))	   
(<global> SCM *false*             (<scm> #f))
(<global> SCM *gp-not-n*          (<scm> #f))
(<global> SCM *gp-is-delayed?*    (<scm> #f))

(<declare-s> void INTERUPT  ())
(<declare-s> void gp_gc     ())
(<declare-s> void gp_gp_prune      (SCM s))
(<declare-s> void gp_gp_prune_tail (SCM s))
(<declare-s> SCM gp_get_state_token  ())
(<declare-s> SCM car   (SCM x))
(<declare-s> SCM cdr   (SCM x))
(<declare-s> int consp (SCM x))
(<declare-s> SCM gp_cons_bang ((SCM x) (SCM y) (SCM s)))
(<declare-s> SCM gp_cons_simple ((SCM x) (SCM y) (SCM s)))
(<declare-s> SCM scm_gr_p   ((SCM x) (SCM y)))
(<declare-s> SCM scm_less_p ((SCM x) (SCM y)))
(<declare-s> SCM scm_geq_p  ((SCM x) (SCM y)))
(<declare-s> SCM scm_leq_p  ((SCM x) (SCM y)))
(<declare-s> gp_copy_vector          (((SCM **) vector) (int nvar)))
(<declare-s> SCM scm_sum             ((SCM x) (SCM y)))
(<declare-s> SCM scm_product         ((SCM x) (SCM y)))
(<declare-s> SCM scm_divide          ((SCM x) (SCM y)))
(<declare-s> SCM gp_m_unify          ((SCM x) (SCM y) (SCM s)))
(<declare-s> SCM gp_gp_unify         ((SCM x) (SCM y) (SCM s)))
(<declare-s> SCM gp_gp_unify_raw     ((SCM x) (SCM y) (SCM s)))
(<declare-s> (SCM *) SCM_I_VECTOR_WELTS ((SCM x)))
(<declare-s> int   scm_to_int        ((SCM i)))
(<declare-s> ulong scm_to_ulong      ((SCM i)))
(<declare-s> SCM scm_from_int        ((int i)))
(<declare-s> int scm2int             ((SCM x)))
(<declare-s> ulong scm2ulong         ((SCM x)))
(<declare-s> SCM scm_difference      ((SCM a) (SCM b)))
(<declare-s> SCM scm_logand          ((SCM a) (SCM b)))
(<declare-s> SCM scm_logior          ((SCM a) (SCM b)))
(<declare-s> SCM scm_logxor          ((SCM a) (SCM b)))
(<declare-s> SCM scm_lognot          ((SCM a)))
(<declare-s> SCM scm_modulo          ((SCM a) (SCM b)))
(<declare-s> SCM scm_ash             ((SCM a) (SCM b)))
(<declare-s> SCM gp_cons_bang        ((SCM a) (SCM b) (SCM s)))
(<declare-s> int SCM_NIMP            ((SCM s)))
(<declare-s> SCM scm_reverse         ((SCM x)))
(<declare-s> SCM scm_cons            ((SCM x) (SCM e)))
(<declare-s> int SCM_CONSP           ((SCM x)))
(<declare-s> SCM scm_c_vector_ref    ((SCM x) (size_t i)))
(<declare-s> SCM scm_c_vector_set_x  ((SCM x) (size_t i) (SCM val)))
(<declare-s> SCM scm_c_make_vector   ((size_t i) (SCM deflt)))
(<declare-s> SCM SCM_CAR             ((SCM x)))
(<declare-s> SCM SCM_CDR             ((SCM x)))
(<declare-s> SCM scm_call_0          ((SCM f)))
(<declare-s> SCM scm_call_1          ((SCM f) (SCM x)))
(<declare-s> SCM scm_call_3          ((SCM f) (SCM x) (SCM y) (SCM z)))
(<declare-s> SCM scm_fluid_ref       ((SCM x)))
(<declare-s> SCM scm_fluid_set_x     ((SCM x) (SCM val)))
(<declare-s> SCM gp_gp_lookup        ((SCM x) (SCM s)))
(<declare-s> SCM lookup              ((SCM x) (SCM s)))
(<declare-s> SCM gp_car              ((SCM x) (SCM  s)))
(<declare-s> SCM gp_gp_cdr           ((SCM x) (SCM  s)))
(<declare-s> SCM gp_with_fluid       ((SCM x) (SCM y)))
(<declare-s> SCM gp_gp_newframe      ((SCM s)))
(<declare-s> SCM gp_gp_unwind_ncons  ((SCM s) (int ncons)))
(<declare-s> SCM gp_gp_unwind_soft   ((int ncons)))
(<declare-s> SCM gp_gp_unwind_tail   ((SCM s)))
(<declare-s> SCM gp_gp_unwind        ((SCM s)))
(<declare-s> SCM gp_set              ((SCM a) (SCM b) (SCM s)))
(<declare-s> int scm_is_true         ((SCM x)))
(<declare-s> int scm_is_false        ((SCM x)))
(<declare-s> SCM gp_fluid_force_bang ((SCM a) (SCM b)))
(<declare-s> SCM gp_pair_bang        ((SCM x) (SCM s)))
(<declare-s> SCM gp_c_vector_x       ((SCM x) (int i)))
(<declare-s> SCM gp_c_vector         ((SCM x) (int i)))
(<declare-s> int scm_is_eq           ((SCM a) (SCM b)))
(<declare-s> SCM scm_equal_p         ((SCM a) (SCM b)))
(<declare-s> SCM gp_pair             ((SCM x) (SCM s)))
(<declare-s> int SCM_I_INUMP         ((SCM x)))
(<declare-s> int SCM_I_INUMP         ((SCM x)))
(<declare-s> scm_t_int64 SCM_I_INUM  ((SCM x)))
(<declare-s> int SCM_FIXABLE         ((scm_t_int64 x)))
(<declare-s> SCM SCM_I_MAKINUM       ((scm_t_int64 x)))
(<declare-s> int SCM_I_IS_VECTOR     ((SCM x)))
(<declare-s> int SCM_I_VECTOR_LENGTH ((SCM x)))
(<declare-s> SCM gp_mkvar            ((SCM s)))
(<declare-s> SCM scm_variable_ref    ((SCM s)))
(<declare-s> SCM scm_make_variable   ((SCM s)))
(<declare-s> int SCM_VARIABLEP       ((SCM s)))
(<declare-s> int gp_varp             ((SCM x) (SCM s)))
(<declare-s> int gp_pair             ((SCM x) (SCM s)))

(<declare-s> SCM gp_custom_fkn       ((SCM model) (SCM a) (SCM b) (SCM c)
				      (SCM d)))
(<declare-s> remove_me_t gp_make_vm_model ())

(define-syntax-rule (MAYBE-ADJUST-VECTOR 
		     pinned? variables variables-scm nvar
		     cnst session middle)
  (<if> (TRUE pinned?)
	(<begin>
	 (<=> variables-scm 
	      (<call> gp_copy_vector (<addr> variables) nvar))
	 (<=> session (CONS variables-scm cnst))
	 (<=> middle  (CONS (<scm> '()) session))
	 (<=> pinned? (<scm> #f)))))
(define-syntax-rule (EQ x y)      (<call> scm_is_eq x y))
(define-syntax-rule (PRUNE s)     (<call> gp_gp_prune s))
(define-syntax-rule (PRUNE-TAIL s)  (<call> gp_gp_prune_tail s))
(define-syntax-rule (GPPAIR? x s) (TRUE (<call> gp_pair    x s)))
(define-syntax-rule (GPCAR x s)   (LOOKUP (<call> gp_car    x s) s))
(define-syntax-rule (GPCDR x s)   (LOOKUP (<call> gp_gp_cdr x s) s))
(define-syntax-rule (GGPAIR? x)   (<call> consp x))
(define-syntax-rule (GGPCAR x)    (<call> car   x))
(define-syntax-rule (GGPCDR x)    (<call> cdr   x))
(define-syntax-rule (GPCONS x y s) (<call> gp_cons_simple x y s))
(define-syntax-rule (1- x) (int->scm (<-> (scm->int x) (<c> 1))))
(define-syntax-rule (GET-TOKEN)   (<call> gp_get_state_token))
(define-syntax-rule (SET x y s)   (<call> gp_set       x y s))
(define-syntax-rule (CAR   x)     (<call> SCM_CAR      x))
(define-syntax-rule (CDR   x)     (<call> SCM_CDR      x))
(define-syntax-rule (TRUE  x)     (<call> scm_is_true  x))
(define-syntax-rule (FALSE x)     (<call> scm_is_false x))
(define-syntax-rule (CONS  x y)   (<call> scm_cons x y))
(define-syntax-rule (PAIR? x)     (<call> SCM_CONSP x))
(define-syntax-rule (NUMBER? s)   (<call> SCM_I_INUMP s))
(define-syntax-rule (MKVAR s)     (<call> gp_mkvar s))
(define-syntax-rule (LOOKUP x s)  (<call> lookup x s))
(define-syntax-rule (VARIABLE? x) (<call> SCM_VARIABLEP x))
(define-syntax-rule (MAKE-VARIABLE x)
  (<call> scm_make_variable x))
(define-syntax-rule (VARIABLE-REF x) 
  (<call> scm_variable_ref x))
(define-syntax-rule (LENGTH st)
  (<recur> lp ((s st) (int i (<c> 0)))
     (if (PAIR? s)
	 (<next> lp (CDR s) (<+> i (<c> 1)))
	 i)))

(define-syntax CALL
  (syntax-rules ()
    ((_ f)
     (<call> scm_call_0 f))
    ((_ f x)
     (<call> scm_call_1 f x))
    ((_ f x y)
     (<call> scm_call_2 f x y))
    ((_ f x y z)
     (<call> scm_call_3 f x y z))))

(define-syntax ERROR
  (syntax-rules ()
    ((_ str) 
     (<icall> 'scm_misc_error 
	      (<c> "prolog-vm")
	      (<c> str)
	      (<scm> '())))
    ((_ str x) 
     (<icall> 'scm_misc_error 
	      (<c> "prolog-vm")
	      (<c> str)
	      (<icall> 'scm_list_1 x)))
    ((_ str x y) 
     (<icall> 'scm_misc_error 
	      (<c> "prolog-vm")
	      (<c> str)
	      (<icall> 'scm_list_2 x y)))))

(define-syntax VECTOR
  (syntax-rules ()
    ((_) (<call> scm_c_make_vector (<c> 0) (<scm> #f)))
    ((_ x) 
     (<let*> ((v        (<call> scm_c_make_vector (<c> 1) (<scm> #f)))
	      ((SCM *)  vp (scm->vector v)))
       (<=> (<ref> vp (<c> 0)) x)
       v))
    ((_ x y)  
     (<let*> ((v        (<call> scm_c_make_vector (<c> 2) (<scm> #f)))
	      ((SCM *)  vp (scm->vector v)))
       (<=> (<ref> vp (<c> 0)) x)
       (<=> (<ref> vp (<c> 1)) y)
       v))
    ((_ x y z)  
     (<let*> ((v        (<call> scm_c_make_vector (<c> 3) (<scm> #f)))
	      ((SCM *)  vp (scm->vector v)))
       (<=> (<ref> vp (<c> 0)) x)
       (<=> (<ref> vp (<c> 1)) y)
       (<=> (<ref> vp (<c> 2)) z)
       v))
    ((_ x y z w)  
     (<let*> ((v        (<call> scm_c_make_vector (<c> 4) (<scm> #f)))
	      ((SCM *)  vp (scm->vector v)))
       (<=> (<ref> vp (<c> 0)) x)
       (<=> (<ref> vp (<c> 1)) y)
       (<=> (<ref> vp (<c> 2)) z)
       (<=> (<ref> vp (<c> 3)) w)
       v))
    ((_ x y z w v)  
     (<let*> ((vv (<call> scm_c_make_vector (<c> 5) (<scm> #f)))
	      ((SCM *)  vp (scm->vector vv)))
       (<=> (<ref> vp (<c> 0)) x)
       (<=> (<ref> vp (<c> 1)) y)
       (<=> (<ref> vp (<c> 2)) z)
       (<=> (<ref> vp (<c> 3)) w)
       (<=> (<ref> vp (<c> 4)) v)
       vv))))

(define-syntax-rule (PRINTF a ...)
  (DB (<icall> 'printf (<c> a) ...)))

(define-syntax FORMAT 
  (syntax-rules ()
   ((_ a)    (DB (<icall> 'scm_simple_format
                 (<scm> #t)
		 (<scm> a)
		 (<scm> '()))))

   ((_ a b)
    (DB (<icall> 'scm_simple_format
                 (<scm> #t)
		 (<scm> a)
		 (<icall> 'scm_list_1 b))))
   ((_ a b c)
    (DB (<icall> 'scm_simple_format
                 (<scm> #t)
		 (<scm> a)
		 (<icall> 'scm_list_2 b c ))))
   ((_ a b c d)
    (DB (<icall> 'scm_simple_format
                 (<scm> #t)
		 (<scm> a)
		 (<icall> 'scm_list_3 b c d))))

   ((_ a b c d e)
    (DB (<icall> 'scm_simple_format
                 (<scm> #t)
		 (<scm> a)
		 (<icall> 'scm_list_4 b c d e))))

   ((_ a b c d e f)
    (DB (<icall> 'scm_simple_format
                 (<scm> #t)
		 (<scm> a)
		 (<icall> 'scm_list_5 b c d e f))))))

(define-syntax TOUCH
  (lambda (x)
    (syntax-case x ()
      ((_ s n)
       (begin
	 (let* ((x (syntax->datum #'s))
		(r (hash-ref touch x #f)))
	   (if r
	       (hash-set! touch x (logior r (syntax->datum #'n)))
	       (hash-set! touch x (syntax->datum #'n))))
	 #'(if #f #f))))))

(define-syntax-rule (PRSTACK sp fp)
  (DB (<icall> 'printf (<c> "sp - fp . %d\n") (<-> fp sp))))

(define-syntax LABEL
  (lambda (x) 
    (syntax-case x ()
      ((_ s)
       (if debug
	   #'(begin
	       (TOUCH s 1)
	       (<begin>
		(<label> s)
		(PRINTF "%s : %d\n"
			(symbol->string 's) (hash-ref *map* 's 0))))
	   #'(begin
	       (TOUCH s 1)
	       (<label> s)))))))

#;
(define-syntax-rule (LABEL s)
  (begin
    (TOUCH s 1)
    (<label> s)))

(define-syntax-rule (DISTANCE-FP-XP fp sp)
  (<if> (q> fp sp) 
	(<-> fp sp)
	(<begin> 
	 (ERROR "negative stack pointer")
	 (<c> 1))))

(define-syntax-rule (NARG fp sp) (DISTANCE-FP-XP fp sp))
(define-syntax-rule (IS-NOT-FAIL fp sp) (q> (NARG fp sp) (<c> 1)))
(define-syntax-rule (SET-S-P p? s p sp fp)
  (<if> (IS-NOT-FAIL fp sp)	
	(<begin>
	 (<=> p? (<c> 0))
	 (<=> s (FARG 1 fp))
	 (<=> p (FARG 2 fp)))
	(<=> p? (<c> 1))))
    
(define-syntax-rule (scm->vector x)
  (<call> SCM_I_VECTOR_WELTS x))

(define-syntax-rule (scm->ulong val)
  (<call> scm2ulong val))

(define-syntax-rule (scm->int val)
  (<call> scm2int val))

(define-syntax-rule (int->scm val)
  (<call> scm_from_int val))

(define-syntax DECR
  (lambda (x)
    (syntax-case x ()
      ((DECR n sp)
       (if (number? (syntax->datum #'n))
	   #'(<=> sp (<+> sp (<c> n)))
	   #'(<=> sp (<+> sp  n)))))))

(define-syntax INCR
  (lambda (x)
    (syntax-case x ()
      ((INCR n sp)
       (if (number? (syntax->datum #'n))
	   #'(<=> sp (<-> sp (<c> n)))
	   #'(<=> sp (<-> sp  n)))))))

(define-syntax-rule (ARG n sp)  (FARG n sp))
(define-syntax-rule (PARG n sp) (PFARG n sp))
(define-syntax-rule (SVAR-REF fp nstack i)
   (<ref> fp (<-> (<+> nstack i))))

(define-syntax-rule (UNPACK-VAR 
		     var? x i pinned? variables variables-scm nvar
		     cnst session middle
		     code1 code2)
   (<if> (PAIR? x)
	 (<let> ((int i (scm->int (CAR x))))
	    code1)	 
         (<let> ((int i (scm->int x)))
	     (<if> (<c> var?)
		   (MAYBE-ADJUST-VECTOR 
		    pinned? variables variables-scm nvar
		    cnst session middle))
	     code2)))

(define-syntax UNPACK-VAR-A 
  (syntax-rules ()
    ((UNPACK-VAR-A a
		   var? pinned? variables variables-scm nvar
		   cnst session middle
		   code1 code2)
     (<if> a
	   code1
	   (<begin>
	    (<if> (<c> var?)
		  (MAYBE-ADJUST-VECTOR 
		   pinned? variables variables-scm nvar
		   cnst session middle))
	    code2)))))

(define-syntax CLEAR 
  (syntax-rules () 
    ((_ 1 sp)
     (<begin>
      (<=> (ARG -1 sp) (<scm> #f))
      (DECR 1 sp)))
    ((_ 2 sp)
     (<begin>
      (<=> (ARG -1 sp) (<scm> #f))
      (<=> (ARG -2 sp) (<scm> #f))
      (DECR 2 sp)))))


(define-syntax FARG
  (lambda (x)
    (syntax-case x ()
      ((FARG n fp)
       (if (number? (syntax->datum #'n))
	   #'(<ref> fp (<c> (- n)))
	   #'(<ref> fp (<-> n)))))))

(define-syntax PFARG
  (lambda (x)
    (syntax-case x ()
      ((PFARG n fp)
       (if (number? (syntax->datum #'n))
	   #'(<+> fp (<c> (- n)))
	   #'(<+> fp (<-> n)))))))

(define-syntax-rule (GET-CC vars)
  (<ref> vars (<c> 0)))
(define-syntax-rule (GET-P  vars)
  (<ref> vars (<c> 1)))
(define-syntax-rule (GET-CUT vars)
  (<ref> vars (<c> 2)))
(define-syntax-rule (GET-PINNED vars)
  (<ref> vars (<c> 3)))

(define-syntax-rule (SET-CC ninst varvec fp)
  (<if> (<==> ninst (<c> 0))
	(<begin>
	 (<=> (GET-CC  varvec) (FARG 3 fp))
	 (<=> (GET-P   varvec) (FARG 2 fp))
	 (<=> (GET-CUT varvec) (FARG 2 fp)))))

(define-syntax-rule (GET-RECIEVE-FP fp variables)
  (<+> fp (scm->int (<ref> vars (<c> 1)))))

(define-syntax-rule (GET-RECIEVE-INSTR sp)
  (<let> ((int ret (scm->int (ARG 2 sp))))
    (<=> (ARG 2 sp) (ARG -1 sp))
    (DECR 1 sp)
    ret))

(define-syntax-rule (SET-STACK stack sp)
  (<recur> lp ((stack stack))
    (<if> (CONS stack)
	  (<begin>
	   (<=> (ARG 0 sp) (CAR stack))
	   (INCR 1 sp)
	   (<next> lp (CDR stack))))))

(define-syntax-rule (GET-STACK fp sp)
  (<recur> lp ((l (<scm> '())))
     (<if> (<==> fp sp)
	   l
	   (<begin>
	    (DECR 1 sp)
	    (<let> ((r (<*> sp)))
	       (<=> (<*> sp) (<scm> #f))
	       (<next> lp (CONS r l)))))))

(define-syntax-rule (LINK-SVARS fp nstack nsloc st)
  (<let> (((SCM *) vp (PFARG (<+> nstack nsloc) fp)))
     (<recur> lp ((int i nsloc))
	(<if> (q> i (<c> 0))
	      (<begin>
	       (<=> st (CONS (ARG -1 vp) st))
	       (<--> vp)
	       (<next> lp (<-> i (<c> 1))))))))

(define-syntax-rule (ADD-STACK-N st fp n ns)
  (<let> (((SCM *) vp (PFARG ns fp)))
    (<recur> lp ((int i n))
     (<if> (q> i (<c> 0))
	   (<if> (PAIR? st)
		 (<begin>
		  (<=> (ARG 0 vp) (CAR st))
		  (<=> st (CDR st))
		  (INCR 1 vp)
		  (<next> lp (<-> i (<c> 1))))
		 (ERROR "Stack link on length missmatch"))))))

(define-syntax-rule (MOVE-HEAD fp xp nstack)
  (<let> ((int n (DISTANCE-FP-XP fp xp)))
    (<if> n
      (<recur> lp ((int i (<+> nstack n)))
        (<if> (<not> (<==> fp xp))
	      (<begin>
	       (<=> (ARG i fp) (ARG -1 xp))
	       (<--> xp)
	       (<next> lp (<-> i (<c> 1)))))))))

(define-syntax-rule (INSTALL-STACK fp sp nstack nsloc sp-stack)
  (<let> ((st sp-stack))
     (ADD-STACK-N st fp nsloc nstack)
     (<let> ((int n (LENGTH st)))
       (MOVE-HEAD fp sp nstack)
       (ADD-STACK-N st fp n (<c> 0))
       (<=> sp (<+> sp nstack)))))
	   
   

(define-syntax-rule (CALL-P p fp sp)
  (<begin>
   (<=> (FARG 0 fp) p)
   (<=> sp (PFARG 1 fp))
   (<goto> ret)))

(define-syntax-rule (BACKTRACK p instructions inst-pt fp sp)
  (<if> (NUMBER? p)
	(<begin>
	 (<=> inst-pt (<+> instructions (scm->int p)))
	 (NEXT inst-pt))
	(CALL-P p fp sp)))

(define-syntax-rule (handle-kc lookup z k 
			       s variables p instructions inst-pt fp sp)
  (<if> (<bit-and> k (<c> 1))
	(<let> ((s0 (<call> gp_gp_unify_raw 
			    lookup z s)))
	       (<if> (TRUE s0)
		     (<=> s s0)
		     (BACKTRACK 
		      p instructions inst-pt fp sp)))
	(<=> lookup z)))


(define-syntax-rule (mk-scm-ss-binop-s s op (fkn ...) inst-pt sp fp nstack)
  (<begin>
   (LABEL op)
   (PRSTACK sp fp)
   (<let*> ((ulong v  (scm->ulong (<*> inst-pt)))
	    (int   v1 (<bit-and> v (<c> #xffff)))
	    (int   v2 (q>> (<bit-and> v (<c> #xffff0000)) (<c> 16)))
	    (int   v3 (q>> v (<c> 32)))
	    (SCM   x  (LOOKUP (SVAR-REF fp nstack v1) s))
	    (SCM   y  (LOOKUP (SVAR-REF fp nstack v2) s)))
     (<=> inst-pt (<+> inst-pt (<c> 1)))
     (<=> (SVAR-REF fp nstack v3) (fkn ... x y))
     (NEXT inst-pt))))

(define-syntax-rule (mk-scm-xx-binop-x s op (fkn ...) inst-pt sp fp nstack 
				       pinned? variables variables-scm nvar
				       cnst session middle
				       p instructions)
  (<begin>
   (LABEL op)
   (PRSTACK sp fp)
   (<let*> ((ulong v  (scm->ulong (<*> inst-pt)))
	    (int   v1 (<bit-and> v (<c> #xffff)))
	    (int   v2 (q>> (<bit-and> v (<c> #xffff0000)) (<c> 16)))
	    (int   v3 (q>> (<bit-and> v (<c> #xffff00000000))
			   v (<c> 32)))
	    (int   q  (q>> v (<c> 48)))
	    (int   k  (q>> q (<c> 3)))
	    (SCM   x  (LOOKUP 
		       (<if> (<bit-and> q (<c> 1))
			     (SVAR-REF fp nstack v1)
			     (<ref> variables    v1)) s))
	    (SCM   y  (LOOKUP 
		       (<if> (<bit-and> q (<c> 2))
			     (SVAR-REF fp nstack v2)
			     (<ref> variables    v2)) s))
	    (SCM  z   (fkn ... x y)))

     (<=> inst-pt (<+> inst-pt (<c> 1)))
     (UNPACK-VAR-A (<bit-and> q (<c> 4))
		   1 pinned? variables variables-scm nvar
		   cnst session middle
       (handle-kc (SVAR-REF fp nstack v3) z k 
		  s variables p instructions inst-pt fp sp)
       (handle-kc (<ref> variables v3) z k 
		  s variables p instructions inst-pt fp sp))
     (NEXT inst-pt))))



(define-syntax-rule (mk-scm-binop s op (fkn ...) inst-pt sp fp)
  (<begin>
   (LABEL op)
   (PRSTACK sp fp)
   (<let> ((SCM x (<call> gp_gp_lookup (ARG -2 sp) s))
	   (SCM y (<call> gp_gp_lookup (ARG -1 sp) s)))
     (CLEAR 1 sp)
     (<=> (ARG -1 sp) (fkn ... x y))
     (NEXT inst-pt))))

(define-syntax-rule (mk-scm-binop-l s op (fkn ...) inst-pt sp fp)
  (<begin>
   (LABEL op)
   (PRSTACK sp fp)
   (<let> ((SCM x (<*> inst-pt))
	   (SCM y (<call> gp_gp_lookup (ARG -1 sp) s)))
     (<=> (ARG -1 sp) (fkn ... x y))
     (<=> inst-pt (<+> inst-pt (<c> 1)))
     (NEXT inst-pt))))

(define-syntax-rule (mk-scm-binop-r s op (fkn ...) inst-pt sp fp)
  (<begin>
   (LABEL op)
   (PRSTACK sp fp)
   (<let> ((SCM x (<*> inst-pt))
	   (SCM y (<call> gp_gp_lookup (ARG -1 sp) s)))
     (<=> (ARG -1 sp) (fkn ... y x))
     (<=> inst-pt (<+> inst-pt (<c> 1)))
     (NEXT inst-pt))))

(define-syntax-rule (mk-scm-unop-minus s op inst-pt sp fp)
  (<begin>
   (LABEL op)
   (PRSTACK sp fp)
   (<let> ((SCM x (<call> gp_gp_lookup (ARG -1 sp) s)))
     (<=> (ARG -1 sp) (<call> scm_difference (<scm> 0) x))
     (NEXT inst-pt))))

(define-syntax-rule (mk-scm-unop s op (fkn ...) inst-pt sp fp)
  (<begin>
   (LABEL op)
   (PRSTACK sp fp)
   (<let> ((SCM x (<call> gp_gp_lookup (ARG -1 sp) s)))
     (<=> (ARG -1 sp) (fkn ... x))
     (NEXT inst-pt))))

(define-syntax-rule (mk-scm-test-2 op fkn s p instructions inst-pt sp fp)
  (<begin>
   (LABEL op)
   (PRSTACK sp fp)
   (<let*> ((x  (ARG -2 sp))
	    (y  (ARG -1 sp))
	    (ss (<call> fkn s x y)))
     (CLEAR 2 sp)
     (<if> ss
	   (NEXT inst-pt)
	   (BACKTRACK p instructions inst-pt fp sp)))))


(define-syntax-rule (mk-i-c-0 mk-scm-i constants i q)
(define-syntax-rule (mk-scm-i op s p variables nstack constants
			      instructions vars inst-pt sp fp)
  (<begin>
   (LABEL op)
   (PRSTACK sp fp)
   (<let*> ((SCM i  (<ref> inst-pt (<c> 0)))
            (SCM m  (<ref> inst-pt (<c> 1))))
     (<=> inst-pt (<+> inst-pt (<c> 2)))
     (<let*> ((SCM x  (ARG -1 sp))
	      (SCM y  q)
	      (SCM ss (<if> (<==> m (<scm> #f))
			    (<call> gp_m_unify      x y s)
			    (<call> gp_gp_unify_raw x y s))))
	(CLEAR 1 sp)
	(<if> (<call> scm_is_true ss)
	      (<begin>
	       (<=> s ss)
	       (NEXT inst-pt))
	      (BACKTRACK p instructions inst-pt fp sp)))))))

(mk-i-c-0 mk-scm-i constants i i)
(mk-i-c-0 mk-scm-c constants i (<ref> constants (scm->int i)))

(define-syntax-rule (mk-eql-i-c mk-scm-equal constants i q)
(define-syntax-rule (mk-scm-equal op s p constants 
				  instructions vars inst-pt sp fp)
  (<begin>
   (LABEL op)
   (PRSTACK sp fp)
   (<let*> ((SCM i  (<ref> inst-pt (<c> 0)))
	    (SCM x  q)
	    (SCM y  (ARG -1 sp)))
     (CLEAR 1 sp)
     (<if> (<call> scm_is_true (<call> scm_equal_p x y))
	   (NEXT inst-pt)
	   (BACKTRACK p instructions inst-pt fp sp))))))

(mk-eql-i-c mk-equal-i constants i i)
(mk-eql-i-c mk-equal-c constants i (<ref> constants (scm->int i)))

(define-syntax-rule (mk-scm-unify op s p variables variables-scm
				  nvar pinned? cnst session middle nstack 
				  instructions vars inst-pt sp fp)
  (<begin>
   (LABEL op)
   (PRSTACK sp fp)
   (<let*> ((ulong code  (scm->ulong (<ref> inst-pt (<c> 0))))
            (int   m     (<bit-and> code (<c> 3)))
	    (int   a     (q>> (<bit-and> code (<c> 4))   (<c> 2)))
	    (int   k     (q>> (<bit-and> code (<c> 24))  (<c> 3)))
	    (int   v     (q>> code                       (<c> 5))))
     (<=> inst-pt (<+> inst-pt (<c> 1)))
     (<let*> ((SCM x  (UNPACK-VAR-A a 
				    0 pinned? variables variables-scm nvar
				    cnst session middle
				    (SVAR-REF fp nstack v)
				    (<ref> variables v)))
	      (SCM y  (ARG -1 sp))
	      (SCM ss (<if> (<==> m (<c> 2))
			    (<call> gp_m_unify x y s)
			    (<if> (<or>
				   (<==> m (<c> 3))
				   (<==> k (<c> 3)))
				  (<call> gp_gp_unify_raw x y s)
				  (<call> gp_gp_unify     x y s)))))
      (CLEAR 1 sp)
      (<if> (<call> scm_is_true ss)
	    (<begin>
	     (<=> s ss)
	     (NEXT inst-pt))
	    (BACKTRACK p instructions inst-pt fp sp))))))

(define-syntax-rule (mk-scm-move op q s p variables variables-scm
				  nvar pinned? cnst session middle nstack 
				  instructions vars inst-pt sp fp)
  (<begin>
   (LABEL op)
   (PRSTACK sp fp)
   (<let*> ((ulong v  (scm->ulong (<ref> inst-pt (<c> 0)))))
     (<=> inst-pt (<+> inst-pt (<c> 1)))
     (UNPACK-VAR-A q 1 pinned? variables variables-scm nvar
		   cnst session middle
		   (<=> (SVAR-REF fp nstack v) (ARG -1 sp))
		   (<=> (<ref> variables    v) (ARG -1 sp)))
     (CLEAR 1 sp)
     (NEXT inst-pt))))

(define-syntax-rule (mk-scm-move-op op q1 q2 q3 binop schmop 
				    s p variables variables-scm
				    nvar pinned? cnst session middle nstack 
				    instructions vars inst-pt sp fp)
  (<begin>
   (LABEL op)
   (PRSTACK sp fp)
   (<let*> ((ulong v  (scm->ulong (<ref> inst-pt (<c> 0))))
	    (int   v1 (<bit-and> v (<c> #xffff)))
	    (int   v2 (q>> (<bit-and> v (<c> #xffff0000))     (<c> 16)))
	    (int   v3 (q>> (<bit-and> v (<c> #xffff00000000)) (<c> 32))))
     (<=> inst-pt (<+> inst-pt (<c> 1)))
     (<let*> ((SCM x (UNPACK-VAR-A q1 0 pinned? variables variables-scm nvar
				   cnst session middle
				   (SVAR-REF fp nstack v1)
				   (<ref> variables    v1)))
	      (SCM y (UNPACK-VAR-A q2 0 pinned? variables variables-scm nvar
				   cnst session middle
				   (SVAR-REF fp nstack v2)
				   (<ref> variables    v2)))
	      (SCM z (ARITH binop schmop x y)))

     (UNPACK-VAR-A q3 1 pinned? variables variables-scm nvar
		   cnst session middle
		   (<=> (SVAR-REF fp nstack v3) z)
		   (<=> (<ref> variables    v3) z))
     (NEXT inst-pt)))))

(define-syntax-rule (mk-scm-move-i-op op q1 q2 binop schmop 
				    s p variables variables-scm
				    nvar pinned? cnst session middle nstack 
				    instructions vars inst-pt sp fp)
  (<begin>
   (LABEL op)
   (PRSTACK sp fp)
   (<let*> ((SCM   x  (<ref> inst-pt (<c> 0)))
	    (ulong v  (scm->ulong (<ref> inst-pt (<c> 1))))
	    (int   v1 (<bit-and> v (<c> #xffff)))
	    (int   v2 (q>> (<bit-and> v (<c> #xffff0000)) (<c> 16))))
     (<=> inst-pt (<+> inst-pt (<c> 2)))
     (<let*> ((SCM y (UNPACK-VAR-A q1 0 pinned? variables variables-scm nvar
				   cnst session middle
				   (SVAR-REF fp nstack v1)
				   (<ref> variables    v1)))
	      (SCM z (ARITH binop schmop x y)))

     (UNPACK-VAR-A q2 1 pinned? variables variables-scm nvar
		   cnst session middle
		   (<=> (SVAR-REF fp nstack v2) z)
		   (<=> (<ref> variables    v2) z))
     (NEXT inst-pt)))))


(define-syntax-rule (mk-scm-move-i-op-x op binop schmop 
				    s p variables variables-scm
				    nvar pinned? cnst session middle nstack 
				    instructions vars inst-pt sp fp)
  (<begin>
   (LABEL op)
   (PRSTACK sp fp)
   (<let*> ((SCM   x  (<ref> inst-pt (<c> 0)))
	    (ulong v  (scm->ulong (<ref> inst-pt (<c> 1))))
	    (int   v1 (<bit-and> v (<c> #xffff)))
	    (int   v2 (q>> (<bit-and> v (<c> #xffff0000)) (<c> 16)))
	    (int   q  (q>> v (<c> 32)))
	    (int   k  (q>> v (<c> 34))))
     (<=> inst-pt (<+> inst-pt (<c> 2)))
     (<let*> ((SCM y (UNPACK-VAR-A (<bit-and> q (<c> 1))
				   0 pinned? variables variables-scm nvar
				   cnst session middle
				   (LOOKUP (SVAR-REF fp nstack v1) s)
				   (LOOKUP (<ref> variables    v1) s)))
	      (SCM z (ARITH binop schmop x y)))

       (UNPACK-VAR-A (<bit-and> q (<c> 2))
		   1 pinned? variables variables-scm nvar
		   cnst session middle
		   (handle-kc (SVAR-REF fp nstack v2) z k 
			      s variables p instructions inst-pt fp sp)
		   (handle-kc (<ref> variables v2) z k 
			      s variables p instructions inst-pt fp sp))
       (NEXT inst-pt)))))

(define-syntax-rule (mk-scm-xmove-op op binop schmop 
				    s p variables variables-scm
				    nvar pinned? cnst session middle nstack 
				    instructions vars inst-pt sp fp)
  (<begin>
   (LABEL op)
   (PRSTACK sp fp)
   (<let*> ((ulong v  (scm->ulong (<ref> inst-pt (<c> 0))))
	    (int   v1 (<bit-and> v (<c> #xffff)))
	    (int   v2 (q>> (<bit-and> v (<c> #xffff0000))     (<c> 16)))
	    (int   v3 (q>> (<bit-and> v (<c> #xffff00000000)) (<c> 32)))
	    (int   q  (q>> v (<c> 48))))
     (<=> inst-pt (<+> inst-pt (<c> 1)))
     (<let*> ((SCM x (UNPACK-VAR-A (<bit-and> q (<c> 1)) 
				   0 pinned? variables variables-scm nvar
				   cnst session middle
				   (SVAR-REF fp nstack v1)
				   (<ref> variables    v1)))
	      (SCM y (UNPACK-VAR-A (<bit-and> q (<c> 2)) 
				   0 pinned? variables variables-scm nvar
				   cnst session middle
				   (SVAR-REF fp nstack v2)
				   (<ref> variables    v2)))
	      (SCM z (ARITH binop schmop x y)))

     (UNPACK-VAR-A (<bit-and> q (<c> 4)) 
		   1 pinned? variables variables-scm nvar
		   cnst session middle
		   (<=> (SVAR-REF fp nstack v3) z)
		   (<=> (<ref> variables    v3) z))
     (NEXT inst-pt)))))
 
(define-syntax-rule (mk-i-c mk-scm-unify-i constants i q)
(define-syntax-rule (mk-scm-unify-i op s p variables variables-scm
				    nvar pinned? cnst session middle
				    nstack constants 
				    instructions vars inst-pt sp fp)
  (<begin>
   (LABEL op)
   (PRSTACK sp fp)
   (<let*> ((SCM n            (<ref> inst-pt (<c> 2)))
            (SCM m            (<ref> inst-pt (<c> 0)))
	    (SCM i            (<ref> inst-pt (<c> 1)))
            (SCM k            (<ref> inst-pt (<c> 3))))
     (<=> inst-pt (<+> inst-pt (<c> 4)))
     (<if> (<==> k (<scm> #f))
	   (<begin>
	    (UNPACK-VAR 1 n ii pinned? variables variables-scm nvar
			cnst session middle
	      (<=> (SVAR-REF fp nstack ii) q)
	      (<=> (<ref> variables ii)    q)) 	      
	    (NEXT inst-pt))
	   (<let*> ((SCM x  (UNPACK-VAR 
			     0 n ii pinned? variables variables-scm nvar
			     cnst session middle
			      (SVAR-REF fp nstack ii)
			      (<ref> variables ii)))
		    (SCM y  q)
		    (SCM ss (<if> (<==> m (<scm> #f))
				  (<call> gp_m_unify       x y s)
				  (<if> (<or>
					 (<==> m (<scm> #t))
					 (<==> k (<scm> #t)))
					(<call> gp_gp_unify_raw x y s)
					(<call> gp_gp_unify     x y s)))))
	     (<if> (<call> scm_is_true ss)
		   (<begin>
		    (<=> s ss)
		    (NEXT inst-pt))

		   (BACKTRACK p instructions inst-pt fp sp))))))))

(mk-i-c mk-scm-unify-i constants i i)
(mk-i-c mk-scm-unify-c constants i (<ref> constants (scm->int i)))

(define-syntax-rule (mk-scm-unify-2 op s p variables variables-scm
				    nvar pinned? cnst session middle nstack
				    instructions vars inst-pt sp fp)
  (<begin>
   (LABEL op)
   (PRSTACK sp fp)
   (<let*> ((ulong code1 (scm->ulong (<ref> inst-pt (<c> 0))))
	    (int   m     (<bit-and> code1 (<c> 3)))
	    (ulong code2 (q>>  code1 (<c> 2)))
	    (int   a1    (<bit-and> code2 (<c> 1)))
	    (int   k1    (q>> (<bit-and> code2 (<c> 6))        (<c> 1)))
	    (int   v1    (q>> (<bit-and> code2 (<c> #xfffff8)) (<c> 3)))
	    (ulong code3 (q>> code2  (<c> 24)))
	    (int   a2   (<bit-and> code3 (<c> 1)))
	    (int   k2   (q>> (<bit-and> code3 (<c> 6))        (<c> 1)))
	    (int   v2   (q>> (<bit-and> code3 (<c> #xfffff8)) (<c> 3))))
     (<=> inst-pt (<+> inst-pt (<c> 1)))
     (<if> (<==> k1 (<c> 2))
           (<begin>
             (<if> (<==> k2 (<c> 2))
		   (UNPACK-VAR-A a2
		    1 pinned? variables variables-scm nvar
		    cnst session middle
		    (<=> (SVAR-REF fp nstack v2) (MKVAR s))
		    (<=> (<ref> variables v2)    (MKVAR s)))) 
	     (<let> ((rhs (UNPACK-VAR-A a2
			   0 pinned? variables variables-scm nvar
			   cnst session middle
			    (SVAR-REF fp nstack v2)
			    (<ref> variables v2)))) 
		(UNPACK-VAR-A a1 1 pinned? variables variables-scm nvar
			    cnst session middle
	          (<=> (SVAR-REF fp nstack v1) rhs)
		  (<=> (<ref> variables v1)    rhs)))
	     (NEXT inst-pt))
	   (<if> (<==> k2 (<c> 2))
		 (<let> ((rhs (UNPACK-VAR-A a2 
			       0 pinned? variables variables-scm nvar
			       cnst session middle
			        (SVAR-REF fp nstack v2)
				(<ref> variables v2)))) 
		   (UNPACK-VAR-A a1 1 pinned? variables variables-scm nvar
			       cnst session middle
		     (<=> (SVAR-REF fp nstack v1) rhs)
		     (<=> (<ref> variables v1)    rhs))
		   (NEXT inst-pt))

		 (<let*> ((SCM x  (UNPACK-VAR-A a1 
				   0 pinned? variables 
				   variables-scm nvar
				   cnst session middle
		                    (SVAR-REF fp nstack v1)
		                    (<ref> variables v1)))

			  (SCM y  (UNPACK-VAR-A a2 
				   0 pinned? variables 
				   variables-scm nvar
				   cnst session middle
		                    (SVAR-REF fp nstack v2)
		                    (<ref> variables v2)))

			  (SCM ss 
			       (<begin>
				(FORMAT "~a = ~a~%" x y)
				(<if> (<==> m  (<c> 2))
					(<call> gp_m_unify x y s)
					(<if> (<or>
					       (<==> m  (<c> 3))
					       (<==> k1 (<c> 3))
					       (<==> k2 (<c> 3)))
					      (<call>
					       gp_gp_unify_raw x y s)
					      (<call> 
					       gp_gp_unify     x y s))))))
		   (<if> (<call> scm_is_true ss)
			 (<begin>
			  (<=> s ss)
			  (NEXT inst-pt))
			 (BACKTRACK p instructions inst-pt fp sp))))))))


(define-syntax-rule (STORE-STATE tp tag s fr p stack)
  (GPCONS (GPCONS tag 
		  (GPCONS p
			  (<let> ((sfr (GPCONS s fr s)))
			    (<if> tp
				  (GPCONS 
				   sfr
				   (<call> scm_fluid_ref *delayers*) s)
				  (GPCONS 
				   sfr (<scm> #f)                    s)))
			  s)
		  s)
	  stack s))

(define-syntax-rule (STORE-STATE-NEG tp tag s fr scut p stack)
  (GPCONS (GPCONS tag 
		  (GPCONS p
			  (<let> ((sfr (GPCONS s (GPCONS fr scut s) s)))
			    (<if> tp
				  (GPCONS 
				   sfr 
				   (<call> scm_fluid_ref *delayers*) s)
				  (GPCONS sfr (<scm> #f) s)))
			  s)
		  s)
	  stack s))

(define-syntax-rule (STORE-STATE-SOFT s tag p fp nstack)
  (<let> ((int v (scm->int tag)))
	 (<=> (SVAR-REF fp nstack v) p)))

	
(define-syntax-rule (CLEAR-SP-XP sp xp)
  (<recur> lp ()
    (<if> (q> sp xp)
	  (<begin> 
	   (PRINTF 
	    (<c> "wrong stack state sp - xp = %d, will equalize\n")
	    (<-> xp sp))
	   (<=> sp xp))
	  (<if> (q< sp xp)
		(<begin>
		 (DECR 1 sp)
		 (<=> (<*> sp) (<scm> #f))
		 (<next> lp))))))

(define-syntax-rule (RESTORE-STATE s tag stack)
  (<recur> lp ()
     (<let> ((x (GGPCAR stack)))
	(<if> (EQ (GGPCAR x) tag)
	      (<let*> ((x1  (GGPCDR x))
		       (x2  (GGPCDR x1))
		       (x3  (GGPCDR x2))
		       (ss  (GGPCDR (GGPCAR x2))))
	         (<if> (TRUE x3)
		       (<begin>
			(<call> scm_fluid_set_x *delayers* x3)
			(<=> s ss)
			(<c> 5))
		       (<begin>
			(<=> s ss)
			(<c> 5))))
	      (<begin>
	       (<=> stack (GPCDR stack s))
	       (<next> lp))))))

(define-syntax-rule (RESTORE-STATE-TAIL s p tag stack)
  (<recur> lp ()
     (<let> ((x (GGPCAR stack)))
       (<if> (EQ (GGPCAR x) tag)
	     (<let*> ((x1  (GGPCDR x))
		      (x2  (GGPCDR x1))
		      (x3  (GGPCDR x2)))
	       (<=> p     (GGPCAR x1))
	       (<=> stack (GGPCDR stack))
	       (<let*> ((sx (GGPCAR x2))
			(fr (GGPCDR sx))
			(ss (GGPCAR sx)))
	          (<if> (TRUE x3)
			(<call> scm_fluid_set_x *delayers* x3))
		  (<=> s ss)
		  fr))
	     (<begin>
	      (<=> stack (GGPCDR stack))
	      (<next> lp))))))

(define-syntax-rule (RESTORE-STATE-TAIL-P p tag stack)
  (<recur> lp ()
     (<let> ((x (GGPCAR stack))) 
       (FORMAT "RESTORE-P x : ~a tag : ~a~%" x tag)
       (<if> (EQ (GGPCAR x) tag)
	     (<let*> ((x1  (GGPCDR x)))
	       (<=> p     (GGPCAR x1))
	       (<=> stack (GGPCDR stack))
	       (PRUNE-TAIL (GGPCDR (GGPCAR (GGPCDR x1)))))
	     (<begin>
	      (<=> stack (GGPCDR stack))
	      (<next> lp))))))


(define-syntax-rule (RESTORE-STATE-TAIL-NEG s scut p tag stack)
  (<recur> lp ()
     (<let> ((x (GGPCAR stack)))
       (<if> (EQ (GGPCAR x) tag)
	     (<let*> ((x1  (GGPCDR x))
		      (x2  (GGPCDR x1))
		      (x3  (GGPCDR x2))
		      (s0  (GGPCAR x2))
		      (x4  (GGPCDR x2))
		      (fr  (GGPCAR x4))
		      (sc  (GGPCDR x4)))
	       (<=> p     (GGPCAR x1))
	       (<=> s     s0)
	       (<=> scut  sc)
	       (<=> stack (GGPCDR stack))
	       (<if> (TRUE x3)
		     (<call> scm_fluid_set_x *delayers* x3))
	       fr)
	     (<begin>
	      (<=> stack (GGPCDR stack))
	      (<next> lp))))))

(define-syntax-rule (RESTORE-STATE-TAIL-NEG-0 scut p tag stack)
  (<recur> lp ()
     (<let> ((x (GGPCAR stack)))
       (<if> (EQ (GGPCAR x) tag)
	     (<let*> ((x1  (GGPCDR x))
		      (x2  (GGPCDR x1))
		      (x4  (GGPCDR x2))
		      (sc  (GGPCDR x4)))
	       (<=> p     (GGPCAR x1))
	       (<=> scut  sc)
	       (<=> stack (GGPCDR stack)))
	     (<begin>
	      (<=> stack (GGPCDR stack))
	      (<next> lp))))))

(define-syntax-rule (RESTORE-STATE-SOFT s p tag fp nstack)
  (<let> ((int v (scm->int tag)))
    (<=> p (SVAR-REF fp nstack v))))
	
(define-syntax-rule (NEXT inst-pt)
  (<let> (((void *) jmp (<ref> *operations*
			       (scm->int (<*> inst-pt)))))
     (<++> inst-pt)
     (<goto> (<*> jmp))))

(define *map* (make-hash-table))
(define-syntax REGISTER
  (syntax-rules ()
    ((_ nm) 
     (begin
       (hash-set! *map* 'nm (instr 'nm))
       (TOUCH nm 2)
       (<=> (<ref> *operations* (<c> (instr 'nm)))
	    (<label-addr> nm))))
    ((_ nm1 nm2)
     (begin
       (hash-set! *map* 'nm2 (instr 'nm1))
       (TOUCH nm2 2)
       (<=> (<ref> *operations* (<c> (instr 'nm1)))
	    (<label-addr> nm2))))))
          
(define-syntax-rule (MAKE-REGS sp)
  (<begin>
   (REGISTER cutter)

   (REGISTER store-state)
   (REGISTER softie)
   (REGISTER softie-light)
   (REGISTER newframe)
   (REGISTER newframe-light)
   (REGISTER newframe-ps)
   (REGISTER newframe-pst)

   (REGISTER unwind)
   (REGISTER unwind-tail)
   (REGISTER unwind-light)
   (REGISTER unwind-light-tail)

   (REGISTER newframe-negation)
   (REGISTER unwind-negation)
   (REGISTER post-negation)

   (REGISTER pre-unify)
   (REGISTER post-unify)
   (REGISTER post-unify-tail)
   (REGISTER post-s)
   (REGISTER post-q)

   (REGISTER clear-sp)
   (REGISTER false)

   (REGISTER set)
   (REGISTER sp-move)
   (REGISTER sp-move-s)
   (REGISTER unify)
   (REGISTER unify-2)
   (REGISTER unify-constant)
   (REGISTER unify-instruction)
   (REGISTER unify-constant-2)
   (REGISTER unify-instruction-2)

   (REGISTER equal-constant)
   (REGISTER equal-instruction)
   
   (REGISTER icurly)
   (REGISTER ifkn)
   (REGISTER icons)
   
   (REGISTER icurly!)
   (REGISTER ifkn!)
   (REGISTER icons!)

   (REGISTER fail)
   (REGISTER cc)
   (REGISTER tail-cc)
   (REGISTER call)
   (REGISTER call-n)
   (REGISTER tail-call)
   
   (REGISTER goto-inst)
   (REGISTER cut)
   (REGISTER post-call)
   (REGISTER post-unicall)
   
   (REGISTER pushv)
   (REGISTER push-constant)
   (REGISTER push-instruction)
   (REGISTER push-variable-s)
   (REGISTER push-variable-v)
   (REGISTER push-2variables-s)
   (REGISTER push-2variables-x)
   (REGISTER push-3variables-s)
   (REGISTER push-3variables-x)
   (REGISTER push-variable-scm)
   (REGISTER pop-variable)
   
   (REGISTER dup)
   (REGISTER pop)
   (REGISTER seek)
   
   (REGISTER mk-cons)
   (REGISTER mk-fkn)
   (REGISTER mk-curly)
   
   (REGISTER #{\\}# lognot)
   (REGISTER op1_- uminus)
   (REGISTER op2+ plus)
   (REGISTER op2- minus)
   (REGISTER op2* mul)
   (REGISTER op2/ divide)
   (REGISTER <<   shift_l)
   (REGISTER >>   shift_r)
   (REGISTER xor)

   (REGISTER plus2_1)
   (REGISTER minus2_1)
   (REGISTER mul2_1)
   (REGISTER div2_1)
   (REGISTER bitand)
   (REGISTER bitor)
   (REGISTER xor1)

   (REGISTER shiftLL)
   (REGISTER shiftLR)
   (REGISTER shiftRL)
   (REGISTER shiftRR)
   (REGISTER shiftLL)
   (REGISTER modL)
   (REGISTER modR)

#|
   (REGISTER ss-add-s)
   (REGISTER ss-sub-s)
   (REGISTER ss-mul-s)
   (REGISTER ss-div-s)

   (REGISTER ss-lshift-s)
   (REGISTER ss-rshift-s)
   (REGISTER ss-and-s)
   (REGISTER ss-or-s)
   (REGISTER ss-xor-s)
   (REGISTER ss-mod-s)

   (REGISTER xx-add-x)
   (REGISTER xx-sub-x)
   (REGISTER xx-mul-x)
   (REGISTER xx-div-x)

   (REGISTER xx-lshift-x)
   (REGISTER xx-rshift-x)
   (REGISTER xx-and-x)
   (REGISTER xx-or-x)
   (REGISTER xx-xor-x)
   (REGISTER xx-mod-x)
|#
   ;(REGISTER //)
   (REGISTER mod modulo)
   ;(REGISTER rem)
   (REGISTER #{/\\}# band)
   (REGISTER #{\\/}# bor)
   ;(REGISTER #{\\}#  bnot)

   (REGISTER op2>  gt)
   (REGISTER op2<  ls)
   (REGISTER op2>= ge)
   (REGISTER op2=< le)

   (REGISTER gtL)
   (REGISTER ltL)
   (REGISTER geL)
   (REGISTER leL)
#|
   (REGISTER ss-gt)
   (REGISTER ss-lt)
   (REGISTER ss-ge)
   (REGISTER ss-le)
   (REGISTER ss-e)
   (REGISTER ss-ne)

   (REGISTER xx-gt)
   (REGISTER xx-lt)
   (REGISTER xx-ge)
   (REGISTER xx-le)
   (REGISTER xx-e)
   (REGISTER xx-ne)
|#
   (REGISTER =:=       eq)
   (REGISTER #{=\\=}# neq)
#|
   (REGISTER is-addi-s)
   (REGISTER is-addi-x)
|#
   sp))

(define-syntax-rule (UNPACK-1 free always middle session const)
  (<begin>
   (<=> always  (<ref> free (<c> 2)))
   (<=> middle  (CDR always))
   (<=> session (CDR middle))
   (<=> const   (CDR session))))

(define-syntax-rule (UNPACK-ALWAYS always p? ninst ctrl-stack pp)
  (<let*> ((SCM x1      (CAR always))	 
	   (SCM x2      (CDR x1)))
     (<=> ninst (scm->int (CAR x1)))
     (<if> p?
	   (UNPACK-ALWAYS-P x2 ctrl-stack pp)
	   (<if> (<==> ninst (<c> 0))
		 (UNPACK-ALWAYS-START x2 ctrl-stack pp)
		 (UNPACK-ALWAYS-CC    x2 ctrl-stack pp)))))

(define-syntax-rule (UNPACK-ALWAYS-START x ctrl-stack pp)
  (<begin>
   (<=> ctrl-stack (<scm> '()))
   (<=> pp         (<scm>  #f))))

(define-syntax-rule (UNPACK-ALWAYS-CC x ctrl-stack pp)
  (<begin>
    (<=> ctrl-stack (CAR x))
    (<=> pp         (CDR x))))
			    
(define-syntax-rule (UNPACK-ALWAYS-P x ctrl-stack pp)
   (<begin>
    (<=> ctrl-stack x)
    (<=> pp        (<scm> #f))))

(define-syntax-rule (UNPACK-MIDDLE middle sp-stack)
  (<=> sp-stack (CAR middle)))

(define-syntax-rule (UNPACK-SESSION session variables)
  (<=> variables (CAR session)))

(define-syntax-rule 
    (UNPACK-CONST const nvar nstack instructions constants tvars)
  (<let> (((SCM *) r (scm->vector const)))
    (<=> nvar         (scm->int    (<ref> r (<c> 0))))
    (<=> nstack       (scm->int    (<ref> r (<c> 1))))
    (<=> constants                 (<ref> r (<c> 2)))
    (<=> instructions              (<ref> r (<c> 3)))
    (<=> tvars                     (<ref> r (<c> 4)))))

(define-syntax-rule (PACK-CONST nvar nstack instructions constants tvars)
  (VECTOR nvar nstack constants instructions tvars))

(define-syntax-rule (PACK-SESSION const)
  (CONS (<scm> #f) const))

(define-syntax-rule (PACK-MIDDLE-START session)
  (CONS (<scm> '()) session))

(define-syntax-rule (PACK-MIDDLE-P-CC session middle sp-stack)
  (<if> (EQ sp-stack (<scm> '()))
	(if (EQ (CAR middle) (<scm> '()))
	    middle
	    (<begin>
	     (<=> middle (CONS sp-stack session))
	     middle))
	(CONS sp-stack session)))

(define-syntax-rule (PACK-ALWAYS-START middle)
  (CONS
   (CONS (<scm> 0) (<scm> '()))
   middle))

(define-syntax-rule (PACK-ALWAYS-P middle inst ctrl-stack)
  (CONS
   (CONS (int->scm inst) ctrl-stack)
   middle))

(define-syntax-rule (PACK-ALWAYS-P- middle inst ctrl-stack)
  (CONS
   (CONS inst ctrl-stack)
   middle))

(define-syntax-rule (PACK-ALWAYS-CC middle inst ctrl-stack pp)
  (CONS
   (CONS (int->scm inst) (CONS ctrl-stack pp))
   middle))

(define-syntax-rule (UNPACK-ENV free narg nlocals)
  (<let> ((x (<ref> free (<c> 1))))
    (<=> narg    (scm->int (CAR x)))
    (<=> nlocals (scm->int (CDR x)))))

(define-syntax-rule (PACK-ENV narg nlocals)
  (CONS (int->scm narg) (int->scm nlocals)))

(define-syntax-rule (PACK-ENV- narg nlocals)
  (CONS narg (int->scm nlocals)))

(define (PACK-START nvar nstack instructions contants tvars)
  (PACK-ALWAYS-START 
   (PACK-MIDDLE-START
    (PACK-SESSION
     (PACK-CONST nvar nstack instructions contants tvars)))))

(<define> SCM pack-start ((SCM nvar )
			  (SCM nstack)
			  (SCM instructions)
			  (SCM contants)
			  (SCM tvars))
  (PACK-START nvar nstack instructions contants tvars))

(define-syntax-rule (PACK-P session middle inst ctrl-stack sp-stack)
  (PACK-ALWAYS-P 
   (PACK-MIDDLE-P-CC session middle sp-stack)
   inst ctrl-stack))

(define-syntax-rule (PACK-P- session middle inst ctrl-stack sp-stack)
  (PACK-ALWAYS-P- 
   (PACK-MIDDLE-P-CC session middle sp-stack)
   inst ctrl-stack))

(define-syntax-rule (PACK-CC session middle inst ctrl-stack sp-stack pp)
  (PACK-ALWAYS-CC
   (PACK-MIDDLE-P-CC session middle sp-stack)
   inst ctrl-stack pp)
)

(define-syntax-rule (NLOCALS-REF      free) (<ref> free (<c> 1)))
(define-syntax-rule (META-REF         free) (<ref> free (<c> 2)))
(define-syntax-rule (INSTR-REF        free) (<ref> free (<c> 3)))
(define-syntax-rule (STORED-STACK-REF free) (<ref> free (<c> 4)))
(define-syntax-rule (GET-NINST free sp recieve?)
  (<if> recieve?
        (GET-RECIEVE-INSTR sp)
	(scm->int (INSTR-REF free))))

(define-syntax-rule (GET-VARIABLES s nvar variables
				   middle session cnst tvars pinned?)
  (<begin>
   (PRINTF "get-variables\n")
   (<if> (TRUE variables)
	 (<let> (((SCM *) vv (scm->vector variables)))
	   (<=> pinned? (VARIABLE-REF (GET-PINNED vv)))
	   vv)
	 (<let*> ((int     n  nvar)
		  (SCM     v  (<call> scm_c_make_vector n (<scm> #f)))
		  ((SCM *) vv (scm->vector v)))
	    (<=> session (CONS v           cnst))
	    (<=> middle  (CONS (<scm> '()) session))
	    (<recur> lp ((int i (<c> 0)))
	      (<if> (q< i n)
		    (<let> ((int var? (scm->int (<ref> tvars i))))
			   (<if> var?
				 (<=> (<ref> vv i) (<call> gp_mkvar s)))
			   (<next> lp (<+> i (<c> 1))))))
	    (<=> (GET-PINNED vv) (GET-TOKEN))
	    vv))))

(define-syntax-rule (DO-CONS s sp)
  (<let> ((c (<call> gp_cons_bang
		      (ARG -2 sp)
		      (ARG -1 sp)
		      s)))
    (<=> (ARG -2 sp)  c)
    (CLEAR 1 sp)))

(define-syntax-rule (PRE-UNIFY nstack inst-pt fp pinned?
			       variables variables-scm nvar
			       cnst session middle call?)
  (<let> ((SCM n (<*> inst-pt)))
   (<=> call? (<c> 0))
   (<=> inst-pt (<+> inst-pt (<c> 1)))
   (UNPACK-VAR 1 n i2 pinned? variables variables-scm nvar
	       cnst session middle 
     (<=> (SVAR-REF fp nstack i2) (<call> scm_fluid_ref *delayers*))
     (<=> (<ref> variables i2) (<call> scm_fluid_ref *delayers*)))))

	
    
(define-syntax-rule (POST-UNIFY inst-pt sp-stack nstack sp fp 
				pinned? variables variables-scm nvar
				cnst session middle)
  (<let*> ((SCM n     (<*> inst-pt))
	   (int nsloc (scm->int (<ref> inst-pt (<c> 1))))
	   (SCM old (UNPACK-VAR 0 n i2 pinned? variables variables-scm nvar
				cnst session middle
		      (SVAR-REF fp nstack i2)
		      (<ref> variables i2))))
    (UNPACK-VAR 1 n i2 pinned? variables variables-scm nvar
		cnst session middle
       (<=> (SVAR-REF fp nstack i2) (<scm> #f))
       (<=> (<ref> variables i2) (<scm> #f)))	 
    (<=> inst-pt (<+> inst-pt (<c> 2)))
    (<if> (<not> (EQ (<call> scm_fluid_ref *delayers*) old))
	  (<begin>
	   (<=> sp-stack (GET-STACK fp sp))
	   (<if> (q> nsloc (<c> 0))
		 (LINK-SVARS fp nstack nsloc sp-stack))
	   (<=> (FARG 0 fp) *dls*)		  
	   (<=> (FARG 4 fp) old)
	   (<=> sp (PFARG 5 fp))
	   (<goto> call)))))

(define-syntax-rule (POST-UNIFY-TAIL inst-pt sp-stack nstack
				     sp fp pinned? variables variables-scm
				     nvar cnst session middle)
  (<let*> ((SCM n   (<*> inst-pt))
	   (SCM old (UNPACK-VAR 0 n i2 pinned? variables variables-scm nvar
				cnst session middle
		      (SVAR-REF fp nstack i2)
		      (<ref> variables i2))))

    (UNPACK-VAR 1 n i2 pinned? variables variables-scm nvar
		cnst session middle
       (<=> (SVAR-REF fp nstack i2) (<scm> #f))
       (<=> (<ref> variables i2) (<scm> #f)))

    (<=> inst-pt (<+> inst-pt (<c> 1)))
    (<if> (<not> (EQ (<call> scm_fluid_ref *delayers*) old))
	  (<begin>
	   (<=> sp-stack (GET-STACK fp sp))
	   (<=> (FARG 0 fp) *dls*)		  
	   (<=> (FARG 4 fp) old)
	   (<=> sp (PFARG 5 fp))
	   (<goto> tail-call))
	  (<goto> cc))))

(define-syntax-rule (ARITH op schmop x y)
  (<if> (<and> (<call> SCM_I_INUMP x) (<call> SCM_I_INUMP y))
	(<let> ((scm_t_int64 n (op (<call> SCM_I_INUM x)
				   (<call> SCM_I_INUM y))))
	       (<if> (<call> SCM_FIXABLE n)
		     (<call> SCM_I_MAKINUM n)
		     (<call> schmop x y)))
	(<call> schmop x y)))

(define-syntax-rule (ARITH-1 op schmop x)
  (<if> (<call> SCM_I_INUMP x)
	(<let> ((scm_t_int64 n (op (<call> SCM_I_INUM x))))
	       (<if> (<call> SCM_FIXABLE n)
		     (<call> SCM_I_MAKINUM n)
		     (<call> schmop x)))
	(<call> schmop x)))

(define-syntax-rule (CMP op schmop p instructions inst-pt fp sp)
  (<let> ((x (ARG -2 sp))
	  (y (ARG -1 sp)))
    (CLEAR 2 sp)
    (FORMAT "~a ~a ~a~%\n" x (<scm> 'schmop) y)
    (<if> (<and> (<call> SCM_I_INUMP x) (<call> SCM_I_INUMP y))
	  (<if> (<not> (op x y))
		(BACKTRACK p instructions inst-pt fp sp))
	  (<if> (<call> scm_is_false (<call> schmop x y))
		(BACKTRACK p instructions inst-pt fp sp)))))

(define-syntax-rule (CMP-SS op schmop p instructions inst-pt fp sp nstack)
  (<let*> ((ulong v   (scm->ulong (<*> inst-pt)))
	   (int   v1  (<bit-and> v (<c> #xffff)))
	   (int   v2  (q>> v (<c> 16)))
	   (x (SVAR-REF fp nstack v1))
	   (y (SVAR-REF fp nstack v2)))
    (<=> inst-pt (<+> inst-pt (<c> 1)))
    (FORMAT "~a ~a ~a~%\n" x (<scm> 'schmop) y)
    (<if> (<and> (<call> SCM_I_INUMP x) (<call> SCM_I_INUMP y))
	  (<if> (<not> (op x y))
		(BACKTRACK p instructions inst-pt fp sp))
	  (<if> (<call> scm_is_false (<call> schmop x y))
		(BACKTRACK p instructions inst-pt fp sp)))))

(define-syntax-rule (CMP-XX op schmop p instructions inst-pt fp sp nstack 
			    variables)
  (<let*> ((ulong v   (scm->ulong (<*> inst-pt)))
	   (int   v1  (<bit-and> v (<c> #xffff)))
	   (int   v2  (q>> (<bit-and> v (<c> #xffff0000)) (<c> 16)))
	   (int   q   (q>> v (<c> 32)))
	   (x (<if> (<bit-and> q (<c> 1))
		    (SVAR-REF fp nstack v1)
		    (<ref> variables v1)))
	   (y (<if> (<bit-and> q (<c> 2))
		    (SVAR-REF fp nstack v2)
		    (<ref> variables v2))))

    (<=> inst-pt (<+> inst-pt (<c> 1)))
    (FORMAT "~a ~a ~a~%\n" x (<scm> 'schmop) y)
    (<if> (<and> (<call> SCM_I_INUMP x) (<call> SCM_I_INUMP y))
	  (<if> (<not> (op x y))
		(BACKTRACK p instructions inst-pt fp sp))
	  (<if> (<call> scm_is_false (<call> schmop x y))
		(BACKTRACK p instructions inst-pt fp sp)))))

(define-syntax-rule (CMP-1 op schmop p instructions inst-pt fp sp)
  (<let> ((x (<*> inst-pt))
	  (y (ARG -1 sp)))
    (<=> inst-pt (<+> inst-pt (<c> 1)))
    (CLEAR 1 sp)
    (FORMAT "~a ~a ~a~%\n" x (<scm> 'schmop) y)
    (<if> (<and> (<call> SCM_I_INUMP x) (<call> SCM_I_INUMP y))
	  (<if> (<not> (op x y))
		(BACKTRACK p instructions inst-pt fp sp))
	  (<if> (<call> scm_is_false (<call> schmop x y))
		(BACKTRACK p instructions inst-pt fp sp)))))


(define-syntax-rule (NOTCMP op schmop p instructions inst-pt fp sp)
  (<let> ((y (<let> ((nn (<*> inst-pt)))
		(<++> inst-pt)
		(<if> (<call> scm_is_false nn)
		      (<let> ((xx (ARG -1 sp)))
		        (CLEAR 1 sp)
		        xx)
		      nn)))		      
	  (x (ARG -1 sp)))
    (FORMAT "~a ~a ~a~%\n" x (<scm> 'schmop) y)
    (<if> (<and> (<call> SCM_I_INUMP x) (<call> SCM_I_INUMP y))
	  (<if> (op x y)
		(BACKTRACK p instructions inst-pt fp sp))
	  (<if> (<call> scm_is_true (<call> schmop x y))
		(BACKTRACK p instructions inst-pt fp sp)))
    (CLEAR 1 sp)))

(define-syntax-rule (NOTCMP-SS op schmop p instructions inst-pt fp sp nstack)
  (<let*> ((ulong v   (scm->ulong (<*> inst-pt)))
	   (int   v1  (<bit-and> v (<c> #xffff)))
	   (int   v2  (q>> v (<c> 16)))
	   (x (SVAR-REF fp nstack v1))
	   (y (SVAR-REF fp nstack v2)))
    (<=> inst-pt (<+> inst-pt (<c> 1)))
    (FORMAT "~a ~a ~a~%\n" x (<scm> 'schmop) y)
    (<if> (<and> (<call> SCM_I_INUMP x) (<call> SCM_I_INUMP y))
	  (<if> (op x y)
		(BACKTRACK p instructions inst-pt fp sp))
	  (<if> (<call> scm_is_true (<call> schmop x y))
		(BACKTRACK p instructions inst-pt fp sp)))))

(define-syntax-rule (NOTCMP-XX op schmop p instructions inst-pt fp sp nstack 
			       variables)
  (<let*> ((ulong v   (scm->ulong (<*> inst-pt)))
	   (int   v1  (<bit-and> v (<c> #xffff)))
	   (int   v2  (q>> (<bit-and> v (<c> #xffff0000)) (<c> 16)))
	   (int   q   (q>> v (<c> 32)))
	   (SCM   x   (<if> (<bit-and> q (<c> 1))
			    (SVAR-REF fp nstack v1)
			    (<ref> variables v1)))
	   (SCM   y   (<if> (<bit-and> q (<c> 2))
			    (SVAR-REF fp nstack v2)
			    (<ref> variables v2))))
    (<=> inst-pt (<+> inst-pt (<c> 1)))
    (FORMAT "~a ~a ~a~%\n" x (<scm> 'schmop) y)
    (<if> (<and> (<call> SCM_I_INUMP x) (<call> SCM_I_INUMP y))
	  (<if> (op x y)
		(BACKTRACK p instructions inst-pt fp sp))
	  (<if> (<call> scm_is_true (<call> schmop x y))
		(BACKTRACK p instructions inst-pt fp sp)))))

(define-syntax-rule (MUL x y)
  (<if> (<and> (<call> SCM_I_INUMP x) (<call> SCM_I_INUMP y))
	(<let> ((scm_t_int64 xx (<call> SCM_I_INUM x))
		(scm_t_int64 yy (<call> SCM_I_INUM y)))
	    (<if> (<and> 
		   (q< xx      (<c> #x40000000))
		   (<and>
		    (q> xx (<-> (<c> #x40000000)))
		    (<and> 
		     (q< yy      (<c> #x40000000))
		     (q> yy (<-> (<c> #x40000000))))))
	       (<let> ((scm_t_int64 n (<*> xx yy)))
		 (<call> SCM_I_MAKINUM n))
	       (<call> scm_product x y)))
	(<call> scm_product x y)))

(define-syntax-rule (SHIFT-L op cop x y)
  (<if> (<and> (<call> SCM_I_INUMP x) (<call> SCM_I_INUMP y))
	(<let> ((scm_t_int64 xx (<call> SCM_I_INUM x))
		(scm_t_int64 yy (<call> SCM_I_INUM y)))
	    (<if> (<and> 
		   (q< xx      (<c> #x10000000))
		   (<and>
		    (q> xx (<-> (<c> #x10000000)))
		    (<and> 
		     (q<= yy (<c> 32))
		     (q>= yy (<c> 0)))))
	       (<let> ((scm_t_int64 n (op xx yy)))
		 (<call> SCM_I_MAKINUM n))		 
	       (<call> cop x y)))
	(<call> cop x y)))

(define-syntax-rule (SHIFT-R op cop x y)
  (<if> (<and> (<call> SCM_I_INUMP x) (<call> SCM_I_INUMP y))
	(<let> ((scm_t_int64 xx (<call> SCM_I_INUM x))
		(scm_t_int64 yy (<call> SCM_I_INUM y)))
	    (<if> (<and> 
		   (q< xx      (<c> #x10000000))
		   (<and>
		    (q> xx (<-> (<c> #x10000000)))
		    (<and> 
		     (q<= yy (<c> 32))
		     (q>= yy (<c> 0)))))
	       (<let> ((scm_t_int64 n (op xx yy)))
		  (<call> SCM_I_MAKINUM n))
	       (<call> cop x (<call> scm_difference y (<scm> 0)))))
	(<call> cop x (<call> scm_difference y (<scm> 0)))))

(define-syntax-rule (NEWFRAME s)
  (<begin>
   (<recur> lp  ((SCM l (<call> scm_fluid_ref *unwind-parameters*)))
    (<if> (<call> SCM_CONSP l)
	  (<begin>
	   (<call> gp_with_fluid
		    *unwind-parameters*
		    (<let> ((SCM x (CAR l)))
		       (CONS
			x
			(<call> 
			 scm_reverse
			 (<recur> lp ((l x) (r (<scm> '())))
			   (<if> (PAIR? l)
				 (<next> lp
					 (CDR l)
					 (<let> ((f (CAR l)))
						(CONS
						 (CONS
						  f
						  (CALL f))
						 r)))
				 r))))))
	   (<next> lp (CDR l)))))
   (<call> gp_gp_newframe s)))
			
(define-syntax-rule (UNWIND-SOFT ncons)
  (<call> gp_gp_unwind_soft ncons))

(define-syntax-rule (UNWIND s ncons)
  (<begin>
   (<call> scm_fluid_set_x *unwind-hooks* (<scm> '()))
   (<call> gp_gp_unwind_ncons s ncons)
   (<recur>  lp ((SCM l (CDR (<call> scm_fluid_ref *unwind-parameters*))))
      (<if> (PAIR? l)
	    (<let> ((SCM f (CAR l)))
	       (CALL (CAR f) (CDR f))
	       (<next> lp (CDR l)))))
   (<recur> lp ((SCM l (<call> scm_reverse
				(<call> scm_fluid_ref *unwind-hooks*))))
     (<if> (PAIR? l)
	   (<begin>
	    (CALL (CAR l) s *false* *true*)
	    (<call> gp_gp_unwind_ncons  s ncons)
	    (<next> lp (CDR l)))))))

(define-syntax-rule (UNWIND-TAIL s)
  (<begin>
   (<call> scm_fluid_set_x *unwind-hooks* (<scm> '()))
   (<recur>  lp ((SCM l (CDR (<call> scm_fluid_ref *unwind-parameters*))))
      (<if> (PAIR? l)
	    (<let> ((SCM f (CAR l)))
	       (CALL (CAR f) (CDR f))
	       (<next> lp (CDR l)))))
   (<let> ((SCM l (<call> scm_reverse
			  (<call> scm_fluid_ref *unwind-hooks*))))
     (<if> (PAIR? l)
	   (<begin>
	    (<call> gp_gp_unwind s)
	    (<recur> lp ((SCM l l))
	      (<if> (PAIR? l)
		    (<begin>
		     (CALL (CAR l) s *false* *true*)
		     (<call> gp_gp_unwind s)
		     (<next> lp (CDR l)))))
	    (<call> gp_gp_unwind_tail s))
	   (<call> gp_gp_unwind_tail s)))))

(<define> SCM gp_c_vector_x ((SCM x) (int n) (SCM s))
  (<if> (<and> (<call> SCM_I_IS_VECTOR x) 
	       (<==> n (<call> SCM_I_VECTOR_LENGTH x)))
	s
	(<if> (TRUE (<call> gp_varp x s))
	      (<let> ((SCM v (<call> scm_c_make_vector n (<scm> #f))))
		     (<recur> lp ((int i n))
		       (<if> (<==> i (<c> 0))
			     (<call> gp_gp_unify x v s)
			     (<let> ((int ii (<-> i (<c> 1))))
				(<call> scm_c_vector_set_x v ii
					(<call> gp_mkvar s))
				(<next> lp ii)))))
	      (<scm> #f))))

(<define> SCM gp_c_vector ((SCM x) (int n) (SCM s))
  (<let> ((x (<call> gp_gp_lookup x s)))
    (<if> (<and> (<call> SCM_I_IS_VECTOR x) 
		 (<==> n (<call> SCM_I_VECTOR_LENGTH x)))
	  s
	  (<c> 0))))

(<global> SCM *model-lambda* (<scm> #f))
(define-syntax-rule (MAKEP-P nlocals session middle inst ctrl-stack 
			     sp-stack)
  (<begin>
   (PRINTF "MAKE-P")
   (<call> gp_custom_fkn *model-lambda*
	   (PACK-ENV (<c> 1) nlocals)
	   (PACK-P session middle inst ctrl-stack sp-stack))))

(define-syntax-rule (MAKEP-P- nlocals session middle inst ctrl-stack 
			     sp-stack)
  (<begin>
   (PRINTF "MAKEP-P-\n");
   (<call> gp_custom_fkn *model-lambda*
	   (PACK-ENV (<c> 1) nlocals)
	   (PACK-P- session middle inst ctrl-stack sp-stack))))

(define-syntax-rule (MK-P p pp code)
  (<begin>
   (FORMAT "MK-P p = ~a, pp = ~a~%" p pp)
   (<if> (NUMBER? p)
	 (<if> (<and> (PAIR? pp) (EQ (CAR pp) p))
	       (CDR pp)
	       code)
	 p)))

(define-syntax-rule (GC iter)
  (<begin>
   (<=> iter (<%> (<+> iter (<c> 1)) (<c> 20)))
   (<if> (<==> iter (<c> 0)) (<call> gp_gc))))

(define-syntax-rule (MAKE-CC narg nlocals session middle inst ctrl-stack
			     sp-stack p0 p pp)
  (<call> gp_custom_fkn *model-lambda*
	  (PACK-ENV narg nlocals)	  
	  (PACK-CC session middle inst ctrl-stack sp-stack
		   (<if> (<and> (PAIR? pp) (EQ (CAR pp) p0))
			 pp
			 (CONS p0 p)))))

(define-syntax-rule (MAKE-CC- narg nlocals session middle inst ctrl-stack 
			      sp-stack p0 p pp)
  (<call> gp_custom_fkn *model-lambda*
	  (PACK-ENV- narg nlocals)	  
	  (PACK-CC session middle inst ctrl-stack sp-stack
		   (<if> (<and> (PAIR? pp) (EQ (CAR pp) p0))
			 pp
			 (CONS p0 p)))))
	  	 		     
(<define> SCM init-vm-data ((SCM dls               ) 
			    (SCM delayers          )
			    (SCM unwind-hooks      )
			    (SCM unwind-parameters )
			    (SCM true              )
			    (SCM false             )
			    (SCM gp-not-n          )            
			    (SCM gp-is-delayed?    )
			    (SCM model-lambda      ))

   (<=> *dls*               dls               )
   (<=> *delayers*          delayers          )
   (<=> *unwind-hooks*      unwind-hooks      )
   (<=> *unwind-parameters* unwind-parameters )
   (<=> *true*              true              )
   (<=> *false*             false             )
   (<=> *gp-not-n*          gp-not-n          )      
   (<=> *gp-is-delayed?*    gp-is-delayed?    )
   (<=> *model-lambda*      model-lambda      )
   (<scm> #f))
 
(<define> (SCM *) vm-raw (((scm_i_thread *) thread)
			  ((vp_t         *) vp)
			  ((SCM *)          fp)
			  ((SCM *)          sp)
			  ((SCM *)          free)
			  ((scm_t_uint32 *) recieve?)
			  (int              register?))
  (<--> sp)
  (<if> register?
	(MAKE-REGS sp)
	(<let> ((SCM     pinned?       (<scm> #f))
		(int     call?         (<c> 1))
		(int     narg          (<c> 0))
		(int     nlocals       (<c> 0))
   		(SCM     always        (<scm> #f))
		(SCM     middle        (<scm> #f))
		(SCM     session       (<scm> #f))
		(SCM     cnst          (<scm> #f))
		(SCM     variables-scm (<scm> #f))
		((SCM *) variables     (<c> 0))
		(SCM     constants-scm (<scm> #f))
		((SCM *) constants     (<c> 0))
		(SCM     instructions-scm (<scm> #f))
		((SCM *) instructions  (<c> 0))
		(SCM     tvars-scm     (<scm> #f))
		((SCM *) tvars         (<c> 0))
		(int     ninst         (<c> 0))
		(int     nstack        (<c> 0))
		(int     nvar          (<c> 2))
		(SCM     ctrl-stack    (<scm> '()))
		(SCM     sp-stack      (<scm> '()))
		((SCM *) inst-pt       (<c> 0))
		(int     p?            (<c> 0))
		(SCM     pp            (<scm> #f))
		(SCM     s             (<scm> #f))
		(SCM     p             (<scm> 0))		 
		(SCM     cut           (<scm> 0))
		(SCM     scut          (<scm> 0))
		(int     iter          (<c> 0)))

           (
            (define-syntax-rule (get vc e)
              (<if> e		     
                    (<ref> variables    vc)
                    (SVAR-REF fp nstack vc)))
            )
           
           (UNPACK-ENV free narg nlocals)
	   
	   (UNPACK-1 free always middle session cnst)    
           (PRSTACK sp fp)

	   (SET-S-P p? s p sp fp)
           
           (<if> p? (<=> sp fp)) 
           
	   (UNPACK-CONST
	    cnst nvar nstack instructions-scm constants-scm tvars-scm)

	   (<=> instructions (scm->vector instructions-scm))
	   (<=> constants    (scm->vector constants-scm))
	   (<=> tvars        (scm->vector tvars-scm))
                         
	   (UNPACK-SESSION session variables-scm)

	   (<=> variables     
		(GET-VARIABLES s nvar variables-scm 
			       middle session cnst
			       tvars pinned?))

	   (UNPACK-ALWAYS always p? ninst ctrl-stack pp)

	   (<if> (<or> p? (<==> ninst (<c> 0)))
		 (<=> call? (<c> 0)))

	   (<if> (<and> (PAIR? pp) (EQ (CDR pp) p))
		 (<=> p  (CAR pp)))

	   (SET-CC ninst variables fp)

	   (UNPACK-MIDDLE middle sp-stack)	   

	   (<=> inst-pt (<+> instructions ninst))

	   (<call> INTERUPT)
	   (<call> gp_gc)

           (<=> cut  p)
           (<=> scut s)
	   (NEXT inst-pt)

	   (LABEL pre-unify)
           (PRSTACK sp fp)
	   (PRE-UNIFY nstack inst-pt fp
		      pinned? variables variables-scm nvar
		      cnst session middle call?)
	   (NEXT inst-pt)

	   (LABEL post-unify)
           (PRSTACK sp fp)
	   (POST-UNIFY inst-pt sp-stack nstack sp fp
		       pinned? variables variables-scm nvar
		       cnst session middle)
	   (NEXT inst-pt)

	   (LABEL post-unify-tail)
           (PRSTACK sp fp)
	   (POST-UNIFY-TAIL inst-pt sp-stack nstack sp fp
			    pinned? variables variables-scm nvar
			    cnst session middle)
	   
	   (LABEL clear-sp)
           (PRSTACK sp fp)
	   (<=> sp-stack (GET-STACK fp sp))
	   (CLEAR-SP-XP sp fp)
	   (NEXT inst-pt)

	   (LABEL cc)
           (PRSTACK sp fp)
	   (<let*> ((SCM st (GET-STACK fp sp))
		    (SCM p  (MK-P p pp
				  (MAKEP-P- nlocals 
					    session 
					    middle 
					    p
					    ctrl-stack 
					    sp-stack)))
		    (SCM cc (GET-CC variables)))
	     (FORMAT "STACK: ~a~%" st)
	     (CLEAR-SP-XP sp fp)
	     (<=> (FARG 0 fp) cc)	  
	     (<=> (FARG 1 fp) s)
	     (<=> (FARG 2 fp) p)
	     (<=> sp (PFARG 3 fp))
	     (<goto> ret))

	   (LABEL cutter)
	   (<let*> ((ulong v   (scm->ulong (<*> inst-pt)))
		    (ulong vc  (<bit-and> v (<c> #xffff)))
		    (ulong vcs (<bit-and> (q>> v (<c> 16)) (<c> #xffff)))
		    (ulong q   (q>> v (<c> 32)))
		    (SCM   xs  (<if> (<bit-and> q (<c> 1))		     
				     (<ref> variables    vc)
				     (SVAR-REF fp nstack vc)))
		    (SCM   xcs (<if> (<bit-and> q (<c> 2))		     
				     (<ref> variables    vcs)
				     (SVAR-REF fp nstack vcs))))
	    (<=> inst-pt (<+> inst-pt (<c> 1)))
	    (<=> cut  xs)  
	    (<=> scut xcs)
	    (<=> (GET-CUT variables) xs)
	    (NEXT inst-pt))

	   (LABEL call)	  
           (PRSTACK sp fp)
	   (<let*> ((SCM p0 p)
		    (SCM p  (MK-P p pp 
				   (MAKEP-P nlocals 
					    session 
					    middle 
					    (<-> inst-pt instructions)
					    ctrl-stack 
					    sp-stack)))
		    (SCM cc (MAKE-CC (<c> 3) 
                                     nlocals session middle 
				     (<-> inst-pt instructions) 
				     ctrl-stack sp-stack p0 p pp))) 
	     (<=> (FARG 1 fp) s)
	     (<=> (FARG 2 fp) p)
	     (<=> (FARG 3 fp) cc)	     
	     (<goto> ret))

	   (LABEL call-n)	  
           (PRSTACK sp fp)
	   (<let*> ((SCM na (<*> inst-pt))
		    (SCM p0 p)
		    (SCM p  (MK-P p pp 
				   (MAKEP-P nlocals 
					    session 
					    middle 
					    (<-> inst-pt instructions)
					    ctrl-stack 
					    sp-stack)))
		    (SCM cc (MAKE-CC- na nlocals session middle 
				     (<-> (<+> inst-pt (<c> 1))
					  instructions)
				     ctrl-stack sp-stack p0 p pp))) 
	     (<=> (FARG 1 fp) s)
	     (<=> (FARG 2 fp) p)
	     (<=> (FARG 3 fp) cc)	     
	     (<goto> ret))
	   	   
	   (LABEL tail-call)	  
           (PRSTACK sp fp) 
	   (<let> ((SCM p  (MK-P p pp 
				   (MAKEP-P- nlocals 
					     session 
					     middle 
					     p
					     ctrl-stack 
					     sp-stack)))
		   (SCM cc (GET-CC variables)))
	     (<=> (FARG 1 fp) s)
	     (<=> (FARG 2 fp) p)
	     (<=> (FARG 3 fp) cc)	     
	     (<goto> ret))

	   (LABEL tail-cc)	  
           (PRSTACK sp fp) 
	   (<let> ((SCM p  (MK-P p pp 
				   (MAKEP-P nlocals 
					    session 
					    middle 
					    (<-> inst-pt instructions)
					    ctrl-stack 
					    sp-stack)))
		   (SCM cc (GET-CC variables)))
             (<=> (FARG 0 fp) cc)
	     (<=> (FARG 1 fp) s)
	     (<=> (FARG 2 fp) p)
	     (<goto> ret))

	   (LABEL store-state)
           (PRSTACK sp fp)
	   (<let> ((SCM tag (<ref> inst-pt (<c> 0)))
		   (SCM np  (<ref> inst-pt (<c> 1))))
	     (<=> inst-pt (<+> inst-pt (<c> 2)))
	     (STORE-STATE-SOFT s tag p fp nstack)
	     (<call> INTERUPT)
	     (NEXT inst-pt))
           
           (LABEL newframe-ps)
           (PRSTACK sp fp)
           (<let*> ((SCM    tag (<ref> inst-pt (<c> 0)))
                    (ulong  x   (scm->ulong (<ref> inst-pt (<c> 0))))
                    (int    p1  (<and> x (<c> #ffff)))
                    (int    s1  (<and> (q>> x (<c> 16)) (<c> #ffff)))
                    (int    p2  (<and> (q>> x (<c> 32)) (<c> 1)))
                    (int    s2  (<and> (q>> x (<c> 32)) (<c> 2)))
                    (SCM    fr  (NEWFRAME s)))
              (<=> inst-pt (<+> inst-pt (<c> 2)))
              (set p1 p2 p)
              (set s1 s2 s)
              (<=> ctrl-stack (STOR-STATE tag ctrl-stack)))
           
           (LABEL newframe-pst)
           (<let*> ((SCM    tag (<ref> inst-pt (<c> 0)))
                    (ulong  x   (scm->ulong (<ref> inst-pt (<c> 0))))
                    (int    p1  (<and> x (<c> #ffff)))
                    (int    s1  (<and> (q>> x (<c> 16)) (<c> #ffff)))
                    (int    t1  (<and> (q>> x (<c> 32)) (<c> #ffff)))
                    (int    p2  (<and> (q>> x (<c> 48)) (<c> 1)))
                    (int    s2  (<and> (q>> x (<c> 48)) (<c> 2)))
                    (SCM    fr  (NEWFRAME s)))
              (<=> inst-pt (<+> inst-pt (<c> 2)))
              (set p1 p2 p)
              (set s1 s2 s)
              (set t1 t2 (<call> scm_fluid_ref *delayers*) s)
              (<=> ctrl-stack (STOR-STATE tag ctrl-stack)))
           
           (PRSTACK sp fp)

	   (LABEL newframe-light)
           (PRSTACK sp fp)
	   (<let> ((SCM tag (<ref> inst-pt (<c> 0)))
		   (SCM np  (<ref> inst-pt (<c> 1))))
	     (<=> inst-pt (<+> inst-pt (<c> 2)))
	     (STORE-STATE-SOFT s tag p fp nstack)
	     (<=> p np)
	     (<call> INTERUPT)
	     (NEXT inst-pt))
	   
	   (LABEL newframe)
           (PRSTACK sp fp)
	   (<let> ((SCM np  (<ref> inst-pt (<c> 0)))
		   (int tp (scm->int (<ref> inst-pt (<c> 1))))
		   (SCM fr (NEWFRAME s)))
	     (<=> inst-pt (<+> inst-pt (<c> 2)))
	     (<=> ctrl-stack (STORE-STATE tp np s fr p ctrl-stack))
	     (<=> s fr)	     
	     (<=> p np)
	     (<call> INTERUPT)
	     (NEXT inst-pt))

	   (LABEL newframe-negation)
           (PRSTACK sp fp)
	   (<let> ((SCM np  (<ref> inst-pt (<c> 0)))
		   (int tp (scm->int (<ref> inst-pt (<c> 1))))
		   (SCM so s)
		   (SCM ss (NEWFRAME s)))
	     (<=> inst-pt (<+> inst-pt (<c> 2)))
	     (<=> s ss)
	     (<=> s (<call> gp_set 
			     *gp-not-n* 
			     (<call>
			      SCM_I_MAKINUM
			      (<+> (<c> 1)
				   (<call> SCM_I_INUM 
					   (<call> gp_gp_lookup 
						   *gp-not-n* s))))
			     s))
	     (<=> s (<call> gp_set *gp-is-delayed?* (<scm> #f) s))
	     (<=> ctrl-stack (STORE-STATE-NEG tp np so ss scut p ctrl-stack))
	     (<=> p   np)
	     (<=> cut np)
	     (<call> INTERUPT)
	     (NEXT inst-pt))



	   ;;\+x = [[newframe-negation a b] 
           ;;       [.x.] 
           ;;       [unwind-negation a c]
           ;;       [label b] [post-negation a] [label c]
           ;; x;y;z = [newframe a b] 
           ;;         [.x.] 
           ;;         [label b] [unwind a c]
           ;;         [.y.]
           ;;         [label c] [unwind-tail a]
           ;;         [.z.]
	   (LABEL post-negation)
           (PRSTACK sp fp)
	   (<let> ((SCM np  (<ref> inst-pt (<c> 0))))
	      (<=> cut      (<ref> inst-pt (<c> 1)))
	      (<=> inst-pt  (<+> inst-pt (<c> 2)))
	      (<let> ((SCM fr (RESTORE-STATE-TAIL-NEG s scut p np ctrl-stack))
		      (int n  (scm->int (<call> gp_gp_lookup *gp-not-n* s)))
		      (SCM d  (<call> gp_gp_lookup *gp-is-delayed?* s)))
		 (UNWIND-TAIL fr)
		 (<if> (<and> (q> n (<c> 1)) 
			      (<call> scm_is_true d))
		       (<begin>
			(<call> gp_fluid_force_bang
				 *gp-is-delayed?* (<scm> #t) s)
			(BACKTRACK p instructions inst-pt fp sp))
		       (NEXT inst-pt))))

	   (LABEL post-s)
	   (PRSTACK sp fp)
	   (<let> ((SCM np  (<ref> inst-pt (<c> 0))))
	      (<=> cut (<ref> inst-pt (<c> 1)))
	      (<=> inst-pt (<+> inst-pt (<c> 2)))
  	      (<let> ((fr (RESTORE-STATE-TAIL-NEG s scut p np ctrl-stack)))
		(UNWIND-TAIL fr)
		(NEXT inst-pt)))

	   (LABEL post-q)
	   (PRSTACK sp fp)
	   (<=> cut (<ref> inst-pt (<c> 0)))
	   (<=> inst-pt (<+> inst-pt (<c> 1)))
	   (NEXT inst-pt)

	   ;; We will
	   (LABEL unwind-tail)
           (PRSTACK sp fp)
	   (<let> ((SCM tag  (<*> inst-pt)))
	      (<=> inst-pt (<+> inst-pt (<c> 1)))
	      (<let> ((ss (RESTORE-STATE-TAIL s p tag ctrl-stack)))
		(UNWIND-TAIL ss)
		(NEXT inst-pt)))

	   (LABEL unwind-light-tail)
           (PRSTACK sp fp)
	   (<let> ((SCM tag  (<*> inst-pt)))
	      (<=> inst-pt (<+> inst-pt (<c> 1)))
	      (RESTORE-STATE-SOFT s p tag fp nstack)
	      (NEXT inst-pt))

	   (LABEL softie)
           (PRSTACK sp fp)
	   (<let> ((SCM tag  (<*> inst-pt)))
	      (<=> inst-pt (<+> inst-pt (<c> 1)))
	      (RESTORE-STATE-TAIL-P p tag ctrl-stack)
	      (NEXT inst-pt))

	   (LABEL softie-light)
           (PRSTACK sp fp)
	   (<let> ((SCM tag  (<*> inst-pt)))
	      (<=> inst-pt (<+> inst-pt (<c> 1)))
	      (RESTORE-STATE-SOFT s p tag fp nstack)
	      (NEXT inst-pt))

	   (LABEL unwind)
           (PRSTACK sp fp)
	   (<let> ((SCM tag (<ref> inst-pt (<c> 0))))
	      (<=> p       (<ref> inst-pt (<c> 1)))
	      (<=> inst-pt (<+> inst-pt (<c> 2)))
	      (<let> ((int ncons (RESTORE-STATE s tag ctrl-stack)))
		(UNWIND s ncons)
		(NEXT inst-pt)))

	   (LABEL unwind-light)
           (PRSTACK sp fp)
	   (<=> p       (<ref> inst-pt (<c> 1)))
	   (<=> inst-pt (<+> inst-pt (<c> 1)))
	   (NEXT inst-pt)


	   (LABEL unwind-negation)
           (PRSTACK sp fp)
	   (<let> ((SCM n (<*> inst-pt)))
	      (<=> cut (<ref> inst-pt (<c> 1)))
	      (RESTORE-STATE-TAIL-NEG-0 scut p n ctrl-stack)
	      (<call> gp_fluid_force_bang *gp-is-delayed?* (<scm> #f) s)
	      (BACKTRACK p instructions inst-pt fp sp))


	   (LABEL false)
           (PRSTACK sp fp)
	   (BACKTRACK p instructions inst-pt fp sp)



	   (LABEL goto-inst)
           (PRSTACK sp fp)
	   (<call> INTERUPT)
	   (GC iter)
	   (<let> ((int ni (scm->int (<*> inst-pt))))
	     (<=> inst-pt (<+> instructions ni))
	     (NEXT inst-pt))


 	   (LABEL post-call)
           (PRSTACK sp fp)
	   (<let> ((SCM c      (<ref> inst-pt (<c> 0)))
		   (SCM pop?   (<ref> inst-pt (<c> 1))))
	     (<=> inst-pt (<+> inst-pt (<c> 2)))
	     (<if> call?
		   (<begin>
		    (<=> call? (<c> 0))
		    (<if> (TRUE pop?)
			  (DECR 3 sp))
		    (<=> cut c)
		    (<if> (PAIR? sp-stack)
			  (INSTALL-STACK fp sp nstack (<c> 0) sp-stack))
		    (NEXT inst-pt))))

 	   (LABEL post-unicall)
           (PRSTACK sp fp)
	   (<let> ((SCM c     (<ref> inst-pt (<c> 0)))
		   (int nsloc (scm->int (<ref> inst-pt (<c> 1)))))
	     (<=> inst-pt (<+> inst-pt (<c> 2)))
             (<if> call?
		   (<begin>
		    (<=> call? (<c> 0))
		    (DECR 3 sp)
		    (<=> cut c)
		    (<if> (PAIR? sp-stack)
			  (INSTALL-STACK fp sp nstack nsloc sp-stack))))
	     (NEXT inst-pt))
	   


	   (LABEL fail)
	   (<if> (EQ p (<scm> 0)) (<=> p (GET-P variables)))		 
	   (BACKTRACK p instructions inst-pt fp sp)

	   (LABEL cut)
           (PRSTACK sp fp)
	   (<if> (EQ cut (<scm> 0))
		 (<=>  p   (GET-CUT variables))
		 (<=>  p   cut))
	   (PRUNE scut)
	   (NEXT inst-pt)	

	   (LABEL set)	   
	   (<let*> ((SCM n (<ref> inst-pt (<c> 0)))
		    (ss    
		     (SET 
		      (UNPACK-VAR 0 n i pinned? variables 
				  variables-scm nvar
				  cnst session middle
			 (SVAR-REF fp nstack i)
			 (<ref> variables i))
		      (ARG -1 sp) s)))
	     (<=> inst-pt (<+> inst-pt (<c> 1)))
	     (DECR 1 sp)
	     (<=> s ss)
	     (NEXT inst-pt))
#|
	   (mk-scm-move-i-op is-addi-s (<c> 1) (<c> 1) <+> scm_sum
			   s p variables variables-scm
			   nvar pinned? cnst session middle nstack
			   instructions variables inst-pt sp fp)

	   (mk-scm-move-i-op-x is-addi-x <+> scm_sum
			   s p variables variables-scm
			   nvar pinned? cnst session middle nstack
			   instructions variables inst-pt sp fp)
|#
	   (mk-scm-move   sp-move (<c> 0) s p variables variables-scm
			   nvar pinned? cnst session middle nstack
			   instructions variables inst-pt sp fp)

	   (mk-scm-move   sp-move-s (<c> 1) s p variables variables-scm
			   nvar pinned? cnst session middle nstack
			   instructions variables inst-pt sp fp)

	   (mk-scm-unify   unify   s p variables variables-scm
			   nvar pinned? cnst session middle nstack
			   instructions variables inst-pt sp fp)

	   (mk-scm-unify-2 unify-2 s p variables variables-scm
			   nvar pinned? cnst session middle nstack 
			   instructions variables inst-pt sp fp)

	   (mk-scm-unify-i unify-instruction-2 s p variables variables-scm
			   nvar pinned? cnst session middle
			   nstack  constants 
			   instructions variables inst-pt sp fp)

	   (mk-scm-unify-c unify-constant-2 s p variables variables-scm
			   nvar pinned? cnst session middle 
			   nstack constants 
			   instructions variables inst-pt sp fp)

	   (mk-scm-i       unify-instruction s p variables nstack 
			   constants 
			   instructions variables inst-pt sp fp)

	   (mk-scm-c       unify-constant s p variables nstack constants 
			   instructions variables inst-pt sp fp)

	   (mk-equal-i     equal-instruction s p constants 
			   instructions variables inst-pt sp fp)

	   (mk-equal-c     equal-constant s p constants 
			   instructions variables inst-pt sp fp)


	   (LABEL icons!)
           (PRSTACK sp fp)
	   (<let*> ((x  (ARG -1 sp))
		    (ss (<call> gp_pair_bang x s)))
	     (<if> (<call> scm_is_true ss)
		   (<begin>
		    (INCR 1 sp)
		    (<=> s ss)
		    (<=> (ARG -2 sp) (<call> gp_gp_cdr x s))
		    (<=> (ARG -1 sp) (<call> gp_car    x s))
		    (NEXT inst-pt))
		   (BACKTRACK p instructions inst-pt fp sp)))

	   (LABEL ifkn!)
           (PRSTACK sp fp)
	   (<let*> ((x  (LOOKUP (ARG -1 sp) s))
		    (ss (<call> gp_c_vector_x x (<c> 1) s)))
	      (<if> (TRUE ss)
		    (<let*> ((xb (<call> scm_c_vector_ref x (<c> 0))))
		       (<=> s ss)
		       (<=> (ARG -1 sp) xb)
		       (NEXT inst-pt))
		    (BACKTRACK p instructions inst-pt fp sp)))

	   (LABEL icurly!)
           (PRSTACK sp fp)
	   (<let*> ((x (LOOKUP (ARG -1 sp) s))
		    (q (<call> gp_c_vector_x x (<c> 2) s)))
	     (<if> (TRUE q)
		   (<let*> ((SCM q  (<call> gp_gp_unify 
					    (<scm> #:brace)
					    (<call> scm_c_vector_ref 
						    x (<c> 0))
					    q)))
		     (<if> q
			   (<begin>
			    (<=> s q)
			    (<=> (ARG -1 sp) 
				 (<call> scm_c_vector_ref x (<c> 1)))
			    (NEXT inst-pt))
			   (BACKTRACK p instructions inst-pt fp sp)))
		   (BACKTRACK p instructions inst-pt fp sp)))

	   (LABEL push-instruction)
           (PRSTACK sp fp)
	   (<let> ((SCM x (<*> inst-pt)))
	     (<++> inst-pt)
	     (<=> (<*> sp) x)
	     (INCR 1 sp))
	   (NEXT inst-pt)

	   (LABEL pushv)
           (PRSTACK sp fp)
	   (<let> ((SCM x (<*> inst-pt)))
	     (<++> inst-pt)
	     (<if> (<call> scm_is_false x)
		   (<=> (<*> sp) (<call> gp_mkvar s))
		   (UNPACK-VAR 0 x i pinned? variables variables-scm nvar
			       cnst session middle
		      (<=> (<*> sp) (SVAR-REF fp nstack i))
		      (<=> (<*> sp) (<ref> variables i))))
	     (INCR 1 sp)
	     (NEXT inst-pt))

	   (LABEL push-constant)
           (PRSTACK sp fp)
	   (<let> ((int n (scm->int (<*> inst-pt))))
	     (<++> inst-pt)
	     (<=> (<*> sp) (<ref> constants n))	  
	     (INCR 1 sp))
	   (NEXT inst-pt)

	   (LABEL push-variable-s)
           (PRSTACK sp fp)
	   (<let*> ((int i      (scm->int (<*> inst-pt)))
		    (SCM v      (UNPACK-VAR-A (<c> 1)
				 0 pinned? variables 
				 variables-scm nvar
				 cnst session middle
				 (SVAR-REF fp nstack i)
				 (<ref> variables i))))			 
	     (<=> inst-pt (<+> inst-pt (<c> 1)))
             (FORMAT "push ~a~%" v)
	     (<=> (ARG 0 sp) v)
	     (INCR 1 sp))
	   (NEXT inst-pt)

	   (LABEL push-variable-v)
           (PRSTACK sp fp)
	   (<let*> ((int i      (scm->int (<*> inst-pt)))
		    (SCM v      (UNPACK-VAR-A (<c> 0)
				 0 pinned? variables 
				 variables-scm nvar
				 cnst session middle
				 (SVAR-REF fp nstack i)
				 (<ref> variables i))))			 
	     (<=> inst-pt (<+> inst-pt (<c> 1)))
             (FORMAT "push ~a~%" v)
	     (<=> (ARG 0 sp) v)
	     (INCR 1 sp))
	   (NEXT inst-pt)
	   
	   (LABEL push-2variables-s)
           (PRSTACK sp fp)
	   (<let*> ((ulong  v   (scm->ulong (<*> inst-pt)))
		    (int    v1  (<bit-and> v (<c> #xffff)))
		    (int    v2  (q>> v (<c> 16)))

		    (SCM    x1  (UNPACK-VAR-A (<c> 1)
					      0 pinned? variables 
					      variables-scm nvar
					      cnst session middle
					      (SVAR-REF fp nstack v1)
					      (<ref> variables v1)))
		    (SCM    x2  (UNPACK-VAR-A (<c> 1)
					      0 pinned? variables 
					      variables-scm nvar
					      cnst session middle
					      (SVAR-REF fp nstack v2)
					      (<ref> variables v2))))

                                      
	     (<=> inst-pt (<+> inst-pt (<c> 1))) 
             (FORMAT "push ~a ~a~%" x1 x2)
	     (<=> (ARG 0 sp) x1)
	     (<=> (ARG 1 sp) x2)
	     (INCR 2 sp))
	   (NEXT inst-pt)

	   (LABEL push-2variables-x)
           (PRSTACK sp fp)
	   (<let*> ((ulong  v   (scm->ulong (<*> inst-pt)))
		    (int    v1  (<bit-and> v (<c> #xffff)))
		    (int    v2  (q>> (<bit-and> v (<c> #xffff0000))
				     (<c> 16)))
		    (int    a   (q>> v (<c> 32)))
		    (SCM    x1  (UNPACK-VAR-A (<bit-and> a (<c> 1))
					      0 pinned? variables 
					      variables-scm nvar
					      cnst session middle
					      (SVAR-REF fp nstack v1)
					      (<ref> variables v1)))
		    (SCM    x2  (UNPACK-VAR-A (<bit-and> a (<c> 2))
					      0 pinned? variables 
					      variables-scm nvar
					      cnst session middle
					      (SVAR-REF fp nstack v2)
					      (<ref> variables v2))))

                                      
	     (<=> inst-pt (<+> inst-pt (<c> 1))) 
             (FORMAT "push ~a ~a~%" x1 x2)
	     (<=> (ARG 0 sp) x1)
	     (<=> (ARG 1 sp) x2)
	     (INCR 2 sp))
	   (NEXT inst-pt)

	   (LABEL push-3variables-s)
           (PRSTACK sp fp)
	   (<let*> ((ulong  v   (scm->ulong (<*> inst-pt)))
		    (int    v1  (<bit-and> v (<c> #xffff)))
		    (int    v2  (q>> (<bit-and> v (<c> #xffff0000))
				     (<c> 16)))
		    (int    v3  (q>> v (<c> 32)))

		    (SCM    x1  (UNPACK-VAR-A (<c> 1)
					      0 pinned? variables
					      variables-scm nvar
					      cnst session middle
					      (SVAR-REF fp nstack v1)
					      (<ref> variables v1)))
		    
		    (SCM    x2  (UNPACK-VAR-A (<c> 1)
					      0 pinned? variables
					      variables-scm nvar
					      cnst session middle
					      (SVAR-REF fp nstack v2)
					      (<ref> variables v2)))

		    (SCM    x3  (UNPACK-VAR-A (<c> 1)
					      0 pinned? variables 
					      variables-scm nvar
					      cnst session middle
					      (SVAR-REF fp nstack v3)
					      (<ref> variables v3))))

                                      
	     (<=> inst-pt (<+> inst-pt (<c> 1))) 
             (FORMAT "push ~a ~a ~a~%" x1 x2 x3)
	     (<=> (ARG 0 sp) x1)
	     (<=> (ARG 1 sp) x2)
	     (<=> (ARG 2 sp) x3)
	     (INCR 3 sp))
	   (NEXT inst-pt)

	   (LABEL push-3variables-x)
           (PRSTACK sp fp)
	   (<let*> ((ulong  v   (scm->ulong (<*> inst-pt)))
		    (int    v1  (<bit-and> v (<c> #xffff)))
		    (int    v2  (q>> (<bit-and> v (<c> #xffff0000))
				     (<c> 16)))
		    (int    v3  (q>> (<bit-and> v (<c> #xffff00000000))
				     (<c> 32)))
		    (int    a   (q>> v (<c> 48)))
		    (SCM    x1  (UNPACK-VAR-A (<bit-and> a (<c> 1))
					      0 pinned? variables 
					      variables-scm nvar
					      cnst session middle
					      (SVAR-REF fp nstack v1)
					      (<ref> variables v1)))
		    (SCM    x2  (UNPACK-VAR-A (<bit-and> a (<c> 2))
					      0 pinned? variables 
					      variables-scm nvar
					      cnst session middle
					      (SVAR-REF fp nstack v2)
					      (<ref> variables v2)))
		    (SCM    x3  (UNPACK-VAR-A (<bit-and> a (<c> 4))
					      0 pinned? variables 
					      variables-scm nvar
					      cnst session middle
					      (SVAR-REF fp nstack v3)
					      (<ref> variables v3))))

                                      
	     (<=> inst-pt (<+> inst-pt (<c> 1)))
             (FORMAT "push ~a ~a ~a~%" x1 x2 x3)
	     (<=> (ARG 0 sp) x1)
	     (<=> (ARG 1 sp) x2)
	     (<=> (ARG 2 sp) x3)
	     (INCR 3 sp))
	   (NEXT inst-pt)

	   (LABEL push-variable-scm)
           (PRSTACK sp fp)
	   (<let*> ((SCM x      (<*> inst-pt))
		    (SCM v      (UNPACK-VAR
				  0 x i pinned? variables 
				  variables-scm nvar
				  cnst session middle
				  (SVAR-REF fp nstack i)
				  (<ref> variables i))))

	     (FORMAT "push-scm id: ~a, value: ~a~%" x v)
	     (<=> inst-pt (<+> inst-pt (<c> 1)))
	     (<=> (<*> sp) v)
	     (INCR 1 sp))
	   (NEXT inst-pt)

	   (LABEL pop-variable)
           (PRSTACK sp fp)
	   (<let> ((int n (scm->int (<*> inst-pt))))
	     (<++> inst-pt)
	     (<=> (<ref> variables n) (ARG -1 sp))
	     (DECR 1 sp))
	   (NEXT inst-pt)

	   (LABEL pop)
	   (PRSTACK sp fp)
	   (<let> ((int n (scm->int (<*> inst-pt))))
             (<recur> lp ((int m n))
		(<if> (<not> (<==> m (<c> 0)))
		      (<begin>
		       (DECR 1 sp)
		       (<=> (ARG 0 sp) (<scm> #f))
		       (<next> lp (<-> m (<c> 1))))))
	     (<++> inst-pt)
	     (PRSTACK sp fp)
	     (NEXT inst-pt))

	   (LABEL seek)
	   (PRSTACK sp fp)
	   (<let> ((int n (scm->int (<*> inst-pt))))
	     (<++> inst-pt)
             (INCR n sp)
	     (NEXT inst-pt))

	   (LABEL dup)
	   (PRSTACK sp fp)
	   (<=> (ARG 0 sp) (ARG -1 sp))
	   (INCR 1 sp)
	   (NEXT inst-pt)
	   
	   (LABEL mk-cons)
           (PRSTACK sp fp)
	   (DO-CONS s sp)
	   (NEXT inst-pt)


	   (LABEL mk-fkn)
           (PRSTACK sp fp)
	   (<let*> ((int n (scm->int (<*> inst-pt))))
	      (<++> inst-pt)
	      (<recur> lp ((int i n))
		 (<if> (q<= i (<c> 0))      
		       (<let> ((SCM v (<call> scm_c_make_vector 
					       (<c> 1) (<scm> #f))))
			 (<call> scm_c_vector_set_x v (<c> 0) (ARG -1 sp))
			 (<=> (ARG -1 sp) v))
		       (<begin> 
			(DO-CONS s sp)
			(<next> lp (<-> i (<c> 1)))))))
	   (NEXT inst-pt)

	   (LABEL mk-curly)
           (PRSTACK sp fp)
	   (<let> ((SCM v (<call> scm_c_make_vector (<c> 2) (<scm> #f))))
	     (<call> scm_c_vector_set_x v (<c> 0) (<scm> #:brace))
	     (<call> scm_c_vector_set_x v (<c> 1) (ARG -1 sp))
	     (<=> (ARG -1 sp) v))
	   (NEXT inst-pt)


	   (LABEL icons)
           (PRSTACK sp fp)
	   (<let*> ((SCM x  (ARG -1 sp))
		    (SCM ss (<call> gp_pair x s)))
	     (<if> (TRUE ss)
		   (<begin>
		    (INCR 1 sp)
		    (<=> (ARG -2 sp)  (<call> gp_gp_cdr x s))
		    (<=> (ARG -1 sp)  (<call> gp_car x s))
		    (NEXT inst-pt))
		   (BACKTRACK p instructions inst-pt fp sp)))

	   (LABEL ifkn)
           (PRSTACK sp fp)
	   (<let*> ((x  (<call> gp_gp_lookup (ARG -1 sp) s))
		    (ss (<call> gp_c_vector x (<c> 1) s)))
		(<if> ss
		      (<let> ((xa (<call> scm_c_vector_ref x (<c> 0))))
		       (<=> (ARG -1 sp) xa)
		       (NEXT inst-pt))
		      (BACKTRACK p instructions inst-pt fp sp)))

	   (LABEL icurly)
           (PRSTACK sp fp)
	   (<let*> ((x  (<call> gp_gp_lookup (ARG -1 sp) s))
		    (ss (<call> gp_c_vector x (<c> 2) s)))
		(<if> ss
		      (<if> (<==> (<scm> #:brace)
				  (<call> gp_gp_lookup
					  (<call> scm_c_vector_ref x (<c> 0))
					  s))
			    (<begin>
			     (<=> (ARG -1 sp) 
				  (<call> scm_c_vector_ref x (<c> 1)))
			     (NEXT inst-pt))
			    (BACKTRACK p instructions inst-pt fp sp))
		      (BACKTRACK p instructions inst-pt fp sp)))

	   (mk-scm-binop s plus   (ARITH <+> scm_sum        )
			 inst-pt sp fp)
	   (mk-scm-binop s minus  (ARITH <-> scm_difference )
			 inst-pt sp fp)
	   (mk-scm-binop s band   (ARITH <bit-and> scm_logand)
			 inst-pt sp fp)
	   (mk-scm-binop s bor   (ARITH <bit-or> scm_logior)
			 inst-pt sp fp)
	   (mk-scm-binop s xor (ARITH <bit-xor> scm_logxor) 
			inst-pt sp fp)

	   (mk-scm-binop s modulo (ARITH <%> scm_modulo)
			 inst-pt sp fp)

	   (mk-scm-binop s mul    (MUL) inst-pt sp fp)

	   (mk-scm-binop s shift_l    (SHIFT-L q<< scm_ash)
			 inst-pt sp fp)

	   (mk-scm-binop s shift_r    (SHIFT-R q>> scm_ash)
			 inst-pt sp fp)

	   (mk-scm-binop s divide (<call>    scm_divide     ) 
			 inst-pt sp fp)
#|
	   (mk-scm-ss-binop-s s ss-add-s  (ARITH <+> scm_sum        )
			      inst-pt sp fp nstack)

	   (mk-scm-ss-binop-s s ss-sub-s  (ARITH <-> scm_difference )
			      inst-pt sp fp nstack)
	   (mk-scm-ss-binop-s s ss-and-s  (ARITH <bit-and> scm_logand)
			      inst-pt sp fp nstack)
	   (mk-scm-ss-binop-s s ss-or-s   (ARITH <bit-or> scm_logior)
			      inst-pt sp fp nstack)
	   (mk-scm-ss-binop-s s ss-xor-s (ARITH <bit-xor> scm_logxor) 
			      inst-pt sp fp nstack)

	   (mk-scm-ss-binop-s s ss-mod-s (ARITH <%> scm_modulo)
			      inst-pt sp fp nstack)

	   (mk-scm-ss-binop-s s ss-mul-s (MUL) inst-pt sp fp nstack)

	   (mk-scm-ss-binop-s s ss-lshift-s    (SHIFT-L q<< scm_ash)
			      inst-pt sp fp nstack)

	   (mk-scm-ss-binop-s s ss-rshift-s    (SHIFT-R q>> scm_ash)
			      inst-pt sp fp nstack)

	   (mk-scm-ss-binop-s s ss-div-s (<call>    scm_divide     ) 
			      inst-pt sp fp nstack)

	   (mk-scm-xx-binop-x s xx-add-x  (ARITH <+> scm_sum        )
			      inst-pt sp fp nstack 
			      pinned? variables variables-scm nvar
			      cnst session middle
			      p instructions)

	   (mk-scm-xx-binop-x s xx-sub-x  (ARITH <-> scm_difference )
			      inst-pt sp fp
			      nstack 
			      pinned? variables variables-scm nvar
			      cnst session middle
			      p instructions)

	   (mk-scm-xx-binop-x s xx-and-x  (ARITH <bit-and> scm_logand)
			      inst-pt sp fp nstack 
			      pinned? variables variables-scm nvar
			      cnst session middle
			      p instructions)

	   (mk-scm-xx-binop-x s xx-or-x   (ARITH <bit-or> scm_logior)
			      inst-pt sp fp nstack 
			      pinned? variables variables-scm nvar
			      cnst session middle
			      p instructions)

	   (mk-scm-xx-binop-x s xx-xor-x (ARITH <bit-xor> scm_logxor) 
			      inst-pt sp fp nstack 
			      pinned? variables variables-scm nvar
			      cnst session middle
			      p instructions)

	   (mk-scm-xx-binop-x s xx-mod-x (ARITH <%> scm_modulo)
			      inst-pt sp fp nstack 
			      pinned? variables variables-scm nvar
			      cnst session middle
			      p instructions)

	   (mk-scm-xx-binop-x s xx-mul-x (MUL) inst-pt sp fp nstack 
			      pinned? variables variables-scm nvar
			      cnst session middle
			      p instructions)

	   (mk-scm-xx-binop-x s xx-lshift-x    (SHIFT-L q<< scm_ash)
			      inst-pt sp fp nstack 
			      pinned? variables variables-scm nvar
			      cnst session middle
			      p instructions)

	   (mk-scm-xx-binop-x s xx-rshift-x    (SHIFT-R q>> scm_ash)
			      inst-pt sp fp nstack 
			      pinned? variables variables-scm nvar
			      cnst session middle
			      p instructions)

	   (mk-scm-xx-binop-x s xx-div-x (<call>    scm_divide     ) 
			      inst-pt sp fp nstack 
			      pinned? variables variables-scm nvar
			      cnst session middle
			      p instructions)
|#

	   (mk-scm-binop-l s plus2_1   (ARITH <+> scm_sum        )
			 inst-pt sp fp)

	   (mk-scm-binop-l s minus2_1  (ARITH <-> scm_difference )
			 inst-pt sp fp)

	   (mk-scm-binop-l s mul2_1    (MUL) inst-pt sp fp)

	   (mk-scm-binop-l s div2_1 (<call>    scm_divide     ) 
			 inst-pt sp fp)

	   (mk-scm-binop-l s bitand   (ARITH <bit-and> scm_logand)
			 inst-pt sp fp)
	   (mk-scm-binop-l s bitor   (ARITH <bit-or> scm_logior)
			 inst-pt sp fp)
	   (mk-scm-binop-l s xor1 (ARITH <bit-xor> scm_logxor) 
			 inst-pt sp fp)
 
	   (mk-scm-binop-l s shiftLL    (SHIFT-L q<< scm_ash)
			 inst-pt sp fp)

	   (mk-scm-binop-l s shiftRL    (SHIFT-R q>> scm_ash)
			 inst-pt sp fp)

	   (mk-scm-binop-r s shiftLR    (SHIFT-L q<< scm_ash)
			 inst-pt sp fp)

	   (mk-scm-binop-r s shiftRR    (SHIFT-R q>> scm_ash)
			 inst-pt sp fp)

	   (mk-scm-binop-l s modL (ARITH <%> scm_modulo)
			 inst-pt sp fp)

	   (mk-scm-binop-r s modR (ARITH <%> scm_modulo)
			 inst-pt sp fp)
	   
	   (mk-scm-unop-minus s uminus inst-pt sp fp)

	   (mk-scm-unop s lognot (ARITH-1 <bit-not> scm_lognot) inst-pt sp fp)

	   (LABEL gt)
           (PRSTACK sp fp)
	   (CMP q> scm_gr_p p instructions inst-pt fp sp)
	   (NEXT inst-pt)

#|
	   (LABEL ss-gt)
           (PRSTACK sp fp)
	   (CMP-SS q> scm_gr_p p instructions inst-pt fp sp nstack)
	   (NEXT inst-pt)

	   (LABEL xx-gt)
           (PRSTACK sp fp)
	   (CMP-XX q> scm_gr_p p instructions inst-pt fp sp nstack variables)
	   (NEXT inst-pt)
|#
	   (LABEL gtL)
           (PRSTACK sp fp)
	   (CMP-1 q> scm_gr_p p instructions inst-pt fp sp)
	   (NEXT inst-pt)

	   (LABEL ltL)
           (PRSTACK sp fp)
	   (CMP-1 q< scm_less_p p instructions inst-pt fp sp)
	   (NEXT inst-pt)

	   (LABEL ls)
           (PRSTACK sp fp)
	   (CMP q< scm_less_p p instructions inst-pt fp sp)
	   (NEXT inst-pt)
#|
	   (LABEL ss-lt)
           (PRSTACK sp fp)
	   (CMP-SS q< scm_less_p p instructions inst-pt fp sp nstack)
	   (NEXT inst-pt)

	   (LABEL xx-lt)
           (PRSTACK sp fp)
	   (CMP-XX q< scm_less_p p instructions inst-pt fp sp nstack variables)
	   (NEXT inst-pt)
|#
	   (LABEL ge)
           (PRSTACK sp fp)
	   (CMP q>= scm_geq_p p instructions inst-pt fp sp)
	   (NEXT inst-pt)
#|
	   (LABEL ss-ge)
           (PRSTACK sp fp)
	   (CMP-SS q>= scm_geq_p p instructions inst-pt fp sp nstack)
	   (NEXT inst-pt)

	   (LABEL xx-ge)
           (PRSTACK sp fp)
	   (CMP-XX q>= scm_geq_p p instructions inst-pt fp sp nstack variables)
	   (NEXT inst-pt)
|#
	   (LABEL geL)
           (PRSTACK sp fp)
	   (CMP-1 q>= scm_geq_p p instructions inst-pt fp sp)
	   (NEXT inst-pt)

	   (LABEL le)
           (PRSTACK sp fp)
	   (CMP q>= scm_leq_p p instructions inst-pt fp sp)
	   (NEXT inst-pt)
#|
	   (LABEL ss-le)
           (PRSTACK sp fp)
	   (CMP-SS q>= scm_leq_p p instructions inst-pt fp sp nstack)
	   (NEXT inst-pt)

	   (LABEL xx-le)
           (PRSTACK sp fp)
	   (CMP-XX q>= scm_leq_p p instructions inst-pt fp sp nstack variables)
	   (NEXT inst-pt)
|#
	   (LABEL leL)
           (PRSTACK sp fp)
	   (CMP-1 q>= scm_leq_p p instructions inst-pt fp sp)
	   (NEXT inst-pt)
	   
	   (LABEL eq)
           (PRSTACK sp fp)
	   (CMP <==> scm_equal_p p instructions inst-pt fp sp)
	   (NEXT inst-pt)
#|
	   (LABEL ss-e)
           (PRSTACK sp fp)
	   (CMP-SS <==> scm_equal_p p instructions inst-pt fp sp nstack)
	   (NEXT inst-pt)

	   (LABEL xx-e)
           (PRSTACK sp fp)
	   (CMP-XX <==> scm_equal_p p instructions inst-pt fp sp nstack 
		   variables)
	   (NEXT inst-pt)
|#
	   (LABEL neq)
           (PRSTACK sp fp)
	   (NOTCMP <==> scm_equal_p p instructions inst-pt fp sp)
	   (NEXT inst-pt)
#|
	   (LABEL ss-ne)
           (PRSTACK sp fp)
	   (NOTCMP-SS <==> scm_equal_p p instructions inst-pt fp sp nstack)
	   (NEXT inst-pt)

	   (LABEL xx-ne)
           (PRSTACK sp fp)
	   (NOTCMP-XX <==> scm_equal_p p instructions inst-pt fp sp nstack
		      variables)
	   (NEXT inst-pt)
|#
	   (<label> ret)
	   sp)))

(<define> void init_prolog_vm()  
  (auto-inits)
  (<call> vm-raw (<c> 0) (<c> 0) (<c> 0) (<c> 0) (<c> 0) (<c> 0) (<c> 1)))

(eval-when (compile)
  (hash-for-each
    (lambda (k v)
      (if (not (= v 3))
	  (warn (format #f "symbol ~a is not touched bothways only ~a" k v))))
    touch))

(clambda->c "prolog-vm.c")
