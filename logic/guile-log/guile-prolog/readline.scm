(define-module (logic guile-log guile-prolog readline)
  #:use-module (logic guile-log)
  #:use-module ((logic guile-log umatch) #:select (*current-stack*))
  #:use-module (ice-9 match)
  #:use-module (ice-9 rdelim)
  #:use-module (logic guile-log dynamic-features)
  #:use-module (logic guile-log slask)
  #:use-module ((logic guile-log iso-prolog) #:select (read_term prolog-run))
  #:export (readline readline_term -n- do-readline-warning))

(define p  (lambda x x))
(define cc (lambda x x))
(define s  (fluid-ref *current-stack*))

(define -n-   (make-fluid 0))
(add-fluid-dynamics s p cc -n-)
 
(define do-readline-warning #t)

(cond-expand
  (guile-3.0
   (define readline_
     (guard
      (if (or #t (provided? 'readline))
          (@ (ice-9 readline) readline)
          (begin
            (lambda (pr)
              (when do-readline-warning
                (warn "readline is not installed - read the guile manual about scheme interactive use"))
              (read-line pr (current-input-port))))))))
  (guile-2.0 or guile-2.2
   (define readline_
     (guard
      (if (provided? 'readline))
          (@ (ice-9 readline) readline)
          (begin
            (lambda (pr)
              (when do-readline-warning
                (warn "readline is not installed - read the guile manual about scheme interactive use"))
              (read-line pr (current-input-port))))))))

(define add-history
  (if  (provided? 'readline)
      (@ (ice-9 readline) add-history)
      (lambda (pr) pr)))

(define repl ((@@ (system repl repl) make-repl) 'scheme #f))

(<define> (readline pr t) (<=> t ,(readline_ pr)))
  
 (<define> (readline_term pr cr T O)
  (<let*> ((l  (let lp ((txt (readline_ pr)) (r '()))
		 (let lp2 ((l (string->list txt)) (rr '()) (i 0))
		   (match l
                     ((#\{ . l)
                      (lp2 l (cons #\{ rr) (+ i 1)))
                     ((#\} . l)
                      (lp2 l (cons #\} rr) (- i 1)))
		     ((#\. . l)
                      (if (= i 0)
                          (reverse ((@ (guile) append) (cons #\. rr) r))
                          (lp2 l (cons #\. rr) i)))
		     ((x   . l)
		      (lp2 l (cons x rr) i))
		     (()
		      (lp (readline_ cr) ((@ (guile) append) rr r)))))))
	   (str (let ((str (string-trim (list->string l))))
		  (add-history str)
		  (when (eq? (string-ref str 0) #\()
			(set! str (string-append "do[" str "]")))
		  (when (eq? (string-ref str 0) #\,)
			(string-set! str 0 #\space)
			(string-set! str (- (string-length str) 1) #\space)
			(with-input-from-string (string-trim str)
			  (lambda ()
			     ((@@ (system repl command) meta-command) repl)))
			(set! str "do[#f]"))
		  str))
		  
	   (p   (open-input-string str)))
    (read_term p T O)))
      
    
(<define> (read_yes ch) (<let> ((x (readline_ "more (y/n/a) > "))) (<=> x ch)))
