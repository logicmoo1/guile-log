(define-module (logic guile-log unify)
  #:use-module (srfi srfi-9)
  #:use-module (logic guile-log)
  #:use-module (logic guile-log vlist)  
  #:use-module ((logic guile-log code-load)
		#: select
		(prolog-closure-closure
		 prolog-closure?
		 gp-lookup
		 gp-attvar-raw?
		 gp-car gp-cdr gp-pair?
		 prolog-closure-state
		 prolog-closure-closed?))
  #:use-module (logic guile-log prolog closed)
  #:use-module (logic guile-log prolog namespace)
  #:export (unify))

(define-syntax-rule (aif it p a b) (let ((it p)) (if it a b)))

(define attributeU (@@ (logic guile-log macros) attributeU))

(define-syntax-rule
  (mk-unify name def
	    (x y kind vec build gvar? gid scm? scm-it
	       do-scm-x       do-scm-y
	       do-attvar-x    do-attvar-y
	       do-gvar-x      do-gvar-y
	       do-namespace-x do-namespace-y
	       inc check
	       )
	    (scm ...)
	    (sc  ...)
	    defs ...)
  (define (name s p cc . def)
    defs ...
             
    (<define> (do-prolog-closure lp x y m sc ...)
      (if (eq? (prolog-closure-closure x)
	       (prolog-closure-closure y))
	  (lp (prolog-closure-state x)
	      (prolog-closure-state y) m sc ...)
	  (if (or (prolog-closure-closed? x)
		  (prolog-closure-closed? y))
	      (if (fluid-ref error-when-closed?)
		  (closed-err x y)))))
                             
    (<define> (do-vector lp x y m sc ...)
      (if (and (vector? y)
	       (= (vector-length x)
		  (vector-length y)))
	  (if (check x y m)
	      (<cc> m)
	      (let ((n (vector-length x)))
		(let lpvec ((i 0) (m (inc x y m)))
		  (if (< i n)
		      (<and>
		       (<values>
			(m) (lp (vector-ref x i)
				(vector-ref y i)
				m sc ...))
		       (lpvec (+ i 1) m))
		      (<cc> m)))))
	  <fail>))

    (<define> (do-and lp x y m sc ...)
      (let ((n (vector-length x)))
	(let lp-and ((i 1) (m m))
	  (if (< i n)
	      (<and>
	       (<values> (m) (lp (vector-ref x i) y m sc ...))
	       (lp-and (+ i 1) m))
	      (<cc> m)))))

    (<define> (do-or lp x y m sc ...)
      (let ((n (vector-length x)))
	(let lp-or ((i 1) (m m))
	  (if (< i n)
	      (<or>
	       (lp (vector-ref x i) y m sc ...)
	       (lp-or (+ i 1) m)))
	  <fail>)))

    (<define> (do-cutor lp x y m sc ...)
      (let ((n (vector-length x)))
	(let lp-or ((i 1) (m m))
	  (if (< i n)
	      (<or>
	       (<and> (lp (vector-ref x i) y m sc ...) <cut>)
	       (lp-or (+ i 1) m)))
	  <fail>)))

    (<define> (do-not lp x y m sc ...)
      (<and>
       (<not>
	(lp (vector-ref x 1) y 0 sc ...)
	(<cc> m))))

    ((<lambda> ()
      (let lp ((x x) (y y) (m 0) scm ...)
	(let ((x (gp-lookup x S)) (y (gp-lookup y S)))
	(cond
	 ((eq? x y)
	  (<cc> m))
      
	 ((gp-attvar-raw? x s)
	  (do-attvar-x lp x y m sc ...))
      
	 ((gp-attvar-raw? y s)
	  (do-attvar-y lp x y m sc ...))
      
	 ((and kind (<var?> x))
	  (<set> x y))
      
	 ((and kind (<var?> y))
	  (<set> y x))

	 ((scm? x)
	  (do-scm-x lp x y m sc ...))

	 ((scm? y)
	  (do-scm-y lp x y m sc ...))
	 
	 ((gvar? x)
	  (do-gvar-x x y sc ...))

	 ((gvar? y)
	  (do-gvar-x x y sc ...))
       
	 ((gp-pair? x S)	
	  (if (gp-pair? y S)
	      (if (check x y m)
		  (<cc> m)
		  (<and>
		   (<values> (m) (lp (gp-car x S) (gp-car y S)
				     (inc x y m) sc ...))
		   (<values> (m) (lp (gp-cdr x S) (gp-cdr y S)
				     m sc ...))))
	      <fail>))
      
	 ((gp-pair? y S)
	  <fail>)

	 ((number? x) 
	  (if (equal? x y)
	      (<cc> m)
	      <fail>))

	 ((number? y)
	  <fail>)
       
	 ((procedure? x)
	  (if (procedure? y)
	      (if (equal? x y)
		  (<cc> m)
		  <fail>)
	      (if (string? y)
		  (if (equal? (symbol->string
			       (procedure-name x))
			      y)
		      (<cc> m)
		      <fail>)
		  <fail>)))

	 ((procedure? y)
	  (if (string? x)
	      (if (equal? (symbol->string
			   (procedure-name y))
			  x)
		  (<cc> m)
		  <fail>)
	      <fail>))
              
	 ((vector? x)
	  (let ((a (vector-ref x 0)))
	    (if (keyword? a)
		(cond	       
		 ((eq? a #:and)
		  (do-and lp x y m sc ...))
		 ((eq? a #:or)
		  (do-or lp x y m sc ...))
		 ((eq? a #:cutor)
		  (do-cutor lp x y m sc ...))
		 ((eq? a #:not)
		  (do-not lp x y m sc ...))
		 (else
		  (do-vector lp x y m sc ...)))
		(do-vector lp x y m))))
	       
	 ((vector? y)
	  (let ((a (vector-ref x 0)))
	    (if (keyword? a)
		(cond
		 ((eq? a #:and)
		  (do-and lp x y m sc ...))
		 ((eq? a #:or)
		  (do-or lp x y m sc ...))
		 ((eq? a #:cutor)
		  (do-cutor lp x y m sc ...))
		 ((eq? a #:not)
		  (do-not lp x y m sc ...))
		 (else
		  <fail>))
		<fail>)))
              
	 ((namespace? x)
	  (do-namespace-x lp x y m sc ...))

	 ((namespace? y)
	  (do-namespace-y lp y x m sc ...))

	 ((prolog-closure? x)
	  (if (prolog-closure? y)
	      (if (check x y m)
		  (<cc> m)
		  (do-prolog-closure lp x y (inc x y m) sc ...))
	      <fail>))
       
	 ((prolog-closure? y)
	  <fail>)

	 (else
	  (if (equal? x y)
	      (<cc> m)
	      <fail>)))))) s p cc)))

(define-record-type <scmwrap> 
  (make-scmwrap wrap)
   scmwrap? 
  (wrap get-scmwrap))

(define-record-type <vec>
  (make-vec id)
  vec? 
  (id get-vec-id))
  
(mk-unify unify-vec (x y kind vec)
	  (x y kind vec build gvar? gid scm? scm-it
	     do-scm-x       do-scm-y
	     do-attvar-x    do-attvar-y
	     do-gvar-x      do-gvar-y
	     do-namespace-x do-namespace-y
	     inc check)
	  ((sx #f) (sy #f))
	  (sx sy)
	  
 (define (gvar? x)  (vec? x))
 (define (gid x)    (get-vec-id x))
 (define (scm? x)   (scmwrap? x))
 (define (scm-it x) (get-scmwrap x))
 
 (<define> (do-gvar-x x y sx sy)
   (unify (vector-ref vec (gid x)) (build S sy y) kind))
 
 (<define> (do-gvar-y x y sx sy)
   (unify (build S sx x) (vector-ref vec (gid y)) kind))
 
 (<define> (do-scm-x lp x y m sx sy)
   (if sy
       (unify (scm-it x) y kind)
       (lp (scm-it x) y m #t #f)))

 (<define> (do-scm-y lp x y m sx sy)
   (if sx
       (unify x (scm-it y) kind)
       (lp x (scm-it y) y m #f #t)))

 (<define> (do-attvar-x lp x y m sx sy)
    (attributeU (build S sy y) x kind)
    (<cc> m))

 (<define> (do-attvar-y lp x y m sx sy)
    (attributeU (build S sx x) y kind)
    (<cc> m))

 (<define> (do-namespace-x lp x y m sx sy)
   (let ((s (ns-unify S (build S sx x) (build S sy y) kind)))
     (if s
	 (<with-s> s (<cc> m))
	 <fail>)))

 (<define> (do-namespace-y lp x y m sx sy)
   (let ((s (ns-unify S (build S sy y) (build S sx x) kind)))
     (if s
	 (<with-s> s (<cc> m))
	 <fail>)))

 (define (check x y m) #f)
 (define (inc   x y m) m)
 
 (define (build s p x)
   (if p
       x
       (let lp ((x x))
	 (let ((x (gp-lookup x s)))
	   (cond
	    ((gvar? x)
	     (vector-ref vec (gid x)))
	    ((scm? x)
	     (scm-it x))
	    ((pair? x)
	     (let* ((carx (car x))
		    (cdrx (cdr x))
		    (cary (lp carx))
		    (cdry (lp cdrx)))
	       (if (and (eq? carx cary) (eq? cdrx cdry))
		   x
		   (cons cary cdry))))
	    ((vector? x)
	     (let ((n (vector-length x)))
	       (cond
		((= n 1)
		 (let* ((ax (vector-ref x 0))
			(ay (lp ax)))
		   (if (eq? ax ay)
		       x
		       (vector ay))))
		((= n 2)
		 (let* ((ax (vector-ref x 0))
			(ay (lp ax))
			(bx (vector-ref x 1))
			(by (lp bx)))
		   (if (and (eq? ax ay) (eq? bx by))
		       x
		       (vector ay by))))
		(else
		 (apply vector (lp (vector->list x)))))))
	    (else
	     x)))))))

(mk-unify unify (x y kind)
	  (x y kind vec build gvar? gid scm? scm-it
	     do-scm-x       do-scm-y
	     do-attvar-x    do-attvar-y
	     do-gvar-x      do-gvar-y
	     do-namespace-x do-namespace-y
	     inc check)
	  ()
	  ()
 (<define> (do-scm-x lp x y m) (<cc> #f))
 (<define> (do-scm-y lp x y m) (<cc> #f))
 (<define> (do-gvar-x x y)     (<cc> #f))
 (<define> (do-gvar-y x y)     (<cc> #f))

 (<define> (do-attvar-x lp x y m)
    (attributeU y x kind)
    (<cc> m))

 (<define> (do-attvar-y lp x y m)
    (attributeU x y kind)
    (<cc> m))

 (<define> (do-namespace-x lp x y m)
    (let ((s (ns-unify S x y kind)))
      (if s
	  (<with-s> s (<cc> m))
	  <fail>)))

 (<define> (do-namespace-y lp x y m)
    (let ((s (ns-unify S y x kind)))
      (if s
	  (<with-s> s (<cc> m))
	  <fail>)))

 (define (check x y m) 
      (if (vhash? x)
	  (vhash-assoc (cons x y) m)
	  #f))
 
 (define (inc x y m)
   (if (vhash? m)
       (vhash-cons (cons x y) #t m)
       (if (< m 10)
	   (+ m 1)
	   (vhash-cons (cons x y) #t vlist-null))))
 
 (define (gvar? x)  #f)
 (define (gid x)    (get-vec-id x))
 (define (scm? x)   #f)
 (define (scm-it x) (get-scmwrap x))
 (define (build s p x) x))
		   
