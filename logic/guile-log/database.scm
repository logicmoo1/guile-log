(define-module (logic guile-log database)
  #:use-module (logic guile-log)
  #:use-module (logic guile-log umatch)
  #:use-module (ice-9 match)
  #:use-module (ice-9 pretty-print)
  #:export (matcher-add make-matcher-env  get-matcher-db compile-matcher 
                        db-list  make-fpattern 
                        <fmatch> <db-lookup>))

(define (pp x) (pretty-print (u-scm x)) x)

(define (make-dlink) #(#f #f 0 #f))


(define (dlink-car! dlink !?)
  (let ((r (vector-ref dlink 0)))
    (if r 
        r
        (if !?
            (let ((r (make-dlink)))
              (vector-set! dlink 0 r)
              r)
            #f))))

(define (dlink-cdr! dlink !?)
  (let ((r (vector-ref dlink 1)))
    (if r 
        r
        (if !?
            (let ((r (make-dlink)))
              (vector-set! dlink 1 r)
              r)
            #f))))
  
(define (dlink-vars dlink)
  (vector-ref dlink 2))

(define (get-nlist-from-atom! a dlink)
  (let* ((r (vector-ref dlink 3)))
    (if r
        (let ((x (assoc a r)))
          (if x
              x
              (let* ((y (cons (cons a 0) r)))
                (vector-set! dlink 3 y)
                (car y))))
        (let ((y (cons (cons a 0) '())))
          (vector-set! dlink 3 y)
          (car y)))))

(define (get-fs-from-atoms a dlink)
  (let ((r (vector-ref dlink 3)))
    (if r (let ((w (assoc a r))) (if w (cdr w) 0)))))
        
           
;; car cdr atoms vars
(define (add-linkage e f dlink)
  (match e
    ((x . l)
     (add-linkage x f (dlink-car! dlink #t))
     (add-linkage l f (dlink-cdr! dlink #t)))

    (() 
     'ok)

    ((? gp-var? v)
     (vector-set! dlink 2 (logior f (dlink-vars dlink))))

    (a
     (let ((w (get-nlist-from-atom! a dlink)))
       (set-cdr! w (logior (cdr w) f))))
    'ok))

(define (matcher-add db a l)
  (db 'add a l))
(define (get-matcher-db db)
  (db 'db))
(define (compile-matcher db)
  (db 'make))

(define (make-matcher-env)
  (let ((db (make-dlink))
        (n   1)
        (s  '()))
    (lambda x
      (match x
        (('add a l)
         (set! s (cons l s))
         (add-linkage a n db)
         (set! n (ash n 1))
         (if #f #f))

        (('db) 
         s)
        
        (('make)
         (let ((m (- n 1)))
           (define (g e f db)             
             (if db
                 (match e
                   ((x . l)
                    (logior
                     (g l (g x f (dlink-car! db #f)) (dlink-cdr! db #f))
                     (logand f (vector-ref db 2))))
                     
                    
                   ((? gp-var?)
                    f)

                   (() 
                    f)

                   (a 
                    (logior
                     (logand f (get-fs-from-atoms a db))
                     (logand f (vector-ref db 2)))))
                 f))
           (lambda (a)
             (let ((f (g a m db)))
               (let loop ((k (ash n -1)) (ret '()) (s s))
                 (if (= k 0)
                     ret
                     (if (= 0 (logand k f))                         
                         (loop (ash k -1) ret                (cdr s))
                         (loop (ash k -1) (cons (car s) ret) (cdr s)))))))))))))

(define (make-fpattern x v)
  (define vars '())
  (define (search-vars x)
    (umatch #:mode - (x)
       ((x . l)
        (begin
          (search-vars x)
          (search-vars l)))
       (x (let ((x (gp-lookup x)))
            (if (gp-var? x)
                (let ((a (assoc x vars)))
                  (if (not a)
                      (set! vars (cons (cons x (u-var!)) vars)))))))))
  
  (define (replace x)
    (umatch #:mode - (x)
       ((x . l)
        (cons (replace x) (replace l)))
       (x (let ((x (gp-lookup x)))
            (if (gp-var? x)
                (cdr (assoc x vars))
                x)))))

  (search-vars x)
  (search-vars v)  
  (list (map cdr vars) (replace x) (replace v)))

(define (<fmatch> p cc pat x y)
  (define (clean-up Fr)
    (gp-swap-to-b)
    (gp-unwind Fr)
    (gp-swap-to-a))

  (let ((w (map (lambda (x) (gp-var!)) (car pat))))
    (gp-swap-to-b)
    (let ((Fr (gp-newframe)))
      (let loop ((vs (car pat)) (w w))
        (match vs
          ((v . l)
           (u-set! v (car w))
           (loop l (cdr w)))
          (() 'ok)))
      (gp-swap-to-a)
      (if (gp-unify! (gp-copy (cadr pat)) x)
          (let ((ret (gp-copy (caddr pat))))
            (clean-up Fr)
            (if (gp-unify-raw! y ret)
                (cc p)
                (u-abort p)))
          (begin
            (clean-up Fr)
            (u-abort p))))))

(define (pp X)
  (pretty-print (u-scm X))
  X)

(define (db-list db x)
  (umatch #:mode - (db)
          ((db) (db x))
          (db   (db x))))
                      
(<define> (<db-lookup> db x y)
    (<match> (db)
      ((db)
       (<cut> (db-lookup (db x) x y)))
      (db
       (<cut> (db-lookup (db x) x y)))))

(<define> (db-lookup db x y)
    (<match> (db)
      ((pat   . l)  (<fmatch> pat x y))
      ((_     . l)  (<cut> (db-lookup l x y)))
      (()           (<cut> <fail>))))
