(define-module (logic guile-log slask)
  #:replace (write)
  #:export (expand_term_dcg scm-brace-eval brace-stx-scm set-unify
                            include-meta put_attr new-machine or-ii-f
                            gp-newframe gp-unwind goal-expand compile-prolog
                            get-flag op assertz-source+
                            arg term term-init-variables term-get-variables
                            found-scm #{,}# call op2= *once* once-f
                            number integer source_sink get-module
                            namespace-switch var->code
                            get-double-quote-flag-fkn  nl
                            compile-lambda maybe-call cp ecp guard))

(define (guard x) x)

(define expand_term_dcg #f)

(define (scm-brace-eval s x) x)

(define (brace-stx-scm . l)
  (warn "X is {...}, is not supported as compilation target")
  #f)

(define set-unify #f)

(define include-meta #t)

(define put_attr #f)

(define new-machine #f)

(define or-ii-f #f)

(define (gp-newframe s) 1)
(define (gp-unwind   s) 2)

(define goal-expand #f)

(define compile-prolog #f)

(define get-flag #f)
(define op #f)

(define assertz-source+ #f)

(define arg  #f)
(define term #f)
(define term-init-variables #f)
(define term-get-variables #f)
(define found-scm #f)

(define call (lambda x x))

(define #{,}# #f)

(define op2= #f)

(define *once* #f)

(define once-f #f)

(define number #f)
(define integer #f)
(define source_sink #f)

(define get-module #f)
(define namespace-switch #f)

(define var->code #f)

(define get-double-quote-flag-fkn #f)

(define write (lambda x x))
(define nl    (lambda x x))

(define compile-lambda #f)

(define maybe-call #f)

(define cp  #f)
(define ecp #f)
