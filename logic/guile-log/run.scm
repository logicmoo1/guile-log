(define-module (logic guile-log run)
  #:use-module (ice-9 match)
  #:use-module (logic guile-log macros)
  #:use-module ((logic guile-log slask) #:select (put_attr write))
  #:use-module (logic guile-log umatch)
  #:use-module (logic guile-log guile-prolog copy-term)
  #:use-module (logic guile-log interleave)
  #:use-module (logic guile-log persistance)
  #:export (<stall> <continue> <take> <run> <eval> <ask>
		    <cont-ref> <cont-set!>
		    *gp-var-tr* *kanren-assq*
                    get-continuation continue
		    *init-tr*))
(define *init-tr* (make-fluid (lambda () #f)))
(<define> (build_attribut_representation res tail x)
  (<let> ((x (<lookup> x)))
    (if (gp-attvar-raw? x S)
	(<recur> lp ((res res) 
		     (l (map (lambda (x) 
			       (let ((res
				      (pk 'cstor (attribute-cstor-repr (car x)))))
				 (if res
				     res
				     (let ((s (gp-newframe S)))
				       ((<lambda> ()
					 (<var> (t)
					  ((car x) (cdr x) t write)
					  (<cc> (<scm> t))))
					s
					(lambda () (gp-unwind s) x)
					(lambda (s p t) (gp-unwind s)
                                                (cons (car x) t)))))))
			     (gp-att-data x S))))
	   (if (pair? l)
	       (<let> ((xx (car l)))
		 (if (pair? xx)
		   (<var> (t)
		     (<=> (,(vector (list put_attr x (car xx) (cdr xx))) . t)
			  res)
		      (lp t (cdr l)))
		   (<var> (t)
		    ((car l) res t x)
		    (lp t (cdr l)))))
	       (<=> res tail))))))

(define bar build_attribut_representation)

(define-named-object *cc* (make-fluid #f))

(define (<stall> s p cc . l)
  (fluid-set! *cc* (cons s (cons (cons p l) cc)))
  (fluid-set! *current-stack* s)
  'stalled)

(define (<cont-ref>)
  (fluid-ref *cc*))

(define (<cont-set!> cc)
  (let ((s (car cc)))
    (fluid-set! *cc* cc)
    (fluid-set! *current-stack* s)))

(define (get-continuation)
  (fluid-ref *cc*))

(define (continue *cc*)
  (if (and *cc* (car *cc*))
      (apply (cddr *cc*) (car *cc*) (cadr *cc*))
      'cannot-continue))
  

(define (<take> n) (<continue> n))
(define <continue>
  (case-lambda 
    (()  (let ((*cc* (fluid-ref *cc*)))
           (if (and *cc* (car *cc*))
               (apply (cddr *cc*) (car *cc*) (cadr *cc*))
               'cannot-continue)))
    ((n) (let ((*cc* (fluid-ref *cc*)))
           (if (and *cc* (integer? n) (not (car *cc*)))
               ((cdr *cc*) n)
               'cannot-continue-and-take-n)))))

(define (make-empty-s) (fluid-ref *current-stack*))

(define-syntax <eval> 
  (syntax-rules ()
    ((_ (v ...) code fini cc)
     (let* ((s  (fluid-ref *current-stack*))
	    (ss (gp-newframe s)))
       (<eval> ss (v ...) code fini cc)))

    ((_ s (v ...) code fini cc)
     (let* ((f (gp-newframe s))
	    (v (gp-var! f)) ...)
       (let ((fi fini)
             (c  cc))
         (<with-guile-log> (f fi c)
           code))))))
   
(define *gp-var-tr* (make-fluid 'v))
                                      
(define tr
  (case-lambda 
    ((x s single?) 
     (tr (fluid-ref *gp-var-tr*) x s single?))

    ((pre x s single?)
     (define init ((fluid-ref *init-tr*)))
     (define r ((<lambda> ()
		 (<var> (xx yy) 
		    (copy_term x xx yy) 
		    (<cc> (cons xx yy))))
		s (lambda x #f) 
		(lambda (ss p res)
		  (set! s ss)
		  (set! res (gp->scm res s))
		  (if (null? (cdr res))
		      (car res)
		      (if single?
			  (set! x (cons (car res) (cdr res)))
			  (set! x (cons (car res) (cdr res))))))))
     (define vars (make-hash-table))
     (define (get-var-number v)
       (let ((r (hashq-ref vars v #f)))
	 (if r 
	     (values r #t)
	     (let ((r n))
	       (let ((k (string->symbol 
			 (format #f "~a~a" pre n))))
		 (set! n (+ n 1))
		 (hashq-set! vars v k)
		 (values k #f))))))

     (define n 0)
     (define tail '())
     (let loop ((x (gp->scm x s)))
       (match x
         ((x . l)
          (cons (loop x) (loop l)))
         (x
	  (let ((x (gp-lookup x s)))
	    (cond
	     ((gp-attvar-raw? x s)
	      (get-var-number x))

	     ((gp-var? x s)
	      (get-var-number x))

	     (else
	      x)))))))))
  
(define *kanren-assq* #f)

(define-syntax-rule (mk<run> <run> <run*>)
  (define-syntax-rule (<run> . l)
    (let ((fr (gp-newframe (fluid-ref *current-stack*))))
      (dynamic-wind
        (lambda () 
          #f)
        (lambda () 
          (let ((r (<run*> . l)))
            #;(gp-unwind fr)
            r))
        (lambda ()
          #f #;(gp-unwind fr))))))

(mk<run> <run> <run*>)

(define-syntax <run*>
  (syntax-rules (*)
    ((_ (v) code ...)
     (let* ((fr1  (gp-newframe (fluid-ref *current-stack*)))
	    (fr   (gp-newframe fr1)))
       (with-fluids ((*current-stack* fr))
       (if *kanren-assq*
           (gp-logical++))
       (let-with-lr-guard fr wind lg rg ((ret '()))
         (lg fr
           (<eval> (v)
           (<and> code ...)
           (lambda x 
             (gp-unwind-tail fr1)
             (reverse ret))
           (lambda (s p)
	     (let ((res (gp-var! s)))
	       (set! ret (cons (tr (gp->scm v s) s #t) ret))
	       (p)))))))))

    ((_ (v ...) code ...)
     (let* ((fr1 (gp-newframe (fluid-ref *current-stack*)))
	    (fr  (gp-newframe fr1)))
       (with-fluids ((*current-stack* fr))
       (if *kanren-assq*
           (gp-logical++))
       (let-with-lr-guard fr wind lg rg ((ret '()))
         (lg fr
         (<eval> (v ...)
           (<and> code ...)
           (lambda x 
             (let ((r ret))
               (set! ret '())
               (gp-unwind-tail fr1)
               (reverse r)))
           (lambda (s p)
             (set! ret (cons (tr (gp->scm (list v ...) s) s #f) ret))
             (p))))))))
               
    
    ((_ * . l)  (<run> . l))

    ((_ m (v) code ...)
     (let* ((fr1   (gp-newframe (fluid-ref *current-stack*)))
	    (fr    (gp-newframe fr1)))
       (with-fluids ((*current-stack* fr))
       (if *kanren-assq*
           (gp-logical++))
       (let-with-lr-guard fr wind lg rg ((n m) (ret '()))
	 (lg fr
         (<eval> (v)
           (<and> code ...)
           (lambda x 
             (let ((r ret))
               (set! n    0)
	       (set! ret '())
               (gp-unwind-tail fr1)
               (reverse r)))
           (lambda (s p)
             (if (= n 0)
                 (let ((r (reverse ret)))
                   (fluid-set! *cc* (cons #f (lambda (mm) 
					       (set! n mm)
					       (set! ret '())
					       (p))))
                   r)
                 (begin
                   (set! n (- n 1))
		   (set! ret (cons (tr (gp->scm v s) s #t) ret))
                   (if (= n 0)
                       (let ((r (reverse ret)))
                         (fluid-set! *cc* (cons #f (lambda (mm) 
						     (set! n mm)
						     (set! ret '())
						     (p))))
                         r)
                       (p)))))))))))
                 

    ((_ m (v ...) code ...)
     (let* ((fr1   (gp-newframe (fluid-ref *current-stack*)))
	    (fr2   (gp-newframe fr1)))
       (with-fluids ((*current-stack* fr2))
       (if *kanren-assq*
           (gp-logical++))
       (let-with-lr-guard fr2 wind lg rg ((n m) (ret '()))
	 (lg fr2
	 (<eval> (v ...)
           (<and> code ...)
           (lambda  x 
             (let ((r ret))
               (set! n    0)
	       (set! ret '())
               (gp-unwind-tail fr1)
               (reverse r)))
           (lambda (s p)
             (if (= n 0)
                 (let ((r (reverse ret)))
                   (fluid-set! *cc* (cons #f (lambda (mm) 
					       (set! n mm)
					       (set! ret '())
					       (p))))
                   r)
                 (begin
                   (set! n (- n 1))
		   (set! ret (cons (tr (list (gp->scm v s) ...) s #f) ret))
                   (if (= n 0)
                       (let ((r (reverse ret)))
                         (fluid-set! *cc* (cons #f (lambda (mm) 
						     (set! n mm)
						     (set! ret '())
						     (p))))
                         r)
                       (p)))))))))))))

(define-syntax <ask*>
  (syntax-rules ()
    ((_ code ...)     
     (let ((cc (lambda (s p) #t))
           (p  (lambda ()  #f)))           
       (let ((s  (make-empty-s)))
         (<with-guile-log> (s p cc)
           (<and> code ...)))))))

(mk<run> <ask> <ask*>)

(<define> (build_attribut_representation res tail x)
  (<let> ((x (<lookup> x)))
    (if (gp-attvar-raw? x S)
	(<recur> lp ((res res) 
		     (l (map (lambda (y) 
			       (let ((res
				      (attribute-cstor-repr (car y))))
				 (if res
				     res
				     (let* ((s (gp-newframe S)))
				       ((<lambda> ()
					  (<var> (t)
					    ((car y) x (cdr y) t write)
					    (<cc> (list #:pr (<scm> t)))))
					s
					(lambda () (gp-unwind s) y)
					(lambda (s p t)
					  (gp-unwind s)
					  t))))))
			     (gp-att-data x S))))
		 (if (pair? l)
		     (<let> ((xx (car l)))
		        (if (pair? xx)
			    (<var> (t)
                              (<=> (,(if (and (pair? xx)
                                              (eq? (car xx) #:pr))
                                         (cadr xx)
                                         (vector 
                                          (list put_attr
                                                x (car xx) (cdr xx))))
                                    . t)
                                   res)
                              (lp t (cdr l)))
			    (<var> (t)
			      ((car l) res t x)
			      (lp t (cdr l)))))
		     (<=> res tail))))))

(set! (@ (logic guile-log macros) build_attribut_representation)
  build_attribut_representation)
