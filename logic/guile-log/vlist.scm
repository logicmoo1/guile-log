(define-module (logic guile-log vlist)
  #:use-module (logic guile-log code-load)
  #:export (vhash-ref vhashq-ref vhasha-ref vhash-fold-new)
  #:re-export(vlist? vlist-cons vlist-head vlist-tail vlist-null?
                  vlist-null list->vlist vlist-ref vlist-set!
		  vlist-fold vlist-fold-right 
                  vlist-last-val vlist-cons* vlist-pair?
                  test-vlist
		  vlist-drop vlist-take
		  vlist-length  vlist-map
		  ;vlist-unfold vlist-unfold-right vlist-append
		  vlist-reverse vlist-filter vlist-delete vlist->list
		  vlist-for-each

                  vlist-truncate! vhash-truncate!
                  vlist-thread-inc vlist-new-thread
                  vlist-refcount-- vlist-refcount++

                  vhash? vhash-cons vhash-consq vhash-consv vhash-consa
                  vhash-assoc vhash-assq vhash-assa vhash-assv
                  vhash-delete vhash-delq vhash-delv
                  vhash-fold vhash-fold-right
                  vhash-set! vhash-setq! vhash-setv!
                 ;vhash-fold* vhash-foldq* vhash-foldv*
                 ;alist->vhash

                  vhash->assoc
		  block-growth-factor init-block-size))

(define (vhash-ref h x e)
  (if (vhash? h)
      (let ((r (vhash-assoc x h)))
	(if r
	    (cdr r)
	    e))
      e))

(define (vhashq-ref h x e)
  (if (vhash? h)
      (let ((r (vhash-assq x h)))
	(if r
	    (cdr r)
	    e))
      e))

(define (vhasha-ref h x e)
  (if (vhash? h)
      (let ((r (vhash-assa x h)))
	(if r
	    (cdr r)
	    e))
      e))

(define (vhash-fold-new f s old new)
  (let ((state-new (struct-ref new 0))
	(I0        (logand (struct-ref new 1) #xfffffff)))
    (define (stop cur new)
      (if (eq? cur new)
	  I0
	  0))

    (let lp ((state-cur (struct-ref old 0)) 
	     (I         (logand (struct-ref new 1)
				 #xfffffff))
	     (s         s))
      (let ((size (logand (vector-ref state-cur 3) #xfffffff))
	    (vec  (vector-ref state-cur 0)))
	(if (eq? size 0)
	    s
	    (let lp2 ((i0 (stop state-cur state-new))
		      (i  I) 
		      (s  s))
	      (if (>= i i0)
		  (lp2 i0 (- i 1) 
		       (f (vector-ref vec i)  (vector-ref vec (+ i size)) s))
		  (if (eq? state-cur state-new)
		      s
		      (lp (vector-ref state-cur 1)
			  (logand (vector-ref state-cur 2)
				  #xfffffff)
			  s)))))))))

(fluid-set! init-block-size 16)
