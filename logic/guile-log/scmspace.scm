(define-module (logic guile-log scmspace)
  #:export (export-scm))

(define (export-scm)
  (let ((l '())
	(m (current-module)))
    (module-for-each (lambda (n v) 
		       (let ((s (symbol-append 'scm- n)))
			 (module-add! m s v #;(variable-ref v))
			 (set! l (cons s l))))
	  (resolve-module '(guile)))
    (module-export! m l)))

  
  
