(define-module (logic guile-log indexer)
  #:use-module (logic guile-log code-load)
  #:re-export (get-index-set get-index-test get-index-tags))
