(define-module (logic guile-log fstream)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-9 gnu)
  #:export (finsert finsert-l fwrite fwrite-l feof? feof-l?
		    fread fread-l fseek fseek-l
		    fpeek fpeek-l
		    rw-fstream-from-file  
		    rw-fstream-from-port
		    rw-fstream-from-string
		    r-fstream-from-file   
		    r-fstream-from-port
		    r-fstream-from-string
		    rw-fstream-from-file-append  
		    rw-fstream-from-port-append  
		    w-fstream-from-file-append  
		    w-fstream-from-port-append  
		    w-fstream-from-string-append
		    rw-fstream-from-string-append
		    feof freadline freadline-l
		    make-empty-rw-fstream
		    make-empty-r-fstream
		    make-empty-w-fstream
		    fstream-rw?
		    fstream-r?
		    fstream-w?
		    fstream?
		    write-fstream
		    rw-fstream-meta
		    r-fstream-meta
		    w-fstream-meta
		    fposition
		    r->rw-fstream
		    rw-fstream-n
		    fmove fmove-l
		    ))

;; Functional port or stream
(define-immutable-record-type <fstream-rw>
  (make-fstream-rw n m l r meta)
  fstream-rw?
  (n     rw-fstream-n     set-rw-fstream-n)
  (m     rw-fstream-m     set-rw-fstream-m)
  (l     rw-fstream-l     set-rw-fstream-l)
  (r     rw-fstream-r     set-rw-fstream-r)
  (meta  rw-fstream-meta  set-rw-fstream-meta))

(define-immutable-record-type <fstream-r>
  (make-fstream-r m r meta)
  fstream-r?
  (m     r-fstream-m     set-r-fstream-m)
  (r     r-fstream-r     set-r-fstream-r)
  (meta  r-fstream-meta  set-r-fstream-meta))

(define-immutable-record-type <fstream-w>
  (make-fstream-w n l meta)
  fstream-w?
  (n     w-fstream-n     set-w-fstream-n)
  (l     w-fstream-l     set-w-fstream-l)
  (meta  w-fstream-meta  set-w-fstream-meta))

(define (fstream? x)
  (or (fstream-rw? x) (fstream-r? x) (fstream-w? x)))

(define make-empty-rw-fstream
  (case-lambda
   (()
    (make-fstream-rw 0 0 '() '() '((#:open . #t) (#:file-name . #f))))
   ((str)
    (make-fstream-rw 0 0 '()  '() `((#:open . #t) (#:file-name . ,str))))))

(define make-empty-r-fstream
  (case-lambda
   (()
    (make-fstream-r 0 '() '((#:open . #t) (#:file-name . #f))))
   ((str)
    (make-fstream-r 0 '() `((#:open . #t) (#:file-name . ,str))))))


(define make-empty-w-fstream
  (case-lambda
   (()
    (make-fstream-w 0 '() '((#:open . #t) (#:file-name . #f))))
   ((str)
    (make-fstream-w 0 '() `((#:open . #t) (#:file-name . ,str))))))
   
(define (finsert stream x)
  (cond
   ((fstream-rw? stream)
    (set-fields stream 
      ((rw-fstream-n) (+ 1 (rw-fstream-n stream)))
      ((rw-fstream-l) (cons x (rw-fstream-l stream)))))
   ((fstream-w? stream)
    (set-fields stream 
      ((w-fstream-n) (+ 1 (w-fstream-n stream)))
      ((w-fstream-l) (cons x (w-fstream-l stream)))))
   (else    
    (error "no rw/w fstream in finsert" stream))))

(define (fwrite stream x)
  (cond
   ((fstream-rw? stream)
    (set-fields stream
     ((rw-fstream-n) (+ 1 (rw-fstream-n stream)))
     ((rw-fstream-l) (cons x (rw-fstream-l stream)))
     ((rw-fstream-m) (if (null? (rw-fstream-r stream))
		      0
		      (- (rw-fstream-m stream) 1)))
     ((rw-fstream-r) (if (null? (rw-fstream-r stream))
		      '()
		      (cdr (rw-fstream-r stream))))))
   ((fstream-w? stream)
    (set-fields stream
      ((w-fstream-n) (+ 1 (w-fstream-n stream)))
      ((w-fstream-l) (cons x (w-fstream-l stream)))))

   (else
    (error "no rw/w fstream in fwrite" stream))))

(define (finsert-l stream x)
  (cond
   ((fstream-rw? stream)
      (set-fields stream
	 ((rw-fstream-m) (+ 1 (rw-fstream-m stream)))
	 ((rw-fstream-r) (cons x (rw-fstream-r stream)))))
   ((fstream-r? stream)
      (set-fields stream
	 ((r-fstream-m) (+ 1 (r-fstream-m stream)))
	 ((r-fstream-r) (cons x (r-fstream-r stream)))))
   (else
    (error "no rw/l fstream in finsert-l" stream))))

(define (fwrite-l stream x)
  (cond
   ((fstream-rw? stream)
      (set-fields stream
	 ((rw-fstream-m) (+ 1 (rw-fstream-m stream)))
	 ((rw-fstream-r) (cons x (rw-fstream-r stream)))
	 ((rw-fstream-n) (if (null? (rw-fstream-l stream))
			  0
			  (- (rw-fstream-n stream) 1)))
	 ((rw-fstream-l) (if (null? (rw-fstream-l stream))
			  '()
			  (cdr (rw-fstream-l stream))))))

   ((fstream-r? stream)
      (set-fields stream
	 ((r-fstream-m) (+ 1 (r-fstream-m stream)))
	 ((r-fstream-r) (cons x (r-fstream-r stream)))))

   (else
    (error "no rw/r fstream in finsert-l" stream))))

(define (feof? stream)
  (cond
   ((fstream-rw? stream)
    (null? (rw-fstream-r stream)))
   ((fstream-r? stream)
    (null? (r-fstream-r stream)))
   ((fstream-r? stream)
    #t)
   (else
    (error "no stream in feof?" stream))))

(define (feof-l? stream)
  (cond
   ((fstream-rw? stream)
    (null? (rw-fstream-l stream)))
   ((fstream-w? stream)
    (null? (w-fstream-l stream)))
   ((fstream-r? stream)
    #t)
   (else
    (error "no rw/r/w fstream in feof-l?" stream))))

(define feof (list 'eof))

(define (fread stream)
  (cond
   ((fstream-rw? stream)
    (if (feof? stream)
	feof
	(values
	 (set-fields stream 
	   ((rw-fstream-n) (+ 1 (rw-fstream-n stream)))
	   ((rw-fstream-l) (cons (car (rw-fstream-r stream))
			      (rw-fstream-l stream)))
	   ((rw-fstream-r) (cdr (rw-fstream-r stream)))
	   ((rw-fstream-m) (- (rw-fstream-m stream) 1)))
	 (car (rw-fstream-r stream)))))

   ((fstream-r? stream)
    (if (feof? stream)
	feof
	(let ((res (car (r-fstream-r stream))))
	  (values
	   (set-fields stream 
	     ((r-fstream-r) (cdr (r-fstream-r stream)))
	     ((r-fstream-m) (- (r-fstream-m stream) 1)))
	   res))))

    (error "no rw/r fstream in fread" stream)))

(define (fread-l stream)
  (cond
   ((fstream-rw? stream)
    (if (feof-l? stream)
	feof
	(values 
	 (set-fields stream
	   ((rw-fstream-m) (+ 1 (rw-fstream-m stream)))
	   ((rw-fstream-r) (cons (car (rw-fstream-l stream))
			      (rw-fstream-r stream)))
	   ((rw-fstream-l) (cdr (rw-fstream-l stream)))
	   ((rw-fstream-n) (- (rw-fstream-n stream) 1)))
	 (car (rw-fstream-l stream)))))

   ((fstream-w? stream)
    (if (feof-l? stream)
	feof
	(let ((res (car (w-fstream-l stream))))
	  (values 
	   (set-fields stream
	     ((w-fstream-l) (cdr (w-fstream-l stream)))
	     ((w-fstream-n) (- (w-fstream-n stream) 1)))
	   res))))

   (else
    (error "no rw/w fstream in fread-l" stream))))

(define (fpeek stream)
  (cond
   ((fstream-rw? stream) 
      (if (null? (rw-fstream-r stream))
	  feof 
	  (car (rw-fstream-r stream))))
   ((fstream-r? stream)
    (if (null? (r-fstream-r stream))
	feof 
	(car (r-fstream-r stream))))
   (else
    (error "no rw/r fstream in fpeek" stream))))

(define (fpeek-l stream)
  (cond
   ((fstream-rw? stream)
    (if (null? (rw-fstream-l stream))
	feof 
	(car (rw-fstream-l stream))))
   ((fstream-w? stream)
    (if (null? (w-fstream-l stream))
	feof 
	(car (w-fstream-l stream))))
   (else
    (error "no rw/w fstream in fpeek-l" stream))))

(define (fseek stream n)
  (cond 
   ((fstream-rw? stream)
    (cond
     ((> (rw-fstream-n stream) n)
      (if (feof-l? stream)
	  stream
	  (fseek (fread-l stream) n)))
     ((= (rw-fstream-n stream) n)
      stream)
     (else
      (fseek-l stream (- (rw-fstream-m stream) (- n (rw-fstream-m stream)))))))
   
   ((fstream-w? stream)
    (cond
     ((> (w-fstream-n stream) n)
      (if (feof-l? stream)
	  stream
	  (fseek (fread-l stream) n)))
     ((= (w-fstream-n stream) n)
      stream)
     (else
      stream)))
   
   (else
    (error "no rw/w fstream in fseek" stream))))

(define (fmove stream n)
  (cond 
   ((< n 0)
    (fmove-l stream (- n)))

   ((or (fstream-rw? stream) (fstream-r? stream))
    (cond
     ((> n 0)
      (if (feof? stream)
	  stream
	  (fmove (fread stream) (- n 1))))
     ((= n 0)
      stream)))    
   
   (else
    (error "no rw/w fstream in fseek" stream))))

(define (fmove-l stream n)
  (cond 
   ((< n 0)
    (fmove stream (- n)))

   ((or (fstream-rw? stream) (fstream-w? stream))
    (cond
     ((> n 0)
      (if (feof? stream)
	  stream
	  (fmove-l (fread-l stream) (- n 1))))
     ((= n 0)
      stream)))
   
   (else
    (error "no rw/w fstream in fseek" stream))))


(define (fseek-l stream m)
  (cond
   ((fstream-rw? stream)
    (cond
     ((> (rw-fstream-m stream) m)
      (if (feof? stream)
	  stream
	  (fseek-l (fread stream) m)))
     ((= (rw-fstream-m stream) m)
      stream)
     (else
      (fseek stream (- (rw-fstream-n stream) (- m (rw-fstream-n stream)))))))
   ((fstream-r? stream)
    (cond
     ((> (r-fstream-m stream) m)
      (if (feof? stream)
	  stream
	  (fseek-l (fread stream) m)))
     ((= (r-fstream-m stream) m)
      stream)
     (else
      stream)))

   (else
    (error "no rw/r fstream in fseek-l" stream))))

	
(define (rw->r stream)
  (make-fstream-r (rw-fstream-m    stream) 
		  (rw-fstream-r    stream) 
		  (rw-fstream-meta stream)))

(define (rw->w stream)
  (make-fstream-w (rw-fstream-n    stream) 
		  (rw-fstream-l    stream) 
		  (rw-fstream-meta stream)))

(define (rw-fstream-from-port port)
  (with-input-from-port port
    (lambda ()
      (let lp ((ch (read-char)) (s (make-empty-rw-fstream `((#:file-name . 
									 ,port)
							    (#:open      . #t))
							  )))
	(if (eof-object? ch)
	    (fseek s 0)
	    (lp (read-char) (fwrite s ch)))))))

(define (rw-fstream-from-port-append port)
  (with-input-from-file port
    (lambda ()
      (let lp ((ch (read-char)) (s (make-empty-rw-fstream `((#:file-name . 
									 ,port)
							    (#:open      . #t))
							  )))
	(if (eof-object? ch)
	    s
	    (lp (read-char) (fwrite s ch)))))))

(define (r-fstream-from-port port)
  (rw->r (rw-fstream-from-port port)))

(define (w-fstream-from-port-append port)
  (rw->w (rw-fstream-from-port-append port)))

(define (rw-fstream-from-file str)
  (with-input-from-file str
    (lambda ()
      (let lp ((ch (read-char)) (s (make-empty-rw-fstream `((#:file-name . ,str)
							    (#:open      . #t))
							  )))
	(if (eof-object? ch)
	    (fseek s 0)
	    (lp (read-char) (fwrite s ch)))))))

(define (rw-fstream-from-file-append str)
  (with-input-from-file str
    (lambda ()
      (let lp ((ch (read-char)) (s (make-empty-rw-fstream `((#:file-name . ,str)
							    (#:open      . #t))
							  )))
	(if (eof-object? ch)
	    s
	    (lp (read-char) (fwrite s ch)))))))

(define (r-fstream-from-file str)
  (rw->r (rw-fstream-from-file str)))

(define (w-fstream-from-file-append str)
  (rw->w (rw-fstream-from-file-append str)))

(define (rw-fstream-from-string str)
  (with-input-from-string str
    (lambda ()
      (let lp ((ch (read-char)) (s (make-empty-rw-fstream `((#:file-name . #f)
							    (#:open      . #t))
							  )))
	(if (eof-object? ch)
	    (fseek s 0)
	    (lp (read-char) (fwrite s ch)))))))

(define (rw-fstream-from-string-append str)
  (with-input-from-string str
    (lambda ()
      (let lp ((ch (read-char)) (s (make-empty-rw-fstream `((#:file-name . #f)
							    (#:open      . #t))
							  )))
	(if (eof-object? ch)
	    (lp (read-char) (fwrite s ch)))))))

(define (r-fstream-from-string str)
  (rw->r (rw-fstream-from-string str)))

(define (w-fstream-from-string-append str)
  (rw->w (rw-fstream-from-string-append str)))

(define (freadline stream)
  (cond
   ((fstream-rw? stream)
    (let ((l    (rw-fstream-l stream))
	  (r    (rw-fstream-r stream))
	  (n    (rw-fstream-n stream))
	  (m    (rw-fstream-m stream))
	  (meta (rw-fstream-meta stream)))
      (let lp ((l l) (r r) (n n) (m m) (res '()))
	(if (null? r)
	    (values
	     (make-fstream-rw n m l r meta)
	     (reverse res))	  
	    (let ((val (car r)))
	      (if (eq? val #\newline)
		  (values
		   (make-fstream-rw (+ n 1) (- m 1) (cons val l) (cdr r) meta)
		   (reverse res))
		  (lp (cons val l) (cdr r) (+ n 1) (- m 1)
		      (cons val res))))))))

   ((fstream-r? stream)
    (let ((r    (r-fstream-r stream))
	  (m    (r-fstream-m stream))
	  (meta (r-fstream-meta stream)))
      (let lp ((r r)  (m m) (res '()))
	(if (null? r)
	    (values
	     (make-fstream-r m r meta)
	     (reverse res))	  
	    (let ((val (car r)))
	      (if (eq? val #\newline)
		  (values
		   (make-fstream-r (- m 1) (cdr r) meta)
		   (reverse res))
		  (lp (cdr r) (- m 1)
		      (cons val res))))))))
    (else
     (error "no rw/r fstream in freadline" stream))))
  
(define (freadline-l stream)
  (cond
   ((fstream-w? stream)
      (let ((l    (w-fstream-l stream))
	    (n    (w-fstream-n stream))
	    (meta (w-fstream-meta stream)))
	(let lp ((l l) (n n) (res '()))
	  (if (null? l)
	      (values
	       (make-fstream-w n l meta)
	       (reverse res))	  
	      (let ((val (car l)))
		(if (eq? val #\newline)
		    (values
		     (make-fstream-w (- n 1) (cdr l) meta)
		     (reverse res))
		    (lp (cdr l) (- n 1)
			(cons val res))))))))
   ((fstream-rw? stream)
      (let ((l    (rw-fstream-l stream))
	    (r    (rw-fstream-r stream))
	    (n    (rw-fstream-n stream))
	    (m    (rw-fstream-m stream))
	    (meta (rw-fstream-meta stream)))
	(let lp ((l l) (r r) (n n) (m m) (res '()))
	  (if (null? l)
	      (values
	       (make-fstream-rw n m l r meta)
	       (reverse res))	  
	      (let ((val (car l)))
		(if (eq? val #\newline)
		    (values
		     (make-fstream-rw (- n 1) (+ m 1) (cdr l) (cons val r) meta)
		     (reverse res))
		    (lp (cdr l) (cons val r) (- n 1) (+ m 1)
			(cons val res))))))))
   (else
    (error "no rw/w fstream in freadline-l" stream))))

(define (write-fstream s)
  (cond
   ((fstream-rw? s)
    (let ((str ((assq #:file-name (rw-fstream-meta s)))))
      (if str
	  (with-output-to-file str
	    (lambda ()
	      (let lp ((l (rw-fstream-r (fseek s 0))))
		(if (pair? l)
		    (begin
		      (format #t "~a" (car l))
		      (lp (cdr l)))
		    #t)))))))
   ((fstream-w? s)
    (let ((str ((assq #:file-name (w-fstream-meta s)))))
      (if str
	  (with-output-to-file str
	    (lambda ()
	      (let lp ((l (reverse (w-fstream-l s))))
		(if (pair? l)
		    (begin
		      (format #t "~a" (car l))
		      (lp (cdr l)))
		    #t)))))))
   (else
    #t)))

(define (fposition s)
  (cond
   ((fstream-rw? s)
    (rw-fstream-n s))
   ((fstream-w? s)
    (rw-fstream-n s))
   ((fstream-r? s)
    0)))

(define (r->rw-fstream fstream)
  (make-fstream-rw
   0 
   (r-fstream-m    fstream)
   '()
   (r-fstream-r    fstream)
   (r-fstream-meta fstream)))
