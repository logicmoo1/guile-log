(define-module (persist persistance-guile-log)
  #:use-module  (persist persistance)
  #:use-module  (persist primitive)
  #:use-module  (logic guile-log procedure-properties)
  #:use-module  (logic guile-log code-load)
  #:use-module  (logic guile-log vlist)
  #:re-export (make-persister 
               load-persists
               save-persists
               persist-set!
               persist-ref

               make-shallow
               associate-getter-setter

               name-object
               name-object-deep
               define-named-object
               define-shallow-object
               define-fluid-object

               pcopyable?
               deep-pcopyable?
               pcopy
               deep-pcopy

               repersist
               ))

(set! (@@ (persist slask) set-object-property!)
  set-object-property!)

(set! (@@ (persist slask) object-property)
  object-property)

(set! (@@ (persist slask) set-procedure-property!)
  set-procedure-property!)

(set! (@@ (persist slask) procedure-property)
  procedure-property)

(set! (@@ (persist slask) procedure-name)
  procedure-name)
  
(set! (@@ (persist slask) gp-cons?)
  (@@ (logic guile-log code-load) gp-cons?))

(set! (@@ (persist slask) gp-cons-1)
  (@@ (logic guile-log code-load) gp-cons-ref-1))

(set! (@@ (persist slask) gp-cons-2)
  (@@ (logic guile-log code-load) gp-cons-ref-2))

(set! (@@ (persist slask) gp-make-pure-cons)
  (@@ (logic guile-log code-load) gp-make-pure-cons))

(set! (@@ (persist slask) gp-cons-set-1!)
  (@@ (logic guile-log code-load) gp-cons-set-1!))

(set! (@@ (persist slask) gp-cons-set-2!)
  (@@ (logic guile-log code-load) gp-cons-set-2!))

(set! (@@ (persist slask) curstack?)
  (lambda (x)
    (eq? x (fluid-ref
            ((@@ (logic guile-log code-load)
                 gp-current-stack-ref))))))

(set! (@@ (persist slask) get-curstack)
  (lambda ()
    (fluid-ref ((@@ (logic guile-log code-load) gp-current-stack-ref)))))

(set! (@@ (persist slask) vlist-null*   ) vlist-null    )
(set! (@@ (persist slask) gp-make-var   ) gp-make-var   )
(set! (@@ (persist slask) gp-clobber-var) gp-clobber-var)
(set! (@@ (persist slask) gp-get-id-data) gp-get-id-data)
(set! (@@ (persist slask) gp?           ) gp?           )

(primitive-module '(logic guile-log code-load))
