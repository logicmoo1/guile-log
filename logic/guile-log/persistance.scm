(define-module (logic guile-log persistance)
  #:use-module (logic guile-log procedure-properties)
  #:use-module (system vm loader)
  #:use-module (system vm program)
  #:use-module (system vm debug)
  #:use-module (logic guile-log code-load)
  #:use-module (ice-9 match)
  #:use-module (oop goops)
  #:use-module (ice-9 pretty-print)
  #:use-module (logic guile-log primitive)
  #:use-module (logic guile-log fstream)
  #:export  (make-persister 
	     load-persists
	     save-persists
	     persist-set!
	     persist-ref

	     make-shallow
	     associate-getter-setter

	     name-object
	     name-object-deep
	     define-named-object
	     define-shallow-object
	     define-fluid-object

	     pcopyable?
	     deep-pcopyable?
	     pcopy
	     deep-pcopy

	     repersist
	     ))

(define gp-cons?          (@@ (logic guile-log code-load) gp-cons?))
(define gp-cons-1         (@@ (logic guile-log code-load) gp-cons-ref-1))
(define gp-cons-2         (@@ (logic guile-log code-load) gp-cons-ref-2))
(define gp-make-pure-cons (@@ (logic guile-log code-load) gp-make-pure-cons))
(define gp-cons-set-1!    (@@ (logic guile-log code-load) gp-cons-set-1!))
(define gp-cons-set-2!    (@@ (logic guile-log code-load) gp-cons-set-2!))

(define-method (pcopyable? x) #f)
(define-method (deep-pcopyable? x) #f)
(define-method (pcopy x) (error "could not copy"))
(define-method (deep-pcopy x) (error "could not deep copy"))

(define (default-print-func struct port)
  (format port "<pre-struct>"))

(define default-struct
  (make-vtable "pw" default-print-func))

(define (struct->data s)
  (let lp ((n 0) (r (list (struct-vtable s))))
    (catch #t
      (lambda ()
	(lp (+ n 1) (cons (struct-ref s n) r)))
      (lambda x
	(reverse r)))))

(define default-code (program-code format))

(define-syntax-rule (aif (it) p x y) (let ((it p)) (if it x y)))
(define lookup-table (make-hash-table))
(define (find-fkn-adress path offset)
  (aif (it) (hash-ref lookup-table path #f)
       (+ (gp-bv-address it) offset)
       (let lp ((maps (all-mapped-elf-images)))
	 (if (pair? maps)
	     (let* ((m (car maps))
		    (a (+ (gp-bv-address m) offset))
		    (x (find-program-sources a)))
	       (if (pair? x)
		   (if (equal? (source-file (car x)) path)
		       (begin
			 (hash-set! lookup-table path m)
			 a)
		       (lp (cdr maps)))
		   (lp (cdr maps))))
	     (error "could not lookup function - did you have all modules loaded?")))))
		 
		     

(define (load-persists log)        (log 'load))
(define (save-persists log)        (log 'save))
(define (persist-set! log tag val) (register-tag log tag val))
(define (persist-ref  log tag)     (log 'get-tag tag))
(define (make-shallow val)
  (set-object-property! val 'shallow val)
  val)

(define (associate-getter-setter o set get)
  (set-object-property! o 'get-accessor get)
  (set-object-property! o 'set-accessor set)
  o)

(define (M x) (lambda (y) (eq? x y)))
(define gp-var    0)
(define var       1)
(define fluid     2)
(define pair      3)
(define vector    4)
(define procedure 5)
(define struct    6)
(define named     7)
(define gp-cons   8)
(define atom      9)
(define code      10)
(define standard-vtable 11)
(define primitive 12)
(define vlist-null-i 13)
(define curstack  14)

(define set-gp-var    20)
(define set-var       21)
(define set-fluid     22)
(define set-pair      23)
(define set-vector    24)
(define set-procedure 25)
(define set-struct    26)
(define set-gp-cons   28)
(define set-accessor  29)
(define make-object   30)
(define make-reducer  31)
(define set-reducer   32)

(define (curstack? x)
  (eq? x (fluid-ref ((@@ (logic guile-log code-load) gp-current-stack-ref)))))

(define (get-curstack)
  (fluid-ref ((@@ (logic guile-log code-load) gp-current-stack-ref))))

(define (hash->assoc h)
  (hash-fold
   (lambda (k v l) (cons (cons k v) l))
   '() h))

(define (assoc->hash l)
  (let lp ((h (make-hash-table)) (l l))
    (if (pair? l)
	(let ((k.v (car l)))
	  (hash-set! h (car k.v) (cdr k.v))
	  (lp h (cdr l)))
	h)))
(define-syntax-rule (ket self l ...)
  (letrec ((self (let l ...))) self))

(define (copy x)
  (let ((p1 (make-persister))
	(p2 (make-persister)))
    (register-tag-shallow p1 'x x)
    (p2 'load-repr (p1 'repr))
    (p2 'get-tag 'x)))

(define (dump x f)
  (let ((p (make-persister #:file f)))
    (register-tag p 'x x)
    (p 'save)))

(define (dumps x)
  (let ((p (make-persister)))
    (register-tag p 'x x)
    (p 'save-str)))

(define (load file)
  (let ((p (make-persister #:file file)))
    (p 'load)
    (p 'get-tag 'x)))

(define (loads s)
  (let ((p (make-persister)))
    (p 'load-str s)
    (p 'get-tag 'x)))

(define (deep-copy x)
  (let ((p1 (make-persister))
	(p2 (make-persister)))
    (register-tag-deep p1 'x x)
    (p2 'load-repr (p1 'repr))
    (p2 'get-tag 'x)))
    
(define (unserialize log)
  (define lmap (make-hash-table))
  (let lp ((l (reverse (log 'data))))
    (if (pair? l)
	(begin
	  (match (car l)
	    (((? (M vlist-null-i)) i)
	     (let ((v vlist-null))
	       (log 'reg-obj i v)))

	    (((? (M curstack)) i)
	     (let ((v (get-curstack)))
	       (log 'reg-obj i v)))

	    (((? (M gp-var)) i)
	     (let ((v (gp-make-var)))
	       (log 'reg-obj i v)))

	    (((? (M var)) i)
	     (let ((v (make-variable 1)))
	       (log 'reg-obj i v)))

	    (((? (M fluid)) i)
	     (let ((v (make-fluid)))
	       (log 'reg-obj i v)))

	    (((? (M pair)) i)
	     (let ((v (cons 0 0)))
	       (log 'reg-obj i v)))

	    (((? (M make-object)) i x)
	     (log 'reg-obj i x))
	    
	    (((? (M make-reducer)) i x)
	     (let* ((f (log 'rev-lookup x))
		    (o (f)))
	       (log 'reg-obj i o)))
	       	    
	    (((? (M struct)) i n)
	     (let ((v (gp-make-struct default-struct n)))
	       (log 'reg-obj i v)))
	   
	    (((? (M vector)) i n)
	     (let ((v (make-vector n)))
	       (log 'reg-obj i v)))

	    (((? (M procedure)) i n)
	     (let ((v (gp-make-null-procedure n default-code)))
	       (log 'reg-obj i v)))

	    (((? (M named)) i globdata)
	     (call-with-values (lambda () (log 'get-global globdata))
	       (lambda (path name)
		 (aif (mod) (resolve-module path)
		      (aif (f) (module-ref mod name)
			   (log 'reg-obj i f)
			   (error 
			    (format 
			     #f 
			     "symbol ~a not present in module ~a at unserializing" name path)))
		      (format 
		       #f 
		       "module ~a is not present at unserializing" path)))))

		   
            (((? (M standard-vtable)) i)
	     (log 'reg-obj i <standard-vtable>))

            (((? (M gp-cons)) i)
	     (let ((v (gp-make-pure-cons)))
	       (log 'reg-obj i v)))
	     
            (((? (M code)) i a)
	     (log 'reg-obj i (int-to-code a)))

            (((? (M primitive)) i a)
	     (log 'reg-obj i (get-primitive a)))

	    (((? (M atom)) i a)
	     (log 'reg-obj i a)
	     a)

	    (((? (M set-gp-var)) i j k)
	     (let ((var (log 'rev-lookup i))
		   (val (log 'rev-lookup k)))
	       (gp-clobber-var var j val)))

	    (((? (M set-var)) i j)
	     (let ((v (log 'rev-lookup i))
		   (x (log 'rev-lookup j)))
	       (variable-set! v x)))

	    (((? (M set-fluid)) i j)
	     (let ((v (log 'rev-lookup i))
		   (x (log 'rev-lookup j)))
	       (fluid-set! v x)))

	    (((? (M set-pair)) i j k)
	     (let ((v (log 'rev-lookup i))
		   (x (log 'rev-lookup j))
		   (y (log 'rev-lookup k)))
	       (set-car! v x)
	       (set-cdr! v y)))

	    (((? (M set-reducer)) io iv)
	     (let ((o (log 'rev-lookup io))
		   (v (log 'rev-lookup iv)))
	       (for-each
		(lambda (x)
		  (apply (car x) o (cdr x)))
		v)))
	     
	    (((? (M set-vector)) i l)
	     (let ((v (log 'rev-lookup i)))
	       (let lp ((l l) (n 0))
		 (if (pair? l)
		     (let ((x (log 'rev-lookup (car l))))
		       (vector-set! v n x)
		       (lp (cdr l) (+ n 1)))))))

	    (((? (M set-struct)) i l)
	     (let ((v (log 'rev-lookup i)))
	       (let lp ((l l) (n 0) (r '()))
		 (if (pair? l)
		     (let ((x (log 'rev-lookup (car l))))
		       (lp (cdr l) (+ n 1) (cons x r)))
		     (gp-set-struct v (reverse r))))))

	    (((? (M set-procedure)) i interface addr l)
	     (let ((proc
		    (log 'rev-lookup i))
		   (path (log 'interface-lookup interface))
		   (l (let lp ((l l))
			(if (pair? l)
			    (cons (log 'rev-lookup (car l)) (lp (cdr l)))
			    '()))))
	       (gp-fill-null-procedure 
		proc (find-fkn-adress path addr) l)))
	       
            (((? (M set-accessor)) obj data)
             (let* ((obj  (log 'rev-lookup obj))
                    (data (log 'rev-lookup data))
		    (set  (object-property obj 'set-accessor)))
	       (if set
		   (set data)
		   (error
		    (format 
		     #f "was not able to get an accessor set from ~a"
		     obj)))))

	    (((? (M set-struct) i l)) 1)
	    
	    (((? (M set-gp-cons)) i id j k)
	     (let ((v (log 'rev-lookup i))
		   (x (log 'rev-lookup j))
		   (y (log 'rev-lookup k)))	       
	       (gp-cons-set-1! v x)
	       (gp-cons-set-2! v y))))
	       
	  
	  (lp (cdr l))))))


(define* (make-persister #:key (file "persist.scm"))
  (ket self
       ((globmap  (make-hash-table))
	(mapglob  (make-hash-table))
	(iglob    0)
	(namemap  (make-hash-table))
	(mapname  (make-hash-table))
	(iname    0)
	(maps     (make-hash-table))
	(rev-maps (make-hash-table))
	(imap     0)
	(res     '())
	(i        0)
	(tags     '())
	(i->x     (make-hash-table))
	(i->obj   vlist-null)
	(obj->i   vlist-null)
	(atom->i  (make-hash-table)))

    (define (reg-global name path)
      (cons
       (aif (it) (hash-ref globmap path #f)
	    it
	    (let ((i iglob))
	      (set! iglob (+ iglob 1))
	      (hash-set! globmap path i)
	      (hash-set! mapglob i path)
	      i))
       (aif (it) (hash-ref namemap name #f)
	    it
	    (let ((i iname))
	      (set! iname (+ iname 1))
	      (hash-set! namemap name i)
	      (hash-set! mapname i name)
	      i))))
    
    (define inc
      (case-lambda 
	(()
	 (let ((res i))
	   (set! i (+ i 1))
	   i))
	((x)
	 (let ((i (inc)))
	   (set! obj->i (vhash-consv (object-address x) i obj->i))
	   (set! i->obj (vhash-consv i                  x i->obj))
	   i))))

    (define (update x) (set! res (cons x res)))
    
    (define-syntax-rule (mk-obj i obj code ...)
      (let ((i  (vhash-assv (object-address obj) obj->i)))
	(if i
	    (cdr i)
	    (let ((i (inc obj)))
	      (update (begin code ...))
	      i))))

    (define-syntax-rule (mk-atom i obj code ...)
      (let ((i (hash-ref atom->i obj #f)))
	(if i
	    i
	    (let ((i (inc)))
	      (hash-set! atom->i obj i)
	      (hash-set! i->x    i   obj)
	      (update (begin code ...))
	      i))))
      
    (define (repr)
      (list tags (hash->assoc maps) (reverse res) imap i
	    (hash->assoc globmap) iglob
	    (hash->assoc namemap) iname))

    (define (f x)
      (map (lambda (x) (cons (cdr x) (car x))) x))
	 
    (define (load-data data)
      (set! tags (list-ref data 0))
      (set! maps (assoc->hash (list-ref data 1)))
      (set! rev-maps (assoc->hash (f (list-ref data 1))))
      (set! res  (reverse (list-ref data 2)))
      (set! imap (list-ref data 3))
      (set! i    (list-ref data 4))
      (set! globmap (assoc->hash (list-ref data 5)))
      (set! mapglob (assoc->hash (f (list-ref data 5))))
      (set! iglob   (list-ref data 6))
      (set! namemap (assoc->hash (list-ref data 7)))
      (set! mapname (assoc->hash (f (list-ref data 7))))
      (set! iname   (list-ref data 8)))

    (case-lambda
      ((kind)
       (case kind
	 ((inc)
	  (inc))

	 ((data)
	  res)

	 ((repr) (repr))
	 
	 ((save)	
	  (let ((s (open-file file "w")))
	    (write (repr) s)
	    (close s)))

	 ((save-str)	
	  (call-with-output-string
	   (lambda (port)
	     (write (repr) port))))
	  
	 ((load)
	  (let* ((s    (open-file file "r"))
		 (data (read s)))
	    (close s)
	    (load-data data)
	    (unserialize self)
	    ))

	 ((print)
	  (pretty-print (repr)))))

      ((kind i j k l)
       (case kind
	 ((set-gp-cons)
	  (update `(,set-gp-cons ,i ,j ,k ,l)))
	 ((set-procedure)
	  (update `(,set-procedure ,i ,j ,k ,l)))))

      ((kind i j k)
       (case kind
	 ((set-gp-var)
	  (update `(,set-gp-var ,i ,j ,k)))
	 ((set-pair)
	  (update `(,set-pair   ,i ,j ,k)))))
       
      ((kind i j)
       (case kind
	 ((make-procedure)
	  (mk-obj n i 
	    `(,procedure ,n ,j)))

	 ((make-struct)
	  (mk-obj n i 
	    `(,struct ,n ,j)))

	 ((make-object)
	  (mk-obj n i
		  `(,make-object ,n ,j)))

	 ((make-reducer)
	  (mk-obj n i
		  `(,make-reducer ,n ,j)))


	 ((make-vector)
	  (mk-obj n i
		  `(,vector ,n ,j)))
	
	 ((reg-obj)
	  (let ((ja (object-address j)))
	    (set! i->obj (vhash-consv i  j i->obj))
	    (set! obj->i (vhash-consv ja i obj->i))))

	 ((store-tag)
	  (set! tags (cons (cons i j) tags)))

	 ((set-var)
	  (update `(,set-var ,i ,j)))

	 ((set-reducer)
	  (update (list set-reducer i j)))
	 
	 ((set-fluid)
	  (update `(,set-fluid ,i ,j)))
	 ((set-accessor)
	  (update `(,set-accessor ,i ,j)))
	 ((set-struct)
	  (update `(,set-struct ,i ,j)))
	 ((set-vector)
	  (update `(,set-vector ,i ,j)))))

      ((kind x)
       (case kind
	 ((load-repr)
	  (load-data x)
	  (unserialize self))

	 ((load-str)
	  (let ((data (call-with-input-string x read)))
	    (load-data data)
	    (unserialize self)))
	 
	 ((get-global)
	  (values 
	   (hash-ref mapglob (car x) #f)
	   (hash-ref mapname (cdr x) #f)))

	 ((get-tag)
	  (let ((r (assoc x tags)))
	    (if r
		(cdr (vhash-assv (cdr r)  i->obj))
		#f)))
	 ((reg-code)
	  (let ((r (hash-ref maps x #f)))
	    (if r
		r
		(let ((i imap))
		  (set! imap (+ i 1))
		  (hash-set! maps x i)
		  (hash-set! rev-maps i x)
		  i))))

	 ((interface-lookup)
	  (hash-ref rev-maps x #f))
	 ((lookup)
	  (let ((x (vhash-assv (object-address x) obj->i)))
	    (if x
		(cdr x)
		#f)))

	 ((rev-lookup)
	  (let ((x (vhash-assv x i->obj)))
	    (if x
		(cdr x)
		#f)))

	 ((named)
	  (let ()
	    (define (mk procedure-property)
	      (let ((path (procedure-property x 'module)))
		(if path
		    (let ((name (aif (it) (object-property x 'name)
				     it
				     (if (procedure? x)
					 (procedure-name x)
					 #f))))
				    
		      (if name
			  (let* ((i (inc x))
				 (o (reg-global name path)))
			    (update `(,named ,i ,o))
			    i)
			  #f))
		    #f)))

	    (aif (it) (mk object-property)
		 it
		 (if (procedure? x)
		     (mk procedure-property)
		     #f))))
		

	 ((make-accessor)
	  #t)

	 ((make-atom)
	  (mk-atom i x
	    `(,atom ,i ,x)))

	 ((make-code)
	  (mk-atom i x
	    `(,code ,i ,(code-to-int x))))

	 ((make-primitive)
	  (mk-atom i x
	    `(,primitive ,i ,(procedure-name x))))

	 ((make-standard-vtable)
	  (mk-obj i x
	    `(,standard-vtable ,i))) 

	 ((make-vlist-null)
	  (mk-obj i x
	    `(,vlist-null-i ,i))) 

	 ((make-curstack)
	  (mk-obj i x
	    `(,curstack ,i))) 

	 ((make-gp-var)
	  (mk-obj i x
	    `(,gp-var ,i))) 

	 ((make-gp-cons)
	  (mk-obj i x
	    `(,gp-cons ,i))) 

	 ((make-var)
	  (mk-obj i x
	    `(,var ,i))) 

	 ((make-fluid)
	  (mk-obj i x 
            `(,fluid ,i)))

	 ((make-pair)
	  (mk-obj i x 
            `(,pair ,i)))

	 ((make-gp-pair)
	  (mk-obj i x 
            `(,gp-cons ,i))))))))

(define (register-tag log tag x)
  (let ((i (persist log x)))
    (log 'store-tag tag i)))

(define (register-tag-shallow log tag x)
  (let ((i (persist log x #:shallow? #t)))
    (log 'store-tag tag i)))

(define (register-tag-deep log tag x)
  (let ((i (persist log x #:deep? #t)))
    (log 'store-tag tag i)))

(define log-log (make-fluid #f))
(define* (repersist . l)
  (define log (fluid-ref log-log))
  (if log
      (apply persist log l)
      (error "Assumes a persist have been executed")))

(define* (persist log x #:key (pred #t) (shallow? #f) (deep? #f))
(with-fluids ((log-log log))
(define* (upersist log x #:optional (pred #t))
  (define-syntax-rule (mk-name make-var x l ...)
    (let ((i (log 'named x)))
      (if i
	  i
	  (log make-var x l ...))))
  
  (define-syntax-rule (mk-name-f make-var x f)
    (let ((i (log 'named x)))
      (if i
	  i
	  (log make-var x (f)))))

  (define (dpersist log x . l)
    (if shallow?
	(list #:shallow x)	
	(apply upersist log x l)))
  
  (define-syntax-rule (do-if-deep x i code)
    (let ()
      (define (f n) (not (n x 'shallow)))
      (define (p)
	(if (f object-property)
	    (if (procedure? x)
		(f procedure-property)
		#t)
	    #f))
      (if (p) code)
      i))

  (define (make-atom)
    (let ((i (log 'make-atom x)))
      i))

  (define (make-a-code)
    (let ((i (log 'make-code x)))
      i))

  (define (make-a-var)
    (let ((i (mk-name 'make-var x)))
      (do-if-deep x i
	(let ((j (dpersist log (variable-ref x))))
	  (log 'set-var i j)))))

  (define (make-a-fluid)
    (let ((i (mk-name 'make-fluid x)))
      (do-if-deep x i
	(let ((j (dpersist log (fluid-ref x))))
	  (log 'set-fluid i j)))))

  (define (make-a-gp-var)
    (let ((i (mk-name 'make-gp-var x)))
      (do-if-deep x i
	(let* ((id (gp-get-id-data x))
	       (v  (gp-get-var-var x))
	       (j  (dpersist log v)))
	  (log 'set-gp-var i id j)))))

  (define (make-a-gp-cons)
    (let ((i (mk-name 'make-gp-cons x)))
      (do-if-deep x i
	(let* ((id (gp-get-id-data x))
	       (u  (gp-cons-1 x))
	       (v  (gp-cons-2 x))
	       (ju (dpersist log u))
	       (jv (dpersist log v)))
	  (log 'set-gp-cons i id ju jv)))))

  (define (make-vlist-null)
    (let ((i (mk-name 'make-vlist-null x)))
      i))

  (define (make-curstack)
    (let ((i (mk-name 'make-curstack x)))
      i))
    
  (define (make-standard-vtable)
    (let ((i (mk-name 'make-standard-vtable x)))
      i))

  (define (make-a-pair)
    (let ((i (mk-name 'make-pair x)))
      (do-if-deep x i
	(let* ((h (car x))
	       (t (cdr x))
	       (j (dpersist log h))
	       (k (dpersist log t)))
	  (log 'set-pair i j k)))))

  (define (make-an-access x y)
    (let ((i (mk-name 'make-accessor x)))
      (let* ((data (dpersist log (y)))
	     (obj  (dpersist log x #f)))	       
	(log 'set-accessor obj data)
	i)))
    
  (define (make-a-struct)
    (let* ((l (struct->data x))
	   (i (mk-name 'make-struct x (- (length l) 1))))
      (do-if-deep x i
		  (let ((l (map (lambda (x) (dpersist log x)) l)))
		    (log 'set-struct i l)))))

  (define (make-a-vector)
    (let* ((n (vector-length x))
	   (i (mk-name 'make-vector x n)))
      (do-if-deep x i
		  (let ((l (map (lambda (x) (dpersist log x)) 
				(vector->list x))))
		    (log 'set-vector i l)))))

  (define (make-copy)
    (mk-name-f 'make-object x (lambda () (pcopy x))))
  
  (define (make-deep-copy)
    (let ((i (log 'named x)))
      (if i
	  (do-if-deep x i
	       (match (deep-pcopy x deep?)
		      ((#:obj x2)
		       (mk-name-f 'make-object x (lambda () x2)))
		      ((#:reduce make data)
		       (let ((id (dpersist log data)))
			 (log 'set-reducer i id)))))
		      
	  (match (deep-pcopy x deep?)
		 ((#:obj x2)
		  (mk-name-f 'make-object x (lambda () x2)))
		 ((#:reduce make data)
		  (let ((i (mk-name-f 'make-reducer x
				      (lambda () (dpersist log make)))))
		    (let ((id (dpersist log data)))
		      (log 'set-reducer i id))))))))
			
	  
  (define (make-a-primitive)
    (let ((i (mk-name 'make-primitive x)))
      i))

  (define (make-a-procedure)
    (let* ((prim?   (primitive? x))
	   (free    (if prim? '() (program-free-variables x)))
	   (nfree   (length free))
	   (i       (mk-name 'make-procedure x nfree)))
      (do-if-deep x i
	(let* ((addr    (program-code x))
	       (elf     (find-mapped-elf-image addr))
	       (elfaddr (gp-bv-address elf))
	       (reladdr (- addr elfaddr))
	       (source  (match (program-sources x)
			       (() #f)
			       (((_ source . _) . _) source)))
	       (path   source
			#;(if source
			    (match (string-split source #\/)
			      ((x ... e) (append 
					  (map string->symbol x) 
					  (list
					   (match (string-split e #\.)
					     ((x ... scm)
					      (string->symbol
					       (string-join x ".")))))))
			      (error "could not translate a procedure"))))
	       (icode   (log 'reg-code path))
	       (l       (map (lambda (x) (dpersist log x)) free)))
	  (log 'set-procedure i icode reladdr l)))))
      

    
   
  (define-syntax-rule (mk code)
    (let ((i (log 'lookup x)))
      (if i
	  i
	  (code))))
  (cond   
   ((and pred (object-property x 'get-accessor))
    =>
    (lambda (y)
      (mk (lambda () (make-an-access x y)))))

   ((and shallow? (object-property x 'copy))
    =>
    (lambda (f)
      (mk (f x))))

   ((and shallow? (pcopyable? x))
    (mk make-copy))
 
   ((deep-pcopyable? x)
    (mk make-deep-copy))
	   	      
   ((curstack? x)
    (mk make-curstack))

   ((eq? x vlist-null)
    (mk make-vlist-null))

   ((eq? x <standard-vtable>)
    (mk make-standard-vtable))  

   ((gp-cons? x)
    (mk make-a-gp-cons))

   ((gp? x)
    (mk make-a-gp-var))

   ((variable? x)
    (mk make-a-var))

   ((fluid? x)
    (mk make-a-fluid))

   ((pair? x)
    (mk make-a-pair))

   ((vector? x)
    (mk make-a-vector))

   ((struct? x)
    (mk make-a-struct))

   ((procedure? x)
    (if (primitive? x)
	(aif (it) (get-primitive x)
	     (mk make-a-primitive)
	     (error "non registred primitive"))
	(mk make-a-procedure)))

   ((code-to-int x)
    (mk make-a-code))

   (else
    (mk make-atom))))
(upersist log x pred)))

(define-syntax name-object
  (syntax-rules ()
    ((_ f)
     (let ((y f))
       (make-shallow y)
       (set-object-property! y 'name   'f)
       (set-object-property! y 'module (module-name (current-module)))))
    ((_ f ...)
     (begin (name-object f) ...))))

(define-syntax-rule (name-object-deep f)
  (let ((y f))
    (set-object-property! y 'name   'f)
    (set-object-property! y 'module (module-name (current-module)))))
  
(define-syntax-rule (define-named-object f x)
  (define f
    (let ((y x))
      (set-object-property! y 'name   'f)
      (set-object-property! y 'module (module-name (current-module)))
      y)))

(define-syntax-rule (define-shallow-object name val)
  (define-named-object name
    (make-shallow val)))

(define-syntax-rule (define-fluid-object name val)
  (define-named-object name
    (associate-getter-setter (make-fluid val) fluid-ref fluid-set!)))
