(define-module (logic guile-log vlist-macros)
  #:use-module (logic guile-log)
  #:use-module (logic guile-log vlist)
  #:export (<vhash-each> <vhash-fold>))

(define-syntax-rule (B x) (logand x #xfffffff))
(define-syntax-rule (get-bt-in   x) (struct-ref x 0))
(define-syntax-rule (get-n-in    x) (B (struct-ref x 1)))
(define-syntax-rule (get-ar      x) (vector-ref x 0))
(define-syntax-rule (get-bt      x) (vector-ref x 1))
(define-syntax-rule (get-n       x) (B (vector-ref x 2)))
(define-syntax-rule (get-s       x) (B (vector-ref x 3)))
(define-syntax-rule (get-key  a  n) (vector-ref a n))
(define-syntax-rule (get-val  a s n) (vector-ref a (+ s n)))

(<define-guile-log-rule> (<vhash-each> P (key val) next in finish body)
(<let> ((btstart (get-bt-in P))
	(istart  (+ (get-n-in P) 1)))
(<letrec> ((f (<lambda> (bt n cc)
		 (<let> ((s (get-s bt)))
		   (if (eq? bt btstart)
		       (if (= s 0)
			   (cc)
			   (g bt n istart cc))
		       (if (= s 0)
			   (f (get-bt bt) (get-n bt) cc)
			   (g bt n 0 (<lambda> () (f (get-bt bt) (get-n bt) cc))))))))
			  
	   (g (<lambda> (bt n i cc)
		(<let*> ((ar  (get-ar bt))
			 (s   (get-s  bt)))
		  (<recur> lp ((i i))
		    (<let> ((next (<lambda> () (lp (+ i 1)))))			
                      (if (<= i n)
			(<let> ((key (get-key ar i))
				(val (get-val ar s i)))
			    body)
			(cc))))))))
   (<let> ((x in))
     (f (get-bt-in x) (get-n-in x) (<lambda> () finish))))))

(<define-guile-log-rule> (<vhash-fold> P ((seed sin) ...) (key val) next in
				       finish body)
(<let> ((btstart      (get-bt-in P))
	(istart  (+ 1 (get-n-in  P))))
(<letrec> ((f (<lambda> (seed ... bt n cc)
	        (<let> ((s (get-s bt)))
		   (if (eq? bt btstart)
		       (if (= s 0)
			   (cc seed ...)
			   (g seed ... bt n istart cc))
		       (if (= s 0)
			   (f seed ... (get-bt bt) (get-n bt) cc)
			   (g seed ... bt n 0
			      (<lambda> (seed ...)
				 (f seed ... (get-bt bt) (get-n bt) cc))))))))
			  
	   (g (<lambda> (seed ... bt n i cc)
		(<let*> ((ar  (get-ar bt))
			 (s   (get-s  bt)))
		  (<recur> lp ((i i) (seed seed) ...)
		    (<let> ((next (<lambda> (seed ...) 
					    (lp (+ i 1) seed ...))))
		     (if (<= i n)
			 (<let> ((key (get-key ar i))
				 (val (get-val ar s i)))
			    body)
			 (cc seed ...))))))))
     (<let> ((x in))
       (f sin ... (get-bt-in x) (get-n-in x) (<lambda> (seed ...) finish))))))
	  
