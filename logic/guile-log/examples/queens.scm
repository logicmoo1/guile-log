(define-module (logic guile-log examples queens)
  #:use-module (logic guile-log)	       
  #:use-module (logic guile-log umatch)	       
  #:use-module (srfi srfi-11)	       
  #:export (run))


(<define> (queens N Qs) 
  (<var> (Ns)
    (<and> (range-list 1   N  Ns)
	   (queens3   Ns '() Qs))))

(<define> (queens3 UnplacedQs SafeQs Qs)
  (<match> () (UnplacedQs)
    ( _  (<var> (Q UnplacedQs1)
	   (<and> (selectq Q UnplacedQs UnplacedQs1)
		  (<not> (attack Q SafeQs))
		  (queens3 UnplacedQs1 (<cons> Q SafeQs) Qs))))
    ('() (<=> SafeQs Qs))
    ( _  <fail>)))

(<define> (attack X Xs) (attack3 X 1 Xs))


(define (attack3 s p cc x n v)
  (let-values (((ca cd s) (gp-pair+ v s)))
    (if s
        (let ((x (gp->scm x  s))
              (y (gp->scm ca s)))
          (let ((f (gp-newframe s)))
            (if (or (eq? x (+ y n)) (eq? x (- y n)))              
                (cc f (lambda ()
                        (gp-unwind f)
                        (attack3 f p cc x (+ n 1) cd)))
                (attack3 f p cc x (+ n 1) cd))))
        (p))))

#;
(<define> (attack3 X N V)
  (<match> () (V)
    ((Y . _)  (<and>
               (<when> (or (eq? (<scm> X) (+ (<scm> Y) N))
                           (eq? (<scm> X) (- (<scm> Y) N))))))
    ((_ . Y)  (attack3 X (+ N 1) Y))
    (_        <fail>)))

(<define> (range-list M N U)
  (<match> () (U)
    ((,M)      (<when> (>= M N) <cut>))
    ((,M . L)  (range-list (+ M 1) N L))
    (_         <fail>)))

(<define> (selectq X U Xs)
  (<match> () (U Xs)
    ((,X . ,Xs)   _             <cc>)
    (( Y .  Ys)   ( Y . Zs)     (selectq X Ys Zs))
    (_            _             <fail>)))

(define (f)
  (<run> * (Q)
    (queens 10 Q)))

(define (run n)
  (if (= n 0)
      #t
      (begin
        (f)
        (run (- n 1)))))
