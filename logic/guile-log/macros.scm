(define-module (logic guile-log macros)
  #:use-module (logic guile-log procedure-properties)
  #:use-module (system base compile)
  #:use-module (ice-9 match-phd)
  #:use-module (logic guile-log guile-log-pre)
  #:use-module (logic guile-log umatch)
  #:use-module (ice-9 pretty-print)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-11)
  #:use-module (system repl repl)
  #:re-export (define-and-log 
		define-guile-log guile-log-macro? log-code-macro 
		log-code-macro?)
  #:export (<next> <or> <and> <not> <cond> <if> <scm-if> <fast-if>
		   functorize adaptable_vars <ground?>
		   <with-guile-log>  <if-some>
		   <cc> <fail> <let> <let*> <var> <nvar>
                   <modvar> <hvar> </.> <when>
		   <define> <define*> <cut> <pp> <pp-dyn> <dyn> <unify>
		   <recur> <letrec>
		   <lambda> <case-lambda> <with-fail> <with-cut> <with-s>
		   <lambda*>
		   <with-cc>
		   <<lambda>> <<case-lambda>> <<match>>
		   <match> <=> <r=> <==> *r* <funcall> <apply>
		   <and!> <and!!> <succeds>
		   <format> <code> <ret>
		   <def> <def-> <<define>> <<define->> <dynwind> 
		   parse<> ->list
		   let<> <_>
		   <state-ref> <state-set!> <lv*> <clear>
		   tr S P CC CUT SCUT <scm>
		   <cons> <cons?> <var?> <values> <windlevel>
		   <syntax-parameterize>	
		   <car> <cdr> <logical++> <logical-->
		   define-guile-log-parser-tool
		   <newframe> <newframe-choice>
		   <unwind> <unwind-tail> <prune> <prune-tail>
		   <define-guile-log-rule>
		   <get-fixed> <get-idfixed> <cp> <lookup> <wrap> <wrap-s>
                   <with-bind>
		   <attvar?> attvar?
		   <put-attr> <put-attr-guarded> <put-attr-weak-guarded> 
		   <put-attr!> <put-attr-guarded!> <put-attr-weak-guarded!> 
		   <get-attr> <del-attr> <del-attr!> <get-attrs>
		   <raw-attvar> <attvar-raw?> <set> <set0> <set!>
		   <with-log-in-code>
		   dls-match gp-not-n gp-is-delayed?
		   build_attribut_representation
		   ))
(eval-when (eval load compile)
 (define (pp x)
   (pretty-print x)
   x))
	
(define (<wrap> f . l)
  (apply f (fluid-ref *current-stack*) (lambda x #f) (lambda (s . l) s) l))
(define (<wrap-s> f s . l)
  (apply f s (lambda x #f) (lambda (s . x) s) l))



(if (not (defined? 'syntax-parameterize))
    (module-set! 
       (current-module)
       'syntax-parameterize 
       (module-ref (current-module) 'fluid-let-syntax)))


(define-syntax-parameter S   
  (lambda (x) (error "S should be bound by fluid-let")))
(define-syntax-parameter SCUT
  (lambda (x) (error "SCUT should be bound by fluid-let")))
(define-syntax-parameter P   
  (lambda (x) (error "P should be bound by fluid-let")))
(define-syntax-parameter CC  
  (lambda (x) (error "CC should be bound by fluid-let")))
(define-syntax-parameter CUT 
  (lambda (x) (error "CUT should be bound by fluid-let")))

(eval-when (compile eval load)
  (define-syntax <_>
    (lambda (x)
      (syntax-case x ()
	((_ . _) (error "_ in guile-log is not a function"))
	(_       #'(gp-var! S))))))

(define-syntax-rule (fl-let (cut s p cc) code ...)
  (syntax-parameterize ((S   (identifier-syntax s))
			(P   (identifier-syntax p))
			(CC  (identifier-syntax cc))
			(CUT (identifier-syntax cut)))
    code ...))

(define-syntax-rule (cc-let (cc) code ...)
  (syntax-parameterize ((CC  (identifier-syntax cc)))
    code ...))
		    
(define-syntax-rule (<scm> x)     
  (let ((u x))
    (if (number? u)
	u
	(gp->scm u S))))

(define-syntax-rule (<cons> x y)  (gp-cons! x y S))
(define-syntax-rule (<car>    x)  (gp-car (gp-lookup x S) S))
(define-syntax-rule (<cdr>    x)  (gp-cdr (gp-lookup x S) S))
(define-syntax-rule (<lookup> x)  (gp-lookup x S))
(define-syntax-rule (<newframe>)  (gp-newframe S))
(define-syntax <newframe-choice>
  (syntax-rules ()
    ((_) (gp-newframe-choice S))
    ((_ s) (gp-newframe-choice s))))
(define-syntax-rule (<unwind> p)  (gp-unwind p))
(define-syntax-rule (<prune>  p)  (gp-prune  p))
(define-syntax-rule (<prune-tail>  p)  (gp-prune-tail  p))
(define-syntax-rule (<unwind-tail> p)  (gp-unwind-tail p))
(define-syntax-rule (<cp>     x ...)  (gp-cp x ... S))
(define-syntax-rule (<cons?>  x)  (gp-pair- (gp-lookup x S) S))
(define-syntax <var?>
  (syntax-rules ()
    ((_ s x) (let ((w (gp-lookup x s)))
	     (gp-var? w s)))
    ((_ x) (<var?> S x))))

(define-syntax-rule (<get-fixed> x y) (gp-get-fixed-free x y S))
(define-syntax-rule (<get-idfixed> x y) (gp-get-id-free x y S))

(define-syntax let-values*
  (syntax-rules ()
    ((let-values* () code ...)
     (begin code ...))
    ((let-values* (x xx ...) . l)
     (let-values (x) (let-values* (xx ...) . l)))))               
  
(define-syntax mk-syms
  (lambda (x)
    (syntax-case x ()
      ((q . l)
       ;(pk (syntax->datum (syntax l)))
       (syntax (mk-syms* . l))))))

(define-syntax-rule (<apply> s p cc f x ... l)
  (apply (gp-lookup f s) s p cc x ... (->list s l)))
       
(define-syntax mk-syms*
  (lambda (x)
    (syntax-case x ()
      ((q 1  (a as ...) rs  l)
       (syntax (mk-syms 10 (as ...) rs l)))
      ((q 10 (a as ...) rs  l)
       (with-syntax ((sym (datum->syntax (syntax q) (gensym "arg"))))
	(syntax (mk-syms 10 (as ...) (sym . rs)  l))))
      ((q 11 (a) rs (f b ...))
       (syntax (f rs b ...)))
      ((q 11 (a as ...) rs  l)
       (with-syntax ((sym (datum->syntax (syntax q) (gensym "pr"))))
            (syntax (mk-syms 11 (as ...) (sym . rs)  l))))
      ((q 2 (a as ...) rs  l)
       (with-syntax ((sym-p (datum->syntax (syntax q) (gensym "pr"))))
            (syntax (mk-syms 2 (as ...) (sym-p . rs)  l))))
      ((q 4 (a as ...) rs  l)
       (with-syntax ((sym-p1 (datum->syntax (syntax q) (gensym "pr" )))
                     (sym-w2 (datum->syntax (syntax q) (gensym "tag")))
                     (sym-p2 (datum->syntax (syntax q) (gensym "pr" ))))
            (syntax (mk-syms 4 (as ...) ((sym-p1 sym-w2 sym-p2) . rs)  l))))
      ((q 2 () rs (f a ...))
       (with-syntax ((sym (datum->syntax (syntax q) (gensym "w"))))
                    (syntax (f w rs a ...))))
      ((q 4 () rs (f a ...))
       (with-syntax ((sym (datum->syntax (syntax q) (gensym "w"))))
                    (syntax (f w rs a ...))))
      ((q n () rs (f a ...))
       (syntax (f rs a ...))))))

(define-guile-log <or>
  (syntax-rules (****)
    ((_ meta   ) (parse<> meta <fail>))
    
    ((_ meta e1) (parse<> meta e1))

    ((_ (cut ss pr cc) . l)
     (let ((s (gp-newframe-choice ss)))
       (or-aux ss (cut s pr cc) . l)))))

(define-syntax or-aux
  (syntax-rules ()
    ((_ ss (cut s p cc) a)
     (begin
       (gp-unwind-tail s)
       (parse<> (cut ss p cc) a)))
    ((_ ss (cut s p cc) a . as)
     (let ((pp (lambda ()
                 (gp-unwind s)
		 (or-aux ss (cut s p cc) . as))))
       (parse<> (cut s pp cc) a)))))
     
(define-and-log <values>
  (syntax-rules ()    
    ((_ (cut s pr cc) (<values> vars a ...) e2 ...)
     (let ((lam (lambda (ss pr2 . vars) 
		  (parse<> (cut ss pr2 cc) 
		     (<and>  e2 ...)))))
       (parse<> (cut s pr lam)
	 (<and> a ...))))))
       
(define-and-log <cc>
  (syntax-rules ()
    ((<cc> (cut s p cc) ())
     (cc s p))
    ((<cc> (cut s p cc) () a ...)
     (<and> (cut s p cc) a ...))

    ((<cc> (cut s p cc) (_ . l))
     (fl-let (cut s p cc) 
	(cc s p . l)))

    ((<cc> (cut s p cc) (_ . l) a ...)
     (<and> (cut s p cc) (<code> (fl-let (cut s p cc) . l))
	    a ...))))

(define-and-log <ret>
  (syntax-rules ()
    ((_ (cut s p cc) (_ code ... r) a ...)
     (let ((cc2 (lambda (s2 p2)
		  (parse<> (cut s2 p2 cc)
		    (<and> a ...)))))
       (fl-let (cut s p cc2) 
	 (begin code ... (gp->scm r S)))))))
	 
(define-and-log <cut> 
  (syntax-rules ()
    ((_ (cut s p cc) () a ...)
     (parse<> (cut s cut cc) 
       (<with-fail> cut
          (<code> (<prune> SCUT))
	  (<and> a ...))))
    ((_ (cut s p cc) (_ . l) a ...)
     (<and> (cut s p cc) 
          (<with-fail> cut 
             (<code> (<prune> SCUT))
             (<and> . l)) a ...))))


    
(define-and-log <fail>
  (syntax-rules ()
    ((_ (cut s p cc) (    ) a ...) (p))
    ((_ (cut s p cc) (_ pp) a ...) (pp))))

(eval-when (compile load eval)
(define (ander x)
  (syntax-case x (<and>)
    ((a ... (<and> . l))
     (if (complex-and? #'l)
	 x
	 (append #'(a ...) (ander #'l))))
    ((a ...)
     #'(a ...)))))

(eval-when (compile load eval)
(define (complex-and? x)
  #t #;
  (syntax-case x ()
    ((a ...)
     (or-map 
      (lambda (x)
	(syntax-case x ()
	  ((n . l)
	   (and-log-macro? (syntax n)))
	  (_ #f)))
      #'(a ...))))))

(eval-when (compile load eval)
(define (fand s p cc cut fs)
  (if (pair? fs)
      (let ((ccc (lambda (s p . u)
		   (if (pair? (cdr fs))
		       (fand s p cc cut (cdr fs))
		       (apply cc s p u)))))
	((car fs) s p ccc cut))
      (cc s p))))

(define-guile-log <and>
  (lambda (x)
    (syntax-case x ()
      ((_ ww aa ...)
       (if (not (complex-and? #'(aa ...)))
	   (syntax-case (list #'ww (ander #'(aa ...))) ()
	     (((cut s p cc) (aaa ...))
	      #'(fand s p cc cut 
		      (list
		       (lambda (ss pp ccc ccut)
			 (parse<> (ccut ss pp ccc) aaa))
		       ...))))
			 
    (syntax-case x ()
      ((_ w)
       #'(parse<> w <cc>))

      ((_ w (n . l) e2 ...)
       (and-log-macro? (syntax n))
       #'(n w (n . l) e2 ...))

      ((_ w n e2 ...)
       (and-log-macro? #'n)
       #'(n w () e2 ...))

      ((_ meta       e1) 
       #'(parse<> meta e1))

      ((_ (cut s pr cc) (a b ...) e2 ...)
       (log-code-macro? #'a)
       #'(a (cut s pr (parse<> (cut s pr cc) 
                        (<and> e2 ...)))
                 b ...))

      ((_ (cut s pr cc) e1 e2 e3 ...)  
       #'(let ((ccc (lambda (ss pr2 . xxx)		   
		      (fl-let (cut ss pr2 cc)
			(parse<> (cut ss pr2 cc) (<and> e2 e3 ...))))))
         (cc-let (ccc) 	   
           (parse<> (cut s pr ccc) e1))))

      ((_ (cut s pr cc) e1)  
       #'(let ((ccc cc))
	   (cc-let (ccc) 	   
	     (parse<> (cut s pr ccc) e1))))))))))

;;This is inspired from kanren
;; (<and!>  a ... ) gives exactly one answer from (and a ....)
;; (<and!!> a ... ) is the same as (and (and! a) ...)
;; this can be modeled by a cut in a less elegant way.
(define-guile-log <and!>
  (syntax-rules ()
    ((_ w) (parse<> w <cc>))
    ((_ (cut s p cc) a ...) 
     (let ((ccc (lambda (ss pp . l) 
		  (apply cc ss p l))))
       (parse<> (cut s p ccc) (<and> a ... ))))))

(define-guile-log <and!!>
  (lambda (x)
    (syntax-case x ()
      ((_ w)
       #'(parse<> w <cc>))

      ((_ (cut s pr cc) (n . l) e2 ...)
       (if (and-log-macro? #'n)
           #t
           #f)
       #'(n (cut s pr cc) (n . l) (<with-fail> pr (<and!!> e2 ...))))

      ((_ (cut s pr cc)  n e2 ...)
       (if (and (identifier? #'n) (and-log-macro? #'n))
           #t
           #f)
       #'(n  (cut s pr cc) (<with-fail> pr (<and!!> e2 ...))))


      ((_ meta       e1       ) 
       #'(parse<> meta (<and!> e1)))

      ((_ (cut s pr cc) (a b ...) e2 ...)
       (log-code-macro? (syntax a))
       #'(a (cut s pr (parse<> (cut s pr cc) 
			 (<and!!> e2 ...)))
                 b ...))

      ((_ (cut s pr cc) e1 e2 ...)  
       #'(let ((ccc (lambda (ss pr2 . xxx) 
		      (parse<> (cut ss pr cc) 
			(<and!!> e2 ...)))))
           (parse<> (cut s pr ccc) e1))))))


;; this will try to make a success and if so reset the state and continue 
;; it's a companion to <not>.
(define-guile-log <succeds>
  (syntax-rules ()
    ((_ (cut s p cc) g ...)
     (let* ((ss   (gp-newframe s))
            (ccc (lambda (ss pp . xxx) (gp-unwind-tail ss) (apply cc s p xxx))))
       (parse<> (cut s p ccc) (<and> g ...))))))



(define-syntax <%fkn%>
  (syntax-rules ()
    ((_ (cut s pr cc) f a ...)
     (fl-let (cut s pr cc)
      (f s pr cc a ... )))))

(define-guile-log <with-cc>
  (syntax-rules ()
    ((_ (cut s p cc) ccc code ...)
     (let ((cccc ccc))
       (syntax-parameterize ((CC (identifier-syntax cccc)))
          (parse<> (cut s p cccc) (<and> code ...)))))))

(define-guile-log <with-s>
  (syntax-rules ()
    ((_ (cut s p cc) ss code ...)
     (let ((sss ss))
       (syntax-parameterize ((S (identifier-syntax sss)))
          (parse<> (cut sss p cc) (<and> code ...)))))))

(define-guile-log <with-fail>
  (syntax-rules ()
    ((_ (cut s p cc) pp code ...)
     (let ((ppp pp))
       (syntax-parameterize ((P (identifier-syntax ppp)))
          (parse<> (cut s ppp cc) (<and> code ...)))))))

(define-guile-log <with-cut>
  (syntax-rules ()
    ((_ (cut s p cc) cutt scutt code ...)
     (let ((cuttt cutt)
	   (scuttt scutt))
       (syntax-parameterize ((CUT  (identifier-syntax cuttt))
			     (SCUT (identifier-syntax scuttt)))
         (parse<> (cuttt s p cc) (<and> code ...)))))))


(define-guile-log <if>
  (syntax-rules ()
    ((_ meta p a)
     (parse<> meta 
	      (let ((s S))
		(<and!> p) 
		a)))
    ((_ (cut s p cc) pred a b)
     (let* ((fr (<newframe-choice> s))
	    (pp (lambda ()
		  (<unwind-tail> fr)
		  (parse<> (cut s p cc) b))))
       (parse<> (cut fr pp cc)
	(<and> 
	 pred 
	 (<with-fail> p 
	    (<code> (<prune-tail> fr))
	    a)))))))

(define-guile-log <scm-if>
  (syntax-rules ()
    ((_ (cut s p cc) pred a)
     (if pred
         (parse<> (cut s p cc) a)
         (p)))

    ((_ (cut s p cc) pred a b)
     (if pred
         (parse<> (cut s p cc) a)
         (parse<> (cut s p cc) b)))))

(define-guile-log <fast-if>
  (syntax-rules ()
    ((_ (cut s p cc) pred a)
     (if pred
         (parse<> (cut s p cc) a)
         (p)))

    ((_ (cut s p cc) pred a b)
     (let ((p2f (lambda () (parse<> (cut s p cc) b))))
       (let ((cc2 (lambda (s3 p3)
		    (parse<> (cut s3 p cc) a))))
	 (parse<> (cut s p2f cc2) pred)))))) 


(define-guile-log <if-some>
  (syntax-rules ()
    ((_ meta p a)
     (<and> p a))
    ((_ meta p a b)
     (parse<> meta (<or> (<and> p a) (<and> (<not> p) b))))))
                         

(define-guile-log <cond>
  (syntax-rules (else)
    ((_ meta (else b ...)) 
     (parse<> meta (<and> b ...)))
    ((_ meta (a b ...))  
     (parse<> meta (<and> a b ...)))
    ((_ meta (a b ...) ab ...)     
     (parse<> meta (<if> a (<and> b ...) (<cond> ab ...))))))
        
(define doit-guard (@@ (logic guile-log umatch) doit-guard))
(define-guile-log <var>
  (syntax-rules ()
    ((_ (cut s p cc) (v ...) code ...) 
     (begin
       (let ((v (gp-var! s)) ...)
	 (fl-let (cut s p cc)
	   (parse<> (cut s p cc) (<and> code ...))))))))

(define gp-make-nvar (@@ (logic guile-log code-load) gp-make-nvar))

(define-guile-log <nvar>
  (syntax-rules ()
    ((_ (cut s p cc) n (v ...) code ...) 
     (let ((nn n))
       (let ((v (gp-make-nvar n)) ...)
	 (fl-let (cut s p cc)
	   (parse<> (cut s p cc) (<and> code ...))))))))

(define-guile-log <modvar>
  (syntax-rules ()
    ((_ (cut s p cc) (v ...) code ...) 
     (begin
       (fluid-set! doit-guard #f)
       (let ((v (gp-var! s)) ...)
	 (fluid-set! doit-guard #t)
	 (fl-let (cut s p cc)
	   (parse<> (cut s p cc) (<and> code ...))))))))

(define-guile-log <hvar>
  (syntax-rules ()
    ((_ (cut s p cc) (v ...) code ...) 
     (begin
       (let ((v (gp-heap-var! s)) ...)
	 (fl-let (cut s p cc)
		 (parse<> (cut s p cc) (<and> code ...))))))))


(define-guile-log <let>
  (syntax-rules ()
    ((_ (cut s p cc) () code ...)
     (let () (parse<> (cut s p cc) (<and> code ...))))
    ((_ (cut s p cc) (v ...) code ...)
       (letrec ((ccc (lambda (ss pp)
                       (fl-let (cut s p cc)
                         (let (v ... )
                           (parse<> (cut ss pp cc) (<and> code ...)))))))
         (ccc s p)))))

(define-guile-log <let*>
  (syntax-rules ()
    ((_ (cut s p cc) () code ...)
     (let () (parse<> (cut s p cc) (<and> code ...))))

    ((_ (cut s p cc) (v ...) code ...)
       (letrec ((ccc (lambda (ss pp)
                       (fl-let (cut s p cc)                       
                         (let* (v ... )
                           (parse<> (cut ss pp cc) (<and> code ...)))))))
         (ccc s p)))))
  
#|
For tabling, negations are tricky. the reason is that when a recursive application runs out of solutions it issues a fail, which in case the mode is not mode will produce a success. What we must make sure is that if a not is delayed, then the not will also be delayed and hence the not will fail at all levels e.g. the code stops at all negations, a delay that hits an or will try the next branches as well but if all or's fails, then the result is a delay so basically delay is a state of a not.
|#

(define gp-not-n        (gp-make-var 0))
(define gp-is-delayed?  (gp-make-var #f))

(define-guile-log <not>
  (syntax-rules ()
    ((_ (cut s p cc) code ...)
     (notter cut s p cc (lambda (ss pp ccc) 
			  (parse<> (pp ss pp ccc)
                             (<with-cut> pp ss
			        (<and> code ...))))))))

(define (notter cut s p cc lam)
  (let* ((ss  (gp-newframe s))
	 (ss2 (gp-set! gp-not-n (+ (gp-lookup gp-not-n s) 1) ss))
	 (ss3 (gp-set! gp-is-delayed? #f ss2))
	 (ccc (lambda (s pp . x)
		(gp-unwind-tail ss)
		(gp-var-set! gp-is-delayed? #f)
		(p)))
	 (ppp (lambda ()
		(let ((n (gp-lookup gp-not-n ss2))
		      (d (gp-lookup gp-is-delayed? ss2)))
		  (gp-unwind ss)
		  (if (and (> n 1) d)
		      (begin
			(gp-var-set! gp-is-delayed? #t) 
			(p))
		      (cc s p))))))
    (lam ss3 ppp ccc)))


(define-guile-log <when>
  (syntax-rules ()
    ((_ (cut s p cc) pred code ...)
     (fl-let (cut s p xcc)
	(if pred 
	    (parse<> (cut s p cc) (<and> code ...))
	    (parse<> (cut s p cc)  <fail>))))))

(define-guile-log <pp>
  (syntax-rules ()
    ((_ (cut s p cc) x) 
     (fl-let (cut s p cc)
        (begin (with-fluids ((*current-stack* S))
		 (pp (gp->scm x s)))
               (parse<> (cut s p cc) <cc>))))))
#;(log-code-macro '<pp>)

(define-guile-log <logical++>
  (syntax-rules ()
    ((_ (cut s p cc)) 
     (begin (use-logical s)
            (parse<> (cut s p cc) <cc>)))))


(define-guile-log <logical-->
  (syntax-rules ()
    ((_ (cut s p cc)) 
     (begin (leave-logical s)
       (parse<> (cut s p cc) <cc>)))))



(define-guile-log <pp-dyn>
  (syntax-rules ()
    ((_ (cut s p cc) a b)   
     (fl-let (cut s p cc)
       (gp-dynwind
        (lambda () 
	  (pp (gp->scm a s)))
        (lambda () 
	  (pp (gp->scm a s)) 
	  (parse<> (cut s p cc) <cc>))
        (lambda (x) 
	  (pp (gp->scm b s)))
        s)))))

(define-guile-log <dyn>
  (syntax-rules ()
    ((_ (cut s p cc) a b)   
     (fl-let (cut s p cc)
       (gp-dynwind
        (lambda () a)
        (lambda () (parse<> (cut s p cc) <cc>))
        (lambda (x) b)
        s)))))

(define-guile-log <format>
  (syntax-rules ()
    ((_ (cut s p cc) stream str a ...)
     (begin 
       (format stream str (gp->scm a s) ...)
       (parse<> (cut s p cc) <cc>)))))
#;(log-code-macro '<format>)

(define-guile-log <code>
  (syntax-rules ()
    ((_ (cut s p cc) code ...)
     (fl-let (cut s p cc)
       (begin code ... (parse<> (cut s p cc) <cc>))))))
#;(log-code-macro '<code>)

(define-syntax parse<>
  (lambda (x)
    (syntax-case x ()
      ((_ . l)
       ;(pp `(parse<> ,@(syntax->datum (syntax l))))
       (syntax (parse2<> . l))))))

(define-syntax parse2<>
  (lambda (x)
    (syntax-case x  (if @ when fast-when cond else case let let* letrec)      
      ((_ meta (match- x (p . u) ...))
       (eq? (syntax->datum #'match-) 'match)
       #'((@ (ice-9 match) match) x (p (<and> meta . u)) ...))

      ((_ meta ((@ (ice-9 -match) --match) x (p . u) ...))
       (and
        (eq? (syntax->datum #'ice-9)   'ice-9)
        (eq? (syntax->datum #'-match)  'match)
        (eq? (syntax->datum #'--match) 'match))

       #'((@ (ice-9 match) match) x (p (<and> meta . u)) ...))
      
      ((_ meta (letrec . l))       
       #'(<letrec> meta . l))

      ((_ meta ((@ (guile) letrec-) . l))
       (and
        (eq? (syntax->datum #'guile)   'guile)
        (eq? (syntax->datum #'letrec-) 'letrec))

       #'(<letrec> meta . l))

      ((_ meta (let lp x ...))
       (symbol? (syntax->datum #'lp))
       #'(<recur> meta lp x ...))

      ((_ meta ((@ (guile) let-) lp x ...))
       (and
        (eq? (syntax->datum #'guile) 'guile)
        (eq? (syntax->datum #'let-)   'let)
        (symbol? (syntax->datum #'lp)))
       #'(<recur> meta lp x ...))
               
      ((_ meta (let . l)) 
       #'(<let> meta . l))

      ((_ meta ((@ (guile) let-) . l))
       (and
        (eq? (syntax->datum #'guile) 'guile)
        (eq? (syntax->datum #'let-)   'let))

       #'(<let> meta . l))

      ((_ meta (let* . l)) 
       #'(<let*> meta . l))

      ((_ meta ((@ (guile) let*-) . l))
       (and
        (eq? (syntax->datum #'guile) 'guile)
        (eq? (syntax->datum #'let*-) 'let*))

       #'(<let*> meta . l))

      ((_ meta (if p . l)      ) 
       #'(<scm-if> meta p . l))

      ((_ meta ((@ (guile) if-) p . l)      )
       (and
        (eq? (syntax->datum #'guile) 'guile)
        (eq? (syntax->datum #'if-)   'if))

       #'(<scm-if> meta p . l))

      ((_ meta (when p . l)    ) 
       #'(<scm-if> meta p (<and> . l)))

      ((_ meta ((@ (guile) when-) p . l)    )
       (and
        (eq? (syntax->datum #'guile) 'guile)
        (eq? (syntax->datum #'when-)   'when))

       #'(<scm-if> meta p (<and> . l)))

      ((_ meta (cond (else a ...) . l))
       #'(<and> meta a ...))

      ((_ meta ((@ (guile) cond-) (else a ...) . l))
       (and
        (eq? (syntax->datum #'guile) 'guile)
        (eq? (syntax->datum #'let-)  'cond))

       #'(<and> meta a ...))

      ((_ meta (cond (p a ...) . l))
       #'(<scm-if> meta p 
            (<and> a ...)
            (cond . l)))

      ((_ meta ((@ (guile) cond-) (p a ...) . l))
       (and
        (eq? (syntax->datum #'guile) 'guile)
        (eq? (syntax->datum #'cond-) 'cond))

       #'(<scm-if> meta p 
            (<and> a ...)
            (cond . l)))

      ((_ meta (cond))
       #'(parse2<> meta <fail>))
      
      ((_ meta ((@ (guile) cond-)))
       (and
        (eq? (syntax->datum #'guile) 'guile)
        (eq? (syntax->datum #'cond-) 'cond))
       #'(parse2<> meta <fail>))

      ((m meta (case q (p a ...) ...))
       (with-syntax ((y (datum->syntax #'m (gensym "x"))))
          (with-syntax (((p ...) (map (lambda (x)
					(syntax-case x (else)
					  (else x)
					  ((a ...) #'(or (eq? y a) ...))
					  (a       #'(or (eq? q a)))))
				      #'(p ...))))
              #'(<let> meta ((y q))
		  (cond (p a ...) ...)))))

      ((m meta ((@ (guile) case-) q (p a ...) ...))
       (and
        (eq? (syntax->datum #'guile) 'guile)
        (eq? (syntax->datum #'case-) 'case))

       (with-syntax ((y (datum->syntax #'m (gensym "x"))))
          (with-syntax (((p ...) (map (lambda (x)
					(syntax-case x (else)
					  (else x)
					  ((a ...) #'(or (eq? y a) ...))
					  (a       #'(or (eq? q a)))))
				      #'(p ...))))
              #'(<let> meta ((y q))
		  (cond (p a ...) ...)))))
       
      ;; And code macros are special
      ((_ meta (f a ...)       )
       (and-log-macro? #'f)
       #'(f meta (f a ...)))

      ((_ meta f       )
       (and-log-macro? #'f)
       #'(f meta ()))

      ;;guile-log macro or function application
      ((_ meta (f ...)         ) 
       #'(dispatch meta (f ...))))))

(define-syntax dispatch
  (lambda (x)
    (syntax-case x ()
      ((_ w (n . l))       
       ;(pk (syntax->datum (syntax (n w . l))))
       (if (guile-log-macro? (syntax n))
           (syntax (n w . l))
           (syntax (<%fkn%> w n . l)))))))

(define-syntax <with-guile-log>
  (syntax-rules ()
    ((_ (scut cut s p cc) code ...) 
     (fl-let (cut s p cc)
	 (syntax-parameterize ((SCUT (identifier-syntax scut)))
            (parse<> (cut s p cc) (<and> code ...)))))

    ((_ (cut s p cc) code ...) 
     (fl-let (cut s p cc)
	 (syntax-parameterize ((SCUT   (identifier-syntax s)))
	    (parse<> (cut s p cc) (<and> code ...)))))
    ((_ (s p cc) code ...)
     (<with-guile-log> (p s p cc) code ...))))

(define <next> (lambda x x))

(define-guile-log <recur>
  (syntax-rules ()
    ((_ (cut s p cc) n ((w v) ...) code ...)
     (letrec ((n (lambda (ss pp cccc w ...)
                   (gp-gc)
                   (<with-guile-log> (cut ss pp cccc) 
                     (<and> code ...)))))
       (parse<> (cut s p cc)
         (n v ...))))))

(define-guile-log <letrec>
  (syntax-rules ()
    ((_ (cut s p cc) ((v . lam) ...) code ...)
     (fl-let (cut s p cc)
        (letrec ((v . lam) ...)
           (parse<> (cut s p cc)
                (<and> code ...)))))))

(define-syntax get-module 
  (lambda (x)
    #`'#,(datum->syntax #'<next> 
                        (module-name (current-module)))))

(define-syntax <define>
  (syntax-rules ()
    ((_ (name . a) code ...)
     (define name
       (letrec ((name (lambda (<S> <Cut> <CC> . a)
                        (gp-gc)
                        (<with-guile-log> (<S> <Cut> <CC>) 
                           (<and> code ...)))))
         (set-procedure-property! name 'module get-module)
         (set-procedure-property! name 'shallow #t)
         name)))))

(define-syntax <define*>
  (syntax-rules ()
    ((_ (name . a) code ...)
     (define name
       (letrec ((name (lambda* (<S> <Cut> <CC> . a)
                        (gp-gc)
                        (<with-guile-log> (<S> <Cut> <CC>) 
                           (<and> code ...)))))
         (set-procedure-property! name 'module get-module)
         (set-procedure-property! name 'shallow #t)
         name)))))


(define delayers (@@ (logic guile-log code-load) *delayers*))
(define (get-del l old)
  (let lp ((l l) (r '()))
    (if (eq? l old)
	r
	(lp (cdr l) (cons (car l) r)))))

(<define> (dls old)
  (<recur> lp ((l (get-del (fluid-ref delayers) old)))
    (if (pair? l)
	(<let> ((x (car l)))
	 (<apply> (car x) (cdr x))
	 (lp (cdr l)))
	(<code> (fluid-set! delayers old)))))

(define (dls-wrap- cut s p cc code)
  (let* ((old  (fluid-ref delayers))
	 (p2   (lambda ()
		 (fluid-set! delayers old)
		 (p)))
	 (cut2 (lambda ()
		 (fluid-set! delayers old)
		 (cut))))
	      
    (fl-let (cut2 s p2 cc)
       (<and> (cut2 s p2 cc)		    
	      (code)
	      (if (eq? (fluid-ref delayers) old)
		  (if (eq? P p2)
		      (<with-fail> p <cc>)
		      <cc>)
		  (<and>
		   (dls old)
		   (if (eq? P p2)
		       (<with-fail> p <cc>)
		       <cc>)))))))

(define-syntax-rule (dls-wrap (cut s p cc)  code)
   (dls-wrap- cut s p cc  (<lambda> () code)))

(define-syntax-rule (dls-match (cut s p cc) old code ...)
  (dls-match- cut s s p cc old 
     (<lambda> (cut scut) (<with-cut> cut scut code ...))))

(define (dls-match- cut scut s p cc old code)
    (<and> (cut s p cc)       
      (if (eq? (fluid-ref delayers) old)
	  <cc>
	  (dls old))
      (code cut scut)))

(define *depth* (make-fluid))

(define-guile-log <match>
  (syntax-rules ()
    ((_ wc as ...)
     (match0 (+ '<match> #f) wc as ...))))
          
(define-syntax match0
  (syntax-rules ()
    ((_ (m n dd) wc (#:dual . u) . l)
     (match0 (m n #t) wc u . l))
    ((_ (m n dd) wc (#:mode m0 . u) . l)
     (match0 (m0 n dd) wc u . l))
    ((_ (m n dd) wc (#:name n0 . u) . l)
     (match0 (m 'n0 dd) wc u . l))
    ((_ meta wc () v . l)
     (mk-syms 1 v () (find-last0 v wc meta l)))))

(define-syntax <lambda>
  (syntax-rules ()
    ((_ as code ...)
     (lambda (<S> <Cut> <CC> . as)
       (gp-gc)
       (<with-guile-log> (<S> <Cut> <CC>) 
         (<and> code ...))))))

(define-syntax <lambda*>
  (syntax-rules ()
    ((_ as code ...)
     (lambda* (<S> <Cut> <CC> . as)
       (gp-gc)
       (<with-guile-log> (<S> <Cut> <CC>) 
         (<and> code ...))))))

(define-syntax <case-lambda>
  (syntax-rules ()
    ((_ (as code ...) ...)
     (case-lambda 
      ((<S> <Cut> <CC> . as)
       (gp-gc)
       (<with-guile-log> (<S> <Cut> <CC>) 
	 (<and> code ...)))
      ...))))


(define-syntax <<lambda>>
  (lambda (x)
    (syntax-case x ()
      ((_ (a ...  code) ...)
       (with-syntax (((b ...) (map (lambda x (datum->syntax #'q (gensym "q")))
				   (car #'((a ...) ...))))
		     ((r ...) (map (lambda x #'_) (car #'((a ...) ...)))))
         #'(lambda (<S> <Cut> <CC> b ...)
             (gp-gc)
	     (<with-guile-log> (<S> <Cut> <CC>) 
	       (<match> (#:mode + #:name '<<lambda>>) (b ...)
	         (a ... (<cut> code)) ... (r ... (<cut> <fail>))))))))))


(define-syntax-rule (map2 f (a ...) b)
  (map (lambda (a ...) (map (lambda (a ...) f) a ...)) b))

(define-syntax <<case-lambda>>
  (lambda (x)
    (syntax-case x ()
      ((_ ((as ... codes) ... (a ... code)) ...)
       #'(<<case-lambda>> <<case-lambda>> 
			  ((as ... codes) ... (a ... code)) ...))
      ((_ nm ((as ... codes) ... (a ... code)) ...)
       (identifier? #'nm)
       (with-syntax ((((b ...) ...) 
		      (map2 (datum->syntax #'q (gensym "q"))
			    (l)
			    #'((a ...) ...)))
		     (((m ...) ...)
		      (map2 #'_
			    (l)
			    #'((a ...) ...))))
	 #'(case-lambda
	    ((<S> <Cut> <CC> b ...)
             (gp-gc)
	     (<with-guile-log> (<S> <Cut> <CC>) 
               (<match> (#:dual #:mode + #:name nm) (b ...)
                 (as ... codes) 
		 ... 
                 (a  ... (<cut> code))
		 (m  ... (<cut> <fail>)))))
	    
	    ...))))))



(define-syntax find-last0
  (syntax-rules ()
    ((_ args v wc me as)
     (mk-syms 11 as () (find-last args v () as wc me)))))

(define-syntax find-last
  (lambda (x)
    (syntax-case x ()
      ((_ . l) 
       ;(pk `(find-last ,@(syntax->datum #'l)))
       (syntax (find-last* . l))))))

(define-syntax find-last*
  (lambda (x)
    (syntax-case x ()      
      ((_  pr       args       v ()   ((a)        ...) wc me)
       (syntax (find-last** me pr args v  ((a) ...)  ()   wc)))

      ((_  pr       args       v ()   ((a as ...) ...) wc me)
       (syntax (find-last  pr       args    v ((a) ...) 
                           ((  as ...) ...) wc me)))
      
      ((_  pr        args      v ((m ...) ...) ((e) ...) wc me)
       (syntax (find-last** me pr args v ((m ...) ...) () 
                            ((e) ...) () wc)))

      ((_ pr args v ((b ...  ) ...) ((a as ...) ... ) wc me)
       (syntax (find-last  pr args v ((b ... a) ...) 
                           ((as   ...) ... ) wc me)))
      ((_ pr args v ((b ...  ) ...) ((as ...) ... ) wc me)
       (error "<match> <<define>> or such has an unbalanced amount of matchers")))))
      

(define-syntax find-last**
  (syntax-rules ()
    ((_ (m nm dd) (pr ...) args v ((a)) ((b) ...) (cut s p cc))
     (let ((del (fluid-ref delayers)))
       (umatch (#:clear del #:dual dd #:mode m #:status s 
		#:tag <nex> #:name nm) ()
	       ((dls-match (cut s <nex> cc) del b))
	       ...
	       ((dls-match (cut s p      cc) del a)))))

    ((_ m pr args v (a aa ...) (b ...) wc)
     (find-last** m pr args v (aa ...) (b ... a) wc))


    ((_ (m nm dd) (pr ...) args v ((as ...)) ((aas ...)  ...) ((a)) ((b) ...)  
        (cut s p cc))
     (let ((del (fluid-ref delayers)))
       (umatch (#:clear del #:dual dd #:mode m #:status s #:tag <next> 
		#:name nm)
         v
	 (aas ... (dls-match (cut s <next> cc) del b))
	 ...
             
	 (as ...  (dls-match (cut s p cc) del a)))))

    ((_ m pr args v ((as ...) (aas ...) ...) ((aass ...)  ...) ((a) (aa) ...) 
        ((b) ...)  wc)
     (find-last** m pr args v ((aas ...) ...) ((aass ...) ... (as ...)) 
                  ((aa) ...) ((b) ... (a)) wc))))


(define-syntax let<>0
  (syntax-rules ()
    ((_ (cut s p cc) (m pat val) code)
     (let ((ss s))
       (umatch (#:status ss #:mode m #:name 'let<>/<=>/<==>) (val)
	       (pat (parse<> (cut ss p cc) (<with-fail> p code)))
	       (_   (parse<> (cut ss p cc) <fail>)))))))

(define-guile-log let<>
  (syntax-rules ()
    ((_ w (a b ...) code ...)
     (let<>0 w a (let<> w (b ...) code ...)))
    ((_ w () code ...)
     (parse<> w (<and> code ...)))))


(define (tr-pat x)
  (syntax-case x (quote unquote)
    ((quote   _) x)
    ((unquote _) x)
    ((x . l)     (cons (tr-pat #'x) (tr-pat #'l)))
    (()          #'())
    (x
     (eq? '_ (syntax->datum #'x))
     #'x)
    ( x          #'(unquote x))))

(define-syntax letify
  (lambda (x)
    (syntax-case x ()
      ((_ w ((m f) pat val) code ...)
       #`(let<>0 w (m #,(tr-pat #'pat) (f val)) code ...))

      ((_ w (m pat val) code ...)
       #`(let<>0 w (m #,(tr-pat #'pat) val) code ...)))))
        
;;TODO, this will unify with a cyclic check!
;;Not possible to use a pure raw form here

     
(define-guile-log <=>
  (syntax-rules ()
    ((_ wc X Y)
     (dls-wrap wc (<=>q (gp-unify! +) X Y)))))

(define-guile-log <r=>
  (syntax-rules ()
    ((_ wc X Y)
     (dls-wrap wc (<=>q (gp-unify-raw! ++) X Y)))))

(define-guile-log <==>
  (syntax-rules ()
    ((_ wc X Y)
     (dls-wrap wc (<=>q (gp-m-unify! -) X Y)))))

(define-guile-log <unify>
  (syntax-rules ()
    ((_ (cut s p cc) unify X Y)
     (let* ((ss s)
	    (ss (unify X Y ss)))
       (<when> (cut ss p cc) ss <cc>)))))
#;(log-code-macro '<unify>)

(define-guile-log <=>q
  (syntax-rules ()
    ((_ w . l)
     (fl-let w (<=>qq w . l)))))

(define-syntax <=>qq
  (syntax-rules (unquote quote _)
    ((_ wc q _   x)
     (parse<> wc <cc>))

    ((_ wc q x   _)
     (parse<> wc <cc>))

    ((_ wc q ()  ()) 
     (parse<> wc <cc>))
    
    ((_ wc q ()  X)
     (<=>q wc q '() X))

    ((_ wc q X   ())
     (<=>q wc q '() X))

    ((_ wc q (X) (Y))
     (<=>q wc q X Y))

    ;;unquote logic so that we can evaluate forms in the unification
    ((_ wc (unify m) (unquote X)  (unquote Y))
     (<unify> wc unify X Y))

    ((_ meta (unify m) (unquote X)  (quote   Y))
     (<unify> meta unify X (quote Y)))

    ((_ wc (u m)     (unquote X)  (Y . L)    )   
     (letify wc (m (Y . L) X) <cc>))

    ((_ wc (unify m) (unquote X)  Y          )
     (<unify> wc unify X Y))

    ((_ wc q Y            (unquote X))   
     (<=>q wc q (unquote X) Y))

    ;; quote logic to protect quoted forms from destructioning
    ((_ wc (unify m) (quote X)  (quote Y))
     (<unify> wc unify (quote X) (quote Y)))

    ((_ wc (u m)     (quote X)  (Y . L)    ) 
     (letify wc (m (Y . L) (quote X)) <cc>))

    ((_ wc (unify m) (quote X)  Y          )  
     (<unify> wc unify (quote X) Y))

    ((_ wc q Y         (quote X))  
     (<=>q wc q (quote X) Y))


    ((_ wc q     (X . Lx) (Y . Ly))
     (parse<> wc (<and> (<=>q q X Y) (<=>q q Lx Ly))))

    ((_ wc (u m)     X        (Y . Ly))
     (letify wc (m (Y . Ly) X) <cc>))

    ((_ wc (u m)     (X . Lx)    Y)
     (letify wc (m (X . Lx) Y) <cc>))

    ((_ wc (unify m) X Y)
     (<unify> wc unify X Y))))

(define-guile-log </.>
  (syntax-rules ()
    ((_ code ...) (<lambda> () code ...))))
 

(define (<funcall> S P CC F . L) 
  (apply (gp-lookup F S) S P CC L))

(define (->list s l)
  (let ((l (gp-lookup l s)))
    (if (pair? l)
	(cons (car l) (->list s (cdr l)))
	(if (gp-pair- l s)
	    (cons (gp-lookup (gp-car l s) s) (->list s (gp-cdr l s)))
	    '()))))

(define-syntax def00
  (lambda (x)
    (syntax-case x ()
      ((_ (f keyw mode a ...) (m ... code) ...)
       (eq? (syntax->datum #'keyw) #:mode)
       (with-syntax (((q ...) (map (lambda x #'_) #'(a ...))))
         #'(<define> (f a ...)
	      (<match> (#:mode mode #:name f) (a ...)
	        (m  ... (<cut> code)) ...
	        (q  ... (<cut> <fail>)))))))))

(define-syntax-rule (<def>  (f . a) . l) (def00 (f #:mode + . a) . l))
(define-syntax-rule (<def-> (f . a) . l) (def00 (f #:mode - . a) . l))

(define-syntax <<define>>
  (syntax-rules ()
    ((_ #:mode mode (f a ...) (m ... code) ...)
     (<define> (f a ...)
         (<match> (#:mode mode #:name f) (a ...)
           (m  ... (<cut> code)) ...)))

    ((_ (f a ...) (m ... code) ...)
     (<define> (f a ...)
       (<match> (#:name f) (a ...)
         (m  ... (<cut> code)) ...)))))

(define-syntax-rule (<<define->> (f . a) . l)
  (<<define>> #:mode - (f . a) . l))

(define-guile-log <dynwind>
  (syntax-rules ()
    ((_ (cut s p cc) redo undo)
     (begin
       (gp-dynwind
	redo
	(lambda () #f)
	undo
	s)
       (cc s p)))))
#;(log-code-macro '<dynwind>)

;; This is code that allow to store a state
(define (pkk x) (pk (vector-ref (car (cdr x)) 3)) x)
(define (<state-ref>)
  (gp-store-state-all (fluid-ref *current-stack*)))

(define (<state-set!> state)
  (gp-restore-state state))

(define-guile-log <lv*>
  (syntax-rules ()
    ((_ meta (((v ...) (f ...)) c ...) code ...)
     (parse<> meta 
       (<and> (f ... v ...)
              (<lv*>  (c ...) code ...))))
    ((_ meta () code ...)
     (parse<> meta (<and> code ...)))))

(define (<clear>)
  (gp-clear (fluid-ref *current-stack*)))


#;(log-code-macro '<fail>)
#|
MAKE SURE TO REVISIT THIS IDEA LATER
(define-guile-log <leancall> 
  (lambda (x)
    (syntax-case x ()
      ((_ (cut s p cc) (w ...) f . l)
       (with-syntax (((ww ...) (generate-temporaries #'(w ...))))
         #'(f s p (lambda (s pp)
                    (let-values* (((ww s) (gp-lookup-clean w s)) ...)
                       (set! w ww) ...
                       (parse<> (cut s pp cc) <cc>))) . l))))))
|#


(define-guile-log <syntax-parameterize>
  (syntax-rules ()
    ((_ w a code ...)
     (syntax-parameterize a 
       (parse<> w (<and> code ...))))))

(define-guile-log <windlevel>
  (syntax-rules ()
    ((_ (cut s p cc) code ...)
     (let ((ss (gp-new-wind-level s)))
       (parse<> (cut ss p cc))))))

   
(define-syntax <%p-values%>
  (lambda (x)
  (syntax-case x ()
    ((_ (cut s pr cc) (((P p) ...) vars (fkn . l))  e2 ...)
     #'(parse<> (cut s pr (lambda (ss pr2 p ... . vars) 
                            (syntax-parameterize
                             ((P  (lambda (x)
                                    (syntax-case x ()
                                      ((x . l) 
                                       #'(p . l))
                                      (x #'p))))
                              ...)
                             (parse<> (cut ss pr2 cc) 
                                (<and> e2 ...)))))
                (fkn P ... . l)))
    ((a . l)
     (error 
      (format #f "wrong application of ~a in <and> like constructs of ~a" 
              (syntax->datum #'a)
              (syntax->datum x)))))))

(define-syntax <%q-values%>
  (lambda (x)
  (syntax-case x ()
    ((_ (cut s pr cc) (((P p) ...) vars a ...) e2 ...)
     #'(parse<> (cut s pr (lambda (ss pr2 p ... . vars) 
			  (syntax-parameterize
			   ((P  (lambda (x)
				  (syntax-case x ()
				    ((x . l) 
				     #'(p . l))
				    (x #'p))))
			    ...)
			   (parse<> (cut ss pr2 cc) 
				    (<and> e2 ...)))))
	      (<and> a ...)))
    ((a . l)
     (error 
      (format #f "wrong application of ~a in <and> like constructs of ~a" 
              (syntax->datum #'a) (syntax->datum x)))))))


(define-syntax define-guile-log-parser-tool
  (lambda (x)
    (syntax-case x ()
      ((_ (lam (X ...)) def .. xx cc)
       (with-syntax (((p ...) (generate-temporaries #'(X ...))))
	 #'(begin
	     (define-syntax-parameter X
	       (lambda  (x) (error "guile-log scanner var not bound")))
	     ...
	     
	     (define-syntax-rule (lam q . code)
	       (<lambda> (p ... . q)
	        (<syntax-parameterize> ((X  (lambda (x)
					   (syntax-case x ()
					     ((x . l) 
					      #'(p . l))
					     (x #'p))))
				     ...)
		   . code)))
	     
	     (define-syntax-rule (def (f . q) . code)
	       (define f (lam q . code)))
	     
	     (define-and-log ..
	       (syntax-rules ()
		 ((_ (cut s pr cc) (_ (fkn . l)))
		  (fkn s pr cc X ... . l))
		 ((_ w (_ () fkn) . es)
		  (<%p-values%> w (((X p) ...) () fkn) . es))
		 ((_ w (_ (x . l) fkn) . es)
		  (<%p-values%> w (((X p) ...) (x . l) fkn) . es))
		 ((_ w (_ x fkn) . es)
		  (<%p-values%> w (((X p) ...) (x)     fkn) . es))))

	     (define-and-log xx
	       (syntax-rules ()
		 ((_ w (_ () . as) . es)
		  (<%q-values%> w (((X p) ...) () . as) . es))
		 ((_ w (_ (x . l) . as) . es)
		  (<%q-values%> w (((X p) ...) (x . l)  . as) . es))
		 ((_ w (_ x . as) . es)
		  (<%q-values%> w (((X p) ...) (x)      . as) . es))))

	     (define-and-log  cc 
	       (syntax-rules ()
		 ((_ (cut s pp ccc) (_ . l))
		  (ccc s pp X ... . l))))))))))

(define-guile-log <with-bind> 
  (lambda (x)
    (syntax-case x ()
      ((_ w ((X I) ...) code ...)
       (with-syntax (((id ...) (generate-temporaries #'(X ...))))
         #'(<let> w ((id I) ...)
             (<syntax-parameterize> ((X (lambda x #'id)) ...)
               (<and> code ...))))))))
  	     
(define-syntax-rule (<define-guile-log-rule> (f a ...) code ...)
  (define-guile-log f
    (syntax-rules ()
      ((_ w a ...)
       (<with-guile-log> w code ...)))))

(define-syntax-rule (attvar?  x) (gp-attvar? (gp-lookup x S) S))
(<define> (<attvar?> x)      (if (gp-attvar? x S) <cc> <fail>))
(<define> (<attvar-raw?> x)  (if (gp-attvar-raw? x S) <cc> <fail>))
(<define> (<put-attr> x m v) 
   (<let> ((s (gp-put-attr x m v S)))
     (<with-s> s <cc>)))

(<define> (<put-attr-guarded> x m v) 
  (<let> ((s (gp-put-attr-guarded x m v S)))
     (<with-s> s <cc>)))

(<define> (<put-attr-weak-guarded> x m v) 
  (<let> ((s (gp-put-attr-weak-guarded x m v S)))
     (<with-s> s <cc>)))

(<define> (<put-attr!> x m v) 
   (<code> (gp-put-attr! x m v S)))

(<define> (<put-attr-guarded!> x m v) 
  (<code> (gp-put-attr-guarded! x m v S)))

(<define> (<put-attr-weak-guarded!> x m v) 
  (<code> (gp-put-attr-weak-guarded! x m v S)))

(<define> (<get-attrs> x m v)
  (<let> ((x (<lookup> x)))
     (when (gp-attvar-raw? x S)
	   (<let> ((l (gp-att-data x S)))
	 (<recur> lp ((l l))
           (<match> (#:mode ++) (l)
              (((,m . ,v) . l)
	       (<cut> (<or> <cc> (lp l))))
	      (_ (<cut> <fail>))))))))

(<define> (<raw-attvar> x)
  (<let> ((x (gp-att-raw-var x S)))
    (when x (<cc> x))))

(<define> (<get-attr> x m v)
  (<let*> ((x   (<lookup> x))
	   (ret (gp-get-attr x m S)))
     (when ret 
       (doit_off)
       (<r=> v ,(gp-lookup-1 ret S))
       (doit_on))))
(<define> (<del-attr!> x m) (<code> (gp-del-attr! x m S)))

(<define> (<del-attr> x m) 
  (<let> ((s (gp-del-attr x m S)))
    (if s (<with-s> s <cc>) <cc>)))

(<define> (test_attr x a)
  (<recur> lp ((data (<lookup> (gp-att-data (<lookup> x) S))))
     (if (pair? data)
	 (<if> ((caar data) a)
	       <cc>
	       (lp (cdr data)))
         <fail>)))


(define (tr-meta f fnew)     
  (define (sieve l)
    (let lp ((l l))
      (if (pair? l)
	  (if (eq? (caar l) 'arity)
	      (lp (cdr l))
	      (cons (car l) (lp (cdr l))))
	  '())))
  (set-object-properties! fnew (object-properties f))
  (if (procedure? f)
      (set-procedure-properties! fnew (sieve (procedure-properties f))))
  fnew)

(define-syntax-rule (functorize f g l ...)
  (let ((goal g)
	(fu   f))
    (let ((res (tr-meta fu (<lambda> x (<apply> goal fu l ... x)))))
      res)))

 
(define-syntax-rule (adaptable_vars f)
  (fluid-let-syntax ((<var> (syntax-rules () 
			      ((_ . l) (<modvar> . l)))))
    f))

(define-syntax <ground?>
  (syntax-rules ()
    ((_ s x)     
     (let lp ((y x))
       (let ((y (gp-lookup y s)))
	 (cond
	  ((<var?> s y)
	   #f)
	  ((pair? y)
	   (and (lp (car y)) (lp (cdr y))))
	  ((vector? y)
	   (lp (vector->list y)))
	  (else
	   #t)))))
    ((_ x) (<ground?> S x))))
    
(<define> (<set> x y)  	 
   (if (gp? x)
       (<with-s> (gp-set! x y S) <cc>)))

(<define> (<set0> x y)  	 
   (if (gp? x)
       (<with-s> (gp-set-1! x y S) <cc>)))

(<define> (<set!> x y)  	 
   (if (gp? x)
       (<code> (gp-var-set! x y))))
       


(<define-guile-log-rule> (<<match>> a b (pat ... l) ...)
  (<let> ((p P))
    (<match> a b (pat ... (<with-fail> p l)) ...)))

(<define> (attributeU val var x)
  (let ((var (<lookup> var))
        (val (<lookup> val))
        (x   (<lookup> x)))
    (cond
     ((and (<var?> val) (not (gp-attvar-raw? val S)))
      (if (eq? val var)
          <cc>
          (<set> val var)))
     
     ((gp-attvar-raw? var S)
      (<and>
       (if (gp-attvar-raw? val S)
           (<if> (test_attr var "nonvar")
                 (<if> (test_attr val "nonvar")
                       <cc>
                       (let ((temp var))
                         (<code> (set! var val) (set! val temp))))
                 <cc>)
           <cc>)
       (let* ((item (<lookup> (gp-att-raw-var var S))))
         (cond
          ((eq? val var)
           <cc>)
          (else
           (let* ((attdxy (gp-att-data var S))
                  (attdx
                   (let lp ((l attdxy))
                     (if (pair? l)
                         (if (pair? (caar l))
                             (if (caaar l)
                                 (cons (car l) (lp (cdr l)))
                                 (lp (cdr l)))
                             (lp (cdr l)))
                         '())))
                  (attdy
                   (let lp ((l attdxy))
                     (if (pair? l)
                         (if (pair? (caar l))
                             (if (cdaar l)
                                 (cons (car l) (lp (cdr l)))
                                 (lp (cdr l)))
                             (cons (car l) (lp (cdr l))))
                         '()))))
             
             (if x
                 (<set> var val)
                 <cc>)
                
             (<recur> lp ((d attdx) (g '()))
               (if (pair? d)
                   (if (pair? (car d))
                       (<var> (gg)
                         (let ((lam (caaar d)))			    
                           (if (object-property 
                                lam 
                                (@@ (logic guile-log code-load) delayed-id))
                               (lam (cdar d) val gg x)	       
                               (lam (cdar d) val gg x))   
                           (lp (cdr d) (cons gg g))))
                       (error 
                        "Bugger Error, wring format in attribute" (car d)))

                   (<recur> lp ((d attdy))
                     (if (pair? d)
                         (if (pair? (car d))
                             (let* ((lam (caar d))
                                    (lam (if (pair? lam) (cdr lam) lam)))
                               (if (object-property 
                                    lam 
                                    (@@ (logic guile-log code-load)
                                        delayed-id))
                                   (lam (cdar d) val x)	       
                                   (lam (cdar d) val x))	       
                               (lp (cdr d)))
                             (error 
                              "Bugger Error, wring format in attribute"
                              (car d)))
                         (<recur> lp ((g g))
                           (if (pair? g)
                               (<and> ((car g)) (lp (cdr g)))
                               <cc>))))))))))))
     (else
      (if x
          (if (eq? var val)
              <cc>
              (<=>  var val))
          (<==> val var))))))
		 
(variable-set! (@@ (logic guile-log code-load) attributeU) attributeU)

(define-syntax-rule (<with-log-in-code> code ...)
  (let ((p  (lambda () #f))
	(cc (lambda x #t))
	(s  (fluid-ref *current-stack*)))
    (parse<> (p s p cc) (<and> code ...))))


(define build_attribut_representation #f)

