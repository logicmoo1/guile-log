(define-module (logic guile-log attributed)
  #:use-module (logic guile-log procedure-properties)
  #:use-module (logic guile-log code-load)
  #:use-module (logic guile-log umatch)
  #:use-module (ice-9 format)
  #:use-module (ice-9 match)
  #:export (add-attribute-printer
	    attribute-printer-ref
	    get-all-attributed-variables
	    att-printer
	    delayed-attribute
	    ref-attribute-projector
	    set-attribute-projector!)

  #:re-export (		 
	       gp-attvar?
	       gp-attvar-raw?
	       gp-put-attr
	       gp-put-attr-guarded
	       gp-put-attr-weak-guarded
	       gp-get-attr
	       gp-del-attr
	       gp-att-data
	       gp-att-raw-var	       
	       multibute
	       ))

(define attvars '() #;(gp-make-weak-list))
(define gp-put-attr-old gp-put-attr)
(define (gp-put-attr v att data s)
 #;(when (not (gp-attvar-raw? v s))
     (wl-add v))
  (gp-put-attr-old v att data s))

(define (get-all-attributed-variables)
  (let lp ((l (car (fluid-ref attvars))))
    (let ((x (car l)))
      (if (gp-attvar-raw? x (fluid-ref *current-stack*))
	  (cons x (lp l))
	  (lp l)))))

(define *printers* (make-weak-key-hash-table))
(define (default-printer lam x s)
  (format #f "(~a : ~a)" lam x))

(define (add-attribute-printer lam printer)
  (hashq-set! *printers* lam printer))
(define (attribute-printer-ref lam)
  (hashq-ref *printers* lam default-printer))

(define (att-printer port x s)
  (format port "@<~{~a,~}>" 
	  (map (lambda (x) 
		 ((attribute-printer-ref (car x)) (car x) (cdr x) s))
	       (gp-att-data x s))))

(define (delayed-attribute lam)
  (set-object-property! lam (@@ (logic guile-log code-load) delayed-id) #t))

(define (set-attribute-projector! x f)
   (set-object-property! x 'projector f))
(define (ref-attribute-projector x)
   (object-property x 'projector))
