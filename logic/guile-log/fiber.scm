(define-module (logic guile-log fiber)
  #:use-module (fibers)
  #:use-module (fibers scheduler)
  #:use-module (fibers channels)
  #:use-module (logic guile-log dynamic-features)
  #:use-module (logic guile-log)
  #:export (scheduler <with-fibers> <spawn-fiber> <sleep>
                      <make-channel> <put-message> <get-message>))


(define scheduler
  (let ((scheduler #f))
    (case-lambda 
      (()     (serialize-scheduler scheduler))
      ((state)
       (if ((@@ (fibers scheduler) scheduler?) state)
           (set! scheduler state)
           (try-update-scheduler scheduler state))))))

(<wrap> add-parameter-dynamics scheduler)

(<define> (<with-fibers> thunk ret)
  (<=> ret
       ,(run-fibers 
         (lambda ()
           (scheduler (current-scheduler))
           (thunk S (lambda () #f) (lambda x #t))))))

(define (mk-fiber f)
  (case-lambda 
    (()  (serialize-fiber f))
    ((s) 
     (if s
         (update-fiber f s)
         f))))

(<define> (<spawn-fiber> fiber <thunk>)
  (let* ((f  (spawn-fiber (lambda () (<thunk> S (lambda () #f) (lambda x #t)))))
         (ff (mk-fiber f)))
    (add-parameter-dynamics ff)
    (<=> fiber ff)))

(<define> (<sleep> n) (<code> (sleep (<lookup> n))))

(define <make-channel>
  (<case-lambda>
   ((ch)   (<=> ch ,(make-channel)))
   ((ch n) (<=> ch ,(make-channel #:queue-size (<lookup> n))))))


(<define> (<put-message> ch message)
  (<var> (out)
    (<=> out ,(<cp> message))              
    (<code> (put-message (<lookup> ch) out))))

(<define> (<get-message> ch message)
  (<=> message ,(get-message (<lookup> ch))))
                 
