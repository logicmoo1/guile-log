(define-module (logic guile-log db vhash)
  #:use-module (ice-9 match)
  #:use-module (oop goops)
  #:use-module (logic guile-log vlist)
  #:use-module (logic guile-log code-load)
  #:use-module (logic guile-log db db)
  #:use-module (logic guile-log db table)
  #:use-module (logic guile-log db objects)
  #:export
  (add-vhash lookup-vhash lookup-rvhash pull-vhashs))
	     
	     
(define-syntax-rule (aif it p . l) (let ((it p)) (if it . l)))

(define make-vhash (@@ (logic guile-log code-load) make-vlist))

(define (add-vhash db vhash)
  (let ((vhashs  (slot-ref db 'vhashs))
	(rvhashs (slot-ref db 'rvhashs))
	(key     (slot-ref db 'vhash-key)))
    (aif it (hash-ref vhashs vhash)
         (car it)
         (let* ((table-id (add-table db (struct-ref vhash 0))))
           (hashq-set! vhashs  vhash (cons* key table-id (struct-ref vhash 1)))
	   (hash-set!  rvhashs key   vhash)
           (slot-set! db 'vhash-key (+ key 1))
	   key))))

(define (lookup-vhash db x)
  (let ((vhashs (slot-ref db 'vhashs)))
    (aif it (hashq-ref vhashs x)
         (car it)
	 (error "no vhash in database"))))

(define (lookup-rvhash db x)
  (let ((vhashs (slot-ref db 'rvhashs)))
    (aif it (hash-ref vhashs x)
         it
	 (error "no vhash in rev database"))))

(define (pull-vhashs db tr trn)
  (let* ((vhashs   (slot-ref db 'vhashs))
	 (rvhashs  (slot-ref db 'rvhashs))
	 (nlast    (slot-ref db 'vhash-last-key))
	 (ncur     (slot-ref db 'vhash-key))
	 (rows     (db-pull-vhashs db nlast))
	 (rowsp    (pair? rows))	 
	 (newlast  (if rowsp
		       (let lp ((rows rows) (n nlast))
			 (match rows
			   (((k . _) . rows)
			    (lp rows (max k n)))
			   (()
			    (+ n 1))))
		       nlast))
	 (newme    (let lp ((i nlast) (l '()))
		     (if (< i ncur)
			 (aif it (hash-ref rvhashs i)
			      (lp (+ i 1)
				  (cons
				   (cons it
					 (hashq-ref vhashs it))
				   l))
			      (lp (+ i 1) l))
			 (reverse l)))))

    (if rowsp
	(let lp ((newme newme))
	  (match newme
	    (((s . (k . _)) . newme)
	     (hashq-remove! vhashs  s)
	     (hash-remove! rvhashs k)
	     (lp newme))
	    (() #t))))
	
    (let lp ((rows rows))
      (match rows
	(((key tid . off) . rows)
	 (let ((m (let ((t (lookup-rtable db tid)))
		    (make-vhash t off))))
			
	   (hash-set! vhashs  m   (cons* key tid off))
	   (hash-set! rvhashs key m)
	   (lp rows)))
	(()
	 #t)))

   (let lp ((newme newme) (l '()) (w '()) (n newlast))
      (match newme
	(((s . (k . v)) . newme)
	 (if rowsp
	     (aif it (hashq-ref vhashs s)
		  (lp newme (cons (cons* s k (car it)) l)
		      w                   n)
		  (lp newme (cons (cons* s k n ) l)
		      (cons (cons* s n v) w) (+ n 1)))
	     (lp newme l
		 (cons (cons* s k v) w) ncur)))
	     
	(()
	 (let lp ((w w) (l '()))
	   (match w
	     (((s n tid . off) . w)
	      (if rowsp
		  (let* ((tid (tr tid))
			 (t   (lookup-rtable db tid))
			 (off (+ (trn t) off)))
		    (let ((v (cons tid off)))
		      (struct-set! s 1 off)
		      (hashq-set! vhashs  s (cons n v))
		      (hash-set! rvhashs n s)
                      (lp w (cons (cons n v) l))))
                  (lp w (cons (cons* n tid off) l))))
	     (()
	      (db-write-vhashs db l))))
	 
	 (slot-set! db 'vhash-key n)
	 (slot-set! db 'vhash-last-key n)

	 (let ((m (make-hash-table)))
	   (let lp ((l l))
	     (match l
	       (((s k1 . k2) . l)
		(hash-set! m k1 k2)
		(lp l))
	       (() #t)))

	   (lambda (k) (hash-ref m k k))))))))

(define (push-vhashs db)
  (let* ((vhashs   (slot-ref db 'vhashs))
	 (rvhashs  (slot-ref db 'rvhashs))
	 (nlast    (slot-ref db 'vhash-last-key))
	 (ncur     (slot-ref db 'vhash-key))
	 (newme    (let lp ((i nlast) (l '()))
		     (if (< i ncur)
			 (aif it (hash-ref rvhashs i)
			      (lp (+ i 1)
				  (cons
				   (cons it
					 (hashq-ref vhashs it))
				   l))
			      (lp (+ i 1) l))
			 (reverse l)))))

   (let lp ((newme newme) (l '()) (w '()) (n nlast))
      (match newme
	(((s . (k . v)) . newme)
	 (lp newme l
	     (cons (cons* s k v) w) ncur))
	     
	(()
	 (let lp ((w w) (l '()))
	   (match w
	     (((s n tid . off) . w)
	      (lp w (cons (cons* n tid off) l)))
	     (()
	      (db-write-vhashs db l))))
	 
	 (slot-set! db 'vhash-key n)
	 (slot-set! db 'vhash-last-key n))))))
	 
(define-values
  (type-add type-from-tp)
  (let ((tpkey (new-object-type)))
    (values
     (lambda (db w)
       (let ((key (add-vhash db w)))
	 (cons tpkey key)))
     (lambda (key) (equal? key tpkey)))))

(define (type-pull db tr-s tr-m tr-d)
  (call-with-values (lambda () (pull-tables db tr-d))
    (lambda (tr tr-n)
      (pull-vhashs db tr tr-n))))

(define (type-push db)
  (push-tables db)
  (push-vhashs db))

(define (type-mk db key)
  (lookup-vhash db key))

(define (type-diff* old new)
  (if (eq? old new)
      '()
      (let ((told (struct-ref old 0))
	    (nold (struct-ref old 1)))
	(let lp ((tnew (struct-ref new 0))
		 (nnew (min (vector-ref (struct-ref new 0) 4)
			    (struct-ref new 1)))
		 (l    '()))
	  (let ((v (vector-ref tnew 0))
		(s (vector-ref tnew 3)))
	    (if (eq? told tnew)
		(let lp2 ((n nnew) (l l))
		  (if (= n nold)
		      (begin
			(vector-set! tnew 4 nold)
			l)
		      (let ((k (vector-ref v (- n 1)))
			    (v (vector-ref v (+ s (- n 1)))))
			(vector-set! v (- n 1) #f)
			(vector-set! v (+ s (- n 1)) #f)
			(lp2 (- n 1) (cons (cons k v)					      
					   l)))))
		(let lp2 ((n nnew) (l l))
		  (if (= n 0)
		      (lp (vector-ref tnew 1) (vector-ref tnew 2) l)
		      (lp2 (- n 1) (cons (cons (vector-ref v (- n 1))
					       (vector-ref v (+ s (- n 1))))
					 l))))))))))

(define (type-append db o l)
  (let lp ((vh (o)) (l l))
    (if (pair? l)
	(lp (vhash-cons (caar l) (cdar l) vh) (cdr l))
	(add-vhash db vh))))

(define (type-diff o old)
  (let ((ret (type-diff* (o) old)))
    (o old)
    ret))
  

(add-db-type vhash?
	     #:mk      lookup-vhash
	     #:from-tp type-from-tp
	     #:pull    type-pull
	     #:add     type-add
	     #:diff    type-diff
	     #:push    type-push
	     #:append  type-append
	     #:default vlist-null)
