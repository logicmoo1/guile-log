(define-module (logic guile-log db string)
  #:use-module (ice-9 match)
  #:use-module (oop goops)
  #:use-module (logic guile-log db db)
  #:export (add-string lookup-string lookup-rstring pull-strings push-strings))

(define-syntax-rule (aif it p . l) (let ((it p)) (if it . l)))

;; Strings
(define (add-string db x)
  (if (not (string? x)) (error "Not a string" x))
  
  (let ((strings  (slot-ref db 'strings))
	(rstrings (slot-ref db 'rstrings)))
    (aif it (hash-ref strings x)
         it
         (let* ((key (slot-ref db 'string-key)))
           (hash-set! strings  x   key)
	   (hash-set! rstrings key x)
	   (slot-set! db 'string-key (+ key 1))
           key))))

(define (lookup-string db x)
  (let ((strings (slot-ref db 'strings)))
    (aif it (hash-ref strings x)
         it
	 (error "no string in database"))))

(define (lookup-rstring db x)
  (let ((strings (slot-ref db 'rstrings)))
    (aif it (hash-ref strings x)
         it
	 (error "no string in database"))))

(define (pull-strings db)
  (let* ((strings  (slot-ref db 'strings))
	 (rstrings (slot-ref db 'rstrings))
	 (nlast    (slot-ref db 'string-last-key))
	 (ncur     (slot-ref db 'string-key))
	 (rows     (db-pull-strings db nlast))
	 (rowsp    (pair? rows))	 
	 (newlast  (if rowsp
		       (let lp ((rows rows) (n nlast))
			 (match rows
			   (((k . s) . rows)
			    (lp rows (max k n)))
			   (()
			    (+ n 1))))
		       nlast))
	 (newme    (let lp ((i nlast) (l '()))
		     (if (< i ncur)
			 (aif it (hash-ref rstrings i)
			      (lp (+ i 1) (cons (cons it i) l))
			      (lp (+ i 1) l))
			 l))))

    ;;Remove the extra from the hashes

    (if rowsp
	(let lp ((newme newme))
	  (match newme
	    (((s . k) . newme)
	     (hash-remove! strings  s)
	     (hash-remove! rstrings k)
	     (lp newme))
	    (() #t))))
	
    (let lp ((rows rows))
      (match rows
	(((key . s) . rows)
	 (hash-set! strings  s key)
	 (hash-set! rstrings key s)
	 (lp rows))
	(()
	 #t)))

    (let lp ((newme newme) (l '()) (w '()) (n newlast))
      (match newme
	(((s . k) . newme)
	 (aif it (and rowsp (hash-ref strings s))
	      (lp newme (cons (cons* s k it) l) w                   n)
	      (lp newme (cons (cons* s k n ) l) (cons (cons s n) w) (+ n 1))))
	(()
	 (let lp ((w w))
	   (match w
	     (((s . n) . w)
	      (hash-set! strings  s n)
	      (hash-set! rstrings n s)
	      (lp w))
	     (() #f)))
	 
	 (slot-set! db 'string-key n)
	 (slot-set! db 'string-last-key n)
	 (db-write-strings db w)

	 (let ((m (make-hash-table)))
	   (let lp ((l l))
	     (match l
	       (((s k1 . k2) . l)
		(hash-set! m k1 k2)
		(lp l))
	       (() #t)))
	   
	   (lambda (k) (hash-ref m k k))))))))

	      
(define (push-strings db)
  (let* ((strings  (slot-ref db 'strings))
	 (rstrings (slot-ref db 'rstrings))
	 (nlast    (slot-ref db 'string-last-key))
	 (ncur     (slot-ref db 'string-key))
	 (newme    (let lp ((i nlast) (l '()))
		     (if (< i ncur)
			 (aif it (hash-ref rstrings i)
			      (lp (+ i 1) (cons (cons it i) l))
			      (lp (+ i 1) l))
			 l))))

    (let lp ((newme newme) (l '()) (w '()) (n nlast))
      (match newme
	(((s . k) . newme)
	 (lp newme (cons (cons* s k n ) l) (cons (cons s n) w) (+ n 1)))
	(()
	 (slot-set! db 'string-key n)
	 (slot-set! db 'string-last-key n)
	 (db-write-strings db w))))))

	      

    

    
