(define-module (logic guile-log db data)
  #:use-module (ice-9 match)
  #:use-module (oop goops)  
  #:use-module (logic guile-log db db)
  #:use-module (logic guile-log db string)
  #:use-module (logic guile-log db module)
  #:export (add-data lookup-data lookup-rdata pull-datas
		     push-datas add-custom))

(define-syntax-rule (aif it p . l) (let ((it p)) (if it . l)))

(define *custom* '())
(define *i*       7)
(define (add-custom f m rref ref)
  (set! *custom* (cons (list f *i* m rref ref) *custom*))
  (set! *i* (+ *i* 1)))

(define (try-custom x)
  (or-map (lambda (f) (and ((car f) x) (cdr f)))  *custom*))

(define (custom-rref i)
  (let lp ((l *custom*))
    (match l
      (((_ ii m rref) . l)
       (if (= i ii)
	   rref
	   (lp l))))))

(define (vec x)
  (cons 0 x))    
(define (con x)
  (cons 1 x))    
(define (pro x)
  (cons 2 x))    
(define (str x)
  (cons 3 x))
(define (sym x)
  (cons 4 x))
(define (key x)
  (cons 5 x))
(define (ver x)
  (cons 6 x))
(define (mod x)
  (cons 7 x))

(define (d x)
  (cons 1 x))
(define (s1 x)
  (cons 2 x))
(define (s2 x)
  (cons 3 x))
(define (s3 x)
  (cons 4 x))
(define (m x)
  (cons 5 x))
(define (e x)
  (cons 6 x))

(define (add-data db x)
  (let ((datas  (slot-ref db 'datas))
	(rdatas (slot-ref db 'rdatas)))
    
    (define variables (make-hash-table))
    (define (reg d x v)
      (let ((key (slot-ref db 'data-key)))
	(hashq-set! datas   x (cons key (d v)))
	(hash-set! rdatas key x)
	(slot-set! db 'data-key (+ key 1))
	key))
    
    (define (make-data x)
      (cond
       ((pair? x)
	(d
	 (aif it (hashq-ref datas x)
	      it
	      (let ((v (list (add-data db (car x))
			     (add-data db (cdr x)))))
		(reg con x v)))))
       
       ((vector? x)
	(d
	 (aif it (hashq-ref datas x)
	      it
	      (let ((v (map (lambda (x) (add-data db x))
			    (vector->list x))))
		(reg vec x v)))))
       
       ((string? x)
	(s1
	 (add-string db x)))
       
       ((symbol? x)
	(s2
	 (add-string db (symbol->string x))))
     
       ((keyword? x)
	(s3
	 (add-string db (symbol->string (keyword->symbol x)))))
     
       ((procedure? x)
	(d
	 (aif it (hashq-ref datas x)
	      it
	      (let ((v
		     (cons
		      (add-string
		       db (symbol->string (procedure-name x)))
		      (add-module
		       db (procedure-property x 'module)))))
		(reg pro x v)))))

       ((module? x)
	(m (add-module db x)))

       ((try-custom x) =>
	(lambda (x)
	  (cons (car x) ((cadr x) db x))))
    
       (else 
	(e x))))

    (make-data x)))

(define (lookup-rdata db x)
  (let ((datas   (slot-ref db 'datas))
	(rdatas  (slot-ref db 'rdatas)))
    (match x
      ((1 . k)
       (aif it (hash-ref rdatas k #f)
	    it
	    #f))
      ((2 . k)
       (lookup-rstring db k))
      ((3 . k)
       (aif it (lookup-rstring db k)
	    (string->symbol it)
	    #f))
      ((4 . k)
       (aif it (lookup-rstring db k)
	    (symbol->keyword (string->symbol it))
	    #f))
      ((5 . k)
       (lookup-rmodule db k))
      ((6 . k) k)
      ((n . k)
       ((custom-rref n) db k)))))

(define (lookup-data db x)
  (let ((datas  (slot-ref db 'datas)))
    (define variables (make-hash-table))
    (define (make-data x)
      (cond
       ((or (pair? x) (vector? x) (procedure? x))
	(aif it (hashq-ref datas x)
	     (d (car it))
	     #f))
	 
       ((string? x)
	(aif it (lookup-string db x)
	     (s1 it)
	     #f))

       ((symbol? x)
	(aif it (lookup-string db x)
	     (s2 it)
	     #f))

       ((keyword? x)
	(aif it (lookup-string db x)
	     (s2 it)
	     #f))
       
       ((module? x)
	(aif it (lookup-module db x)
	     (m it)
	     #f))

       ((try-custom x) =>
	(lambda (x)
	  (match x
	    ((*i* m rref ref)
	     (aif it (ref db x)
		  (cons *i* it)
		  #f)))))
       
       (else 
	(e x))))

    (make-data x)))

(define (pull-datas db trstr trmod)
  (let* ((datas   (slot-ref db 'datas))
	 (rdatas  (slot-ref db 'rdatas))
	 (nlast   (slot-ref db 'data-last-key))
	 (ncur    (slot-ref db 'data-key))
	 (rows    (db-pull-datas db nlast))
	 (rowsp   (pair? rows))	 
	 (newlast (if rowsp
		      (let lp ((rows rows) (n nlast))
			(match rows
			  (((k . _) . rows)
			   (lp rows (max k n)))
			  (()
			   (+ n 1))))
		      nlast))
	 (newme   (let lp ((i nlast) (l '()))
		    (if (< i ncur)
			(aif it (hash-ref rdatas i)
			     (lp (+ i 1)
				 (cons
				  (cons it (hashq-ref datas it))
				  l))
			     (lp (+ i 1) l))
			(reverse l)))))

    ;;Remove the extra from the hashes
    (if rowsp
	(let lp ((newme newme))
	  (match newme
	    (((s . (k . _)) . newme)
	     (hashq-remove! datas  s)
	     (hash-remove!  rdatas k)
	     (lp newme))
	    (() #t))))

    (let lp ((rows rows))
      (match rows
	(((key . v) . rows)
	 (define (mkk v) (lookup-rdata db v))
	 (let ((m (match v
		    ((0 . l)
		     (list->vector (map mkk l)))
		    ((1 . l)
		     (apply cons (map mkk l)))
		    ((2 nm . mod)
		     (let ((nm  (string->symbol
				 (lookup-rstring db nm)))
			   (mod (lookup-rmodule db mod)))
		       (module-ref mod nm))))))
	   (hash-set! datas  m (cons key v))
	   (hash-set! rdatas key m)
	   (lp rows)))
	(()
	 #t)))

    (let lp ((newme newme) (l '()) (w '()) (n newlast))
      (match newme
	(((s . (k . v)) . newme)
	 (if rowsp
	     (aif it (hashq-ref datas s)
		  (lp newme (cons (cons* s k (car it)) l)
		      w                   n)
		  (lp newme (cons (cons* s k n) l)
		      (cons (cons* s n v) w) (+ n 1)))
	     (lp newme l (cons (cons* s k v) w) ncur)))
	(()
	 (let ()
	   (define tr
	     (let ((m (make-hash-table)))
	       (let lp ((l l))
		 (match l
		   (((s k1 . k2) . l)
		    (hash-set! m k1 k2)
		    (lp l))
		   (() #t)))
	       
	       (lambda (k)
		 (match k
		   ((1 . k)
		    (cons 1 (hash-ref m k k)))
		   (((and a (or 2 3 4)) . k)
		    (cons a (trstr k)))
		   ((5 . k)
		    (cons 5 (trmod k)))
		   ((6 . k)
		    (cons 6 k))
		   (k (hash-ref m k k))))))

	   (let lp ((w w) (l '()))
	     (match w
	       (((s n . v) . w)
		(if rowsp
		    (let ((v (match v
			       (((and a (or 0 1)) . l)
				(cons a (map tr l)))
			       ((2 nm . mod)
				(cons* 2 (trstr nm)
				       (trmod mod))))))
		      (hash-set! datas s (cons n v))
		      (hash-set! rdatas n s)))
		(lp w (cons (cons n v) l)))

	       (()
		(if (pair? l)
		    (db-write-datas db l)))))
	 
	   (slot-set! db 'data-key n)
	   (slot-set! db 'data-last-key n)
	 
	   tr))))))

(define (push-datas db)
  (let* ((datas   (slot-ref db 'datas))
	 (rdatas  (slot-ref db 'rdatas))
	 (nlast   (slot-ref db 'data-last-key))
	 (ncur    (slot-ref db 'data-key))
	 (newme   (let lp ((i nlast) (l '()))
		    (if (< i ncur)
			(aif it (hash-ref rdatas i)
			     (lp (+ i 1)
				 (cons
				  (cons it (hashq-ref datas it))
				  l))
			     (lp (+ i 1) l))
			(reverse l)))))

    (let lp ((newme newme) (l '()) (w '()) (n nlast))
      (match newme
	(((s . (k . v)) . newme)
	 (lp newme l (cons (cons* s k v) w) ncur))
	(()
	 (let ()
	   (let lp ((w w) (l '()))
	     (match w
	       (((s n . v) . w)
		(lp w (cons (cons n v) l)))

	       (()
		(if (pair? l)
		    (db-write-datas db l)))))
	 
	   (slot-set! db 'data-key n)
	   (slot-set! db 'data-last-key n)))))))
