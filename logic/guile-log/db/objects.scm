(define-module (logic guile-log db objects)
  #:use-module (ice-9 match)
  #:use-module (oop goops)
  #:use-module (logic guile-log db db)
  #:use-module (logic guile-log db string)
  #:use-module (logic guile-log db module)
  #:use-module (logic guile-log db data)
  #:export
  (new-object-type add-db-type add-object get-object pull-objects))

(define-syntax-rule (aif it p . l) (let ((it p)) (if it . l)))

(define *ntp* 0)
(define (new-object-type)
  (let ((ret *ntp*))
    (set! *ntp* (+ 1 *ntp*))
    ret))

;;(tp? . #(key mk-obj tp? pull))
(define-inlinable (type-is t    o) ((car t) o))
(define-inlinable (type-data    t) (cdr t))

(define-inlinable (type-mk      d) (vector-ref d 0))
(define-inlinable (type-from-tp d) (vector-ref d 1))
(define-inlinable (type-pull    d) (vector-ref d 2))
(define-inlinable (type-add     d) (vector-ref d 3))
(define-inlinable (type-diff    d) (vector-ref d 4))
(define-inlinable (type-push    d) (vector-ref d 5))
(define-inlinable (type-append  d) (vector-ref d 6))
(define-inlinable (type-default d) (vector-ref d 7))

(define types '())

(define* (add-db-type test #:key
		      (mk #f) (from-tp #f) (pull #f) (add #f)
		      (diff #f) (push #f) (append #f) (default #f))
  (let ((l (list mk from-tp pull add diff push append default)))
    (if (and-map (lambda (x) x) l)
	(set! types (cons (cons test
				(list->vector l))
			  types))
	(error "missing function in add-tp-type"))))

(define (get-type o)
  (let lp ((t types))
    (if (pair? t)
        (let ((d (car t)))
          (if (type-is d o)
              (type-data d)
              (lp (cdr t))))
	(error "wrong type"))))

(define (get-type-from-tp o)
  (let lp ((t types))
    (if (pair? t)
        (let ((d (car t)))
          (if ((type-from-tp (type-data d)) o)
	    (type-data d)
	    (lp (cdr t))))
	(error "wrong type"))))

(define (pull-all-types . l)
  (let lp ((t types))
    (if (pair? t)
        (let ((d (car t)))
	  (apply (type-pull (type-data d)) l)
	  (lp (cdr t))))))

(define (push-all-types . l)
  (let lp ((t types))
    (if (pair? t)
        (let ((d (car t)))
	  (apply (type-push (type-data d)) l)
	  (lp (cdr t))))))

(define (pull db)
  (let* ((s-tr (pull-strings db))
	 (m-tr (pull-modules db s-tr))
	 (d-tr (pull-datas db s-tr m-tr)))
    (pull-all-types db s-tr m-tr d-tr)))

(define (push db)
  (push-strings   db)
  (push-modules   db)
  (push-datas     db)
  (push-all-types db)
  (push-objects   db))

(define (mk o)
  (case-lambda
    (() o)
    ((x) (set! o x))))

(define (add-object db str w)
  (let ((o        (mk w))
        (tp       (get-type w))
	(objects  (slot-ref db 'objects))
	(robjects (slot-ref db 'robjects)))
    (aif it (hash-ref objects str)
         (error (format #f "object already exists with name ~a" str))
         (begin
           (let* ((key (slot-ref db 'object-key)))
             (hash-set! objects str (cons* key o ((type-add tp) db (w))))
	     (hash-set! robjects key str)
	     (slot-set! db 'object-key (+ key 1))
	     o)))))

(define (update-object db str o)
  (let* ((w        (o))
	 (tp       (get-type w))
	 (objects  (slot-ref db 'objects))
	 (robjects (slot-ref db 'robjects)))
    (aif it (hash-ref objects str)
         (begin
           (let* ((key (slot-ref db 'object-key)))
             (hash-set! objects str (cons* key o ((type-add tp) db w)))
	     (hash-set! robjects key str)
	     (slot-set! db 'object-key (+ key 1))
	     o))
	 (error "can't updatye an unexisting object"))))

(define (get-object db str)
  (let ((objects (slot-ref db 'objects)))
    (aif it (hash-ref objects str)
	 (cddr it)
	 it)))

(define get-all-objects
  (case-lambda
    ((objects)
     (let ((h (make-hash-table)))
       (hash-for-each
        (lambda (str v)
          (hash-set! h str ((cddr v))))
        objects)
       h))
    
    ((db objects stored newr)
     (let ((l '()))
       (hash-for-each
        (lambda (str v)
          (if (hash-ref newr str)
              (let* ((o (cddr v))
                     (w (o))
                     (t (get-type w))
                     (n (hash-ref stored str (type-default t)))
                     (x ((type-diff t) o n)))
                (if (pair? x)
                    (set! l (cons (cons* str t n x) l))))
              (let* ((o (cddr v))
                     (w (o))
                     (t (get-type w)))
                ((type-add t) db w))))
        objects)))))

(define (add-conflicting-data db new)
  (let lp ((l new))
    (match l
      (((str t n o x) . l)
       ((type-append (type-data t)) db o x)
       (update-object db str o))
       
      (() (values)))))

(define (push-objects db)
  ((slot-ref db 'lock))
  (let* ((objects  (slot-ref db 'objects))
	 (robjects (slot-ref db 'robjects))
	 (nlast    (slot-ref db 'object-last-key))
	 (ncur     (slot-ref db 'object-key))
	 (newme    (let lp ((i nlast) (l '()))
		     (if (< i ncur)
			 (aif it (hash-ref robjects i)
			      (lp (+ i 1) (cons (cons it i) l))
			      (lp (+ i 1) l))
			 l))))

    
    (let lp ((newme newme) (l '()) (w '()) (n nlast))
      (match newme
	(((s . k) . newme)
	 (lp newme (cons (cons* s k n ) l) (cons (cons s n) w) (+ n 1)))
	(()	 
	 (slot-set! db 'object-key n)
	 (slot-set! db 'object-last-key n)
	 (db-write-objects db w))))))

(define (pull-objects db . l)
  ((slot-ref db 'lock))
  (let* ((objects  (slot-ref db 'objects))
	 (robjects (slot-ref db 'robjects))
	 (stored   (slot-ref db 'stored-objects))
	 (nlast    (slot-ref db 'object-last-key))
	 (ncur     (slot-ref db 'object-key))
	 (rows     (db-pull-objects db nlast))
         (newr     (let ((h (make-hash-table)))
                     (let lp ((r rows))
                       (match r
                         (((k s . l) . r)
                          (hash-set! h s s)
                          (lp r))
                         (()
                          h)))))
         (new      (get-all-objects db objects stored newr))
	 (rowsp    (pair? rows))         
	 (newlast  (if rowsp
		       (let lp ((rows rows) (n nlast))
			 (match rows
			   (((k . s) . rows)
			    (lp rows (max k n)))
			   (()
			    (+ n 1))))
		       nlast))         
	 (newme    (let lp ((i nlast) (l '()))
		     (if (< i ncur)
			 (aif it (hash-ref robjects i)
			      (lp (+ i 1) (cons (cons it i) l))
			      (lp (+ i 1) l))
			 l))))

    ;;Don't Remove the extra from the hashes, these should be fresh new ones
    (pull db)
    
    (let lp ((rows rows))
      (match rows
	(((key s tp . tpkey) . rows)
	 (let ((v (aif it (hash-ref objects s)
		       (let ((v (caddr it)))
			 (v ((type-mk (get-type-from-tp tp)) db tpkey))
			 v)
		       (mk ((type-mk (get-type-from-tp tp)) db tpkey)))))
	   (hash-set! objects  s (cons* key tp v))
	   (hash-set! robjects key s)
	   (lp rows)))
	(()
	 #t)))
    
    (let lp ((newme newme) (l '()) (w '()) (n newlast))
      (match newme
	(((s . k) . newme)
	 (aif it (and rowsp (hash-ref objects s))
	      (lp newme (cons (cons* s k it) l) w                   n)
	      (lp newme (cons (cons* s k n ) l) (cons (cons s n) w) (+ n 1))))
	(()
	 (let lp ((w w))
	   (match w
	     (((s . n) . w)
	      (hash-set! objects  s n)
	      (hash-set! robjects n s)
	      (lp w))
	     (() #f)))
	 
	 (slot-set! db 'object-key n)
	 (slot-set! db 'object-last-key n)
	 (db-write-objects db w)
	 
	 (let ((m (make-hash-table)))
	   (let lp ((l l))
	     (match l
	       (((s k1 . k2) . l)
		(hash-set! m k1 k2)
		(lp l))
	       (() #t)))
	   
	   (lambda (k) (hash-ref m k k))))))

    (add-conflicting-data db new)
    (push db)
    (slot-set! db 'stored-objects (get-all-objects objects))
    ((slot-ref db 'unlock) db)))


	      

    


	

	 
	 
