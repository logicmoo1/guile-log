(define-module (logic guile-log db table)
  #:use-module (ice-9 match)
  #:use-module (oop goops)
  #:use-module (logic guile-log vlist)
  #:use-module (logic guile-log db db)
  #:use-module (logic guile-log db data)
  #:export
  (add-table lookup-table lookup-rtable pull-tables push-tables))

(define make-vhash (@@ (logic guile-log code-load) make-vlist))

(define-syntax-rule (aif it p . l) (let ((it p)) (if it . l)))

(define (enlarge! t)
  (let ((v (vector-ref t 0))
	(s (vector-ref t 3)))
    (if (= (* 4 s) (vector-length v))
	(let lp ((i 0) (l '()))
	  (if (< i s)
	      (lp (+ i 1) (cons (cons (vector-ref v i)
				      (vector-ref v (+ i s)))
				l))	      
	      (let lp ((l (reverse! l)) (m (make-vhash
					    (vector (make-vector (* 8 s))
						    (vector-ref v 1)
						    (vector-ref v 2)
						    (* 2 s))
					    -1)))
		(match l
		  (((k . v) . l)
		   (lp l (vhash-cons k v m)))
		  (()
		   (* 2 s))))))
	#f)))
		
(define (fill-table db table dbtable dbtn tn)
  (define diff (@@ (logic guile-log code-load) vblock-diff))
  (let ((tables  (slot-ref db 'tables))
	(rtables (slot-ref db 'rtables)))
    (let lp ((i (slot-ref db 'table-key)) (l (diff table dbtn)))
      (if (pair? l)
	  (let ((kn (add-data db (caar l)))
		(vn (add-data db (cdar l))))
	    (hashq-set! tables table (cons* i kn vn dbtable))
	    (hash-set! rtables i table)
	   (lp (+ i 1) (cdr l)))
	  (begin
	    (slot-set! db 'table-key i))))))

(define (add-table db table)
  (if (= (vector-ref table 3) 0)
      #f
      (let* ((tables  (slot-ref db 'tables))
             (rtables (slot-ref db 'rtables))
             (tn      (vector-ref table 4)))
	(aif it (hash-ref tables table)
	     (match it		 
	       ((key dbtn . l)
		(if (= dbtn tn)
		    key
		    (begin
		      (fill-table db table key dbtn tn)
		      (hash-set! tables table (cons* key tn l))
		      key))))
	     (let* ((key      (slot-ref db 'table-key))
		    (key      (begin
				(slot-set! db 'table-key (+ key 1))
				key))
		    (oldtable (vector-ref table 1))
		    (oldoff   (vector-ref table 2))
		    (newsz    (vector-ref table 3))
		    (old-key  (add-table db oldtable)))
	       (fill-table db table key 0 tn)
	       (if old-key
		   (hashq-set! tables table
                               (cons* key tn newsz old-key oldoff))
                   (hashq-set! tables table (cons* key tn newsz)))
               (hash-set! rtables key table)
	       key)))))


(define (lookup-table db table)
  (let ((tables (slot-ref db 'tables)))
    (car (hashq-ref tables table))))

(define (lookup-rtable db key)
  (let ((tables (slot-ref db 'rtables)))
    (hashq-ref tables key)))

(define (pull-tables db dtr)
  (let* ((tables   (slot-ref db 'tables))
	 (rtables  (slot-ref db 'rtables))
	 (nlast    (slot-ref db 'table-last-key))
	 (ncur     (slot-ref db 'table-key))
	 (rows     (db-pull-tables db nlast))
	 (rowsp    (pair? rows))
	 (menl     (make-hash-table))
	 (newlast  (if rowsp
		       (let lp ((rows rows) (n nlast))
			 (match rows
			   (((k . _) . rows)
			    (lp rows (max k n)))
			   (()
			    (+ n 1))))
		       nlast))
	 (newme    (let lp ((i nlast) (l '()))
		     (if (< i ncur)
			 (aif it (hash-ref rtables i)
			      (lp (+ i 1)
				  (cons
				   (cons it
					 (hashq-ref tables it))
				   l))
			      (lp (+ i 1) l))
			 (reverse l)))))

    (if rowsp
	(let lp ((newme (reverse newme)))
	  (match newme
	    (((s key kn vn . tn) . newme)
	     (hashq-remove! tables  s)
	     (hash-remove! rtables key)
	     (hashq-set! tables s
			 (match (hashq-ref tables s)
			   ((key n . l)
			    (cons* key (- n 1) l))))
	     (vector-set! s 4 (- (vector-ref s 4) 1)))
	    
	    (((s . (k . _)) . newme)
	     (hashq-remove! tables  s)
	     (hash-remove! rtables k)
	     (lp newme))
	    
	    (() #t))))

    (let lp ((rows rows))
      (match rows
	(((key nt . ts) . rows)
	 (let ((t (vector (make-vector (* 4 ts)) vlist-null 0 ts 0)))
	   (hashq-set! tables  t  (cons* key 0 ts))
	   (hash-set! rtables key t)
	   (lp rows)))
	  
	(((key nt ts to . no) . rows)
	 (let* ((told (lookup-rtable db to))
                (t    (vector (make-vector (* 4 ts)) told no ts 0)))
	   (hashq-set! tables  t (cons* key 0 nt to no))
	   (hash-set! rtables key t)
	   (lp rows)))
	   
	(((key kn vn . tn) . rows)
	 (let ((k  (lookup-rdata  db kn))
	       (v  (lookup-rdata  db vn))
	       (t  (lookup-rtable db tn)))
	   (hashq-set! menl t (+ (hashq-ref menl t 0) 1))
	   (hashq-set! tables t
		       (match (hashq-ref tables t)
			 ((key n . l)
			  (cons* key (+ n 1) l))))
	   (vhash-cons k v (make-vhash t (vector-ref t 4)))
	   (hashq-set! tables  key (cons* key kn vn tn))
	   (hash-set! rtables key key)
	   (lp rows)))
	  
	(()
         #f)))


   (let lp ((newme newme) (l '()) (w '()) (n newlast))
      (match newme
	(((s . (k . v)) . newme)
	 (if rowsp
	     (aif it (hashq-ref tables s)
		  (lp newme (cons (cons* s k (car it)) l)
		      w                   n)
		  (lp newme (cons (cons* s k n ) l)
		      (cons (cons* s n v) w) (+ n 1)))
	     (lp newme l
		 (cons (cons* s k v) w) ncur)))
	     
	(()
	 (let ()
	   (define tr
	     (let ((m (make-hash-table)))
	       (let lp ((l l))
		 (match l
		   (((s k1 . k2) . l)
		    (hash-set! m k1 k2)
		    (lp l))
		   (() #t)))
	       (lambda (k) (hash-ref m k k))))
	   
	   (define (trn x)
	     (hash-ref menl x 0))
	     
	   (let lp ((w (reverse w)) (l '()))
	     (match w
	       (((s key . (and v (kn vn . tn))) . w)
		(if rowsp
		    (let* ((tn (tr tn))
                           (kn (dtr kn))
                           (vn (dtr vn))
                           (t (lookup-rtable db tn))
                           (k (lookup-rdata db kn))
                           (v (lookup-rdata db vn))
                           (v (cons* kn vn tn)))
		      (vhash-cons k v (make-vhash t (vector-ref t 4)))
		      (hashq-set! tables  key (cons key v))
		      (hashq-set! tables t
				  (match (hashq-ref tables t)
				    ((key n . l)
				     (cons* key (+ n 1) l))))
		      (hash-set!  rtables key key)
		      (lp w (cons (cons key v) l)))
		    (lp w (cons (cons key v) l))))

	       (((s key . (and v  (m ts to . no))) . w)
		(if rowsp
		    (let* ((to (tr to))
			   (t2 (lookup-rtable db to))
			   (no (+ (trn t2) no))
			   (t  (vector (make-vector (* 4 ts)) t2 no 0))
			   (w  (cons* 0 ts to no))
			   (v  (list ts to no)))
		      (hashq-set! tables  t (cons key w))
		      (hash-set!  rtables key t)
		      (lp w (cons (cons key v) l)))
		    (lp w (cons (list key ts to no) l))))

	       (((s key n . ts) . w)
		(if rowsp
		    (let* ((t  (vector (make-vector (* 4 ts)) vlist-null 0 0))
			   (v  (list ts)))
		      (hashq-set! tables  t (cons* key 0 ts))
		      (hash-set!  rtables key t)
		      (lp w (cons (cons* key ts) l)))
		    (lp w (cons (cons* key ts) l))))
	       
	       (()
		(db-write-tables db l))))

	   (slot-set! db 'table-key      n)
	   (slot-set! db 'table-last-key n)

	   (values tr trn)))))))
	 
(define (push-tables db)
  (let* ((tables   (slot-ref db 'tables))
	 (rtables  (slot-ref db 'rtables))
	 (nlast    (slot-ref db 'table-last-key))
	 (ncur     (slot-ref db 'table-key))
	 (newme    (let lp ((i nlast) (l '()))
		     (if (< i ncur)
			 (aif it (hash-ref rtables i)
			      (lp (+ i 1)
				  (cons
				   (cons it
					 (hashq-ref tables it))
				   l))
			      (lp (+ i 1) l))
			 (reverse l)))))


   (let lp ((newme newme) (l '()) (w '()) (n nlast))
      (match newme
	(((s . (k . v)) . newme)
	 (lp newme l
	     (cons (cons* s k v) w) ncur))
	     
	(()
	 (let ()
	   (let lp ((w (reverse w)) (l '()))
	     (match w
	       (((s key . (and v (kn vn . tn))) . w)
		(lp w (cons (cons key v) l)))

	       (((s key . (and v  (m ts to . no))) . w)
		(lp w (cons (list key ts to no) l)))

	       (((s key n . ts) . w)
		(lp w (cons (cons* key ts) l)))
	       
	       (()
		(db-write-tables db l))))

	   (slot-set! db 'table-key      n)
	   (slot-set! db 'table-last-key n)))))))
	 



