SCM match_taglist = SCM_EOL;
#define GPM_REG(i,nm)						 \
  do{									\
    match_taglist=scm_cons(SCM_PACK ((((scm_t_bits) &&nm)<<2) | 2), match_taglist); \
    if(create_jump_table_p == i + 10) goto nm;				\
  } while(0)

#define B64   (SCM_UNPACK(run[i]) >> 2)
#define B32_1 ((SCM_UNPACK(run[i]) >> 2) & 0xffffffff)
#define B32_2 (((SCM_UNPACK(run[i]) >> 2) & 0xffffffff00000000) >> 32)
#define B16_1 ((SCM_UNPACK(run[i]) >> 2) & 0xffff)
#define B16_2 (((SCM_UNPACK(run[i]) >> 2) & 0xffff0000) >> 16)
#define B16_3 (((SCM_UNPACK(run[i]) >> 2) & 0xffff00000000) >> 32)
#define B16_4 (((SCM_UNPACK(run[i]) >> 2) & 0xffff000000000000) >> 48) 

#define BB64   (SCM_UNPACK(run[i++]) >> 2)
#define BB32_1 ((SCM_UNPACK(run[i++]) >> 2) & 0xffffffff)
#define BB32_2 (((SCM_UNPACK(run[i++]) >> 2) & 0xffffffff00000000) >> 32)
#define BB16_1 ((SCM_UNPACK(run[i++]) >> 2) & 0xffff)
#define BB16_2 (((SCM_UNPACK(run[i++]) >> 2) & 0xffff0000) >> 16)
#define BB16_3 (((SCM_UNPACK(run[i++]) >> 2) & 0xffff00000000) >> 32)
#define BB16_4 (((SCM_UNPACK(run[i++]) >> 2) & 0xffff000000000000) >> 48)

#define NEXT_TAG				\
  do {						\
    scm_t_bits a = BB64;			\
    if(i > N) goto leave;			\
    goto *((void**) (a));			\
  }						\
  while(0)

#define PUSH_E(e) (ee[ne++] = e)
#define POP     (e = ee[--ne])

#define PUSH_VEC(ee,vec,n)			\
do {						\
  vvec[nv] = vec;				\
  vns[nv++]  = n;				\
 } while(0)

#define POP_VEC (nv--)

#define NEXT_TAG_VEC				\
do{						\
  int ii = vns[nv - 1];				\
  e = vvec[nv - 1][ii];				\
  vns[nv - 1] = ii + 1;				\
 } while (0)


#define PUSH_FRAME(i,fr)			\
do{						\
  next_i[nfr]   = i;				\
  next_e[nfr]   = ne;				\
  next_s[nfr]   = s;				\
  next_fr[nfr]  = fr;				\
  next_v[nfr++] = nv;				\
 } while(0)

#define REDO_FRAME(i,fr)			\
  do{						\
    next_i[nfr-1] = (i);			\
    fr = next_fr[nfr-1];			\
  } while(0) 

#define UNWIND					\
do {						\
  if(nfr == 0) return SCM_BOOL_F;		\
  SCM fr;					\
  UNDO_FRAME(i, fr);				\
  gp_unwind(fr);				\
  NEXT_TAG;					\
 } while(0)


#define UNDO_FRAME(g,fr)			\
  do{						\
    nfr--;					\
    g  = next_i[nfr];				\
    ne = next_e[nfr];				\
    fr = next_fr[nfr];				\
    nv = next_v[nfr];				\
    s  = next_s[nfr];				\
    e = ee[ne];					\
  } while(0)

#define POP_FRAME (nfr--)

void fill_in_missing(SCM x)
{
  while(SCM_CONSP(x))
    {
      scm_fluid_set_x(SCM_CAR(x), SCM_BOOL_F);
      x = SCM_CDR(x);
    }
}

SCM gp_vector_bang(SCM e, int n, SCM s)
{
  e = gp_gp_lookup(e,s);
  if(SCM_I_IS_VECTOR(e))
    {
      if(scm_c_vector_length(e) == n)
	return s;
      else
	return SCM_BOOL_F;
    }
  else if(gp_varp(e, s))
    {
      SCM v = scm_c_make_vector(n, SCM_BOOL_F);
      int i;
      for(i=0; i < n; i++)
	scm_c_vector_set_x(v, i, gp_mkvar(s));
      
      s = gp_ref_set(e, v, s);
      
      return s;
    }
  return SCM_BOOL_F;
}

//#define PK(X) printf(X);
#define PK(X)
// A virtual machine for matching
SCM gp_matcher(SCM s, SCM e, SCM run_, int create_jump_table_p, int mode_p)
{
  SCM vars[100],ee[100],next_fr[100],next_s[100];
  SCM *vvec[100];
  int next_i[100],next_e[100],next_v[100],vns[100], nfr = 0, ne = 0, nv=0;
  SCM *run = 0;
  int    N = 0;
  
  if(run_ && scm_vector_p(run_))
    {
      run = SCM_I_VECTOR_WELTS(run_);
      N = scm_c_vector_length(run_);
    }
  
  register int i = 0;  

  if(create_jump_table_p) goto create;

  NEXT_TAG;
    
 match_var:
  PK("match_var\n");
  vars[BB64] = e;
  NEXT_TAG;

 match_evar:
  PK("match_evar\n");
  scm_fluid_set_x(run[i++], e);
  NEXT_TAG;
  
 unify_scm:
  PK("unify_scm\n");
  s = gp_gp_unify(run[i++], e, s);
  if(scm_is_true(s))
    NEXT_TAG;
  else
    UNWIND;
  
 unify_internal:
  PK("unify_internal\n");
  s = gp_gp_unify(vars[BB64], e, s);
  if(scm_is_true(s))
    NEXT_TAG;
  else
    UNWIND;

 unify_external:
  PK("unify_external\n");
  s = gp_gp_unify(scm_fluid_ref(run[i++]), e, s);
  if(scm_is_true(s))
    NEXT_TAG;
  else
    UNWIND;
  
 unify_internal_minus:
  PK("unify_internal\n");
  s = gp_m_unify(vars[BB64], e, s);
  if(scm_is_true(s))
    NEXT_TAG;
  else
    UNWIND;

 unify_external_minus:
  PK("unify_external\n");
  s = gp_m_unify(scm_fluid_ref(run[i++]), e, s);
  if(scm_is_true(s))
    NEXT_TAG;
  else
    UNWIND;

 unify_scm_minus:
  PK("unify_scmn");
  s = gp_m_unify(run[i++], e, s);
  if(scm_is_true(s))
    NEXT_TAG;
  else
    UNWIND;


 cons_plus:  
  PK("cons_plus\n");
  s = gp_pair_bang(e, s);
  if(scm_is_true(s))
    {
      e = gp_gp_lookup(e, s);
      PUSH_E(gp_gp_cdr(e, s));
      e = gp_car(e, s);
      NEXT_TAG;
    }
  else
    UNWIND;

 cons_minus:  
  PK("cons_minus\n");
  s = gp_pair(e, s);
  if(scm_is_true(s))
    {
      PUSH_E(gp_gp_cdr(e, s));
      e = gp_car(e, s);
      NEXT_TAG;
    }
  else
    UNWIND;
  

 vector_plus:  
  {
    PK("vector_plus\n");
    int n = BB64;
    s = gp_vector_bang(e,n,s);
    if(scm_is_true(s))
      {
	if(n > 0)
	  {
	    SCM  vec_   = gp_gp_lookup(e, s);
	    SCM *vec    = SCM_I_VECTOR_WELTS(vec_);
	    if(n > 1)
	      PUSH_VEC(ee,vec,1);
	    e = vec[0];
	  }
	NEXT_TAG;
      }
    else
      UNWIND;
  }

 vector_minus:  
  PK("vector_minus\n");
  int n = BB64;
  e = gp_gp_lookup(e, s);
  if(SCM_I_IS_VECTOR(e) && (scm_c_vector_length(e) == n))
    {
      SCM *vec    = SCM_I_VECTOR_WELTS(e);
      if(n > 1)
	PUSH_VEC(e,vec,1);
      e = vec[0];
    }
  else
    UNWIND;

 next_vec:
  PK("next_vec\n");
  NEXT_TAG_VEC;
  NEXT_TAG;

 pop_vec:
  PK("pop_vec\n");
  POP_VEC;
  goto next_vec;

 pop: 
  PK("pop\n");
  POP;
  NEXT_TAG;
  
 and_tag:
  PK("and_tag\n");
  PUSH_E(e);
  NEXT_TAG;

 or_tag:
  {
    PK("or_tag\n");
    int g = B64;
    SCM fr = gp_newframe(s);
    PUSH_FRAME(g,fr);
    fill_in_missing(run[i++]);
    NEXT_TAG;
  }
    
 next_or:
  {
    PK("next_or\n");
    SCM fr;
    REDO_FRAME(BB64,fr);
    gp_gp_unwind(fr);
    fill_in_missing(run[i++]);
    NEXT_TAG;
  }

 last_or:
  {
    PK("last_or\n");
    SCM fr;
    UNDO_FRAME(i,fr);
    gp_gp_unwind(fr);
    POP_FRAME;
    fill_in_missing(run[i++]);
    NEXT_TAG;
  }

 fail:
  PK("fail\n");
  UNWIND;
   
 pop_frame:
  PK("pop_frame\n");
  POP_FRAME;
  NEXT_TAG;

 create:
  GPM_REG(0,match_var);
  GPM_REG(1,match_evar);
  GPM_REG(2,unify_scm);
  GPM_REG(3,unify_internal);
  GPM_REG(4,unify_external);
  GPM_REG(5,cons_plus);
  GPM_REG(6,cons_minus);
  GPM_REG(7,vector_plus);  
  GPM_REG(8,vector_minus);  
  GPM_REG(9,pop_vec);
  GPM_REG(10,pop); 
  GPM_REG(11,and_tag);
  GPM_REG(12,or_tag);
  GPM_REG(13,next_or);
  GPM_REG(14,last_or);
  GPM_REG(15,fail);
  GPM_REG(16,pop_frame);
  GPM_REG(17,leave);
  GPM_REG(18,unify_scm_minus);
  GPM_REG(19,unify_internal_minus);
  GPM_REG(20,unify_external_minus);

 leave:
  PK("leave\n");
  return s;
}

SCM_DEFINE(gp_get_taglist, "gp-get-taglist", 0, 0, 0, 
           (), "")
#define FUNC_NAME s_scm_get_taglist
{
  return match_taglist;
}
#undef FUNC_NAME

SCM_DEFINE(gp_match, "gp-match", 3, 0, 0, 
           (SCM e, SCM run, SCM s), "")
#define FUNC_NAME s_gp_match
{
  return gp_matcher(s, e, run, 0, 0);
}
#undef FUNC_NAME


 void init_matching()
 {
   gp_matcher((SCM) 0, (SCM) 0, (SCM) 0, 1, 0);
 }
