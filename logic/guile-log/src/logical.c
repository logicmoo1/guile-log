/*
This define the logical variables
*/

#include <stdlib.h>
#include "functional-tree.c"


static inline SCM get_gp_key(SCM *id)
{
  if(GP_STAR(id))
    return SCM_PACK(((SCM_UNPACK(id[0]) >> 4*9) & 0xfffc) | 0x10);
  
  scm_misc_error("get_gp_key","not a gp variables as input",SCM_EOL);
  return 0;
}

/*
SCM make_logical()
{
  SCM ret,*id;
  SCM_NEWSMOB(ret,GP_MK_FRAME_UNBD(gp_type), (void*)0);

  id = GP_GETREF(ret);

  unsigned long k = random() << H_BITS; 

  *(id + 0) = SCM_PACK(GP_MK_FRAME_UNBD(gp_type) | k);
  *(id + 1) = SCM_UNBOUND;
  return ret;
}
*/

SCM make_logical()
{
  SCM ret,*id;
  SCM_NEWSMOB(ret,GP_MK_FRAME_UNBD(gp_type), (void*)0);

  id = GP_GETREF(ret);

  *(id + 0) = SCM_PACK(GP_MK_FRAME_UNBD(gp_type));
  *(id + 1) = SCM_UNBOUND;
  return ret;
}

static inline SCM get_l(SCM l)
{
  return SCM_CAR(l);
}

static inline SCM logical_lookup3_(SCM x, SCM l, SCM rest, int refp,
                                   int *found);
static inline SCM logical_lookup(SCM x, SCM l, SCM rest, int refp, int *found)
{
  /*
  {
    //new tree code here
  retry_tree:
    if(!GP_STAR(x)) return x;
    SCM hash = get_gp_key(GP_GETREF(x));
    SCM y = gp_tree_lookup(s,hash,x);
    if(scm_is_eq(x,y)) return y;
    x = y;
    goto retry_tree;
  }
  */
  gp_debug0("logical cons lookup\n");
  if(!GP(x)) 
    {
      return x; 
    }

  
 retry:
  if(scm_is_eq(l, rest))
    {
      return x;
    }

  if(SCM_CONSP(l))
    {
      SCM car = SCM_CAR(l);
      if(SCM_CONSP(car))
        {
          if(scm_is_eq(SCM_CAR(car),x))
            {
              SCM y = SCM_CDR(car);
              *found = 1;
              if(!GP(y)) 
                {
                  if(refp)
                    return x;
                  else
                    return y;
                }

	      if(!GP_UNBOUND(GP_GETREF(y)))
		return y;
              
              return x;
            }
          else
            {
              l = SCM_CDR(l);
              goto retry;
            }
        }
      else if SCM_NULLP(l)
        {
          return x;
        }
      else if (vlist_p(car))
        {
          SCM y = logical_lookup3_(x, car, rest, refp, found);
          if(!*found)
            {
              l = SCM_CDR(l);
              goto retry;
            }
 
          return y;
        }
      else if (SCM_I_IS_VECTOR(car))
        {
          SCM list_of_engines = SCM_SIMPLE_VECTOR_REF(car,0);
          SCM rest2 = SCM_CDR(l);
        recur:
          if(!SCM_CONSP(list_of_engines))
            {
              l = rest2;
              goto retry;
            }

          SCM engine = SCM_CAR(list_of_engines);

          if(scm_is_eq(engine, rest))
            {
              list_of_engines = SCM_CDR(list_of_engines);
              goto recur;
            }

          SCM y = logical_lookup3_(x, engine, rest, refp, found);
          if(!*found)
            {
              list_of_engines = SCM_CDR(list_of_engines);
              goto recur;
            }
          
          return y;
        }
      else
        scm_misc_error("logical_lookup","malformed assoc",SCM_EOL);
    }
  return x;
}


static inline SCM logical_lookup3_(SCM x, SCM l, SCM rest, int refp, int *found)
{
  gp_debug1("logical lookup %d\n", refp);
  if(!GP(x)) 
    {
      return x; 
    }

  if(scm_is_eq(l,rest))
    {
      return x;
    }

  if(SCM_NULLP(l))
    {
      return x;
    }

  if(SCM_CONSP(l))
    {
      return logical_lookup(x,l,rest,refp, found);
    }

  {
    SCM v;
    gp_debug0("logical3\n");
    if(!vlist_p(l))
      scm_misc_error("lookup",
                     "nota vlist: ~a",
                     scm_list_1(l));

    v = vhash_assq_unify(x,l);
  
    if(!scm_is_eq(v, SCM_UNSPECIFIED))
      {
        *found = 1;
        if(!GP(v)) 
          {
            if(refp)
              {
                return x;
              }
          }
      
        return v;                      
      }
  
    return x;
  }
}

static inline SCM logical_lookup_all(SCM x, SCM l, int refp)
{
  int found = 0;
 recur:
  {
    SCM y = logical_lookup3_(x,l, SCM_EOL, refp, &found);
    if(found)
      {
        if(x==y)
          return x;
        
        x = y;
        found = 0;
        goto recur;
      }
    else
      return y;
  }

  return x;
}

static inline SCM logical_lookup_l(SCM x, SCM *l)
{
  SCM ll = l[0];
  return logical_lookup_all(x,ll,0);
}

static inline SCM logical_lookup3(SCM x, SCM l)
{
  return logical_lookup_all(x,l,0);
}

static inline SCM logical_lookup4(SCM x, SCM l)
{
  return logical_lookup_all(x,l,1);
}


static inline SCM wrap(SCM l, SCM x)
{
  return x;
}

SCM logical_add2__(SCM x, SCM v, SCM l)
{
  //format3("add ~a . ~a in ~a~%",x,v,l);
  gp_debug0("logical add__\n");
  int n;SCM pt;
  for(n = 0,pt = l;SCM_CONSP(pt);pt = SCM_CDR(pt),n++);
    
  if(n >= 10)
    {
      SCM val = scm_fluid_ref(init_block_size);
      scm_fluid_set_x(init_block_size, scm_from_int(128));
      for(pt = scm_reverse(l),l = vlist_null;SCM_CONSP(pt);pt = SCM_CDR(pt))
        {
          SCM ptx = SCM_CAR(pt);
          l = vhash_consq_unify(SCM_CAR(ptx), SCM_CDR(ptx), l);
        }
      l = vhash_consq_unify(x, v, l);
	  
      scm_fluid_set_x(init_block_size, val);
      
      return l;
    }
  else
    return scm_cons(scm_cons(x,v),l);
}

SCM logical_add2_(SCM x, SCM v, SCM l)
{
  return vhash_consq_unify(x, v, l);
}

SCM logical_add2(SCM x, SCM v, SCM ll)
{  
  gp_debug0("logical add\n");

  if(scm_is_eq(x,v))
    return ll;

  if(SCM_CONSP(ll))
    {
      SCM e = SCM_CAR(ll);
      if(vlist_p(e))
        {
          SCM new_vlist = logical_add2_(x, v, e);
          return wrap(ll, scm_cons(new_vlist, SCM_CDR(ll)));
        }
      else if (SCM_I_IS_VECTOR(e))
        {
          SCM new_vlist = logical_add2_(x, v, vlist_null);
          return wrap(ll, scm_cons(new_vlist, ll));
        }
      else
        return wrap(ll, logical_add2__(x, v, ll));
    }
  
  if(SCM_NULLP(ll))
    return wrap(ll, logical_add2__(x, v, ll));
  else 
    return wrap(ll, logical_add2_(x, v, ll));
}



static SCM inline wrap_l(SCM *l, SCM val)
{
  l[0] = val;
  return SCM_BOOL_T;
}

static SCM inline logical_add2_l__(SCM x, SCM v, SCM *l)
{
  //gp_debug0("logical add_l__\n");
  if(SCM_CONSP(*l) || SCM_NULLP(*l))
    {
      int n;SCM pt;
      SCM ll = l[0];
      for(n = 0,pt = ll;SCM_CONSP(pt);pt = SCM_CDR(pt),n++)
	;
    
      if(n >= 10)
	{
	  SCM val = scm_fluid_ref(init_block_size);
	  scm_fluid_set_x(init_block_size, scm_from_int(128));
	  for(pt = scm_reverse(ll), ll = vlist_null
		;SCM_CONSP(pt)
		;pt = SCM_CDR(pt))
	    {
	      SCM ptx = SCM_CAR(pt);
	      ll = vhash_consq_unify(SCM_CAR(ptx), SCM_CDR(ptx), ll);
	    }
	  ll = vhash_consq_unify(x, v, ll);
	  
	  scm_fluid_set_x(init_block_size, val);
	  *l = ll;
	  l[1] = GP_UNREF(SCM_I_VECTOR_WELTS(SCM_STRUCT_SLOT_REF(ll,0)));
	  l[2] = SCM_PACK(my_scm_to_int(SCM_STRUCT_SLOT_REF(ll,1)));
	  return SCM_BOOL_T;
	}
      else
	{
	  SCM ret = scm_cons(scm_cons(x,v),*l);
	  *l = ret;
	}
      return SCM_BOOL_T;
    }
  return SCM_BOOL_T;
}

static SCM inline logical_add2_l_(SCM x, SCM v, SCM *l)
{
  vhash_consq_l(x, v, l);
  return SCM_BOOL_T;
}


static SCM inline logical_add2_l(SCM x, SCM v, SCM *l)
{  
  gp_debug0("logical add_l\n");
  //format3("add_l ~a . ~a in ~a~%",x,v,*l);
  if(scm_is_eq(x,v))
    return SCM_BOOL_T;

  SCM ll = l[0];

  if(SCM_CONSP(ll))
    {
      SCM e = SCM_CAR(ll);
      if(vlist_p(e))
        {
          SCM new_vlist = logical_add2_(x, v, e);
          return wrap_l(l, scm_cons(new_vlist, SCM_CDR(ll)));
        }
      else if (SCM_I_IS_VECTOR(e))
        {
          SCM new_vlist = logical_add2_(x, v, vlist_null);
          return wrap_l(l, scm_cons(new_vlist, ll));
        }
      else
        return logical_add2_l__(x, v, l);
    }
  
  if(SCM_NULLP(ll))
    return logical_add2_l__(x, v, l);
  else 
    return logical_add2_l_(x, v, l);
}

SCM_DEFINE(gp_guard_vars, "gp-guard-vars",1,0,0, (SCM s), "")
#define FUNC_NAME s_gp_guards_vars
{
  if(SCM_CONSP(s))
    if(vlist_p(s))
      vlist_incref(s);
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

SCM_DEFINE(gp_heap_var, "gp-heap-var!", 0, 0, 0, (), 
	   "make heap var!")
#define FUNC_NAME s_gp_heap_var
{
  return make_logical();
}
#undef FUNC_NAME

SCM_DEFINE(gp_logical_incr, "gp-logical++", 0, 0, 0, (), 
	   "increase logic indicator")
#define FUNC_NAME s_gp_logical_incr
{
  struct gp_stack *gp = get_gp();
  gp->_logical_ ++;
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

SCM_DEFINE(gp_logical_decr, "gp-logical--", 0, 0, 0, (), 
	   "increase logic indicator")
#define FUNC_NAME s_gp_logical_incr
{
  struct gp_stack *gp = get_gp();
  gp->_logical_ --;
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

SCM_DEFINE(gp_set_thread_safe, "gp-thread-safe-set!", 2, 0, 0, 
           (SCM s, SCM bool), 
	   "increase logic indicator")
#define FUNC_NAME s_gp_logical_incr
{
  struct gp_stack *gp = get_gp();
  gp->_thread_safe_ = (scm_is_false(bool) ? 0 : 1);
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME
