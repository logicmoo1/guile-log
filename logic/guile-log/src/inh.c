#define MINUS(x,k) SCM_PACK((scm_t_bits) (((long) SCM_UNPACK(x)) - (k << 2)))
inline SCM scm_high_bit(SCM x)
{
  return scm_ash(scm_from_int(1), MINUS(scm_integer_length(x), 1));
}

SCM get_bits_from_set(SCM set, SCM v)
{
  SCM i, bits, outbits, hstar, h, hbits;
  SCM mask, set_to_i, i_to_j, j_to_inh, i_to_inh;

  if(scm_is_false(v)) return scm_from_int(0);

  mask     = scm_c_vector_ref(v, 0);
  set_to_i = scm_c_vector_ref(v, 1);
  i_to_j   = scm_c_vector_ref(v, 2);
  j_to_inh = scm_c_vector_ref(v, 3);
  i_to_inh = scm_c_vector_ref(v, 4);

  if(scm_is_false(set_to_i)) return scm_from_int(0);

  i = scm_hash_ref(set_to_i, set, SCM_BOOL_F);

  if(scm_is_false(i)) return scm_from_int(0);

  if(scm_is_false(scm_eqv_p(scm_logand(i, mask), scm_from_int(0))))
    { //We have a direct mask
      SCM j = scm_hash_ref(i_to_j, i, SCM_BOOL_F);
      if(scm_is_false(j)) return scm_from_int(0);
      return scm_hash_ref(j_to_inh, j, scm_from_int(0));
    }
  
  bits = scm_hash_ref(i_to_inh, i, SCM_BOOL_F);
  if(scm_is_false(bits)) return scm_from_int(0);

  outbits = scm_from_int(0);
  bits = scm_logand(mask, bits);

 retry:
  if(scm_is_true(scm_equal_p(scm_from_int(0), bits)))
    return outbits;

  h     = scm_high_bit(bits);
  hbits = scm_hash_ref(i_to_inh, h, SCM_BOOL_F);
  bits  = scm_logand(bits, scm_lognot(hbits));
  hstar = scm_hash_ref(i_to_j, h, SCM_BOOL_F);
  outbits = scm_logior(outbits, scm_hash_ref(j_to_inh, hstar, SCM_BOOL_F));
  goto retry;
}
