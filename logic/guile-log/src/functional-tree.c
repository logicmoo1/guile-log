
//#(k v l r)

inline SCM make_new_gp_tree_node(SCM hash,SCM key,SCM val)
{
  SCM ret = scm_c_make_vector(4,SCM_BOOL_F);
  scm_c_vector_set_x(ret,0,hash);
  scm_c_vector_set_x(ret,1,scm_cons(scm_cons(key,val),SCM_EOL));
  return ret;
}

inline SCM make_gp_tree_node(SCM k,SCM v,SCM l,SCM r)
{
  SCM ret = scm_c_make_vector(4,SCM_BOOL_F);
  scm_c_vector_set_x(ret,0,k);
  scm_c_vector_set_x(ret,1,v);
  scm_c_vector_set_x(ret,2,l);
  scm_c_vector_set_x(ret,3,r);
  return ret;
}

SCM gp_tree_lookup(SCM tree, SCM hash, SCM key)
{
 retry:

  if(scm_is_false (scm_vector_p (tree))) return key;

  SCM k = scm_c_vector_ref (tree,0);
  if(scm_is_eq (hash,k))
    {
      SCM v = scm_c_vector_ref(tree,1);
    retry_assq:
      if(!SCM_CONSP (v)) return key;
      
      SCM u = SCM_CAR (v);      
      if(!SCM_CONSP (u)) scm_misc_error("gp_tree_lookup","assumes a cons assq"
                                        ,SCM_EOL);
      
      if(scm_is_eq (key, SCM_CAR (u))) return SCM_CDR (u);
      v = SCM_CDR(v);
      goto retry_assq;
    }

  if(SCM_UNPACK (hash) < SCM_UNPACK (k))
    tree = scm_c_vector_ref (tree,2);
  else
    tree = scm_c_vector_ref (tree,3);

  goto retry;
}

SCM gp_tree_add(SCM tree, SCM hash, SCM key, SCM val)
{
  if(scm_is_false (scm_vector_p (tree))) 
    return make_new_gp_tree_node(hash,key,val);
  
  SCM k = scm_c_vector_ref (tree,0);
  if(scm_is_eq (hash,k))
    {
      SCM v = scm_cons (scm_cons (key,val), scm_c_vector_ref(tree,1));
      make_gp_tree_node(k,v,scm_c_vector_ref(tree,2),scm_c_vector_ref(tree,3));
    }

  if(SCM_UNPACK (hash) < SCM_UNPACK (k))    
    return make_gp_tree_node(k,scm_c_vector_ref(tree,1),
                             gp_tree_add(scm_c_vector_ref(tree,2),
                                         hash,key,val),
                             scm_c_vector_ref(tree,3));
  else
    return make_gp_tree_node(k,scm_c_vector_ref(tree,1),
                             scm_c_vector_ref(tree,2),
                             gp_tree_add(scm_c_vector_ref(tree,3),
                                         hash,key,val));                        
}

