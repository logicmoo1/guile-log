#include <pthread.h>
int gp_gc_p = 0;

// guile 2.2 internals normal API does not work in gc hooks

#define SCM_WEAK_TABLE(x) ((scm_t_weak_table *) SCM_CELL_WORD_1 (x))

#if defined(V20)
typedef struct {
  unsigned long hash;
  scm_t_bits key;
  scm_t_bits value;
} scm_t_weak_entry;

typedef struct {
  scm_t_weak_entry *entries;    /* the data */
  scm_i_pthread_mutex_t lock;   /* the lock */
  scm_t_weak_table_kind kind;   /* what kind of table it is */
  unsigned long size;    	/* total number of slots. */
  unsigned long n_items;	/* number of items in table */
  unsigned long lower;		/* when to shrink */
  unsigned long upper;		/* when to grow */
  int size_index;		/* index into hashtable_size */
  int min_size_index;		/* minimum size_index */
} scm_t_weak_table;


static void
copy_weak_entry_gc (scm_t_weak_entry *src, scm_t_weak_entry *dst)
{
  dst->key   = src->key;
  dst->value = src->value;
}


SCM
scm_c_weak_table_fold_in_gc (scm_t_table_fold_fn proc, void *closure,
                       SCM init, SCM table)
{
  scm_t_weak_table *t;
  scm_t_weak_entry *entries;
  unsigned long k, size;

  t = SCM_WEAK_TABLE (table);

  size = t->size;
  entries = t->entries;

  for (k = 0; k < size; k++)
    {
      if (entries[k].hash)
        {
          scm_t_weak_entry copy;
          
          copy_weak_entry_gc (&entries[k], &copy);
      
          if (copy.key && copy.value)
            {
              /* Release table lock while we call the function.  */
              init = proc (closure,
                           SCM_PACK (copy.key), SCM_PACK (copy.value),
                           init);
            }
        }
    }

  return init;
}
#else
typedef struct scm_weak_entry scm_t_weak_entry;

struct scm_weak_entry {
  unsigned long hash;
  scm_t_weak_entry *next;
  scm_t_bits key;
  scm_t_bits value;
};


struct weak_entry_data {
  scm_t_weak_entry *entry;
  scm_t_bits key;
  scm_t_bits value;
};

typedef struct {
  scm_t_weak_entry **buckets;   /* the data */
  scm_i_pthread_mutex_t lock;   /* the lock */
  scm_t_weak_table_kind kind;   /* what kind of table it is */
  unsigned long n_buckets;    	/* total number of buckets. */
  unsigned long n_items;	/* number of items in table */
  unsigned long lower;		/* when to shrink */
  unsigned long upper;		/* when to grow */
  int size_index;		/* index into hashtable_size */
  int min_size_index;		/* minimum size_index */
  GC_word last_gc_no;
} scm_t_weak_table;


static void*
do_read_weak_entry (void *data)
{
  struct weak_entry_data *e = data;

  e->key = e->entry->key;
  e->value = e->entry->value;

  return NULL;
}

static void
read_weak_entry (scm_t_weak_entry *entry, scm_t_bits *key, scm_t_bits *value)
{
  struct weak_entry_data data;

  data.entry = entry;
  do_read_weak_entry(&data);

  *key = data.key;
  *value = data.value;
}

SCM
scm_c_weak_table_fold_in_gc (scm_t_table_fold_fn proc, void *closure,
                       SCM init, SCM table)
{
  scm_t_weak_table *t;
  unsigned long k;

  t = SCM_WEAK_TABLE (table);

  for (k = 0; k < t->n_buckets; k++)
    {
      scm_t_weak_entry *entry;
      for (entry = t->buckets[k]; entry; entry = entry->next)
        {
          scm_t_bits key, value;
          read_weak_entry (entry, &key, &value);
      
          if (key && value)
            init = proc (closure, SCM_PACK(key), SCM_PACK(value), init);
        }
    }
      
  return init;
}
#endif

static inline void enlarge_stack(struct gp_stack *gp, int N, int NN)         
{
  SCM * old = gp->gp_stack; 
  gp->gp_stack = 
    (SCM *) scm_gc_malloc_pointerless(sizeof(SCM) * NN,"gp->gp_stack");
  
  SCM *pt, *pt2;
  for(pt = old, pt2 = gp->gp_stack; pt < gp->gp_si; pt++, pt2++)
    {
      *pt2 = *pt;
    }
  gp->gp_nns = gp->gp_stack + NN - 2; 
  gp->gp_si  = pt2;

  for(; pt2 < gp->gp_nns; pt2++)
    {
      *pt2 = SCM_BOOL_F;
    }



}

static inline void enlarge_frstack(struct gp_stack *gp, int N, int NN)
{
  SCM * old = gp->gp_frstack; 
  gp->gp_frstack = 
    (SCM *) scm_gc_malloc_pointerless(sizeof(SCM) * NN,"gp->gp_stack");
  
  SCM *pt, *pt2;
  for(pt = old, pt2 = gp->gp_frstack; pt < gp->gp_fr; pt++, pt2++)
    {
      *pt2 = *pt;
    }
  gp->gp_nnfr = gp->gp_frstack + NN - 2; 

  gp->gp_fr  = pt2;
}

static inline void enlarge_cstack(struct gp_stack *gp, int N, int NN)
{
  SCM * old = gp->gp_cstack; 
  gp->gp_cstack = 
    (SCM *) scm_gc_malloc_pointerless(sizeof(SCM) * NN,"gp->gp_cstack");
  
  SCM *pt, *pt2;
  for(pt = old, pt2 = gp->gp_cstack; pt < gp->gp_ci; pt++, pt2++)
    {
      *pt2 = *pt;
    }
  gp->gp_nnc = gp->gp_cstack + NN - 2; 
  
  gp->gp_ci  = pt2;
}


static inline void enlarge_csstack(struct gp_stack *gp, int N, int NN)         
{
  SCM * old = gp->gp_cons_stack; 
  gp->gp_cons_stack = 
    (SCM *) scm_gc_malloc_pointerless(sizeof(SCM) * NN,"gp->gp_stack");
  
  SCM *pt, *pt2;
  for(pt = old, pt2 = gp->gp_cons_stack; pt < gp->gp_cs; pt++, pt2++)
    {
      *pt2 = *pt;
    }

  gp->gp_nncs = gp->gp_cons_stack + NN - 2; 
  gp->gp_cs  = pt2;

  for(; pt2 < gp->gp_nncs; pt2++)
    {
      *pt2 = SCM_BOOL_F;
    }
}

static int isBefore = 1;

#ifdef HAS_GP_GC
int gp_gc_counter = 0;
static inline void gp_gc_inc(struct gp_stack *gp)
{      
    long Ns = gp->gp_nns  - gp->gp_stack;
    long Nc = gp->gp_nncs - gp->gp_cons_stack;
    long Nf = gp->gp_nnfr - gp->gp_frstack;
    long N  = gp->gp_nnc  - gp->gp_cstack;

    long ns = gp->gp_nns  - gp->gp_si;
    long nc = gp->gp_nncs - gp->gp_cs;
    long nf = gp->gp_nnfr - gp->gp_fr;
    long n  = gp->gp_nnc  - gp->gp_ci;


    if(ns < 5)
      {
        enlarge_stack(gp,Ns,2*Ns);
        ns = gp->gp_nns  - gp->gp_si;
      }
    if(nc < 5)
      {
        enlarge_csstack(gp,Nc,2*Nc);
        nc = gp->gp_nncs - gp->gp_cs;
      }
    if(nf < 10)
      {
        enlarge_frstack(gp,Nf,2*Nf);
        nf = gp->gp_nnfr - gp->gp_fr;
      }
    if(n  < 5)
      {
        enlarge_cstack(gp,N,2*N);
        n  = gp->gp_nnc  - gp->gp_ci;
      }

    n = (nf > n) ? ((nc > n) ? (n > ns ? ns : n) 
		   : (nc > ns ? ns : nc))
               :  ((nc > nf) ? (nf > ns ? ns : nf)
		   : (nc > ns ? ns : nc));

    N = (Nf > N) ? ((Nc > Nf) ? (Ns > Nc ? Ns : N) :
                    (Ns > Nf ? Ns : Nf)) :
      ((Nc > N) ? (Ns > Nc ? Ns : Nc) : (Ns > N ? Ns : N));

  if(N < 20000) return;

  gp_gc_counter++;
  if (n > 1000)
    {
      if(gp_gc_counter >= 10000)
        {
          scm_gc();
	  gp_gc();
          gp_gc_counter = 0;
        }
    }
  else if (n > 100)
    {
      if(gp_gc_counter >= 1000)
        {
          scm_gc();
	  gp_gc();
          gp_gc_counter = 0;
        }
    }
  else
    {
      if(gp_gc_counter >= 100)
        {
          scm_gc();
	  gp_gc();
          gp_gc_counter = 0;
        }
    }
}
#else
static inline void gp_gc_inc(struct gp_stack *gp)
{
}
#endif

pthread_mutex_t gp_gc_lock = PTHREAD_MUTEX_INITIALIZER;
void gp_no_gc()
{
#ifdef HAS_GP_GC
  pthread_mutex_lock(&gp_gc_lock);
  gp_gc_p ++;
  pthread_mutex_unlock(&gp_gc_lock);
#endif
}

void gp_do_gc()
{
#ifdef HAS_GP_GC
  pthread_mutex_lock(&gp_gc_lock);
  gp_gc_p --;
  pthread_mutex_unlock(&gp_gc_lock);
  gp_gc();
#endif
}

int is_gc_locked()
{
  int ret = 0;

  pthread_mutex_lock(&gp_gc_lock);
  if(gp_gc_p)
    ret = 1;
  else
    ret = 0;
  pthread_mutex_unlock(&gp_gc_lock);
  
  return ret;
}

SCM sweep_folder (void* closure, SCM stack, SCM val, SCM seed)
{
  gp_sweep_handle(stack);
  //gp_clear_marks(stack, !isBefore);
  return seed;
}

void *gp_after_mark_hook(void *hook_data, void *fn_data, void *data)
{
#ifdef HAS_GP_GC
  DB(printf("after mark\n");)
  if(scm_is_true(gp_stacks))
    {
      pthread_mutex_lock(&gp_gc_lock);
      if(!gp_gc_p)
        {
          register_weak_keys();
          scm_c_weak_table_fold_in_gc
            (sweep_folder,(void *)0, SCM_BOOL_F, gp_stacks);          
        }
      pthread_mutex_unlock(&gp_gc_lock);
    }
  DB(printf("after mark fini\n");)
#endif
  return (void *)0;
}

SCM before_folder (void* closure, SCM stack, SCM val, SCM seed)
{
  DB(printf("before folder\n");)
  gp_clear_marks(stack, isBefore);
  DB(printf("before fini\n");)
  return seed;
}

void *gp_before_mark_hook(void *hook_data, void *fn_data, void *data)
{
#ifdef HAS_GP_GC
  DB(printf("before mark\n");)
  if(scm_is_true(gp_stacks))
    {
      prepare_weak_keys();
      register_weak_keys();
      scm_c_weak_table_fold_in_gc
        (before_folder,(void *)0, SCM_BOOL_F, gp_stacks);                
    }
    DB(printf("before mark fin\n");)
#endif
  
  return (void *)0;
}

void init_gpgc()
{
#ifdef HAS_GP_GC
  const int appendp = 0;
  void  *data = (void *) 0;
  scm_c_hook_add(&scm_after_gc_c_hook, gp_after_mark_hook, data, appendp);
  scm_c_hook_add(&scm_before_gc_c_hook, gp_before_mark_hook, data, appendp);
#endif
}

