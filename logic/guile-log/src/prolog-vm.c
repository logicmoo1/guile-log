SCM scm_equal_p109263;

SCM scm_leq_p107118;

SCM scm_geq_p104973;

SCM scm_less_p102828;

SCM scm_gr_p100683;

SCM brace68201;

SCM scm_num6942;

SCM scm_string100301;

SCM scm_string73598;

SCM scm_string26080;


void * _soperations_s[1024];

SCM _sdls_s = SCM_BOOL_F;

SCM _sdelayers_s = SCM_BOOL_F;

SCM _sunwind__hooks_s = SCM_BOOL_F;

SCM _sunwind__parameters_s = SCM_BOOL_F;

SCM _strue_s = SCM_BOOL_F;

SCM _sfalse_s = SCM_BOOL_F;

SCM _sgp__not__n_s = SCM_BOOL_F;

SCM _sgp__is__delayed_p_s = SCM_BOOL_F;

SCM pack__start (SCM nvar, SCM nstack, SCM instructions, SCM contants, SCM tvars);

SCM pack__start (SCM nvar, SCM nstack, SCM instructions, SCM contants, SCM tvars) {
     {
        SCM ret151661;
         {
            
             {
                SCM a151662;
                SCM a151663;
                a151662 = scm_cons(scm_num6942, SCM_EOL);
                 {
                    SCM a151664;
                    SCM a151665;
                    a151664 = SCM_EOL;
                     {
                        SCM a151666;
                        SCM a151667;
                        a151666 = SCM_BOOL_F;
                         {
                            SCM vv151659;
                            vv151659 = scm_c_make_vector(5, SCM_BOOL_F);
                             {
                                SCM * vp151660;
                                vp151660 = SCM_I_VECTOR_WELTS(vv151659);
                                 {
                                    
                                    
                                    
                                    AREF(vp151660, 0) = nvar;
                                    
                                    AREF(vp151660, 1) = nstack;
                                    
                                    AREF(vp151660, 2) = contants;
                                    
                                    AREF(vp151660, 3) = instructions;
                                    
                                    AREF(vp151660, 4) = tvars;
                                    
                                    a151667 = vv151659;
                                }
                            }
                        }
                        a151665 = scm_cons(a151666, a151667);
                    }
                    a151663 = scm_cons(a151664, a151665);
                }
                ret151661 = scm_cons(a151662, a151663);
            }
        }
        return ret151661;
    }
}

SCM gp_c_vector_x (SCM x, int n, SCM s);

SCM gp_c_vector_x (SCM x, int n, SCM s) {
     {
        SCM ret151669;
         {
            
             {
                SCM x151668;
                x151668 = gp_gp_lookup(x, s);
                
                
                
                if (SCM_I_IS_VECTOR(x151668)
                      && n == SCM_I_VECTOR_LENGTH(x151668)) {
                    ret151669 = s;
                } else {
                    if (gp_varp(x151668, s)) {
                         {
                            SCM v151670;
                            v151670 = scm_c_make_vector(n, SCM_BOOL_F);
                            
                            
                            
                             {
                                int i151671;
                                i151671 = n;
                                 {
                                    
                                  lp151672:
                                     {
                                        
                                        if (i151671 == 0) {
                                            ret151669
                                              = gp_gp_unify(x151668,
                                                            v151670,
                                                            s);
                                        } else {
                                             {
                                                int ii151673;
                                                ii151673 = i151671 - 1;
                                                
                                                
                                                
                                                scm_c_vector_set_x(v151670,
                                                                   ii151673,
                                                                   gp_mkvar(s));
                                                
                                                 {
                                                    int next151676;
                                                    next151676 = ii151673;
                                                    i151671 = next151676;
                                                    goto lp151672;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        ret151669 = 0;
                    }
                }
            }
        }
        return ret151669;
    }
}

SCM gp_c_vector (SCM x, int n, SCM s);

SCM gp_c_vector (SCM x, int n, SCM s) {
     {
        SCM ret151678;
         {
            
             {
                SCM x151677;
                x151677 = gp_gp_lookup(x, s);
                
                
                
                if (SCM_I_IS_VECTOR(x151677)
                      && n == SCM_I_VECTOR_LENGTH(x151677)) {
                    ret151678 = s;
                } else {
                    ret151678 = 0;
                }
            }
        }
        return ret151678;
    }
}

SCM _smodel__lambda_s = SCM_BOOL_F;

SCM init__vm__data (SCM dls, SCM delayers, SCM unwind__hooks, SCM unwind__parameters, SCM true, SCM false, SCM gp__not__n, SCM gp__is__delayed_p, SCM model__lambda);

SCM init__vm__data (SCM dls, SCM delayers, SCM unwind__hooks, SCM unwind__parameters, SCM true, SCM false, SCM gp__not__n, SCM gp__is__delayed_p, SCM model__lambda) {
     {
        SCM ret151679;
         {
            
            _sdls_s = dls;
            
            _sdelayers_s = delayers;
            
            _sunwind__hooks_s = unwind__hooks;
            
            _sunwind__parameters_s = unwind__parameters;
            
            _strue_s = true;
            
            _sfalse_s = false;
            
            _sgp__not__n_s = gp__not__n;
            
            _sgp__is__delayed_p_s = gp__is__delayed_p;
            
            _smodel__lambda_s = model__lambda;
            
            ret151679 = SCM_BOOL_F;
        }
        return ret151679;
    }
}

SCM * vm__raw (SCM * fp, SCM * sp, SCM * free, scm_t_uint32 * recieve_p, int register_p);

SCM * vm__raw (SCM * fp, SCM * sp, SCM * free, scm_t_uint32 * recieve_p, int register_p) {
     {
        SCM * ret151707;
         {
            
            --sp;
            
            if (register_p) {
                 {
                    
                    AREF(_soperations_s, 0) = &&store__state;
                    
                    AREF(_soperations_s, 4) = &&softie;
                    
                    AREF(_soperations_s, 1) = &&newframe;
                    
                    AREF(_soperations_s, 2) = &&unwind;
                    
                    AREF(_soperations_s, 3) = &&unwind__tail;
                    
                    AREF(_soperations_s, 7) = &&newframe__negation;
                    
                    AREF(_soperations_s, 8) = &&unwind__negation;
                    
                    AREF(_soperations_s, 9) = &&post__negation;
                    
                    AREF(_soperations_s, 24) = &&pre__unify;
                    
                    AREF(_soperations_s, 25) = &&post__unify;
                    
                    AREF(_soperations_s, 26) = &&post__unify__tail;
                    
                    AREF(_soperations_s, 5) = &&post__s;
                    
                    AREF(_soperations_s, 6) = &&post__q;
                    
                    AREF(_soperations_s, 27) = &&clear__sp;
                    
                    AREF(_soperations_s, 28) = &&false;
                    
                    AREF(_soperations_s, 10) = &&set;
                    
                    AREF(_soperations_s, 20) = &&unify;
                    
                    AREF(_soperations_s, 19) = &&unify__2;
                    
                    AREF(_soperations_s, 12) = &&unify__constant;
                    
                    AREF(_soperations_s, 13) = &&unify__instruction;
                    
                    AREF(_soperations_s, 14) = &&unify__constant__2;
                    
                    AREF(_soperations_s, 15) = &&unify__instruction__2;
                    
                    AREF(_soperations_s, 63) = &&equal__constant;
                    
                    AREF(_soperations_s, 64) = &&equal__instruction;
                    
                    AREF(_soperations_s, 16) = &&icurly;
                    
                    AREF(_soperations_s, 17) = &&ifkn;
                    
                    AREF(_soperations_s, 18) = &&icons;
                    
                    AREF(_soperations_s, 21) = &&icurly_e;
                    
                    AREF(_soperations_s, 22) = &&ifkn_e;
                    
                    AREF(_soperations_s, 23) = &&icons_e;
                    
                    AREF(_soperations_s, 29) = &&fail;
                    
                    AREF(_soperations_s, 30) = &&cc;
                    
                    AREF(_soperations_s, 31) = &&tail__cc;
                    
                    AREF(_soperations_s, 32) = &&call;
                    
                    AREF(_soperations_s, 33) = &&call__n;
                    
                    AREF(_soperations_s, 34) = &&tail__call;
                    
                    AREF(_soperations_s, 35) = &&goto__inst;
                    
                    AREF(_soperations_s, 36) = &&cut;
                    
                    AREF(_soperations_s, 37) = &&post__call;
                    
                    AREF(_soperations_s, 38) = &&post__unicall;
                    
                    AREF(_soperations_s, 44) = &&pushv;
                    
                    AREF(_soperations_s, 39) = &&push__constant;
                    
                    AREF(_soperations_s, 40) = &&push__instruction;
                    
                    AREF(_soperations_s, 41) = &&push__variable;
                    
                    AREF(_soperations_s, 42) = &&push__variable__scm;
                    
                    AREF(_soperations_s, 43) = &&pop__variable;
                    
                    AREF(_soperations_s, 50) = &&dup;
                    
                    AREF(_soperations_s, 49) = &&pop;
                    
                    AREF(_soperations_s, 48) = &&seek;
                    
                    AREF(_soperations_s, 45) = &&mk__cons;
                    
                    AREF(_soperations_s, 46) = &&mk__fkn;
                    
                    AREF(_soperations_s, 47) = &&mk__curly;
                    
                    AREF(_soperations_s, 106) = &&lognot;
                    
                    AREF(_soperations_s, 104) = &&uminus;
                    
                    AREF(_soperations_s, 66) = &&plus;
                    
                    AREF(_soperations_s, 68) = &&minus;
                    
                    AREF(_soperations_s, 70) = &&mul;
                    
                    AREF(_soperations_s, 72) = &&divide;
                    
                    AREF(_soperations_s, 80) = &&shift_l;
                    
                    AREF(_soperations_s, 83) = &&shift_r;
                    
                    AREF(_soperations_s, 96) = &&xor;
                    
                    AREF(_soperations_s, 67) = &&plus2_1;
                    
                    AREF(_soperations_s, 69) = &&minus2_1;
                    
                    AREF(_soperations_s, 71) = &&mul2_1;
                    
                    AREF(_soperations_s, 73) = &&div2_1;
                    
                    AREF(_soperations_s, 93) = &&bitand;
                    
                    AREF(_soperations_s, 95) = &&bitor;
                    
                    AREF(_soperations_s, 97) = &&xor1;
                    
                    AREF(_soperations_s, 81) = &&shiftLL;
                    
                    AREF(_soperations_s, 82) = &&shiftLR;
                    
                    AREF(_soperations_s, 84) = &&shiftRL;
                    
                    AREF(_soperations_s, 85) = &&shiftRR;
                    
                    AREF(_soperations_s, 81) = &&shiftLL;
                    
                    AREF(_soperations_s, 87) = &&modL;
                    
                    AREF(_soperations_s, 88) = &&modR;
                    
                    AREF(_soperations_s, 86) = &&modulo;
                    
                    AREF(_soperations_s, 92) = &&band;
                    
                    AREF(_soperations_s, 94) = &&bor;
                    
                    AREF(_soperations_s, 52) = &&gt;
                    
                    AREF(_soperations_s, 53) = &&ls;
                    
                    AREF(_soperations_s, 54) = &&ge;
                    
                    AREF(_soperations_s, 55) = &&le;
                    
                    AREF(_soperations_s, 56) = &&eq;
                    
                    AREF(_soperations_s, 57) = &&neq;
                    
                    ret151707 = sp;
                }
            } else {
                 {
                    SCM pinned_p151680;
                    int call_p151681;
                    int narg151682;
                    int nlocals151683;
                    SCM always151684;
                    SCM middle151685;
                    SCM session151686;
                    SCM cnst151687;
                    SCM variables__scm151688;
                    SCM * variables151689;
                    SCM constants__scm151690;
                    SCM * constants151691;
                    SCM instructions__scm151692;
                    SCM * instructions151693;
                    SCM tvars__scm151694;
                    SCM * tvars151695;
                    int ninst151696;
                    int nstack151697;
                    int nvar151698;
                    SCM ctrl__stack151699;
                    SCM sp__stack151700;
                    SCM * inst__pt151701;
                    int p_p151702;
                    SCM pp151703;
                    SCM s151704;
                    SCM p151705;
                    SCM cut151706;
                    pinned_p151680 = SCM_BOOL_F;
                    call_p151681 = 1;
                    narg151682 = 0;
                    nlocals151683 = 0;
                    always151684 = SCM_BOOL_F;
                    middle151685 = SCM_BOOL_F;
                    session151686 = SCM_BOOL_F;
                    cnst151687 = SCM_BOOL_F;
                    variables__scm151688 = SCM_BOOL_F;
                    variables151689 = 0;
                    constants__scm151690 = SCM_BOOL_F;
                    constants151691 = 0;
                    instructions__scm151692 = SCM_BOOL_F;
                    instructions151693 = 0;
                    tvars__scm151694 = SCM_BOOL_F;
                    tvars151695 = 0;
                    ninst151696 = 0;
                    nstack151697 = 0;
                    nvar151698 = 2;
                    ctrl__stack151699 = SCM_EOL;
                    sp__stack151700 = SCM_EOL;
                    inst__pt151701 = 0;
                    p_p151702 = 0;
                    pp151703 = SCM_BOOL_F;
                    s151704 = SCM_BOOL_F;
                    p151705 = scm_num6942;
                    cut151706 = scm_num6942;
                    
                    
                    
                     {
                        SCM x151708;
                        x151708 = AREF(free, 1);
                        
                        
                        
                        narg151682 = scm_to_int(SCM_CAR(x151708));
                        
                        nlocals151683 = scm_to_int(SCM_CDR(x151708));
                    }
                    
                     {
                        
                        always151684 = AREF(free, 2);
                        
                        middle151685 = SCM_CDR(always151684);
                        
                        session151686 = SCM_CDR(middle151685);
                        
                        cnst151687 = SCM_CDR(session151686);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        int pred151910;
                         {
                            
                            int x151911;
                            
                            int y151912;
                            
                            if (fp > sp) {
                                x151911 = fp - sp;
                            } else {
                                 {
                                    
                                    scm_misc_error("prolog-vm",
                                                   "negative stack pointer",
                                                   SCM_EOL);
                                    
                                    x151911 = 1;
                                }
                            }
                            
                            y151912 = 1;
                            pred151910 = x151911 > y151912;
                        }
                        if (pred151910) {
                             {
                                
                                p_p151702 = 0;
                                
                                s151704 = AREF(fp, -1);
                                
                                p151705 = AREF(fp, -2);
                            }
                        } else {
                            p_p151702 = 1;
                        }
                    }
                    
                    if (p_p151702) {
                        sp = fp;
                    }
                    
                     {
                        SCM * r151709;
                        r151709 = SCM_I_VECTOR_WELTS(cnst151687);
                        
                        
                        
                        nvar151698 = scm_to_int(AREF(r151709, 0));
                        
                        nstack151697 = scm_to_int(AREF(r151709, 1));
                        
                        constants__scm151690 = AREF(r151709, 2);
                        
                        instructions__scm151692 = AREF(r151709, 3);
                        
                        tvars__scm151694 = AREF(r151709, 4);
                    }
                    
                    instructions151693
                      = SCM_I_VECTOR_WELTS(instructions__scm151692);
                    
                    constants151691 = SCM_I_VECTOR_WELTS(constants__scm151690);
                    
                    tvars151695 = SCM_I_VECTOR_WELTS(tvars__scm151694);
                    
                    variables__scm151688 = SCM_CAR(session151686);
                    
                     {
                        
                        printf("get-variables\n");
                        
                        if (scm_is_true(variables__scm151688)) {
                             {
                                SCM * vv151710;
                                vv151710
                                  = SCM_I_VECTOR_WELTS(variables__scm151688);
                                
                                
                                
                                pinned_p151680
                                  = scm_variable_ref(AREF(vv151710, 2));
                                
                                variables151689 = vv151710;
                            }
                        } else {
                             {
                                int n151711;
                                n151711 = nvar151698;
                                 {
                                    SCM v151712;
                                    v151712
                                      = scm_c_make_vector(n151711, SCM_BOOL_F);
                                     {
                                        SCM * vv151713;
                                        vv151713 = SCM_I_VECTOR_WELTS(v151712);
                                         {
                                            
                                            
                                            
                                            session151686
                                              = scm_cons(v151712, cnst151687);
                                            
                                            middle151685
                                              = scm_cons(SCM_EOL,
                                                         session151686);
                                            
                                             {
                                                int i151915;
                                                i151915 = 0;
                                                 {
                                                    
                                                  lp151916:
                                                     {
                                                        
                                                        if (i151915 < n151711) {
                                                             {
                                                                int var_p151917;
                                                                var_p151917
                                                                  = scm_to_int(AREF(tvars151695,
                                                                                    i151915));
                                                                
                                                                
                                                                
                                                                if (var_p151917) {
                                                                    AREF(vv151713,
                                                                         i151915)
                                                                      = gp_mkvar(s151704);
                                                                }
                                                                
                                                                 {
                                                                    int next151918;
                                                                    next151918
                                                                      = i151915
                                                                          + 1;
                                                                    i151915
                                                                      = next151918;
                                                                    goto lp151916;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            
                                            AREF(vv151713, 2)
                                              = gp_get_state_token();
                                            
                                            variables151689 = vv151713;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        SCM x1151714;
                        x1151714 = SCM_CAR(always151684);
                         {
                            SCM x2151715;
                            x2151715 = SCM_CDR(x1151714);
                             {
                                
                                
                                
                                ninst151696 = scm_to_int(SCM_CAR(x1151714));
                                
                                if (p_p151702) {
                                     {
                                        
                                        ctrl__stack151699 = x2151715;
                                        
                                        pp151703 = SCM_BOOL_F;
                                    }
                                } else {
                                    if (ninst151696 == 0) {
                                         {
                                            
                                            ctrl__stack151699 = SCM_EOL;
                                            
                                            pp151703 = SCM_BOOL_F;
                                        }
                                    } else {
                                         {
                                            
                                            ctrl__stack151699
                                              = SCM_CAR(x2151715);
                                            
                                            pp151703 = SCM_CDR(x2151715);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                    if (p_p151702 || ninst151696 == 0) {
                        call_p151681 = 0;
                    }
                    
                    if (SCM_CONSP(pp151703)
                          && scm_is_eq(SCM_CDR(pp151703), p151705)) {
                        p151705 = SCM_CAR(pp151703);
                    }
                    
                    if (ninst151696 == 0) {
                         {
                            
                            AREF(variables151689, 0) = AREF(fp, -3);
                            
                            AREF(variables151689, 1) = AREF(fp, -2);
                        }
                    }
                    
                    sp__stack151700 = SCM_CAR(middle151685);
                    
                    inst__pt151701 = instructions151693 + ninst151696;
                    
                     {
                        void * jmp151716;
                        jmp151716
                          = AREF(_soperations_s, scm_to_int(*(inst__pt151701)));
                        
                        
                        
                        ++inst__pt151701;
                        
                        goto *jmp151716;
                    }
                    
                     {
                        
                        
                      pre__unify:
                        
                        printf("%s : %d\n", "pre-unify", 24);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM n151717;
                        n151717 = *inst__pt151701;
                        
                        
                        
                        inst__pt151701 = inst__pt151701 + 1;
                        
                        if (SCM_CONSP(n151717)) {
                             {
                                int i2151923;
                                i2151923 = scm_to_int(SCM_CAR(n151717));
                                
                                
                                
                                AREF(fp, -(nstack151697 + i2151923))
                                  = scm_fluid_ref(_sdelayers_s);
                            }
                        } else {
                             {
                                int i2151924;
                                i2151924 = scm_to_int(n151717);
                                
                                
                                
                                if (1) {
                                    if (scm_is_true(pinned_p151680)) {
                                         {
                                            
                                            variables__scm151688
                                              = gp_copy_vector(&(variables151689),
                                                               nvar151698);
                                            
                                            session151686
                                              = scm_cons(variables__scm151688,
                                                         cnst151687);
                                            
                                            middle151685
                                              = scm_cons(SCM_EOL,
                                                         session151686);
                                            
                                            pinned_p151680 = SCM_BOOL_F;
                                        }
                                    }
                                }
                                
                                AREF(variables151689, i2151924)
                                  = scm_fluid_ref(_sdelayers_s);
                            }
                        }
                    }
                    
                     {
                        void * jmp151718;
                        jmp151718
                          = AREF(_soperations_s, scm_to_int(*(inst__pt151701)));
                        
                        
                        
                        ++inst__pt151701;
                        
                        goto *jmp151718;
                    }
                    
                     {
                        
                        
                      post__unify:
                        
                        printf("%s : %d\n", "post-unify", 25);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM n151719;
                        n151719 = *inst__pt151701;
                         {
                            int nsloc151720;
                            nsloc151720 = scm_to_int(AREF(inst__pt151701, 1));
                             {
                                SCM old151723;
                                if (SCM_CONSP(n151719)) {
                                     {
                                        int i2151721;
                                        i2151721
                                          = scm_to_int(SCM_CAR(n151719));
                                        
                                        
                                        
                                        old151723
                                          = AREF(fp,
                                                 -(nstack151697 + i2151721));
                                    }
                                } else {
                                     {
                                        int i2151722;
                                        i2151722 = scm_to_int(n151719);
                                        
                                        
                                        
                                        if (0) {
                                            if (scm_is_true(pinned_p151680)) {
                                                 {
                                                    
                                                    variables__scm151688
                                                      = gp_copy_vector(&(variables151689),
                                                                       nvar151698);
                                                    
                                                    session151686
                                                      = scm_cons(variables__scm151688,
                                                                 cnst151687);
                                                    
                                                    middle151685
                                                      = scm_cons(SCM_EOL,
                                                                 session151686);
                                                    
                                                    pinned_p151680
                                                      = SCM_BOOL_F;
                                                }
                                            }
                                        }
                                        
                                        old151723
                                          = AREF(variables151689, i2151722);
                                    }
                                }
                                 {
                                    
                                    
                                    
                                    if (SCM_CONSP(n151719)) {
                                         {
                                            int i2151928;
                                            i2151928
                                              = scm_to_int(SCM_CAR(n151719));
                                            
                                            
                                            
                                            AREF(fp,
                                                 -(nstack151697 + i2151928))
                                              = SCM_BOOL_F;
                                        }
                                    } else {
                                         {
                                            int i2151929;
                                            i2151929 = scm_to_int(n151719);
                                            
                                            
                                            
                                            if (1) {
                                                if (scm_is_true(pinned_p151680)) {
                                                     {
                                                        
                                                        variables__scm151688
                                                          = gp_copy_vector(&(variables151689),
                                                                           nvar151698);
                                                        
                                                        session151686
                                                          = scm_cons(variables__scm151688,
                                                                     cnst151687);
                                                        
                                                        middle151685
                                                          = scm_cons(SCM_EOL,
                                                                     session151686);
                                                        
                                                        pinned_p151680
                                                          = SCM_BOOL_F;
                                                    }
                                                }
                                            }
                                            
                                            AREF(variables151689, i2151929)
                                              = SCM_BOOL_F;
                                        }
                                    }
                                    
                                    inst__pt151701 = inst__pt151701 + 2;
                                    
                                    if (!scm_is_eq(scm_fluid_ref(_sdelayers_s),
                                                   old151723)) {
                                         {
                                            
                                             {
                                                SCM l151930;
                                                l151930 = SCM_EOL;
                                                 {
                                                    
                                                  lp151938:
                                                     {
                                                        
                                                        if (fp == sp) {
                                                            sp__stack151700
                                                              = l151930;
                                                        } else {
                                                             {
                                                                
                                                                sp = sp + 1;
                                                                
                                                                 {
                                                                    SCM r151939;
                                                                    r151939
                                                                      = *sp;
                                                                    
                                                                    
                                                                    
                                                                    *sp
                                                                      = SCM_BOOL_F;
                                                                    
                                                                     {
                                                                        SCM next151942;
                                                                        next151942
                                                                          = scm_cons(r151939,
                                                                                     l151930);
                                                                        l151930
                                                                          = next151942;
                                                                        goto lp151938;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            
                                            if (nsloc151720 > 0) {
                                                 {
                                                    SCM * vp151931;
                                                    vp151931
                                                      = fp
                                                          + -nstack151697
                                                               + nsloc151720;
                                                    
                                                    
                                                    
                                                     {
                                                        int i151943;
                                                        i151943 = nsloc151720;
                                                         {
                                                            
                                                          lp151948:
                                                             {
                                                                
                                                                if (i151943
                                                                      > 0) {
                                                                     {
                                                                        
                                                                        sp__stack151700
                                                                          = scm_cons(AREF(vp151931,
                                                                                          1),
                                                                                     sp__stack151700);
                                                                        
                                                                        --vp151931;
                                                                        
                                                                         {
                                                                            int next151949;
                                                                            next151949
                                                                              = i151943
                                                                                  - 1;
                                                                            i151943
                                                                              = next151949;
                                                                            goto lp151948;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            
                                            AREF(fp, 0) = _sdls_s;
                                            
                                            AREF(fp, -4) = old151723;
                                            
                                            sp = fp + -5;
                                            
                                            goto call;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        void * jmp151724;
                        jmp151724
                          = AREF(_soperations_s, scm_to_int(*(inst__pt151701)));
                        
                        
                        
                        ++inst__pt151701;
                        
                        goto *jmp151724;
                    }
                    
                     {
                        
                        
                      post__unify__tail:
                        
                        printf("%s : %d\n", "post-unify-tail", 26);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM n151725;
                        n151725 = *inst__pt151701;
                         {
                            SCM old151728;
                            if (SCM_CONSP(n151725)) {
                                 {
                                    int i2151726;
                                    i2151726 = scm_to_int(SCM_CAR(n151725));
                                    
                                    
                                    
                                    old151728
                                      = AREF(fp, -(nstack151697 + i2151726));
                                }
                            } else {
                                 {
                                    int i2151727;
                                    i2151727 = scm_to_int(n151725);
                                    
                                    
                                    
                                    if (0) {
                                        if (scm_is_true(pinned_p151680)) {
                                             {
                                                
                                                variables__scm151688
                                                  = gp_copy_vector(&(variables151689),
                                                                   nvar151698);
                                                
                                                session151686
                                                  = scm_cons(variables__scm151688,
                                                             cnst151687);
                                                
                                                middle151685
                                                  = scm_cons(SCM_EOL,
                                                             session151686);
                                                
                                                pinned_p151680 = SCM_BOOL_F;
                                            }
                                        }
                                    }
                                    
                                    old151728
                                      = AREF(variables151689, i2151727);
                                }
                            }
                             {
                                
                                
                                
                                if (SCM_CONSP(n151725)) {
                                     {
                                        int i2151954;
                                        i2151954
                                          = scm_to_int(SCM_CAR(n151725));
                                        
                                        
                                        
                                        AREF(fp, -(nstack151697 + i2151954))
                                          = SCM_BOOL_F;
                                    }
                                } else {
                                     {
                                        int i2151955;
                                        i2151955 = scm_to_int(n151725);
                                        
                                        
                                        
                                        if (1) {
                                            if (scm_is_true(pinned_p151680)) {
                                                 {
                                                    
                                                    variables__scm151688
                                                      = gp_copy_vector(&(variables151689),
                                                                       nvar151698);
                                                    
                                                    session151686
                                                      = scm_cons(variables__scm151688,
                                                                 cnst151687);
                                                    
                                                    middle151685
                                                      = scm_cons(SCM_EOL,
                                                                 session151686);
                                                    
                                                    pinned_p151680
                                                      = SCM_BOOL_F;
                                                }
                                            }
                                        }
                                        
                                        AREF(variables151689, i2151955)
                                          = SCM_BOOL_F;
                                    }
                                }
                                
                                inst__pt151701 = inst__pt151701 + 1;
                                
                                if (!scm_is_eq(scm_fluid_ref(_sdelayers_s),
                                               old151728)) {
                                     {
                                        
                                         {
                                            SCM l151956;
                                            l151956 = SCM_EOL;
                                             {
                                                
                                              lp151963:
                                                 {
                                                    
                                                    if (fp == sp) {
                                                        sp__stack151700
                                                          = l151956;
                                                    } else {
                                                         {
                                                            
                                                            sp = sp + 1;
                                                            
                                                             {
                                                                SCM r151964;
                                                                r151964 = *sp;
                                                                
                                                                
                                                                
                                                                *sp
                                                                  = SCM_BOOL_F;
                                                                
                                                                 {
                                                                    SCM next151967;
                                                                    next151967
                                                                      = scm_cons(r151964,
                                                                                 l151956);
                                                                    l151956
                                                                      = next151967;
                                                                    goto lp151963;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        
                                        AREF(fp, 0) = _sdls_s;
                                        
                                        AREF(fp, -4) = old151728;
                                        
                                        sp = fp + -5;
                                        
                                        goto tail__call;
                                    }
                                } else {
                                    goto cc;
                                }
                            }
                        }
                    }
                    
                     {
                        
                        
                      clear__sp:
                        
                        printf("%s : %d\n", "clear-sp", 27);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM l151729;
                        l151729 = SCM_EOL;
                         {
                            
                          lp151970:
                             {
                                
                                if (fp == sp) {
                                    sp__stack151700 = l151729;
                                } else {
                                     {
                                        
                                        sp = sp + 1;
                                        
                                         {
                                            SCM r151971;
                                            r151971 = *sp;
                                            
                                            
                                            
                                            *sp = SCM_BOOL_F;
                                            
                                             {
                                                SCM next151974;
                                                next151974
                                                  = scm_cons(r151971, l151729);
                                                l151729 = next151974;
                                                goto lp151970;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        
                      lp151975:
                         {
                            
                            if (sp > fp) {
                                 {
                                    
                                    printf("wrong stack state sp - xp = %d, will equalize\n",
                                           (fp - sp));
                                    
                                    sp = fp;
                                }
                            } else {
                                if (sp < fp) {
                                     {
                                        
                                        sp = sp + 1;
                                        
                                        *sp = SCM_BOOL_F;
                                        
                                        goto lp151975;
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        void * jmp151730;
                        jmp151730
                          = AREF(_soperations_s, scm_to_int(*(inst__pt151701)));
                        
                        
                        
                        ++inst__pt151701;
                        
                        goto *jmp151730;
                    }
                    
                     {
                        
                        
                      cc:
                        
                        printf("%s : %d\n", "cc", 30);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM st151732;
                         {
                            SCM l151731;
                            l151731 = SCM_EOL;
                             {
                                
                              lp151978:
                                 {
                                    
                                    if (fp == sp) {
                                        st151732 = l151731;
                                    } else {
                                         {
                                            
                                            sp = sp + 1;
                                            
                                             {
                                                SCM r151979;
                                                r151979 = *sp;
                                                
                                                
                                                
                                                *sp = SCM_BOOL_F;
                                                
                                                 {
                                                    SCM next151982;
                                                    next151982
                                                      = scm_cons(r151979,
                                                                 l151731);
                                                    l151731 = next151982;
                                                    goto lp151978;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                         {
                            SCM p151733;
                            if (SCM_I_INUMP(p151705)) {
                                if (SCM_CONSP(pp151703)
                                      && scm_is_eq(SCM_CAR(pp151703), p151705)) {
                                    p151733 = SCM_CDR(pp151703);
                                } else {
                                    p151733
                                      = gp_custom_fkn(_smodel__lambda_s,
                                                      scm_cons(scm_from_int(1),
                                                               scm_from_int(nlocals151683)),
                                                      scm_cons(scm_cons(p151705,
                                                                        ctrl__stack151699),
                                                               scm_is_eq(sp__stack151700,
                                                                         SCM_EOL) ? middle151685 : scm_cons(sp__stack151700,
                                                                                                            session151686)));
                                }
                            } else {
                                p151733 = p151705;
                            }
                             {
                                SCM cc151734;
                                cc151734 = AREF(variables151689, 0);
                                 {
                                    
                                    
                                    
                                    scm_simple_format(SCM_BOOL_T,
                                                      scm_string26080,
                                                      scm_list_1(st151732));
                                    
                                     {
                                        
                                      lp151983:
                                         {
                                            
                                            if (sp > fp) {
                                                 {
                                                    
                                                    printf("wrong stack state sp - xp = %d, will equalize\n",
                                                           (fp - sp));
                                                    
                                                    sp = fp;
                                                }
                                            } else {
                                                if (sp < fp) {
                                                     {
                                                        
                                                        sp = sp + 1;
                                                        
                                                        *sp = SCM_BOOL_F;
                                                        
                                                        goto lp151983;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    
                                    AREF(fp, 0) = cc151734;
                                    
                                    AREF(fp, -1) = s151704;
                                    
                                    AREF(fp, -2) = p151733;
                                    
                                    sp = fp + -3;
                                    
                                    goto ret;
                                }
                            }
                        }
                    }
                    
                     {
                        
                        
                      call:
                        
                        printf("%s : %d\n", "call", 32);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM p0151735;
                        p0151735 = p151705;
                         {
                            SCM p151736;
                            if (SCM_I_INUMP(p151705)) {
                                if (SCM_CONSP(pp151703)
                                      && scm_is_eq(SCM_CAR(pp151703), p151705)) {
                                    p151736 = SCM_CDR(pp151703);
                                } else {
                                    p151736
                                      = gp_custom_fkn(_smodel__lambda_s,
                                                      scm_cons(scm_from_int(1),
                                                               scm_from_int(nlocals151683)),
                                                      scm_cons(scm_cons(scm_from_int((inst__pt151701
                                                                                        - instructions151693)),
                                                                        ctrl__stack151699),
                                                               scm_is_eq(sp__stack151700,
                                                                         SCM_EOL) ? middle151685 : scm_cons(sp__stack151700,
                                                                                                            session151686)));
                                }
                            } else {
                                p151736 = p151705;
                            }
                             {
                                SCM cc151737;
                                cc151737
                                  = gp_custom_fkn(_smodel__lambda_s,
                                                  scm_cons(scm_from_int(3),
                                                           scm_from_int(nlocals151683)),
                                                  scm_cons(scm_cons(scm_from_int((inst__pt151701
                                                                                    - instructions151693)),
                                                                    scm_cons(ctrl__stack151699,
                                                                             (SCM_CONSP(pp151703)
                                                                                && scm_is_eq(SCM_CAR(pp151703),
                                                                                             p0151735)) ? pp151703 : scm_cons(p0151735,
                                                                                                                              p151736))),
                                                           scm_is_eq(sp__stack151700,
                                                                     SCM_EOL) ? middle151685 : scm_cons(sp__stack151700,
                                                                                                        session151686)));
                                 {
                                    
                                    
                                    
                                    AREF(fp, -1) = s151704;
                                    
                                    AREF(fp, -2) = p151736;
                                    
                                    AREF(fp, -3) = cc151737;
                                    
                                    goto ret;
                                }
                            }
                        }
                    }
                    
                     {
                        
                        
                      call__n:
                        
                        printf("%s : %d\n", "call-n", 33);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM na151738;
                        na151738 = *inst__pt151701;
                         {
                            SCM p0151739;
                            p0151739 = p151705;
                             {
                                SCM p151740;
                                if (SCM_I_INUMP(p151705)) {
                                    if (SCM_CONSP(pp151703)
                                          && scm_is_eq(SCM_CAR(pp151703),
                                                       p151705)) {
                                        p151740 = SCM_CDR(pp151703);
                                    } else {
                                        p151740
                                          = gp_custom_fkn(_smodel__lambda_s,
                                                          scm_cons(scm_from_int(1),
                                                                   scm_from_int(nlocals151683)),
                                                          scm_cons(scm_cons(scm_from_int((inst__pt151701
                                                                                            - instructions151693)),
                                                                            ctrl__stack151699),
                                                                   scm_is_eq(sp__stack151700,
                                                                             SCM_EOL) ? middle151685 : scm_cons(sp__stack151700,
                                                                                                                session151686)));
                                    }
                                } else {
                                    p151740 = p151705;
                                }
                                 {
                                    SCM cc151741;
                                    cc151741
                                      = gp_custom_fkn(_smodel__lambda_s,
                                                      scm_cons(na151738,
                                                               scm_from_int(nlocals151683)),
                                                      scm_cons(scm_cons(scm_from_int((inst__pt151701
                                                                                        + 1
                                                                                        - instructions151693)),
                                                                        scm_cons(ctrl__stack151699,
                                                                                 (SCM_CONSP(pp151703)
                                                                                    && scm_is_eq(SCM_CAR(pp151703),
                                                                                                 p0151739)) ? pp151703 : scm_cons(p0151739,
                                                                                                                                  p151740))),
                                                               scm_is_eq(sp__stack151700,
                                                                         SCM_EOL) ? middle151685 : scm_cons(sp__stack151700,
                                                                                                            session151686)));
                                     {
                                        
                                        
                                        
                                        AREF(fp, -1) = s151704;
                                        
                                        AREF(fp, -2) = p151740;
                                        
                                        AREF(fp, -3) = cc151741;
                                        
                                        goto ret;
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        
                        
                      tail__call:
                        
                        printf("%s : %d\n", "tail-call", 34);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM p151742;
                        SCM cc151743;
                        if (SCM_I_INUMP(p151705)) {
                            if (SCM_CONSP(pp151703)
                                  && scm_is_eq(SCM_CAR(pp151703), p151705)) {
                                p151742 = SCM_CDR(pp151703);
                            } else {
                                p151742
                                  = gp_custom_fkn(_smodel__lambda_s,
                                                  scm_cons(scm_from_int(1),
                                                           scm_from_int(nlocals151683)),
                                                  scm_cons(scm_cons(scm_from_int((inst__pt151701
                                                                                    - instructions151693)),
                                                                    ctrl__stack151699),
                                                           scm_is_eq(sp__stack151700,
                                                                     SCM_EOL) ? middle151685 : scm_cons(sp__stack151700,
                                                                                                        session151686)));
                            }
                        } else {
                            p151742 = p151705;
                        }
                        cc151743 = AREF(variables151689, 0);
                        
                        
                        
                        AREF(fp, -1) = s151704;
                        
                        AREF(fp, -2) = p151742;
                        
                        AREF(fp, -3) = cc151743;
                        
                        goto ret;
                    }
                    
                     {
                        
                        
                      tail__cc:
                        
                        printf("%s : %d\n", "tail-cc", 31);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM p151744;
                        SCM cc151745;
                        if (SCM_I_INUMP(p151705)) {
                            if (SCM_CONSP(pp151703)
                                  && scm_is_eq(SCM_CAR(pp151703), p151705)) {
                                p151744 = SCM_CDR(pp151703);
                            } else {
                                p151744
                                  = gp_custom_fkn(_smodel__lambda_s,
                                                  scm_cons(scm_from_int(1),
                                                           scm_from_int(nlocals151683)),
                                                  scm_cons(scm_cons(scm_from_int((inst__pt151701
                                                                                    - instructions151693)),
                                                                    ctrl__stack151699),
                                                           scm_is_eq(sp__stack151700,
                                                                     SCM_EOL) ? middle151685 : scm_cons(sp__stack151700,
                                                                                                        session151686)));
                            }
                        } else {
                            p151744 = p151705;
                        }
                        cc151745 = AREF(variables151689, 0);
                        
                        
                        
                        AREF(fp, 0) = cc151745;
                        
                        AREF(fp, -1) = s151704;
                        
                        AREF(fp, -2) = p151744;
                        
                        goto ret;
                    }
                    
                     {
                        
                        
                      store__state:
                        
                        printf("%s : %d\n", "store-state", 0);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM np151746;
                        np151746 = AREF(inst__pt151701, 0);
                        
                        
                        
                        inst__pt151701 = inst__pt151701 + 1;
                        
                        ctrl__stack151699
                          = scm_cons(scm_cons(np151746,
                                              scm_cons(SCM_BOOL_F, p151705)),
                                     ctrl__stack151699);
                        
                         {
                            void * jmp151988;
                            jmp151988
                              = AREF(_soperations_s,
                                     scm_to_int(*(inst__pt151701)));
                            
                            
                            
                            ++inst__pt151701;
                            
                            goto *jmp151988;
                        }
                    }
                    
                     {
                        
                        
                      newframe:
                        
                        printf("%s : %d\n", "newframe", 1);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM np151747;
                        int tp151748;
                        SCM ss151750;
                        np151747 = AREF(inst__pt151701, 0);
                        tp151748 = scm_to_int(AREF(inst__pt151701, 1));
                         {
                            
                             {
                                SCM l151749;
                                l151749
                                  = scm_fluid_ref(_sunwind__parameters_s);
                                 {
                                    
                                  lp151992:
                                     {
                                        
                                        if (SCM_CONSP(l151749)) {
                                             {
                                                
                                                 {
                                                    SCM a151996;
                                                    SCM a151997;
                                                    a151996
                                                      = _sunwind__parameters_s;
                                                     {
                                                        SCM x151993;
                                                        x151993
                                                          = SCM_CAR(l151749);
                                                        
                                                        
                                                        
                                                         {
                                                            SCM a152002;
                                                            SCM a152003;
                                                            a152002 = x151993;
                                                             {
                                                                SCM a152006;
                                                                 {
                                                                    SCM l151998;
                                                                    l151998
                                                                      = x151993;
                                                                     {
                                                                        SCM r151999;
                                                                        r151999
                                                                          = SCM_EOL;
                                                                         {
                                                                            
                                                                          lp152007:
                                                                             {
                                                                                
                                                                                if (SCM_CONSP(l151998)) {
                                                                                     {
                                                                                        SCM next152009;
                                                                                        SCM next152010;
                                                                                        next152009
                                                                                          = SCM_CDR(l151998);
                                                                                         {
                                                                                            SCM f152008;
                                                                                            f152008
                                                                                              = SCM_CAR(l151998);
                                                                                            
                                                                                            
                                                                                            
                                                                                            next152010
                                                                                              = scm_cons(scm_cons(f152008,
                                                                                                                  scm_call_0(f152008)),
                                                                                                         r151999);
                                                                                        }
                                                                                        l151998
                                                                                          = next152009;
                                                                                        r151999
                                                                                          = next152010;
                                                                                        goto lp152007;
                                                                                    }
                                                                                } else {
                                                                                    a152006
                                                                                      = r151999;
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                a152003
                                                                  = scm_reverse(a152006);
                                                            }
                                                            a151997
                                                              = scm_cons(a152002,
                                                                         a152003);
                                                        }
                                                    }
                                                    gp_with_fluid(a151996,
                                                                  a151997);
                                                }
                                                
                                                 {
                                                    SCM next152011;
                                                    next152011
                                                      = SCM_CDR(l151749);
                                                    l151749 = next152011;
                                                    goto lp151992;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            
                            ss151750 = gp_gp_newframe(s151704);
                        }
                        
                        
                        
                        inst__pt151701 = inst__pt151701 + 2;
                        
                        s151704 = ss151750;
                        
                        ctrl__stack151699
                          = scm_cons(scm_cons(np151747,
                                              scm_cons(s151704,
                                                       tp151748 ? scm_cons(p151705,
                                                                           scm_fluid_ref(_sdelayers_s)) : p151705)),
                                     ctrl__stack151699);
                        
                        p151705 = np151747;
                        
                         {
                            void * jmp151991;
                            jmp151991
                              = AREF(_soperations_s,
                                     scm_to_int(*(inst__pt151701)));
                            
                            
                            
                            ++inst__pt151701;
                            
                            goto *jmp151991;
                        }
                    }
                    
                     {
                        
                        
                      newframe__negation:
                        
                        printf("%s : %d\n", "newframe-negation", 7);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM np151751;
                        int tp151752;
                        SCM ss151754;
                        np151751 = AREF(inst__pt151701, 0);
                        tp151752 = scm_to_int(AREF(inst__pt151701, 1));
                         {
                            
                             {
                                SCM l151753;
                                l151753
                                  = scm_fluid_ref(_sunwind__parameters_s);
                                 {
                                    
                                  lp152015:
                                     {
                                        
                                        if (SCM_CONSP(l151753)) {
                                             {
                                                
                                                 {
                                                    SCM a152019;
                                                    SCM a152020;
                                                    a152019
                                                      = _sunwind__parameters_s;
                                                     {
                                                        SCM x152016;
                                                        x152016
                                                          = SCM_CAR(l151753);
                                                        
                                                        
                                                        
                                                         {
                                                            SCM a152025;
                                                            SCM a152026;
                                                            a152025 = x152016;
                                                             {
                                                                SCM a152029;
                                                                 {
                                                                    SCM l152021;
                                                                    l152021
                                                                      = x152016;
                                                                     {
                                                                        SCM r152022;
                                                                        r152022
                                                                          = SCM_EOL;
                                                                         {
                                                                            
                                                                          lp152030:
                                                                             {
                                                                                
                                                                                if (SCM_CONSP(l152021)) {
                                                                                     {
                                                                                        SCM next152032;
                                                                                        SCM next152033;
                                                                                        next152032
                                                                                          = SCM_CDR(l152021);
                                                                                         {
                                                                                            SCM f152031;
                                                                                            f152031
                                                                                              = SCM_CAR(l152021);
                                                                                            
                                                                                            
                                                                                            
                                                                                            next152033
                                                                                              = scm_cons(scm_cons(f152031,
                                                                                                                  scm_call_0(f152031)),
                                                                                                         r152022);
                                                                                        }
                                                                                        l152021
                                                                                          = next152032;
                                                                                        r152022
                                                                                          = next152033;
                                                                                        goto lp152030;
                                                                                    }
                                                                                } else {
                                                                                    a152029
                                                                                      = r152022;
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                a152026
                                                                  = scm_reverse(a152029);
                                                            }
                                                            a152020
                                                              = scm_cons(a152025,
                                                                         a152026);
                                                        }
                                                    }
                                                    gp_with_fluid(a152019,
                                                                  a152020);
                                                }
                                                
                                                 {
                                                    SCM next152034;
                                                    next152034
                                                      = SCM_CDR(l151753);
                                                    l151753 = next152034;
                                                    goto lp152015;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            
                            ss151754 = gp_gp_newframe(s151704);
                        }
                        
                        
                        
                        inst__pt151701 = inst__pt151701 + 2;
                        
                        s151704 = ss151754;
                        
                        s151704
                          = gp_set(_sgp__not__n_s,
                                   SCM_I_MAKINUM((1
                                                    + SCM_I_INUM(gp_gp_lookup(_sgp__not__n_s,
                                                                              s151704)))),
                                   s151704);
                        
                        s151704
                          = gp_set(_sgp__is__delayed_p_s, SCM_BOOL_F, s151704);
                        
                        ctrl__stack151699
                          = scm_cons(scm_cons(np151751,
                                              scm_cons(ss151754,
                                                       tp151752 ? scm_cons(p151705,
                                                                           scm_fluid_ref(_sdelayers_s)) : p151705)),
                                     ctrl__stack151699);
                        
                        p151705 = np151751;
                        
                        cut151706 = np151751;
                        
                         {
                            void * jmp152014;
                            jmp152014
                              = AREF(_soperations_s,
                                     scm_to_int(*(inst__pt151701)));
                            
                            
                            
                            ++inst__pt151701;
                            
                            goto *jmp152014;
                        }
                    }
                    
                     {
                        
                        
                      post__negation:
                        
                        printf("%s : %d\n", "post-negation", 9);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM np151755;
                        np151755 = AREF(inst__pt151701, 0);
                        
                        
                        
                        cut151706 = AREF(inst__pt151701, 1);
                        
                        inst__pt151701 = inst__pt151701 + 2;
                        
                         {
                            
                          lp152041:
                             {
                                
                                 {
                                    SCM x152042;
                                    x152042 = SCM_CAR(ctrl__stack151699);
                                    
                                    
                                    
                                    if (scm_is_eq(SCM_CAR(x152042), np151755)) {
                                         {
                                            SCM x1152043;
                                            x1152043 = SCM_CDR(x152042);
                                             {
                                                SCM ss152044;
                                                ss152044 = SCM_CAR(x1152043);
                                                 {
                                                    SCM x2152045;
                                                    x2152045
                                                      = SCM_CDR(x1152043);
                                                     {
                                                        
                                                        
                                                        
                                                        ctrl__stack151699
                                                          = SCM_CDR(ctrl__stack151699);
                                                        
                                                        s151704 = ss152044;
                                                        
                                                        if (SCM_CONSP(x2152045)) {
                                                             {
                                                                
                                                                p151705
                                                                  = SCM_CAR(x2152045);
                                                                
                                                                scm_fluid_set_x(_sdelayers_s,
                                                                                SCM_CDR(x2152045));
                                                            }
                                                        } else {
                                                            p151705 = x2152045;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                         {
                                            
                                            ctrl__stack151699
                                              = SCM_CDR(ctrl__stack151699);
                                            
                                            goto lp152041;
                                        }
                                    }
                                }
                            }
                        }
                        
                         {
                            int n152037;
                            SCM d152038;
                            n152037
                              = scm_to_int(gp_gp_lookup(_sgp__not__n_s,
                                                        s151704));
                            d152038
                              = gp_gp_lookup(_sgp__is__delayed_p_s, s151704);
                            
                            
                            
                             {
                                
                                scm_fluid_set_x(_sunwind__hooks_s, SCM_EOL);
                                
                                gp_gp_unwind_tail(s151704);
                                
                                 {
                                    SCM l152046;
                                    l152046
                                      = SCM_CDR(scm_fluid_ref(_sunwind__parameters_s));
                                     {
                                        
                                      lp152050:
                                         {
                                            
                                            if (SCM_CONSP(l152046)) {
                                                 {
                                                    SCM f152051;
                                                    f152051 = SCM_CAR(l152046);
                                                    
                                                    
                                                    
                                                    scm_call_1(SCM_CAR(f152051),
                                                               SCM_CDR(f152051));
                                                    
                                                     {
                                                        SCM next152052;
                                                        next152052
                                                          = SCM_CDR(l152046);
                                                        l152046 = next152052;
                                                        goto lp152050;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                
                                 {
                                    SCM l152047;
                                    l152047
                                      = scm_reverse(scm_fluid_ref(_sunwind__hooks_s));
                                     {
                                        
                                      lp152053:
                                         {
                                            
                                            if (SCM_CONSP(l152047)) {
                                                 {
                                                    
                                                    scm_call_3(SCM_CAR(l152047),
                                                               s151704,
                                                               _sfalse_s,
                                                               _strue_s);
                                                    
                                                    gp_gp_unwind_tail(s151704);
                                                    
                                                     {
                                                        SCM next152054;
                                                        next152054
                                                          = SCM_CDR(l152047);
                                                        l152047 = next152054;
                                                        goto lp152053;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            
                            if (n152037 > 1 && scm_is_true(d152038)) {
                                 {
                                    
                                    gp_fluid_force_bang(_sgp__is__delayed_p_s,
                                                        SCM_BOOL_T,
                                                        s151704);
                                    
                                    if (SCM_I_INUMP(p151705)) {
                                         {
                                            
                                            inst__pt151701
                                              = instructions151693
                                                  + scm_to_int(p151705);
                                            
                                             {
                                                void * jmp152048;
                                                jmp152048
                                                  = AREF(_soperations_s,
                                                         scm_to_int(*(inst__pt151701)));
                                                
                                                
                                                
                                                ++inst__pt151701;
                                                
                                                goto *jmp152048;
                                            }
                                        }
                                    } else {
                                         {
                                            
                                            AREF(fp, 0) = p151705;
                                            
                                            sp = fp + -1;
                                            
                                            goto ret;
                                        }
                                    }
                                }
                            } else {
                                 {
                                    void * jmp152049;
                                    jmp152049
                                      = AREF(_soperations_s,
                                             scm_to_int(*(inst__pt151701)));
                                    
                                    
                                    
                                    ++inst__pt151701;
                                    
                                    goto *jmp152049;
                                }
                            }
                        }
                    }
                    
                     {
                        
                        
                      post__s:
                        
                        printf("%s : %d\n", "post-s", 5);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM np151756;
                        np151756 = AREF(inst__pt151701, 0);
                        
                        
                        
                        cut151706 = AREF(inst__pt151701, 1);
                        
                        inst__pt151701 = inst__pt151701 + 2;
                        
                         {
                            
                          lp152064:
                             {
                                
                                 {
                                    SCM x152065;
                                    x152065 = SCM_CAR(ctrl__stack151699);
                                    
                                    
                                    
                                    if (scm_is_eq(SCM_CAR(x152065), np151756)) {
                                         {
                                            SCM x1152066;
                                            x1152066 = SCM_CDR(x152065);
                                             {
                                                SCM ss152067;
                                                ss152067 = SCM_CAR(x1152066);
                                                 {
                                                    SCM x2152068;
                                                    x2152068
                                                      = SCM_CDR(x1152066);
                                                     {
                                                        
                                                        
                                                        
                                                        ctrl__stack151699
                                                          = SCM_CDR(ctrl__stack151699);
                                                        
                                                        s151704 = ss152067;
                                                        
                                                        if (SCM_CONSP(x2152068)) {
                                                             {
                                                                
                                                                p151705
                                                                  = SCM_CAR(x2152068);
                                                                
                                                                scm_fluid_set_x(_sdelayers_s,
                                                                                SCM_CDR(x2152068));
                                                            }
                                                        } else {
                                                            p151705 = x2152068;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                         {
                                            
                                            ctrl__stack151699
                                              = SCM_CDR(ctrl__stack151699);
                                            
                                            goto lp152064;
                                        }
                                    }
                                }
                            }
                        }
                        
                         {
                            
                            scm_fluid_set_x(_sunwind__hooks_s, SCM_EOL);
                            
                            gp_gp_unwind_tail(s151704);
                            
                             {
                                SCM l152059;
                                l152059
                                  = SCM_CDR(scm_fluid_ref(_sunwind__parameters_s));
                                 {
                                    
                                  lp152069:
                                     {
                                        
                                        if (SCM_CONSP(l152059)) {
                                             {
                                                SCM f152070;
                                                f152070 = SCM_CAR(l152059);
                                                
                                                
                                                
                                                scm_call_1(SCM_CAR(f152070),
                                                           SCM_CDR(f152070));
                                                
                                                 {
                                                    SCM next152071;
                                                    next152071
                                                      = SCM_CDR(l152059);
                                                    l152059 = next152071;
                                                    goto lp152069;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            
                             {
                                SCM l152060;
                                l152060
                                  = scm_reverse(scm_fluid_ref(_sunwind__hooks_s));
                                 {
                                    
                                  lp152072:
                                     {
                                        
                                        if (SCM_CONSP(l152060)) {
                                             {
                                                
                                                scm_call_3(SCM_CAR(l152060),
                                                           s151704,
                                                           _sfalse_s,
                                                           _strue_s);
                                                
                                                gp_gp_unwind_tail(s151704);
                                                
                                                 {
                                                    SCM next152073;
                                                    next152073
                                                      = SCM_CDR(l152060);
                                                    l152060 = next152073;
                                                    goto lp152072;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        
                         {
                            void * jmp152061;
                            jmp152061
                              = AREF(_soperations_s,
                                     scm_to_int(*(inst__pt151701)));
                            
                            
                            
                            ++inst__pt151701;
                            
                            goto *jmp152061;
                        }
                    }
                    
                     {
                        
                        
                      post__q:
                        
                        printf("%s : %d\n", "post-q", 6);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM np151757;
                        np151757 = AREF(inst__pt151701, 0);
                        
                        
                        
                        cut151706 = AREF(inst__pt151701, 1);
                        
                        inst__pt151701 = inst__pt151701 + 2;
                        
                         {
                            
                          lp152077:
                             {
                                
                                 {
                                    SCM x152078;
                                    x152078 = SCM_CAR(ctrl__stack151699);
                                    
                                    
                                    
                                    if (scm_is_eq(SCM_CAR(x152078), np151757)) {
                                         {
                                            SCM x1152079;
                                            x1152079 = SCM_CDR(x152078);
                                             {
                                                SCM ss152080;
                                                ss152080 = SCM_CAR(x1152079);
                                                 {
                                                    SCM x2152081;
                                                    x2152081
                                                      = SCM_CDR(x1152079);
                                                     {
                                                        
                                                        
                                                        
                                                        ctrl__stack151699
                                                          = SCM_CDR(ctrl__stack151699);
                                                        
                                                        s151704 = ss152080;
                                                        
                                                        if (SCM_CONSP(x2152081)) {
                                                             {
                                                                
                                                                p151705
                                                                  = SCM_CAR(x2152081);
                                                                
                                                                scm_fluid_set_x(_sdelayers_s,
                                                                                SCM_CDR(x2152081));
                                                            }
                                                        } else {
                                                            p151705 = x2152081;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                         {
                                            
                                            ctrl__stack151699
                                              = SCM_CDR(ctrl__stack151699);
                                            
                                            goto lp152077;
                                        }
                                    }
                                }
                            }
                        }
                        
                         {
                            void * jmp152074;
                            jmp152074
                              = AREF(_soperations_s,
                                     scm_to_int(*(inst__pt151701)));
                            
                            
                            
                            ++inst__pt151701;
                            
                            goto *jmp152074;
                        }
                    }
                    
                     {
                        
                        
                      unwind__tail:
                        
                        printf("%s : %d\n", "unwind-tail", 3);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM tag151758;
                        tag151758 = *inst__pt151701;
                        
                        
                        
                        inst__pt151701 = inst__pt151701 + 1;
                        
                         {
                            
                          lp152087:
                             {
                                
                                 {
                                    SCM x152088;
                                    x152088 = SCM_CAR(ctrl__stack151699);
                                    
                                    
                                    
                                    if (scm_is_eq(SCM_CAR(x152088), tag151758)) {
                                         {
                                            SCM x1152089;
                                            x1152089 = SCM_CDR(x152088);
                                             {
                                                SCM ss152090;
                                                ss152090 = SCM_CAR(x1152089);
                                                 {
                                                    SCM x2152091;
                                                    x2152091
                                                      = SCM_CDR(x1152089);
                                                     {
                                                        
                                                        
                                                        
                                                        ctrl__stack151699
                                                          = SCM_CDR(ctrl__stack151699);
                                                        
                                                        s151704 = ss152090;
                                                        
                                                        if (SCM_CONSP(x2152091)) {
                                                             {
                                                                
                                                                p151705
                                                                  = SCM_CAR(x2152091);
                                                                
                                                                scm_fluid_set_x(_sdelayers_s,
                                                                                SCM_CDR(x2152091));
                                                            }
                                                        } else {
                                                            p151705 = x2152091;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                         {
                                            
                                            ctrl__stack151699
                                              = SCM_CDR(ctrl__stack151699);
                                            
                                            goto lp152087;
                                        }
                                    }
                                }
                            }
                        }
                        
                         {
                            
                            scm_fluid_set_x(_sunwind__hooks_s, SCM_EOL);
                            
                            gp_gp_unwind_tail(s151704);
                            
                             {
                                SCM l152082;
                                l152082
                                  = SCM_CDR(scm_fluid_ref(_sunwind__parameters_s));
                                 {
                                    
                                  lp152092:
                                     {
                                        
                                        if (SCM_CONSP(l152082)) {
                                             {
                                                SCM f152093;
                                                f152093 = SCM_CAR(l152082);
                                                
                                                
                                                
                                                scm_call_1(SCM_CAR(f152093),
                                                           SCM_CDR(f152093));
                                                
                                                 {
                                                    SCM next152094;
                                                    next152094
                                                      = SCM_CDR(l152082);
                                                    l152082 = next152094;
                                                    goto lp152092;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            
                             {
                                SCM l152083;
                                l152083
                                  = scm_reverse(scm_fluid_ref(_sunwind__hooks_s));
                                 {
                                    
                                  lp152095:
                                     {
                                        
                                        if (SCM_CONSP(l152083)) {
                                             {
                                                
                                                scm_call_3(SCM_CAR(l152083),
                                                           s151704,
                                                           _sfalse_s,
                                                           _strue_s);
                                                
                                                gp_gp_unwind_tail(s151704);
                                                
                                                 {
                                                    SCM next152096;
                                                    next152096
                                                      = SCM_CDR(l152083);
                                                    l152083 = next152096;
                                                    goto lp152095;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        
                         {
                            void * jmp152084;
                            jmp152084
                              = AREF(_soperations_s,
                                     scm_to_int(*(inst__pt151701)));
                            
                            
                            
                            ++inst__pt151701;
                            
                            goto *jmp152084;
                        }
                    }
                    
                     {
                        
                        
                      softie:
                        
                        printf("%s : %d\n", "softie", 4);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM tag151759;
                        tag151759 = *inst__pt151701;
                        
                        
                        
                        inst__pt151701 = inst__pt151701 + 1;
                        
                         {
                            
                          lp152100:
                             {
                                
                                 {
                                    SCM x152101;
                                    x152101 = SCM_CAR(ctrl__stack151699);
                                    
                                    
                                    
                                    if (scm_is_eq(SCM_CAR(x152101), tag151759)) {
                                         {
                                            SCM x1152102;
                                            x1152102 = SCM_CDR(x152101);
                                             {
                                                SCM x2152103;
                                                x2152103 = SCM_CDR(x1152102);
                                                 {
                                                    
                                                    
                                                    
                                                    ctrl__stack151699
                                                      = SCM_CDR(ctrl__stack151699);
                                                    
                                                    if (SCM_CONSP(x2152103)) {
                                                        p151705
                                                          = SCM_CAR(x2152103);
                                                    } else {
                                                        p151705 = x2152103;
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                         {
                                            
                                            ctrl__stack151699
                                              = SCM_CDR(ctrl__stack151699);
                                            
                                            goto lp152100;
                                        }
                                    }
                                }
                            }
                        }
                        
                         {
                            void * jmp152097;
                            jmp152097
                              = AREF(_soperations_s,
                                     scm_to_int(*(inst__pt151701)));
                            
                            
                            
                            ++inst__pt151701;
                            
                            goto *jmp152097;
                        }
                    }
                    
                     {
                        
                        
                      unwind:
                        
                        printf("%s : %d\n", "unwind", 2);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM tag151760;
                        tag151760 = AREF(inst__pt151701, 0);
                        
                        
                        
                        p151705 = AREF(inst__pt151701, 1);
                        
                        inst__pt151701 = inst__pt151701 + 2;
                        
                         {
                            
                          lp152109:
                             {
                                
                                 {
                                    SCM x152110;
                                    x152110 = SCM_CAR(ctrl__stack151699);
                                    
                                    
                                    
                                    if (scm_is_eq(SCM_CAR(x152110), tag151760)) {
                                         {
                                            SCM x1152111;
                                            x1152111 = SCM_CDR(x152110);
                                             {
                                                SCM ss152112;
                                                ss152112 = SCM_CAR(x1152111);
                                                 {
                                                    SCM x2152113;
                                                    x2152113
                                                      = SCM_CDR(x1152111);
                                                     {
                                                        
                                                        
                                                        
                                                        s151704 = ss152112;
                                                        
                                                        if (SCM_CONSP(x2152113)) {
                                                            scm_fluid_set_x(_sdelayers_s,
                                                                            SCM_CDR(x2152113));
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                         {
                                            
                                            ctrl__stack151699
                                              = SCM_CDR(ctrl__stack151699);
                                            
                                            goto lp152109;
                                        }
                                    }
                                }
                            }
                        }
                        
                         {
                            
                            scm_fluid_set_x(_sunwind__hooks_s, SCM_EOL);
                            
                            gp_gp_unwind(s151704);
                            
                             {
                                SCM l152104;
                                l152104
                                  = SCM_CDR(scm_fluid_ref(_sunwind__parameters_s));
                                 {
                                    
                                  lp152114:
                                     {
                                        
                                        if (SCM_CONSP(l152104)) {
                                             {
                                                SCM f152115;
                                                f152115 = SCM_CAR(l152104);
                                                
                                                
                                                
                                                scm_call_1(SCM_CAR(f152115),
                                                           SCM_CDR(f152115));
                                                
                                                 {
                                                    SCM next152116;
                                                    next152116
                                                      = SCM_CDR(l152104);
                                                    l152104 = next152116;
                                                    goto lp152114;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            
                             {
                                SCM l152105;
                                l152105
                                  = scm_reverse(scm_fluid_ref(_sunwind__hooks_s));
                                 {
                                    
                                  lp152117:
                                     {
                                        
                                        if (SCM_CONSP(l152105)) {
                                             {
                                                
                                                scm_call_3(SCM_CAR(l152105),
                                                           s151704,
                                                           _sfalse_s,
                                                           _strue_s);
                                                
                                                gp_gp_unwind(s151704);
                                                
                                                 {
                                                    SCM next152118;
                                                    next152118
                                                      = SCM_CDR(l152105);
                                                    l152105 = next152118;
                                                    goto lp152117;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        
                         {
                            void * jmp152106;
                            jmp152106
                              = AREF(_soperations_s,
                                     scm_to_int(*(inst__pt151701)));
                            
                            
                            
                            ++inst__pt151701;
                            
                            goto *jmp152106;
                        }
                    }
                    
                     {
                        
                        
                      unwind__negation:
                        
                        printf("%s : %d\n", "unwind-negation", 8);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM n151761;
                        n151761 = *inst__pt151701;
                        
                        
                        
                        cut151706 = AREF(inst__pt151701, 1);
                        
                         {
                            
                          lp152122:
                             {
                                
                                 {
                                    SCM x152123;
                                    x152123 = SCM_CAR(ctrl__stack151699);
                                    
                                    
                                    
                                    if (scm_is_eq(SCM_CAR(x152123), n151761)) {
                                         {
                                            SCM x1152124;
                                            x1152124 = SCM_CDR(x152123);
                                             {
                                                SCM ss152125;
                                                ss152125 = SCM_CAR(x1152124);
                                                 {
                                                    SCM x2152126;
                                                    x2152126
                                                      = SCM_CDR(x1152124);
                                                     {
                                                        
                                                        
                                                        
                                                        ctrl__stack151699
                                                          = SCM_CDR(ctrl__stack151699);
                                                        
                                                        s151704 = ss152125;
                                                        
                                                        if (SCM_CONSP(x2152126)) {
                                                             {
                                                                
                                                                p151705
                                                                  = SCM_CAR(x2152126);
                                                                
                                                                scm_fluid_set_x(_sdelayers_s,
                                                                                SCM_CDR(x2152126));
                                                            }
                                                        } else {
                                                            p151705 = x2152126;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                         {
                                            
                                            ctrl__stack151699
                                              = SCM_CDR(ctrl__stack151699);
                                            
                                            goto lp152122;
                                        }
                                    }
                                }
                            }
                        }
                        
                         {
                            
                            scm_fluid_set_x(_sunwind__hooks_s, SCM_EOL);
                            
                            gp_gp_unwind_tail(s151704);
                            
                             {
                                SCM l152119;
                                l152119
                                  = SCM_CDR(scm_fluid_ref(_sunwind__parameters_s));
                                 {
                                    
                                  lp152127:
                                     {
                                        
                                        if (SCM_CONSP(l152119)) {
                                             {
                                                SCM f152128;
                                                f152128 = SCM_CAR(l152119);
                                                
                                                
                                                
                                                scm_call_1(SCM_CAR(f152128),
                                                           SCM_CDR(f152128));
                                                
                                                 {
                                                    SCM next152129;
                                                    next152129
                                                      = SCM_CDR(l152119);
                                                    l152119 = next152129;
                                                    goto lp152127;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            
                             {
                                SCM l152120;
                                l152120
                                  = scm_reverse(scm_fluid_ref(_sunwind__hooks_s));
                                 {
                                    
                                  lp152130:
                                     {
                                        
                                        if (SCM_CONSP(l152120)) {
                                             {
                                                
                                                scm_call_3(SCM_CAR(l152120),
                                                           s151704,
                                                           _sfalse_s,
                                                           _strue_s);
                                                
                                                gp_gp_unwind_tail(s151704);
                                                
                                                 {
                                                    SCM next152131;
                                                    next152131
                                                      = SCM_CDR(l152120);
                                                    l152120 = next152131;
                                                    goto lp152130;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        
                        gp_fluid_force_bang(_sgp__is__delayed_p_s,
                                            SCM_BOOL_F,
                                            s151704);
                        
                        if (SCM_I_INUMP(p151705)) {
                             {
                                
                                inst__pt151701
                                  = instructions151693 + scm_to_int(p151705);
                                
                                 {
                                    void * jmp152121;
                                    jmp152121
                                      = AREF(_soperations_s,
                                             scm_to_int(*(inst__pt151701)));
                                    
                                    
                                    
                                    ++inst__pt151701;
                                    
                                    goto *jmp152121;
                                }
                            }
                        } else {
                             {
                                
                                AREF(fp, 0) = p151705;
                                
                                sp = fp + -1;
                                
                                goto ret;
                            }
                        }
                    }
                    
                     {
                        
                        
                      false:
                        
                        printf("%s : %d\n", "false", 28);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                    if (SCM_I_INUMP(p151705)) {
                         {
                            
                            inst__pt151701
                              = instructions151693 + scm_to_int(p151705);
                            
                             {
                                void * jmp151762;
                                jmp151762
                                  = AREF(_soperations_s,
                                         scm_to_int(*(inst__pt151701)));
                                
                                
                                
                                ++inst__pt151701;
                                
                                goto *jmp151762;
                            }
                        }
                    } else {
                         {
                            
                            AREF(fp, 0) = p151705;
                            
                            sp = fp + -1;
                            
                            goto ret;
                        }
                    }
                    
                     {
                        
                        
                      goto__inst:
                        
                        printf("%s : %d\n", "goto-inst", 35);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        int ni151763;
                        ni151763 = scm_to_int(*(inst__pt151701));
                        
                        
                        
                        inst__pt151701 = instructions151693 + ni151763;
                        
                         {
                            void * jmp152140;
                            jmp152140
                              = AREF(_soperations_s,
                                     scm_to_int(*(inst__pt151701)));
                            
                            
                            
                            ++inst__pt151701;
                            
                            goto *jmp152140;
                        }
                    }
                    
                     {
                        
                        
                      post__call:
                        
                        printf("%s : %d\n", "post-call", 37);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM c151764;
                        SCM pop_p151765;
                        c151764 = AREF(inst__pt151701, 0);
                        pop_p151765 = AREF(inst__pt151701, 1);
                        
                        
                        
                        inst__pt151701 = inst__pt151701 + 2;
                        
                        if (call_p151681) {
                             {
                                
                                call_p151681 = 0;
                                
                                if (scm_is_true(pop_p151765)) {
                                    sp = sp + 3;
                                }
                                
                                cut151706 = c151764;
                                
                                if (SCM_CONSP(sp__stack151700)) {
                                     {
                                        SCM st152143;
                                        st152143 = sp__stack151700;
                                        
                                        
                                        
                                         {
                                            SCM * vp152149;
                                            vp152149 = fp + -nstack151697;
                                            
                                            
                                            
                                             {
                                                int i152153;
                                                i152153 = 0;
                                                 {
                                                    
                                                  lp152158:
                                                     {
                                                        
                                                        if (i152153 > 0) {
                                                            if (SCM_CONSP(st152143)) {
                                                                 {
                                                                    
                                                                    AREF(vp152149,
                                                                         0)
                                                                      = SCM_CAR(st152143);
                                                                    
                                                                    st152143
                                                                      = SCM_CDR(st152143);
                                                                    
                                                                    vp152149
                                                                      = vp152149
                                                                          - 1;
                                                                    
                                                                     {
                                                                        int next152161;
                                                                        next152161
                                                                          = i152153
                                                                              - 1;
                                                                        i152153
                                                                          = next152161;
                                                                        goto lp152158;
                                                                    }
                                                                }
                                                            } else {
                                                                scm_misc_error("prolog-vm",
                                                                               "Stack link on length missmatch",
                                                                               SCM_EOL);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        
                                         {
                                            int n152152;
                                             {
                                                SCM s152150;
                                                s152150 = st152143;
                                                 {
                                                    int i152151;
                                                    i152151 = 0;
                                                     {
                                                        
                                                      lp152166:
                                                         {
                                                            
                                                             {
                                                                SCM next152167;
                                                                int next152168;
                                                                next152167
                                                                  = SCM_CDR(s152150);
                                                                next152168
                                                                  = i152151
                                                                      + 1;
                                                                s152150
                                                                  = next152167;
                                                                i152151
                                                                  = next152168;
                                                                goto lp152166;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            
                                            
                                            
                                             {
                                                int n152164;
                                                if (fp > sp) {
                                                    n152164 = fp - sp;
                                                } else {
                                                     {
                                                        
                                                        scm_misc_error("prolog-vm",
                                                                       "negative stack pointer",
                                                                       SCM_EOL);
                                                        
                                                        n152164 = 1;
                                                    }
                                                }
                                                
                                                
                                                
                                                if (n152164) {
                                                     {
                                                        int i152171;
                                                        i152171
                                                          = nstack151697
                                                              + n152164;
                                                         {
                                                            
                                                          lp152174:
                                                             {
                                                                
                                                                if (!(fp == sp)) {
                                                                     {
                                                                        
                                                                        AREF(fp,
                                                                             -(i152171))
                                                                          = AREF(sp,
                                                                                 1);
                                                                        
                                                                        --sp;
                                                                        
                                                                         {
                                                                            int next152180;
                                                                            next152180
                                                                              = i152171
                                                                                  - 1;
                                                                            i152171
                                                                              = next152180;
                                                                            goto lp152174;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            
                                             {
                                                SCM * vp152165;
                                                vp152165 = fp + -0;
                                                
                                                
                                                
                                                 {
                                                    int i152183;
                                                    i152183 = n152152;
                                                     {
                                                        
                                                      lp152188:
                                                         {
                                                            
                                                            if (i152183 > 0) {
                                                                if (SCM_CONSP(st152143)) {
                                                                     {
                                                                        
                                                                        AREF(vp152165,
                                                                             0)
                                                                          = SCM_CAR(st152143);
                                                                        
                                                                        st152143
                                                                          = SCM_CDR(st152143);
                                                                        
                                                                        vp152165
                                                                          = vp152165
                                                                              - 1;
                                                                        
                                                                         {
                                                                            int next152191;
                                                                            next152191
                                                                              = i152183
                                                                                  - 1;
                                                                            i152183
                                                                              = next152191;
                                                                            goto lp152188;
                                                                        }
                                                                    }
                                                                } else {
                                                                    scm_misc_error("prolog-vm",
                                                                                   "Stack link on length missmatch",
                                                                                   SCM_EOL);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            
                                            sp = sp + nstack151697;
                                        }
                                    }
                                }
                                
                                 {
                                    void * jmp152144;
                                    jmp152144
                                      = AREF(_soperations_s,
                                             scm_to_int(*(inst__pt151701)));
                                    
                                    
                                    
                                    ++inst__pt151701;
                                    
                                    goto *jmp152144;
                                }
                            }
                        }
                    }
                    
                     {
                        
                        
                      post__unicall:
                        
                        printf("%s : %d\n", "post-unicall", 38);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM c151766;
                        int nsloc151767;
                        c151766 = AREF(inst__pt151701, 0);
                        nsloc151767 = scm_to_int(AREF(inst__pt151701, 1));
                        
                        
                        
                        inst__pt151701 = inst__pt151701 + 2;
                        
                        if (call_p151681) {
                             {
                                
                                call_p151681 = 0;
                                
                                sp = sp + 3;
                                
                                cut151706 = c151766;
                                
                                if (SCM_CONSP(sp__stack151700)) {
                                     {
                                        SCM st152196;
                                        st152196 = sp__stack151700;
                                        
                                        
                                        
                                         {
                                            SCM * vp152202;
                                            vp152202 = fp + -nstack151697;
                                            
                                            
                                            
                                             {
                                                int i152206;
                                                i152206 = nsloc151767;
                                                 {
                                                    
                                                  lp152211:
                                                     {
                                                        
                                                        if (i152206 > 0) {
                                                            if (SCM_CONSP(st152196)) {
                                                                 {
                                                                    
                                                                    AREF(vp152202,
                                                                         0)
                                                                      = SCM_CAR(st152196);
                                                                    
                                                                    st152196
                                                                      = SCM_CDR(st152196);
                                                                    
                                                                    vp152202
                                                                      = vp152202
                                                                          - 1;
                                                                    
                                                                     {
                                                                        int next152214;
                                                                        next152214
                                                                          = i152206
                                                                              - 1;
                                                                        i152206
                                                                          = next152214;
                                                                        goto lp152211;
                                                                    }
                                                                }
                                                            } else {
                                                                scm_misc_error("prolog-vm",
                                                                               "Stack link on length missmatch",
                                                                               SCM_EOL);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        
                                         {
                                            int n152205;
                                             {
                                                SCM s152203;
                                                s152203 = st152196;
                                                 {
                                                    int i152204;
                                                    i152204 = 0;
                                                     {
                                                        
                                                      lp152219:
                                                         {
                                                            
                                                             {
                                                                SCM next152220;
                                                                int next152221;
                                                                next152220
                                                                  = SCM_CDR(s152203);
                                                                next152221
                                                                  = i152204
                                                                      + 1;
                                                                s152203
                                                                  = next152220;
                                                                i152204
                                                                  = next152221;
                                                                goto lp152219;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            
                                            
                                            
                                             {
                                                int n152217;
                                                if (fp > sp) {
                                                    n152217 = fp - sp;
                                                } else {
                                                     {
                                                        
                                                        scm_misc_error("prolog-vm",
                                                                       "negative stack pointer",
                                                                       SCM_EOL);
                                                        
                                                        n152217 = 1;
                                                    }
                                                }
                                                
                                                
                                                
                                                if (n152217) {
                                                     {
                                                        int i152224;
                                                        i152224
                                                          = nstack151697
                                                              + n152217;
                                                         {
                                                            
                                                          lp152227:
                                                             {
                                                                
                                                                if (!(fp == sp)) {
                                                                     {
                                                                        
                                                                        AREF(fp,
                                                                             -(i152224))
                                                                          = AREF(sp,
                                                                                 1);
                                                                        
                                                                        --sp;
                                                                        
                                                                         {
                                                                            int next152233;
                                                                            next152233
                                                                              = i152224
                                                                                  - 1;
                                                                            i152224
                                                                              = next152233;
                                                                            goto lp152227;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            
                                             {
                                                SCM * vp152218;
                                                vp152218 = fp + -0;
                                                
                                                
                                                
                                                 {
                                                    int i152236;
                                                    i152236 = n152205;
                                                     {
                                                        
                                                      lp152241:
                                                         {
                                                            
                                                            if (i152236 > 0) {
                                                                if (SCM_CONSP(st152196)) {
                                                                     {
                                                                        
                                                                        AREF(vp152218,
                                                                             0)
                                                                          = SCM_CAR(st152196);
                                                                        
                                                                        st152196
                                                                          = SCM_CDR(st152196);
                                                                        
                                                                        vp152218
                                                                          = vp152218
                                                                              - 1;
                                                                        
                                                                         {
                                                                            int next152244;
                                                                            next152244
                                                                              = i152236
                                                                                  - 1;
                                                                            i152236
                                                                              = next152244;
                                                                            goto lp152241;
                                                                        }
                                                                    }
                                                                } else {
                                                                    scm_misc_error("prolog-vm",
                                                                                   "Stack link on length missmatch",
                                                                                   SCM_EOL);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            
                                            sp = sp + nstack151697;
                                        }
                                    }
                                }
                            }
                        }
                        
                         {
                            void * jmp152197;
                            jmp152197
                              = AREF(_soperations_s,
                                     scm_to_int(*(inst__pt151701)));
                            
                            
                            
                            ++inst__pt151701;
                            
                            goto *jmp152197;
                        }
                    }
                    
                     {
                        
                        
                      fail:
                        
                        printf("%s : %d\n", "fail", 29);
                    }
                    
                    if (scm_is_eq(p151705, scm_num6942)) {
                        p151705 = AREF(variables151689, 1);
                    }
                    
                    if (SCM_I_INUMP(p151705)) {
                         {
                            
                            inst__pt151701
                              = instructions151693 + scm_to_int(p151705);
                            
                             {
                                void * jmp151768;
                                jmp151768
                                  = AREF(_soperations_s,
                                         scm_to_int(*(inst__pt151701)));
                                
                                
                                
                                ++inst__pt151701;
                                
                                goto *jmp151768;
                            }
                        }
                    } else {
                         {
                            
                            AREF(fp, 0) = p151705;
                            
                            sp = fp + -1;
                            
                            goto ret;
                        }
                    }
                    
                     {
                        
                        
                      cut:
                        
                        printf("%s : %d\n", "cut", 36);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                    if (scm_is_eq(cut151706, scm_num6942)) {
                        p151705 = AREF(variables151689, 1);
                    } else {
                        p151705 = cut151706;
                    }
                    
                     {
                        void * jmp151769;
                        jmp151769
                          = AREF(_soperations_s, scm_to_int(*(inst__pt151701)));
                        
                        
                        
                        ++inst__pt151701;
                        
                        goto *jmp151769;
                    }
                    
                     {
                        
                        
                      set:
                        
                        printf("%s : %d\n", "set", 10);
                    }
                    
                     {
                        SCM n151770;
                        n151770 = AREF(inst__pt151701, 0);
                         {
                            SCM ss151773;
                             {
                                SCM a152254;
                                SCM a152255;
                                SCM a152256;
                                if (SCM_CONSP(n151770)) {
                                     {
                                        int i151771;
                                        i151771 = scm_to_int(SCM_CAR(n151770));
                                        
                                        
                                        
                                        a152254
                                          = AREF(fp, -(nstack151697 + i151771));
                                    }
                                } else {
                                     {
                                        int i151772;
                                        i151772 = scm_to_int(n151770);
                                        
                                        
                                        
                                        if (0) {
                                            if (scm_is_true(pinned_p151680)) {
                                                 {
                                                    
                                                    variables__scm151688
                                                      = gp_copy_vector(&(variables151689),
                                                                       nvar151698);
                                                    
                                                    session151686
                                                      = scm_cons(variables__scm151688,
                                                                 cnst151687);
                                                    
                                                    middle151685
                                                      = scm_cons(SCM_EOL,
                                                                 session151686);
                                                    
                                                    pinned_p151680
                                                      = SCM_BOOL_F;
                                                }
                                            }
                                        }
                                        
                                        a152254
                                          = AREF(variables151689, i151772);
                                    }
                                }
                                a152255 = AREF(sp, 1);
                                a152256 = s151704;
                                ss151773 = gp_set(a152254, a152255, a152256);
                            }
                             {
                                
                                
                                
                                inst__pt151701 = inst__pt151701 + 1;
                                
                                sp = sp + 1;
                                
                                s151704 = ss151773;
                                
                                 {
                                    void * jmp152253;
                                    jmp152253
                                      = AREF(_soperations_s,
                                             scm_to_int(*(inst__pt151701)));
                                    
                                    
                                    
                                    ++inst__pt151701;
                                    
                                    goto *jmp152253;
                                }
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          unify:
                            
                            printf("%s : %d\n", "unify", 20);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM n151774;
                            n151774 = AREF(inst__pt151701, 1);
                             {
                                SCM m151775;
                                m151775 = AREF(inst__pt151701, 0);
                                 {
                                    SCM k151776;
                                    k151776 = AREF(inst__pt151701, 2);
                                     {
                                        
                                        
                                        
                                        inst__pt151701 = inst__pt151701 + 3;
                                        
                                        if (k151776 == SCM_BOOL_F) {
                                             {
                                                
                                                if (SCM_CONSP(n151774)) {
                                                     {
                                                        int i152262;
                                                        i152262
                                                          = scm_to_int(SCM_CAR(n151774));
                                                        
                                                        
                                                        
                                                        AREF(fp,
                                                             -(nstack151697
                                                                 + i152262))
                                                          = AREF(sp, 1);
                                                    }
                                                } else {
                                                     {
                                                        int i152263;
                                                        i152263
                                                          = scm_to_int(n151774);
                                                        
                                                        
                                                        
                                                        if (1) {
                                                            if (scm_is_true(pinned_p151680)) {
                                                                 {
                                                                    
                                                                    variables__scm151688
                                                                      = gp_copy_vector(&(variables151689),
                                                                                       nvar151698);
                                                                    
                                                                    session151686
                                                                      = scm_cons(variables__scm151688,
                                                                                 cnst151687);
                                                                    
                                                                    middle151685
                                                                      = scm_cons(SCM_EOL,
                                                                                 session151686);
                                                                    
                                                                    pinned_p151680
                                                                      = SCM_BOOL_F;
                                                                }
                                                            }
                                                        }
                                                        
                                                        AREF(variables151689,
                                                             i152263)
                                                          = AREF(sp, 1);
                                                    }
                                                }
                                                
                                                 {
                                                    
                                                    AREF(sp, 1) = SCM_BOOL_F;
                                                    
                                                    sp = sp + 1;
                                                }
                                                
                                                 {
                                                    void * jmp152264;
                                                    jmp152264
                                                      = AREF(_soperations_s,
                                                             scm_to_int(*(inst__pt151701)));
                                                    
                                                    
                                                    
                                                    ++inst__pt151701;
                                                    
                                                    goto *jmp152264;
                                                }
                                            }
                                        } else {
                                             {
                                                SCM x152267;
                                                if (SCM_CONSP(n151774)) {
                                                     {
                                                        int i152265;
                                                        i152265
                                                          = scm_to_int(SCM_CAR(n151774));
                                                        
                                                        
                                                        
                                                        x152267
                                                          = AREF(fp,
                                                                 -(nstack151697
                                                                     + i152265));
                                                    }
                                                } else {
                                                     {
                                                        int i152266;
                                                        i152266
                                                          = scm_to_int(n151774);
                                                        
                                                        
                                                        
                                                        if (0) {
                                                            if (scm_is_true(pinned_p151680)) {
                                                                 {
                                                                    
                                                                    variables__scm151688
                                                                      = gp_copy_vector(&(variables151689),
                                                                                       nvar151698);
                                                                    
                                                                    session151686
                                                                      = scm_cons(variables__scm151688,
                                                                                 cnst151687);
                                                                    
                                                                    middle151685
                                                                      = scm_cons(SCM_EOL,
                                                                                 session151686);
                                                                    
                                                                    pinned_p151680
                                                                      = SCM_BOOL_F;
                                                                }
                                                            }
                                                        }
                                                        
                                                        x152267
                                                          = AREF(variables151689,
                                                                 i152266);
                                                    }
                                                }
                                                 {
                                                    SCM y152268;
                                                    y152268 = AREF(sp, 1);
                                                     {
                                                        SCM ss152269;
                                                        if (m151775
                                                              == SCM_BOOL_F) {
                                                            ss152269
                                                              = gp_m_unify(x152267,
                                                                           y152268,
                                                                           s151704);
                                                        } else {
                                                            if (m151775
                                                                  == SCM_BOOL_T
                                                                  || k151776
                                                                       == SCM_BOOL_T) {
                                                                ss152269
                                                                  = gp_gp_unify_raw(x152267,
                                                                                    y152268,
                                                                                    s151704);
                                                            } else {
                                                                ss152269
                                                                  = gp_gp_unify(x152267,
                                                                                y152268,
                                                                                s151704);
                                                            }
                                                        }
                                                         {
                                                            
                                                            
                                                            
                                                             {
                                                                
                                                                AREF(sp, 1)
                                                                  = SCM_BOOL_F;
                                                                
                                                                sp = sp + 1;
                                                            }
                                                            
                                                            if (scm_is_true(ss152269)) {
                                                                 {
                                                                    
                                                                    s151704
                                                                      = ss152269;
                                                                    
                                                                     {
                                                                        void * jmp152275;
                                                                        jmp152275
                                                                          = AREF(_soperations_s,
                                                                                 scm_to_int(*(inst__pt151701)));
                                                                        
                                                                        
                                                                        
                                                                        ++inst__pt151701;
                                                                        
                                                                        goto *jmp152275;
                                                                    }
                                                                }
                                                            } else {
                                                                if (SCM_I_INUMP(p151705)) {
                                                                     {
                                                                        
                                                                        inst__pt151701
                                                                          = instructions151693
                                                                              + scm_to_int(p151705);
                                                                        
                                                                         {
                                                                            void * jmp152276;
                                                                            jmp152276
                                                                              = AREF(_soperations_s,
                                                                                     scm_to_int(*(inst__pt151701)));
                                                                            
                                                                            
                                                                            
                                                                            ++inst__pt151701;
                                                                            
                                                                            goto *jmp152276;
                                                                        }
                                                                    }
                                                                } else {
                                                                     {
                                                                        
                                                                        AREF(fp,
                                                                             0)
                                                                          = p151705;
                                                                        
                                                                        sp
                                                                          = fp
                                                                              + -1;
                                                                        
                                                                        goto ret;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          unify__2:
                            
                            printf("%s : %d\n", "unify-2", 19);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM m151777;
                            m151777 = AREF(inst__pt151701, 0);
                             {
                                SCM n1151778;
                                n1151778 = AREF(inst__pt151701, 1);
                                 {
                                    SCM k1151779;
                                    k1151779 = AREF(inst__pt151701, 2);
                                     {
                                        SCM n2151780;
                                        n2151780 = AREF(inst__pt151701, 3);
                                         {
                                            SCM k2151781;
                                            k2151781 = AREF(inst__pt151701, 4);
                                             {
                                                
                                                
                                                
                                                inst__pt151701
                                                  = inst__pt151701 + 5;
                                                
                                                if (k1151779 == SCM_BOOL_F) {
                                                     {
                                                        
                                                        if (scm_is_eq(k2151781,
                                                                      SCM_BOOL_F)) {
                                                            if (SCM_CONSP(n2151780)) {
                                                                 {
                                                                    int i2152284;
                                                                    i2152284
                                                                      = scm_to_int(SCM_CAR(n2151780));
                                                                    
                                                                    
                                                                    
                                                                    AREF(fp,
                                                                         -(nstack151697
                                                                             + i2152284))
                                                                      = gp_mkvar(s151704);
                                                                }
                                                            } else {
                                                                 {
                                                                    int i2152285;
                                                                    i2152285
                                                                      = scm_to_int(n2151780);
                                                                    
                                                                    
                                                                    
                                                                    if (1) {
                                                                        if (scm_is_true(pinned_p151680)) {
                                                                             {
                                                                                
                                                                                variables__scm151688
                                                                                  = gp_copy_vector(&(variables151689),
                                                                                                   nvar151698);
                                                                                
                                                                                session151686
                                                                                  = scm_cons(variables__scm151688,
                                                                                             cnst151687);
                                                                                
                                                                                middle151685
                                                                                  = scm_cons(SCM_EOL,
                                                                                             session151686);
                                                                                
                                                                                pinned_p151680
                                                                                  = SCM_BOOL_F;
                                                                            }
                                                                        }
                                                                    }
                                                                    
                                                                    AREF(variables151689,
                                                                         i2152285)
                                                                      = gp_mkvar(s151704);
                                                                }
                                                            }
                                                        }
                                                        
                                                         {
                                                            SCM rhs152288;
                                                            if (SCM_CONSP(n2151780)) {
                                                                 {
                                                                    int i2152286;
                                                                    i2152286
                                                                      = scm_to_int(SCM_CAR(n2151780));
                                                                    
                                                                    
                                                                    
                                                                    rhs152288
                                                                      = AREF(fp,
                                                                             -(nstack151697
                                                                                 + i2152286));
                                                                }
                                                            } else {
                                                                 {
                                                                    int i2152287;
                                                                    i2152287
                                                                      = scm_to_int(n2151780);
                                                                    
                                                                    
                                                                    
                                                                    if (0) {
                                                                        if (scm_is_true(pinned_p151680)) {
                                                                             {
                                                                                
                                                                                variables__scm151688
                                                                                  = gp_copy_vector(&(variables151689),
                                                                                                   nvar151698);
                                                                                
                                                                                session151686
                                                                                  = scm_cons(variables__scm151688,
                                                                                             cnst151687);
                                                                                
                                                                                middle151685
                                                                                  = scm_cons(SCM_EOL,
                                                                                             session151686);
                                                                                
                                                                                pinned_p151680
                                                                                  = SCM_BOOL_F;
                                                                            }
                                                                        }
                                                                    }
                                                                    
                                                                    rhs152288
                                                                      = AREF(variables151689,
                                                                             i2152287);
                                                                }
                                                            }
                                                            
                                                            
                                                            
                                                            if (SCM_CONSP(n1151778)) {
                                                                 {
                                                                    int i1152303;
                                                                    i1152303
                                                                      = scm_to_int(SCM_CAR(n1151778));
                                                                    
                                                                    
                                                                    
                                                                    AREF(fp,
                                                                         -(nstack151697
                                                                             + i1152303))
                                                                      = rhs152288;
                                                                }
                                                            } else {
                                                                 {
                                                                    int i1152304;
                                                                    i1152304
                                                                      = scm_to_int(n1151778);
                                                                    
                                                                    
                                                                    
                                                                    if (1) {
                                                                        if (scm_is_true(pinned_p151680)) {
                                                                             {
                                                                                
                                                                                variables__scm151688
                                                                                  = gp_copy_vector(&(variables151689),
                                                                                                   nvar151698);
                                                                                
                                                                                session151686
                                                                                  = scm_cons(variables__scm151688,
                                                                                             cnst151687);
                                                                                
                                                                                middle151685
                                                                                  = scm_cons(SCM_EOL,
                                                                                             session151686);
                                                                                
                                                                                pinned_p151680
                                                                                  = SCM_BOOL_F;
                                                                            }
                                                                        }
                                                                    }
                                                                    
                                                                    AREF(variables151689,
                                                                         i1152304)
                                                                      = rhs152288;
                                                                }
                                                            }
                                                        }
                                                        
                                                         {
                                                            void * jmp152289;
                                                            jmp152289
                                                              = AREF(_soperations_s,
                                                                     scm_to_int(*(inst__pt151701)));
                                                            
                                                            
                                                            
                                                            ++inst__pt151701;
                                                            
                                                            goto *jmp152289;
                                                        }
                                                    }
                                                } else {
                                                    if (k2151781 == SCM_BOOL_F) {
                                                         {
                                                            SCM rhs152292;
                                                            if (SCM_CONSP(n1151778)) {
                                                                 {
                                                                    int i2152290;
                                                                    i2152290
                                                                      = scm_to_int(SCM_CAR(n1151778));
                                                                    
                                                                    
                                                                    
                                                                    rhs152292
                                                                      = AREF(fp,
                                                                             -(nstack151697
                                                                                 + i2152290));
                                                                }
                                                            } else {
                                                                 {
                                                                    int i2152291;
                                                                    i2152291
                                                                      = scm_to_int(n1151778);
                                                                    
                                                                    
                                                                    
                                                                    if (0) {
                                                                        if (scm_is_true(pinned_p151680)) {
                                                                             {
                                                                                
                                                                                variables__scm151688
                                                                                  = gp_copy_vector(&(variables151689),
                                                                                                   nvar151698);
                                                                                
                                                                                session151686
                                                                                  = scm_cons(variables__scm151688,
                                                                                             cnst151687);
                                                                                
                                                                                middle151685
                                                                                  = scm_cons(SCM_EOL,
                                                                                             session151686);
                                                                                
                                                                                pinned_p151680
                                                                                  = SCM_BOOL_F;
                                                                            }
                                                                        }
                                                                    }
                                                                    
                                                                    rhs152292
                                                                      = AREF(variables151689,
                                                                             i2152291);
                                                                }
                                                            }
                                                            
                                                            
                                                            
                                                            if (SCM_CONSP(n2151780)) {
                                                                 {
                                                                    int i1152307;
                                                                    i1152307
                                                                      = scm_to_int(SCM_CAR(n2151780));
                                                                    
                                                                    
                                                                    
                                                                    AREF(fp,
                                                                         -(nstack151697
                                                                             + i1152307))
                                                                      = rhs152292;
                                                                }
                                                            } else {
                                                                 {
                                                                    int i1152308;
                                                                    i1152308
                                                                      = scm_to_int(n2151780);
                                                                    
                                                                    
                                                                    
                                                                    if (1) {
                                                                        if (scm_is_true(pinned_p151680)) {
                                                                             {
                                                                                
                                                                                variables__scm151688
                                                                                  = gp_copy_vector(&(variables151689),
                                                                                                   nvar151698);
                                                                                
                                                                                session151686
                                                                                  = scm_cons(variables__scm151688,
                                                                                             cnst151687);
                                                                                
                                                                                middle151685
                                                                                  = scm_cons(SCM_EOL,
                                                                                             session151686);
                                                                                
                                                                                pinned_p151680
                                                                                  = SCM_BOOL_F;
                                                                            }
                                                                        }
                                                                    }
                                                                    
                                                                    AREF(variables151689,
                                                                         i1152308)
                                                                      = rhs152292;
                                                                }
                                                            }
                                                            
                                                             {
                                                                void * jmp152309;
                                                                jmp152309
                                                                  = AREF(_soperations_s,
                                                                         scm_to_int(*(inst__pt151701)));
                                                                
                                                                
                                                                
                                                                ++inst__pt151701;
                                                                
                                                                goto *jmp152309;
                                                            }
                                                        }
                                                    } else {
                                                         {
                                                            SCM x152295;
                                                            if (SCM_CONSP(n1151778)) {
                                                                 {
                                                                    int i1152293;
                                                                    i1152293
                                                                      = scm_to_int(SCM_CAR(n1151778));
                                                                    
                                                                    
                                                                    
                                                                    x152295
                                                                      = AREF(fp,
                                                                             -(nstack151697
                                                                                 + i1152293));
                                                                }
                                                            } else {
                                                                 {
                                                                    int i1152294;
                                                                    i1152294
                                                                      = scm_to_int(n1151778);
                                                                    
                                                                    
                                                                    
                                                                    if (0) {
                                                                        if (scm_is_true(pinned_p151680)) {
                                                                             {
                                                                                
                                                                                variables__scm151688
                                                                                  = gp_copy_vector(&(variables151689),
                                                                                                   nvar151698);
                                                                                
                                                                                session151686
                                                                                  = scm_cons(variables__scm151688,
                                                                                             cnst151687);
                                                                                
                                                                                middle151685
                                                                                  = scm_cons(SCM_EOL,
                                                                                             session151686);
                                                                                
                                                                                pinned_p151680
                                                                                  = SCM_BOOL_F;
                                                                            }
                                                                        }
                                                                    }
                                                                    
                                                                    x152295
                                                                      = AREF(variables151689,
                                                                             i1152294);
                                                                }
                                                            }
                                                             {
                                                                SCM y152298;
                                                                if (SCM_CONSP(n2151780)) {
                                                                     {
                                                                        int i2152296;
                                                                        i2152296
                                                                          = scm_to_int(SCM_CAR(n2151780));
                                                                        
                                                                        
                                                                        
                                                                        y152298
                                                                          = AREF(fp,
                                                                                 -(nstack151697
                                                                                     + i2152296));
                                                                    }
                                                                } else {
                                                                     {
                                                                        int i2152297;
                                                                        i2152297
                                                                          = scm_to_int(n2151780);
                                                                        
                                                                        
                                                                        
                                                                        if (0) {
                                                                            if (scm_is_true(pinned_p151680)) {
                                                                                 {
                                                                                    
                                                                                    variables__scm151688
                                                                                      = gp_copy_vector(&(variables151689),
                                                                                                       nvar151698);
                                                                                    
                                                                                    session151686
                                                                                      = scm_cons(variables__scm151688,
                                                                                                 cnst151687);
                                                                                    
                                                                                    middle151685
                                                                                      = scm_cons(SCM_EOL,
                                                                                                 session151686);
                                                                                    
                                                                                    pinned_p151680
                                                                                      = SCM_BOOL_F;
                                                                                }
                                                                            }
                                                                        }
                                                                        
                                                                        y152298
                                                                          = AREF(variables151689,
                                                                                 i2152297);
                                                                    }
                                                                }
                                                                 {
                                                                    SCM ss152299;
                                                                    if (m151777
                                                                          == SCM_BOOL_F) {
                                                                        ss152299
                                                                          = gp_m_unify(x152295,
                                                                                       y152298,
                                                                                       s151704);
                                                                    } else {
                                                                        if (k1151779
                                                                              == SCM_BOOL_T
                                                                              || k2151781
                                                                                   == SCM_BOOL_T) {
                                                                            ss152299
                                                                              = gp_gp_unify_raw(x152295,
                                                                                                y152298,
                                                                                                s151704);
                                                                        } else {
                                                                            ss152299
                                                                              = gp_gp_unify(x152295,
                                                                                            y152298,
                                                                                            s151704);
                                                                        }
                                                                    }
                                                                     {
                                                                        
                                                                        
                                                                        
                                                                        if (scm_is_true(ss152299)) {
                                                                             {
                                                                                
                                                                                s151704
                                                                                  = ss152299;
                                                                                
                                                                                 {
                                                                                    void * jmp152312;
                                                                                    jmp152312
                                                                                      = AREF(_soperations_s,
                                                                                             scm_to_int(*(inst__pt151701)));
                                                                                    
                                                                                    
                                                                                    
                                                                                    ++inst__pt151701;
                                                                                    
                                                                                    goto *jmp152312;
                                                                                }
                                                                            }
                                                                        } else {
                                                                            if (SCM_I_INUMP(p151705)) {
                                                                                 {
                                                                                    
                                                                                    inst__pt151701
                                                                                      = instructions151693
                                                                                          + scm_to_int(p151705);
                                                                                    
                                                                                     {
                                                                                        void * jmp152313;
                                                                                        jmp152313
                                                                                          = AREF(_soperations_s,
                                                                                                 scm_to_int(*(inst__pt151701)));
                                                                                        
                                                                                        
                                                                                        
                                                                                        ++inst__pt151701;
                                                                                        
                                                                                        goto *jmp152313;
                                                                                    }
                                                                                }
                                                                            } else {
                                                                                 {
                                                                                    
                                                                                    AREF(fp,
                                                                                         0)
                                                                                      = p151705;
                                                                                    
                                                                                    sp
                                                                                      = fp
                                                                                          + -1;
                                                                                    
                                                                                    goto ret;
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          unify__instruction__2:
                            
                            printf("%s : %d\n", "unify-instruction-2", 15);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM n151782;
                            n151782 = AREF(inst__pt151701, 2);
                             {
                                SCM m151783;
                                m151783 = AREF(inst__pt151701, 0);
                                 {
                                    SCM i151784;
                                    i151784 = AREF(inst__pt151701, 1);
                                     {
                                        SCM k151785;
                                        k151785 = AREF(inst__pt151701, 3);
                                         {
                                            
                                            
                                            
                                            inst__pt151701
                                              = inst__pt151701 + 4;
                                            
                                            if (k151785 == SCM_BOOL_F) {
                                                 {
                                                    
                                                    if (SCM_CONSP(n151782)) {
                                                         {
                                                            int ii152320;
                                                            ii152320
                                                              = scm_to_int(SCM_CAR(n151782));
                                                            
                                                            
                                                            
                                                            AREF(fp,
                                                                 -(nstack151697
                                                                     + ii152320))
                                                              = i151784;
                                                        }
                                                    } else {
                                                         {
                                                            int ii152321;
                                                            ii152321
                                                              = scm_to_int(n151782);
                                                            
                                                            
                                                            
                                                            if (1) {
                                                                if (scm_is_true(pinned_p151680)) {
                                                                     {
                                                                        
                                                                        variables__scm151688
                                                                          = gp_copy_vector(&(variables151689),
                                                                                           nvar151698);
                                                                        
                                                                        session151686
                                                                          = scm_cons(variables__scm151688,
                                                                                     cnst151687);
                                                                        
                                                                        middle151685
                                                                          = scm_cons(SCM_EOL,
                                                                                     session151686);
                                                                        
                                                                        pinned_p151680
                                                                          = SCM_BOOL_F;
                                                                    }
                                                                }
                                                            }
                                                            
                                                            AREF(variables151689,
                                                                 ii152321)
                                                              = i151784;
                                                        }
                                                    }
                                                    
                                                     {
                                                        void * jmp152322;
                                                        jmp152322
                                                          = AREF(_soperations_s,
                                                                 scm_to_int(*(inst__pt151701)));
                                                        
                                                        
                                                        
                                                        ++inst__pt151701;
                                                        
                                                        goto *jmp152322;
                                                    }
                                                }
                                            } else {
                                                 {
                                                    SCM x152325;
                                                    if (SCM_CONSP(n151782)) {
                                                         {
                                                            int ii152323;
                                                            ii152323
                                                              = scm_to_int(SCM_CAR(n151782));
                                                            
                                                            
                                                            
                                                            x152325
                                                              = AREF(fp,
                                                                     -(nstack151697
                                                                         + ii152323));
                                                        }
                                                    } else {
                                                         {
                                                            int ii152324;
                                                            ii152324
                                                              = scm_to_int(n151782);
                                                            
                                                            
                                                            
                                                            if (0) {
                                                                if (scm_is_true(pinned_p151680)) {
                                                                     {
                                                                        
                                                                        variables__scm151688
                                                                          = gp_copy_vector(&(variables151689),
                                                                                           nvar151698);
                                                                        
                                                                        session151686
                                                                          = scm_cons(variables__scm151688,
                                                                                     cnst151687);
                                                                        
                                                                        middle151685
                                                                          = scm_cons(SCM_EOL,
                                                                                     session151686);
                                                                        
                                                                        pinned_p151680
                                                                          = SCM_BOOL_F;
                                                                    }
                                                                }
                                                            }
                                                            
                                                            x152325
                                                              = AREF(variables151689,
                                                                     ii152324);
                                                        }
                                                    }
                                                     {
                                                        SCM y152326;
                                                        y152326 = i151784;
                                                         {
                                                            SCM ss152327;
                                                            if (m151783
                                                                  == SCM_BOOL_F) {
                                                                ss152327
                                                                  = gp_m_unify(x152325,
                                                                               y152326,
                                                                               s151704);
                                                            } else {
                                                                if (m151783
                                                                      == SCM_BOOL_T
                                                                      || k151785
                                                                           == SCM_BOOL_T) {
                                                                    ss152327
                                                                      = gp_gp_unify_raw(x152325,
                                                                                        y152326,
                                                                                        s151704);
                                                                } else {
                                                                    ss152327
                                                                      = gp_gp_unify(x152325,
                                                                                    y152326,
                                                                                    s151704);
                                                                }
                                                            }
                                                             {
                                                                
                                                                
                                                                
                                                                if (scm_is_true(ss152327)) {
                                                                     {
                                                                        
                                                                        s151704
                                                                          = ss152327;
                                                                        
                                                                         {
                                                                            void * jmp152331;
                                                                            jmp152331
                                                                              = AREF(_soperations_s,
                                                                                     scm_to_int(*(inst__pt151701)));
                                                                            
                                                                            
                                                                            
                                                                            ++inst__pt151701;
                                                                            
                                                                            goto *jmp152331;
                                                                        }
                                                                    }
                                                                } else {
                                                                    if (SCM_I_INUMP(p151705)) {
                                                                         {
                                                                            
                                                                            inst__pt151701
                                                                              = instructions151693
                                                                                  + scm_to_int(p151705);
                                                                            
                                                                             {
                                                                                void * jmp152332;
                                                                                jmp152332
                                                                                  = AREF(_soperations_s,
                                                                                         scm_to_int(*(inst__pt151701)));
                                                                                
                                                                                
                                                                                
                                                                                ++inst__pt151701;
                                                                                
                                                                                goto *jmp152332;
                                                                            }
                                                                        }
                                                                    } else {
                                                                         {
                                                                            
                                                                            AREF(fp,
                                                                                 0)
                                                                              = p151705;
                                                                            
                                                                            sp
                                                                              = fp
                                                                                  + -1;
                                                                            
                                                                            goto ret;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          unify__constant__2:
                            
                            printf("%s : %d\n", "unify-constant-2", 14);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM n151786;
                            n151786 = AREF(inst__pt151701, 2);
                             {
                                SCM m151787;
                                m151787 = AREF(inst__pt151701, 0);
                                 {
                                    SCM i151788;
                                    i151788 = AREF(inst__pt151701, 1);
                                     {
                                        SCM k151789;
                                        k151789 = AREF(inst__pt151701, 3);
                                         {
                                            
                                            
                                            
                                            inst__pt151701
                                              = inst__pt151701 + 4;
                                            
                                            if (k151789 == SCM_BOOL_F) {
                                                 {
                                                    
                                                    if (SCM_CONSP(n151786)) {
                                                         {
                                                            int ii152338;
                                                            ii152338
                                                              = scm_to_int(SCM_CAR(n151786));
                                                            
                                                            
                                                            
                                                            AREF(fp,
                                                                 -(nstack151697
                                                                     + ii152338))
                                                              = AREF(constants151691,
                                                                     scm_to_int(i151788));
                                                        }
                                                    } else {
                                                         {
                                                            int ii152339;
                                                            ii152339
                                                              = scm_to_int(n151786);
                                                            
                                                            
                                                            
                                                            if (1) {
                                                                if (scm_is_true(pinned_p151680)) {
                                                                     {
                                                                        
                                                                        variables__scm151688
                                                                          = gp_copy_vector(&(variables151689),
                                                                                           nvar151698);
                                                                        
                                                                        session151686
                                                                          = scm_cons(variables__scm151688,
                                                                                     cnst151687);
                                                                        
                                                                        middle151685
                                                                          = scm_cons(SCM_EOL,
                                                                                     session151686);
                                                                        
                                                                        pinned_p151680
                                                                          = SCM_BOOL_F;
                                                                    }
                                                                }
                                                            }
                                                            
                                                            AREF(variables151689,
                                                                 ii152339)
                                                              = AREF(constants151691,
                                                                     scm_to_int(i151788));
                                                        }
                                                    }
                                                    
                                                     {
                                                        void * jmp152340;
                                                        jmp152340
                                                          = AREF(_soperations_s,
                                                                 scm_to_int(*(inst__pt151701)));
                                                        
                                                        
                                                        
                                                        ++inst__pt151701;
                                                        
                                                        goto *jmp152340;
                                                    }
                                                }
                                            } else {
                                                 {
                                                    SCM x152343;
                                                    if (SCM_CONSP(n151786)) {
                                                         {
                                                            int ii152341;
                                                            ii152341
                                                              = scm_to_int(SCM_CAR(n151786));
                                                            
                                                            
                                                            
                                                            x152343
                                                              = AREF(fp,
                                                                     -(nstack151697
                                                                         + ii152341));
                                                        }
                                                    } else {
                                                         {
                                                            int ii152342;
                                                            ii152342
                                                              = scm_to_int(n151786);
                                                            
                                                            
                                                            
                                                            if (0) {
                                                                if (scm_is_true(pinned_p151680)) {
                                                                     {
                                                                        
                                                                        variables__scm151688
                                                                          = gp_copy_vector(&(variables151689),
                                                                                           nvar151698);
                                                                        
                                                                        session151686
                                                                          = scm_cons(variables__scm151688,
                                                                                     cnst151687);
                                                                        
                                                                        middle151685
                                                                          = scm_cons(SCM_EOL,
                                                                                     session151686);
                                                                        
                                                                        pinned_p151680
                                                                          = SCM_BOOL_F;
                                                                    }
                                                                }
                                                            }
                                                            
                                                            x152343
                                                              = AREF(variables151689,
                                                                     ii152342);
                                                        }
                                                    }
                                                     {
                                                        SCM y152344;
                                                        y152344
                                                          = AREF(constants151691,
                                                                 scm_to_int(i151788));
                                                         {
                                                            SCM ss152345;
                                                            if (m151787
                                                                  == SCM_BOOL_F) {
                                                                ss152345
                                                                  = gp_m_unify(x152343,
                                                                               y152344,
                                                                               s151704);
                                                            } else {
                                                                if (m151787
                                                                      == SCM_BOOL_T
                                                                      || k151789
                                                                           == SCM_BOOL_T) {
                                                                    ss152345
                                                                      = gp_gp_unify_raw(x152343,
                                                                                        y152344,
                                                                                        s151704);
                                                                } else {
                                                                    ss152345
                                                                      = gp_gp_unify(x152343,
                                                                                    y152344,
                                                                                    s151704);
                                                                }
                                                            }
                                                             {
                                                                
                                                                
                                                                
                                                                if (scm_is_true(ss152345)) {
                                                                     {
                                                                        
                                                                        s151704
                                                                          = ss152345;
                                                                        
                                                                         {
                                                                            void * jmp152349;
                                                                            jmp152349
                                                                              = AREF(_soperations_s,
                                                                                     scm_to_int(*(inst__pt151701)));
                                                                            
                                                                            
                                                                            
                                                                            ++inst__pt151701;
                                                                            
                                                                            goto *jmp152349;
                                                                        }
                                                                    }
                                                                } else {
                                                                    if (SCM_I_INUMP(p151705)) {
                                                                         {
                                                                            
                                                                            inst__pt151701
                                                                              = instructions151693
                                                                                  + scm_to_int(p151705);
                                                                            
                                                                             {
                                                                                void * jmp152350;
                                                                                jmp152350
                                                                                  = AREF(_soperations_s,
                                                                                         scm_to_int(*(inst__pt151701)));
                                                                                
                                                                                
                                                                                
                                                                                ++inst__pt151701;
                                                                                
                                                                                goto *jmp152350;
                                                                            }
                                                                        }
                                                                    } else {
                                                                         {
                                                                            
                                                                            AREF(fp,
                                                                                 0)
                                                                              = p151705;
                                                                            
                                                                            sp
                                                                              = fp
                                                                                  + -1;
                                                                            
                                                                            goto ret;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          unify__instruction:
                            
                            printf("%s : %d\n", "unify-instruction", 13);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM i151790;
                            i151790 = AREF(inst__pt151701, 0);
                             {
                                SCM m151791;
                                m151791 = AREF(inst__pt151701, 1);
                                 {
                                    
                                    
                                    
                                    inst__pt151701 = inst__pt151701 + 2;
                                    
                                     {
                                        SCM x152356;
                                        x152356 = AREF(sp, 1);
                                         {
                                            SCM y152357;
                                            y152357 = i151790;
                                             {
                                                SCM ss152358;
                                                if (m151791 == SCM_BOOL_F) {
                                                    ss152358
                                                      = gp_m_unify(x152356,
                                                                   y152357,
                                                                   s151704);
                                                } else {
                                                    ss152358
                                                      = gp_gp_unify_raw(x152356,
                                                                        y152357,
                                                                        s151704);
                                                }
                                                 {
                                                    
                                                    
                                                    
                                                     {
                                                        
                                                        AREF(sp, 1)
                                                          = SCM_BOOL_F;
                                                        
                                                        sp = sp + 1;
                                                    }
                                                    
                                                    if (scm_is_true(ss152358)) {
                                                         {
                                                            
                                                            s151704 = ss152358;
                                                            
                                                             {
                                                                void * jmp152361;
                                                                jmp152361
                                                                  = AREF(_soperations_s,
                                                                         scm_to_int(*(inst__pt151701)));
                                                                
                                                                
                                                                
                                                                ++inst__pt151701;
                                                                
                                                                goto *jmp152361;
                                                            }
                                                        }
                                                    } else {
                                                        if (SCM_I_INUMP(p151705)) {
                                                             {
                                                                
                                                                inst__pt151701
                                                                  = instructions151693
                                                                      + scm_to_int(p151705);
                                                                
                                                                 {
                                                                    void * jmp152362;
                                                                    jmp152362
                                                                      = AREF(_soperations_s,
                                                                             scm_to_int(*(inst__pt151701)));
                                                                    
                                                                    
                                                                    
                                                                    ++inst__pt151701;
                                                                    
                                                                    goto *jmp152362;
                                                                }
                                                            }
                                                        } else {
                                                             {
                                                                
                                                                AREF(fp, 0)
                                                                  = p151705;
                                                                
                                                                sp = fp + -1;
                                                                
                                                                goto ret;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          unify__constant:
                            
                            printf("%s : %d\n", "unify-constant", 12);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM i151792;
                            i151792 = AREF(inst__pt151701, 0);
                             {
                                SCM m151793;
                                m151793 = AREF(inst__pt151701, 1);
                                 {
                                    
                                    
                                    
                                    inst__pt151701 = inst__pt151701 + 2;
                                    
                                     {
                                        SCM x152369;
                                        x152369 = AREF(sp, 1);
                                         {
                                            SCM y152370;
                                            y152370
                                              = AREF(constants151691,
                                                     scm_to_int(i151792));
                                             {
                                                SCM ss152371;
                                                if (m151793 == SCM_BOOL_F) {
                                                    ss152371
                                                      = gp_m_unify(x152369,
                                                                   y152370,
                                                                   s151704);
                                                } else {
                                                    ss152371
                                                      = gp_gp_unify_raw(x152369,
                                                                        y152370,
                                                                        s151704);
                                                }
                                                 {
                                                    
                                                    
                                                    
                                                     {
                                                        
                                                        AREF(sp, 1)
                                                          = SCM_BOOL_F;
                                                        
                                                        sp = sp + 1;
                                                    }
                                                    
                                                    if (scm_is_true(ss152371)) {
                                                         {
                                                            
                                                            s151704 = ss152371;
                                                            
                                                             {
                                                                void * jmp152374;
                                                                jmp152374
                                                                  = AREF(_soperations_s,
                                                                         scm_to_int(*(inst__pt151701)));
                                                                
                                                                
                                                                
                                                                ++inst__pt151701;
                                                                
                                                                goto *jmp152374;
                                                            }
                                                        }
                                                    } else {
                                                        if (SCM_I_INUMP(p151705)) {
                                                             {
                                                                
                                                                inst__pt151701
                                                                  = instructions151693
                                                                      + scm_to_int(p151705);
                                                                
                                                                 {
                                                                    void * jmp152375;
                                                                    jmp152375
                                                                      = AREF(_soperations_s,
                                                                             scm_to_int(*(inst__pt151701)));
                                                                    
                                                                    
                                                                    
                                                                    ++inst__pt151701;
                                                                    
                                                                    goto *jmp152375;
                                                                }
                                                            }
                                                        } else {
                                                             {
                                                                
                                                                AREF(fp, 0)
                                                                  = p151705;
                                                                
                                                                sp = fp + -1;
                                                                
                                                                goto ret;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          equal__instruction:
                            
                            printf("%s : %d\n", "equal-instruction", 64);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM i151794;
                            i151794 = AREF(inst__pt151701, 0);
                             {
                                SCM x151795;
                                x151795 = i151794;
                                 {
                                    SCM y151796;
                                    y151796 = AREF(sp, 1);
                                     {
                                        
                                        
                                        
                                         {
                                            
                                            AREF(sp, 1) = SCM_BOOL_F;
                                            
                                            sp = sp + 1;
                                        }
                                        
                                        if (scm_is_true(scm_equal_p(x151795,
                                                                    y151796))) {
                                             {
                                                void * jmp152382;
                                                jmp152382
                                                  = AREF(_soperations_s,
                                                         scm_to_int(*(inst__pt151701)));
                                                
                                                
                                                
                                                ++inst__pt151701;
                                                
                                                goto *jmp152382;
                                            }
                                        } else {
                                            if (SCM_I_INUMP(p151705)) {
                                                 {
                                                    
                                                    inst__pt151701
                                                      = instructions151693
                                                          + scm_to_int(p151705);
                                                    
                                                     {
                                                        void * jmp152383;
                                                        jmp152383
                                                          = AREF(_soperations_s,
                                                                 scm_to_int(*(inst__pt151701)));
                                                        
                                                        
                                                        
                                                        ++inst__pt151701;
                                                        
                                                        goto *jmp152383;
                                                    }
                                                }
                                            } else {
                                                 {
                                                    
                                                    AREF(fp, 0) = p151705;
                                                    
                                                    sp = fp + -1;
                                                    
                                                    goto ret;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          equal__constant:
                            
                            printf("%s : %d\n", "equal-constant", 63);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM i151797;
                            i151797 = AREF(inst__pt151701, 0);
                             {
                                SCM x151798;
                                x151798
                                  = AREF(constants151691, scm_to_int(i151797));
                                 {
                                    SCM y151799;
                                    y151799 = AREF(sp, 1);
                                     {
                                        
                                        
                                        
                                         {
                                            
                                            AREF(sp, 1) = SCM_BOOL_F;
                                            
                                            sp = sp + 1;
                                        }
                                        
                                        if (scm_is_true(scm_equal_p(x151798,
                                                                    y151799))) {
                                             {
                                                void * jmp152390;
                                                jmp152390
                                                  = AREF(_soperations_s,
                                                         scm_to_int(*(inst__pt151701)));
                                                
                                                
                                                
                                                ++inst__pt151701;
                                                
                                                goto *jmp152390;
                                            }
                                        } else {
                                            if (SCM_I_INUMP(p151705)) {
                                                 {
                                                    
                                                    inst__pt151701
                                                      = instructions151693
                                                          + scm_to_int(p151705);
                                                    
                                                     {
                                                        void * jmp152391;
                                                        jmp152391
                                                          = AREF(_soperations_s,
                                                                 scm_to_int(*(inst__pt151701)));
                                                        
                                                        
                                                        
                                                        ++inst__pt151701;
                                                        
                                                        goto *jmp152391;
                                                    }
                                                }
                                            } else {
                                                 {
                                                    
                                                    AREF(fp, 0) = p151705;
                                                    
                                                    sp = fp + -1;
                                                    
                                                    goto ret;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        
                        
                      icons_e:
                        
                        printf("%s : %d\n", "icons!", 23);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM x151800;
                        x151800 = AREF(sp, 1);
                         {
                            SCM ss151801;
                            ss151801 = gp_pair_bang(x151800, s151704);
                             {
                                
                                
                                
                                if (scm_is_true(ss151801)) {
                                     {
                                        
                                        sp = sp - 1;
                                        
                                        s151704 = ss151801;
                                        
                                        AREF(sp, 2)
                                          = gp_gp_cdr(x151800, s151704);
                                        
                                        AREF(sp, 1) = gp_car(x151800, s151704);
                                        
                                         {
                                            void * jmp152398;
                                            jmp152398
                                              = AREF(_soperations_s,
                                                     scm_to_int(*(inst__pt151701)));
                                            
                                            
                                            
                                            ++inst__pt151701;
                                            
                                            goto *jmp152398;
                                        }
                                    }
                                } else {
                                    if (SCM_I_INUMP(p151705)) {
                                         {
                                            
                                            inst__pt151701
                                              = instructions151693
                                                  + scm_to_int(p151705);
                                            
                                             {
                                                void * jmp152399;
                                                jmp152399
                                                  = AREF(_soperations_s,
                                                         scm_to_int(*(inst__pt151701)));
                                                
                                                
                                                
                                                ++inst__pt151701;
                                                
                                                goto *jmp152399;
                                            }
                                        }
                                    } else {
                                         {
                                            
                                            AREF(fp, 0) = p151705;
                                            
                                            sp = fp + -1;
                                            
                                            goto ret;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        
                        
                      ifkn_e:
                        
                        printf("%s : %d\n", "ifkn!", 22);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM x151802;
                        x151802 = AREF(sp, 1);
                         {
                            SCM ss151803;
                            ss151803 = gp_c_vector_x(x151802, 1, s151704);
                             {
                                
                                
                                
                                if (ss151803) {
                                     {
                                        SCM xa152406;
                                        xa152406
                                          = gp_gp_lookup(x151802, ss151803);
                                         {
                                            SCM xb152407;
                                            xb152407
                                              = scm_c_vector_ref(xa152406, 0);
                                             {
                                                
                                                
                                                
                                                s151704 = ss151803;
                                                
                                                AREF(sp, 1) = xb152407;
                                                
                                                 {
                                                    void * jmp152409;
                                                    jmp152409
                                                      = AREF(_soperations_s,
                                                             scm_to_int(*(inst__pt151701)));
                                                    
                                                    
                                                    
                                                    ++inst__pt151701;
                                                    
                                                    goto *jmp152409;
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    if (SCM_I_INUMP(p151705)) {
                                         {
                                            
                                            inst__pt151701
                                              = instructions151693
                                                  + scm_to_int(p151705);
                                            
                                             {
                                                void * jmp152408;
                                                jmp152408
                                                  = AREF(_soperations_s,
                                                         scm_to_int(*(inst__pt151701)));
                                                
                                                
                                                
                                                ++inst__pt151701;
                                                
                                                goto *jmp152408;
                                            }
                                        }
                                    } else {
                                         {
                                            
                                            AREF(fp, 0) = p151705;
                                            
                                            sp = fp + -1;
                                            
                                            goto ret;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        
                        
                      icurly_e:
                        
                        printf("%s : %d\n", "icurly!", 21);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM x151804;
                        x151804 = AREF(sp, 1);
                         {
                            SCM q151805;
                            q151805 = gp_c_vector_x(x151804, 2, s151704);
                             {
                                
                                
                                
                                if (q151805) {
                                     {
                                        SCM xa152414;
                                        xa152414
                                          = gp_gp_lookup(x151804, q151805);
                                         {
                                            SCM q152415;
                                            q152415
                                              = gp_gp_unify(brace68201,
                                                            scm_c_vector_ref(xa152414,
                                                                             0),
                                                            q151805);
                                             {
                                                
                                                
                                                
                                                if (q152415) {
                                                     {
                                                        
                                                        s151704 = q152415;
                                                        
                                                        AREF(sp, 1)
                                                          = scm_c_vector_ref(xa152414,
                                                                             1);
                                                        
                                                         {
                                                            void * jmp152417;
                                                            jmp152417
                                                              = AREF(_soperations_s,
                                                                     scm_to_int(*(inst__pt151701)));
                                                            
                                                            
                                                            
                                                            ++inst__pt151701;
                                                            
                                                            goto *jmp152417;
                                                        }
                                                    }
                                                } else {
                                                    if (SCM_I_INUMP(p151705)) {
                                                         {
                                                            
                                                            inst__pt151701
                                                              = instructions151693
                                                                  + scm_to_int(p151705);
                                                            
                                                             {
                                                                void * jmp152418;
                                                                jmp152418
                                                                  = AREF(_soperations_s,
                                                                         scm_to_int(*(inst__pt151701)));
                                                                
                                                                
                                                                
                                                                ++inst__pt151701;
                                                                
                                                                goto *jmp152418;
                                                            }
                                                        }
                                                    } else {
                                                         {
                                                            
                                                            AREF(fp, 0)
                                                              = p151705;
                                                            
                                                            sp = fp + -1;
                                                            
                                                            goto ret;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    if (SCM_I_INUMP(p151705)) {
                                         {
                                            
                                            inst__pt151701
                                              = instructions151693
                                                  + scm_to_int(p151705);
                                            
                                             {
                                                void * jmp152416;
                                                jmp152416
                                                  = AREF(_soperations_s,
                                                         scm_to_int(*(inst__pt151701)));
                                                
                                                
                                                
                                                ++inst__pt151701;
                                                
                                                goto *jmp152416;
                                            }
                                        }
                                    } else {
                                         {
                                            
                                            AREF(fp, 0) = p151705;
                                            
                                            sp = fp + -1;
                                            
                                            goto ret;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        
                        
                      push__instruction:
                        
                        printf("%s : %d\n", "push-instruction", 40);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM x151806;
                        x151806 = *inst__pt151701;
                        
                        
                        
                        ++inst__pt151701;
                        
                        *sp = x151806;
                        
                        sp = sp - 1;
                    }
                    
                     {
                        void * jmp151807;
                        jmp151807
                          = AREF(_soperations_s, scm_to_int(*(inst__pt151701)));
                        
                        
                        
                        ++inst__pt151701;
                        
                        goto *jmp151807;
                    }
                    
                     {
                        
                        
                      pushv:
                        
                        printf("%s : %d\n", "pushv", 44);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM x151808;
                        x151808 = *inst__pt151701;
                        
                        
                        
                        ++inst__pt151701;
                        
                        if (scm_is_false(x151808)) {
                            *sp = gp_mkvar(s151704);
                        } else {
                            if (SCM_CONSP(x151808)) {
                                 {
                                    int i152429;
                                    i152429 = scm_to_int(SCM_CAR(x151808));
                                    
                                    
                                    
                                    *sp = AREF(fp, -(nstack151697 + i152429));
                                }
                            } else {
                                 {
                                    int i152430;
                                    i152430 = scm_to_int(x151808);
                                    
                                    
                                    
                                    if (0) {
                                        if (scm_is_true(pinned_p151680)) {
                                             {
                                                
                                                variables__scm151688
                                                  = gp_copy_vector(&(variables151689),
                                                                   nvar151698);
                                                
                                                session151686
                                                  = scm_cons(variables__scm151688,
                                                             cnst151687);
                                                
                                                middle151685
                                                  = scm_cons(SCM_EOL,
                                                             session151686);
                                                
                                                pinned_p151680 = SCM_BOOL_F;
                                            }
                                        }
                                    }
                                    
                                    *sp = AREF(variables151689, i152430);
                                }
                            }
                        }
                        
                        sp = sp - 1;
                        
                         {
                            void * jmp152431;
                            jmp152431
                              = AREF(_soperations_s,
                                     scm_to_int(*(inst__pt151701)));
                            
                            
                            
                            ++inst__pt151701;
                            
                            goto *jmp152431;
                        }
                    }
                    
                     {
                        
                        
                      push__constant:
                        
                        printf("%s : %d\n", "push-constant", 39);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        int n151809;
                        n151809 = scm_to_int(*(inst__pt151701));
                        
                        
                        
                        ++inst__pt151701;
                        
                        *sp = AREF(constants151691, n151809);
                        
                        sp = sp - 1;
                    }
                    
                     {
                        void * jmp151810;
                        jmp151810
                          = AREF(_soperations_s, scm_to_int(*(inst__pt151701)));
                        
                        
                        
                        ++inst__pt151701;
                        
                        goto *jmp151810;
                    }
                    
                     {
                        
                        
                      push__variable:
                        
                        printf("%s : %d\n", "push-variable", 41);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM x151811;
                        x151811 = *inst__pt151701;
                         {
                            int new_p151812;
                            new_p151812 = scm_to_int(AREF(inst__pt151701, 1));
                             {
                                SCM v151816;
                                if (new_p151812) {
                                     {
                                        SCM v151813;
                                        v151813 = gp_mkvar(s151704);
                                        
                                        
                                        
                                        if (SCM_CONSP(x151811)) {
                                             {
                                                int i152437;
                                                i152437
                                                  = scm_to_int(SCM_CAR(x151811));
                                                
                                                
                                                
                                                AREF(fp,
                                                     -(nstack151697 + i152437))
                                                  = v151813;
                                            }
                                        } else {
                                             {
                                                int i152438;
                                                i152438 = scm_to_int(x151811);
                                                
                                                
                                                
                                                if (1) {
                                                    if (scm_is_true(pinned_p151680)) {
                                                         {
                                                            
                                                            variables__scm151688
                                                              = gp_copy_vector(&(variables151689),
                                                                               nvar151698);
                                                            
                                                            session151686
                                                              = scm_cons(variables__scm151688,
                                                                         cnst151687);
                                                            
                                                            middle151685
                                                              = scm_cons(SCM_EOL,
                                                                         session151686);
                                                            
                                                            pinned_p151680
                                                              = SCM_BOOL_F;
                                                        }
                                                    }
                                                }
                                                
                                                AREF(variables151689, i152438)
                                                  = v151813;
                                            }
                                        }
                                        
                                        v151816 = v151813;
                                    }
                                } else {
                                    if (SCM_CONSP(x151811)) {
                                         {
                                            int i151814;
                                            i151814
                                              = scm_to_int(SCM_CAR(x151811));
                                            
                                            
                                            
                                            v151816
                                              = AREF(fp,
                                                     -(nstack151697 + i151814));
                                        }
                                    } else {
                                         {
                                            int i151815;
                                            i151815 = scm_to_int(x151811);
                                            
                                            
                                            
                                            if (0) {
                                                if (scm_is_true(pinned_p151680)) {
                                                     {
                                                        
                                                        variables__scm151688
                                                          = gp_copy_vector(&(variables151689),
                                                                           nvar151698);
                                                        
                                                        session151686
                                                          = scm_cons(variables__scm151688,
                                                                     cnst151687);
                                                        
                                                        middle151685
                                                          = scm_cons(SCM_EOL,
                                                                     session151686);
                                                        
                                                        pinned_p151680
                                                          = SCM_BOOL_F;
                                                    }
                                                }
                                            }
                                            
                                            v151816
                                              = AREF(variables151689, i151815);
                                        }
                                    }
                                }
                                 {
                                    
                                    
                                    
                                    inst__pt151701 = inst__pt151701 + 2;
                                    
                                    *sp = v151816;
                                    
                                    sp = sp - 1;
                                }
                            }
                        }
                    }
                    
                     {
                        void * jmp151817;
                        jmp151817
                          = AREF(_soperations_s, scm_to_int(*(inst__pt151701)));
                        
                        
                        
                        ++inst__pt151701;
                        
                        goto *jmp151817;
                    }
                    
                     {
                        
                        
                      push__variable__scm:
                        
                        printf("%s : %d\n", "push-variable-scm", 42);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM x151818;
                        x151818 = *inst__pt151701;
                         {
                            SCM v151821;
                             {
                                SCM a152445;
                                SCM a152446;
                                if (SCM_CONSP(x151818)) {
                                     {
                                        int i151819;
                                        i151819 = scm_to_int(SCM_CAR(x151818));
                                        
                                        
                                        
                                        a152445
                                          = AREF(fp, -(nstack151697 + i151819));
                                    }
                                } else {
                                     {
                                        int i151820;
                                        i151820 = scm_to_int(x151818);
                                        
                                        
                                        
                                        if (0) {
                                            if (scm_is_true(pinned_p151680)) {
                                                 {
                                                    
                                                    variables__scm151688
                                                      = gp_copy_vector(&(variables151689),
                                                                       nvar151698);
                                                    
                                                    session151686
                                                      = scm_cons(variables__scm151688,
                                                                 cnst151687);
                                                    
                                                    middle151685
                                                      = scm_cons(SCM_EOL,
                                                                 session151686);
                                                    
                                                    pinned_p151680
                                                      = SCM_BOOL_F;
                                                }
                                            }
                                        }
                                        
                                        a152445
                                          = AREF(variables151689, i151820);
                                    }
                                }
                                a152446 = s151704;
                                v151821 = gp_gp_lookup(a152445, a152446);
                            }
                             {
                                
                                
                                
                                scm_simple_format(SCM_BOOL_T,
                                                  scm_string73598,
                                                  scm_list_2(x151818, v151821));
                                
                                inst__pt151701 = inst__pt151701 + 1;
                                
                                *sp = v151821;
                                
                                sp = sp - 1;
                            }
                        }
                    }
                    
                     {
                        void * jmp151822;
                        jmp151822
                          = AREF(_soperations_s, scm_to_int(*(inst__pt151701)));
                        
                        
                        
                        ++inst__pt151701;
                        
                        goto *jmp151822;
                    }
                    
                     {
                        
                        
                      pop__variable:
                        
                        printf("%s : %d\n", "pop-variable", 43);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        int n151823;
                        n151823 = scm_to_int(*(inst__pt151701));
                        
                        
                        
                        ++inst__pt151701;
                        
                        AREF(variables151689, n151823) = AREF(sp, 1);
                        
                        sp = sp + 1;
                    }
                    
                     {
                        void * jmp151824;
                        jmp151824
                          = AREF(_soperations_s, scm_to_int(*(inst__pt151701)));
                        
                        
                        
                        ++inst__pt151701;
                        
                        goto *jmp151824;
                    }
                    
                     {
                        
                        
                      pop:
                        
                        printf("%s : %d\n", "pop", 49);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        int n151825;
                        n151825 = scm_to_int(*(inst__pt151701));
                        
                        
                        
                         {
                            int m152454;
                            m152454 = n151825;
                             {
                                
                              lp152456:
                                 {
                                    
                                    if (!(m152454 == 0)) {
                                         {
                                            
                                            sp = sp + 1;
                                            
                                            AREF(sp, 0) = SCM_BOOL_F;
                                            
                                             {
                                                int next152461;
                                                next152461 = m152454 - 1;
                                                m152454 = next152461;
                                                goto lp152456;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        
                        ++inst__pt151701;
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            void * jmp152455;
                            jmp152455
                              = AREF(_soperations_s,
                                     scm_to_int(*(inst__pt151701)));
                            
                            
                            
                            ++inst__pt151701;
                            
                            goto *jmp152455;
                        }
                    }
                    
                     {
                        
                        
                      seek:
                        
                        printf("%s : %d\n", "seek", 48);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        int n151826;
                        n151826 = scm_to_int(*(inst__pt151701));
                        
                        
                        
                        ++inst__pt151701;
                        
                        sp = sp - n151826;
                        
                         {
                            void * jmp152464;
                            jmp152464
                              = AREF(_soperations_s,
                                     scm_to_int(*(inst__pt151701)));
                            
                            
                            
                            ++inst__pt151701;
                            
                            goto *jmp152464;
                        }
                    }
                    
                     {
                        
                        
                      dup:
                        
                        printf("%s : %d\n", "dup", 50);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                    AREF(sp, 0) = AREF(sp, 1);
                    
                    sp = sp - 1;
                    
                     {
                        void * jmp151827;
                        jmp151827
                          = AREF(_soperations_s, scm_to_int(*(inst__pt151701)));
                        
                        
                        
                        ++inst__pt151701;
                        
                        goto *jmp151827;
                    }
                    
                     {
                        
                        
                      mk__cons:
                        
                        printf("%s : %d\n", "mk-cons", 45);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM c151828;
                        c151828
                          = gp_cons_bang(AREF(sp, 2), AREF(sp, 1), s151704);
                        
                        
                        
                        AREF(sp, 2) = c151828;
                        
                         {
                            
                            AREF(sp, 1) = SCM_BOOL_F;
                            
                            sp = sp + 1;
                        }
                    }
                    
                     {
                        void * jmp151829;
                        jmp151829
                          = AREF(_soperations_s, scm_to_int(*(inst__pt151701)));
                        
                        
                        
                        ++inst__pt151701;
                        
                        goto *jmp151829;
                    }
                    
                     {
                        
                        
                      mk__fkn:
                        
                        printf("%s : %d\n", "mk-fkn", 46);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        int n151830;
                        n151830 = scm_to_int(*(inst__pt151701));
                         {
                            
                            
                            
                            ++inst__pt151701;
                            
                             {
                                int i152471;
                                i152471 = n151830;
                                 {
                                    
                                  lp152472:
                                     {
                                        
                                        if (i152471 <= 0) {
                                             {
                                                SCM v152473;
                                                v152473
                                                  = scm_c_make_vector(1,
                                                                      SCM_BOOL_F);
                                                
                                                
                                                
                                                scm_c_vector_set_x(v152473,
                                                                   0,
                                                                   AREF(sp, 1));
                                                
                                                AREF(sp, 1) = v152473;
                                            }
                                        } else {
                                             {
                                                
                                                 {
                                                    SCM c152474;
                                                    c152474
                                                      = gp_cons_bang(AREF(sp,
                                                                          2),
                                                                     AREF(sp,
                                                                          1),
                                                                     s151704);
                                                    
                                                    
                                                    
                                                    AREF(sp, 2) = c152474;
                                                    
                                                     {
                                                        
                                                        AREF(sp, 1)
                                                          = SCM_BOOL_F;
                                                        
                                                        sp = sp + 1;
                                                    }
                                                }
                                                
                                                 {
                                                    int next152477;
                                                    next152477 = i152471 - 1;
                                                    i152471 = next152477;
                                                    goto lp152472;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        void * jmp151831;
                        jmp151831
                          = AREF(_soperations_s, scm_to_int(*(inst__pt151701)));
                        
                        
                        
                        ++inst__pt151701;
                        
                        goto *jmp151831;
                    }
                    
                     {
                        
                        
                      mk__curly:
                        
                        printf("%s : %d\n", "mk-curly", 47);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM v151832;
                        v151832 = scm_c_make_vector(2, SCM_BOOL_F);
                        
                        
                        
                        scm_c_vector_set_x(v151832, 0, brace68201);
                        
                        scm_c_vector_set_x(v151832, 1, AREF(sp, 1));
                        
                        AREF(sp, 1) = v151832;
                    }
                    
                     {
                        void * jmp151833;
                        jmp151833
                          = AREF(_soperations_s, scm_to_int(*(inst__pt151701)));
                        
                        
                        
                        ++inst__pt151701;
                        
                        goto *jmp151833;
                    }
                    
                     {
                        
                        
                      icons:
                        
                        printf("%s : %d\n", "icons", 18);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM x151834;
                        x151834 = AREF(sp, 1);
                         {
                            SCM ss151835;
                            ss151835 = gp_pair(x151834, s151704);
                             {
                                
                                
                                
                                if (scm_is_true(ss151835)) {
                                     {
                                        
                                        sp = sp - 1;
                                        
                                        AREF(sp, 2)
                                          = gp_gp_cdr(x151834, s151704);
                                        
                                        AREF(sp, 1) = gp_car(x151834, s151704);
                                        
                                         {
                                            void * jmp152480;
                                            jmp152480
                                              = AREF(_soperations_s,
                                                     scm_to_int(*(inst__pt151701)));
                                            
                                            
                                            
                                            ++inst__pt151701;
                                            
                                            goto *jmp152480;
                                        }
                                    }
                                } else {
                                    if (SCM_I_INUMP(p151705)) {
                                         {
                                            
                                            inst__pt151701
                                              = instructions151693
                                                  + scm_to_int(p151705);
                                            
                                             {
                                                void * jmp152481;
                                                jmp152481
                                                  = AREF(_soperations_s,
                                                         scm_to_int(*(inst__pt151701)));
                                                
                                                
                                                
                                                ++inst__pt151701;
                                                
                                                goto *jmp152481;
                                            }
                                        }
                                    } else {
                                         {
                                            
                                            AREF(fp, 0) = p151705;
                                            
                                            sp = fp + -1;
                                            
                                            goto ret;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        
                        
                      ifkn:
                        
                        printf("%s : %d\n", "ifkn", 17);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM x151836;
                        x151836 = gp_gp_lookup(AREF(sp, 1), s151704);
                         {
                            SCM ss151837;
                            ss151837 = gp_c_vector(x151836, 1, s151704);
                             {
                                
                                
                                
                                if (ss151837) {
                                     {
                                        SCM xa152488;
                                        xa152488
                                          = scm_c_vector_ref(x151836, 0);
                                        
                                        
                                        
                                        AREF(sp, 1) = xa152488;
                                        
                                         {
                                            void * jmp152490;
                                            jmp152490
                                              = AREF(_soperations_s,
                                                     scm_to_int(*(inst__pt151701)));
                                            
                                            
                                            
                                            ++inst__pt151701;
                                            
                                            goto *jmp152490;
                                        }
                                    }
                                } else {
                                    if (SCM_I_INUMP(p151705)) {
                                         {
                                            
                                            inst__pt151701
                                              = instructions151693
                                                  + scm_to_int(p151705);
                                            
                                             {
                                                void * jmp152489;
                                                jmp152489
                                                  = AREF(_soperations_s,
                                                         scm_to_int(*(inst__pt151701)));
                                                
                                                
                                                
                                                ++inst__pt151701;
                                                
                                                goto *jmp152489;
                                            }
                                        }
                                    } else {
                                         {
                                            
                                            AREF(fp, 0) = p151705;
                                            
                                            sp = fp + -1;
                                            
                                            goto ret;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        
                        
                      icurly:
                        
                        printf("%s : %d\n", "icurly", 16);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM x151838;
                        x151838 = gp_gp_lookup(AREF(sp, 1), s151704);
                         {
                            SCM ss151839;
                            ss151839 = gp_c_vector(x151838, 2, s151704);
                             {
                                
                                
                                
                                if (ss151839) {
                                    if (brace68201
                                          == gp_gp_lookup(scm_c_vector_ref(x151838,
                                                                           0),
                                                          s151704)) {
                                         {
                                            
                                            AREF(sp, 1)
                                              = scm_c_vector_ref(x151838, 1);
                                            
                                             {
                                                void * jmp152495;
                                                jmp152495
                                                  = AREF(_soperations_s,
                                                         scm_to_int(*(inst__pt151701)));
                                                
                                                
                                                
                                                ++inst__pt151701;
                                                
                                                goto *jmp152495;
                                            }
                                        }
                                    } else {
                                        if (SCM_I_INUMP(p151705)) {
                                             {
                                                
                                                inst__pt151701
                                                  = instructions151693
                                                      + scm_to_int(p151705);
                                                
                                                 {
                                                    void * jmp152496;
                                                    jmp152496
                                                      = AREF(_soperations_s,
                                                             scm_to_int(*(inst__pt151701)));
                                                    
                                                    
                                                    
                                                    ++inst__pt151701;
                                                    
                                                    goto *jmp152496;
                                                }
                                            }
                                        } else {
                                             {
                                                
                                                AREF(fp, 0) = p151705;
                                                
                                                sp = fp + -1;
                                                
                                                goto ret;
                                            }
                                        }
                                    }
                                } else {
                                    if (SCM_I_INUMP(p151705)) {
                                         {
                                            
                                            inst__pt151701
                                              = instructions151693
                                                  + scm_to_int(p151705);
                                            
                                             {
                                                void * jmp152497;
                                                jmp152497
                                                  = AREF(_soperations_s,
                                                         scm_to_int(*(inst__pt151701)));
                                                
                                                
                                                
                                                ++inst__pt151701;
                                                
                                                goto *jmp152497;
                                            }
                                        }
                                    } else {
                                         {
                                            
                                            AREF(fp, 0) = p151705;
                                            
                                            sp = fp + -1;
                                            
                                            goto ret;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          plus:
                            
                            printf("%s : %d\n", "plus", 66);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x151840;
                            SCM y151841;
                            x151840 = gp_gp_lookup(AREF(sp, 2), s151704);
                            y151841 = gp_gp_lookup(AREF(sp, 1), s151704);
                            
                            
                            
                             {
                                
                                AREF(sp, 1) = SCM_BOOL_F;
                                
                                sp = sp + 1;
                            }
                            
                            if (SCM_I_INUMP(x151840) && SCM_I_INUMP(y151841)) {
                                 {
                                    scm_t_int64 n152506;
                                    n152506
                                      = SCM_I_INUM(x151840)
                                          + SCM_I_INUM(y151841);
                                    
                                    
                                    
                                    if (SCM_FIXABLE(n152506)) {
                                        AREF(sp, 1) = SCM_I_MAKINUM(n152506);
                                    } else {
                                        AREF(sp, 1)
                                          = scm_sum(x151840, y151841);
                                    }
                                }
                            } else {
                                AREF(sp, 1) = scm_sum(x151840, y151841);
                            }
                            
                             {
                                void * jmp152507;
                                jmp152507
                                  = AREF(_soperations_s,
                                         scm_to_int(*(inst__pt151701)));
                                
                                
                                
                                ++inst__pt151701;
                                
                                goto *jmp152507;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          minus:
                            
                            printf("%s : %d\n", "minus", 68);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x151842;
                            SCM y151843;
                            x151842 = gp_gp_lookup(AREF(sp, 2), s151704);
                            y151843 = gp_gp_lookup(AREF(sp, 1), s151704);
                            
                            
                            
                             {
                                
                                AREF(sp, 1) = SCM_BOOL_F;
                                
                                sp = sp + 1;
                            }
                            
                            if (SCM_I_INUMP(x151842) && SCM_I_INUMP(y151843)) {
                                 {
                                    scm_t_int64 n152512;
                                    n152512
                                      = SCM_I_INUM(x151842)
                                          - SCM_I_INUM(y151843);
                                    
                                    
                                    
                                    if (SCM_FIXABLE(n152512)) {
                                        AREF(sp, 1) = SCM_I_MAKINUM(n152512);
                                    } else {
                                        AREF(sp, 1)
                                          = scm_difference(x151842, y151843);
                                    }
                                }
                            } else {
                                AREF(sp, 1) = scm_difference(x151842, y151843);
                            }
                            
                             {
                                void * jmp152513;
                                jmp152513
                                  = AREF(_soperations_s,
                                         scm_to_int(*(inst__pt151701)));
                                
                                
                                
                                ++inst__pt151701;
                                
                                goto *jmp152513;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          band:
                            
                            printf("%s : %d\n", "band", 92);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x151844;
                            SCM y151845;
                            x151844 = gp_gp_lookup(AREF(sp, 2), s151704);
                            y151845 = gp_gp_lookup(AREF(sp, 1), s151704);
                            
                            
                            
                             {
                                
                                AREF(sp, 1) = SCM_BOOL_F;
                                
                                sp = sp + 1;
                            }
                            
                            if (SCM_I_INUMP(x151844) && SCM_I_INUMP(y151845)) {
                                 {
                                    scm_t_int64 n152518;
                                    n152518
                                      = SCM_I_INUM(x151844)
                                          & SCM_I_INUM(y151845);
                                    
                                    
                                    
                                    if (SCM_FIXABLE(n152518)) {
                                        AREF(sp, 1) = SCM_I_MAKINUM(n152518);
                                    } else {
                                        AREF(sp, 1)
                                          = scm_logand(x151844, y151845);
                                    }
                                }
                            } else {
                                AREF(sp, 1) = scm_logand(x151844, y151845);
                            }
                            
                             {
                                void * jmp152519;
                                jmp152519
                                  = AREF(_soperations_s,
                                         scm_to_int(*(inst__pt151701)));
                                
                                
                                
                                ++inst__pt151701;
                                
                                goto *jmp152519;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          bor:
                            
                            printf("%s : %d\n", "bor", 94);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x151846;
                            SCM y151847;
                            x151846 = gp_gp_lookup(AREF(sp, 2), s151704);
                            y151847 = gp_gp_lookup(AREF(sp, 1), s151704);
                            
                            
                            
                             {
                                
                                AREF(sp, 1) = SCM_BOOL_F;
                                
                                sp = sp + 1;
                            }
                            
                            if (SCM_I_INUMP(x151846) && SCM_I_INUMP(y151847)) {
                                 {
                                    scm_t_int64 n152524;
                                    n152524
                                      = SCM_I_INUM(x151846)
                                          | SCM_I_INUM(y151847);
                                    
                                    
                                    
                                    if (SCM_FIXABLE(n152524)) {
                                        AREF(sp, 1) = SCM_I_MAKINUM(n152524);
                                    } else {
                                        AREF(sp, 1)
                                          = scm_logior(x151846, y151847);
                                    }
                                }
                            } else {
                                AREF(sp, 1) = scm_logior(x151846, y151847);
                            }
                            
                             {
                                void * jmp152525;
                                jmp152525
                                  = AREF(_soperations_s,
                                         scm_to_int(*(inst__pt151701)));
                                
                                
                                
                                ++inst__pt151701;
                                
                                goto *jmp152525;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          xor:
                            
                            printf("%s : %d\n", "xor", 96);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x151848;
                            SCM y151849;
                            x151848 = gp_gp_lookup(AREF(sp, 2), s151704);
                            y151849 = gp_gp_lookup(AREF(sp, 1), s151704);
                            
                            
                            
                             {
                                
                                AREF(sp, 1) = SCM_BOOL_F;
                                
                                sp = sp + 1;
                            }
                            
                            if (SCM_I_INUMP(x151848) && SCM_I_INUMP(y151849)) {
                                 {
                                    scm_t_int64 n152530;
                                    n152530
                                      = SCM_I_INUM(x151848)
                                          ^ SCM_I_INUM(y151849);
                                    
                                    
                                    
                                    if (SCM_FIXABLE(n152530)) {
                                        AREF(sp, 1) = SCM_I_MAKINUM(n152530);
                                    } else {
                                        AREF(sp, 1)
                                          = scm_logxor(x151848, y151849);
                                    }
                                }
                            } else {
                                AREF(sp, 1) = scm_logxor(x151848, y151849);
                            }
                            
                             {
                                void * jmp152531;
                                jmp152531
                                  = AREF(_soperations_s,
                                         scm_to_int(*(inst__pt151701)));
                                
                                
                                
                                ++inst__pt151701;
                                
                                goto *jmp152531;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          modulo:
                            
                            printf("%s : %d\n", "modulo", 86);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x151850;
                            SCM y151851;
                            x151850 = gp_gp_lookup(AREF(sp, 2), s151704);
                            y151851 = gp_gp_lookup(AREF(sp, 1), s151704);
                            
                            
                            
                             {
                                
                                AREF(sp, 1) = SCM_BOOL_F;
                                
                                sp = sp + 1;
                            }
                            
                            if (SCM_I_INUMP(x151850) && SCM_I_INUMP(y151851)) {
                                 {
                                    scm_t_int64 n152536;
                                    n152536
                                      = SCM_I_INUM(x151850)
                                          % SCM_I_INUM(y151851);
                                    
                                    
                                    
                                    if (SCM_FIXABLE(n152536)) {
                                        AREF(sp, 1) = SCM_I_MAKINUM(n152536);
                                    } else {
                                        AREF(sp, 1)
                                          = scm_modulo(x151850, y151851);
                                    }
                                }
                            } else {
                                AREF(sp, 1) = scm_modulo(x151850, y151851);
                            }
                            
                             {
                                void * jmp152537;
                                jmp152537
                                  = AREF(_soperations_s,
                                         scm_to_int(*(inst__pt151701)));
                                
                                
                                
                                ++inst__pt151701;
                                
                                goto *jmp152537;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          mul:
                            
                            printf("%s : %d\n", "mul", 70);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x151852;
                            SCM y151853;
                            x151852 = gp_gp_lookup(AREF(sp, 2), s151704);
                            y151853 = gp_gp_lookup(AREF(sp, 1), s151704);
                            
                            
                            
                             {
                                
                                AREF(sp, 1) = SCM_BOOL_F;
                                
                                sp = sp + 1;
                            }
                            
                            if (SCM_I_INUMP(x151852) && SCM_I_INUMP(y151853)) {
                                 {
                                    scm_t_int64 xx152542;
                                    scm_t_int64 yy152543;
                                    xx152542 = SCM_I_INUM(x151852);
                                    yy152543 = SCM_I_INUM(y151853);
                                    
                                    
                                    
                                    if (xx152542 < 1073741824
                                          && xx152542 > -1073741824
                                               && yy152543 < 1073741824
                                                    && yy152543 > -1073741824) {
                                         {
                                            scm_t_int64 n152547;
                                            n152547 = xx152542 * yy152543;
                                            
                                            
                                            
                                            AREF(sp, 1)
                                              = SCM_I_MAKINUM(n152547);
                                        }
                                    } else {
                                        AREF(sp, 1)
                                          = scm_product(x151852, y151853);
                                    }
                                }
                            } else {
                                AREF(sp, 1) = scm_product(x151852, y151853);
                            }
                            
                             {
                                void * jmp152544;
                                jmp152544
                                  = AREF(_soperations_s,
                                         scm_to_int(*(inst__pt151701)));
                                
                                
                                
                                ++inst__pt151701;
                                
                                goto *jmp152544;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          shift_l:
                            
                            printf("%s : %d\n", "shift_l", 80);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x151854;
                            SCM y151855;
                            x151854 = gp_gp_lookup(AREF(sp, 2), s151704);
                            y151855 = gp_gp_lookup(AREF(sp, 1), s151704);
                            
                            
                            
                             {
                                
                                AREF(sp, 1) = SCM_BOOL_F;
                                
                                sp = sp + 1;
                            }
                            
                            if (SCM_I_INUMP(x151854) && SCM_I_INUMP(y151855)) {
                                 {
                                    scm_t_int64 xx152554;
                                    scm_t_int64 yy152555;
                                    xx152554 = SCM_I_INUM(x151854);
                                    yy152555 = SCM_I_INUM(y151855);
                                    
                                    
                                    
                                    if (xx152554 < 268435456
                                          && xx152554 > -268435456
                                               && yy152555 <= 32
                                                    && yy152555 >= 0) {
                                         {
                                            scm_t_int64 n152559;
                                            n152559 = xx152554 << yy152555;
                                            
                                            
                                            
                                            AREF(sp, 1)
                                              = SCM_I_MAKINUM(n152559);
                                        }
                                    } else {
                                        AREF(sp, 1)
                                          = scm_ash(x151854, y151855);
                                    }
                                }
                            } else {
                                AREF(sp, 1) = scm_ash(x151854, y151855);
                            }
                            
                             {
                                void * jmp152556;
                                jmp152556
                                  = AREF(_soperations_s,
                                         scm_to_int(*(inst__pt151701)));
                                
                                
                                
                                ++inst__pt151701;
                                
                                goto *jmp152556;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          shift_r:
                            
                            printf("%s : %d\n", "shift_r", 83);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x151856;
                            SCM y151857;
                            x151856 = gp_gp_lookup(AREF(sp, 2), s151704);
                            y151857 = gp_gp_lookup(AREF(sp, 1), s151704);
                            
                            
                            
                             {
                                
                                AREF(sp, 1) = SCM_BOOL_F;
                                
                                sp = sp + 1;
                            }
                            
                            if (SCM_I_INUMP(x151856) && SCM_I_INUMP(y151857)) {
                                 {
                                    scm_t_int64 xx152564;
                                    scm_t_int64 yy152565;
                                    xx152564 = SCM_I_INUM(x151856);
                                    yy152565 = SCM_I_INUM(y151857);
                                    
                                    
                                    
                                    if (xx152564 < 268435456
                                          && xx152564 > -268435456
                                               && yy152565 <= 32
                                                    && yy152565 >= 0) {
                                         {
                                            scm_t_int64 n152569;
                                            n152569 = xx152564 >> yy152565;
                                            
                                            
                                            
                                            AREF(sp, 1)
                                              = SCM_I_MAKINUM(n152569);
                                        }
                                    } else {
                                        AREF(sp, 1)
                                          = scm_ash(x151856,
                                                    scm_difference(y151857,
                                                                   scm_num6942));
                                    }
                                }
                            } else {
                                AREF(sp, 1)
                                  = scm_ash(x151856,
                                            scm_difference(y151857,
                                                           scm_num6942));
                            }
                            
                             {
                                void * jmp152566;
                                jmp152566
                                  = AREF(_soperations_s,
                                         scm_to_int(*(inst__pt151701)));
                                
                                
                                
                                ++inst__pt151701;
                                
                                goto *jmp152566;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          divide:
                            
                            printf("%s : %d\n", "divide", 72);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x151858;
                            SCM y151859;
                            x151858 = gp_gp_lookup(AREF(sp, 2), s151704);
                            y151859 = gp_gp_lookup(AREF(sp, 1), s151704);
                            
                            
                            
                             {
                                
                                AREF(sp, 1) = SCM_BOOL_F;
                                
                                sp = sp + 1;
                            }
                            
                            AREF(sp, 1) = scm_divide(x151858, y151859);
                            
                             {
                                void * jmp152574;
                                jmp152574
                                  = AREF(_soperations_s,
                                         scm_to_int(*(inst__pt151701)));
                                
                                
                                
                                ++inst__pt151701;
                                
                                goto *jmp152574;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          plus2_1:
                            
                            printf("%s : %d\n", "plus2_1", 67);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x151860;
                            SCM y151861;
                            x151860 = *inst__pt151701;
                            y151861 = gp_gp_lookup(AREF(sp, 1), s151704);
                            
                            
                            
                            if (SCM_I_INUMP(x151860) && SCM_I_INUMP(y151861)) {
                                 {
                                    scm_t_int64 n152577;
                                    n152577
                                      = SCM_I_INUM(x151860)
                                          + SCM_I_INUM(y151861);
                                    
                                    
                                    
                                    if (SCM_FIXABLE(n152577)) {
                                        AREF(sp, 1) = SCM_I_MAKINUM(n152577);
                                    } else {
                                        AREF(sp, 1)
                                          = scm_sum(x151860, y151861);
                                    }
                                }
                            } else {
                                AREF(sp, 1) = scm_sum(x151860, y151861);
                            }
                            
                            inst__pt151701 = inst__pt151701 + 1;
                            
                             {
                                void * jmp152578;
                                jmp152578
                                  = AREF(_soperations_s,
                                         scm_to_int(*(inst__pt151701)));
                                
                                
                                
                                ++inst__pt151701;
                                
                                goto *jmp152578;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          minus2_1:
                            
                            printf("%s : %d\n", "minus2_1", 69);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x151862;
                            SCM y151863;
                            x151862 = *inst__pt151701;
                            y151863 = gp_gp_lookup(AREF(sp, 1), s151704);
                            
                            
                            
                            if (SCM_I_INUMP(x151862) && SCM_I_INUMP(y151863)) {
                                 {
                                    scm_t_int64 n152583;
                                    n152583
                                      = SCM_I_INUM(x151862)
                                          - SCM_I_INUM(y151863);
                                    
                                    
                                    
                                    if (SCM_FIXABLE(n152583)) {
                                        AREF(sp, 1) = SCM_I_MAKINUM(n152583);
                                    } else {
                                        AREF(sp, 1)
                                          = scm_difference(x151862, y151863);
                                    }
                                }
                            } else {
                                AREF(sp, 1) = scm_difference(x151862, y151863);
                            }
                            
                            inst__pt151701 = inst__pt151701 + 1;
                            
                             {
                                void * jmp152584;
                                jmp152584
                                  = AREF(_soperations_s,
                                         scm_to_int(*(inst__pt151701)));
                                
                                
                                
                                ++inst__pt151701;
                                
                                goto *jmp152584;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          mul2_1:
                            
                            printf("%s : %d\n", "mul2_1", 71);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x151864;
                            SCM y151865;
                            x151864 = *inst__pt151701;
                            y151865 = gp_gp_lookup(AREF(sp, 1), s151704);
                            
                            
                            
                            if (SCM_I_INUMP(x151864) && SCM_I_INUMP(y151865)) {
                                 {
                                    scm_t_int64 xx152589;
                                    scm_t_int64 yy152590;
                                    xx152589 = SCM_I_INUM(x151864);
                                    yy152590 = SCM_I_INUM(y151865);
                                    
                                    
                                    
                                    if (xx152589 < 1073741824
                                          && xx152589 > -1073741824
                                               && yy152590 < 1073741824
                                                    && yy152590 > -1073741824) {
                                         {
                                            scm_t_int64 n152592;
                                            n152592 = xx152589 * yy152590;
                                            
                                            
                                            
                                            AREF(sp, 1)
                                              = SCM_I_MAKINUM(n152592);
                                        }
                                    } else {
                                        AREF(sp, 1)
                                          = scm_product(x151864, y151865);
                                    }
                                }
                            } else {
                                AREF(sp, 1) = scm_product(x151864, y151865);
                            }
                            
                            inst__pt151701 = inst__pt151701 + 1;
                            
                             {
                                void * jmp152591;
                                jmp152591
                                  = AREF(_soperations_s,
                                         scm_to_int(*(inst__pt151701)));
                                
                                
                                
                                ++inst__pt151701;
                                
                                goto *jmp152591;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          div2_1:
                            
                            printf("%s : %d\n", "div2_1", 73);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x151866;
                            SCM y151867;
                            x151866 = *inst__pt151701;
                            y151867 = gp_gp_lookup(AREF(sp, 1), s151704);
                            
                            
                            
                            AREF(sp, 1) = scm_divide(x151866, y151867);
                            
                            inst__pt151701 = inst__pt151701 + 1;
                            
                             {
                                void * jmp152601;
                                jmp152601
                                  = AREF(_soperations_s,
                                         scm_to_int(*(inst__pt151701)));
                                
                                
                                
                                ++inst__pt151701;
                                
                                goto *jmp152601;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          bitand:
                            
                            printf("%s : %d\n", "bitand", 93);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x151868;
                            SCM y151869;
                            x151868 = *inst__pt151701;
                            y151869 = gp_gp_lookup(AREF(sp, 1), s151704);
                            
                            
                            
                            if (SCM_I_INUMP(x151868) && SCM_I_INUMP(y151869)) {
                                 {
                                    scm_t_int64 n152604;
                                    n152604
                                      = SCM_I_INUM(x151868)
                                          & SCM_I_INUM(y151869);
                                    
                                    
                                    
                                    if (SCM_FIXABLE(n152604)) {
                                        AREF(sp, 1) = SCM_I_MAKINUM(n152604);
                                    } else {
                                        AREF(sp, 1)
                                          = scm_logand(x151868, y151869);
                                    }
                                }
                            } else {
                                AREF(sp, 1) = scm_logand(x151868, y151869);
                            }
                            
                            inst__pt151701 = inst__pt151701 + 1;
                            
                             {
                                void * jmp152605;
                                jmp152605
                                  = AREF(_soperations_s,
                                         scm_to_int(*(inst__pt151701)));
                                
                                
                                
                                ++inst__pt151701;
                                
                                goto *jmp152605;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          bitor:
                            
                            printf("%s : %d\n", "bitor", 95);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x151870;
                            SCM y151871;
                            x151870 = *inst__pt151701;
                            y151871 = gp_gp_lookup(AREF(sp, 1), s151704);
                            
                            
                            
                            if (SCM_I_INUMP(x151870) && SCM_I_INUMP(y151871)) {
                                 {
                                    scm_t_int64 n152610;
                                    n152610
                                      = SCM_I_INUM(x151870)
                                          | SCM_I_INUM(y151871);
                                    
                                    
                                    
                                    if (SCM_FIXABLE(n152610)) {
                                        AREF(sp, 1) = SCM_I_MAKINUM(n152610);
                                    } else {
                                        AREF(sp, 1)
                                          = scm_logior(x151870, y151871);
                                    }
                                }
                            } else {
                                AREF(sp, 1) = scm_logior(x151870, y151871);
                            }
                            
                            inst__pt151701 = inst__pt151701 + 1;
                            
                             {
                                void * jmp152611;
                                jmp152611
                                  = AREF(_soperations_s,
                                         scm_to_int(*(inst__pt151701)));
                                
                                
                                
                                ++inst__pt151701;
                                
                                goto *jmp152611;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          xor1:
                            
                            printf("%s : %d\n", "xor1", 97);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x151872;
                            SCM y151873;
                            x151872 = *inst__pt151701;
                            y151873 = gp_gp_lookup(AREF(sp, 1), s151704);
                            
                            
                            
                            if (SCM_I_INUMP(x151872) && SCM_I_INUMP(y151873)) {
                                 {
                                    scm_t_int64 n152616;
                                    n152616
                                      = SCM_I_INUM(x151872)
                                          ^ SCM_I_INUM(y151873);
                                    
                                    
                                    
                                    if (SCM_FIXABLE(n152616)) {
                                        AREF(sp, 1) = SCM_I_MAKINUM(n152616);
                                    } else {
                                        AREF(sp, 1)
                                          = scm_logxor(x151872, y151873);
                                    }
                                }
                            } else {
                                AREF(sp, 1) = scm_logxor(x151872, y151873);
                            }
                            
                            inst__pt151701 = inst__pt151701 + 1;
                            
                             {
                                void * jmp152617;
                                jmp152617
                                  = AREF(_soperations_s,
                                         scm_to_int(*(inst__pt151701)));
                                
                                
                                
                                ++inst__pt151701;
                                
                                goto *jmp152617;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          shiftLL:
                            
                            printf("%s : %d\n", "shiftLL", 81);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x151874;
                            SCM y151875;
                            x151874 = *inst__pt151701;
                            y151875 = gp_gp_lookup(AREF(sp, 1), s151704);
                            
                            
                            
                            if (SCM_I_INUMP(x151874) && SCM_I_INUMP(y151875)) {
                                 {
                                    scm_t_int64 xx152622;
                                    scm_t_int64 yy152623;
                                    xx152622 = SCM_I_INUM(x151874);
                                    yy152623 = SCM_I_INUM(y151875);
                                    
                                    
                                    
                                    if (xx152622 < 268435456
                                          && xx152622 > -268435456
                                               && yy152623 <= 32
                                                    && yy152623 >= 0) {
                                         {
                                            scm_t_int64 n152625;
                                            n152625 = xx152622 << yy152623;
                                            
                                            
                                            
                                            AREF(sp, 1)
                                              = SCM_I_MAKINUM(n152625);
                                        }
                                    } else {
                                        AREF(sp, 1)
                                          = scm_ash(x151874, y151875);
                                    }
                                }
                            } else {
                                AREF(sp, 1) = scm_ash(x151874, y151875);
                            }
                            
                            inst__pt151701 = inst__pt151701 + 1;
                            
                             {
                                void * jmp152624;
                                jmp152624
                                  = AREF(_soperations_s,
                                         scm_to_int(*(inst__pt151701)));
                                
                                
                                
                                ++inst__pt151701;
                                
                                goto *jmp152624;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          shiftRL:
                            
                            printf("%s : %d\n", "shiftRL", 84);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x151876;
                            SCM y151877;
                            x151876 = *inst__pt151701;
                            y151877 = gp_gp_lookup(AREF(sp, 1), s151704);
                            
                            
                            
                            if (SCM_I_INUMP(x151876) && SCM_I_INUMP(y151877)) {
                                 {
                                    scm_t_int64 xx152632;
                                    scm_t_int64 yy152633;
                                    xx152632 = SCM_I_INUM(x151876);
                                    yy152633 = SCM_I_INUM(y151877);
                                    
                                    
                                    
                                    if (xx152632 < 268435456
                                          && xx152632 > -268435456
                                               && yy152633 <= 32
                                                    && yy152633 >= 0) {
                                         {
                                            scm_t_int64 n152635;
                                            n152635 = xx152632 >> yy152633;
                                            
                                            
                                            
                                            AREF(sp, 1)
                                              = SCM_I_MAKINUM(n152635);
                                        }
                                    } else {
                                        AREF(sp, 1)
                                          = scm_ash(x151876,
                                                    scm_difference(y151877,
                                                                   scm_num6942));
                                    }
                                }
                            } else {
                                AREF(sp, 1)
                                  = scm_ash(x151876,
                                            scm_difference(y151877,
                                                           scm_num6942));
                            }
                            
                            inst__pt151701 = inst__pt151701 + 1;
                            
                             {
                                void * jmp152634;
                                jmp152634
                                  = AREF(_soperations_s,
                                         scm_to_int(*(inst__pt151701)));
                                
                                
                                
                                ++inst__pt151701;
                                
                                goto *jmp152634;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          shiftLR:
                            
                            printf("%s : %d\n", "shiftLR", 82);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x151878;
                            SCM y151879;
                            x151878 = *inst__pt151701;
                            y151879 = gp_gp_lookup(AREF(sp, 1), s151704);
                            
                            
                            
                            if (SCM_I_INUMP(y151879) && SCM_I_INUMP(x151878)) {
                                 {
                                    scm_t_int64 xx152642;
                                    scm_t_int64 yy152643;
                                    xx152642 = SCM_I_INUM(y151879);
                                    yy152643 = SCM_I_INUM(x151878);
                                    
                                    
                                    
                                    if (xx152642 < 268435456
                                          && xx152642 > -268435456
                                               && yy152643 <= 32
                                                    && yy152643 >= 0) {
                                         {
                                            scm_t_int64 n152645;
                                            n152645 = xx152642 << yy152643;
                                            
                                            
                                            
                                            AREF(sp, 1)
                                              = SCM_I_MAKINUM(n152645);
                                        }
                                    } else {
                                        AREF(sp, 1)
                                          = scm_ash(y151879, x151878);
                                    }
                                }
                            } else {
                                AREF(sp, 1) = scm_ash(y151879, x151878);
                            }
                            
                            inst__pt151701 = inst__pt151701 + 1;
                            
                             {
                                void * jmp152644;
                                jmp152644
                                  = AREF(_soperations_s,
                                         scm_to_int(*(inst__pt151701)));
                                
                                
                                
                                ++inst__pt151701;
                                
                                goto *jmp152644;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          shiftRR:
                            
                            printf("%s : %d\n", "shiftRR", 85);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x151880;
                            SCM y151881;
                            x151880 = *inst__pt151701;
                            y151881 = gp_gp_lookup(AREF(sp, 1), s151704);
                            
                            
                            
                            if (SCM_I_INUMP(y151881) && SCM_I_INUMP(x151880)) {
                                 {
                                    scm_t_int64 xx152652;
                                    scm_t_int64 yy152653;
                                    xx152652 = SCM_I_INUM(y151881);
                                    yy152653 = SCM_I_INUM(x151880);
                                    
                                    
                                    
                                    if (xx152652 < 268435456
                                          && xx152652 > -268435456
                                               && yy152653 <= 32
                                                    && yy152653 >= 0) {
                                         {
                                            scm_t_int64 n152655;
                                            n152655 = xx152652 >> yy152653;
                                            
                                            
                                            
                                            AREF(sp, 1)
                                              = SCM_I_MAKINUM(n152655);
                                        }
                                    } else {
                                        AREF(sp, 1)
                                          = scm_ash(y151881,
                                                    scm_difference(x151880,
                                                                   scm_num6942));
                                    }
                                }
                            } else {
                                AREF(sp, 1)
                                  = scm_ash(y151881,
                                            scm_difference(x151880,
                                                           scm_num6942));
                            }
                            
                            inst__pt151701 = inst__pt151701 + 1;
                            
                             {
                                void * jmp152654;
                                jmp152654
                                  = AREF(_soperations_s,
                                         scm_to_int(*(inst__pt151701)));
                                
                                
                                
                                ++inst__pt151701;
                                
                                goto *jmp152654;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          modL:
                            
                            printf("%s : %d\n", "modL", 87);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x151882;
                            SCM y151883;
                            x151882 = *inst__pt151701;
                            y151883 = gp_gp_lookup(AREF(sp, 1), s151704);
                            
                            
                            
                            if (SCM_I_INUMP(x151882) && SCM_I_INUMP(y151883)) {
                                 {
                                    scm_t_int64 n152662;
                                    n152662
                                      = SCM_I_INUM(x151882)
                                          % SCM_I_INUM(y151883);
                                    
                                    
                                    
                                    if (SCM_FIXABLE(n152662)) {
                                        AREF(sp, 1) = SCM_I_MAKINUM(n152662);
                                    } else {
                                        AREF(sp, 1)
                                          = scm_modulo(x151882, y151883);
                                    }
                                }
                            } else {
                                AREF(sp, 1) = scm_modulo(x151882, y151883);
                            }
                            
                            inst__pt151701 = inst__pt151701 + 1;
                            
                             {
                                void * jmp152663;
                                jmp152663
                                  = AREF(_soperations_s,
                                         scm_to_int(*(inst__pt151701)));
                                
                                
                                
                                ++inst__pt151701;
                                
                                goto *jmp152663;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          modR:
                            
                            printf("%s : %d\n", "modR", 88);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x151884;
                            SCM y151885;
                            x151884 = *inst__pt151701;
                            y151885 = gp_gp_lookup(AREF(sp, 1), s151704);
                            
                            
                            
                            if (SCM_I_INUMP(y151885) && SCM_I_INUMP(x151884)) {
                                 {
                                    scm_t_int64 n152668;
                                    n152668
                                      = SCM_I_INUM(y151885)
                                          % SCM_I_INUM(x151884);
                                    
                                    
                                    
                                    if (SCM_FIXABLE(n152668)) {
                                        AREF(sp, 1) = SCM_I_MAKINUM(n152668);
                                    } else {
                                        AREF(sp, 1)
                                          = scm_modulo(y151885, x151884);
                                    }
                                }
                            } else {
                                AREF(sp, 1) = scm_modulo(y151885, x151884);
                            }
                            
                            inst__pt151701 = inst__pt151701 + 1;
                            
                             {
                                void * jmp152669;
                                jmp152669
                                  = AREF(_soperations_s,
                                         scm_to_int(*(inst__pt151701)));
                                
                                
                                
                                ++inst__pt151701;
                                
                                goto *jmp152669;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          uminus:
                            
                            printf("%s : %d\n", "uminus", 104);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x151886;
                            x151886 = gp_gp_lookup(AREF(sp, 1), s151704);
                            
                            
                            
                            AREF(sp, 1)
                              = scm_difference(scm_from_int(0), x151886);
                            
                             {
                                void * jmp152674;
                                jmp152674
                                  = AREF(_soperations_s,
                                         scm_to_int(*(inst__pt151701)));
                                
                                
                                
                                ++inst__pt151701;
                                
                                goto *jmp152674;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          lognot:
                            
                            printf("%s : %d\n", "lognot", 106);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x151887;
                            x151887 = gp_gp_lookup(AREF(sp, 1), s151704);
                            
                            
                            
                            if (SCM_I_INUMP(x151887)) {
                                 {
                                    scm_t_int64 n152675;
                                    n152675 = ~SCM_I_INUM(x151887);
                                    
                                    
                                    
                                    if (SCM_FIXABLE(n152675)) {
                                        AREF(sp, 1) = SCM_I_MAKINUM(n152675);
                                    } else {
                                        AREF(sp, 1) = scm_lognot(x151887);
                                    }
                                }
                            } else {
                                AREF(sp, 1) = scm_lognot(x151887);
                            }
                            
                             {
                                void * jmp152676;
                                jmp152676
                                  = AREF(_soperations_s,
                                         scm_to_int(*(inst__pt151701)));
                                
                                
                                
                                ++inst__pt151701;
                                
                                goto *jmp152676;
                            }
                        }
                    }
                    
                     {
                        
                        
                      gt:
                        
                        printf("%s : %d\n", "gt", 52);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM y151889;
                        SCM x151890;
                         {
                            SCM nn151888;
                            nn151888 = *inst__pt151701;
                            
                            
                            
                            ++inst__pt151701;
                            
                            if (scm_is_false(nn151888)) {
                                 {
                                    SCM xx152680;
                                    xx152680 = AREF(sp, 1);
                                    
                                    
                                    
                                     {
                                        
                                        AREF(sp, 1) = SCM_BOOL_F;
                                        
                                        sp = sp + 1;
                                    }
                                    
                                    y151889 = xx152680;
                                }
                            } else {
                                y151889 = nn151888;
                            }
                        }
                        x151890 = AREF(sp, 1);
                        
                        
                        
                        scm_simple_format(SCM_BOOL_T,
                                          scm_string100301,
                                          scm_list_3(x151890,
                                                     scm_gr_p100683,
                                                     y151889));
                        
                        if (SCM_I_INUMP(x151890) && SCM_I_INUMP(y151889)) {
                            if (!(x151890 > y151889)) {
                                if (SCM_I_INUMP(p151705)) {
                                     {
                                        
                                        inst__pt151701
                                          = instructions151693
                                              + scm_to_int(p151705);
                                        
                                         {
                                            void * jmp152678;
                                            jmp152678
                                              = AREF(_soperations_s,
                                                     scm_to_int(*(inst__pt151701)));
                                            
                                            
                                            
                                            ++inst__pt151701;
                                            
                                            goto *jmp152678;
                                        }
                                    }
                                } else {
                                     {
                                        
                                        AREF(fp, 0) = p151705;
                                        
                                        sp = fp + -1;
                                        
                                        goto ret;
                                    }
                                }
                            }
                        } else {
                            if (scm_is_false(scm_gr_p(x151890, y151889))) {
                                if (SCM_I_INUMP(p151705)) {
                                     {
                                        
                                        inst__pt151701
                                          = instructions151693
                                              + scm_to_int(p151705);
                                        
                                         {
                                            void * jmp152679;
                                            jmp152679
                                              = AREF(_soperations_s,
                                                     scm_to_int(*(inst__pt151701)));
                                            
                                            
                                            
                                            ++inst__pt151701;
                                            
                                            goto *jmp152679;
                                        }
                                    }
                                } else {
                                     {
                                        
                                        AREF(fp, 0) = p151705;
                                        
                                        sp = fp + -1;
                                        
                                        goto ret;
                                    }
                                }
                            }
                        }
                        
                         {
                            
                            AREF(sp, 1) = SCM_BOOL_F;
                            
                            sp = sp + 1;
                        }
                    }
                    
                     {
                        void * jmp151891;
                        jmp151891
                          = AREF(_soperations_s, scm_to_int(*(inst__pt151701)));
                        
                        
                        
                        ++inst__pt151701;
                        
                        goto *jmp151891;
                    }
                    
                     {
                        
                        
                      ls:
                        
                        printf("%s : %d\n", "ls", 53);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM y151893;
                        SCM x151894;
                         {
                            SCM nn151892;
                            nn151892 = *inst__pt151701;
                            
                            
                            
                            ++inst__pt151701;
                            
                            if (scm_is_false(nn151892)) {
                                 {
                                    SCM xx152697;
                                    xx152697 = AREF(sp, 1);
                                    
                                    
                                    
                                     {
                                        
                                        AREF(sp, 1) = SCM_BOOL_F;
                                        
                                        sp = sp + 1;
                                    }
                                    
                                    y151893 = xx152697;
                                }
                            } else {
                                y151893 = nn151892;
                            }
                        }
                        x151894 = AREF(sp, 1);
                        
                        
                        
                        scm_simple_format(SCM_BOOL_T,
                                          scm_string100301,
                                          scm_list_3(x151894,
                                                     scm_less_p102828,
                                                     y151893));
                        
                        if (SCM_I_INUMP(x151894) && SCM_I_INUMP(y151893)) {
                            if (!(x151894 < y151893)) {
                                if (SCM_I_INUMP(p151705)) {
                                     {
                                        
                                        inst__pt151701
                                          = instructions151693
                                              + scm_to_int(p151705);
                                        
                                         {
                                            void * jmp152695;
                                            jmp152695
                                              = AREF(_soperations_s,
                                                     scm_to_int(*(inst__pt151701)));
                                            
                                            
                                            
                                            ++inst__pt151701;
                                            
                                            goto *jmp152695;
                                        }
                                    }
                                } else {
                                     {
                                        
                                        AREF(fp, 0) = p151705;
                                        
                                        sp = fp + -1;
                                        
                                        goto ret;
                                    }
                                }
                            }
                        } else {
                            if (scm_is_false(scm_less_p(x151894, y151893))) {
                                if (SCM_I_INUMP(p151705)) {
                                     {
                                        
                                        inst__pt151701
                                          = instructions151693
                                              + scm_to_int(p151705);
                                        
                                         {
                                            void * jmp152696;
                                            jmp152696
                                              = AREF(_soperations_s,
                                                     scm_to_int(*(inst__pt151701)));
                                            
                                            
                                            
                                            ++inst__pt151701;
                                            
                                            goto *jmp152696;
                                        }
                                    }
                                } else {
                                     {
                                        
                                        AREF(fp, 0) = p151705;
                                        
                                        sp = fp + -1;
                                        
                                        goto ret;
                                    }
                                }
                            }
                        }
                        
                         {
                            
                            AREF(sp, 1) = SCM_BOOL_F;
                            
                            sp = sp + 1;
                        }
                    }
                    
                     {
                        void * jmp151895;
                        jmp151895
                          = AREF(_soperations_s, scm_to_int(*(inst__pt151701)));
                        
                        
                        
                        ++inst__pt151701;
                        
                        goto *jmp151895;
                    }
                    
                     {
                        
                        
                      ge:
                        
                        printf("%s : %d\n", "ge", 54);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM y151897;
                        SCM x151898;
                         {
                            SCM nn151896;
                            nn151896 = *inst__pt151701;
                            
                            
                            
                            ++inst__pt151701;
                            
                            if (scm_is_false(nn151896)) {
                                 {
                                    SCM xx152714;
                                    xx152714 = AREF(sp, 1);
                                    
                                    
                                    
                                     {
                                        
                                        AREF(sp, 1) = SCM_BOOL_F;
                                        
                                        sp = sp + 1;
                                    }
                                    
                                    y151897 = xx152714;
                                }
                            } else {
                                y151897 = nn151896;
                            }
                        }
                        x151898 = AREF(sp, 1);
                        
                        
                        
                        scm_simple_format(SCM_BOOL_T,
                                          scm_string100301,
                                          scm_list_3(x151898,
                                                     scm_geq_p104973,
                                                     y151897));
                        
                        if (SCM_I_INUMP(x151898) && SCM_I_INUMP(y151897)) {
                            if (!(x151898 >= y151897)) {
                                if (SCM_I_INUMP(p151705)) {
                                     {
                                        
                                        inst__pt151701
                                          = instructions151693
                                              + scm_to_int(p151705);
                                        
                                         {
                                            void * jmp152712;
                                            jmp152712
                                              = AREF(_soperations_s,
                                                     scm_to_int(*(inst__pt151701)));
                                            
                                            
                                            
                                            ++inst__pt151701;
                                            
                                            goto *jmp152712;
                                        }
                                    }
                                } else {
                                     {
                                        
                                        AREF(fp, 0) = p151705;
                                        
                                        sp = fp + -1;
                                        
                                        goto ret;
                                    }
                                }
                            }
                        } else {
                            if (scm_is_false(scm_geq_p(x151898, y151897))) {
                                if (SCM_I_INUMP(p151705)) {
                                     {
                                        
                                        inst__pt151701
                                          = instructions151693
                                              + scm_to_int(p151705);
                                        
                                         {
                                            void * jmp152713;
                                            jmp152713
                                              = AREF(_soperations_s,
                                                     scm_to_int(*(inst__pt151701)));
                                            
                                            
                                            
                                            ++inst__pt151701;
                                            
                                            goto *jmp152713;
                                        }
                                    }
                                } else {
                                     {
                                        
                                        AREF(fp, 0) = p151705;
                                        
                                        sp = fp + -1;
                                        
                                        goto ret;
                                    }
                                }
                            }
                        }
                        
                         {
                            
                            AREF(sp, 1) = SCM_BOOL_F;
                            
                            sp = sp + 1;
                        }
                    }
                    
                     {
                        void * jmp151899;
                        jmp151899
                          = AREF(_soperations_s, scm_to_int(*(inst__pt151701)));
                        
                        
                        
                        ++inst__pt151701;
                        
                        goto *jmp151899;
                    }
                    
                     {
                        
                        
                      le:
                        
                        printf("%s : %d\n", "le", 55);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM y151901;
                        SCM x151902;
                         {
                            SCM nn151900;
                            nn151900 = *inst__pt151701;
                            
                            
                            
                            ++inst__pt151701;
                            
                            if (scm_is_false(nn151900)) {
                                 {
                                    SCM xx152731;
                                    xx152731 = AREF(sp, 1);
                                    
                                    
                                    
                                     {
                                        
                                        AREF(sp, 1) = SCM_BOOL_F;
                                        
                                        sp = sp + 1;
                                    }
                                    
                                    y151901 = xx152731;
                                }
                            } else {
                                y151901 = nn151900;
                            }
                        }
                        x151902 = AREF(sp, 1);
                        
                        
                        
                        scm_simple_format(SCM_BOOL_T,
                                          scm_string100301,
                                          scm_list_3(x151902,
                                                     scm_leq_p107118,
                                                     y151901));
                        
                        if (SCM_I_INUMP(x151902) && SCM_I_INUMP(y151901)) {
                            if (!(x151902 >= y151901)) {
                                if (SCM_I_INUMP(p151705)) {
                                     {
                                        
                                        inst__pt151701
                                          = instructions151693
                                              + scm_to_int(p151705);
                                        
                                         {
                                            void * jmp152729;
                                            jmp152729
                                              = AREF(_soperations_s,
                                                     scm_to_int(*(inst__pt151701)));
                                            
                                            
                                            
                                            ++inst__pt151701;
                                            
                                            goto *jmp152729;
                                        }
                                    }
                                } else {
                                     {
                                        
                                        AREF(fp, 0) = p151705;
                                        
                                        sp = fp + -1;
                                        
                                        goto ret;
                                    }
                                }
                            }
                        } else {
                            if (scm_is_false(scm_leq_p(x151902, y151901))) {
                                if (SCM_I_INUMP(p151705)) {
                                     {
                                        
                                        inst__pt151701
                                          = instructions151693
                                              + scm_to_int(p151705);
                                        
                                         {
                                            void * jmp152730;
                                            jmp152730
                                              = AREF(_soperations_s,
                                                     scm_to_int(*(inst__pt151701)));
                                            
                                            
                                            
                                            ++inst__pt151701;
                                            
                                            goto *jmp152730;
                                        }
                                    }
                                } else {
                                     {
                                        
                                        AREF(fp, 0) = p151705;
                                        
                                        sp = fp + -1;
                                        
                                        goto ret;
                                    }
                                }
                            }
                        }
                        
                         {
                            
                            AREF(sp, 1) = SCM_BOOL_F;
                            
                            sp = sp + 1;
                        }
                    }
                    
                     {
                        void * jmp151903;
                        jmp151903
                          = AREF(_soperations_s, scm_to_int(*(inst__pt151701)));
                        
                        
                        
                        ++inst__pt151701;
                        
                        goto *jmp151903;
                    }
                    
                     {
                        
                        
                      eq:
                        
                        printf("%s : %d\n", "eq", 56);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM y151905;
                        SCM x151906;
                         {
                            SCM nn151904;
                            nn151904 = *inst__pt151701;
                            
                            
                            
                            ++inst__pt151701;
                            
                            if (scm_is_false(nn151904)) {
                                 {
                                    SCM xx152748;
                                    xx152748 = AREF(sp, 1);
                                    
                                    
                                    
                                     {
                                        
                                        AREF(sp, 1) = SCM_BOOL_F;
                                        
                                        sp = sp + 1;
                                    }
                                    
                                    y151905 = xx152748;
                                }
                            } else {
                                y151905 = nn151904;
                            }
                        }
                        x151906 = AREF(sp, 1);
                        
                        
                        
                        scm_simple_format(SCM_BOOL_T,
                                          scm_string100301,
                                          scm_list_3(x151906,
                                                     scm_equal_p109263,
                                                     y151905));
                        
                        if (SCM_I_INUMP(x151906) && SCM_I_INUMP(y151905)) {
                            if (!(x151906 == y151905)) {
                                if (SCM_I_INUMP(p151705)) {
                                     {
                                        
                                        inst__pt151701
                                          = instructions151693
                                              + scm_to_int(p151705);
                                        
                                         {
                                            void * jmp152746;
                                            jmp152746
                                              = AREF(_soperations_s,
                                                     scm_to_int(*(inst__pt151701)));
                                            
                                            
                                            
                                            ++inst__pt151701;
                                            
                                            goto *jmp152746;
                                        }
                                    }
                                } else {
                                     {
                                        
                                        AREF(fp, 0) = p151705;
                                        
                                        sp = fp + -1;
                                        
                                        goto ret;
                                    }
                                }
                            }
                        } else {
                            if (scm_is_false(scm_equal_p(x151906, y151905))) {
                                if (SCM_I_INUMP(p151705)) {
                                     {
                                        
                                        inst__pt151701
                                          = instructions151693
                                              + scm_to_int(p151705);
                                        
                                         {
                                            void * jmp152747;
                                            jmp152747
                                              = AREF(_soperations_s,
                                                     scm_to_int(*(inst__pt151701)));
                                            
                                            
                                            
                                            ++inst__pt151701;
                                            
                                            goto *jmp152747;
                                        }
                                    }
                                } else {
                                     {
                                        
                                        AREF(fp, 0) = p151705;
                                        
                                        sp = fp + -1;
                                        
                                        goto ret;
                                    }
                                }
                            }
                        }
                        
                         {
                            
                            AREF(sp, 1) = SCM_BOOL_F;
                            
                            sp = sp + 1;
                        }
                    }
                    
                     {
                        
                        
                      neq:
                        
                        printf("%s : %d\n", "neq", 57);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM y151908;
                        SCM x151909;
                         {
                            SCM nn151907;
                            nn151907 = *inst__pt151701;
                            
                            
                            
                            ++inst__pt151701;
                            
                            if (scm_is_false(nn151907)) {
                                 {
                                    SCM xx152765;
                                    xx152765 = AREF(sp, 1);
                                    
                                    
                                    
                                     {
                                        
                                        AREF(sp, 1) = SCM_BOOL_F;
                                        
                                        sp = sp + 1;
                                    }
                                    
                                    y151908 = xx152765;
                                }
                            } else {
                                y151908 = nn151907;
                            }
                        }
                        x151909 = AREF(sp, 1);
                        
                        
                        
                        scm_simple_format(SCM_BOOL_T,
                                          scm_string100301,
                                          scm_list_3(x151909,
                                                     scm_equal_p109263,
                                                     y151908));
                        
                        if (SCM_I_INUMP(x151909) && SCM_I_INUMP(y151908)) {
                            if (x151909 == y151908) {
                                if (SCM_I_INUMP(p151705)) {
                                     {
                                        
                                        inst__pt151701
                                          = instructions151693
                                              + scm_to_int(p151705);
                                        
                                         {
                                            void * jmp152763;
                                            jmp152763
                                              = AREF(_soperations_s,
                                                     scm_to_int(*(inst__pt151701)));
                                            
                                            
                                            
                                            ++inst__pt151701;
                                            
                                            goto *jmp152763;
                                        }
                                    }
                                } else {
                                     {
                                        
                                        AREF(fp, 0) = p151705;
                                        
                                        sp = fp + -1;
                                        
                                        goto ret;
                                    }
                                }
                            }
                        } else {
                            if (scm_is_true(scm_equal_p(x151909, y151908))) {
                                if (SCM_I_INUMP(p151705)) {
                                     {
                                        
                                        inst__pt151701
                                          = instructions151693
                                              + scm_to_int(p151705);
                                        
                                         {
                                            void * jmp152764;
                                            jmp152764
                                              = AREF(_soperations_s,
                                                     scm_to_int(*(inst__pt151701)));
                                            
                                            
                                            
                                            ++inst__pt151701;
                                            
                                            goto *jmp152764;
                                        }
                                    }
                                } else {
                                     {
                                        
                                        AREF(fp, 0) = p151705;
                                        
                                        sp = fp + -1;
                                        
                                        goto ret;
                                    }
                                }
                            }
                        }
                        
                         {
                            
                            AREF(sp, 1) = SCM_BOOL_F;
                            
                            sp = sp + 1;
                        }
                    }
                    
                    
                  ret:
                    
                    ret151707 = sp;
                }
            }
        }
        return ret151707;
    }
}

void init_prolog_vm ();

void init_prolog_vm () {
     {
        
         {
            scm_equal_p109263 = scm_from_locale_symbol("scm_equal_p");
            scm_leq_p107118 = scm_from_locale_symbol("scm_leq_p");
            scm_geq_p104973 = scm_from_locale_symbol("scm_geq_p");
            scm_less_p102828 = scm_from_locale_symbol("scm_less_p");
            scm_gr_p100683 = scm_from_locale_symbol("scm_gr_p");
            brace68201 = scm_from_locale_keyword("brace");
            scm_num6942 = scm_c_locale_stringn_to_number("0", 1, 10);
            scm_string100301 = scm_from_locale_string("~a ~a ~a~%\n");
            scm_string73598
              = scm_from_locale_string("push-scm id: ~a, value: ~a~%");
            scm_string26080 = scm_from_locale_string("STACK: ~a~%");
            
        }
        
        vm__raw(0, 0, 0, 0, 1);
    }
}

