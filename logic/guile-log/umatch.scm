; UNIFY MATCH MATCHER COMPILER

;; Copyright (C) 2001,2008,2009,2010 Free Software Foundation, Inc.

;;;; This library is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU Lesser General Public
;;;; License as published by the Free Software Foundation; either
;;;; version 3 of the License, or (at your option) any later version.
;;;; 
;;;; This library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;;; Lesser General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU Lesser General Public
;;;; License along with this library; if not, write to the Free Software
;;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

;;; Code:

(define-module (logic guile-log umatch)
  #:use-module (logic guile-log procedure-properties)
  #:use-module (logic guile-log code-load)
  #:use-module (logic guile-log persistance)
  #:use-module (logic guile-log vlist)
  #:use-module (logic guile-log attributed)
  #:use-module (logic guile-log persistance)
  #:use-module (ice-9 match-phd-lookup)
  #:use-module (ice-9 pretty-print)
  #:use-module (ice-9 format)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-11)
  #:use-module (srfi srfi-9)
  #:export 
  (gp-member gp-right-of gp-einstein gp-next-to
	     gp-store-state gp-store-state-all
	     )
  #:re-export (gp-clear  #;gp-newframe   gp-heap-var! 
                         ;gp-unify! gp-unify-raw! gp-m-unify!
                          gp-car gp-cdr
			   gp-print gp?
		         
			 gp-get-id-data
			 gp-get-var-var
			 gp-clobber-var
		     
                         gp-budy 
                         gp-lookup gp-lookup-1
                         gp-var? gp-cons! gp-set! 
                         gp-var-number gp-print-stack
                         gp-pair? gp-pair!? gp-null? gp-null!?
                         gp-pair+ gp-pair* gp-pair-
			 
			 gp-ref-set! gp-set-1!
			 gp-var-set gp-guard-vars
			 gp-deterministic?
                        #;gp-unwind
			 gp-prune gp-prune-tail
			 gp-fluid-set
                         gp-atomic? 
                         gp-logical++ gp-logical--
                         gp-get-stack
                         gp-module-init
                         gp-make-stack                         
                         gp-thread-safe-set!			 
			 gp-undo-safe-variable-rguard                
			 gp-undo-safe-variable-lguard                
			 gp-undo-safe-variable-guard                
			 gp-abort gp-prompt

                         gp-clear-frame
                         gp-clear-frame!

			 gp-handlers-ref
			 gp-handlers-set!

			 gp-cont-ids-ref
			 gp-cont-ids-set!

                         gp-gc

			 gp-make-attribute-from-data
			 gp-attvar?
			 gp-attvar-raw?
			 gp-att-raw-var
			 gp-att-data
			 gp-put-attr
			 gp-put-attr-guarded
			 gp-put-attr-weak-guarded
			 gp-put-attr!
			 gp-put-attr-guarded!
			 gp-put-attr-weak-guarded!
			 gp-get-attr
			 gp-del-attr
			 gp-del-attr!
			 
			 gp-add-unwind-hook
			 gp-mark-permanent
			 
			 gp-make-ephermal-pair

			 gp-get-taglist
			 gp-match

			 int-to-code
			 code-to-int

			 gp-make-null-procedure

                         gp-push-engine
                         gp-peek-engine
                         gp-pop-engine
                         gp-combine-engines
                         gp-combine-state
			 )
		         
                         

  #:export(gp-restore-wind  gp-dynwind gp-restore-state
                            gp-logical-var? umatch **um** 
                            gp-printer gp-var-ref
                            with-guarded-states with-guarded-globals
                            use-logical leave-logical *current-stack* 
                            gp-cp gp->scm
			    gp-make-var
			    gp-new-wind-level
			    gp-with-wind
			    gp-windlevel-ref
			    gp-fluid-set!
			    gp-var!
			    *gp*
                            *windlevel*
			    gp-var-set! 
			    gp-new-postpone-level
			    gp-get-fixed-free
			    gp-get-id-free
                            gp-rebased-level-ref
                            get-free-variables-map
			    gp-newframe
			    gp-newframe-choice
			    gp-unwind
			    gp-unwind-tail
			    *unwind-parameters*
			    gp-unify! 
			    gp-unify-raw! gp-m-unify!
			    gp-unifier gp-raw-unifier gp-m-unifier
			    gp-scm-unpin
			    *unwind-hooks*
			    *var-attributator*
			    add-new-unwind-hook
			    doit_on
			    doit_off

			    ref-attribute-constructors
			    do-attribute-constructors
			    set-attribute-cstor!
                            attribute-cstor-ref
			    attribute-cstor-repr
                            gp-make-engine
                            #;gp-unwind))

(define old (@ (logic guile-log code-load) gp-make-var))

(define gp-make-var
  (case-lambda 
   (()  (let ((v ((@ (logic guile-log code-load) gp-make-var))))
	  (if (not (fluid-ref inhibit-doit))
	      (do-varit v (fluid-ref *current-stack*)))
	  v))
   ((x) (let ((ret ((@ (logic guile-log code-load) gp-make-var))))
	  (if (not (fluid-ref inhibit-doit))
	      (gp-var-set! ret x))
	  ret))))

(define (doit_off s p cc)
  (fluid-set! inhibit-doit #t)
  (cc s p))

(define (doit_on s p cc)
  (fluid-set! inhibit-doit #f)
  (cc s p))

(define doit-guard (make-fluid #t))
(define inhibit-doit (make-fluid #f))
(define (do-varit v s)
  (let ((l (fluid-ref *var-attributator*)))
    (if (and (pair? l) (not (fluid-ref doit-guard)))
	(begin
	  (fluid-set! inhibit-doit #t)	
	  (let lp ((l (reverse l)))
	    (if (pair? l)		
		(let* ((r  (car l))
		       (rr (car r)))
		  (if (gp-put-attr v (caar rr) (gp-cp (cdar rr) (cdr r) s) s)
		      #t		 
		      (warn "could not put attribute on var"))
		  (lp (cdr l)))))
	  (fluid-set! inhibit-doit #f)))))

(define-named-object *var-attributator* (make-fluid '()))
(define (gp-var! s)
  (let ((v (gp-var!- s)))
    (if (not (fluid-ref inhibit-doit))
	(do-varit v s))
    v))

(define gp-unifier     (make-fluid gp-unify!-))
(define gp-raw-unifier (make-fluid gp-unify-raw!-))
(define gp-m-unifier   (make-fluid gp-m-unify!-))

(define (gp-unify!     x y s) ((fluid-ref gp-unifier)     x y s))
(define (gp-unify-raw! x y s) ((fluid-ref gp-raw-unifier) x y s))
(define (gp-m-unify!   x y s) ((fluid-ref gp-m-unifier)   x y s))

;;fast call 1 args
(gp-module-init)


(define *unwind-hooks* (make-fluid '()))
(define newf   (@@ (logic guile-log code-load) gp-newframe))
(define newfch (@@ (logic guile-log code-load) gp-newframe-choice))
(define unw    (@@ (logic guile-log code-load) gp-unwind))
(define unw-t  (@@ (logic guile-log code-load) gp-unwind-tail))
(define (add-new-unwind-hook lam) 
  (fluid-set! *unwind-hooks* 
	      (cons lam (fluid-ref *unwind-hooks*))))


(define *unwind-parameters* (make-fluid '()))

(begin
  (define gp-unwind
    (lambda (fr)
      (fluid-set! *unwind-hooks* '())
      (unw fr)
      (let ((l.a (fluid-ref *unwind-parameters*)))
	(if (pair? l.a)
            (for-each 
             (lambda (f) ((car f) (cdr f)))
             (cdr l.a))))
      (let lp ((l (reverse (fluid-ref *unwind-hooks*))))
	(if (pair? l)
	    (begin
	      ((car l) fr (lambda () #f) (lambda x #t))
	      (unw fr)
	      (lp (cdr l)))))))

  (define gp-unwind-tail
    (lambda (fr)
      (fluid-set! *unwind-hooks* '())
      (let ((ll (reverse (fluid-ref *unwind-hooks*))))
	(if (pair? ll)
	    (begin
	      (unw fr)
	      (let ((l.a (fluid-ref *unwind-parameters*)))
		(if (pair? l.a)
		    (for-each 
		     (lambda (f) ((car f) (cdr f)))
		     (cdr l.a))))
      
	      (let lp ((l ll))
		(if (pair? l)
		    (begin
		      ((car l) fr (lambda () #f) (lambda x #t))
		      (unw fr)
		      (lp (cdr l)))))
	      (unw-t fr))
	    (begin
       	      (unw-t fr)
	      (let ((l.a (fluid-ref *unwind-parameters*)))
		(if (pair? l.a)
		    (for-each 
		     (lambda (f) ((car f) (cdr f)))
		     (cdr l.a)))))))))
	      
  (define (gp-newframe s)
    (let ((l.a (fluid-ref *unwind-parameters*)))
      (if (pair? l.a)
          (gp-fluid-set *unwind-parameters*
                        (cons (car l.a)
                              (let lp ((l (car l.a)))
				(if (pair? l)
				    (let ((f (car l)))
				      (cons
				       (cons f (f))
				       (lp (cdr l))))
				    '())))))
      (let ((r (newf s)))
	r)))

  (define (gp-newframe-choice s)
    (let ((l.a (fluid-ref *unwind-parameters*)))
      (if (pair? l.a)
          (gp-fluid-set *unwind-parameters*
                        (cons (car l.a)
                              (let lp ((l (car l.a)))
				(if (pair? l)
				    (let ((f (car l)))
				      (cons
				       (cons f (f))
				       (lp (cdr l))))
				    '())))))
      (let ((r (newfch s)))
	r))))

(define gp-fluid-set! fluid-set!)

;; assq kind of base structure
(define mk-logical gp-var!)
(define logical? gp?)

(define gp-logical-var? gp?)

(define-syntax **um** (syntax-rules () ((_ . l) (umatch . l))))
(define dyn (@@ (logic guile-log code-load) gp-dynwind))

(define *states* #t)

(define (gp-make-engine id n)
  (gp-make-stack id 0 n n n n))

(define-named-object *gp* (gp-current-stack-ref))
(define root-engine (gp-make-stack 0 0 5000000 5000000 5000000 1000000))
(fluid-set! *gp* root-engine)

(define *current-stack* (make-fluid '()))
((@@(logic guile-log code-load) gp-set-current-stack) *current-stack*)
(gp-push-engine (fluid-ref *current-stack*)
                root-engine)


(define (gp-var-set! v val)
  ((@ (logic guile-log code-load) gp-var-set!)
   v val (fluid-ref *current-stack*)))

(define (glup x s) (gp-lookup x s))

(use-modules (srfi srfi-11))

(define (before? x s)
  (let ((v (vhashq-ref (fluid-ref recursive-handler) x #f)))
    (if v
	(if (eq? v #t)
	    (let ((i (fluid-ref recursive-i)))
	      (fluid-set! recursive-handler
			  (vhash-consq x i (fluid-ref recursive-handler)))
	      (fluid-set! recursive-i (+ 1 (fluid-ref recursive-i)))
	      i)
	    v)
	(begin
	  (fluid-set! recursive-handler 
		      (vhash-consq x #t (fluid-ref recursive-handler)))
	  #f))))

(define (head? x s)
  (let ((v (vhashq-ref (fluid-ref recursive-handler) x #f)))
    (if v
	(if (eq? v #t)
	    #f
	    v)
	#f)))

(define-syntax-rule (string-it x s port pre post code)
  (let ((lam1 (lambda (code2)
		(with-fluids ((recursive-handler 
			       vlist-null)
			      (recursive-i       
			       0))
		   (code2))))
	(lam2 (lambda ()           
		(let ((r (before? x s)))
		  (if r
		      (format port "~a#~a~a" pre r post)
		      (let ((str code))
			(let((r (head? x s)))
			  (if r
			      (format port "<~a>~a}" r str)
			      (format port str)))))))))
    (if (< (fluid-ref recursive-i) 0)
	(lam1 lam2)
	(lam2))))


(define (get-line s x)
  (let lp ((x x))
    (let ((x (glup x s)))
      (let ((y (glup (gp-cdr x s) s)))
	(if (gp? y) 
	    (cond
	     ((gp-pair? y s)
	      (string-it y s #f (format #f "~a|" (gp-car x s)) ")>"
	       (format #f "~a ~a" (gp-car x s) (lp y))))
	     ((null? y)
	      (format #f "~a)>" (gp-car x s)))
	     (else
	      (format #f "~a|~a)>" (gp-car x s) y)))
	    (format #f "~a|~a)>" (gp-car x s) y))))))


(define recursive-handler (make-fluid vlist-null))
(define recursive-i       (make-fluid -1))

(define (gp-printer port x)
  (define (f port l d)
    (if (eq? (length l) 1)
        (format port "<#gp {~{~a~}~a}>" l d)
        (format port "<#gp {~a ~{ ~a~}~a}>" (car l) (cdr l) d)))

  (define (m x)
    (if (pair? x)
	(format #f "~a : ~a" (car x) (cdr x))
	x))

  (if ((@@ (logic guile-log code-load) gp-ok?) x)
      (let ((s (fluid-ref *current-stack*)))
	(let ((x (glup x s)))
	  (if (gp? x)
	      (cond
	       ((gp-attvar-raw? x s)
		(string-it x s port "" ""
			   (att-printer #f x s)))

	       ((gp-pair? x s)
            (string-it x s port "" ""
		       (format #f "#<(~a" (get-line s x))))
	     
	       (else
		(let ((varn (gp-var-number x s)))
		  (format port "<#~a>" (m varn)))))
	      (format port "<#gp ~a>" x))))
      (format port "<#gp NULL>")))
  
(define gp-var-ref 
  (case-lambda 
    ((x)   (gp-lookup x (fluid-ref *current-stack*)))
    ((x s) (gp-lookup x s))))

        
(define (gp-dynwind pre action post s)
  (dyn pre post s)
  (action))


;;prompts will be just a continuation lambda


 
(define-syntax id-12345 
  (syntax-rules () 
    ((a x s) (gp-lookup x s))
    ((a x)   x)))

(define-syntax-rule (ppair? x s)
  (if (pair? x) s #f))

(define-syntax-rule (nnull? x s)
  (if (null? x) s #f))
(define-syntax-rule (eequal? x y s)
  (if (equal? x y) s #f))

(define-syntax-rule (ccar x s) (car x))
(define-syntax-rule (ccdr x s) (cdr x))
(define-syntax-rule (uset x s v) (gp-set! x v s))
(define-syntax-rule (uset-car x s v) (gp-var-set (gp-car x s) v s))
(define-syntax-rule (uset-cdr x s v) (gp-var-set (gp-cdr x s) v s))
(define-syntax-rule (cset-car x s v) (gp-set! (car x) v s))
(define-syntax-rule (cset-cdr x s v) (gp-set! (cdr x) v s))
(define-syntax-rule (cpair? x s) 
  (if (pair? x) 
      (values (car x) (cdr x) s)
      (values #f #f #f)))

(define-syntax-parameter _gp-unify! (identifier-syntax gp-unify!))
(define-syntax-parameter _gp-pair+  (identifier-syntax gp-pair+))
(define-syntax-parameter _gp-null!? (identifier-syntax gp-null!?))

(make-phd-matcher umatch0
  (        (gp-car gp-cdr _gp-pair+ _gp-null!?   _gp-unify!     id-12345 
		   (uset-car uset-cdr uset))
  (    (+  (gp-car gp-cdr _gp-pair+ _gp-null!?   _gp-unify!     id-12345 
		   (uset-car uset-cdr uset)))
       (+r (gp-car gp-cdr _gp-pair+ _gp-null!?   gp-unify!-     id-12345 
		   (uset-car uset-cdr uset)))
       (++ (gp-car gp-cdr gp-pair+ gp-null!?   gp-unify-raw! id-12345 
		   (uset-car uset-cdr uset)))
       (++r (gp-car gp-cdr gp-pair+ gp-null!?   gp-unify-raw!- id-12345 
		    (uset-car uset-cdr uset)))
       (-  (gp-car gp-cdr gp-pair- gp-null?    gp-m-unify!   id-12345 
		   (uset-car uset-cdr uset)))
       (-r  (gp-car gp-cdr gp-pair- gp-null?    gp-m-unify!-   id-12345 
		    (uset-car uset-cdr uset)))
       (*  ( ccar  ccdr  cpair?  nnull?  eequal? id-12345 
		   (cset-car cset-cdr uset))))))

(define-syntax-rule (clear-fkn) #f)

(define delayers (@@ (logic guile-log code-load) *delayers*))

(define-syntax umatch
  (lambda (x)
    (syntax-case x ()
      ((umatch . l) 
       (with-syntax ((w (datum->syntax (syntax l) '*gp-fi*)))
         (syntax 
	  (let ()
	    (define-syntax sc (lambda (x) #'(fluid-ref *current-stack*)))
	    (umatch* (fluid-ref delayers) #f sc "anon" w #f * . l))))))))

;;unsyntax construct that works quite ok
(define (pp x) (pretty-print x) x)


(define-syntax umatch* 
  (lambda (x)
    (syntax-case x ()
      ((umatch* qq dd ss nn tt rr mm (#:clear cl . u) . l)
       (syntax (umatch* cl d ss nn tt rr mm u . l)))

      ((umatch* cl dd ss nn tt rr mm (#:dual d . u) . l)
       (syntax (umatch* cl d ss nn tt rr mm u . l)))

      ((umatch* cl dd ss nn tt rr mm (#:dual d . u) . l)
       (syntax (umatch* cl d ss nn tt rr mm u . l)))

      ((umatch* cl dd ss nn tt rr mm (#:status s . u) . l)
       (syntax (umatch* cl dd s nn tt rr mm u . l)))

      ((umatch* cl dd ss nn tt rr mm (#:name n . u) . l)
       (syntax (umatch* cl dd ss n tt rr mm  u . l)))

      ((umatch* cl dd ss nn tt rr mm (#:tag    t . u) . l)
       (syntax (umatch* cl dd ss nn t rr mm u . l)))

      ((umatch* cl dd ss nn tt rr  mm (#:raw . u)    . l)
       (syntax (umatch* cl dd ss nn tt #t mm u . l)))

      ((umatch* cl dd ss nn tt rr mm (#:mode   m . u) . l)
       (syntax (umatch* cl dd ss nn tt rr m u . l)))

      ((umatch* cl d s n t r m () args a ...) 
       (syntax (umatch** (a ...) () args (cl d s n t r m)))))))


(define-syntax umatch**
  (lambda (x)
    (syntax-case x ()
      ((qq . l) 
       ;(pk `(umatch** ,@(syntax->datum #'l)))
       #'(umatch**+ . l)))))

(define-syntax umatch**+
  (syntax-rules ()
    ((qq ((code) ...)  a . l)             
     (umatch*** (code ...) a . l))

    ((qq ((a as ...) ...) () . l)   
     (umatch** ((as ...) ...) ((a) ...) . l))

    ((qq ((a as ...) ...) ((b ...) ...) . l)   
     (umatch** ((as ...) ...) ((b ... a) ...) . l))))


(define-syntax umatch***
  (lambda (x)
    (syntax-case x ()
      ((qq . l) 
       ;(pk `(umatch*** ,@(syntax->datum #'l)))
       #'(umatch***+ . l)))))

(define-syntax-rule (mk-failure0 fr code)
  (lambda ()
    (gp-unwind fr)
    (code)))

(define-syntax-rule (mk-failure0-l fr code)
  (lambda ()
    (gp-unwind fr)
    (code)))



(define-syntax mk-failure
  (syntax-rules ()
    ((qq fr del code)
     (mk-failure0 fr (lambda () (fluid-set! delayers del) code)))))

(define-syntax mk-failure-l
  (syntax-rules ()
    ((qq fr del code)
     (mk-failure0-l fr (lambda () (fluid-set! delayers del) code)))))

(define-syntax mk-failure-
  (syntax-rules ()
    ((qq fr code)
     (lambda () (code)))))
(define (er x) (error x))
(define-syntax umatch***+
  (syntax-rules (+)
    ((qq a b c (_ _ #f . l))
     (er (format #f "umatch: #f in state position: ~a" '(a b c))))

    ((qq (code1 code2) () () (dels #t s n t _ _))
       (umatch0 s (#:args) 
                ((arguments) (-> t (mk-failure-l s dels)) 
                 code1)

                ((arguments) (-> t (mk-failure- s))		 
                 code2)
                (_ (er (format #f "umatch ~a did not match" n)))))

    ((qq (code) () () (dels _ s n t _ _))
       (umatch0 s (#:args) 
                ((arguments) (-> t (mk-failure- s dels)) 
                 code)
                (_ (er (format #f "umatch ~a did not match" n)))))

    ((qq (codes ... code1 code2) () () (dels #t s n t _ _))
     (let ((s (gp-newframe-choice s)))
       (umatch0 s (#:args) 
                ((arguments) (-> t (mk-failure s dels)) 
                 codes)
                ...
                ((arguments) (-> t (mk-failure-l s)) 
                 code1)
                ((arguments) (-> t (mk-failure- s)) 
                 code2)
                (_ (er (format #f "umatch ~a did not match" n))))))

    ((qq (codes ... code) () () (dels _ s n t _ _))
     (let ((s (gp-newframe-choice s)))
       (umatch0 s (#:args) 
                ((arguments) (-> t (mk-failure s dels)) 
                 codes)
                ...
                ((arguments) (-> t (mk-failure- s)) 
                 code)

                (_ (er (format #f "umatch ~a did not match" n))))))


    ((qq (code) ((a ...)) arg (dels _ s n t #t +))
     (umatch0 s (#:args . arg) 
              ((arguments (++ ++ a) ...) 
               (-> t  (mk-failure- s))                   
               code)
              (_ (er (format #f "umatch ~a did not match" n)))))

    ((qq (code1 code2) ((a ...)) arg (dels #t s n t #t +))
       (umatch0 s (#:args . arg) 
              ((arguments (++ ++ a) ...) 
               (-> t  (mk-failure-l s dels))
              ((arguments (++ ++ a) ...) 
               (-> t  (mk-failure- s))                   
               code)               
              (_ (er (format #f "umatch ~a did not match" n))))))

    

    ((qq (code) ((a ...)) arg (dels _ s n t #t +))
     (let ((sold s))
       (umatch0 s (#:args . arg) 
                ((arguments (++ ++ a) ...) (-> t (mk-failure- s))            
                 (gp-clear-frame)
                 (let ((s sold)) code))
                (_ (er (format #f "umatch ~a did not match" n))))))

    ((qq (code1 code2) ((a1 ...) (a2 ...)) arg (dels #t s n t #t +))
     (let ((sold s)
           (s    (gp-newframe-choice s)))         
       (umatch0 s (#:args . arg) 
                ((arguments (++ ++ a1) ...) (-> t (mk-failure-l s dels))
                 (gp-clear-frame)
                 (let ((s sold)) code))
                ((arguments (++ ++ a2) ...) (-> t (mk-failure- s))            
                 (gp-clear-frame)
                 (let ((s sold)) code))
                (_ (er (format #f "umatch ~a did not match" n))))))

    ((qq (codes ... code1 code2) ((as ...) ... (a1 ...) (a2 ...)) arg 
	(dels #t s n t #t +))
     (let ((sold s)
           (s    (gp-newframe-choice s)))
       (umatch0 s (#:args . arg) 
                ((arguments (++ ++ as) ...) 
                 (-> t  (mk-failure s dels))
                   
                 codes)
                ...
		((arguments (++ ++ a1) ...) (-> t (mk-failure-l s))            
                 (gp-clear-frame)
                 (let ((s sold)) code1))
        
                ((arguments (++ ++ a2) ...) (-> t (mk-failure- s))            
                 (gp-clear-frame)
                 (let ((s sold)) code2))
                (_ (er (format #f "umatch ~a did not match" n))))))

    ((qq (codes ... code) ((as ...) ... (a ...)) arg (dels _ s n t #t +))
     (let ((sold s)
           (s    (gp-newframe-choice s)))
       (umatch0 s (#:args . arg) 
                ((arguments (++ ++ as) ...) 
                 (-> t  (mk-failure s dels))                   
                 codes)
                ...
                ((arguments (++ ++ a) ...) (-> t (mk-failure- s))            
                 (gp-clear-frame)
                 (let ((s sold)) code))
                (_ (er (format #f "umatch ~a did not match" n))))))

    
    ((qq (code) ((a ...)) arg (_ _ s n t #t +))
     (let ((sold s)
           (s    (gp-newframe-choice s)))
       (umatch0 s (#:args . arg) 
                ((arguments (++ ++ a) ...) 
		 (-> t  (mk-failure- s))
		 (let ((s sold))
		   (gp-clear-frame))
                 code)
                (_ (er (format #f "umatch ~a did not match" n))))))

    ((qq (code1 code2) ((a1 ...) (a2 ...)) arg (_ #t s n t #t +))
     (let* ((sold s)
	    (s    (gp-newframe-choice s)))
       (umatch0 s (#:args . arg) 
                ((arguments (++ ++ a1) ...) 
		 (-> t  (mk-failure-l s))
		 (let ((s sold))
		   (gp-clear-frame)
		   code1))

                ((arguments (++ ++ a2) ...) 
		 (-> t  (mk-failure- s))
		 (let ((s sold))
		   (gp-clear-frame)
		   code2))

                (_ (er (format #f "umatch ~a did not match" n))))))
    
    ((qq (codes ... code1 code2) ((as ...) ... (a1 ...) (a2 ...))
	arg (dels #t s n t #t +))
     (let ((sold s)
           (s    (gp-newframe-choice s)))
       (umatch0 s (#:args . arg) 
                ((arguments (++ ++ as) ...) 
                 (-> t  (mk-failure s dels))
                   
                 code)
                ...
		((arguments (++ ++ a1) ...) 
                 (-> t  (mk-failure-l s))
                 (gp-clear-frame)
                 (let ((s sold)) code1))
        
                ((arguments (++ ++ a2) ...) 
                 (-> t  (mk-failure- s))
                 (gp-clear-frame)
                 (let ((s sold)) code2))
                (_ (er (format #f "umatch ~a did not match" n))))))

    ((qq (codes ... code) ((as ...) ... (a ...)) arg (dels _ s n t #t +))
     (let ((sold s)
           (s    (gp-newframe-choice s)))
       (umatch0 s (#:args . arg) 
                ((arguments (++ ++ as) ...) 
                 (-> t  (mk-failure s dels))
                   
                 code)
                ...
                ((arguments (++ ++ a) ...) 
                 (-> t  (mk-failure- s))
                 (gp-clear-frame)
                 (let ((s sold)) code))
                (_ (er (format #f "umatch ~a did not match" n))))))
    
    ((qq (code) ((a ...)) arg (_ _ s n t r m))
     (umatch0 s (#:args . arg) 
              ((arguments (m m a) ...) 
               code)
              (_ (error (format #f "umatch ~a did not match" n)))))

    ((qq (code) ((a ...)) arg (_ _ s n t r m))
     (let ((sold s)
           (s   (gp-newframe-choice s)))
       (umatch0 s (#:args . arg) 
                ((arguments (m m a) ...) 
                 (let ((s sold))
                   (gp-clear-frame)
                   code))
                (_ (er (format #f "umatch ~a did not match" n))))))

    ((qq (code1 code2) ((a1 ...) (a2 ...)) arg (dels #t s n t r m))
     (let ((sold s)
           (s   (gp-newframe-choice s)))
       (umatch0 s (#:args . arg) 
                ((arguments (m m a1) ...) 
                 (-> t (mk-failure-l s dels))
		 (let ((s sold))
		   (gp-clear-frame)
		   code1))

                ((arguments (m m a2) ...)
		 (let ((s sold)) 
		   (gp-clear-frame)
		   code2))
                (_ (er (format #f "umatch ~a did not match" n))))))

    ((qq (codes ... code1 code2) ((as ...) ... (a1 ...) (a2 ...)) arg
	(dels #t s n t r m))
     (let ((sold s)
           (s   (gp-newframe-choice s)))
       (umatch0 s (#:args . arg) 
                ((arguments (m m as) ...) 
                 (-> t (mk-failure s dels))
                 codes)
                ...
		((arguments (m m a1) ...) 
                 (-> t (mk-failure-l s dels))
                 (let ((s sold))
                   (gp-clear-frame)
                   code1))
        
                ((arguments (m m a2) ...) 
                 (let ((s sold))
                   (gp-clear-frame)
                   code2))
                (_ (er (format #f "umatch ~a did not match" n))))))

    ((qq (codes ... code) ((as ...) ... (a ...)) arg (dels _ s n t r m))
     (let ((sold s)
           (s   (gp-newframe-choice s)))
       (umatch0 s (#:args . arg) 
                ((arguments (m m as) ...) 
                 (-> t (mk-failure s dels))
                 codes)
                ...
                ((arguments (m m a) ...) 
                 (let ((s sold))
                   (gp-clear-frame)
                   code))
                (_ (er (format #f "umatch ~a did not match" n))))))))


(define gp-restore-state-raw (@ (logic guile-log code-load) gp-restore-state))

(define *windlevel* (gp-make-var 1))
((@@ (logic guile-log code-load) gp-set-tester) *windlevel*)

(define (gp-new-wind-level s)
  (let ((r (gp-lookup *windlevel* s)))
    (let ((s (gp-var-set *windlevel* (+ r 1) s)))
      (values s (+ r 1)))))

(define *postpone-level* (gp-make-var 1000000))

(define (gp-new-postpone-level s)
  (let ((r (gp-lookup *postpone-level* s)))
    (gp-var-set *postpone-level* (+ r 1) s)
    (values
     s
     (+ r 1))))

(define-syntax-rule (gp-with-wind s wind code ...)
  (call-with-values (lambda () (gp-new-wind-level s))
    (lambda (s wind) 
      code ...)))

(define (gp-windlevel-ref s) (gp-lookup *windlevel* s))

(define (gp-restore-state x)
  (gp-restore-state-raw (cdr x) #t))

(define (use-logical s)
  (gp-dynwind
   (lambda ()  
     (gp-logical++))
   (lambda ()
     (gp-logical++))
   (lambda (x) 
     (gp-logical--))
   s))

(define (leave-logical s)
  (gp-dynwind
   (lambda () 
     (gp-logical--))
   (lambda ()
     (gp-logical--))
   (lambda (x)
     (gp-logical++))
   s))

(define gp-member -gp-member)

(define gp-right-of -gp-right-of)
(define gp-einstein -einstein)
(define gp-next-to  -next-to)

(define *cp-constructors* (make-fluid '()))
(define (gp-cp . x) 
  (fluid-set! *cp-constructors* '())
  (apply (fluid-ref *gp-cp*) x))
  
(define (attribute-cstor-ref id)
  (let ((res (object-property id 'attribute-cstor)))
    (if res
	(cdr res)
	res)))

(define (attribute-cstor-repr id)
  (let ((res (object-property id 'attribute-cstor)))
    (if res
	(car res)
	res)))

(define (set-attribute-cstor! id . val)
  (set-object-property! id 'attribute-cstor val))

(define do-attribute-constructors 
  (case-lambda 
    ((s p cc l)
     (let lp ((l l) (s s) (p p))
       (if (pair? l)
           ((caar l) s p (lambda (s p) (lp (cdr l) s p)) (cdar l))
           (cc s p))))

    ((s p cc)
     (let lp1 ((l (fluid-ref *cp-constructors*)) (s s) (p p))
       (if (pair? l)
           (let ((var (car l)))
             (if (gp-attvar-raw? var s)
                 (let ((data (gp-att-data var s)))
                   (let lp2 ((data data) (s s) (p p))
                     (if (pair? data)
                         (let ((cstr (attribute-cstor-ref (caar data))))
                           (cstr s p (lambda (s p . x)
                                       (lp2 (cdr data) s p))
                                 var))
                         (lp1 (cdr l) s p))))
                 (lp1 (cdr l) s p)))
           (cc s p))))))

(define (ref-attribute-constructors s)
  (let lp1 ((l (fluid-ref *cp-constructors*)) (r '()))
    (if (pair? l)
	(let ((var (car l)))
	  (if (gp-attvar-raw? var s)
	      (let ((data (gp-att-data var s)))
		(let lp2 ((data data) (r r))
		  (if (pair? data)
		      (let ((cstr (attribute-cstor-ref (caar data))))
			(lp2 (cdr data) (cons (cons cstr var)
					      r)))
		      (lp1 (cdr l) r))))
	      (lp1 (cdr l) r)))
	(reverse r))))
		
(define *gp->scm* (make-fluid gp->scm-))
(define gp->scm
  (case-lambda
   ((x s)
    ((fluid-ref *gp->scm*) x s))
   (x
    (apply (fluid-ref *gp->scm*) x))))

(define recurs-map 
  (let ((r (make-fluid (make-hash-table))))
    (case-lambda
     (() 
      (fluid-ref r))
     ((x) 
      (fluid-set! r x)))))

(define (gp-cp+ . l) (apply gp-cp++ #f  l))
(define *gp-cp* (make-fluid gp-cp+))
(define (gp-scm+ x s) (gp-cp++ #t x s))
(define (gp-scm-unpin x s) (gp-cp++ #t x '() s #t))

(define gp-cp++
(case-lambda 
 ((scm? x s ) 
  (gp-cp++ scm? x '() s #f))
 ((scm? x l s) 
  (gp-cp++ scm? x l s #f))
 ((scm? x l s unpin?)
  (define vs (gp->scm- l s))
  (define tr (make-hash-table))
  (define datas '())
  (define first-map (make-hash-table))
  (define ii 1)
  (define (pp x)
    (when (< ii 10)
	(pk 'gp->scm x)
	(pk 'gp->scm (object-address x)))
    (set! ii (+ ii 1))
    x)

  (define (get    v)
    (if scm? v (hashq-ref tr v #f)))
  
  (define (update v w)
    (if scm? #f (hashq-set! tr v w)))

  (define (id x) x)

  (define i 0)
  (let ((res
         (let lp ((x x))
           (let ((x (gp-lookup x s)))
             (let lp2 ((first? #t))	
               (cond
		((or (null? x) (number? x) (string? x) (procedure? x))
		 x)

		((and (< i 30) (begin (set! i (+ i 1)) (gp-pair? x s)))
                 (cons (lp (gp-car x s))
                       (lp (gp-cdr x s))))       
		
                ((begin (set! i 50) (and (hashq-ref (recurs-map) x #f) first?))
                 (let ((d (hashq-ref first-map x #f)))
                   (if d
                       d
                       (let ((data (make-variable #f)))
                         (hashq-set! first-map x data)
                         (set! datas (cons (cons data (lp2 #f)) datas))
                         data))))

		((gp-pair? x s)
                 (cons (lp (gp-car x s))
                       (lp (gp-cdr x s))))
       
                ((gp-attvar-raw? x s)
                 (cond
                  (scm?
                   x)

                  ((memq x vs)
                   x)

                  ((get x) =>
                   id)

                  (else
                   (let* ((w (gp-make-var))
                          (l (let lp2 ((data (gp-att-data x s)))
                               (if (pair? data)
                                   (let ((id (caar data)))
                                     (if (or #t (not (attribute-cstor-ref id)))
                                         (let ((fr (gp-newframe s)))
                                           (id
                                            s
                                            (lambda ()
                                              (gp-unwind fr)
					      (cons
					       (lp (car data))
					       (lp2 (cdr data))))
                                            (lambda (ss p x)
                                              (gp-unwind fr)
                                              (gp-put-attr!
                                               w id x s)
                                              (lp2 (cdr data)))
                                            (cdar data) lp "cp"))
                                         (lp2 (cdr data))))
                                   '()))))
                     
                     (let lp ((l l))
                       (if (pair? l)
                           (begin
                             (gp-put-attr! w (caar l) (cdar l) s)
                             (lp (cdr l)))))
                     
                     (fluid-set! *cp-constructors* 
                                 (cons w
                                       (fluid-ref *cp-constructors*)))
                     
                     (update x w)
                     w))))
                                
                ((gp-var? x s)
                 (cond
                  (scm?
                   (begin
                     (when unpin? (gp-mark-permanent x))
                     x))		
                  ((memq x vs)
                   x)
                   
                  ((get x) =>
                   id)
                   
                  (else
                   (let ((w (gp-make-var)))
                     (update x w)
                     w))))
       
                ((vector? x)
                 (apply vector (map lp (vector->list x))))
       
                ((prolog-closure? x)
                 (let ((parent (prolog-closure-parent x))
                       (l      (lp (prolog-closure-state x))))          
                   (make-prolog-closure
                    (apply parent l)
                    parent
                    l
                    (prolog-closure-closed? x))))
       
                ((namespace? x)
                 (let ((val (lp (namespace-val x))))
                   (if (namespace? x)
                       x
                       (make-namespace
                        val
                        (namespace-ns x)
                        (namespace-local? x)
                        (namespace-lexical? x)))))

                ((variable? x)
                 (lp (variable-ref x)))

                (else
                 x)))))))

    (if (pair? datas)
	(let lp ((l datas) (s s))
	  (if (pair? l)
	      (begin
		(variable-set! (caar l) (cdar l))
		(lp (cdr l)  s))
	      res))
	res)))))

(define (get-free-variables-map x s)
  (define tr (make-hash-table))
  (let lp ((x x))
    (let ((x (gp-lookup x s)))
      (cond
       ((gp-pair? x s)
        (begin
          (lp (gp-car x s))
          (lp (gp-cdr x s))))
       ((gp-var? x s)
        (if (not (hashq-ref tr x #f))
            (hashq-set! tr x #t)))
       ((vector? x)
        (let lp2 ((i (- (vector-length x) 1)))
          (if (>= i 0)
              (begin 
                (lp (vector-ref x i))
                (lp2 (- i 1))))))       


       ((prolog-closure? x)
        (lp (prolog-closure-state x)))

       ((namespace? x)
        (lp (namespace-val x)))

       (else #f))))

  tr)

(define (gp-get-id-free fixed forced-free s)
  (define h (make-hash-table))
  (define o (make-hash-table))
  (let lp ((v forced-free))
    (if (pair? v)
	(begin
	  (hashq-set! h (car v) #t)
	  (lp (cdr v)))))
  (let lp ((x fixed))
    (let ((x (gp-lookup x s)))
      (if (hashq-ref h x #f)
	  #f
	  (cond
	   ((gp-var? x s)
	    (hashq-set! o x #t))
	   ((gp-pair? x s)
	    (lp (gp-car x s))
	    (lp (gp-cdr x s)))
	   ((vector? x)
	    (for-each lp (vector->list x)))
	   (else
	    #f)))))
  (hash-fold
   (lambda (k v s) (cons k s))
   '() o))
	     
	  
	

(define (gp-get-fixed-free fixed forced-free s)
  (let ((tr-fixed (get-free-variables-map fixed s))
	(tr-free  (get-free-variables-map forced-free s)))
    (hash-fold (lambda (k v l)
		 (if (hashq-ref tr-free k #f)
		     l
		     (cons k l)))
	       '()
	       tr-fixed)))


(define (gp-rebased-level-ref v)
  (let ((h  (gp-handlers-ref)))
    (if (null? h)
	v
	(cons v h))))

;;Use this to track unhealthy uses of unwind frames
#;
(begin
  (define *unmap* (make-weak-key-hash-table))
  (define (gp-newframe s)
    (let ((unwinded (make-variable #f)))
      (if (fluid? s)
	  (set! s (fluid-ref s)))
      (gp-dynwind 
       (lambda x (variable-set! unwinded #f))
       (lambda x (variable-set! unwinded #f))
       (lambda x (variable-set! unwinded #t))
       s)
     
      (let ((u (newf s)))
	;(pk 'u u)
	(hashq-set! *unmap* u unwinded)
	u)))


  (define (gp-unwind fr)
    (if (variable-ref (hashq-ref *unmap* fr))
	(error "unwinding an already unwinded value"))
    (unw fr)))

(set! (@@ (logic guile-log slask) gp-unwind  ) gp-unwind  )
(set! (@@ (logic guile-log slask) gp-newframe) gp-newframe)

(define (gp-store-state s)
  (let ((ss (gp-newframe s)))
    (cons ss ((@@ (logic guile-log code-load) gp-store-state) ss))))
(define (gp-store-state-all s)
  (let ((ss (gp-newframe s)))
    (cons ss ((@@ (logic guile-log code-load) gp-store-state-all) ss))))

(define (gp-restore-wind state fr)
  (gp-restore-state-raw (cdr state) fr))
