(define-module (logic guile-log prolog pre)
  #:use-module (ice-9 match)
  #:use-module (ice-9 pretty-print)
  #:export (get.. get-c pp get-binding get-refstr *prolog-file*
		  *closure-creations*))

(define pp
  (case-lambda
   ((s x)
    (pretty-print `(,s ,(syntax->datum x)))
    x)
   ((x)
    (pretty-print (syntax->datum x))
    x)))

(define (get.. op l)
  (match l
   (((_ _ (? (lambda (x) (equal? op x))) _) x y _ _)
    (cons x (get.. op y)))
   (x (list x))))

(define (get-c f l)
  (define (get0 f x)
    (match x      
      (((_ _ "," _) x y _ _)
        (cons (f x) (get0 f y)))
      (x (list (f x)))))

  (match l
   (((_ _ "|" _) x y _ _)
    (let lp ((l (get0 f x)))
      (if (pair? l)
	  (cons (car l) (lp (cdr l)))
	  (f y))))

   (((_ _ "," _) x y _ _)
    (cons (f x) (get0 f y)))
  
   (x (list (f x)))))
   
(define *prolog-file* (make-fluid #f))

(define (get-refstr N M)
  (if (fluid-ref *prolog-file*)
      (format #f "~a:(~a,~a)" (fluid-ref *prolog-file*)      M N)
      (format #f "~a:(~a,~a)" (module-name (current-module)) M N)))
            
(define *closure-creations* (make-fluid (make-hash-table)))
