(define-module (logic guile-log prolog compile2)
  #:use-module (logic guile-log procedure-properties)
  #:use-module (logic guile-log guile-prolog closure)
  #:use-module (logic guile-log guile-prolog copy-term)
  #:use-module (ice-9 pretty-print)
  #:use-module (ice-9 match)
  #:use-module (ice-9 time)
  #:use-module (logic guile-log)
  #:use-module (logic guile-log functional-database)
  #:use-module ((logic guile-log prolog names)
                #:select (! fail true false
			    prolog-and prolog-or prolog-not prolog=..))
  #:use-module (logic guile-log prolog goal-functors)
  #:use-module (system base compile)
  #:use-module (system base language)
  #:use-module ((logic guile-log umatch)
                #:select (gp-attvar-raw? gp-att-data gp-att-raw-var
					 gp-make-var gp-var? gp-lookup))
  #:export (compile-prolog))

(define this-mod (resolve-module '(logic guile-log prolog compile2)))

(define (default-extensions)
  (list prolog-and 'and prolog-or 'or prolog-not 'not prolog=.. '=..))

(define (simplify x)
  (datum->syntax #'simple-stx (syntax->datum x)))

(define do-print #f)
(define pp
  (case-lambda
   ((s x)
    (when do-print
      (pretty-print `(,s ,(syntax->datum x))))
    x)
   ((x)
    (when do-print
      (pretty-print (syntax->datum x)))
    x)))

(define ppp
  (case-lambda
  ((s x)
    (when #t
      (pretty-print `(,s ,(syntax->datum x))))
    x)
   ((x)
    (when #t
      (pretty-print (syntax->datum x)))
    x)))

(define (make-vars n)
  (let lp ((i 0) (r '()))
    (if (< i n)
	(lp (+ i 1) 
	    (cons
	     ((@ (logic guile-log umatch) gp-make-var))
	     r))
	r)))

(define (make-vars-v n)
  (list->vector (make-vars n)))

(define varn    (gensym "fkns"   ))
(define invarn  (gensym "inhouse"))
(define exvarn  (gensym "exhouse"))
(define svarn   (symbol->string varn  ))
(define sinvarn (symbol->string invarn))
(define sexvarn (symbol->string exvarn))

(define (ident? x)
  (or (char? x)
      (boolean? x)
      (string? x)
      (null?   x)
      (symbol? x)
      (number? x)))

(define-syntax-rule (G  x) 'x)
(define-syntax-rule (GL x) 'x)
(define-syntax-rule (UM x) 'x)
(define-syntax-rule (FU x) 'x)
(define-syntax-rule (C  c) 'x)
(define-syntax-rule (lumelunda code)
  (let ((action (lambda () code))
	(f      #f))
    (lambda x
      (if (not f) (set! f (action)))
      (apply f x))))

(define is-compile-all (make-fluid #f))
(define (compile-prolog s a f source? extention?)
  (define meta-only? (and (pair? extention?)
			  (eq? (car extention?) #t)
			  (cadr extention?)))
  
  (define fast-compile?
    (and (pair? extention?)
	 (eq? (car extention?) #t)
	 (not (cadr extention?))))

  (define in-house (make-hash-table))
  (define ex-house (make-hash-table))
  (define ex-ex    (make-hash-table))
  (define fkns     (make-hash-table))

  (define isSource #f)
  (define ifkn 0)
  (define (add-fkn x)
    (let ((r (hashq-ref fkns x #f)))
      (unless r
	(if (procedure? x)
	    (let ((n   (procedure-name x))
		  (mod (procedure-property x 'module)))
	      (if (not mod)
		  (set! mod (module-name (current-module))))
	      (hashq-set! fkns x (list `(list       ,svarn ,ifkn)
				       `(vector-ref ,varn  ,ifkn)
				       ifkn
				       `(@@ ,mod ,n)))
	      (set! ifkn (+ ifkn 1)))))))

  (define (get-fkn x)
    (let ((r (hashq-ref fkns x #f)))
      (if r 
	  (if isSource
	      (list-ref r 0)
	      (list-ref r 1))
	  (error "BUG in get-fkn"))))

  (define (get-var x)
    (let ((r (hashq-ref ex-house x #f)))
      (if r
	  (if isSource
	      (list-ref r 0)
	      (list-ref r 2))
	  (let ((r (hashq-ref in-house x #f)))
	    (if r
		(if isSource
		    (list-ref r 0)
		    (error "no in-house in external match"))
		(error "did not find r"))))))

  (define i-inhouse 0)
  (define i-exhouse 0)

  (define (add-var x)
    (let ((r1 (hashq-ref ex-house x #f))
          (r2 (hashq-ref in-house x #f)))     
      (unless (or r1 r2)
         (hashq-set! in-house x 
		     (list (list 'list sinvarn i-inhouse)
			   i-inhouse))
	 (set! i-inhouse (+ i-inhouse 1)))))

  (define (add-exvar x)
    (let ((r1 (hashq-ref ex-house x #f)))
      (unless r1
	(let* ((s (gensym "MATCHVAR"))
	       (l (list (list 'list sexvarn i-exhouse)
			i-exhouse
			s)))
	  (set! i-exhouse (+ i-exhouse 1))		       
	  (hashq-set! ex-ex    s l)
	  (hashq-set! ex-house x l)))))
	  


  (define (map* f l)
    (umatch (#:mode - #:status s #:name map*) (l)       
      ((x . l)
       (cons (f x) (map* f l)))
      (() '())
      (x  (f x))))

  (define (for-each* f l)
    (umatch (#:mode - #:status s #:name for-each*) (l)      
      ((x . l)
       (begin
         (f x) 
         (for-each* f l)))
      (() '())
      (x (f x))))

  (define match-map   #f)
  (define match-map-o #f)
  (define match-map-i #f)
  (define-syntax-rule (recur-search x code)
    (let* ((x (gp-lookup x s))
	   (r (hashq-ref match-map x #f)))
      (if r
	  (if (eq? r #t)
	      (hashq-set! match-map x (gensym "REQ")))
	  (begin
	    (hashq-set! match-map x #t)
	    code))))

  
  (define (scan-var add-var x)
    (umatch (#:mode - #:status s #:name scan-var) (x)       
     (#(#:brace x)
      (scan-var add-var x))
     (#((f . a))
      (begin 
        (recur-search x
	 (cond
	  ((and (struct? f) (prolog-closure? f))
	   (add-fkn (prolog-closure-parent f))
	   (for-each* (lambda (x) (scan-var add-var x)) 
		      (prolog-closure-state f))
	   (for-each* (lambda (x) (scan-var add-var x)) a))         
	  ((procedure? f)
	   (add-fkn f)
	   (for-each* (lambda (x) (scan-var add-var x)) a))
	  (else
	   (for-each* (lambda (x) (scan-var add-var x)) (cons f a)))))))

      ((a . l)
       (recur-search x
	 (begin
	   (scan-var add-var a)
	   (scan-var add-var l))))

      (x
       (let ((x (gp-lookup x s)))
         (cond 
          ((gp-var? x s)
           (add-var x))
	  ((gp-attvar-raw? x s)
	   (if (fluid-ref is-compile-all)
	       (recur-search x
	         (let ((v (gp-att-raw-var x s))
		       (d (gp-att-data x s)))
		   (scan-var add-var v)
		   (scan-var add-var d)))
	       (scan-var add-var (gp-att-raw-var x s))))

	  ((procedure? x)
           (add-fkn x))
          ((and (struct? x) (prolog-closure? x))
	   (recur-search x
	     (for-each
	      (lambda (x)
		(scan-var add-var x))
	      (prolog-closure-state x))))
	  
	   ((not (ident? x))
	    (add-fkn x))
	   (else
	    #t))))))
    
  (define (scan-goal x)
    (umatch (#:mode - #:status s #:name scan-goal) (x)       
      (#(#:brace a)
       (scan-goal a))
      
      (#((f . a))
       (cond
        ((and (struct? f) (prolog-closure? f))
         (add-fkn (prolog-closure-parent f))
         (for-each* scan-goal (prolog-closure-state f))
         (for-each* scan-goal a))         
        ((procedure? f)
         (add-fkn f)
         (for-each* scan-goal a))
	(else
	 (for-each* scan-goal (cons f a)))))

      ((u . v)
       (scan-var add-var x))

      (x
       (cond 
        ((gp-var? x s)
         (add-var x))

	((gp-attvar-raw? x s)
	 (scan-var add-var x))

        ((procedure? x)
         (add-fkn x))

        ((and (struct? x) (prolog-closure? x))
         (add-fkn (prolog-closure-parent x))
         (for-each* scan-goal (prolog-closure-state x)))
        
	(else
         #t)))))

  (define (compile-a match-map-i x)
    (define-syntax-rule (do code)
      (let* ((x (gp-lookup x s))
	     (c (lambda () code))
	     (r1 (hashq-ref match-map-o x #f)))
	(if (and r1 (not (eq? r1 #t)))
	    (if (hashq-ref match-first x #f)
		r1
		(begin
		  (hashq-set! match-first x #t)
		  `(and ,r1 ,(c))))
	    (c))))

    (umatch (#:mode - #:status s #:name compile-a) (x)
       (#(#:brace x)
	(list 'unquote
	      (list (G vector) #:brace 
		    (list (G quasiquote)
			  (compile-a match-map-i x)))))

       (#((f . a))
	(do
	  (list 'unquote
	  (let ((f (gp-lookup f s)))
	    (cond
	     ((and (struct? f) (prolog-closure? f))
	      (list (G vector)
		    (list (G quasiquote)
			  (cons (list 'unquote
				      (cons 
				       (get-fkn (prolog-closure-parent f))
				       (map* (lambda (x) 
					       (compile-a match-map-i x))
					     (prolog-closure-state f))))
				(map* (lambda (x) 
					(compile-a match-map-i x))
				      a)))))
	     (else
	      (list (G vector)
		    (list (G quasiquote)
			  (cons (list 'unquote 
				      (if (procedure? f)
					  (get-fkn f)
					  (get-var a)))

				(map* (lambda (x) 
					(compile-a match-map-i x))
				      a))))))))))

       ((a . l)
        (do
	    (let* ((a  (list (G quasiquote) (compile-a match-map-i a)))
		   (b  (list (G quasiquote) (compile-a match-map-i l)))
		   (r0 (list (G cons) a b)))
	      (list 'unquote r0))))
       (a
	(list 'unquote
	      (let ((a (gp-lookup a s)))
		(cond
		 ((gp-var? a s)
		  (get-var a))
		 ((gp-attvar-raw? a s)
		  ;; TODO This is not working
		  (if (fluid-ref is-compile-all)
		      (let ((raw (compile-a match-map-i (gp-att-raw-var a s)))
			    (l   (compile-a match-map-i (gp-att-data   a s))))
			(vector
			 `((@@ (logic guile-log) create)
			   ,raw ,l)))
		      (list (G quasiquote)
			    (compile-a match-map-i (gp-att-raw-var a s)))))
		 ((procedure? a)
		  (get-fkn a))
		 ((symbol? a)
		  (list (G quote) a))
		 ((null? a)
		 (list 'quote a))
		 ((and (struct? a) (prolog-closure? a))
		  (cons (get-fkn (prolog-closure-parent a))
			(map* (lambda (x) (compile-a match-map-i x))
			      (prolog-closure-state a))))
		 (else x)))))))

  (define (compile-s match-map-i x)
    (umatch (#:mode - #:status s #:name compile-s) (x)
       (#(#:brace x)
	(vector #:brace (compile-s match-map-i x)))
       
       (#((f . a))
	(let ((f (gp-lookup f s)))
	  (cond
	   ((procedure? f)	   
	    (cond
	     ((eq? (object-property f 'prolog-functor-type) #:scm)
	      (cons* 
	       (get-fkn f)
	       (GL S)
	       (map* (lambda (x) 
		       (compile-s match-map-i x))
		     a)))
	     
	     ((eq? (object-property f 'prolog-functor-type) #:goal)
	      (cons*
	       (list '@ '(logic guile-log iso-prolog) (procedure-name f))
	       (map* (lambda (x) 
		       (compile-s match-map-i x))
		     a)))
	       
	     (else
	      (cons* 
	       (get-fkn f)
	       (map* (lambda (x) 
		       (compile-s match-map-i x))
		     a)))))
	   (else
	    (error "copmile-s fkn not a procedure")))))


       (a
	(let ((a (gp-lookup a s)))
	  (cond
	   ((gp-var? a s)
	    (list (GL <lookup>) (get-var a)))
	   ((procedure? a)
	    (get-fkn a))
	   ((symbol? a)
	    (list (G quote) a))
	   (else
	    a))))))
        

  (define (get-goal-types f)
    (object-property f 'goal-compile-types))
              
  (define (get-goal-stub f)
    (object-property f 'goal-compile-stub))

  (define (comp-map* l ll)
    (umatch (#:mode - #:status s #:name comp-map*) (l)       
      ((x . l)
       (match ll
         (('g . ll)
          (cons (compile-goal x) (comp-map* l ll)))
         (('v . ll)
          (cons (compile-a match-map-i x) (comp-map* l ll)))
         (('a . ll)
          (cons (list (G quasiquote) (compile-a match-map-i x))
		(comp-map* l ll)))
         (('s . ll)
          (cons (compile-s match-map-i x)
		(comp-map* l ll)))
         (('ff . ll)
	  ;; This is not used
          (cons (compile-match match-map-i x) (comp-map* l ll)))))
      (() '())))

  (define (compile-goal x)
    (pp 'compgoal x)
    (umatch (#:mode - #:status s #:name compile-goal) (x)
      (#(#:brace a)
       (error "{ ... } is not supported in goal"))

      (#((";" #(("->" x y)) z))
       (list (GL <if>) 
	     (compile-goal x) 
	     (compile-goal y) 
	     (compile-goal z)))
      (#((f . a))
       (let ((f (gp-lookup f s)))
         (cond
          ((and (struct? f) (prolog-closure? f))
           (cons (cons (get-fkn (prolog-closure-parent f))
                       (map* (lambda (x)
			       (list (G quasiquote) 
				     (compile-a match-map-i x)))
			     (prolog-closure-state f)))
                 (map* (lambda (x) 
			 (list (G quasiquote)
			       (compile-a match-map-i x)))
		       a)))

          ((goal-fkn? f)
	   (apply (get-goal-stub f ) (comp-map* a (get-goal-types f))))
          
          ((procedure? f)	   
           (cons 
	    (get-fkn f) 
	    (map* (lambda (x) 
		    (list (G quasiquote)
			  (compile-a match-map-i x)))
		  a)))

          (else
	   (cons* (list (GL <lookup>) (get-var x))
		  (map* (lambda (x) 
			  (list (G quasiquote)
				(compile-a match-map-i x)))
			a))))))

      ((u . v)
       (list (G quasiquote) (compile-a match-map-i x)))

      (x
       (let ((x (gp-lookup x s)))
	 (cond
	  ((gp-var? x s)
	   (list (list (GL <lookup>) (get-var x))))
	  ((eq? x true)
	   (GL <cc>))
	  ((or (eq? x fail) (eq? x false))
	   (GL <fail>))
	  ((eq? x !)
	   (GL <cut>))
	  ((procedure? x)
	   (compile-goal (vector (list x))))
	  ((and (struct? f) (prolog-closure? f))
	   (compile-goal (vector (list x))))
	  (else 
	   (error (format #f "Atom ~a is not allowed as a goal" x))))))))
  
  (define match-first (make-hash-table))
  (define compile-match 
    (case-lambda 
     ((x) (compile-match match-map-o x))
     ((match-map-o x)
    (define-syntax-rule (do code)
      (let* ((x (gp-lookup x s))
	     (c (lambda () code))
	     (r1 (hashq-ref match-map-o x #f)))
	(if (and r1 (not (eq? r1 #t)))
	    (if (hashq-ref match-first x #f)
		r1
		(begin
		  (hashq-set! match-first x #t)
		  `(and ,r1 ,(c))))
	    (c))))

    (pp 'match x)		  
    (umatch (#:mode - #:status s #:name compile-match) (x)
     (#(#:brace a)
      (vector #:brace (compile-match a)))

     (#((f . a))
      (let lp ((first? #t))
	(if first?
	    (if extention? 		
		(let ((r (let ((r (member f extention?)))
			   (if r 
			       (member (cadr r) (default-extensions))
			       r))))
		  (if r
		      (cons (cadr r) (map compile-match a))
		      (let ((r (member f (default-extensions))))
			(if r
			    (cons (cadr r) (map* compile-match a))
			    (lp #f)))))
		(lp #f))	    
	    (if (procedure? f)
		(do (vector `((,'unquote ,(get-fkn f)) 
			      ,@(map* compile-match a))))
		(do (vector (map* compile-match (cons f a))))))))

     ((a . l)
      (do (cons (compile-match a) (compile-match l))))

     (a
      (let ((a (gp-lookup a s)))
	(cond
	 ((gp-var? a s)
	  (get-var a))
	 ((gp-attvar-raw? a s)
	  (do
	      (if (fluid-ref is-compile-all)
		  (let* ((v (gp-att-raw-var a s))
			 (d (gp-att-data a s))
			 (v (compile-match v))
			 (d (compile-match d))
			 (x (gensym "x")))
		    
		    `(and (= (lambda (,x) (,(UM gp-att-raw-var) ,x ,(GL S))) 
			     ,v)
			  (= (lambda (,x) (,(UM gp-att-data)    ,x ,(GL S)))
			     ,d)))		 
		  (compile-match (gp-att-raw-var x s)))))
	 ((procedure? a)
	  `(,(string->symbol "unquote") ,(get-fkn a)))
	 ((symbol? a)
	  `(quote ,a))        
	 ((not (ident? a))
	  `(,(string->symbol "unquote") ,(get-fkn a)))
	 (else
	  a))))))))
  
  (define var-first (make-hash-table))
  (define (compile-var match-map-i x)
    (define-syntax-rule (do x code)
      (let* ((x  (gp-lookup x s))
	     (c  (lambda () code))
	     (r1 (hashq-ref match-map-i x #f)))
	(if (and r1 (not (eq? r1 #t)))
	    (if (hashq-ref var-first x #f)
		r1
		(begin
		  (hashq-set! var-first x #t)
		  `(,(G let) ((,r1 (,(G make-variable) #f)))
		    (,(G variable-set!) ,r1 ,(c))
		    ,r1)))
	    (c))))

    (define (map-cons match-map-i l)
      (umatch (#:mode - #:status s #:name compile-match) ((pp 'compile-var2 l))
       ((a . l)
	(do l
          (list (G cons) (compile-var match-map-i a) (map-cons match-map-i l))))
       (a (compile-var match-map-i a))))

    (umatch (#:mode - #:status s #:name compile-match) ((pp 'compil-var1 x))
      (#(#:brace a)
       (list (G vector) #:brace (compile-var match-map-i a)))

      (#((f . a))
       (do x 
        (let ((f (gp-lookup f s)))
         (cond
          ((and (struct? f) (prolog-closure? f))
           `(,(G vector) (,(G list)
                          ,(cons (get-fkn (prolog-closure-parent f))
                                 (map* (lambda (x)
					 (compile-a match-map-i x))
                                       (prolog-closure-state f)))
                          ,@(map* (lambda (x) 
				    (compile-var match-map-i x))
				  a))))
          (else
	   (let* ((l (map* (lambda (x)
			     (compile-var match-map-i x))
			   a))		  
		  (l (let lp ((l l) (r '()))
		       (if (pair? l)
			   (lp (cdr l) (cons (car l) r))
			   (if (null? l)
			       (reverse (cons (list (G quote) '()) r))
			       (reverse (cons l   r)))))))
			       
	     (list (G vector) (cons* (G cons*) 
				     (if (procedure? f)
					 (get-fkn f)
					 (get-var f))
				     l))))))))
       ((a . l)
       (do x
	   (list (G cons) (compile-var match-map-i a) 
		 (map-cons match-map-i l))))
      
      (a
       (let ((a (pp 'u (gp-lookup a s))))
         (cond
          ((gp-var? a s)
           (get-var a))
	  ((gp-attvar-raw? a s)
           (do a
	     (if (fluid-ref is-compile-all)
		 (let* ((v (gp-att-raw-var a s))
			(d (gp-att-data a s))
			(v (compile-var match-map-i v))
			(d (compile-var match-map-i d)))
		   `(,(UM mak-attvar) ,v ,d))
		 (compile-var match-map-i (gp-att-raw-var a s)))))

          ((procedure? a)
           (get-fkn a))
          ((symbol? a)
           (list 'quote a))
          ((and (struct? a) (prolog-closure? a))
	   (do x
             (cons (get-fkn (prolog-closure-parent a))
		   (map* (lambda (x)
			   (list (G quaeiquote) (compile-a match-map-i x)))
			 (prolog-closure-state a)))))
          ((not (ident? a))
           (get-fkn a))
          ((null? a)
           ''())
          (else
           a))))))
                 
  (set! match-map   (make-hash-table))
  (scan-var add-exvar a)

  (set! match-map-o match-map)
  (set! match-map   (make-hash-table))

  (scan-goal f)

  (set! match-map-i match-map)
  (pp `(compile ,a ,f))
  (let* ((aa    (begin (set! isSource #f)
		       (pp (compile-match a))))
	 (aaa   (begin (set! isSource #t)
		       (pp (compile-var match-map-o a))))
	 (fff   (pp (compile-var match-map-i f)))
	 (vfkn  (pp
		 (map
		 (lambda (x) (list-ref x 3))
		 (sort (hash-fold (lambda (k v r) (cons v r)) '() fkns)
		       (lambda (x y) (< (list-ref x 2) (list-ref y 2)))))))
	 (nvars (length (hash-fold (lambda (k v r) (cons v r)) '() in-house)))
	 (ovar0 (pp
		 (sort
		 (hash-fold (lambda (k v r) (cons v r)) '() ex-house)
		 (lambda (x y)
		   (< (list-ref x 1) (list-ref y 1))))))
	 (ovars (pp
		 (map (lambda (x) (list-ref x 2)) ovar0))))
    
    (datum->syntax #'make-vars
		   (ppp
		    `(let ()
		       (define (f ,invarn ,varn ,@ovars)
			 ((@@ (logic guile-log functional-database)
			      <lambda-dyn-meta>)
			  ,aa 
			  (make-meta 
			   ,(pp ((@ (guile) eval) aaa this-mod))
			   ,(pp ((@ (guile) eval) fff this-mod))
			   ,invarn
			   ,varn
			   (vector ,@ovars))))
		       (apply f
			      (make-vars-v ,nvars) (vector ,@vfkn)
			      (make-vars ,(length ovars))))))))
