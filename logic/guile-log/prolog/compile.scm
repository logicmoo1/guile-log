(define-module (logic guile-log prolog compile)
  #:use-module (logic guile-log procedure-properties)
  #:use-module (logic guile-log guile-prolog closure)
  #:use-module (logic guile-log guile-prolog copy-term)
  #:use-module (ice-9 pretty-print)
  #:use-module (ice-9 match)
  #:use-module (ice-9 time)
  #:use-module (logic guile-log)
  #:use-module ((logic guile-log slask) #:select (include-meta))
  #:use-module (logic guile-log functional-database)
  #:use-module ((logic guile-log prolog names)
                #:select (! fail true false
			    prolog-and prolog-or prolog-not prolog=..))
  #:use-module (logic guile-log prolog goal-functors)
  #:use-module (system base compile)
  #:use-module (system base language)
  #:use-module ((logic guile-log umatch)
                #:select (gp-attvar-raw? gp-att-data gp-att-raw-var
				gp-cp	 gp-make-var gp-var? gp-lookup))
  #:export (compile-prolog))

(define (default-extensions)
  (list prolog-and 'and prolog-or 'or prolog-not 'not prolog=.. '=..))

(define (simplify x)
  (datum->syntax #'simple-stx (syntax->datum x)))

(define do-print #f)
(define pp
  (case-lambda
   ((s x)
    (when do-print
      (pretty-print `(,s ,(syntax->datum x))))
    x)
   ((x)
    (when do-print
      (pretty-print (syntax->datum x)))
    x)))

(define ppp
  (case-lambda
  ((s x)
    (when #t
      (pretty-print `(,s ,(syntax->datum x))))
    x)
   ((x)
    (when #t
      (pretty-print (syntax->datum x)))
    x)))

(define (make-vars n)
  (let lp ((i 0) (r '()))
    (if (< i n)
	(lp (+ i 1) 
	    (cons
	     ((@ (logic guile-log umatch) gp-make-var))
	     r))
	r)))

(define (ident? x)
  (or (char? x)
      (boolean? x)
      (string? x)
      (null?   x)
      (symbol? x)
      (number? x)))

(define-syntax-rule (G  x) '(@ (guile) x))
(define-syntax-rule (I  x) '(@ (ice-9 match) x))
(define-syntax-rule (GL x) '(@ (logic guile-log) x))
(define-syntax-rule (UM x) '(@ (logic guile-log umatch) x))
(define-syntax-rule (FU x) '(@ (logic guile-log prolog goal-functor) x))
(define-syntax-rule (C  c) '(@@ (logic guile-log prolog compile) x))
(define-syntax-rule (lumelunda code)
  (let ((action (lambda () code))
	(f      #f))
    (lambda x
      (if (not f) (set! f (action)))
      (apply f x))))


(define is-compile-all (make-fluid #f))
(define (compile-prolog s a f source? extention?)
  (define meta-only? (and (pair? extention?)
			  (eq? (car extention?) #t)
			  (cadr extention?)))
  
  (define fast-compile?
    (and (pair? extention?)
	 (eq? (car extention?) #t)
	 (not (cadr extention?))))

  (define in-house (make-hash-table))
  (define ex-house (make-hash-table))
  (define fkns     (make-hash-table))
  
  (define (add-fkn x)
    (let ((r (hashq-ref fkns x #f)))
      (unless r
	(if (procedure? x)
	    (let ((n   (procedure-name x))
		  (mod (procedure-property x 'module)))
	      (if (or (not mod))
		  (hashq-set! fkns x (gensym "FKN"))))
	    (hashq-set! fkns x (gensym "FKN"))))))

  (define (get-fkn x)
    (let ((r (hashq-ref fkns x #f)))
      (if r 
	  r
	  (let ((n   (procedure-name x))
		(mod (let ((m (procedure-property x 'module)))
		       (if m
			   (if (pair? m)
			       m
			       (module-name m))
			   m))))
	    (if (and mod n)
		`(@@ ,mod ,n)
		(error (format #f
			       "BUG, prolog compile did not find fkn ~a"
			       x)))))))

  (define (get-var x)
    (let ((r (hashq-ref ex-house x #f)))
      (if r
          r
          (hashq-ref in-house x #f))))

  (define (add-var x)
    (let ((r1 (hashq-ref ex-house x #f))
          (r2 (hashq-ref in-house x #f)))     
      (unless (or r1 r2)
         (hashq-set! in-house x (gensym "VAR-")))))

  (define (add-exvar x)
    (let ((r1 (hashq-ref ex-house x #f)))
      (unless r1
        (hashq-set! ex-house x (gensym "MATCH-VAR-")))))

  (define (map* f l)
    (umatch (#:mode - #:status s #:name map*) (l)       
      ((x . l)
       (cons (f x) (map* f l)))
      (() '())
      (x  (f x))))

  (define (for-each* f l)
    (umatch (#:mode - #:status s #:name for-each*) (l)      
      ((x . l)
       (begin
         (f x) 
         (for-each* f l)))
      (() '())
      (x (f x))))

  (define match-map   #f)
  (define match-map-o #f)
  (define match-map-i #f)
  (define-syntax-rule (recur-search x code)
    (let* ((x (gp-lookup x s))
	   (r (hashq-ref match-map x #f)))
      (if r
	  (if (eq? r #t)
	      (hashq-set! match-map x (gensym "REQ")))
	  (begin
	    (hashq-set! match-map x #t)
	    code))))

  (define (scan-var add-var x)
    (umatch (#:mode - #:status s #:name scan-var) (x)       
     (#(#:brace x)
      (scan-var add-var x))
     (#((f . a))
      (begin 
        (recur-search x
	 (cond
	  ((and (struct? f) (prolog-closure? f))
	   (add-fkn (prolog-closure-parent f))
	   (for-each* (lambda (x) (scan-var add-var x)) 
		      (prolog-closure-state f))
	   (for-each* (lambda (x) (scan-var add-var x)) a))         
	  ((procedure? f)
	   (add-fkn f)
	   (for-each* (lambda (x) (scan-var add-var x)) a))
	  (else
	   (for-each* (lambda (x) (scan-var add-var x)) (cons f a)))))))

      ((a . l)
       (recur-search x
	 (begin
	   (scan-var add-var a)
	   (scan-var add-var l))))

      (x
       (let ((x (gp-lookup x s)))
         (cond 
          ((gp-var? x s)
           (add-var x))
	  ((gp-attvar-raw? x s)
           (add-fkn x))
	  ((procedure? x)
           (add-fkn x))
          ((and (struct? x) (prolog-closure? x))
	   (recur-search x
	     (for-each
	      (lambda (x)
		(scan-var add-var x))
	      (prolog-closure-state x))))
	  
	   ((not (ident? x))
	    (add-fkn x))
	   (else
	    #t))))))
    
  (define (scan-goal x)
    (umatch (#:mode - #:status s #:name scan-goal) (x)       
      (#(#:brace a)
       (scan-goal a))
      (#((f . a))
       (cond
        ((and (struct? f) (prolog-closure? f))
         (add-fkn (prolog-closure-parent f))
         (for-each* scan-goal (prolog-closure-state f))
         (for-each* scan-goal a))         
        ((procedure? f)
         (add-fkn f)
         (for-each* scan-goal a))
	(else
	 (for-each* scan-goal (cons f a)))))

      ((u . v)
       (scan-var add-var x))

      (x
       (cond 
        ((gp-var? x s)
         (add-var x))

	((gp-attvar-raw? x s)
	 (add-fkn x))

        ((procedure? x)
         (add-fkn x))

        ((and (struct? x) (prolog-closure? x))
         (add-fkn (prolog-closure-parent x))
         (for-each* scan-goal (prolog-closure-state x)))
        
	(else
         #t)))))

  (define (compile-a match-map-i x)
    (define-syntax-rule (do code)
      (let* ((x (gp-lookup x s))
	     (c (lambda () code))
	     (r1 (hashq-ref match-map-o x #f)))
	(if (and r1 (not (eq? r1 #t)))
	    (if (hashq-ref match-first x #f)
		r1
		(begin
		  (hashq-set! match-first x #t)
		  `(and ,r1 ,(c))))
	    (c))))

    (umatch (#:mode - #:status s #:name compile-a) (x)
       (#(#:brace x)
	(list 'unquote
	      (list (G vector) #:brace 
		    (list (G quasiquote)
			  (compile-a match-map-i x)))))

       (#((f . a))
	(do
	  (list 'unquote
	  (let ((f (gp-lookup f s)))
	    (cond
	     ((and (struct? f) (prolog-closure? f))
	      (list (G vector)
		    (list (G quasiquote)
			  (cons (list 'unquote
				      (cons 
				       (get-fkn (prolog-closure-parent f))
				       (map* (lambda (x) 
					       (compile-a match-map-i x))
					     (prolog-closure-state f))))
				(map* (lambda (x) 
					(compile-a match-map-i x))
				      a)))))
	     (else
	      (list (G vector)
		    (list (G quasiquote)
			  (cons (list 'unquote 
				      (if (procedure? f)
					  (get-fkn f)
					  (get-var a)))

				(map* (lambda (x) 
					(compile-a match-map-i x))
				      a))))))))))

       ((a . l)
        (do
	    (let* ((a  (list (G quasiquote) (compile-a match-map-i a)))
		   (b  (list (G quasiquote) (compile-a match-map-i l)))
		   (r0 (list (G cons) a b)))
	      (list 'unquote r0))))
       (a
	(list 'unquote
	      (let ((a (gp-lookup a s)))
		(cond
		 ((gp-var? a s)
		  (get-var a))
		 ((gp-attvar-raw? a s)
                  (get-fkn a))
		 ((procedure? a)
		  (get-fkn a))
		 ((symbol? a)
		  (list (G quote) a))
		 ((null? a)
		 (list 'quote a))
		 ((and (struct? a) (prolog-closure? a))
		  (cons (get-fkn (prolog-closure-parent a))
			(map* (lambda (x) (compile-a match-map-i x))
			      (prolog-closure-state a))))
		 (else x)))))))

  (define (compile-s match-map-i x)
    (umatch (#:mode - #:status s #:name compile-s) (x)
       (#(#:brace x)
	(vector #:brace (compile-s match-map-i x)))
       
       (#((f . a))
	(let ((f (gp-lookup f s)))
	  (cond
	   ((procedure? f)	   
	    (cond
	     ((eq? (object-property f 'prolog-functor-type) #:scm)
	      (cons* 
	       (get-fkn f)
	       (GL S)
	       (map* (lambda (x) 
		       (compile-s match-map-i x))
		     a)))
	     
	     ((eq? (object-property f 'prolog-functor-type) #:goal)
	      (cons*
	       (list '@ '(logic guile-log iso-prolog) (procedure-name f))
	       (map* (lambda (x) 
		       (compile-s match-map-i x))
		     a)))
	       
	     (else
	      (cons* 
	       (get-fkn f)
	       (map* (lambda (x) 
		       (compile-s match-map-i x))
		     a)))))
	   (else
	    (error "copmile-s fkn not a procedure")))))


       (a
	(let ((a (gp-lookup a s)))
	  (cond
	   ((gp-var? a s)
	    (list (GL <lookup>) (get-var a)))
	   ((procedure? a)
	    (get-fkn a))
	   ((symbol? a)
	    (list (G quote) a))
	   (else
	    a))))))
        

  (define (get-goal-types f)
    (object-property f 'goal-compile-types))
              
  (define (get-goal-stub f)
    (object-property f 'goal-compile-stub))

  (define (comp-map* l ll)
    (umatch (#:mode - #:status s #:name comp-map*) (l)       
      ((x . l)
       (match ll
         (('g . ll)
          (cons (compile-goal x) (comp-map* l ll)))
         (('v . ll)
          (cons (compile-a match-map-i x) (comp-map* l ll)))
         (('a . ll)
          (cons (list (G quasiquote) (compile-a match-map-i x))
		(comp-map* l ll)))
         (('s . ll)
          (cons (compile-s match-map-i x)
		(comp-map* l ll)))
         (('ff . ll)
	  ;; This is not used
          (cons (compile-match match-map-i x) (comp-map* l ll)))))
      (() '())))

  (define (compile-goal x)
    (pp 'compgoal x)
    (umatch (#:mode - #:status s #:name compile-goal) (x)
      (#(#:brace a)
       (error "{ ... } is not supported in goal"))

      (#((";" #(("->" x y)) z))
       (list (GL <if>) 
	     (compile-goal x) 
	     (compile-goal y) 
	     (compile-goal z)))
      (#((f . a))
       (let ((f (gp-lookup f s)))
         (cond
          ((and (struct? f) (prolog-closure? f))
           (cons (cons (get-fkn (prolog-closure-parent f))
                       (map* (lambda (x)
			       (list (G quasiquote) 
				     (compile-a match-map-i x)))
			     (prolog-closure-state f)))
                 (map* (lambda (x) 
			 (list (G quasiquote)
			       (compile-a match-map-i x)))
		       a)))

          ((goal-fkn? f)
	   (apply (get-goal-stub f ) (comp-map* a (get-goal-types f))))
          
          ((procedure? f)	   
           (cons 
	    (get-fkn f) 
	    (map* (lambda (x) 
		    (list (G quasiquote)
			  (compile-a match-map-i x)))
		  a)))

          (else
	   (cons* (list (GL <lookup>) (get-var x))
		  (map* (lambda (x) 
			  (list (G quasiquote)
				(compile-a match-map-i x)))
			a))))))

      ((u . v)
       (list (G quasiquote) (compile-a match-map-i x)))

      (x
       (let ((x (gp-lookup x s)))
	 (cond
	  ((gp-var? x s)
	   (list (list (GL <lookup>) (get-var x))))
	  ((eq? x true)
	   (GL <cc>))
	  ((or (eq? x fail) (eq? x false))
	   (GL <fail>))
	  ((eq? x !)
	   (GL <cut>))
	  ((procedure? x)
	   (compile-goal (vector (list x))))
	  ((and (struct? f) (prolog-closure? f))
	   (compile-goal (vector (list x))))
	  (else 
	   (error (format #f "Atom ~a is not allowed as a goal" x))))))))
  
  (define match-first (make-hash-table))
  (define compile-match 
    (case-lambda 
     ((x) (compile-match match-map-o x))
     ((match-map-o x)
    (define-syntax-rule (do code)
      (let* ((x (gp-lookup x s))
	     (c (lambda () code))
	     (r1 (hashq-ref match-map-o x #f)))
	(if (and r1 (not (eq? r1 #t)))
	    (if (hashq-ref match-first x #f)
		r1
		(begin
		  (hashq-set! match-first x #t)
		  `(and ,r1 ,(c))))
	    (c))))

    (pp 'match x)		  
    (umatch (#:mode - #:status s #:name compile-match) (x)
     (#(#:brace a)
      (vector #:brace (compile-match a)))

     (#((f . a))
      (let lp ((first? #t))
	(if first?
	    (if extention? 		
		(let ((r (let ((r (member f extention?)))
			   (if r 
			       (member (cadr r) (default-extensions))
			       r))))
		  (if r
		      (cons (cadr r) (map compile-match a))
		      (let ((r (member f (default-extensions))))
			(if r
			    (cons (cadr r) (map* compile-match a))
			    (lp #f)))))
		(lp #f))	    
	    (if (procedure? f)
		(do (vector `((,'unquote ,(get-fkn f)) 
			      ,@(map* compile-match a))))
		(do (vector (map* compile-match (cons f a))))))))

     ((a . l)
      (do (cons (compile-match a) (compile-match l))))

     (a
      (let ((a (gp-lookup a s)))
	(cond
	 ((gp-var? a s)
	  (get-var a))
	 ((gp-attvar-raw? a s)
          `(,(string->symbol "unquote") ,(get-fkn a)))
	 ((procedure? a)
	  `(,(string->symbol "unquote") ,(get-fkn a)))
	 ((symbol? a)
	  `(quote ,a))        
	 ((not (ident? a))
	  `(,(string->symbol "unquote") ,(get-fkn a)))
	 (else
	  a))))))))
  
  (define var-first (make-hash-table))
  (define (compile-var match-map-i x)
    (define-syntax-rule (do x code)
      (let* ((x  (gp-lookup x s))
	     (c  (lambda () code))
	     (r1 (hashq-ref match-map-i x #f)))
	(if (and r1 (not (eq? r1 #t)))
	    (if (hashq-ref var-first x #f)
		r1
		(begin
		  (hashq-set! var-first x #t)
		  `(,(G let) ((,r1 (,(G make-variable) #f)))
		    (,(G variable-set!) ,r1 ,(c))
		    ,r1)))
	    (c))))

    (define (map-cons match-map-i l)
      (umatch (#:mode - #:status s #:name compile-match) ((pp 'compile-var2 l))
       ((a . l)
	(do l
          (list (G cons) (compile-var match-map-i a) (map-cons match-map-i l))))
       (a (compile-var match-map-i a))))

    (umatch (#:mode - #:status s #:name compile-match) ((pp 'compil-var1 x))
      (#(#:brace a)
       (list (G vector) #:brace (compile-var match-map-i a)))

      (#((f . a))
       (do x 
        (let ((f (gp-lookup f s)))
         (cond
          ((and (struct? f) (prolog-closure? f))
           `(,(G vector) (,(G list)
                          ,(cons (get-fkn (prolog-closure-parent f))
                                 (map* (lambda (x)
					 (compile-a match-map-i x))
                                       (prolog-closure-state f)))
                          ,@(map* (lambda (x) 
				    (compile-var match-map-i x))
				  a))))
          (else
	   (let* ((l (map* (lambda (x)
			     (compile-var match-map-i x))
			   a))		  
		  (l (let lp ((l l) (r '()))
		       (if (pair? l)
			   (lp (cdr l) (cons (car l) r))
			   (if (null? l)
			       (reverse (cons (list (G quote) '()) r))
			       (reverse (cons l   r)))))))
			       
	     (list (G vector) (cons* (G cons*) 
				     (if (procedure? f)
					 (get-fkn f)
					 (get-var f))
				     l))))))))
       ((a . l)
       (do x
	   (list (G cons) (compile-var match-map-i a) 
		 (map-cons match-map-i l))))
      
      (a
       (let ((a (pp 'u (gp-lookup a s))))
         (cond
          ((gp-var? a s)
           (get-var a))
	  ((gp-attvar-raw? a s)
           (get-fkn a))
          ((procedure? a)
           (get-fkn a))
          ((symbol? a)
           (list 'quote a))
          ((and (struct? a) (prolog-closure? a))
	   (do x
             (cons (get-fkn (prolog-closure-parent a))
		   (map* (lambda (x)
			   (list (G quaeiquote) (compile-a match-map-i x)))
			 (prolog-closure-state a)))))
          ((not (ident? a))
           (get-fkn a))
          ((null? a)
           ''())
          (else
           a))))))
                 
  (set! match-map   (make-hash-table))
  (scan-var add-exvar a)

  (set! match-map-o match-map)
  (set! match-map   (make-hash-table))

  (scan-goal f)

  (set! match-map-i match-map)
  (pp `(compile ,a ,f))
  (let* ((aa    (pp 'aa (compile-match a)))
	 (ff    (compile-goal f))
	 (aaa   (pp 'aaa (compile-var match-map-o a)))
	 (fff   (compile-var match-map-i f))
	 (vfkn  (hash-fold (lambda (k v r) (cons v r)) '() fkns))
	 (ffkn  (pp 'ffkn
                    (hash-fold (lambda (k v r) (cons k r)) '() fkns)))
         (v1.v2 (let lp ((vfkn vfkn) (ffkn ffkn) (v1 '()) (v2 '()))
                  (if (pair? ffkn)
                      (let ((f (car ffkn)))
                        (if (gp-attvar-raw? f s)
                            (lp (cdr vfkn) (cdr ffkn) (cons
                                                       (cons f (car vfkn))
                                                       v1) v2)
                            (lp (cdr vfkn) (cdr ffkn) v1
                                (cons
                                 (cons f (car vfkn))
                                 v2))))
                      (cons v1 v2))))

         (ffkn1 (map car (car v1.v2)))
         (vfkn1 (map cdr (car v1.v2)))
         (ffkn2 (map car (cdr v1.v2)))
         (vfkn2 (map cdr (cdr v1.v2)))
                
	 (vars  (hash-fold (lambda (k v r) (cons v r)) '() in-house))
	 (varq  (map (lambda v (gensym "VARIN-")) vars))
	 (ovars (hash-fold (lambda (k v r) (cons v r)) '() ex-house))
	 (ovarq (map (lambda v (gensym "MATCHVARIN-")) ovars)))
    (define src #f)
    (define lam
      (with-fluids ((*current-language* (lookup-language 'scheme))) 
	(set! src
	  (lambda (lam u)
	    (cond
	     (meta-only?
	      (pp 'comp1
		  `(,@lam (,@u ,@vfkn1 ,@vfkn2 ,@varq ,@ovarq)
			  (,(G let) ,(map (lambda (w v) (list w v))
					  (append vars ovars)
					  (append varq ovarq))
			   ((@@ (logic guile-log functional-database) 
				<lambda-dyn-meta>) ,aa 
				,(if include-meta
				     (list (G cons) `,aaa `,fff)
				     (list (G cons) `(list) `(list))))))))
	       
	      (fast-compile?
	       (pp 'comp2 
		   `(,@lam (,@u ,@vfkn1 @vfkn2 ,@varq ,@ovarq)
			   (,(G let) ,(map (lambda (w v) (list w v))
					   (append vars ovars)
					   (append varq ovarq))
			    ((@ (logic guile-log) <lambda> ) (cut scut ,@aa)
			     ((@ (logic guile-log) <with-cut>) cut scut
                              (,(G let) ((v12345 (,(GL <cp>)
                                                  (,(G list) ,@vfkn1))))
                               (,(I match) v12345
                                ((,vfkn1
			         (,(GL <var>) ,vars ,ff)))))))))))

	       ((and (pair? extention?))
		(pp 'comp3
		     `(,@lam (,@u ,@vfkn1 ,@vfkn2 ,@varq ,@ovarq)
			     (,(G let) ,(map (lambda (w v) (list w v))
					     (append vars ovars)
					     (append varq ovarq))
			      ((@@ (logic guile-log functional-database) 
				   <lambda-dyn-extended>) ,vfkn1 ,aa 
				   (,(GL <var>) ,vars ,ff)
				   ,(list (G cons) `,aaa `,fff))))))
	       (else
		(pp 'comp4 
		    `(,@lam (,@u ,@vfkn1 ,@vfkn2 ,@varq ,@ovarq)
			    (,(G let) ,(map (lambda (w v) (list w v))
					    (append vars ovars)
					    (append varq ovarq))
                             ((@@ (logic guile-log functional-database) 
                                  <lambda-dyn>) ,vfkn1 ,aa 
                                  (,(GL <var>) ,vars ,ff)
                                  ,(list (G cons) `,aaa `,fff)))))))))
	(if (not source?)
	    (if meta-only?
		(eval (src (list (G lambda)) '())
		      (current-module))
		(compile (src (list (G lambda)) '())
			 #:env (current-module))))))

    (define (lamlam lam)
      (lambda (f)
	(lam f)))
    (if source?
        (pp 'source
          #`(#,(datum->syntax source? (src (list (G lambda)) '()))
           #,@(append (map (lamlam
                            (lambda (f)
                              (datum->syntax
                               source?
                               (let ((mod (procedure-property f 'module)))
                                 (if mod
                                     (list '@@ mod
                                               (procedure-name f))
                                     (procedure-name f))))))
                           ffkn)
                      (map (lambda (x) #'((@ (logic guile-log umatch)
                                             gp-make-var)))
                           (append vars ovars)))))
	(apply lam
	       (append (gp-cp ffkn1 s)
                       (append
                        ffkn2
                        (map (lambda (x) (gp-make-var)) 
                             (append vars ovars))))))))

(set! (@@ (logic guile-log slask) compile-prolog) compile-prolog)

