(define-module (logic guile-log prolog namespace)
  #:use-module (logic guile-log procedure-properties)
  #:use-module (logic guile-log code-load)
  #:use-module ((logic guile-log)
                #:select (<define> <lookup> <scm> <=> <code> <wrap>))
  #:use-module (logic guile-log umatch)
  #:use-module (logic guile-log prolog error)
  #:use-module (logic guile-log prolog closed)
  #:use-module (logic guile-log prolog names)
  #:use-module (logic guile-log dynamic-features)
  #:use-module (ice-9 match)
  #:use-module (ice-9 pretty-print)
  #:re-export (	   make-namespace
                   namespace?
                   namespace-val
                   namespace-ns                     
                   namespace-local?
                   namespace-lexical?
                   setup-namespace)

  #:export (namespace_p
            namespace_val
            namespace_ns
            namespace_local_p
            namespace_lexical_p

            error_on_namespace_switch
            fail_on_namespace_switch
            ok_on_namespace_switch
            get_namespace_switch_handle x

            namespace_white_list_handle
            set_no_namespace_whitelist
            namespace_white_list_ref
            namespace_white_list_set
            
            namespace_switch            

	    ns-unify
            ))

(define namespace_switch #f)

(<define> (namespace_p        
           x)   (when (namespace?         (<lookup> x))))
(<define> (namespace_val      
           x v) (<=> v ,(namespace-val    (<lookup> x))))
(<define> (namespace_ns       
           x v) (<=> v ,(namespace-ns     (<lookup> x))))
(<define> (namespace_local_p  
           x v) (when (namespace-local?   (<lookup> x))))
(<define> (namespace_lexical_p  
           x v) (when (namespace-lexical? (<lookup> x))))

(define do-print #f)
(define pp
  (case-lambda
   ((s . x)
    (when do-print
      (pretty-print `(,s ,(syntax->datum x))))
    x)
   ((x)
    (when do-print
      (pretty-print (syntax->datum x)))
    x)))

(define ppp
  (case-lambda
   ((s x)
    (when #t
      (pretty-print `(,s ,(syntax->datum x))))
    x)
   ((x)
    (when #t
      (pretty-print (syntax->datum x)))
    x)))

#|
Unification that varifies namespaces in a unification e.g. we can do

X@@a = Y
X@   = current open module

Two things will happen
1) Var = string, will be translated to an actual function in a lookup 
                 for the namespace
2) Var = atom,  atom have to be in the namespaced module.
3) new namespaces found changes the restricted namespace
|#



(define fail-when-new-namespace? (make-fluid #f))
(<wrap> add-fluid-dynamics fail-when-new-namespace?)

(<define> (error_on_namespace_switch)
  (<code> (fluid-set! fail-when-new-namespace? 'error)))
(<define> (fail_on_namespace_switch)
  (<code> (fluid-set! fail-when-new-namespace? 'fail)))
(<define> (ok_on_namespace_switch)
  (<code> (fluid-set! fail-when-new-namespace? #f)))
(<define> (get_namespace_switch_handle x) 
  (<=> x ,fail-when-new-namespace?))

(define white-list-namespaces    (make-fluid #f))
(<wrap> add-fluid-dynamics white-list-namespaces)

(<define> (namespace_white_list_handle H) 
          (<=> H white-list-namespaces))
(<define> (set_no_namespace_whitelist) 
   (<code> (fluid-set! white-list-namespaces #f)))
(<define> (namespace_white_list_ref x) 
   (<=> x ,(fluid-ref white-list-namespaces)))
(<define> (namespace_white_list_set x)
  (<code> (fluid-set! white-list-namespaces (<scm> x))))


(mk-sym namespace_switch)
(define (err x) (<wrap> permission_error namespace_switch true x))

(define (comp-ok? ns1 local1? ns2 local2? binary? rw-in)
  (define ret-ok (if rw-in #t 'read))
  (let ((fail (fluid-ref fail-when-new-namespace?))
        (wl   (fluid-ref white-list-namespaces)))
    (define (not-ok)
      (cond
       ((eq? fail 'fail)
        #f)
       ((eq? fail 'error)
        (err (list ns1 '===> ns2)))))
          
    (if (and (equal? ns1 ns2) (or local1? (not local2?)))
        ret-ok
        (if binary?
            (not-ok)
            (if fail
                (if (eq? fail #t)
                    (not-ok)
                    (if (not wl)
                        ret-ok
                        (let lp-wl ((l wl) (found #f))
                          (match l
                            (((local? rw? . dir) . l)
                             (if (or (eq? local? true) (not local2?))
                                 (let lp ((dir dir) (ns ns2))
                                   (match dir
                                     (((or '* "*")) 
                                      (if (eq? rw? true)
                                          #t
                                          (lp-wl l #t)))

                                     (()  
                                      (if (null? ns)
                                          (if (eq? rw? true)
                                              #t
                                              (lp-wl l #t))
                                          (lp-wl l found)))
                                 
                                     ((x . dir)
                                      (match ns
                                        ((y . ns)
                                         (if (equal? x y)
                                             (lp dir ns)
                                             (lp-wl l found)))
                                        (_ (lp-wl l found))))))
                                 (lp-wl l found)))
                            (()
                             (if found
                                 'read
                                 (not-ok)))))))
                (not-ok))))))
  

(define (translate x ns l? rw?)
  (let ((sym (string->symbol x))
        (mod (resolve-module ns)))
    (if l?
        (if (module-defined? mod sym)
            (module-ref mod sym)
            (if rw?
                (let ((f (make-sym mod sym)))
                  (module-define! mod sym f)
                  f)
                #f))
        (let ((pub (module-public-interface mod)))
          (if pub
              (if (module-defined? pub sym)
                  (module-ref pub sym)
                  (if rw?
                      (let ((f (make-sym mod sym)))
                        (module-define! mod sym f)
                        (module-set! pub sym (module-ref mod sym))
                        f)
                      #f))
              #f)))))


(define (validate x ns local? s rw?)
  (define it #f)
  (define-syntax-rule (aif p x y)
    (let ((q (pp 'aif p)))
      (if q
          (begin
            (set! it (if (eq? q #t) #t #f))
            x)
          y)))

  (define (f x s cont)
    (cond
     ((prolog-closure? x)
      (if (validate (prolog-closure-parent x) ns local? s rw?)
	  (validate (prolog-closure-state  x) ns local? s rw?)
	  #f))
    
     ((namespace? x)
      (aif (comp-ok? ns local? (namespace-ns x) (namespace-local? x) #f rw?)
           (if (namespace-lexical? x)
               (error "lexical in validate is a bug!")
               (validate (namespace-val    x) (namespace-ns x) 
                         (namespace-local? x) s it))
           #f))
     
     ((vector? x)
      (validate (vector->list x) ns local? s rw?))
     
     ((gp-var? x s)
      s)

     ((procedure? x)
      (let ((mod (procedure-property x 'module)))
        (if mod
            (if (equal? mod ns)
                (if (not local?)
                    (if (module-defined?
                         (module-public-interface (resolve-module mod))
                         (procedure-name x))
                        s
                        #f)
                    s)
                #f)
            #f)))
     
     (else
      (cont x s))))

  (pp 'validate x ns  local?)

  (f x s
   (lambda (x s)
     (let lp ((s s) (x x))
       (umatch (#:mode - #:status s #:name 'validate) (x)
	 ((x . l)
	  (let ((s (validate (gp-lookup x s) ns local? s rw?)))
	    (if s
		(lp s (gp-lookup l s))
		s)))
	 (x (f x s (lambda (x s) s))))))))

(define (ns-unify  s ns y bang?) 
  (pp 'ns-unify s ns y bang?)
  (ns-unify* s ns y bang?))
(define (ns-unify* s xin y bang?)
  (define (unify x y s)
    (if bang?
        (gp-unify! x y s)
        (gp-m-unify! x y s)))
  (define it #f)
  (define-syntax-rule (aif p x y)
    (let ((q (pp 'aif p)))
      (if q
          (begin
            (set! it (if (eq? q #t) #t #f))
            x)
          y)))

  (let ((x    (gp-lookup (namespace-val xin) s))
	(ns   (namespace-ns       xin))
        (lx?  (namespace-local?   xin))
        (lex? (namespace-lexical? xin)))
    
    (let lp ((rw? #t)
             (x      (gp-lookup x s)) (y      (gp-lookup y s)) 
             (ns-x   ns)              (ns-y   #f) 
             (lx?    lx?)             (ly?    #f)
             (x-lex? lex?)            (y-lex? #f) (? #f) (s s))

      (pp 'lp x y ns-x ns-y lx? ly? #:x-lex? x-lex? #:y-lex? y-lex?)

      (cond
       ((namespace? y)
        (let ((ns-y2 (namespace-ns y))
              (ly2?  (namespace-local? y))
              (lex2? (namespace-lexical? y)))
          (aif (if (not ns-y) #f (comp-ok? ns-y ly? ns-y2 ly2? #f rw?))
               (lp it
                   x      (gp-lookup (namespace-val y) s)
                   ns-x   ns-y2
                   lx?    ly2?
                   x-lex? lex2? #t s)
               #f)))
	
       ((namespace? x)
        (let ((ns-x2 (namespace-ns x))
              (lx2?  (namespace-local? x))
              (lex2? (namespace-lexical? x)))
          (aif (comp-ok? ns-x lx? ns-x2 lx2? #f rw?)
              (if ?
                  (lp it
                      (gp-lookup (namespace-val x) s) y
                      ns-x2                           ns-y
                      lx2?                            ly?
                      lex2?                           y-lex? ? s)
                  (lp it
                      (gp-lookup (namespace-val x) s) y
                      ns-x2                           ns-x2
                      lx2?                            lx2?
                      lex2?                           lex2? ? s))
              #f)))
                  

       ((not ns-y)
        (lp rw?
            x      y
            ns-x   ns-x
            lx?    lx?
            x-lex? #t ? s))
	   
	
       (else
        (if (and (equal? ns-x ns-y) (eq? lx? ly?))
            (cond
             ((gp-var? x s)
              (if (gp-var? y s)
                  (if (and x-lex? y-lex? bang?)
                      (let ((s (gp-set! x y s)))
                        (gp-set! x (make-namespace (gp-var! s)
                                                   ns-x lx? #f)
                                 s))
                      (if bang?
 			  (cond
 			   (x-lex?
 			    (gp-set! x (make-namespace y ns-x lx? #f) s))
 			   (y-lex?
                            (gp-set! y (make-namespace x ns-y ly? #f) s))
 			   (else
 			    (gp-set! x y s)))
                          (if (eq? x y)
                              s
                              #f)))
                  (imprint! x y ns-x lx? x-lex? y-lex? s bang? rw?)))
		   
             ((gp-var? y s)                    
              (imprint! y x ns-y ly? y-lex? x-lex? s bang? rw?))
                   
             ((or (vector? x) (vector? y))
              (if (and (vector? x) (vector? y))
                  (lp rw?
                      (vector->list x) (vector->list y) ns-x ns-y lx? ly? 
                      x-lex? y-lex? ? s)
                  #f))
	   
             ((or (procedure? x) (procedure? y))
              (if (eq? (procedure? x) (procedure? y))
                  s
                  #f))

             ((or (prolog-closure? x) (prolog-closure? y))
              (if (and (prolog-closure? x) (prolog-closure? y))
                  (if (eq? (prolog-closure-parent x) 
                           (prolog-closure-parent y))
                      (lp rw?
                          (prolog-closure-state x)
                          (prolog-closure-state y)
                          ns-x ns-y lx? ly? x-lex? y-lex? ? s)
                      (if (fluid-ref error-when-closed?)
                          ((@@ (logic guile-log prolog closed) err)
                           x y)
                          #f))))
	   
             (else
              (umatch (#:mode - #:status s #:name 'ns-1) (x y)
                      ((xa . xl) (ya . yl)
                       (let lp-x ((s s) (x x) (y y))
                         (umatch (#:mode - #:status s #:name 'ns-2) (x y)
                           ((xa . xl) (ya . yl)
                            (let ((s (lp rw?
                                         (gp-lookup xa s)
                                         (gp-lookup ya s)
                                         ns-x ns-y lx? ly? 
                                         x-lex? y-lex? ? s)))
                              (if s
                                  (lp-x s (gp-lookup xl s) (gp-lookup yl s))
                                  s)))
                                
                           (x        y
                            (lp rw? x y ns-x ns-y lx? ly? 
                                x-lex? y-lex? ? s)))))

                      (x y
                         (if (equal? x y) s #f)))))

            #f))))))

(define (imprint! x y ns lx? lex? y-lex? s bang? rw?)
  (if (not bang?)
      #f
      (imprint!* x y ns lx? lex? y-lex? s rw?)))
(define (imprint!* x y ns lx? lex? y-lex? s rw?)
  (define (unify x y s)
    (gp-unify! x y s))

  (define (check-proc proc)
    (let ((name (procedure-name proc)))
      (if name
          (let ((modnm (procedure-property proc 'module)))
            (if modnm
                (if (equal? ns modnm)
                    (if lx?
                        #t
                        (module-defined? (module-public-interface 
                                          (resolve-module ns))
                                         name))
                    #f)
                #f))
          #f)))

  (define it #f)
  (define-syntax-rule (aif p x y)
    (let ((q (pp 'aif p)))
      (if q
          (begin
            (set! it (if (eq? q #t) #t #f))
            x)
          y)))
  
  (let lp ((rw? rw?) (s s) (y (gp-lookup y s)) (x (gp-lookup x s)) 
           (y-lex? y-lex?) (ns ns) (lx? lx?))
    (pp 'imp y x)
    (if (or (gp-var? x s) (pair? x))
    (let ((f (lambda ()
               (cond
                ((vector? y)
                 (let* ((xx (gp-var! s))
                        (ly (vector->list y))
                        (lx (map (lambda (x) (gp-var! s)) ly)))
                   (if y-lex?
                       (let lp2 ((s s) (llx lx) (lly ly))
                         (if (pair? llx)
                             (lp2 (lp rw? s 
                                      (gp-lookup (car lly) s)
                                      (gp-lookup (car llx) s)
                                      y-lex? ns lx?)
                                  (cdr llx)
                                  (cdr lly))
                             (gp-set! x (list->vector
                                         (map (lambda (x) (gp-lookup x s)) lx))
                                      s)))
                       (let ((s (validate y ns lx? s rw?)))
                         (if s
                             (gp-set! x y s)
                             #f)))))
                         
                ((string? y)                 
                 (let ((f (translate y ns lx? rw?)))
                   (if f
                       (gp-set! x f s)
                       #f)))
                
                ((prolog-closure? y)
                 (if (check-proc (prolog-closure-parent y))
                     (let*  ((xx (gp-var! s))
                             (ly (prolog-closure-state y))
                             (lx (map (lambda (x) (gp-var! s)) ly)))
                       (if y-lex?
                           (let ((s (lp rw? s ly lx y-lex? ns lx?)))
                             (if s                                   
                                 (gp-set! x
                                          (make-prolog-closure
                                           (prolog-closure-closure y)
                                           (prolog-closure-parent  y)
                                           lx
                                           (prolog-closure-closed? y))
                                          s)
                                 #f))
                           (let ((s (validate (gp-lookup ly s)
                                              ns lx? s rw?)))
                             (if s
                                 (gp-set! x y s)
                                 #f))))
                     
                     #f))
                    
                ((procedure? y)
                 (if (check-proc y)
                     (gp-set! x y s)
                     #f))
                  
             
                ((namespace? y)
                 (let ((ns2  (namespace-ns y))
                       (lx2? (namespace-local? y)))
                   (aif (comp-ok? ns lx? ns2 lx2? #t rw?)
                        (if (namespace-lexical? y)
                            (lp it s (namespace-val y) x #t ns2 lx2?)
                            (let ((s (validate y ns2 lx2? s it)))
                              (if s
                                  (gp-set! x y s)
                                  #f)))
                        #f)))

             

                ((gp-var? y s)
                 (let ((s (gp-set! x y s)))
                   (if y-lex?
                       (gp-set! y
                                (make-namespace (gp-var! s) ns lx? #f)
                                s)
                       s)))
                (else
                 (gp-set! x y s))))))

      (umatch (#:mode - #:status s #:name imprint!) (y)
        ((yy . ly) 
         (umatch (#:mode + #:status s #:name 'imprint2) (x)
	   ((xx . lx)        
            (let ((s (lp rw? s (gp-lookup yy s) (gp-lookup xx s) 
                         y-lex? ns lx?)))
              (if s
                  (lp rw? s (gp-lookup ly s) (gp-lookup lx s) 
                      y-lex? ns lx?)
                  s)))))

        (_ (f))))
    s)))


(setup-namespace <namespace-type> ns-unify)
