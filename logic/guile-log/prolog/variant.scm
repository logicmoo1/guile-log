(define-module (logic guile-log prolog variant)
  #:use-module (logic guile-log)
  #:use-module ((logic guile-log slask) #: select (op2=))
  #:use-module (logic guile-log code-load)
  #:use-module (logic guile-log vlist)
  #:use-module (logic guile-log guile-prolog attribute)
  #:use-module ((logic guile-log umatch)
                #:select (gp-var? gp-var! gp-unwind))
  #:export (is-variant? subsumes_term
			term_subsumer mk-variant
			unifiable unifiable0 vareq))

(define a 1)

(define-syntax-rule (aif it p a b) (let ((it p)) (if it a b)))

(define ei (gensym "ei"))
(define ej (gensym "ej"))

(define (mk-i) (cons ei 0))
(define (mk-j) (cons ej 0))
(define (mk-v) (make-hash-table))

(<define> (mk-v0 x y) (mk-variant x y (mk-i) (mk-j) (mk-v) (mk-v) -1 -1))
(<define> (mk-vr x y) (mk-variant x y (mk-i) (mk-j) (mk-v) (mk-v) #t #t))

(define mk-vv (make-fluid mk-v0))

(define (get-next i)
  (set-cdr! i (+ 1 (cdr i)))
  (cdr i))

(define (vref hx x)
  (hashq-ref hx x #f))

(define (vset! h x y)
  (hashq-set! h x y))

(<define> (vget ex ey i h x)
 (let* ((x  (<lookup> x)))
   (if (<var?> x)
       (let ((i (get-next i)))
         (<set> x (cons* ex i (gp-var! S)))
         (<cc> i))
       (if (eq? (car x) ex)
           (<cc> (cadr x))
           (let ((x (cddr x)))
             (if (<var?> x)
                 (let ((i (get-next i)))
                   (<=> x i)
                   (<cc> i))
                 (<cc> x)))))))

(<define> (fail . l) <fail>)

(<define> (check x hx sx)
  (if (eq? sx -1)
      (<cc> -1)
      (if sx
	  (if (vref hx x)
	      (<cc> #f)
	      (<and>
	       (<code> (vset! hx x #t))
	       (<cc> #t)))
	  (<cc> #f))))

(define-inlinable (te x y) (not (or x y)))


(<define> (mk-variant x y ix iy hx hy sx sy)
 (if (te sx sy)
     <cc>
     (let ((ex (car ix))
           (ey (car iy))
           (x (<lookup> x))
	   (y (<lookup> y)))
       (<match> (#:mode -) (x y)
         ((xa . _) y
          (<if> (<or> (<==> xa ex) (<==> xa ey))
                (<and>
                 <cut>
                 (<values> (jx) (vget ex ey ix hx x))
                 (<values> (jy) (vget ey ex iy hy y))
                 (<==> jx jy))
                <fail>))

         (x (ya . _)
          (<if> (<or> (<==> ya ex) (<==> ya ey))
                (<and>
                 <cut>
                 (<values> (jx) (vget ex ey ix hx x))
                 (<values> (jy) (vget ey ex iy hy y))
                 (<==> jx jy))
                <fail>))
         
         ((xa . xb) (ya . yb)
	  (<and>
	   <cut>
	   (<values> (sx) (check x hx sx))
	   (<values> (sy) (check y hy sy))
	   (if (te sx sy)
	       <cc>
	       (<and>
		(mk-variant xa ya ix iy hx hy sx sy)
		(mk-variant xb yb ix iy hx hy sx sy)))))
	 
	 (#(a ...) #(b ...)
	  (<and>
	   <cut>
	   (<values> (sx) (check x hx sx))
	   (<values> (sy) (check y hy sy))
	   (if (te sx sy)
	       <cc>
	       (mk-variant a b ix iy hx hy sx sy))))

	 (_ _
	    (<and>
	     <cut>
	  (cond
	   ((gp-attvar-raw? x S)
	    (<values> (sx) (check x hx sx))
	    (<values> (sy) (check y hy sy))
	    (if (te sx sy)
		<cc>
		(<let> ((data (gp-att-data x S)))
		  (<recur> lp ((data (<lookup> data)))
		   (if (pair? data)
		       (let ((lam (caar data)))
			 (lam x y ix iy hx hy sx sy mk-variant)
                         (lp (cdr data)))
		       <cc>)))))
	   
	   ((gp-attvar-raw? y S)
	    (<values> (sx) (check x hx sx))
	    (<values> (sy) (check y hy sy))
	    (if (te sx sy)
		<cc>
		(<let> ((data (gp-att-data y S)))
                 (<recur> lp ((data (<lookup> data)))
                   (if (pair? data)
		       (let ((lam (caar data)))
			 (lam y x iy ix hy hx sy sx mk-variant)
                         (lp (cdr data)))
		       <cc>)))))
	 
	   ((and (prolog-closure? x) (prolog-closure? y))
	    (<values> (sx) (check x hx sx))
	    (<values> (sy) (check y hy sy))
	    (if (te sx sy)
		(if (eq? (prolog-closure-parent x) (prolog-closure-parent y))
		    (<let> ((stx (prolog-closure-state x))
			    (sty (prolog-closure-state y)))	      
			   (mk-variant stx sty ix iy hx hy sx sy)))))

	   ((and (<var?> x) (<var?> y))
            (<values> (jx) (vget ex ey ix hx x))
            (<values> (jy) (vget ey ex iy hy y))
            (<==> jx jy))

	   (else
	    (<==> x y)))))))))

(<define> (vareq x y ex ey ix iy hx hy)
  (<values> (jx) (vget ex ey ix hx x))
  (<values> (jy) (vget ey ex iy hy y))
  (<==> jx jy))

(<define> (is-variant? x y)
   (let ((s (gp-newframe S)))            
     (<with-s> s ((fluid-ref mk-vv) x y))
     (<code> (gp-unwind s))))


(define (add-var s x l)
  (if (gp-attvar? x s)
      (begin
	(set-car! l (cons x (car l)))
	#t)
      #f))

(define (mk-l) (cons '() #f))

(<define> (subsumes x y l h)
  (<match> (#:mode -) (x y)
    ((++ (a . b)) (u . v)
     (<cut>
      (if (href h y)
	  (<=> x ,(href h y))
	  (<and>
	   (<code> (hset! h y x)) 
	   (subsumes a u l h)
	   (subsumes b v l h)))))
    ((+ + #(x))    #(y)
     (<cut>
      (if (href h y)
	  (<=> x ,(href h y))
	  (<and>
	   (<code> (hset! h y x)) 	  
	   (subsumes x y l h)))))
    (_ _
     (<cut>
       (<let> ((y (<lookup> y))
	       (x (<lookup> x)))
	 (if (add-var S y l)	   
	     (if (attvar? x)
		 (<if> (<==> x y) 
		       <cc> 
		       (<=> x y))
		 <fail>)
	     (if (attvar? x)
		 (<=>  x y)
		 (<==> x y))))))))

(<define> (subs0 x y l) (subsumes x y l #f))
(<define> (subsr x y l) (subsumes x y l (mk-h)))

(define subs (make-fluid subsr))

(<define> (subsumes_term x y)
  (<let> ((l (mk-l))
	  (p P)
	  (s0 S)
	  (s (<newframe>)))
    ((fluid-ref subs) x y l)
    (when (and-map (lambda (x) (eq? x (<lookup> x))) (car l)))
    (<code> (<unwind-tail> s))    
    (<with-s> s0
    (<with-fail> p <cc>))))



(<define> (genner X0 Y0 Z0 h)
 (<match> (#:mode -) (X0 Y0 Z0)
   ((X . Y) (A . B) (+ + (U . V))
    (<cut>
     (if (href h X0 Y0)
	 (<=> Z0 ,(href h X0 Y0))
	 (<and>
	  (<code> (hset! h X0 Y0 Z0))
	  (genner X A U h)
	  (genner Y B V h)))))
   (#(A) #(B) (+ + #(C))
    (<cut>
     (if (href h X0 Y0)
	 (<=> Z0 ,(href h X0 Y0))
	 (<and>
	  (<code> (hset! h X0 Y0 Z0))
	  (genner A B C h)))))

   (X X (+ + X) 
    (<cut> <cc>))
   (_ _ _ (<cut> <cc>))))

(<define> (gen0 X Y Z) (genner X Y Z #f))
(<define> (genr X Y Z) (genner X Y Z (mk-h)))

(define gen (make-fluid genr))

(<define> (term_subsumer X Y Z) ((fluid-ref gen) X Y Z))

(<define> (u X Y L pred h)
  (<match> (#:mode -) (X Y)
    (X X 
     (<cut>
      (if pred (<cc> L) <cc>)))

    ((? <var?> X) Y
     (<and> 
      (when (not (and (gp-attvar-raw? (<lookup> X) S)
		      (<wrap-s> test_attr S X "nonvar"))))
      (<cut> 
       (if pred
           (<cc> (cons (vector (list op2= X Y)) L))
           <cc>))))

    (X (? <var?> Y)
       (<and>
        (when (not (and (gp-attvar-raw? (<lookup> Y) S)
			(<wrap-s> test_attr S Y "nonvar"))))
        (<cut>	
         (if pred
             (<cc> (cons (vector (list op2= Y X)) L))
             <cc>))))

    (#(A) #(B)
     (<cut>
      (if (href h X Y)
	  (if pred
	      (<cc> L)
	      <cc>)
	  (<and>
	   (<code> (hset! h X Y #t))
	   (u A B L pred h)))))

    (#(_ A) #(_ B)
     (<cut>
      (if (href h X Y)
	  (if pred
	      (<cc> L)
	      <cc>)
	  (<and>
	   (<code> (hset! h X Y #t))
	   (u A B L pred h)))))

    ((X2 . Y2) (A . B)
     (<cut>
      (if (href h X Y)
	  (if pred
	      (<cc> L)
	      <cc>)
	  (<and>
	   (<code> (hset! h X Y #t))
	   (if pred
	       (<and>
		(<values> (L2) (u X2 A L pred h))
		(u Y2 B L2 pred h))
	       (<and>
		(u X2 A L pred h)
		(u Y2 B L pred h)))))))

    (X Y
       (let ((X (<lookup> X))
             (Y (<lookup> Y)))
         (cond
          ((gp-attvar-raw? X S)
           (let ((data (gp-att-data X S)))
             (<recur> lp ((data (<lookup> data)) (L L))
               (if (pair? data)
                   (let ((lam (caar data)))
                     (<values> (L) (lam X Y L pred h u))
                     (lp (cdr data) L))
                   (<cc> L)))))

          ((gp-attvar-raw? Y S)
           (let ((data (gp-att-data Y S)))
             (<recur> lp ((data (<lookup> data)) (L L))
               (if (pair? data)
                   (let ((lam (caar data)))
                     (<values> (L) (lam Y X L pred h u))
                     (lp (cdr data) L))
                   (<cc> L)))))
        
          (else      
           (<cut> <fail>)))))))


(define href 
  (case-lambda
   ((h x y)
    (if h
	(vhasha-ref (variable-ref h) (list x y) #f)
	#f))
   ((h x)
    (if h
	(vhashq-ref (variable-ref h) x #f)
	#f))))

(define hset! 
  (case-lambda
   ((h x y val)
    (if h
	(variable-set! h (vhash-consa (list x y) val (variable-ref h)))
	#f))
   ((h x val)
    (if h
	(variable-set! h (vhash-consq x val (variable-ref h)))
	#f))))

(define (mk-h)
  (make-variable vlist-null))

(define u0 (<lambda> (x y l p) (u x y l p #f)))
(define ur (<lambda> (x y l p) (u x y l p (mk-h))))
(define uu (make-fluid ur))

(<define> (unifiable X Y Us)
  (<values> (L) ((fluid-ref uu) X Y '() #t))
  (<=> Us L))

(<define> (unifiable0 X Y)
  ((fluid-ref uu) X Y '() #f))
    
