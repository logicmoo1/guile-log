(define-module (logic guile-log prolog varstat)
  #:use-module (ice-9 match)
  #:use-module (ice-9 pretty-print)
  #:export (init-first-variables first-variable? first-variable!
				 register-variables
				 local-variable! local-variable?
				 with-varstat variable-not-included?))


(define do-print #f)
(define pp
  (case-lambda
   ((s x)
    (when do-print
      (pretty-print `(,s ,(syntax->datum x))))
    x)
   ((x)
    (when do-print
      (pretty-print (syntax->datum x)))
    x)))

(define ppp
  (case-lambda
   ((s x)
    (when #t
      (pretty-print `(,s ,(syntax->datum x))))
    x)
   ((x)
    (when #t
      (pretty-print (syntax->datum x)))
    x)))

(define-syntax-rule (with-varstat . code)
  (with-fluids ((*vs* #f) (*vl* #f)) (begin (begin . code))))

(define *vs* (make-fluid (make-hash-table)))
(define *vl* (make-fluid (make-hash-table)))
(define (init-first-variables)
  (fluid-set! *vs* (make-hash-table))
  (fluid-set! *vl* (make-hash-table)))

(define (first-variable? v id)
  (let ((i (hash-ref (fluid-ref *vs*) v -2)))
    (if (and (pair? i) (<= (cdr i) 2))
	(= (car i) id)
	#f)))

(define (first-variable! v)
  (hash-set! (fluid-ref *vs*) v -1))

(define (local-variable! v)
  (hash-set! (fluid-ref *vl*) v #t))

(define (local-variable? v)
  (hash-ref (fluid-ref *vl*) v #f))

(define (variable-not-included? v x)
  (define (off) #f)
  (define (g x)
    (match (pp 'g x)
    (()
     #'<cc>)    
    (((kind _ op _) x y n m)
     (and (g x) (g y)))
    
    (((kind _ op _) x n m)
     (g x))
    
    ((#:group x)
     (g x))

    ((#:list l _ _)
     (g l))

    ((#:scm-term (#:atom s . _) l _ _)
     (off))
       
    ((#:term (#:atom w . _) () #f n m)
     #t)

    ((#:term (#:atom v . _) l #f n m)
     (g l))

    ((#:termvar w id l . _)
     (if (eq? v w)
	 #f
	 (g l)))

    ((#:variable w . _)
     (if (eq? v w)
	 #f
	 #t))

    ((#:lam-term  . l)
     (off))

    ((x) (g x))
    (_ #t)))
  
  (g x))
  

(define (register-variables x)
  (define o #f)
  (define (off) #t #;(set! o #t))
  (define (reg v id)
    (when (not o)
       (let ((r (hash-ref (fluid-ref *vs*) v #f)))
	 (if (or (not r) (pair? r))
	     (if (not r)
		 (hash-set! (fluid-ref *vs*) v (cons id 1))
		 (hash-set! (fluid-ref *vs*) v (cons (car r) 
						     (+ (cdr r) 1))))))))

  (define (g x gl)
    (when (not o)
    (match (pp 'g2 x)
    (()
     #'<cc>)    
    (((kind _ op _) x y n m)
     (g x gl) (g y gl))
    
    (((kind _ op _) x n m)
     (g x gl))
    
    ((#:group x)
     (g x gl))

    ((#:list l _ _)
     (g l gl))

    ((#:scm-term (#:atom s . _) l _ _)
     (g l gl))
       
    ((#:term (#:atom v . _) () #f n m)
     #t)

    ((#:term (#:atom v . _) l #f n m)
     (g l gl))

    ((#:termvar v id l . _)
     (reg v id)
     (g l gl))

    ((#:variable x id . _)
     (reg x id))

    ((#:lam-term  . l)
     (off))

    ((x) (g x gl))
    (_ #t))))
  (g x #t))

	
