(define-module (logic guile-log prolog global)
  #:use-module (logic guile-log)
  #:use-module (logic guile-log persistance)
  #:use-module (logic guile-log umatch)
  #:use-module (logic guile-log vlist)
  #:use-module (logic guile-log prolog error)
  #:use-module (logic guile-log dynamic-features)
  #:export (b_setval b_getval nb_setval nb_getval nb_current
		     setarg nb_setarg *globals-map*))

(define-named-object *globals-map* (make-fluid vlist-null))
(<wrap> add-vhash-dynamics *globals-map*)

(<define> (b_setval atom val)
  (<let> ((atom (<lookup> atom)))
    (cond
     ((<var?> atom)
      (instantiation_error))
     ((procedure? atom)
      (<let> ((r (vhashq-ref (fluid-ref *globals-map*) atom #f)))
	(if (not r)
	    (<and>
	     (<code> (set! r (gp-make-var))
		     (fluid-set! *globals-map*
				 (vhash-consq atom r 
					      (fluid-ref *globals-map*))))
	     (<set!> r '()))
	    <cc>)
	(<set0> r val)))

     ((string? atom)
      (<let> ((mod (current-module))
	      (sym (string->symbol atom)))
	(if (module-defined? mod sym)
	    (b_setval (module-ref mod sym) val)
	    (type_error "no-module-variable" atom))))
     (else
      (type_error "atom" atom)))))

(<define> (nb_setval atom val)
  (<let> ((atom (<lookup> atom)))
    (cond
     ((<var?> atom)
      (instantiation_error))
     ((procedure? atom)
      (<let> ((r (vhashq-ref (fluid-ref *globals-map*) atom #f)))
	(if (not r)
	    (<code>
	     (set! r (gp-make-var))
	     (fluid-set! *globals-map*
			 (vhash-consq atom r 
				      (fluid-ref *globals-map*))))
	    <cc>)
	;;Attributed variables is not handeled correctly here.
	(<set!> r (<cp> val))))
     ((string? atom)
      (<let> ((mod (current-module))
	      (sym (string->symbol atom)))
	(if (module-defined? mod sym)
	    (b_setval (module-ref mod sym) val)
	    (type_error "no-module-variable" atom))))
     (else
      (type_error "atom" atom)))))

(<define> (b_getval atom val)
  (<let> ((atom (<lookup> atom)))
    (cond
     ((<var?> atom)
      (instantiation_error))
     ((procedure? atom)
      (<recur> lp ()
	(<let> ((r (vhashq-ref (fluid-ref *globals-map*) atom #f)))
	  (if (not r)
	      (<and>
	       (existence_error "undefined_global_variable" atom) 
	       (lp))	     
	      (<and>
	       (<=> val ,(gp-lookup-1 r S)))))))
     ((string? atom)
      (<let> ((mod (current-module))
	      (sym (string->symbol atom)))
	(if (module-defined? mod sym)
	    (b_getval (module-ref mod sym) val)
	    (type_error "no-module-variable" atom))))
     (else
      (type_error "atom" atom)))))

(<define> (nb_getval atom val) (b_getval atom val))
	  
(<define> (nb_current name value)
  (<recur> lp ((l (vhash->assoc *globals-map*)))
    (<match> (#:mode + #:name nb_current) (l)
      (((,name . ,value) . l)
       <cc>)
      (()
       (<cut> <fail>)))))

(<define> (setarg i term value)
  (<let> ((i (<lookup> i)))
    (<<match>> (#:mode - #:name setarg) (term)
       ((x . l)
	(cond
	 ((= i 1)
	  (cond
	   ((<var?> x)
	    (<set0> x value))
	   (else
	    (<let> ((l (<lookup> term)))
	      (if (pair? l)
		  (<let> ((v (gp-make-var value)))
 		    (<code> (set-car! l v))
		    (<set!> v x)
		    (<set0> v value))
		  <fail>)))))
	 ((= i 2)
	  (cond
	   ((<var?> l)
	    (<set0> x value))
	   (else
	    (<let> ((l (<lookup> term)))
	      (if (pair? l)
		  (<let> ((v (gp-make-var value)))
 		    (<code> (set-cdr! l v))
		    (<set!> v x)
		    (<set0> v value))
		  <fail>)))))
	 (else
	  (type_error "wrong arity" term))))
		
       (#((f . args))
	(<cut>
	 (<recur> lp ((args args) (i i))
	   (if (> i 1)
	       (<match> (#:mode -) (args)
		 ((x . l) (<cut> (lp l (- i 1))))
		 (else
		  (type_error "wrong arity" term)))
	       (<let*> ((args (<lookup> args))
			(x    (gp-car args S)))
		 (if (<var?> x)
		     (<set0> x value)
		     (if (pair? args)
			 (<let> ((v (gp-make-var value)))
			   (<code> (set-car! args v))
			   (<set!> v x)
			   (<set0> v value))
			 (type_error "term" term))))))))
       (else
	(type_error "term" term)))))

(<define> (nb_setarg i term value)
  (<let> ((i (<lookup> i)))
    (<<match>> (#:mode - #:name nb_setarg) (term)
       ((x . l)
	(cond
	 ((= i 1)
	  (cond
	   ((<var?> x)
	    (<set!> x value))
	   (else
	    (<let> ((l (<lookup> term)))
	      (if (pair? l)	      
		  (<code> (set-car! l value))
		  <fail>)))))
	 ((= i 2)
	  (cond
	   ((<var?> l)
	    (<set!> l value))
	   (else
	    (<let> ((l (<lookup> l)))
	      (if (pair? l)	      
		  (<code> (set-cdr! l value))
		  <fail>)))))
	 (else
	  (type_error "wrong arity" term))))

       (#((f . args))
	(<cut>
	 (<recur> lp ((args args) (i i))
	   (if (> i 0)
	       (<match> (#:mode -) (args)
		 ((x . l) (<cut> (lp l (- i 1))))
		 (else
		  (type_error "wrong arity" term)))
	       (<let*> ((args (<lookup> args))
			(x    (gp-car args S)))
		 (if (pair? args)
		     (<code> (set-car! args (<cp> value)))
		     (<code> (gp-var-set! x (<cp> value)))))))))

       (else
	(type_error "term" term)))))
