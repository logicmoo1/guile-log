(define-module (logic guile-log prolog parser)
    #:use-module (logic guile-log procedure-properties)
    #:use-module (logic guile-log parsing operator-parser)
    #:use-module (ice-9 match)
    #:use-module (ice-9 time)
    #:use-module (ice-9 eval-string)
    #:use-module (logic guile-log vlist)
    #:use-module ((logic guile-log umatch)
		  #:select
		  (*current-stack* gp-unwind gp-unwind-tail gp-newframe))
    #:use-module (ice-9 pretty-print)
    #:use-module (logic guile-log dynamic-features)
    #:use-module (system syntax)

    #:use-module ((logic guile-log slask) #:select
                  (expand_term_dcg goal-expand assertz-source+ term
                                   term-init-variables term-get-variables
                                   found-scm var->code guard))

    #:use-module (logic guile-log parser)
    #:use-module (logic guile-log prolog pre)
    #:use-module (logic guile-log prolog names)
    #:use-module (logic guile-log prolog error)
    #:use-module (logic guile-log prolog symbols)
    #:use-module ((logic guile-log) 
		  #:select (<let> <pp> <scm> <code> <let*> 
				  <var> <=> <fail> <match>
				  <cut> <and> <or> <define>
				  <cc> <not> <if> <values>
				  <recur> <<match>> <lambda>
				  <catch> <ret> <format> S
				  (<_> . GL:_)))
    #:re-export (*prolog-file* get-refstr)
    #:export (prolog-parse define-parser-directive add-op rem-op
                           reset-operator-map
                           define-parser-directive-onfkn prolog-parse-read
                           ops->assq assq->ops 
                           get_prolog_operators_handle
			   opdata-ref
			   opdata-set!
                           get-ops
			   *standard-opmap*
			   *goal-expansions*
			   *prolog-ops*
			   *swi-standard-operators*
			   prolog-parse-module
			   *term-expansions*
			   expand_term
			   expand_goal
			   expand_term_0))



(define do-print #f)
(define pp
  (case-lambda
    ((s x)
     (when do-print
       (pretty-print `(,s ,(syntax->datum x))))
     x)
    ((x)
     (when do-print
       (pretty-print (syntax->datum x)))
     x)))

(define ppp
  (case-lambda
    ((s x)
     (when #t
       (pretty-print `(,s ,(syntax->datum x))))
     x)
    ((x)
     (when #t
       (pretty-print (syntax->datum x)))
     x)))

(eval-when (compile eval load)	   
  (define haserror #f)
  (define oldwarn warn)
  (define (warn . x)
    (set! haserror #t)
    (apply oldwarn x)))

(define-syntax-rule (wrap@ u code)
  (<scm>
    (let ((r code)
	  (u (<scm> u)))
      (if u
	  `(#:@ ,r ,@u)
	  r))))

(define %   (f-char #\%))
(define precom (f-seq (f-char #\/) (f-char #\*)))
(define podcom (f-seq (f-char #\*) (f-char #\/)))
(define cmplx-com (letrec ((f (lambda (n)
                                (f-seq!
                                 (f* (f-or! 
                                      (f-seq precom (Ds (f (+ n 1))))
                                      (f-not (f-or podcom precom))
                                      f-nl))
                                 podcom))))
                    (f-seq! precom (f 0))))
          
(define wf-char    (f-or! (f-char #\space) (f-char #\tab) f-nl))
(define -ws   (f* wf-char))
(define -ws+  (f+ wf-char))
(define comment (f-seq % (f* (f-not f-nl))))
(define ws+ (f-and! (f+ (f-or! comment cmplx-com -ws+))))
(define ws  (f-and! (f* (f-or! comment cmplx-com -ws+))))

(define first-var      (f-reg! "[A-Z_]"))
(define rest-var       (f-reg! "[a-zA-Z_0-9]"))
(define variable-tok   (mk-token (f-seq first-var (f* rest-var))))
		
(define first-atom (f-reg! "[a-z]"))
(define atom-tok   (mk-token (f-seq first-atom (f* rest-var))))
(define any (f-reg "."))

(define special (f-or! (f-reg "[][(),;\"'{}|@]")
		       (f-and 
			(f-seq (f-tag ".") ws+)
			(f-tag "."))))

;; SWI-PROLOG OPERATOR PRECEDENCE PARSER
(define expr* #f)
(define expr (Ds expr*))

(define (rem-op type op)
  (rem-operator *prolog-ops* type op))

(define (add-op prio spec op)
  (define (proj x)
    (cond
     ((symbol? x)
      (symbol->string x))
     ((procedure? x)
      (symbol->string (procedure-name x)))
     (else x)))

  (let* ((opp  (proj op))
	 (oppp (string->symbol opp)))
    (add-sym #f #f
	     (list #:atom oppp #f #f 0 0))
    (if (module-defined? (current-module) oppp)
	(let ((op (module-ref (current-module) oppp)))
	  (if (procedure? op)
	      (set-procedure-property! op 'prolog-operator opp))))
    (add-operator *prolog-ops* spec opp prio ws)))

(define (get-ops)
  (table->assq (fluid-ref *prolog-ops*)))

(define (opdata-ref) (fluid-ref *prolog-ops*))
(define (opdata-set! x) (fluid-set! *prolog-ops* x))

(define *prolog-ops* (make-opdata))

(add-fluid-dynamics (fluid-ref *current-stack*)
                    (lambda x #f)
                    (lambda x #t)
                    *prolog-ops*)
(<define> (get_prolog_operators_handle x) (<=> x *prolog-ops*))

(define tid 0)
(define (init-time)
  (set! tid (vector-ref (times) 0)))
(define (pk-time a)
  1 #;(pk `(time ,a ,(/ (- (vector-ref (times) 0) tid) 1e9))))
(define tid2 0)
(define (init-time2)
  (set! tid2 (vector-ref (times) 0)))
(define (pk-time2 a)
  1 #;(pk `(time ,a ,(/ (- (vector-ref (times) 0) tid2) 1e9))))

(define (ops->assq)
  (define (trfkn x)
    (match x
      ((x . l)
       #`(cons #,(trfkn x) #,(trfkn l)))
      (x
       (if (procedure? x)
           #'ws
           #`'#,(datum->syntax #'*prolog-ops* x)))))
           
  (trfkn (table->assq (fluid-ref *prolog-ops*))))

(define (assq->ops a)
  (fluid-set! *prolog-ops* (assq->table a)))

(for-each
 (lambda (x)
   (match x
    ((a b c) (add-operator *prolog-ops* a c b ws))))
 `((xfx 1200 -->)
   (xfx 1200 :-)
   (fx  1200 :-)
   (fx  1200 ?-)
   (xfy 1100 ";")
   (xfy 1100 ";;")
   (xfy 1100 "|")
   (fy  1100 "|")
   (xfy 1050 ->)
   (xfy 1050 -i>)
   (xfy 1050 *->)
   (xfy 1000 ",")
   (yfx 1000 ",,")
   (fy  900  "\\+")
   ,@(map (lambda (x) `(xfx 700 ,x))
	  '(<= 
            < = =.. =@= =:= =< == ← <= → => ?=
            "=\\=" > >= @< @=< @> @>= "\\=" "\\==" is ∈ ∉ ∋
	      ⊂ ⊆ ⊃ ⊇ ≡ ⊈ ⊄ ⊉ ⊅))
   (xfy 400 :)
   ,@(map (lambda (x) `(yfx 500 ,x)) '(+ - ∪ ⊔ ⊕ ∖ "/\\" "\\/" xor))
   ,@(map (lambda (x) `(yfx 400 ,x)) '(∩ ∖ ∖∖ * / // rdiv << >> mod rem))
   (xfx 200 **)
   (xfy 200 ^)
   (xf  200 ᶜ)
   ,@(map (lambda (x) `(fy 200 ,x)) '(- + "\\"))))

(define *swi-operators-fluid* (make-opdata))

(for-each
 (lambda (x)
   (match x
    ((a b c) (add-operator *swi-operators-fluid* a c b ws))))
 `((xfx 1200 -->)
   (xfx 1200 :-)
   (fx  1150 :-)
   (fx  1150 ?-)
   (xfy 1100 ";")
   (xfy 1100 ";;")
   (xfy 1100 "|")
   (fy  1100 "|")
   (xfy 1050 ->)
   (xfy 1050 -i>)
   (xfy 1050 *->)
   (xfy 1000 ",")
   (yfx 1000 ",,")
   (xfx 990  ":=")
   (fy  900  "\\+")
   ,@(map (lambda (x) `(xfx 700 ,x))
	  '(< = =.. =@= "\\=@=" <= ← → => ?=
	      =:= =< == "=\\=" > >= @< @=< @> @>= "\\=" "\\==" as is 
	      ∈ ∉ ∋ ⊂ ⊆ ⊃ ⊇ ≡ ⊈ ⊄ ⊉ ⊅))
   (xfy 600 :)
   ,@(map (lambda (x) `(yfx 500 ,x)) '(+ - ∪ ⊔ ⊕  "/\\" "\\/" xor))
   (fx 500 ?)
   ,@(map (lambda (x) `(yfx 400 ,x)) '(* ∩ ∖ ∖∖ / // div rdiv << >> mod rem))
   (xfx 200 **)
   (xfy 200 ^)
   ,@(map (lambda (x) `(fy 200 ,x)) '(- + "\\"))
   (fx  1  "$")))

(define *swi-standard-operators* (fluid-ref *swi-operators-fluid*))

(define *standard-opmap* (fluid-ref *prolog-ops*))
(define (reset-operator-map)
  (fluid-set! *prolog-ops* *standard-opmap*))

(define fop    ((mk-fop *prolog-ops*) '(fy fx xfx xfy yfx yf xf)))
(define funop  ((mk-fop *prolog-ops*) '(fy fx)))
(define fbinop ((mk-fop *prolog-ops*) '(xfx xfy yfx)))
(define opsym
  (<p-lambda> (c)
   (.. (p) (fop c))
   (<match> (#:mode - #:name 'opsym) ((pp 'op p))
     ((_ _ op _)
      (<cut> (<and> (<p-cc> (<scm> op))))))))
     
(define symbolic-1 (f-not! (f-or! wf-char special rest-var)))
(define symbolic (letrec ((sym* 			   
			   (f-or! 
			    (f-seq! symbolic-1 (Ds sym*))
			    f-true)))
                   (mk-token (f-seq symbolic-1 sym*))))
  

(define quotes 
  (mk-token (f-seq 'quotes (f-char #\') (f* (f-reg! "[^']")) (f-char #\'))))

(define hex      (mk-token (f-seq "0x" (f+ (f-reg "[0-9abcdef]")))))
(define integer  (f+ (f-reg! "[0-9]")))
(define fraction (f-seq integer  (f-or! (f-seq (f-char! #\.) integer) 
					f-true)))
(define exponent (mk-token
		   (f-seq!! fraction 
			    (f-or!
			     (f-seq
			      (f-char! #\e) 
			      (f-or! (f-char! #\+)
				     (f-char! #\-)
				     f-true)
			      integer)
			     f-true))))

(define (mk-id S c cc) cc)
(define key-body
  (let ((body  (f+ (f-not! (f-or ws+ (f-reg "[,()]"))))))
    (mk-token body)))

(define number
  (p-freeze 'number
    (<p-lambda> (c)
      (.. (c) (ws c))
      (<let> ((n N) (m M))
        (.. (c) ((f-or! hex exponent) c))
	(.. (q) (ws c))
        (<p-cc> `(#:number ,(string->number c) ,n ,m))))
    mk-id))

(define keyword
  (let ((sharp (f-tag "#"))
	(colon (f-char #\:)))
    (p-freeze 'keyword
      (<p-lambda> (c)
        (.. (c) (ws c))
	(<let> ((n N) (m M))
	  (.. (c) (sharp c))
	  (.. (c) (colon c))
	  (.. (c2) (key-body c))
	  (.. (c)  (ws c2))	    
	  (<p-cc> `(#:keyword ,(symbol->keyword 
				(string->symbol c2)) ,n ,m))))
      mk-id)))


(define f-quote   (f-char  #\'))
(define f-dquote  (f-char  #\"))
(define f-quote!  (f-char! #\'))
(define f-dquote!  (f-char! #\"))
(define f-esc     (f-char  #\\))
(define not-quote (f-not! (f-or f-quote f-esc)))
(define not-dquote (f-not! (f-or f-dquote f-esc)))
(define esc       (f-seq f-esc (f-reg! ".")))
(define f-hex
  (let ((f-x (f-char #\x)))
    (define hex-body (mk-token (f+ (f-not! f-esc))))
    (<p-lambda> (c)
      (<var> (cc cc*)
        (.. (c1) (f-esc c))
        (.. (c1) (f-x c1))
        (.. (c*) (hex-body cc))
        (<let> ((x (integer->char (string->number c* 16))))
           (.. (c1) (f-esc c1))
           (<=> c (x . cc*))
           (<p-cc> cc*))))))

(define str-body  (mk-token (f* (f-or! not-quote 
                                       f-hex
                                       (f-seq! f-quote f-quote!)
                                       esc))))

(define dstr-body  (mk-token (f* (f-or! not-dquote 
                                        f-hex
                                        (f-seq! f-dquote f-dquote!)
                                        esc))))




(define chr-prefix (f-tag "0'"))
(define chr-body (mk-token (f-reg! ".")))
(define char 
  (p-freeze 'char
    (<p-lambda> (c)
      (.. (c) (ws c))
      (<let> ((n N) (m M))
        (.. (c)  (chr-prefix c))
        (.. (c*) (chr-body   c))
        (.. (c)  (ws c*))
        (<p-cc> `(#:number ,(char->integer (string-ref c* 0)) ,n ,m))))
    mk-id))

(define (mk-string x)
  (list #:string (if (symbol? x)
                     (symbol->string x)
                     x) 0 0))

(define (mk-list . l)
  (let ((l (reverse l)))
    (let lp ((l (cdr l)) (r (car l)))
      (if (pair? l)
          (lp (cdr l) (list (list xfy 1000 "," #\,) (car l) r 0 0))
          r))))

(define @tag-body
  (let ((l (f-tag "("))
        (r (f-tag ")")))
    (<p-lambda> (c)
       (.. (c) (ws c))
       (<or>
         (<and>
          (.. (c)  (l c))
          (.. (cx) (expr c))
          (.. (c)  (r cx))
          (.. (c)  (ws c))
          (<p-cc> cx))
         (<and>
          (.. (cx) ((f-or atom qstring dstring) c))
	  (.. (c2) ((f-seq ws l) cx))
          (.. (cy) ((f-or atom qstring dstring) c))
	  (.. (c2) ((f-seq ws r) cy))
          (<p-cc> (mk-list (mk-string "language")
                           (mk-string "prolog")
                           (mk-string "modules")
                           (mk-string (list-ref cx 1))
			   (mk-string (list-ref cy 1)))))
         (<and>
          (.. (cx) ((f-or atom qstring dstring) c))
          (<p-cc> (mk-list (mk-string "language")
                           (mk-string "prolog")
                           (mk-string "modules")
                           (mk-string (list-ref cx 1)))))))))

(define @tag
  (let* ((@  (f-tag "@"))
	 (!  (f-tag "!"))
	 (@* (f-and (f-not fop) @))
	 (@@ (f-tag "@@")))
    (<p-lambda> (c)
      (<let> ((n N) (m M))
       (<and>
        (.. (c) (ws c))
        (<or>
         (<and>
          (.. (c)  (@@ c))
          (.. (cx) (@tag-body c))
          (.. (c)  (ws c))
          (<p-cc> `(@@ ,cx)))
	 
         (<and>
          (.. (c)  (@* c))
          (.. (cx) (@tag-body c))
          (.. (c)  (ws c))
          (<p-cc> `(@ ,cx)))

	 (<and>
	  (.. (c) (@@  c))
	  (.. (c) (ws+ c))
	  (<p-cc> `(@@ ,(list->vector (module-name (current-module))))))
	 
	 (<and>
	  (.. (c) (@  c))
	  (.. (c) (ws+ c))
	  (<p-cc> `(@ ,(list->vector (module-name (current-module))))))
	 (<and>
	  (.. (c) (@@  c))
	  (.. (c) (! c))
	  (<p-cc> `(@@ #:current-module)))
	 
	 (<and>
	  (.. (c) (@  c))
	  (.. (c) (! c))
	  (<p-cc> `(@ #:current-module)))

         (<p-cc> #f)))))))

(define qstring
  (p-freeze 'qstring
    (let ((l (f-tag "("))
          (r (f-tag ")")))
      (<p-lambda> (c)
        (.. (c) (ws c))
        (.. (c) (f-quote c))
        (<let> ((n N) (m M))
           (.. (c*) (str-body c))
           (.. (c)  (f-quote c*))
           (.. (c) (ws c))
           (.. (u) (@tag c))
           (.. (c) (ws u))
           (<p-cc> (<scm>
		    (if u 
			`(#:atom ,(string->symbol c*)
				 ,(car u) ,(cadr u) ,n ,m)
			`(#:atom ,(string->symbol c*) #f #f
				 ,n ,m)))))))
    mk-id))

(define dstring
  (p-freeze 'dstring
    (<p-lambda> (c)
      (.. (c) (ws c))
      (.. (c)  (f-dquote c))
      (<let> ((n N) (m M))
         (.. (c*) (dstr-body c))
         (.. (c)  (f-dquote c*))
         (.. (c) (ws c))
         (.. (u) (@tag c))
         (<p-cc> 
          (let ((r `(#:string ,c* ,n ,m)))
            (if u
                `(#:atom 
		  ,(string->symbol c*)
		  ,(car u) ,(cadr u) ,n ,m)
                r)))))
    mk-id))

(define vid 0)
(define (get-vid)
  (set! vid (+ vid 1))
  vid)

(define variable  
  (p-freeze 'variable
    (<p-lambda> (c)
      (.. (c) (ws c))
      (<let> ((n N) (m M))
        (.. (c) (variable-tok c))
	(.. (q) (ws c))
        (.. (u) (@tag q))
        (<p-cc>
         (wrap@ u `(#:variable ,(string->symbol c) ,(get-vid) ,n ,m)))))
    mk-id))
#;
(define symbolic-tok
  (<p-lambda> (c)
      (.. (c) (ws c))
      (<let> ((x X) (xl XL) (n N) (m M))
        (..  (c1) (symbolic c))	
 	(<let*> ((an N) (am M))
	  (<or> 
	   (<and>
	    (<values> (bx bxl bn bm bc) (fop x xl n m c))
            (<cut>
	      (when (or (> am bm) (and (= am bm) (> an bn)))
	       (.. (q) (ws c1))
	       (.. (u) (@tag q))
	       (<p-cc> 
		(if u
		    `(#:atom     
		      ,(string->symbol c1) ,(car u) ,(cadr u) ,n ,m)
		    `(#:atom ,(string->symbol c1) #f #f ,n ,m))))))
	   (<and>
	    (.. (q) (ws c1))
	    (.. (u) (@tag q))
	    (<p-cc> 
	     (if u
		 `(#:atom     
		   ,(string->symbol c1) ,(car u) ,(cadr u) ,n ,m)
		 `(#:atom ,(string->symbol c1) #f #f ,n ,m)))))))))

(define (must-be-atom f)
  (f-seq f (f-not* (f-seq ws (f-tag "(")))))

(define symbolic-tok
  (<p-lambda> (c)
      (.. (c) (ws c))
      (<let> ((x X) (xl XL) (n N) (m M))
        (..  (c1) ((f-seq symbolic (f-and (f-seq ws special) f-true)) c))	
	(..  (q) (ws c1))
	(..  (u) (@tag q))
	(<p-cc> 
	 (if u
	     `(#:atom     
	       ,(string->symbol c1) ,(car u) ,(cadr u) ,n ,m)
	     `(#:atom ,(string->symbol c1) #f #f ,n ,m))))))

(define symsym (must-be-atom symbolic-tok))

(define symbolic-tok2
  (<p-lambda> (c)
      (.. (c) (ws c))
      (<let> ((x X) (xl XL) (n N) (m M))
        (..  (c1) (symbolic c))	
	(.. (q) (ws c1))
	(.. (u) (@tag q))
	(<p-cc> 
	 (if u
	     `(#:atom     
	       ,(string->symbol c1) ,(car u) ,(cadr u) ,n ,m)
	     `(#:atom ,(string->symbol c1) #f #f ,n ,m))))))

(define op-tok
  (<p-lambda> (c)
      (.. (c) (ws c))
      (<let> ((n N) (m M))
        (.. (c) (opsym c))
	(.. (q) (ws c))
        (<p-cc> `(#:atom ,(string->symbol c)  #f #f ,n ,m)))))

(define atom
  (p-freeze 'atom
    (<p-lambda> (c)
      (.. (c) (ws c))
      (<let> ((n N) (m M))
        (.. (c1) (atom-tok c))
        (.. (c) (ws c1))
        (.. (u) (@tag c))        
        (<p-cc> 
         (if u
             `(#:atom ,(string->symbol c1) ,(car u) ,(cadr u) ,n ,m)
             `(#:atom ,(string->symbol c1) #f #f ,n ,m)))))
    mk-id))

(define isNonFkn
  (p-freeze 'exp
    (<p-lambda> (c)		
      (.. (c2) (expr c))
      (when (match (<scm> c2) 
		 (((_ _ "," _) . _) #t)
		 (_                 #f)))
      (<p-cc> c2))
    mk-id))

(define paranthesis
  (let ((l (f-tag "("))
	(r (f-tag ")")))
    (p-freeze 'parenthesis
      (<p-lambda> (c)
        (.. (c1) (ws c))
	(.. (c2) (l    c1))
	(.. (c3) (expr c2))
	(.. (c4) (r    c3))
	(.. (c5) (ws   c4))
        (.. (u)  (@tag c5))
	(<or>
	 (<and>
	  (.. (c6) (paranthesis u))
	  (<p-cc> `(#:fknfkn (#:group ,(<scm> c3)) ,c6)))
	 (<p-cc> (wrap@ u `(#:group ,(<scm> c3))))))
      mk-id)))



(define term-tok
  (let ((l  (f-tag "("))
	(r  (f-tag ")"))
        (lb (f-tag "["))
	(rb (f-tag "]")))
    (p-freeze 'term
      (<p-lambda> (c)
        (.. (c0) (ws c))
	(<let> ((n N) (m M))
          (xx (c1) (<or> (.. (atom c0))
			 (.. (qstring c0))
			 (.. ((f-and 
			       (f-not (f-and (f-not fbinop) funop))
			       symbolic-tok)
			      c0))))
	  (.. (c2) ((f-seq ws l)    c1))
	  (.. (c3) ((f-or expr ws) c2))
	  (.. (c4) (r    c3))
	  (.. (c5) (ws   c4))
          (xx (c6)
              (<or>               
               (<and> 
                (.. (a1) (lb c5))
                (.. (a2) ((f-or expr ws) '()))
                (.. (a3) (rb a2))
                (<p-cc> a2))
               (<p-cc> #f)))
	  (.. (c7) (ws c6))
          (.. (u)  (@tag c7))
	  (<p-cc> 
           (wrap@ u (if (eq? c2 c3)
			`(#:term ,c1 () ,(<scm> c6) ,n ,m)
			`(#:term ,c1 ,(<scm> c3) ,(<scm> c6) ,n ,m))))))
      mk-id)))

(define term-unop
  (let ((l  (f-tag "("))
	(r  (f-tag ")")))
    (p-freeze 'term-unop
      (<p-lambda> (c)
        (.. (c0) (ws c))
	(<let> ((n N) (m M))
	  (.. (c1) ((f-and 
		     (f-not fbinop) 
		     funop
		     symbolic-tok)
		    c0))
	  (.. (c2) ((f-seq ws (f-and l paranthesis)) c1))
	  (<p-cc> 
           `(#:term ,c1 ,(<scm> c2) #f ,n ,m))))
      mk-id)))

(define scm-tok
  (let* ((l (f-tag "["))
         (r (f-tag "]"))
         (e (mk-token (f* (f-or (f-seq ws+ (f-ichar " ")) (f-not! r))))))
    (p-freeze 'scm-term
      (<p-lambda> (c)
        (.. (c0) (ws c))
	(<let> ((n N) (m M))
          (.. (c1) (atom c0 ))
	  (.. (c2) ((f-seq l ws) c1))	 
	  (.. (c3) (e   c2))         
	  (.. (c4) (r   c3))
	  (.. (c5) (ws   c4))
	  (if (eq? c2 c3)
	      (<p-cc> `(#:scm-term ,c1 () ,n ,m))
	      (<p-cc> `(#:scm-term ,c1 ,(<scm> c3) ,n ,m)))))
      mk-id)))

(define brack
  (let* ((l  (f-tag "{"))
         (r  (f-tag "}"))
         (l! (f-tag! "{"))
         (r! (f-tag! "}")))
    (letrec ((body (lambda (n)
                     (f*
                      (f-or!				
                       (f-seq l! (Ds (body (+ n 1))) r!)
                       f-nl!
                       (f-seq (f-not! (f-or l r))))))))
      (mk-token (body 0)))))

(define lam-tok
  (let* ((l  (f-tag "{"))
         (r  (f-tag "}"))
         (l! (f-tag! "{"))
         (r! (f-tag! "}"))
         (e  brack))
    (p-freeze 'lam-tok
      (<p-lambda> (c)
        (.. (c0) (ws c))
        (<let> ((n N) (m M))
          (.. (c1) (atom c0))
	  (.. (c2) (l    c1))
          (xx (cl) (<or>
                    (<and> 
                     (.. (a) (l c2))
                     (<p-cc> #t))
                    (<p-cc> #f)))
	  (xx (c3)
	      (<or>
	       (<and>                 
		(.. (u) ((f-seq expr ws (f-and r f-true)) cl))
		(if (match u (((_ _ "|" _) . l) #f) (else #t))
		    (<p-cc> u)
		    (.. (e cl))))
	       (.. (e cl))
               (<p-cc> c2)))
	  (.. (c4) (r c3))
          (xx (c5) (if cl
                       (.. (r c4))
                       (<p-cc> #f)))
          (.. (u)  (@tag c5))
          (<p-cc> 
           (wrap@ u (if (eq? c2 c3)
			`(#:lam-term ,c1 null         ,cl  ,n ,m)
			`(#:lam-term ,c1 ,(<scm> c3)  ,cl  ,n ,m))))))
      mk-id)))

(define set-tok
  (let* ((l  (f-tag "{"))
         (r  (f-tag "}"))
         (l! (f-tag! "{"))
         (r! (f-tag! "}"))
         (e  (letrec ((body (lambda (n)
			      (f*
			       (f-or
				ws+
				(f-seq l! (Ds (body (+ n 1))) r!)
				(f-not! (f-or l r)))))))
	       (mk-token (body 0)))))

    (p-freeze 'set-tok
      (<p-lambda> (c)
        (.. (c0) (ws c))
	(let ((n N) (m M))
          (<or>
           (<and>
            (.. (c1) ((f-seq l r) c0))
            (<p-cc> `(#:lam-term #f null #f ,n ,m)))
           (<and>
            (.. (c2) (l    c0))
            (xx (c3) (<or> (.. (expr c2)) (<p-cc> '())))
            (.. (c4) (r    c3))
            (.. (c5) (ws   c4))
            (<p-cc>           
             `(#:lam-term #f ,(<scm> c3) #f ,n ,m))))))
      mk-id)))

(define true/false
  (let ((tag (mk-token (f-seq (f-or (f-tag! "#t") (f-tag! "#f"))
			      (f-not* rest-var)))))			      
    (p-freeze 'true/false
      (<p-lambda> (c)
        (.. (c0) (ws c))
	(<let> ((n N) (m M))
	  (.. (c)  (tag c0))
	  (<p-cc>
	   `(#:keyword ,(if (equal? c "#t") #t #f) ,n ,m))))
      mk-id)))

(define termvar-tok
  (let ((l (f-tag "("))
	(r (f-tag ")")))
    (p-freeze 'termvar
      (<p-lambda> (c)
        (.. (c0) (ws c))
	(<let> ((n N) (m M))
	  (.. (c1) (variable-tok c0))
	  (.. (c2) ((f-seq ws l)    c1))
	  (.. (c3) ((f-or expr ws) c2))
	  (.. (c4) (r    c3))
	  (.. (c5) (ws c4))
          (.. (u)  (@tag c5))
	  (<p-cc> 
           (wrap@ u (if (eq? c2 c3)
                        `(#:termvar ,(string->symbol c1) ,(get-vid) () ,n ,m)
                        `(#:termvar ,(string->symbol c1) ,(get-vid) 
				    ,(<scm> c3) ,n ,m))))))
      mk-id)))

(define termop-tok
  (let ((l (f-tag "("))
	(r (f-tag ")")))
    (p-freeze 'termop
      (<p-lambda> (c)
        (.. (c0) (ws c))
	(<let> ((n N) (m M))
	  (xx (cx) (<if> (<not> 
			  (<and>
			   (.. (q1) (funop c0))
			   (.. (q2) (ws q1))
			   (.. (q3) (l q2))))
			 (<p-cc> 1)
			 <fail>))
          (.. (c1) (symbolic c0))
	  (.. (cq) (ws c1))
	  (.. (c2) (l    c1))
	  (.. (c3) ((f-or expr ws) c2))
	  (.. (c4) (r    c3))
	  (.. (c5) (ws   c4))
          (.. (u)  (@tag c5))
          (if (match (<scm> c3)
                (((_ _ "," _) _ _ _ _)
                 #t)
                (_ 
                 #f))
              (<p-cc>
               (wrap@  u `(#:term (#:atom ,(string->symbol c1) ,n ,m)
                                  ,(<scm> c3) #f ,n ,m)))
              <fail>)))
      mk-id)))

(define term-binop-tok
  (let ((l (f-tag "("))
	(r (f-tag ")")))
    (p-freeze 'term-binop
      (<p-lambda> (c)
        (.. (c0) (ws c))
	(<let> ((n N) (m M))
          (.. (p)  (fbinop c0))
	  (.. (c2) (l    GL:_))
	  (.. (c3) ((f-or expr ws) c2))
	  (.. (c4) (r    c3))
	  (.. (c5) (ws   c4))
          (.. (u)  (@tag c5))
          (<match> (#:mode - #:name 'term-binop) ((<scm> c3))
                (((_ _ "," _) x y _ _)
                 (<cut> 
                  (<p-cc> 
                   (wrap@ u (list (<scm> p) x y n m)))))
                (_ 
                 (<cut> <fail>)))))
      mk-id)))

(define termstring-tok
  (let ((l (f-tag "("))
	(r (f-tag ")")))
    (p-freeze 'termstring
      (<p-lambda> (c)
        (.. (c0) (ws c))
	(<let> ((n N) (m M))
	  (.. (c1) (dstring c0))
	  (.. (c2) (l    c1))
	  (.. (c3) ((f-or expr ws) c2))
	  (.. (c4) (r    c3))
	  (.. (c5) (ws c4))
          (.. (u)  (@tag c5))
	  (if (eq? c2 c3)
	      (<p-cc> `(#:termstring ,c1 () ,n ,m))
              (<let*> ((c3 (<scm> c3))
                (a (match c3
                     (((_ _ "," _) x y _ _)
                      (list x y))
                     (_ #f))))
                       (<p-cc> 
                        (wrap@ u (if a
                                     `(#:termstring ,c1 ,a ,n ,m)
                                     `(#:termstring ,c1 ,c3 ,n ,m))))))))
      mk-id)))

(define list-tok 
  (let ((l (f-tag "["))
	(r (f-tag "]")))
    (p-freeze 'list
      (<p-lambda> (c)
	(.. (c1) (ws c))
	(<let> ((n N) (m M))
	  (.. (c2) (l    c1))
	  (.. (c3) ((f-or expr ws) c2))
	  (.. (c4) (r    c3))
	  (.. (c5) (ws   c4))
          (.. (u)  (@tag c5))
          (<p-cc> 
           (wrap@ u (if (eq? c2 c3)
                        `(#:list () ,n ,m)
                        `(#:list ,(<scm> c3) ,n ,m))))))
      mk-id)))


#;(define tok (f-or! list-e term-tok termvar-tok atom symbolic variable number))
(define tok (f-or! 'token
		   paranthesis keyword
                   char list-tok true/false 
		   termvar-tok #;term-binop-tok #;termop-tok
                   termstring-tok term-tok term-unop scm-tok lam-tok
                   set-tok
		   number qstring dstring atom variable symsym
		   #;op-tok))

(define e (mk-operator-expression ws tok (f-or! op-tok symbolic-tok2)
				  *prolog-ops*))
(set! expr* (<p-lambda> (c) (.. (e 1200))))

(define (read-1 stx x) x)
(define (f-read-1 stx m)
  (<p-lambda> (c)
    (.. (d) (m GL:_))
    (<p-cc> (read-1 stx (<scm> d)))))

(define auto-comma (f-seq ws ":-" ws
			  (f-cons* expr (f-seq ws expr)
				   (ff* (f-seq ws expr)))))

(define (module-analysis x)
  (match x
    ((#:term _ (op (#:atom name . _) (#:list l . _) . _) . _)
     (let lp ((l (get.. "," l)) (r '()))
       (match l
	 (((#:term (#:atom 'op . _) l . _) . u)
	  (lp u (cons
		 (cons #:op
		       (let lp2 ((l (get.. "," l)))
			 (match l
			   (((#:atom x . _) . l)
			    (cons x (lp2 l)))
			   (((#:number x . _) . l)
			    (cons x (lp2 l)))
			   (_ '()))))
		 r)))
	 (((#:term (#:atom 'term . _) l . _) . u)
	  (lp u (cons
		 (cons #:term
		       (let lp2 ((l l))
			 (match l
			   ((#:atom x . _)
			    (cons x '())))))
		 r)))
	 (((#:term (#:atom 'goal . _) l . _) . u)
	  (lp u (cons
		 (cons #:goal
		       (let lp2 ((l l))
			 (match l
			   ((#:atom x . _)
			    (cons x '())))))
		 r)))
	 (((#:term (#:atom 'dyn . _) l . _) . u)
	  (lp u (cons
		 (cons #:dyn
		       (let lp2 ((l l))
			 (match l
			   ((#:atom x . _)
			    (cons x '())))))
		 r)))
	 (((op (#:atom nm . _) . _) . l)
	  (lp l (cons nm r)))
	 (((op (#:group (#:atom nm . _)) . _) . l)
	  (lp l (cons nm r)))
	 (()
	  (cons name (reverse r))))))))

(define module-term
  (<p-lambda> (c)
    (.. (d) (term-tok c))
    (if (match (<scm> d) 
	  ((#:term (#:atom 'module . _) . _)
	   #t)
	  (_ 
	   #f))
	(<p-cc> (module-analysis (<scm> d)))
	<fail>)))

(define module-tok       (f-or! (f-seq ws ":-" ws module-term ws ".") 
				(f-out #f)))
(define skip-module-tok  f-true)
(define (resurge x n m)
  (match x
    ((a) a)
    ((a . l)
     `((xfy _ "," _) ,a ,(resurge l n m) ,n ,m))
    (()  '())))
  
(define auto-comma-tok
  (<p-lambda> (c)
     (.. (c) (ws c))
     (<let> ((n N) (m M))
       (.. (c) (auto-comma c))
       (<p-cc> `((fx _ ,":-" _) (#:term ,(car c) (#:list
						  ,(resurge (cdr c) n m)
						  ,n ,m)
					#f ,n ,m)
		 ,n ,m)))))

(define endpoint (f-or
		  (f-and (f-not (f-seq "." (f-and (f-reg ".") (f-not ws+))))
			 ".")
		  f-eof))

(define (fp m) (f-seq ws (f-or! (f-seq m ws endpoint ws)
				(f-seq  auto-comma-tok ws endpoint ws))))
(define (f-parse-1 stx m)
  (<p-lambda> (c)
   (<let> ((mm M))
      (.. (d) ((fp m)  GL:_))
      (<p-cc> (cons (pp 'p1 (parse-1 mm S stx (pp 'man (<scm> d)))) c)))))

;; For now we do not do anything here but it is possible to implement
;; parser directions here
(define-syntax-rule (define-parser-directive (f . a) . code)
  (begin
    (define f (guard (lambda a . code)))
    (set-object-property! f 'prolog-directive #t)))

(define-syntax-rule (define-parser-directive-onfkn on (f . a) . code)
  (begin
    (define f (guard (lambda a . code)))
    (set-object-property! on 'prolog-directive f)))

(define *term-expansions* (make-fluid '()))
(define *goal-expansions* (make-fluid '()))


(define (ass stx x)
  (fluid-set! found-scm #f)
  (init-time2)
  (let ((res 
  (let lp ()
    (catch #t
	   (lambda ()
	     (term-init-variables)
	     (let ((code (pp (term stx (pp 1 x))))
		   (vs   (map
			  (lambda (x)
			    (list (datum->syntax stx x)
				  #`((@ (logic guile-log umatch)
					gp-make-var))))
			  (term-get-variables))))
	       (if (not (fluid-ref found-scm))
		   (pp 3 (eval
			  (pp 4 `((@@ (logic guile-log prolog base) stp)
				  ((@ (guile) let-syntax)
				   ((f ((@ (guile) lambda) (z)
					,#`(let #,vs `#,code))))
				   f)))
			  (current-module)))
		   #f)))
	   (lambda x
	     (match x
		    (('unbound-variable _ _ (nm) _)
		     (make-sym (current-module) nm)
		     (lp))
		    (_
		     (pk 'fault-in-ass x)
		     (error x))))))))
    (pk-time2 'ass)
    res))

(define-syntax-rule (with line (f s b c a ...))
  (let* ((u s)
	 (u (gp-newframe u)))
    (let ((r #;(f u b c a ...) 
	   (scheme-wrapper
	     (lambda ()
	       ((<lambda> ()
		  (<catch> 'prolog #f             
		    (<lambda> () (f a ...))
		    (<lambda> (tag next l)
		      (<format> #t
			"DYNAMIC ERROR:~%=> ~a at line ~a~%~%"
			(var->code (<scm> l)) line)
		      (<ret> #f))))
		u b c)))))
      (gp-unwind-tail u)
      r)))



;; This will log all function is a current database in non boot code
(<define> (all x) <fail>)
(<define> (alla r)
   (<recur> lp ((r r))
      (if (pair? r)
	  (<and> (<or> (all (car r)) <cc>) <cut> (lp (cdr r)))
	  <cc>)))

(define laster (<lambda> (x y change) (if change (<=> x y) <fail>)))

;; this will translate dcg rules in the end in non boot code
(set! (@@ (logic guile-log slask) expand_term_dcg)
  (<lambda> (x y) <fail>))

(<define> (term_exp x y) <fail>)
(<define> (goal_exp x y) <fail>)

(<define> (expand_term_0 x z)
  (<let> ((L (cons expand_term_dcg
		   (append (fluid-ref *term-expansions*)
			   (list term_exp)))))
    (<recur> lp ((l L) (is-tr #f) (x (list x)))
      (if (pair? l)	  
	  (<recur> lp2 ((x x) (r '()) (is-tr2 #f))
	    (<code> (pp (list 'texpand x l)))
	    (if (pair? x)
		(<var> (y)
		   (<if> ((car l) (car x) y)
			 (lp2 (cdr x) (append (reverse (<scm> y)) r) #t)
			 (lp2 (cdr x) (cons (car x) r) is-tr2)))
		(if is-tr2
		    (lp (cdr l) #t    (reverse r))
		    (lp (cdr l) is-tr (reverse r)))))

	  (<recur> lp ((x x) (is-tr is-tr) (r '()))
	     (if (pair? x)
		 (<let> ((xx (car x)))
		   (<var> (w)		 
		     (<if> (<and>
			    (when goal-expand)
			    (<code> 'goal-expand)
			    (goal-expand xx (append 
					     (fluid-ref *goal-expansions*) 
					     (list goal_exp)) w))
			   (lp (cdr x) #t     (cons (<scm> w) r))
			   (lp (cdr x) is-tr  (cons xx r)))))
		 (<and>
		  (alla (reverse r))
		  (laster (reverse r) z is-tr))))))))

(<define> (expand_term xin z)
  (<let> ((L (cons expand_term_dcg
		   (fluid-ref *term-expansions*))))
    (<recur> lp ((l L) (is-tr #f) (x (list xin)))
      (if (pair? l)
	  (<recur> lp2 ((x x) (r '()) (is-tr2 #f))
	    (if (pair? x)
		(<var> (y)
		   (<if> ((car l) (car x) y)
			 (lp2 (cdr x) (append (reverse (<scm> y)) r) #t)
			 (lp2 (cdr x) (cons (car x) r) is-tr2)))
		(if is-tr2
		    (lp (cdr l) #t    (reverse r))
		    (lp (cdr l) is-tr (reverse r)))))
	  (<recur> lp ((x x) (is-tr is-tr) (r '()))
	     (if (pair? x)
		 (<let> ((xx (car x)))
		   (<var> (w)		 
		     (<if> (<and>
			    (when goal-expand)
			    (goal-expand xx (append 
					     (fluid-ref *goal-expansions*) 
					     (list goal_exp)) w))
			   (lp (cdr x) #t     (cons w r))
			   (lp (cdr x) is-tr  (cons xx r)))))
		 (if is-tr
		     (<=> (z . _) ,(reverse r))
		     (<=> z       ,xin))))))))

(<define> (expand_goal x y)
  (<if> (goal-expand x
		     (append 
		      (fluid-ref *goal-expansions*) 
		      (list goal_exp)) y)
	<cc>
	(<=> x y)))
		  
(<define> (expand-0 line stx x)
  (<var> (y m xx)
     ;(<pp> `(expand-0))
     ;(<<match>> (#:mode -) (x)
     ; (#((_ x _)) (<pp> x))
     ; (_          <cc>))
     (<code> (pp 'expand-0))	 
     (expand_term_0 x y)
     (<code> (pp (list 'expand-0-termed y)))
     ;(<pp> `(expand-0-y ,(pk y)))
     (<recur> lp ((y y) (r '()))
       (<<match>> (#:mode - #:name expand) (y)
	 ((x . l)
	  (<and>
	   (lp l (cons (pp (<scm>
			    (begin
			      (init-time2)
			      (let ((res
				     (assertz-source+
				      S (lambda () #f) (lambda (s p x) x)
				      stx 
				      (<scm> x) #f)))
				(pk-time2 'assertz)
				res))))
		       r))))
	 (()
	  (<cc> (pp #`(begin 
			#,@(map
			    (lambda (f)
			      #`(with #,line 
				      ((<lambda> () #,f)
				       (fluid-ref *current-stack*)
				       (lambda () #f)
				       (lambda x #t))))
			    (reverse r))))))
	 (x 
	  (lp (list x) r))))))


(define (code? code)
  (define (test x)
    (let ((x (module-ref (current-module) x)))
      (if (and (procedure? x)
	       (procedure-property x 'dynamic-directive))
	  #t
	  #f)))
 
  (match code
    ((#:term (#:atom x . _) . _)
     (test x))
    ((#:atom x . _)
     (test x))
    (_ #t)))
      
(define (parse-1 line s stx x)
  (define (ferr f n m)
    `(#:translated 0 
      ,(format 
        #f 
        "in ~a term directive ~a did not point to an available global directive"
        (get-refstr n m) f)))
  (pk-time 'parse)
  (init-time)
  (let ((code 
	 (if (match x (((_ _ "-" _)  code . _) (code? code)) (_ #t)) 
	     (with line
		   (expand-0 
		    s (lambda () #f) (lambda (s p x) x) 
		    line stx (ass stx x)))
	     #f)))
    (pk-time 'expand)
    (init-time)
    (if code
	`(#:translated 0 ,code)
	(with-fluids ((*current-stack* s))
	  (match x
	    (((fx _ ":-" _ ) (#:scm-term (#:atom scm _ _ _ _) l _ _) N M)
	     `(#:translated
	       0 ,(case scm
		    ((scm s)
		     (datum->syntax 
		      stx (eval-string 
			   (format #f "((@ (guile) quote) ~a)" l)
			   #:lang 'scheme)))
		    ((quote q)
		     #`'#,(datum->syntax 
			   stx (eval-string (string-append "'" l)
					    #:lang 'scheme))))))
	    

	    (((fx _ ":-" _ ) (#:term (#:atom nm _ _ _ _) l _ _ _) N M)
	     (call-with-values 
		 (lambda () (syntax-local-binding (datum->syntax stx nm)))
	       (lambda (type val)
		 (case type
		   ((global)
		    (let* ((sym (car val))
			   (mod (cdr val))
			   (f   (module-ref (resolve-module mod) sym))
			   (p   (object-property f 'prolog-directive)))
		      (if p
			  (if (procedure? p)
			      `(#:translated 0 ,(p stx l N M))
			      `(#:translated 0 ,(f stx l N M)))
			  (ferr nm N M))))
		   (else
		    (ferr nm N M))))))
	    

	    (x x))))))


;; A very simple error recovery and analyzer :-)
(define ferr* (f-seq (f+ (f-or! ws+ (f-not (f-char #\.))))
                     endpoint
                     ws))
(define ferr
  (<p-lambda> (c)
     (<let> ((n N) (m M))
       (.. (d) (ferr* c))
       (<code> (warn (format #f "Error somewhere beteen ~a -> ~a"
                             (get-refstr n m) (get-refstr N M))))
       (<p-cc> #f))))

(define (f-skip f)
  (<p-lambda> (c)
     (.. (d) (f c))
     (<p-cc> c)))

;; Operator parsing
(define (op-directive priority specifier Operator)
  (add-operator *prolog-ops* specifier Operator priority ws))

(define-syntax-rule (retit x ...)
  (catch #t
   (lambda () x ...)
   (lambda y #f)))

(define (prolog-tokens stx)
  (let ((f (f-seq 
	    ;(f-skip skip-module-tok)
	    (f* (f-clear-body
		 (f-or!		
		  (f-parse-1 stx expr) 
		  ferr))))))
        
    (<p-lambda> (c)
      (.. (d) (f '()))
      (<p-cc> (retit (reverse d))))))

(define (prolog-read-token stx)
  (let ((f (f-or 'statement (f-seq ws (f-read-1 stx expr)
				   ws (f-or (f-seq endpoint ws) f-eof))
                 ferr)))
    (<p-lambda> (c)
      (.. (d) (f '()))
      (<p-cc> (pp 'token (retit (reverse d)))))))

(define-syntax-rule (emod x)
  (begin
    (set! haserror #f)
    (let ((res x))
      (if haserror
	  #f
	  res))))

(define (prolog-parse stx . l) 
  (emod
   (with-fluids ((*translator* char-convert))
    (apply parse 
	   (append l (list (prolog-tokens stx)))))))

(define (prolog-parse-module stx . l) 
  (emod
   (with-fluids ((*translator* char-convert))
    (apply parse 
	   (append l (list module-tok))))))

(define (prolog-parse-read stream stx . l) 
  (emod
   (with-fluids ((*translator* char-convert))
    (apply parse-no-clear stream
	   (append l (list (prolog-read-token stx)))))))

