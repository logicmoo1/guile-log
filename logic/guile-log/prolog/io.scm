(define-module (logic guile-log prolog io)
  #:use-module (logic guile-log procedure-properties)
  #:use-module (logic guile-log)
  #:use-module (logic guile-log vlist)
  #:use-module (logic guile-log memoize)
  #:use-module (logic guile-log fstream)
  #:use-module (logic guile-log attributed)
  #:use-module (logic guile-log guile-prolog attribute)
  #:use-module ((logic guile-log umatch)
                #:select (gp-var? gp-lookup gp->scm))
  #:use-module (logic guile-log guile-prolog closure)
  #:use-module (rnrs io ports)
  #:use-module (ice-9 match)  
  #:use-module (ice-9 pretty-print)
  #:use-module (ice-9 format)
  #:use-module (logic guile-log prolog base)
  #:use-module ((logic guile-log prolog util)
                #:select ((member . pr-member) append))
  #:use-module (logic guile-log prolog directives)
  #:use-module ((logic guile-log prolog goal-transformers)
		#:renamer (lambda (x)
			    (if (eq? x '=>)
				'xxx
				x)))
  #:use-module (logic guile-log prolog error)
  #:use-module (logic guile-log prolog char)
  #:use-module (logic guile-log prolog names)
  #:use-module (logic guile-log prolog pre)
  #:use-module (logic guile-log prolog operators)
  #:use-module (logic guile-log prolog namespace)
  #:use-module (logic guile-log guile-prolog copy-term)
  #:use-module (rnrs io ports) 
  #:use-module (logic guile-log fstream)
  #:replace (write open close read)
  #:export (nl pp current_input current_output standard_input standard_output
               set_input set_output put_char put_code stream_property
               get_byte put_byte peek_byte
               get_char get_code peek_char peek_code flush_output
               read_term write_term write_canonical writeq
))

(define do-print #f)
(define pp
  (case-lambda
    ((s x)
     (when do-print
       (pretty-print `(,s ,(syntax->datum x))))
     x)
    ((x)
     (when do-print
       (pretty-print (syntax->datum x)))
     x)))

(define ppp
  (case-lambda
    ((s x)
     (when #t
       (pretty-print `(,s ,(syntax->datum x))))
     x)
    ((x)
     (when #t
       (pretty-print (syntax->datum x)))
     x)))

(define (stream-alias-lookup ss)
  (if (procedure? ss)
      (let ((r (procedure-property ss 'prolog-alias)))
        (if r r ss))
      ss))

(define *standard-input* 
  (make-fluid (current-input-port)))
(define *standard-output*
  (make-fluid (current-output-port)))
(define *current-input*
  (make-fluid (current-input-port)))
(define *current-output*
  (make-fluid (current-output-port)))

(define atom? (@@ (logic guile-log prolog goal-transformers) atom?))

(define (fport? x)
  (and (fluid? x)
       (let ((x (fluid-ref x)))
	 (or (fstream-rw? x)
	     (fstream-r? x)
	     (fstream-w? x)))))

(define (binary-port? x) #f)
(define source/sink?          (lambda (x)
                                (or (string? x) (procedure? x))))
(define prolog-stream?        (lambda (x) (or (port? x) (fport? x))))
(define prolog-alias?         (lambda (x)
                                (and (procedure? x)
                                     (procedure-property x 'prolog-alias))))

(define prolog-stream-alias?  
  (lambda (x)
    (or (prolog-alias? x) (prolog-stream? x))))

(define prolog-output-stream? 
  (lambda (x) 
    (or 
     (and (port? x) (output-port? x))
     (and (fluid? x)
	  (let ((x (fluid-ref x)))
	    (fstream-rw? x)
	    (fstream-w? x))))))

(define prolog-input-stream?
  (lambda (x) 
    (or 
     (and (port? x) (input-port? x))
     (and (fluid? x)
	  (let ((x (fluid-ref x)))
	    (fstream-rw? x)
	    (fstream-r? x))))))

(define (assq-false x)
  (or
   (and (pair? x) (not (cdr x)))
   #t))

(define (prolog-stream-closed? x)
  (or (and (port? x) (port-closed? x))
      (and
       (fluid? x)
       (let ((x (fluid-ref x)))
	 (and
	  (and (fstream-rw? x) (assq-false (assq #:open (rw-fstream-meta x))))
	  (and (fstream-r? x)  (assq-false (assq #:open (r-fstream-meta x))))
	  (and (fstream-w? x)  (assq-false (assq #:open
						 (w-fstream-meta x)))))))))
	  

(define-syntax-rule (mk-test current_input *current-input*)
  (<define> (current_input x)
    (<let*> ((xx (<lookup> x))
             (x  (stream-alias-lookup xx)))
      (cond
       ((<var?> x)
        (<=> x ,(fluid-ref *current-input*)))
       ((prolog-stream? x)
        (<=> x ,(fluid-ref *current-input*)))
       (else
        (domain_error stream xx))))))

(define (id x) x)

(mk-test current_input  *current-input*   )
(mk-test current_output *current-output*  )
(mk-test standard_input *standard-input*  )
(mk-test standard_ouput *standard-output* )

(define-syntax-rule (mk-inp set_input *current-input* input 
                            prolog-output-stream?)
  (<define> (set_input x)
    (<let*> ((xx (<lookup> x))
             (x  (stream-alias-lookup xx)))
      (cond
       ((prolog-stream-alias? x)
        (cond
         ((prolog-stream-closed? x)
          (domain_error stream_or_alias xx))
         ((prolog-output-stream? x)
          (permission_error input stream xx))
         (else
          (<code> (fluid-set! *current-input* x)))))
       
   
       ((<var?> x) 
        (instantiation_error))
   
       (else
        (if (procedure? xx)
            (existence_error stream xx)
            (domain_error stream_or_alias xx)))))))

(mk-inp set_input  *current-input*  input  prolog-output-stream?)
(mk-inp set_output *current-output* output prolog-input-stream?)

(define (partial-list? s x)
  (let lp ((x x)) 
    (if (pair? x)
        (if (gp-var? x s)
            #t
            (lp (cdr x)))
        (if (gp-var? x s)
            #t
            #f))))

(define option-list (list ))

(define (eq l) (lambda (x)
                 (if (string? l)
                     (cond
                      ((string? x)
                       (equal? x l))
                      ((procedure? x)
                       (equal? (symbol->string
                                (procedure-name x)) l))
                      (else #f))                         
                     (eq? x l))))

(define (open-option? s x)
  (match x
    (#(((? (eq type)) (or (? (eq text)) (? (eq binary)))))
     #t)
    (#(((? (eq reposition)) (or (? (eq true)) (? (eq false)))))
     #t)
    (#(((? (eq functional)) (or (? eq true) (? eq false))))
     #t)
    (#(((? (eq alias)) a))
     (let ((a (gp-lookup a s)))
       (and (procedure? a) (or (not (prolog-alias? a))
                               (let ((st (procedure-property a 'prolog-alias)))
                                 (or (prolog-stream-closed? st)))))))
    (#(((? (eq eof_action)) (or (? (eq error)) (? (eq eof_code))
                                (? (eq "reset")))))
     #t)
    (_
     #f)))

(define default-open-option `((#:type       . ,text) 
                              (#:reposition . ,false)
                              (#:eof-action . ,eof_code)
                              (#:alias      . #f)
			      (#:functional . #f)))
                               


  
     
(define (open-file-wrap fn mode)
  (let ((fn (cond
	     ((procedure? fn)
	      (symbol->string (procedure-name fn)))
	     ((symbol? fn)
	      (symbol->string fn))
	     (else fn))))
	      
    ((@@ (guile) catch) #t
     (lambda () 
       (let ((s (open-file fn mode)))
	 (cons s #f)))
     (lambda x (cons #t #t)))))

(define (open-ffile-wrap fn mode)
  (let ((fn (cond
	     ((procedure? fn)
	      (symbol->string (procedure-name fn)))
	     ((symbol? fn)
	      (symbol->string fn))
	     (else fn))))
	      
  ((@@ (guile) catch) #t
    (lambda () 
      (let ((s (cond
		((equal? mode "w")
		 (make-fluid (make-empty-w-fstream `((#:file-name . ,fn)))))
		((equal? mode "a")
		 (make-fluid (w-fstream-from-file-append fn)))
		((equal? mode "r")
		 (make-fluid (r-fstream-from-file fn)))
		((equal? mode "rw")
		 (make-fluid (rw-fstream-from-file fn)))
		((equal? mode "rwa")
		 (make-fluid (rw-fstream-from-file-append fn)))
		(else
		 #F))))
        (if s
	    (cons s #f)
	    (cons #f #t))))
    (lambda x (cons #t #t)))))

(define *open-ports* (make-fluid '()))

(define open
  (<case-lambda>
   ((SS Mode Stream)
    (open SS Mode Stream '()))
   ((SS Mode Stream Options)
    (<let> ((SS      (<lookup> SS))
            (Options (<scm> Options))
            (Mode    (<lookup> Mode))
	    (Stream  (<lookup> Stream)))
     (cond
       ((or (<var?> SS) (<var?> Mode) (partial-list? S Options))
        (instantiation_error))
       
       ((not (or (atom? Mode) (<var?> Mode)))
        (type_error atom Mode))

       ((not (list? Options))
        (type_error list Options))

       ((not (<var?> Stream))
        (type_error variable Stream))
       
       ((not (source/sink? SS))
        (domain_error source_sink SS))

       ((not (member Mode mode-list))
        (domain_error io_mode Mode))
              
       (else
        (<let*> ((Option  (reverse Options))
                 (ErOpt   (or-map (lambda (x) 
                                    (if (open-option? S x)
                                        #f
                                        x))
                                  Options)))
          (if ErOpt 
              (domain_error stream_option ErOpt) 
              (<and> <cc>))
          (<var> (A)                    
            (<and>             
             (<if> (pr-member (vector (list reposition A)) Options)
                   (<if> (<==> true A)
                         (permission_error open source_sink
                                           (vector (list reposition A)))
                         (<and> <cc>))
                   (<and> <cc>))
             
             (<var> (Repo Alias EOF Type Functional)
               (<or>
                (pr-member (vector (list functional Functional)) Option)
                (<=> Functional ,(cdr (assq #:functional default-open-option))))
               (<or>
                (pr-member (vector (list type Type)) Option)
                (<=> Type ,(cdr (assq #:type default-open-option))))
               (<or>
                (pr-member (vector (list reposition Repo)) Option)
                (<=> Repo ,(cdr (assq #:reposition default-open-option))))
               (<or>
                (pr-member (vector (list alias Alias)) Option)
                (<=> Alias #f))
               (<or>
                (pr-member (vector (list eof_action EOF)) Option)
                (<=> EOF ,(cdr (assq #:eof-action default-open-option))))
               (<cut>
                (<let> ((t  (<lookup> Type))
                        (r  (<lookup> Repo))
                        (a  (<lookup> Alias))
                        (e  (<lookup> EOF))
			(fu (<lookup> Functional)))
                   (<let*> ((mode (if (eq? t text)
                                    (cond
                                     ((eq? Mode write)
                                      "w")
                                     ((eq? Mode read)
                                      "r")
                                     ((eq? Mode append)
                                      "a"))
                                    (cond
                                     ((eq? Mode write)
                                      "wb")
                                     ((eq? Mode read)
                                      "rb")
                                     ((eq? Mode append)
                                      "ab"))))
                          (s-e    (if (eq? fu true)
				      (open-ffile-wrap SS mode)
				      (open-file-wrap SS mode)))
                          (s      (car s-e))
                          (er     (cdr s-e)))  
                    (if er
                       (existence_error source_sink SS)
                       (<and>
                        (<code> (set-object-property! s 'prolog-meta
                                  `((#:repos . #f)
                                    (#:eof   . ,e)
                                    (#:mode   . ,Mode)))
                                (fluid-set! *open-ports*
                                            (cons s (fluid-ref *open-ports*)))
                                (if a
                                    (set-procedure-property! 
                                     a 'prolog-alias s)))
                        
                        (<=> Stream s)
                        )))))))))))))))

(define (assq-get x a)
  (let ((q (assq a x)))
    (if q (cdr q) q)))

(define (fstream-file s)
  (if (fluid? s)
      (let ((s (fluid-ref s)))
	(cond
	 ((fstream-rw? s)
	  (assq-get (rw-fstream-meta s) #:file-name))
	 ((fstream-r? s)
	  (assq-get (r-fstream-meta s) #:file-name))
	 ((fstream-w? s)
	  (assq-get (w-fstream-meta s) #:file-name))
	 (else
	  #f)))
      #f))

(define (peek-char-adv s)
  (if (port? s)
      (peek-char s)
      (let ((s (fluid-ref s)))
	(fpeek s))))

(define (port-position-adv s)
  (if (port? s)
      (port-position s)
      (let ((s (fluid-ref s)))
	(fposition s))))
	 

(<define> (stream_property s prop)
  (<let*> ((ss    (<lookup> s))
           (s     (stream-alias-lookup ss))
           (props (object-property s 'prolog-meta)))
    (cond 
     ((<var?> s)
      (<recur> lp ((l (fluid-ref *open-ports*)))
        (if (pair? l)
            (<or> (stream_property (car l) prop)
                  (lp (cdr l))))))

     ((not (prolog-stream-alias? s))
      (if (procedure? ss)
          (existence_error stream ss)
          (domain_error stream ss)))

     ((prolog-stream-closed? s)
      (existence_error stream ss))

     ((<var?> prop)
      (<var> (P)
        (<or>
         (<=> prop ,(vector (list file_name P)))
         (<=> prop ,(vector (list mode      P)))
         (<=> prop ,(vector (list type      P)))
         (<=> prop ,input)
         (<=> prop ,output)
         (<=> prop ,(vector (list alias         P)))
         (<=> prop ,(vector (list position      P)))
         (<=> prop ,(vector (list end_of_stream P)))
         (<=> prop ,(vector (list eof_action    P))))
        (stream_property s prop)))
         

     (else
      (<match> (#:mode - #:name stream_property) (prop)
        (#((,file_name f))
	 (if (port? s)
	     (<cut> (<=> f ,(port-filename s)))
	     (<cut> (<=> f ,(fstream-file s)))))

        (#((,mode      m))
         (<cut>
          (<=> m ,(if props
                      (cdr (assq #:mode props))
                      (let ((q (port-mode s)))
                        (cond 
                         ((equal? q "w") output)
                         ((equal? q "r") input)
                         ((equal? q "a") append)))))))
       
        (#((,type     t))
         (<cut>
          (<=> t ,(if (and (port? s) (binary-port? s))
                      binary
                      text))))
        (,input
         (<cut> 
          (when (prolog-input-stream? s))))

        (,output
         (<cut>
          (when (prolog-output-stream? s))))
        
        (#((,alias   a))
         (<cut> <fail>))

        (#((,position p))
         (<cut>
          (when (or (eq? (cdr (assq #:repos props)) true)
                    (if (port? s) 
			(port-has-port-position? s)
			#t))
		
            (<=> p ,(port-position-adv s)))))

        (#((,reposition p))
         (<cut>
          (<=> p ,(if (or (eq? (cdr (assq #:repos props)) true)
                          (port-has-port-position? s)
			  (and (fluid? s) 
			       (let ((s (fluid-ref s)))
				 (fstream-rw? s))))
				     
                      true
                      false))))
        
        
        (#((,end_of_stream e))
         (<cut>
          (if (prolog-input-stream? s)
              (if (prolog-output-stream? s)
                  <fail>
                  (if (eof-object? (peek-char-adv s))
                      (<=> e at)
                      (<=> e no)))
              (<=> e at))))
                    
        (#((,eof_action a))
         (<cut>
          (<=> a ,(if props
                      (cdr (assq #:eof props))
                      (cdr (assq #:eof-action default-open-option))))))

        (_ 
         (domain_error stream_property prop)))))))

(define (stream-option? opt)
  (match opt
    (#(((? (eq force)) (or (? (eq true)) (? (eq false)))))
     #t)
    (_
     #f)))

(define (close-port-adv s)
  (if (port? s)
      (close-port s)
      (let ((ss (fluid-ref s)))
	(fluid-set! s (write-fstream ss)))))

(define close
  (<case-lambda>
   ((s)
    (close s '()))
   ((s options)    
    (<let*> ((ss      (<lookup> s))
             (s       (stream-alias-lookup ss))
             (options (<scm> options))
             (so      #f))
      (cond 
       ((<var?> s)
        (instantiation_error))       
       ((not (list? options))
        (type_error list options))
       ((or-map (lambda (x) (<var?> x)) options)
        (instantiation_error))
       ((not (prolog-stream-alias? s))
        (if (procedure? ss)
            (existence_error stream ss)
            (domain_error stream_or_alias ss)))
       ((let ((soo (or-map (lambda (x) (if (not (stream-option? x)) x #f))
                           options)))
          (set! so soo)          
          soo)
        (domain_error stream_option so))

       ((prolog-stream-closed? s)
        (existence_error stream ss))

       (else
        (<code> 
         (if (eq? s (fluid-ref *current-input*))
             (fluid-set! *current-input* (fluid-ref *standard-input*)))
         (if (eq? s (fluid-ref *current-output*))
             (fluid-set! *current-output* (fluid-ref *standard-output*)))
         (if (not (or (eq? s (fluid-ref *standard-input*))
                      (eq? s (fluid-ref *standard-output*))))             
             (begin
               (fluid-set! *open-ports* (let lp ((l (fluid-ref *open-ports*))
                                                 (r '()))
                                          (if (pair? l)
                                              (if (eq? s (car l))
                                                  ((@@ (guile) append)
                                                   (reverse r) 
                                                   (cdr l))
                                                  (lp (cdr l) 
                                                      (cons (car l) r))))))
               (close-port-adv s))))))))))

  

(define nl
  (<case-lambda>
   ((s)
    (<let*> ((ss (<lookup> s))
             (s  (stream-alias-lookup ss)))
     (cond
      ((<var?> s)
       (instantiation_error))       
      ((not (prolog-stream-alias? s))
       (domain_error stream_or_alias ss))
      ((prolog-stream-closed? s)
       (if (procedure? ss)
           (existence_error stream ss)
           (domain_error stream_or_alias ss)))        
      ((not (prolog-output-stream? s))
       (permission_error output stream ss))       
      (else
       (<format> s "~%") <cc>))))

   (()
    (nl (fluid-ref *current-output*)))))


(define (mklist l s)
  (umatch (#:mode - #:status s #:name mklist) (l)
    ((x . l)
     (cons x (mklist l s)))
    (() '())))

(define (binop* f op l s)
  (let* ((type (procedure-property f 'prolog-operator-type))
	 (type (if type type 'xfx))
	 (fxy  (cond
		((eq? type 'xfy) binopxfy)
		((eq? type 'yfx) binopyfx)
		((eq? type 'xfx) binopxfy)
                (else binopxfy))))
    (let lp ((l l))
      (if (pair? l)
	  (if (eq? (length l) 2)
	      (fxy op (vector (list op (car l) (cadr l))) s)
	      l)
	  '()))))

(define (binopxfy op x s)
  (umatch (#:mode -r #:status s #:name 'binop) (x)
    (#((,op a b))
     (cons a (binopxfy op b s)))
    (x (list x))))

(define (binopyfx op x s)
  (binopyfx0 op x s))

(define (binopyfx0 op x s)
  (umatch (#:mode -r #:status s #:name 'binop) (x)
    (#((,op a b))
     (let ((x (binopyfx0 op a s)))
       ((@ (guile) append) x (list b))))
    (x (list x))))

(define prop procedure-property)
(define (procedure-property f k)
  (if (procedure? f)
      (prop f k)
      #f))

(define (pkf . l) (car (reverse l)))

(define pipe (module-ref (current-module) (string->symbol "|")))

(define* (scm->pl s x #:optional (ns? #f) (quoted? #f) (ignore? #f) (numbervars? #f))
  (define (first-redo h) #f)

;    (set! first-map h)
;    (vlist-truncate! h))

  (define first-map   vlist-null)
  (define action-map  (make-hash-table))
  (define action-i    0)
  (<define> (action x)
     (<let*> ((x (<lookup> x))
	      (r (hashq-ref action-map x #f)))
       (if (not r)
	   (<code> (hashq-set! action-map x action-i)
		   (set! action-i (+ 1 action-i)))
	   <cc>)))

  (define *variables* (make-hash-table))
  (define *closures*  (make-hash-table))
  (define i 0)
  (define (next)
    (let ((s (string-append "X" (number->string i))))
      (set! i (+ i 1))
      s))

  (define (next-q)
    (let ((s (string-append "X" (number->string i))))
      (set! i (+ i 1))
      s))

  (define p-op #f)
  
  (define (list-it x)
    (let ((h first-map))
       (umatch (#:mode -r #:status s #:name 'list-it1) (x)
        ((a)
	 (begin
	   (first-redo h)
	   (format #f "~a" (lp a))))
	((a . b)
	 (begin
	   (first-redo h)
	   (let ((r (lp? b (lambda ()
			     (format #f "[~a]" (list-it b))))))
	     (first-redo h)
	     (if r
		 (format #f "~a|~a" (lp a) r)	     
		 (umatch (#:mode -r #:status s #:name 'list-it2) (b)
	            ((u . v)
		     (format #f "~a, ~a" (lp a) (begin (first-redo h) 
						       (list-it b))))
		    (u
		     (format #f "~a|~a"  (lp a) (begin (first-redo h)
						       (lp b)))))))))
	(a (lp a)))))

  (define (str x) (if (symbol? x)
		      (symbol->string x)
		      x))

  (define (gen@ ll a)
    (cond
     ((gp-var? a s)
      (let ((r (hashq-ref *variables* a #f)))
	(if r
	    r
	    (let ((n (next)))                       
	      (hashq-set! *variables* a n)
	      n))))

     ((not ns?)
      (if quoted?
	  (format #f "'~a'" (str (procedure-name a)))
	  (format #f "~a" (str (procedure-name a)))))
     
     ((and ns? (not quoted?))
      (let ((m  (current-module))
	    (ns (procedure-name a))
	    (n  (str (procedure-name a))))
	(if (not quoted?)
	    (format #f "~a" n)
	    (if (and (module-defined? m ns) (eq? (module-ref m ns) a))
		(format #f "~a" n)
		(match ll
		       (('language 'prolog 'modules x)
			(format #f "~a@@~a" 
				n x))
		       (("language" "prolog" "modules" x)
			(format #f "~a@@~a" 
				n x))
		       ((_ . _)
			(format #f "~a@@('~a'~{,'~a'~})" 
				n (car ll) (cdr ll)))
		       (_
			(format #f "~a" n)))))))

     (else
      (match ll
        (('language 'prolog 'modules x)
	 (format #f "'~a'@@~a" 
		 (str (procedure-name a)) x))
        (("language" "prolog" "modules" x)
	 (format #f "'~a'@@~a" 
		 (str (procedure-name a)) x))
        ((_ . _)
	 (format #f "'~a'@@(~a~{,~a~})" 
		 (str (procedure-name a)) (car ll) (cdr ll)))
        (_
	 (format #f "'~a'"    (str (procedure-name a))))))))

  

  (define (lp0 x trail)
    (let ((x (gp-lookup x s)))
      (let ((v (hashq-ref action-map x #f)))
	(if v
	    (let ((w (vhashq-ref first-map x #f)))
	      (if w
		  (format #f "ref[~a]" v)
		  (begin
		    (set! first-map (vhash-consq x #t first-map))
		    (string-append
		     (format #f "{~a}" v)
		     (trail x #t)))))
	    (trail x #f)))))
  
  (define (lp? x trail)
    (let ((x (gp-lookup x s)))
      (let ((v (hashq-ref action-map x #f)))
	(if v
	    (let ((w (vhashq-ref first-map x #f)))
	      (if w
		  (format #f "ref[~a]" v)
		  (begin
		    (set! first-map (vhash-consq x #t first-map))
		    (format #f "{~a}~a" v (trail)))))
	    #f))))
  
  (define (lp x) (lp0 x trail))
  
  (define touched (make-fluid #f))
  
  (define (trail x qq)
    (let lp2 ((x x))
    (umatch (#:mode -r #:status s #:name scm->pl) (x)
      (#(#:brace code)
       (begin
         (set! p-op #f)        
         (if (eq? code #:null)
             (format #f "{}")                 
             (format #f "{~a}" (lp code)))))

      (#((f a . l))      
       (let ((f (gp-lookup f s))
	     (x (gp-lookup x s))
	     (h first-map))
         (cond
	  ((not f)
	   (format #f "#f"))
	  ((or (pair? f) (vector? f))
	   (format #f "~a(~a~{, ~a~})" (lp f) (lp a) (map lp (gp->scm l s))))
	  ((eq? f pipe)
	   (trail (cons a l) qq))
          ((string? f)
           (set! p-op ",")
           (format #f "'~a'(~a~{, ~a~})" f (lp a) (map lp (gp->scm l s))))
	  
          ((and (struct? f) (prolog-closure? f))
           (set! p-op ",")
           (let ((args (map (lambda (x) (first-redo h) (lp x))
			    (prolog-closure-state f)))
                 (pre  (vector (cons* (prolog-closure-parent f) '()))))
             (if quoted?
		 (let ((n (hashq-ref *closures* f #f)))
		   (if n
		       (format #f "~a(~a~{,~a~})" n
			       (lp a) (map lp (gp->scm l s)))
		       (let ((n (next)))
			 (hashq-set! *closures* f n)
			 (if (not (pair? args))
			     (format #f "~a is ~a(),~a(~a~{,~a~})"
				     n (lp2 pre) n
				     (lp a) (map lp (gp->scm l s)))
			     (format #f "~a is ~a(~a~{,~a~}),~a(~a~{,~a~})" 
				     n (lp2 pre)
				     (car args) (cdr args) 
 				     n (lp a) (map lp (gp->scm l s)))))))
                 (format #f "~a(~a~{,~a~})"
                         (lp2 pre) (lp a) (map lp (gp->scm l s))))))
	            
          (else	   
           (let* ((h     first-map)
                  (op    (procedure-property f 'prolog-operator))
                  (pred  (if p-op
                             (if (equal? p-op ",")
                                 (if (equal? op "-")
                                     #t
                                     #f))
                             #t)))
             
               (define (fmop op l)
                 (cond
                  ((>= (length l) 2)
                   (format #f (if pred "~a~{~a~a~}" "(~a~{ ~a ~a~})") (car l)
                           (let lp ((l (cdr l)))
                             (if (pair? l) 
                                 (cons* op (car l) (lp (cdr l)))
                                 '()))))
                  (else
		   (let ((tp (procedure-property f 'prolog-operator-type)))
		     (if (or (eq? tp 'xf) (eq? tp 'yf))
			 (format #f "~a~a" (car l) op)
			 (format #f "~a~a" op (car l)))))))

               (if op (set! p-op op) (set! p-op ","))
               
               (if (and op (not (and ns? quoted?)))        
                   (fmop op (map (lambda (x)
                                   (let ((s-op p-op))
                                     (first-redo h)
                                     (let ((r (lp x)))
                                       (set! p-op s-op)
                                       r)))                                     
                                 (mklist (binop* f op (cons a l) s) s)))
                   (let ((ll (map (lambda (x) (format #f "'~a'" x)) 
                                  (if (procedure? f)
				      (get-attached-module f ns?)
				      '()))))
		     (format #f "~a(~a~{,~a~})"   
			     (gen@ ll f)
			     (lp a) (map lp (mklist l s))))))))))
      
      (#((f))
       (let ((f (gp-lookup f s)))
       (cond
        ((string? f)
         (format #f "'~a'" f))

        ((or (pair? f) (vector? f))
         (format #f "~a" (lp f)))

        ((and (struct? f) (prolog-closure? f))
         (let ((args (map lp (prolog-closure-state f)))
               (pre  (vector (cons* (prolog-closure-parent f) '()))))
	   (let ((n (hashq-ref *closures* f #f)))
	     (if quoted?
		 (if n
		     (format #f "~a" n)
		     (let ((n (next)))
		       (hashq-set! *closures* f n)
		       (if (not (pair? args))
			   (format #f "~a is ~a(), ~a" n (lp2 pre) n)
			   (format #f "~a is ~a(~a~{,~a~}), ~a" 
				   n (lp2 pre)
				   (car args) (cdr args) n))))
		 (format #f "~a" (lp2 pre))))))

           
        (else
         (let ((ll (map (lambda (x) (format #f "'~a'" x)) 
                        (if (procedure? f)
			    (get-attached-module f ns?)
			    '()))))
           (if quoted?
	       (gen@ ll f)
	       (gen@ #f f)))))))
      
      ((a . l)
       (format #f "[~a]" (list-it x)))

      (a
       (let ((a (gp-lookup a s)))
         (cond
          ((string? a)           
           (format #f (if quoted? "'~a'" "~a")
                   (list->string
                    (let lp ((l (string->list a)))
                      (if (pair? l)
                          (let ((x (car l)))
                            (if (eq? x #\')
                                (cons* #\' #\' (lp (cdr l)))
                                (cons x (cdr l))))
                          '())))))
	  
	      
          ((or (gp-var? a s) (gp-attvar-raw? a s))
           (let ((r (hashq-ref *variables* a #f)))
             (if r
                 r
                 (let ((n (next)))                       
                   (hashq-set! *variables* a n)
                   n))))

          ((procedure? a)
           (if quoted
               (let ((ll (map (lambda (x)
                               (if (symbol? x)
                                   (symbol->string x)
                                   x))
                             (if (procedure? a)
       	 	                 (get-attached-module a ns?)
			         '()))))
                 (gen@ ll a))
                (gen@ #f a)))

                 

          ((struct? a) 
           (cond
             ((object-property (struct-vtable a) 'prolog-printer)
               => (lambda (printer)
                  (printer lp a quoted?)))

            ((prolog-closure? a)
	     (lp2 (vector (list a))))
	    
            ((namespace? a)
             (let ((v  (lp (namespace-val a)))
                   (at (if (namespace-local? a)
                           '@@
                           '@))
                   (l  (let ((l (namespace-ns a)))
                         (match l
                           (('language 'prolog 'modules x)
                            x)
                           (("language" "prolog" "modules" x)
                            x)
                           (_ l)))))
               (format #f "~a~a~a" v at l)))
            (else
             (format #f "~a"   a))))

          (else
           (format #f "~a"   a))))))))
  (<wrap-s> (with-atomic-frec (<lambda> (x) (rec-action00 action x))) s x)
  (lp x))

(define (fformat port str . l)
  (if (port? port)
      (apply format port str l)
      (let ((s   (fluid-ref port))
	    (str (apply format #f str l)))
	(let lp ((l (string->list str)) (s s))
	  (if (pair? l)
	      (lp (cdr l) (fwrite s (car l)))
	      (fluid-set! port s))))))

(define write_term
  (<case-lambda>
   ((s t opts)
    (<let*> ((ss (<lookup> s))
             (s  (stream-alias-lookup ss)))
     (cond
       ((<var?> s)
        (instantiation_error))
       ((not (prolog-stream-alias? s))
        (if (procedure? ss)
            (existence_error stream ss)
            (domain_error stream_or_alias ss)))        
       ((prolog-stream-closed? s)
        (existence_error stream ss))
       ((not (prolog-output-stream? s))
        (permission_error output stream ss))
       ((binary-port? s)
        (permission_error input binary_stream ss))
       (else
        (<let> ((ns #t)
                (q  #f)
                (i  #t)
                (n  #f))
               
         (<recur> lp ((opts opts))
          (<match> (#:mode - #:name write_term_opts) (opts)
           ((opt . opts)
            (<let> ((opt (<lookup> opt)))
              (cond
               ((<var?> opt)
                (instantiation_error))
               (else
                (<match> (#:mode - #:name write_term_opt) (opt)
                  (#((,quoted ,true))
                   (<code> (set! q #t) (set! ns #t)))
                  (#((,quoted ,false))
                   (<code> (set! q #f) (set! ns #f)))
                  (#((,ignore_ops ,true))
                   (<code> (set! i #t)))
                  (#((,ignore_ops ,false))
                   (<code> (set! i #f)))
                  (#((,numbervars ,false))
                   (<code> (set! n #f)))
                  (#((,numbervars ,true))
                   (<code> (set! n #t)))
                  (_
                   (domain_error write_option opt)))))
              (lp opts)))

           (()
            (<and>
	     <cut>
	     (<let> ((p  P)
		     (s0 S)
		     (fr (<newframe>)))
	       (<values> (t ts) (copy-term-3 t))
	       (<code> (fformat s "~a" (scm->pl S t ns q i n)))
	       (<recur> lp ((ts ts))		 
                  (if (and q (pair? ts))
		      (<and>
		       <cut>
		       (<code> 
			(fformat s ",~%~a" (scm->pl S (car ts) ns q i n)))
		       (lp (cdr ts)))
		      <cc>))
	       (<code> (<unwind-tail> fr))
	       (<with-s> s0
	       (<with-fail> p <cc>)))))

           (_
            (instantiation_error)))))))))
   ((t opts)
    (write_term (fluid-ref *current-output*) t opts))))

(set! write 
  (<case-lambda>
   ((term)
    (write_term term '()))
   ((stream term)
    (write_term stream term '()))))

(define qt (list (vector (list quoted true)) (vector (list numbervars true))))
(define writeq
  (<case-lambda>
   ((s t)
    (write_term s t qt))
   ((t)
    (write_term t qt))))

(define cn (list (vector (list quoted true)) (vector (list ignore_ops true))))
(define write_canonical 
  (<case-lambda>
   ((s t)
    (write_term s t cn))
   ((t)
    (write_term t cn))))

(set-procedure-property! write 'name 'write)

(define ppp
  (case-lambda
   ((s x)
    (when #t
      (pretty-print `(,s ,(syntax->datum x))))
    x)
   ((x)
    (when #t
      (pretty-print (syntax->datum x)))
    x)))

(define pp
  (<case-lambda> 
   ((x)
    (<pp> x))
   ((n x)
    (<pp> `(,n ,x)))))

(<define> (read* s term v vn si)  
  (<let*> ((s   (<scm> s))
	   (s0  S)
	   (fr  (<newframe>))
           (e   (call-with-values 
                    (lambda () (read-prolog-term S s (current-module)))
                  (lambda x x))))
     (<code> (<unwind-tail> fr))
     (<with-s> s0
       <cut>
       (<or>
	(<and> (<=> ,(list term v vn si) e) <cut>)
	(<=> ,(list term v vn si) ,(list end_of_file '() '() '())))
       (<code> (fluid-set! *closure-creations* (make-hash-table))))))


(define read_term
  (<case-lambda>
   ((s t opts)
    (<let*> ((ss (<lookup> s))
             (s  (stream-alias-lookup ss)))
      (cond       
       ((<var?> s)
        (instantiation_error))
       ((not (prolog-stream-alias? s))
        (if (procedure? ss)
            (existence_error stream ss)
            (domain_error stream_or_alias ss)))        
       ((prolog-stream-closed? s)
        (existence_error stream ss))
       ((not (prolog-input-stream? s))
        (permission_error input stream ss))
       ((binary-port? s)
        (permission_error input binary_stream ss))

       (else
        (<var> (vars varnames singletons) 
         (<recur> lp ((opts opts))
          (<match> (#:mode - #:name read_term_opts) (opts)
           ((opt . opts)
            (<let> ((opt (<lookup> opt)))	       
              (cond
               ((<var?> opt)
                (instantiation_error))
               (else
                (<match> (#:mode - #:name read_term_opt) (opt)
                 (#((,variables X))
                  (<=> X vars))
                 (#((,variable_names X))
                  (<=> X varnames))
                 (#((,singletons X))
                  (<=> X singletons))
                 (else
                  (domain_error read_option opt)))
                (lp opts)))))
           (()
            (<and>
             (<cut> (read* s t vars varnames singletons))))
           (_
            (instantiation_error)))))))))

   ((t opts)
    (read_term (fluid-ref *current-input*) t opts))))

(set! read 
      (<case-lambda> 
       ((s t) (read_term s t '()))
       ((  t) (read_term   t '()))))

(set-procedure-property! read 'name 'read)

(define-syntax-rule (prolog-char? x)
  (or (procedure? x) (char? x) (and (string? x) 
                                    (= (string-length x) 1))))

(define (->ch x)
  (define (ch1? x) (= (string-length x) 1))
        
  (cond
   ((and (string? x) (ch1? x))
    (string-ref x 0))
   ((procedure? x)
    (->ch (procedure-name x)))
   ((symbol? x)
    (->ch (symbol->string x)))
   ((char? x) 
    x)
   (else
    #f)))

(define (write-char-adv ch s)
  (if (port? s)
      (write-char ch s)
      (let ((ss (fluid-ref s)))
	(fluid-set! s (fwrite s ch)))))

(define put_char 
  (<case-lambda>
   ((s ch)
    (<let*> ((ss (<lookup> s))
             (s  (stream-alias-lookup ss))
             (ch (<lookup> ch)))
      (cond
       ((<var?> s)
        (instantiation_error))
       ((<var?> ch)
        (instantiation_error))
       ((not (prolog-char? ch))
        (type_error character ch))
       ((not (prolog-stream-alias? s))
        (if (procedure? ss)
            (existence_error stream ss)
            (domain_error stream_or_alias ss)))        
       ((prolog-stream-closed? s)
        (existence_error stream ss))
       ((not (prolog-output-stream? s))
        (permission_error output stream ss))
       ((binary-port? s)
        (permission_error input binary_stream ss))
       (else
        (<code> (write-char-adv (->ch ch) s))))))
   
   ((ch)
    (put_char (fluid-ref *current-output*) ch))))
        

(define put_code
  (<case-lambda>
   ((s ch)
    (<let*> ((ss  (<lookup> s))
             (s  (stream-alias-lookup ss))
             (ch (<lookup> ch)))
      (cond
       ((<var?> s)
        (instantiation_error))

       ((<var?> ch)
        (instantiation_error))
       
       ((not (and (number? ch) (integer? ch)))
        (type_error integer ch))
       
       ((not (prolog-stream-alias? s))
        (existence_error stream ss))

       ((prolog-stream-closed? s)
        (if (procedure? ss)
            (existence_error stream ss)
            (domain_error stream_or_alias ss)))       

       ((not (prolog-output-stream? s))
        (permission_error output stream ss))

       ((binary-port? s)
        (permission_error input binary_stream ss))

       (else
        (<var> (chch)
          (char_code chch ch)
          (put_char s (string-ref (<lookup> chch) 0)))))))
   ((ch)
    (put_code (fluid-ref *current-output*) ch))))

(define put_byte
  (<case-lambda>
   ((s ch)
    (<let*> ((ss (<lookup> s))
             (s  (stream-alias-lookup ss))
             (ch (<lookup> ch)))
      (cond
       ((<var?> s)
        (instantiation_error))

       ((<var?> ch)
        (instantiation_error))
       
       ((not (and (number? ch) (integer? ch) (< ch 256) (>= ch 0)))
        (type_error byte ch))
       
       ((not (prolog-stream-alias? s))
        (if (procedure? ss)
            (existence_error stream ss)
            (domain_error stream_or_alias ss)))        

       ((prolog-stream-closed? s)
        (existence_error stream ss))

       ((not (prolog-output-stream? s))
        (permission_error output stream ss))

       #;((not (binary-port? s))
        (permission_error output text_stream ss))

       (else
        (<code> (put-u8 s ch))))))

   ((ch)
    (put_byte (fluid-ref *current-output*) ch))))
     
(define (<-ch ch)
  (if (eof-object? ch)
      end_of_stream
      (list->string (list ch))))

(define (read-char-adv s)
  (if (port? s)
      (read-char s)
      (let ((ss (fluid-ref s)))
	(call-with-values (lambda () (fread ss))
	  (lambda (st val)
	    (fluid-set! s st)
	    val)))))

(define (peek-char-adv s)
  (if (port? s)
      (peek-char s)
      (let ((ss (fluid-ref s)))
	(fpeek ss))))

(define get_char
  (<case-lambda>
   ((ch)
    (get_char (fluid-ref *current-input*) ch))

   ((s ch)
    (<let*> ((ss  (<lookup> s))
             (s  (stream-alias-lookup ss))
             (ch (<lookup> ch)))
      (cond
       ((<var?> s)
        (instantiation_error))
       ((and (not (<var?> ch)) (not (prolog-char? ch)))
        (type_error character ch))
       ((not (prolog-stream-alias? s))
        (if (procedure? ss)
            (existence_error stream ss)
            (domain_error stream_or_alias ss)))        
       ((prolog-stream-closed? s)
        (existence_error stream ss))
       ((not (prolog-input-stream? s))
        (permission_error input stream ss))
       ((binary-port? s)
        (permission_error input binary_stream ss))

       (else
        (<=> ch ,(<-ch (read-char-adv s)))))))))

(define get_code
  (<case-lambda>
   ((code)
    (get_code (fluid-ref *current-input*) code))

   ((s code)
    (<let*> ((ss  (<lookup> s))
             (s  (stream-alias-lookup ss))
             (code (<lookup> code)))
      (cond
       ((<var?> s)
        (instantiation_error))
       ((and (not (<var?> code)) (not (and (number? code) (integer? code))))
        (type_error integer code))
       ((not (prolog-stream-alias? s))
        (if (procedure? ss)
            (existence_error stream ss)
            (domain_error stream_or_alias ss)))        
       ((prolog-stream-closed? s)
        (existence_error stream ss))
       ((not (prolog-input-stream? s))
        (permission_error input stream ss))
       ((binary-port? s)
        (permission_error input binary_stream ss))
    
       (else
        (<var> (ch)
          (<=> ch ,(<-ch (read-char-adv s)))
          (char_code ch code))))))))

(define get_byte
  (<case-lambda>
   ((code)
    (get_byte (fluid-ref *current-input*) code))

   ((s code)
    (<let*> ((ss   (<lookup> s))
             (s    (stream-alias-lookup ss))
             (code (<lookup> code)))
      (cond
       ((<var?> s)
        (instantiation_error))
       ((and (not (<var?> code)) (not (and (number? code) (integer? code))))
        (type_error integer code))
       ((not (prolog-stream-alias? s))
        (if (procedure? ss)
            (existence_error stream ss)
            (domain_error stream_or_alias ss)))        
       ((prolog-stream-closed? s)
        (existence_error stream ss))
       ((not (prolog-input-stream? s))
        (permission_error input stream ss))
       #;((not (binary-port? s))
        (permission_error input text_stream ss))
    
       (else
        (<var> (ch)
          (<=> ch ,(get-u8 s)))))))))


(define peek_char
  (<case-lambda>
   ((ch)
    (peek_char (fluid-ref *current-input*) ch))

   ((s ch)
    (<let*> ((ss  (<lookup> s))
             (s  (stream-alias-lookup ss))
             (ch (<lookup> ch)))
      (cond
       ((<var?> s)
        (instantiation_error))
       ((and (not (<var?> ch)) (not (prolog-char? ch)))
        (type_error character ch))

       ((not (prolog-stream-alias? s))
        (if (procedure? ss)
            (existence_error stream ss)
            (domain_error stream_or_alias ss)))        

       ((prolog-stream-closed? s)
        (existence_error stream ss))

       ((not (prolog-input-stream? s))
        (permission_error input stream ss))
       #;((binary-port? s)
        (permission_error input binary_stream ss))


       (else
        (<=> ch ,(<-ch (peek-char s)))))))))

(define peek_code
  (<case-lambda>
   ((code)
    (peek_code (fluid-ref *current-input*) code))

   ((s code)
    (<let*> ((ss  (<lookup> s))
             (s  (stream-alias-lookup ss))
             (code (<lookup> code)))
      (cond
       ((<var?> s)
        (instantiation_error))
       ((and (not (<var?> code)) (not (and (number? code) (integer? code))))
        (type_error integer code))
       ((not (prolog-stream-alias? s))
        (if (procedure? ss)
            (existence_error stream ss)
            (domain_error stream_or_alias ss)))        
       ((prolog-stream-closed? s)
        (existence_error stream ss))
       ((not (prolog-input-stream? s))
        (permission_error input stream ss))
       ((binary-port? s)
        (permission_error input binary_stream ss))

       (else
        (<var> (ch)
          (<=> ch ,(<-ch (peek-char-adv s)))
          (char_code ch code))))))))

(define peek_byte
  (<case-lambda>
   ((code)
    (peek_byte (fluid-ref *current-input*) code))

   ((s code)
    (<let*> ((ss  (<lookup> s))
             (s  (stream-alias-lookup ss))
             (code (<lookup> code)))
      (cond
       ((<var?> s)
        (instantiation_error))

       ((and (not (<var?> code)) (not (and (number? code) (integer? code))))
        (type_error integer code))

       ((not (prolog-stream-alias? s))
        (if (procedure? ss)
            (existence_error stream ss)
            (domain_error stream_or_alias ss)))        

       ((prolog-stream-closed? s)
        (existence_error stream ss))

       ((not (prolog-input-stream? s))
        (permission_error input stream ss))
       #;((not (binary-port? s))
        (permission_error input text_stream ss))

       (else
        (<=> code ,(lookahead-u8 s))))))))


(define mode-list (list read write append))

(define flush_output
  (<case-lambda>
   ((s)
    (<let*> ((ss  (<lookup> s))
             (s  (stream-alias-lookup ss)))
     (cond
      ((<var?> s)
        (instantiation_error))
      
      ((not (prolog-stream-alias? s))
       (if (procedure? ss)
           (existence_error stream ss)
           (domain_error stream_or_alias ss)))        

      ((prolog-stream-closed? s)
       (existence_error stream ss))

      ((prolog-input-stream? s)
       (permission_error output stream ss))

      (else
       (if (port? s)
	   (<code> (force-output s)))))))
   
   (()
    (<code> (flush-all-ports)))))
      
(set! (@@ (logic guile-log slask) write) write)
(set! (@@ (logic guile-log slask) nl   ) nl)
