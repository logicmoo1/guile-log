(define-module (logic guile-log prolog goal)
  #:use-module (logic guile-log prolog pre)
  #:use-module (logic guile-log prolog goal-functors)
  #:use-module (logic guile-log prolog pre)
  #:use-module (logic guile-log prolog parser)
  #:use-module (logic guile-log prolog load)
  #:use-module (logic guile-log prolog error)
  #:use-module ((logic guile-log slask) #:select
                (brace-stx-scm arg compile-lambda))
  #:use-module (logic guile-log prolog operators)
  #:use-module ((logic guile-log) #:renamer (lambda (x)
					      (if (eq? x '<_>)
						  'GL:_
						  x)))
  #:use-module (system syntax)
  #:use-module (ice-9 match)
  #:use-module (ice-9 eval-string)
  #:use-module (ice-9 pretty-print)
  #:export (goal scm dcg))


(define do-print #f)
(define pp
  (case-lambda
   ((s x)
    (when do-print
      (pretty-print `(,s ,(syntax->datum x))))
    x)
   ((x)
    (when do-print
      (pretty-print (syntax->datum x)))
    x)))
(define ppp
  (case-lambda
   ((s x)
    (when #t
      (pretty-print `(,s ,(syntax->datum x))))
    x)
   ((x)
    (when #t
      (pretty-print (syntax->datum x)))
    x)))
        
(define (->string x)
  (if (symbol? x)
      (symbol->string x)
      x))

(define (mk-scheme stx s l scm?)
  (let* ((sym (eval-string (string-append "'" l) #:lang 'scheme))
         (w   (datum->syntax stx sym)))
    (case s
      ((do)
       (if scm? 
           (error "do[] is a goal and not an expression")
           #`(<code> #,w)))
      ((when)
       (if scm?
           w 
           #`(if #,w <cc>)))
      ((v var)
       (if scm? #`(<scm> #,w) #`(eval-goal #,w)))
      ((s scm)
       (if scm? w (error "scm[] not allowed as a goal"))))))

(<define> (caller x l)
   (<<match>> (#:mode - #:name caller) (x)
     (#(u)
      (goal-eval (vector (append u l))))
     (x
      (<let> ((x (<lookup> x)))
	(if (procedure? x)
	    (goal-eval (vector (cons x l)))
	    (type_error "callable" x))))))

(define bindings (@@ (logic guile-log prolog operators) bindings))

(define (goal stx z)
  (define (garg stx x) #``#,(arg stx x))
  (match (pp 'goal z)
    (()
     #'<cc>)

    ;;This does not work on the meta level
    (((kind _ "*" _)
      (#:term (#:atom f _ _ n m) l . _)
      code . _)

     (let* ((l (map (lambda (x) (get.. "," (cadr x))) (get.. "," l))))
       #`(<recur> #,(datum->syntax stx f)
                  #,(map (lambda (x)
                           (let ((var (car  x))
                                 (val (cadr x)))
                             (match var
                               ((#:variable v . _)
                                #`(#,(datum->syntax stx v)
                                   `#,(arg stx var))))))
                         l)
                  #,(with-fluids ((bindings (cons f (fluid-ref bindings))))
                      (goal stx code)))))
           
           
    (((kind _ op _) x y n m)
     (f->stxfkn #f #f op #f #f garg #:goal stx 2 n m (list x y)))
    
    (((kind _ op _) x n m)
     (f->stxfkn #f #f op #f #f garg #:goal stx 1 n m (list x)))
    
    ((#:group x)
     (goal stx x))

    ((#:list (or (#:variable x _ _ _)
                 (#:atom     x _ _ _ _)
                 (#:string   x _ _)) _ _)
     (datum->syntax stx `(load-prolog ,x)))

    ((#:scm-term (#:atom s . _) l _ _)
     (mk-scheme stx s l #f))
       
    ((#:atom 'true . _)  #'<cc>)
    ((#:atom (or 'false 'fail) . _)  #'<fail>)
    ((#:atom '! . _) #'<cut>)

    ((#:atom v _ _ n m)
     (goal stx `(#:term ,z () #f ,n ,m)))
    
    ((#:variable '_ n m) 
     (warn (format #f "compilation-error ~a '_' cannot be a goal"
		   (get-refstr n m))))

    ((#:term     (and atom (#:atom f _ _ n m)) (and ag ((_ _ "|" _) x y _ _))
		 #f . _)
     #`(caller `#,(arg stx atom) `#,(arg stx (list #:list ag n m))))

    ((#:term     (and atom (#:atom f _ _ n m)) (and ag ((_ _ "|" _) y _ _))
		 #f . _)
     #`(caller `#,(arg stx atom) `#,(arg stx (list #:list y n m))))

    ((#:term (#:atom 'call . _) ((_ _ "," _) (and f (#:atom . _)) l n m) . u)
     (goal stx `(#:term ,f ,l ,@u)))
    
    ((#:term (#:atom 'call . _) ((_ _ "," _) f l n m) . u)
     #`(caller `#,(arg stx f) `#,(arg stx (list #:list l n m))))

    ((#:term ((not #:atom) . l) . u)
     #`(goal-eval CUT #,(garg stx z)))

    ((#:term (and atom (#:atom f . _)) () #f n m)
     (if (member f (fluid-ref bindings))
         #`(#,(datum->syntax stx f))
         (f->stxfkn #f #f f #f atom garg #:goal stx #f n m '())))
    
    ((#:term (and atom (#:atom f amp _ _ _)) l #f n m)     
     (let ((l (get.. "," l)))
       (if (member f (fluid-ref bindings))
           #`(#,(datum->syntax stx f) #,@(map (lambda (x) #``#,(arg stx x))
                                            l))
           (f->stxfkn #f #f f #f atom garg #:goal stx #f n m l))))
        
    ((#:termvar v id l . _)
     #`(goal-eval `#,(arg stx z)))

    ((#:variable x id . _)
     #`(goal-eval #,(datum->syntax stx x)))

    ((x) (goal stx x))))

;;We do not, use eval-scm to eval objects in scm contexts
(define (scm stx x)
  (define (sarg stx x) #``#,(arg stx x))
  (define (sscm stx x) (scm stx x))
  (match (pp 'scm x)
    (((kind _ "-"  _) (#:number x . _) n m)
     (- x))

    (((kind _ op _) x y n m)
     (f->stxfkn #f #f op #f #f sscm #:scm stx 2 n m (list x y)))
    
    (((kind _ op _) x n m)
     (f->stxfkn #f #f op #f #f   sscm #:scm stx 1 n m (list x)))

    ((#:group x)
     (scm stx x))

    ((#:lam-term (or (#:atom s . _) (and #f s)) l closed? _ _)
     (if s
	 #``,#,(compile-lambda stx l s closed?)
	 (brace-stx-scm stx x)))
    
    ((#:lam-term #f l _  _ _)
     (brace-stx-scm stx x))

    ((#:scm-term (#:atom s . _) l _ _)
     (mk-scheme stx s l #t))

    ((#:variable X . _)
     #`(<scm> #,(datum->syntax stx X)))

    ((#:number x . _) x)

    ((and atom (#:atom f _ _ n m))
     (f->stxfkn #f #f f #f atom sscm #:scm stx #f n m '()))

    ((#:term (and atom (#:atom f . _)) l #f n m)
     (let ((l (get.. "," l)))
       (f->stxfkn #f #f f #f atom sscm #:sscm stx #f n m l)))))

(define (add x y z n m)
  (if z
      `((xfy _ "," _) ,x ((xfy _ "," _) ,y ,z ,n ,m) ,n ,m)
      ` ((xfy _ "," _) ,x ,y ,n ,m)))

(define (comma-it l n m)
  (if (null? l)
      '()
      (let* ((r (reverse l))
	     (h `(#:string ,(car r) ,n ,m))
	     (l (cdr r)))
	(let lp ((res h) (l l))
	  (if (pair? l)
	      (lp `((xfy _ "," _) (#:string ,(car l) ,n ,m) ,res ,n ,m) (cdr l))
	      res)))))

(define (->s s) (format #f "~a" s))
    
(define (dcg In Out x)
  (match (pp 'dcg x)
    ((#:group l . _)
     (dcg In Out l))
    (((_ _ "+" _) (#:list x n2 m2) n m)
     (dcg-list In Out (get-c (lambda (y) y) x) n2 m2))
    ((#:list () n m)
     `((xfx _ "=" _) 
       ,In ,Out ,n ,m))
    ((#:list x n m)
     `((xfx _ "=" _) 
       ,In (#:list ((xfy _ "|" _) ,x ,Out ,n ,m) ,n ,m)
       ,n ,m))
    (((and op (a b "," c)) x y n m)
     (let ((Y (list #:variable (gensym "Y") n m)))
       `(,op ,(dcg In Y x) ,(dcg Y Out y) ,n ,m)))
    (((and op (a b ";" c)) x y n m)
     `(,op ,(dcg In Out x) ,(dcg In Out y) ,n ,m))
    (((and op (a b "\\+" c)) x n m)
     (let ((Y (list #:variable '_ n m)))
       `(,op ,(dcg In Y x) ,n ,m)))
    (((and op (a b "->" c)) x y n m)
     (let ((Y (list #:variable (gensym "Y") n m)))
       `(,op ,(dcg In Y x) ,(dcg Y Out y) ,n ,m)))
    ((#:term nm x . (and l (_ n m)))
     (cons* #:term nm (add In Out x n m) l))
    ((and sym (#:atom (or '! 'true) _ _ n m))
     `((xfy _ "," _) ((xfx _ "=" _) ,In ,Out ,n ,m) ,sym ,n ,m))
    ((and sym (#:atom  (or 'fail 'false) _ _ n m))
     sym)
    ((and sym (#:atom nm _ _  n m))
     (list #:term sym (add In Out #f n m) #f n m))
    ((#:lam-term #f l #f n m) (=> fail)
     (if (string? l)
	 (fail)
	`((xfy _ "," _) ((xfx _ "=" _) ,In ,Out ,n ,m) ,l ,n ,m)))
    ((#:string s n m)
     (dcg In Out `(#:list ,(comma-it (map ->s (string->list s)) n m) ,n ,m)))
    (else
     (error "Got a wrong dcg term" x))))


(define (dcg-list In Out x n m)
  (match x
   ((a . b)
    (let ((Ain  (list #:variable (gensym "Ain") n m))
	  (Bin  (list #:variable (gensym "Bin") n m))
	  (Aout (list #:variable (gensym "Aout") n m))
	  (Bout (list #:variable (gensym "Bout") n m)))
   `((xfy _ "," _) 
     ((xfx _ "=" _) 
      (#:list ((xfy _ "|" _) ,Ain ,Bin ,n ,m) ,n ,m)
      In ,n ,m)
     ((xfy _ "," _) 
      ((xfx _ "=" _) (#:list ((xfy _ "|" _) ,Aout ,Bout ,n ,m) ,n ,m)
       Out ,n ,m)
      ((xfy _ "," _)
       ,(dcg Ain Aout a)
       ,(dcg-list Bin Bout b n m)
       ,n ,m)
      ,n ,m)
     ,n ,m)))

   (()
    `((xfy _ "," _) 
      ((xfx _ "=" _) 
       In (#:list () ,n ,m) ,n ,m)
      ((xfx _ "=" _) 
       Out (#:list () ,n ,m) ,n ,m)
      ,n ,m))
   (x
    (dcg In Out x))))
