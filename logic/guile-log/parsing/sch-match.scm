(define-module (logic guile-log parsing sch-match)
  #:use-module (logic guile-log parsing scheme)
  #:use-module (logic guile-log parser)
  #:export (~and ~or ~not ~var _  scm-sed scm-match)
  #:re-export (define-match-class define-splicing-match-class)) 

(define-match-syntax ~and and-matcher)
(define-match-syntax ~or  or-matcher)
(define-match-syntax ~not not-matcher)
(define-match-syntax ~var var-matcher)
(define-match-syntax _    anon-matcher)

(define-syntax-rule (scm-match pat)
  (lambda (pre bang post)
    (ck () (compile-match 'pat 'pre 'bang 'post))))

(define-syntax-rule (scm-sed . l) (sed-parse . l))
