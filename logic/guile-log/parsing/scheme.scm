(define-module (logic guile-log parsing scheme)
  #:use-module (ice-9 match)
  #:use-module (logic guile-log ck)
  #:use-module (ice-9 pretty-print)
  #:use-module ((syntax parse) #:select (syntax-parse 
					 pattern
					 define-syntax-class
					 define-splicing-syntax-class))
  #:use-module (logic guile-log parser)
  #:use-module (syntax id-table)
  #:use-module (compat racket misc)
  #:use-module ((logic guile-log) #:select (<code> <var>))
  #:export (compile-match compile-parse sed-parse
	    and-matcher or-matcher not-matcher var-matcher anon-matcher
	    ck
	    define-match-syntax 
	    define-match-class  define-splicing-match-class 
	    Self
	    ))

(define (pp x)
  (pretty-print x)
  x)

(define-syntax-rule (mk tok1 tok2 f* s** f-not f-tag white no-nl any butq
			f-nl s-char s-reg line-com com ws ws+ char string
			mk-par sexp mk-token s-pk)
(begin
  (define white (f-or f-nl (f-char #\space) (f-char #\tab)))
  (define no-nl (s-reg "[^\n]"))
  (define any   (s-reg "."))
  (define butq  (s-reg "[^\"]"))

  
  (define line-com
    (letrec ((semi  (f-char #\;))
	     (it    no-nl)
	     (f     (f-or! f-eof f-nl (f-seq!! it (Ds f)))))
      (f-seq semi f)))
	   
  
  (define com
    (letrec* ((right     (f-tag "|#"))
	      (left      (f-tag "#|"))
	      (not-right (f-not right))
	      (it        (f-or! f-nl f not-right))
	      (g         (f-or!  f-eof (f-seq!! it (Ds g)) right))
	      (f         (f-seq!! left g)))
      f))
  
  (define char
    (let ((sharp (f-tag "#"))
	  (ch1   (f-tag "\\("))
	  (ch2   (f-tag "\\)"))
	  (ch3   (f-tag "\\\""))
	  (ch4   (f-tag "\\;")))
      (f-seq sharp (f-or! ch1 ch2 ch3 ch4))))

  (define ws  (mk-token (f* (f-or! white line-com com))))
  (define ws+ (f+ (f-or! white line-com com)))
  
  (define string
    (letrec* ((bs    (f-tag "\\"))	   
	      (q     (f-tag "\""))
	      (quote (f-seq  bs any))
	      (it    (f-and!
		      (f-or f-nl quote butq)))
	      (str   (f-or f-eof (f-seq it (Ds str)) q)))
	 (f-seq q str)))
  


  (define sexp 
    (let ()
      (define (tok-it pre ! post m) 
	(tok2 (f-seq!! (tok1 ws) 
		       pre   (tok1 m) ! post
		       (tok1 ws))))

      (letrec* ((pa-left  (f-tag "("))
		(pa-right (f-tag ")"))
		(br-left  (f-tag "["))
		(br-right (f-tag "]"))		
		(rest-tok (mk-token
			   (f+ (f-seq 
				(f-not (f-or ws+
					     pa-left pa-right
					     br-left br-right))
				))))
		(char-tok   (mk-token char))
		(string-tok (mk-token string))
		(sx       (lambda (pre ! post)
			    (tok-it pre ! post
			      (f-or!
				(f-seq 
				       pa-left 				      
				       (s** (Ds (sx f-true f-true 
						    f-true)))
				       pa-right)
				(f-seq br-left 
				       (s** (f-seq 
					     (Ds (sx f-true f-true 
						     f-true))))
				       br-right)
				string-tok
				char-tok
				rest-tok)))))
				
	       (case-lambda 
		(()           (sx f-true f-true f-true))
		((pre ! post) (sx pre ! post))))))))

(define (s** m) 
  (letrec ((g (<p-lambda> (c)
		(<var> (w)
		  (.. (d) (m w))
		  (<p-cc> (cons d c)))))
	   (u   (f* g)))
    (<p-lambda> (c)		
      (.. (c) (u '()))
      (<p-cc> (reverse c)))))


(define-syntax-rule (id x) x)
(define (tok1 tok)
  (<p-lambda> (c)
    (.. (d) (tok c))
    (<p-cc> (cons d c))))

(define (tok2 m)
  (<p-lambda> (c)
     (.. (c) (m '()))
     (<p-cc> (match c ((z y x) (vector x y z))))))

(define-syntax-rule (mute-pk x) f-true)

(mk id id f* f* f-not f-tag white no-nl any butq
    f-nl f-char f-reg line-com com ws ws+ char string 
    mk-par sexp id mute-pk)

(mk tok1 tok2 f* s** f-not! f-tag! white! no-nl! any! butq! 
    f-nl! f-char! f-reg! line-com! com! ws! ws+! char! 
    string! 
    mk-par! sexp! mk-token s-pk)

(mk id id f* f* pr-not f-tag-pr white pr-no-nl pr-any pr-butq
    f-nl-pr pr-char pr-reg pr-line-com pr-com pr-ws
    pr-ws+ pr-char
    pr-string pr-mk-par
    pr-sexp id mute-pk)
		       
(define s-parens
  (case-lambda
   ((m a b c)
    (letrec ((pa-left  (f-tag "("))
	     (pa-right (f-tag ")"))
	     (br-left  (f-tag "["))
	     (br-right (f-tag "]"))
	     (sx       (f-seq 
			ws
			(f-or!
			 (f-seq pa-left m pa-right)
			 (f-seq br-left m br-right))
			ws)))
		      

      (f-seq!! ws a (f-or!
		     (f-seq pa-left m pa-right)
		     (f-seq br-left m br-right)) b c ws)))
   ((m)
    (s-parens m f-true f-true f-true))))

;; ---------------------------------------------------------------------
(define body #f)
(define fkn  #f)

(define-syntax-rule (define-ck-rule o (nm . pat) res)
  (define-syntax nm
    (syntax-rules o
      ((_ . pat) res))))

(define-syntax-class (Iexp v)
  (pattern #(ws-l m ws-r)
	   #:fail-unless (equal? (syntax->datum #'m) (format #f "~a" v))
	   (format #f "failed to match literal ~a" v)   
	   #:with l  #'ws-l
	   #:with r  #'ws-r
	   #:with it #'m)
  (pattern m
	   #:fail-unless (equal? (syntax->datum #'m) (format #f "~a" v))
	   (format #f "failed to match literal ~a" v)   
	   #:with l  ""
	   #:with r  ""
	   #:with it #'m))

(define-syntax-class Sexp
  (pattern #(ws-l m ws-r)
	   #:with l  #'ws-l
	   #:with r  #'ws-r
	   #:with it #'m)
  (pattern m
	   #:with l  ""
	   #:with r  ""
	   #:with it #'m))
   

(define E 
  (case-lambda
   ((f)               (f-seq!! ws f ws))
   ((f pre bang post) (f-seq!! ws pre f bang post ws))))

(define-ck-rule (quote) (p-par  s 'it)       (ck s '(~var _   (Iexp it))))
(define-ck-rule (quote) (p-var  s 'var)      (ck s '(~var var Sexp)))
(define-ck-rule (quote) (p-item s 'it)       (ck s '(~var _   (Iexp 'it))))
(define-syntax p-list 
  (syntax-rules (quote)
    ((_ s '#f '(it ...)) 
     (ck s '#(_ (it ...) _)))
    ((_ s '#t '(it ...)) 
     (ck s '(it ...)))))

(define-ck-rule (quote) (p* s 'p '(l ...))     (ck s '(p (... ...) l ...)))

(define-ck-rule (quote) (e-par  s 'it)       (ck s '(E (f-tag it))))
(define-ck-rule (quote) (e-var  s 'var)      (ck s '(sexp)))
(define-ck-rule (quote) (e-item s 'it)       (ck s '(E (f-tag 'it))))
(define-ck-rule (quote) (e-list s 'l '(x ...))
  (ck s '(s-parens (f-seq x ...))))
(define-ck-rule (quote) (e* s 'p '(l ...))   (ck s '((f* p) l ...)))

(define mute-it  #'(f-and f-or f-not e-item f-seq e* e-list e-var e-par #f))
(define mute-env #`'(#t #,mute-it #,@mute-it))
(define parse-it #'(~and  ~or  ~not  p-item  ~seq p* p-list p-var p-par #t))
(define parse-env #`'(#f #,parse-it #,@parse-it))

(define env!     #''(s-and! f-or! f-not! f-tag!))

(define-ck-rule (quote) (mute s '(_ m . l)) (ck s '(#t m . m)))

(define-syntax-rule (mk-get nm tag li)
  (define-syntax nm
    (lambda (x)
      (syntax-case x ()
	((_ s 'li) #'(ck s 'tag))))))

(mk-get ck-and  and  (_ _ and                                        . _))
(mk-get ck-or   or   (_ _ and or                                     . _))
(mk-get ck-not  not  (_ _ and or not                                 . _))
(mk-get ck-item item (_ _ and or not item                            . _))
(mk-get ck-cons cons (_ _ and or not item cons                       . _))
(mk-get ck-n    n... (_ _ and or not item cons n...                  . _))
(mk-get ck-list list (_ _ and or not item cons n... list             . _))
(mk-get ck-var  var  (_ _ and or not item cons n... list var         . _))
(mk-get ck-par  par  (_ _ and or not item cons n... list var par     . _))
(mk-get ck-cls  cls  (_ _ and or not item cons n... list var par cls . _))

(define-syntax compile-par-quote
  (syntax-rules (unquote quasiquote quote)
    ((_ s 'env '((unquote x) . l))
     (ck s (dispatch (is-macro? 'x) 'x 'env 'fkn 'l
		     'compile-par-quote 'compile-body-quote)))
    ((_ s 'env '(unquote x))
     (ck s (dispatch (is-macro? 's) 'x 'env 'top '()  
		     'compile-par-quote 'compile-body-quote)))
    ((_ s 'env '(x . l))
     (ck s (match-list (ck-list 'env) (compile-body-quote 'env '(x . l)))))
    ((_ s 'env 'w)
     (ck s (match-item (ck-item 'env) 'w)))))

(define-syntax rebuild
  (syntax-rules (quote _) 
    ((rebuild s '#(_ p _) 'a 'b)
     (ck s '#(a p b)))
    ((rebuild s 'p 'a 'b)
     (ck s '#(a p b)))))
    

(define-syntax match-vect
  (syntax-rules (quote)
    ((_ s '#t 'env 'a 'x 'b 'comp)
     (ck s (rebuild (comp '#f 'env 'x) 'a 'b)))
    ((_ s '#f 'env 'a 'x 'b 'comp)
     (comp s '#f 'env 'x))))

(define-syntax compile-par
  (lambda (x)
    (syntax-case x (unquote quasiquote quote)
      ((_ s 'l 'env '#(a x b))
       #'(ck s (match-vect (ck-cls 'env) 'env 'a 'x 'b 'compile-par)))

      ((_ s 'l 'env '(quasiquote x))
       #'(compile-par-quote s 'l 'env 'x))

      ((_ s 'l 'env '(unquote x))
       #'(ck s (match-par (ck-par 'env) 'x)))

      ((_ s 'l 'env ''x)
       #'(ck s (match-item (ck-item 'env) 'x)))

      ((_ s 'll 'env '(x . l))
       (identifier? #'x)
       #'(ck s (dispatch 'll (is-macro? 'x) 'x 'env 'fkn 'l 
			 'compile-par 'compile-body)))

      ((_ s 'll 'env '(x ooo . l))
       (and (identifier? #'ooo) (eq? (syntax->datum #'ooo) '...))
       #'(ck s (match-list 'll (ck-list 'env) 
			   (match... (ck-n 'env) (compile-par '#f 'env 'x) 
				     (compile-body 'env 'l)))))
       
      ((_ s 'll 'env '(x . l))
       #'(ck s (match-list 'll (ck-list 'env) 
			   (match-cons (compile-par '#f 'env 'x)
				       (compile-body 'env 'l)))))

      ((_ s 'l 'env 'x)
       #'(ck s (dispatch 'l (is-macro? 'x) 'x 'env 'top '()
			 'compile-par 'compile-body))))))

(define-ck-rule (quote) (match-list s 'll 'list 'w) (list s 'll 'w))
(define-ck-rule (quote) (match-item s 'item 'w) (item s 'w))
(define-ck-rule (quote) (match...   s 'n* 'p '(l ...))
  (n* s 'p '(l ...)))

(define-syntax match-cons
  (syntax-rules (quote) 
    ((match-cons s 'x '())    
     (ck s '(x)))
    ((match-cons s 'x 'y)    
     (ck s '(x . y)))))


(define-syntax dispatch
  (lambda (x)
    (syntax-case x (quote fkn body top)
      ((_ s 'll '#t 'x . l)
       #'(x s 'll .  l))

      ((_ s 'll '#f 'x 'env 'top _ p b)
       (identifier? #'x)
       #'(ck s (make-var (ck-var 'env) 'x)))

      ((_ s 'll '#f 'x 'env 'body '(ooo . l) 'p 'b)
       (and (identifier? #'ooo) (eq? (syntax->datum #'ooo) '...))
       #'(ck s (match... (ck-n 'env) (compile-par '#f 'env 'x) 
			 (compile-body 'env 'l))))
      
      ((_ s 'll '#f 'x 'env 'body 'l 'p 'b)
       #'(ck s (match-cons (p '#f 'env 'x) (b 'env 'l))))

      ((_ s 'll '#f 'x 'env 'fkn '(ooo . l) 'p 'b)
       (and (identifier? #'ooo) (eq? (syntax->datum #'ooo) '...))
       #'(ck s (match-list 'll (ck-list 'env) 
			   (match... (ck-n 'env) (compile-par '#f 'env 'x) 
				     (compile-body 'env 'l)))))
      
      ((_ s 'll '#f 'x 'env 'fkn 'l 'p 'b)
       #'(ck s (match-list 'll (ck-list 'env) 
			   (match-cons (p '#f 'env 'x) (b 'env 'l)))))
      ((_ s 'll _ 'x 'env . l)
       #'(ck s (match-item (ck-item 'env) 'x))))))

(define-ck-rule (quote) (make-var s 'var 'x)  (var s 'x))
(define-ck-rule (quote) (match-par s 'par 'x) (par s 'x))

(define-syntax compile-body
  (lambda (x)
    (syntax-case x (quote quasiquote unquote)
      ((_ s 'env '(x  ooo . l))
       (and (identifier? #'ooo) (eq? (syntax->datum #'ooo) '...))
       #'(ck s (match... (ck-n 'env) (compile-par '#f 'env 'x) 
			 (compile-body 'env 'l))))

      ((_ s 'env '(x . l))
       (identifier? #'x)
       #'(ck s (dispatch '#f (is-macro? 'x) 'x 'env 'body 'l
			 'compile-par 'compile-body)))

      ((_ s 'env '())
       #'(ck s '()))

      ((_ s 'env '(x . l))
       #'(ck s (match-cons (compile-par '#f 'env 'x) 
			   (compile-body 'env 'l)))))))

(define-syntax compile-body-quote
  (lambda (x)
    (syntax-case x (unquote quote)
      ((_ s 'env '(x  ooo . l))
       (and (identifier? #'ooo) (eq? (syntax->datum #'ooo) '...))
       #'(ck s (match... (ck-n 'env) (compile-par-quote 'env 'x) 
			 (compile-body-quote 'env 'l))))

      ((_ s 'env '((unquote x) . l))
       #'(ck s (dispatch (is-macro? 'x) 'x 'env 'body 'l)))

      ((_ s 'env '())
       #'(ck s '()))

      ((_ s 'env '(x . l))
       #'(ck s (match-cons (compile-par-quote  'env 'x) 
			   (compile-body-quote 'env 'l))))
      ((_ s 'env 'x)
       #'(compile-par-quote s 'env 'x)))))

(define *match-macros* (make-free-id-table))

(define-syntax is-macro? 
  (lambda (x)
    (syntax-case x (quote)
      ((_ s 'x)
       (if (and (identifier? #'x)
		(free-id-table-ref *match-macros* #'x #f))
	   #'(ck s '#t)
	   #'(ck s '#f))))))

(define-syntax define-match-syntax
  (lambda (x)
    (syntax-case x ()
      ((_ n . l)
       #'(begin
	   (define-syntax n . l)
	   (define-values ()
	     (begin
	       (free-id-table-set! *match-macros* #'n #t)
	       (values))))))))
	   

(define-ck-rule (quote) (mute s '(_ l . _)) (ck s '(#t l . l)))
(define and-matcher
  (syntax-rules (body fkn quote)
    ((_ s 'l 'env 'fkn '(a ... b) 'compile-par 'c2)
     (ck s (match-and (ck-and 'env) 
		      (compile-par 'l (mute 'env) 'a) ... 
		      (compile-par 'l 'env 'b))))))

(define-syntax match-and
  (syntax-rules (quote)
    ((_ s 'and 'a ...)
     (ck s '(and a ...)))))

(define or-matcher
  (syntax-rules (body fkn quote)
    ((_ s 'l 'env 'fkn '(a ...) 'compile-par 'c2)
     (ck s (match-or (ck-or 'env) (compile-par 'l 'env 'a) ...)))))

(define-syntax match-or
  (syntax-rules (quote)
    ((_ s 'or 'a ...)
     (ck s '(or a ...)))))

(define not-matcher
  (syntax-rules (body fkn quote)
    ((_ s 'l 'env 'fkn '(a) 'compile-par 'c2)
     (ck s (match-not (ck-not 'env) (compile-par 'l 'env 'a))))))

(define-syntax match-not
  (syntax-rules (quote)
    ((_ s 'not 'a)
     (ck s '(not a)))))

(define var-matcher
  (syntax-rules (quote fkn)
    ((_ s 'l 'env 'fkn '(v class) . _)
     (ck s (match-var2 (ck-cls 'env) 'v 'class)))))

(define-syntax match-var2
  (syntax-rules (quote) 
    ((match-var2 s 'kind 'v '(cl . a))
     (cl s 'kind 'v 'a))
    ((match-var2 s 'kind 'v 'cl)
     (cl s 'kind 'v))))

(define anon-matcher
  (syntax-rules (quote fkn body top)
    ((_ s 'll 'env 'fkn 'l 'par 'b)
     (ck s (match-list 'll (ck-list 'env)
	     (match-anon (ck-cls 'env) (b 'env 'l)))))
    ((_ s 'll 'env 'body 'l 'par 'b)
     (ck s (match-anon (ck-cls 'env) (b 'env 'l))))
    ((_ s 'll 'env 'top '() 'par 'b)
     (ck s (match-anon (ck-cls 'env) '())))))

(define-syntax match-anon
  (syntax-rules (quote)
    ((_ s '#t '())
     (ck s '_))
    ((_ s '#t '(l ...))
     (ck s '(~seq _ l ...)))
    ((_ s '#f '())
     (ck s '(sexp)))
    ((_ s '#f '(l ...))
     (ck s '(f-seq!! (sexp) l ...)))))

(define-syntax sed-getter
  (lambda (x)
    (syntax-case x (quote)
      ((_ s 'p)
       #`(ck s (compile-par '#f #,mute-env 'p))))))

(define-syntax sed-setter
  (lambda (x)
    (syntax-case x (quote)
      ((_ s 'p '(res ...))
       #`(ck s (sed-setter-end (compile-par '#f #,parse-env 'p) 
			       '(res ...)))))))

(define-ck-rule (quote) (sed-setter-end s 'pat '(res ...))
  (ck s '(pat res ...)))

(define-ck-rule (quote) (sed-parse (pat res ...) ...)
  (ck () (sed-compile (getter (sed-getter 'pat) ...)
		      (setter (sed-setter 'pat '(res ...)) ...))))

(define-ck-rule (quote) (getter s 'm ...) (ck s '(f-or! m ...)))

(define-ck-rule (quote) (setter s 'm ...) 
  (ck s '(lambda (x) (syntax-parse x m ...))))

(define (sed-print x)
  (syntax-case x ()
    (#(a b c)
     (begin
       (format #t "~a" (syntax->datum #'a))
       (sed-print #'b)
       (format #t "~a" (syntax->datum #'c))
       #t))
    ((x ...)
     (begin       
       (format #t "(")
       (for-each sed-print #'(x ...))
       (format #t ")")
       #t))
    (x
     (begin
       (format #t "~a" (syntax->datum #'x))
       #t)))
  (if #f #f))

(define-syntax-parameter Self (lambda (x) 
				(error "Self must be in grep/sed macro")))

(define-ck-rule (quote) (sed-compile s 'match 'f)
  (ck s (ck-pk '(lambda (a b cc)
		  (let ((m match)
			(l (<p-lambda> (c)
			     (.. (c) ((sexp! a b cc) c))
			     (<code> (sed-print (f c))
				     #;(format #t "~%"))
			     (<p-cc> 'ok))))
		    (f-and m l))))))

(define-syntax ck-pk
  (lambda (x)
    (syntax-case x (quote)
      ((_ s 'x) 
       (begin
	 (pretty-print (syntax->datum #'x))
	 #'(ck s 'x))))))

(define-ck-rule (quote) (add-vars s '(p x ...) 'a 'b 'c)
  (ck s '(p x ... a b c)))

(define-syntax compile-match
  (lambda (x)
    (syntax-case x (quote)
      ((_ s 'p) 
       #`(ck s (compile-par '#f #,mute-env 'p))))))

(define-syntax compile-parse
  (lambda (x)
    (syntax-case x (quote)
      ((_ s 'p) #`(ck s (compile-par '#f #,parse-env 'p))))))

(define-ck-rule (quote) (make-stx-class s 'nm 'pat ... '((l ...) ...))
  (ck s '(define-syntax-class nm (pattern pat l ...) ...)))

(define-syntax define-match-class 
  (lambda (x)
    (syntax-case x (pattern)
      ((_ (nm . args) (pattern pat l ...) ...)
       (with-syntax ((nm-class (datum->syntax
				#'nm (string->symbol
				      (string-append
				       (symbol->string (syntax->datum #'nm))
				       "-class"))))
		     (nm-match (datum->syntax
				#'nm (string->symbol
				      (string-append
				       (symbol->string (syntax->datum #'nm))
				       "-match")))))
         #'(begin
	     (ck () (ck-pk
		     (make-stx-class '(nm-class . args) 
				     (compile-parse 'pat) ... 
				     '((l ...) ...))))
	     (define (nm-match . args)
	       (f-or (ck () (compile-match 'pat) ...)))

	     (define-syntax nm
	       (syntax-rules (quote)
		 ((_ s '#t 'var 'a)
		  (ck s '(~var var (nm-class . a))))
		 ((_ s '#t 'var)
		  (ck s '(~var var nm-class)))
		 ((_ s '#f 'var 'a)
		  (ck s '(nm-match . a)))
		 ((_ s '#f 'var)
		  (ck s 'nm-match)))))))

      ((_ nm (pattern pat l ...) ...)
       (with-syntax ((nm-class (datum->syntax
				#'nm (string->symbol
				      (string-append
				       (symbol->string (syntax->datum #'nm))
				       "-class"))))
		     (nm-match (datum->syntax
				#'nm (string->symbol
				      (string-append
				       (symbol->string (syntax->datum #'nm))
				       "-match")))))
         #'(begin
	     (ck () (ck-pk (make-stx-class 'nm-class (compile-parse 'pat) ... 
					   '((l ...) ...))))
	     (define nm-match (f-or (ck () (compile-match 'pat) ...)))
	     (define-syntax nm
	       (syntax-rules (quote)
		 ((_ s '#t 'var)
		  (ck s '(~var var nm-class)))
		 ((_ s '#t 'var 'a)
		  (ck s '(~var var (nm-class . a))))
		 ((_ s '#f 'var 'a)
		  (ck s '(nm-match . a)))
		 ((_ s '#f 'var)
		  (ck s 'nm-match))))))))))


(define-ck-rule (quote) (make-stx-splicing-class s 'nm 'pat ... ((l ...) ...))
  (ck s '(define-splicing-syntax-class nm (pattern pat l ...) ...)))

(define-syntax define-splicing-match-class 
  (lambda (x)
    (syntax-case x (pattern)
      ((_ (nm . args) (pattern pat l ...) ...)
       (with-syntax ((nm-class (datum->syntax
				#'nm (string->symbol
				      (string-append
				       (symbol->string (syntax->datum #'nm))
				       "-class"))))
		     (nm-match (datum->syntax
				#'nm (string->symbol
				      (string-append
				       (symbol->string (syntax->datum #'nm))
				       "-match")))))
         #'(begin
	     (ck () (make-stx-splicing-class '(nm-class . args)
					     (compile-parse 'pat) ... 
					     '((l ...) ...)))
	     (define (nm-match . args) (f-or (ck () (compile-match 'pat)) ...))
	     (define-syntax nm
	       (syntax-rules (quote)
		 ((_ s '#t 'var 'a)
		  (ck s '(~var var (nm-class . a))))
		 ((_ s '#t 'var)
		  (ck s '(~var var nm-class)))
		 ((_ s '#f 'var 'a)
		  (ck s '(nm-match . a)))
		 ((_ s '#f 'var)
		  (ck s 'nm-match)))))))

      ((_ nm (pattern pat l ...) ...)
       (with-syntax ((nm-class (datum->syntax
				#'nm (string->symbol
				      (string-append
				       (symbol->string (syntax->datum #'nm))
				       "-class"))))
		     (nm-match (datum->syntax
				#'nm (string->symbol
				      (string-append
				       (symbol->string (syntax->datum #'nm))
				       "-match")))))
         #'(begin
	     (ck () (make-stx-splicing-class 'nm-class (compile-parse 'pat) ... 
				    '((l ...) ...)))
	     (define nm-match (f-or (ck () (ck-pk (compile-match 'pat)) ...)))
	     (define-syntax nm
	       (syntax-rules (quote)
		 ((_ s '#t 'var)
		  (ck s '(~var var nm-class)))
		 ((_ s '#t 'var 'a)
		  (ck s '(~var var (nm-class . a))))
		 ((_ s '#f 'var)
		  (ck s 'nm-match))
		 ((_ s '#f 'var 'a)
		  (ck s '(nm-match . a)))))))))))
