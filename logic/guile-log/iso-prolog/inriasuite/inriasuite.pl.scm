(use-modules (logic guile-log iso-prolog))
(eval-when (compile eval load)
  (make-unbound-term or)
  (make-unbound-term and))

(compile-file "inriasuite.pl")
(save-operator-table)