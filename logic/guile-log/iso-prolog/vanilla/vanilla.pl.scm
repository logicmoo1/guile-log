(use-modules (logic guile-log iso-prolog))
(define car #f)
(eval-when (compile eval load)
  (make-unbound-term or)
  (make-unbound-term and))
(reset-prolog)

(compile-prolog-file "vanilla.pl")

(save-operator-table)
(prolog-run 1 (local_initialization))
