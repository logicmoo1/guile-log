(define-module (logic guile-log vm vm)
  #:use-module (logic guile-log vm op)
  #:use-module (logic guile-log vm fkn)
  #:use-module (logic guile-log vm utils)
  #:use-module (logic guile-log vm push)
  #:use-module (logic guile-log vm unify)
  #:use-module (logic guile-log vm newframe)
  #:use-module (logic guile-log vm unwind)
  #:use-module (logic guile-log vm call)
  #:re-export (                              
                   ;; unwind
                   unwind-light-tail unwind-light unwind unwind-negation
                   unwind-tail
                   unwind-ps unwind-psc unwind-p
                   post-negation  post-s post-q post-c
                   softie-light softie-ps softie-pc post-sc softie-psc
		   restore-c restore-s restore-pc restore-p fail-psc fail-pc fail-p0 fail-p
                   
                   ;; call
		   in-call cc-call in-tailcall in-post-call post-unicall
                   push-3 pushtail-3 handle-spcc cut scmcall0
                   
                   ;; newframe
                   newframe-ps newframe-pc newframe-psc newframe-light newframe
                   newframe-negation store-state store-ps store-p store-s
                   
                   ;; utils
                   gfalse gtrue sp svar inlabel init-proc base
                   
                   ;; push
                   cutter goto-inst sp-move equal-instruction
		   push-instruction pushv push-variable
		   pop-variable pop seek dup clear-sp push_at
		   set_p push-variable-scm push-s
                   
                   ;; unify
                   ggset unify unify-2 unify-constant-2
                   unify-instruction pre-unify post-unify-tail
                   post-unify unify-instruction-2

                   
                   ;; fkn
                   icons! ifkn! icurly!
		   mk-cons mk-fkn mk-curly
		   icons ifkn icurly do_n_cons
                   
                   ;; op
                   plus minus band bor xor modulo mul shift-l shift-r
		   divide gt lt le eq neq
                   ))
    



  
