(define-module (logic guile-log vm call)
  #:use-module ((logic guile-log iso-prolog)
		#:renamer
		(lambda (x)
		  (cond
		   ((eq? x 'reset)
		    'reset_iso)
		   ((eq? x 'cc)
		    'cc_iso)
		   ((eq? x 'pp)
		    'pp_iso)
		   (else
		    x))))
  
  #:use-module (logic guile-log vm utils)
  #:use-module (logic guile-log vm fkn)
  #:export (in-call cc-call in-tailcall in-post-call post-unicall
                    push-3 pushtail-3 handle-spcc cut scmcall0))

(compile-prolog-string
"
   -trace.
   'handle-spcc'(C,I,I,L,LL) :-
      generate(spcc(C),L,LL).
/*
      NCUT  is C,
      NCC   is C + 1,
      NP    is C + 2,
      NS    is C + 3,
      NVEC  is C + 4,
      write(1),
      id(cut,var(ICUT)),
      write(2),
      id(cc,var(ICC)),
      write(1),
      generate('scm-set!/immediate'(NVEC,ICUT,NCUT),L,L1),
      generate('scm-set!/immediate'(NVEC,ICC,NCC),L1,L2),
      write(2),
      gset(p,svar(NP),I,L2,L3),
      gset(s,svar(NS),I,L3,L4),
      write(3),
      gset(vec,svar(NVEC),I,L4,L5),
      write(4),
      gset(const,vconst,I,L5,LL).
*/
    
   'push-3'(P,C,I,II,L,LL) :-
      args([cc(C),pp(P),s],I,II,L,LL).

   'pushtail-3'(P,I,II,L,LL) :-
      args([cc,pp(P),s],I,II,L,LL).

   'cc-call'(P,I,0,L,LL) :-
     scmtailcall(cc,[pp(P),s],0,_,L,LL).

   move_block(0,I,I,L,L).
   move_block(N,I,II,L,LL) :-
      I2 is I - 1,
      NN is N - 1,
      move(NN,base+I2,L,L2),
      move_block(NN,I2,II,L2,LL).

   move_block_rev(N,N,I,L,L).
   move_block_rev(N,I,II,L,LL) :-      
      move(N,base+I,L,L2),
      NN is N + 1,
      move_block_rev(NN,I2,II,L2,LL).

   'in-call'(N,I,II,L,LL) :-
     NN is N + 4,
     M  is I - NN - 1,
     move_block(NN,I,II,L,L2),
     tailstub(NN,L2,LL).

   'in-tailcall'(N,I,II,L,LL) :-
     NN is N + 4,
     M  is I - NN - 1,
     move_block(NN,I,II,L,L2),
     tailstub(NN,L2,LL).

   'in-post-call'(I,II,L,LL) :-
      generate('assert-narg-ee/locals'(3,nvec),L,L1),
      gset(const,vconst,I,L1,LL),
      II = 0.
      
   'post-unicall'(I,II,L,LL) :-
      'goto-stackstart'(II),
      'install-stack'(I1,II,L,LL).

    cut(ground(_),V,I,I,L,L) :- !.
    cut(_,false,I,I,L,L) :- !,
       gset(p,cut,I,L,LL).
    cut(_,V,I,I,L,LL) :-
       gset(p,V,I,L,LL).

    -trace.
    scmcall0(N,II,I,L,LL) :-
       generate(call(N,I),L,L1),
       reset(II,L1,LL).
")
    
