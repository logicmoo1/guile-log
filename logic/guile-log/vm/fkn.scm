(define-module (logic guile-log vm fkn)
  #:use-module ((logic guile-log iso-prolog)
		#:renamer
		(lambda (x) (if (eq? x 'reset)
				'reset_iso
				x)))
  #:use-module (logic guile-log vm utils)
  #:export (icons! ifkn! icurly!
		   mk-cons mk-fkn mk-curly
		   icons ifkn icurly do_n_cons))

(define fkn!? (lambda (x) x))

(compile-prolog-string
"
  'icons!'(I,II,L,LL) :-
    J is I - 1,
    generate('icons!'(base+J),L,LL),
    II is I + 1.

  'ifkn!'(I,L,LL) :-
     J is I - 1,
     generate('ifkn!'(base+J),L,LL).

  'icurly!'(I,L,LL) :-
     J is I - 1,
     generate('icurly!'(base+J),L,LL).

   'mk-cons'(I,II,L,LL) :-
      generate('mk-cons'(base+I),L,LL),
      II is I - 1.

   'mk-fkn'(N,I,II,L,LL) :-
      generate('mk-fkn'(N,base+I),L,LL),
      II is I - N + 1.

   'mk-curly'(I,L,LL) :-
      generate('mk-curly'(base+I),L,LL).

   icons(I,II,L,LL) :-
      J is I - 1,
      generate(icons(base+J),L,LL),
      II is I + 1.

   ifkn(I,L,LL) :-
      J is I - 1,
      generate(ifkn(base+J),L,LL).

   icurly(I,L,LL) :-
      J is I - 1,
      generate(icurly(base+J),L,LL).
")
  
