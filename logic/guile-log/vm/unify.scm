(define-module (logic guile-log vm unify)
  #:use-module (logic guile-log vm utils)
  #:use-module (logic guile-log vm call)
  #:use-module ((logic guile-log iso-prolog)
		#:renamer
		(lambda (s)
		  (cond
		   ((eq? s 'cc) 'pl-cc)
		   ((eq? s 'reset) 'pl-reset)
		   (else s))))
  
  #:export (ggset unify unify-2 unify-constant-2 unify-instruction-2
                  unify-instruction pre-unify post-unify-tail
                  post-unify
))


(compile-prolog-string
"

  ggset(V,I,I,L,LL) :-     
     scmcall('gp-set',[V,sp(-1),s],I,I1,L,L1),
     reset(I,L1,LL).

  -trace.
  'pre-unify'(V,I,I,L,LL) :-
     generate('pre-unify'(base+I),L,L1),
     gset(V,sp(I),I,L1,LL).

  'post-unify'(V,Nsloc,I,I,L,LL) :-
     gref(V,sp(I),L,L1),    
     generate('post-unify'(base+I,E),L1,L2),
     generate('recap'(base+I),L2,L3),
     label(E,L3,LL).

  'post-unify-tail'(V,I,0,L,LL) :-
     gref(V,sp(I),L,L1),    
     generate('post-unify-tail'(base+I),L1,LL).

   -trace.
   unify(Code,V,I,II,L,LL) :-
      M is (Code /\\ 3),
      A is (Code /\\ 4)  >> 2,
      K is (Code /\\ 24) >> 3,      
      gset(sp(I),V,I,L,L1),
      (
        M==2 ->
          generate('gp-m-unify',L1,L2);
        (M==3 ; K==3) ->
          generate('gp-unify-raw',L1,L2);
        generate('gp-unify',L1,L2)
      ),
      II is I - 1,
      reset(II,L2,LL).



   'unify-2'(Code,V1,V2,I,L,LL) :-
      M     is Code /\\ 3,
      Code2 is Code >> 2,
      A1    is Code2 /\\ 1,
      K1    is (Code2 /\\ 6) >> 1,
      Code3 is Code2 >> 24,
      A2    is Code3 /\\ 1,
      K2    is (Code3 /\\ 6) >> 1,
      
      (
          K1 == 2 ->
              (
                 (
                    K2 == 2 ->
                       (
                         scmcall('gp-var!',[s],I,I1,L,L1),
                         gset(V2,sp(I1-1),L1,L2),
                         reset(I,L2,LL)
                       );
                    L2=L
                 )
              );
          (
              K2 == 2 ->
                 (
                    gset(V2,V1,I,L,L1),
                    reset(I,L1,LL)
                 );
              (
                 M == 2 ->
                   (
                      pltest('gp-m-unify',[V1,V2,s],I,_,L,L1),
                      reset(I,L1,LL)
                   );
                 (
                    (
                      (M==3;K1==3;K2==3) ->
                         pltest_s('gp-unify-raw',[V1,V2,s],I,L,L1);
                      pltest_S('gp-unify',[V1,V2,s],I,L,L1)
                    ),
                    reset(I,L1,LL)
                 )
              )
          )
      ).

   'unify-instruction-2'(M,C,V,K,I,II,L,LL) :- 'unify-constant-2'(M,C,V,K,I,II,L,LL).
   -trace.
   'unify-constant-2'(M,C,V,K,I,I,L,LL) :-
      I1 is I + 1,
      I2 is I + 2,
      (
        K == #f ->
          gset(V,c(C),I,L,LL);
        (
             M == #f ->
                (
                   reset(I2,L,L1),
                   gset(sp(I),V,I1,L1,L2),
                   gset(sp(I1),c(C),I2,L2,L3),
                   generate('gp-m-unify'(base+I+1),L3,L4),
                   reset(I,L4,LL)
                );
             (
               reset(I2,L,L1),
               gset(sp(I),V,I1,L1,L2),
               gset(sp(I1),c(C),I2,L2,L3),
               (
                 (M=#t;K=#t) ->
                    generate('gp-unify-raw'(base+I1),L3,L4);
                  generate('gp-unify'(base+I1),L3,L4)
               ),
               reset(I,L4,LL)
             )
        )
      ).

   'unify-instruction'(C,M,I,II,L,LL) :-
       gref(c(C),I,L,L1),
       (
          M == #f ->
             generate('gp_m_unify',L1,L2);
          generate('gp-unify-raw',L1,L2)
       ),
       II is I - 1,
       reset(II,L2,LL)

")
