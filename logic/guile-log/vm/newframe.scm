(define-module (logic guile-log vm newframe)
  #:use-module (logic guile-log vm utils)
  #:use-module ((logic guile-log iso-prolog)
  		#:renamer
		(lambda (s)
		  (cond
		   ((eq? s 'cc) 'pl-cc)
		   ((eq? s 'reset) 'pl-reset)
		   (else s))))

  #:export (newframe-ps newframe-psc newframe-light newframe
                        newframe-negation newframe-pc
                        store-state store-ps store-p store-s))

(compile-prolog-string
"
   -trace.
   'store-ps'(P,S,I,I,L,LL) :-
      gset(P,p,I,L,L1),
      gset(S,s,I,L1,LL).

   'store-pc'(P,C,I,I,L,LL) :-
      gset(P,p,I,L,L1),
      gset(C,cc,I,L1,LL).

   -trace.
   'store-p'(P,I,I,L,LL) :-
      gset(P,p,I,L,LL).

   'store-s'(S,I,I,L,LL) :-
      gset(S,s,I,L,LL).

    % Probably not used
    
    mkconses2([],_,L,L).
    mkconses2([A|U],I,L,LL) :-
      gset(sp(I),A,I,L,L1),
      II is I + 1,
      'mk-cons'(II,L1,L2),
       mkconses(U,I,L2,LL).

    mkconses([A|U],I,L,LL) :-
      reset(I+2,L,L1),
      gset(sp(I),A,I,L1,L2),
      II is I + 1,
      mkconses2(U,II,L2,L3),
      reset(I+1,L3,LL).
    
    'store-state'([],_,L,L).
    'store-state'(U,I,L,LL) :-
       mkconses(U,I,L,L1),
       II is I + 1,
       gset(sp(II),cstack,III,L1,L2),
       'mk-cons'(L2,L3),
       gset(cstack,sp(I),L3,L4),
       reset(I,L4,LL).       

   'newframe-ps'(P,S,I,I,L,LL) :-
       generate(newframe,L,L1),
       gset(S,s,I,L1,L2),
       gset(P,p,I,L2,LL).

   'newframe-pc'(P,C,I,I,L,LL) :-
       gset(P,p,I,L,L1),
       gset(C,p,I,L1,L2).

   'newframe-psc'(P,S,C,I,I,L,LL) :-
       generate(newframe,L,L1),
       gset(S,s,I,L1,L2),
       gset(P,p,I,L2,L3),
       gset(C,cut,I,L3,L4),
       gset(cut,p,I,L4,LL).

   
   -trace.
   'newframe-light'(P,_,I,I,L,LL) :-
      gset(P,p,I,L,LL).

   -trace.
   newframe(S,P,A,I,I,L,LL) :-
      gset(P,p,I,L,L1),
      generate(newframe,L1,L2),
      gset(S,s,I,L2,LL).

   'newframe-negation'(NP,TP,I,L,LL) :-
      I1 is I + 1,
      gset(sp(I),l(NP),I,L1,L2),
      'store-state'([sp(I),c(TP),scut,p,newframe],II,L1,L2),
      III is II + 1,
      gset(cstack,sp(II),III,L2,L3),
      scmcall('gp-newframe-negation',[s],II,I2,L2,L3),
      gset(s,sp(I2),I2,L3,L4),
      gset(p,sp(I),II,L4,L5),
      gset(cut,sp(I),II,L5,L6),
      reset(I,L6,LL).      
")
