(define-module (language prolog modules user)
  #:use-module (logic guile-log)
  #:use-module (logic guile-log prolog error)
  #:use-module (logic guile-log iso-prolog)
  #:export (exception))

(compile-prolog-string
"
:- dynamic(exception/3).

exception(X,Y,Tag) :- fail.
")


	  
(<define> ($exe x atfail)
 (<or>
  (<and>
   (<match> (#:mode -) (x)
    (#(("error" #(("existence_error" tag val)) _))
      (<var> (action)
	(exception tag val action)
	<cut>
	(<<match>> (#:mode -) (action)
	  ("error" (atfail))
	  ("retry" <cc>)
	  ("fail"  <fail>)
	  (_         (atfail)))))
     (_  (atfail)))
   <cut>)
  (atfail)))
  
(set! user-exception-hook $exe)


