:- module(forward_chaining,
	  [term(compile),
	   term(directive_macro),
	   op(1200,xfx,'=f>'),
	   fire/3,
	   set_trigger/1,
	   database/1]).

/*
This is a small library of forward chaining mechanism. The forward chaining
is using a database that is a dynamic variable marked through
:- dynamic(db/1).
:- database(db).

We define the chaning system, by defining a trigger function
:- dynamic(t/1).
:- set_trigger(t).

After this directive all =f> rules will attach that logic to t/1

With this system one can use the =f> operator to define rules

:- dynamic(f/2).
f(X,Y),f(Y,Z) =f> f(X,Z).

And we can fire rules by,
?- fire(t,f(0,1)).
?- fire(t,f(1,2)).
?- fire(t,f(2,3)).

And then get the database of all chains,
?- f(X,Y).
0,1
1,2
0,2
2,3
1,3
0,1

*/

:- use_module(library(apply)).

:- dynamic(chain/2).

trigger.

ret(X) :- chain(X,_) -> retract(chain(X,_)) ; true.

set_trigger(X) :-
   ret(trigger),
   asserta(chain(trigger,X)).

directive_macro((:- set_trigger(X)),[]) :-
    set_trigger(trigger).

directive_macro((?- set_trigger(X)),[?- true]) :-
    set_trigger(X).



fand(Y,[X,W],[X,(W,Y)]).

compile_head((X,Y),Z) :- !,
    compile_head(X,ZX), maplist(fand(Y),ZX,ZZX),
    compile_head(Y,ZY), maplist(fand(X),ZY,ZZY),
    append(ZZX,ZZY,Z).

compile_head((X;Y),Z) :- !,
    compile_head(X,ZX),
    compile_head(Y,ZY),
    append(ZX,ZY,Z).

compile_head({X},[[true,true]]) :- !.
compile_head(X  ,[[X   ,true]]).

compile_code((X,Y),(ZX,ZY)) :- !,
    compile_code(X,ZX),
    compile_code(Y,ZY).

compile_code((X;Y),(ZX;ZY)) :- !,
    compile_code(X,ZX),
    compile_code(Y,ZY).

compile_code((X->Y),(ZX->ZY)) :- !,
    compile_code(X,ZX),
    compile_code(Y,ZY).

compile_code({X},X) :- !.

compile_code(X,Z) :- chain(trigger, T), Z = fire(T,X).


compile_b((X,Y),(ZX,ZY)) :- !,
    compile_b(X,ZX),
    compile_b(Y,ZY).

compile_b((X,Y),(ZX;ZY)) :- !,
    compile_b(X,ZX),
    compile_b(Y,ZY).

compile_b({X},X) :- !.
compile_b(X  ,X).


for_each(X) :- (X,fail);true.

new(H) :- H -> fail ; assertz(H).

g(_,[true,_],true).
g(Code,[H,B],Z) :-
    chain(trigger,T),
    compile_b(B,BB),
    Z = (T(H) :- ((BB,Code,fail) ; true)).

h([true|X],L              ) :- h(X,L).
h([X   |L],(assertz(X),LL)) :- h(L,LL).
h([],true).

hh([true|X],L      ) :- hh(X,L).
hh([X   |L],[X|LL] ) :- hh(L,LL).
hh([],[]).

fire(Trig,H) :- new(H),((Trig(H), fail) ; true).

compile((Head =f> Code), Z) :-
    compile_head(Head,ZZ),
    compile_code(Code,Code2),
    maplist(g(Code2),ZZ,ZZZ),
    hh(ZZZ,Z).


compile(?- (Head =f> Code), [?- Z]) :-
    compile_head(Head,ZZ),
    compile_code(Code,Code2),
    maplist(g(Code2),ZZ,ZZZ),
    h(ZZZ,Z).
