(define-module (language prolog modules library vhash)
  #:use-module (logic guile-log)
  #:use-module (srfi srfi-9)
  #:use-module (logic guile-log prolog goal-transformers)
  #:use-module (logic guile-log vlist)
  #:export (empty_assoc put_assoc put_assoc_x get_assoc assoc_to_list
			clear_assoc mk_node))

(<define> (mk_node nd . l)
   (<=> nd ,(map (lambda (x) (<lookup> x)) l)))
	  
(define-record-type <h>
  (mk val)
  h?
  (val      r s!))

(<define> (clear_assoc X)
   (<let> ((H (<lookup> X)))
      (<code> (s! H vlist-null))))     

(<define> (empty_assoc X)
   (<let> ((vh (mk vlist-null)))
     (<=> X vh)))

(<define> (put_assoc HEntry H0 Node H)
  (<let> ((HEntry (<scm> HEntry))
	  (H0     (r (<lookup> H0))))
     (<=> H ,(mk (vhash-cons HEntry (<lookup> Node) H0)))))

(<define> (put_assoc_x HEntry H0 Node)
  (<let> ((HEntry (<scm> HEntry))
	  (H0     (<lookup> H0)))
     (<code> (s!  H0 (vhash-set! HEntry (<lookup> Node) (r H0))))))

(<define> (get_assoc HEntry H0 Node)
  (<let*> ((HEntry (<scm> HEntry))
	   (H0     (r (<lookup> H0)))
	   (V      (vhash-ref H0 HEntry #f)))
    (if V
	(<and>
	 (<=> Node V))
	(<and>
	 <fail>))))
    
       

(<define> (assoc_to_list H L)
  (<=> L ,(map (lambda (k) (vector (list gop2- (car k) (cdr k))))
	       (vhash->assoc (r (<lookup> H))))))
