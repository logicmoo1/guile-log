(define-module (language prolog spec)
  #:use-module (system base compile)
  #:use-module (system base language)
  #:use-module (logic guile-log guile-prolog readline)
  #:use-module (logic guile-log prolog modules)
  #:use-module (language scheme compile-tree-il)
  #:use-module (logic guile-log guile-prolog interpreter)
  #:export (prolog))

;;;
;;; Language definition
;;;

(define (get-string boot port)
  (pre-compile-prolog-file-no-check
   (port-filename port)
   #:boot? boot))

(define (int)
  (catch #t
    (lambda ()
      (if (fluid-ref (@@ (system base compile) %in-compile))
          #f
          #t))
    (lambda x #f)))

(define (in)
  (catch #t
    (lambda ()
      (fluid-set! (@@ (system base compile) %in-compile) #t))
    (lambda x #f)))

(define mapper (make-weak-key-hash-table))

(define (prolog-reader-wrap boot)
  (lambda (port env)
    (if (int)
        (read-prolog port env)
        (let lp ((port2 (hash-ref mapper port)))
          (if port2
              (read port2)
              (let ((port2
                     (open-input-string (get-string boot port))))
                (use-modules (language python guilemod))
                (in)
                (hash-set! mapper port port2)
                (lp port2)))))))
         


(define-language prolog
  #:title	"Prolog"
  #:reader
  (prolog-reader-wrap #f)
  #:compilers   `((tree-il . ,compile-tree-il))
  #:evaluator	(lambda (x module) (primitive-eval x))
  #:printer write
  #:make-default-environment
  (lambda ()
    (let ((m (make-fresh-user-module)))                    
      ;; Provide a separate `current-reader' fluid so that
      ;; compile-time changes to `current-reader' are
      ;; limited to the current compilation unit.
      (module-define! m 'current-reader (make-fluid))
      
      ;; Default to `simple-format', as is the case until
      ;; (ice-9 format) is loaded.  This allows
      ;; compile-time warnings to be emitted when using
      ;; unsupported options.
      (module-set! m 'format simple-format)
      
      m)))
