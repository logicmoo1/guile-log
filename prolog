#!/usr/local/bin/guile \
--no-auto-compile -e main -s
!#

(use-modules (ice-9 match)
             (language prolog spec)
             (system repl common)
             (system repl repl)
             (ice-9 readline))

(eval-when (eval load compile)
  (activate-readline))

(define* (main #:optional (args (command-line)))
  (match args
    ((_ file)
     (let ((compiled (string-append file ".go")))
       (compile-file file
                     #:from prolog
                     #:output-file compiled)
         (load-compiled compiled)))
    ((_)
     (repl-default-option-set! 'prompt "?- ")
     (set! (@@ (system repl common) repl-welcome)
           (const (display "\
GuileLog Prolog on Guile, version 0.1.0
(Hit Ctrl-D to exit.)
")))
	(setlocale LC_ALL "")
	(use-modules (system base language))
	(use-modules (system repl repl))
	(use-modules (system repl common))
	(use-modules (ice-9 readline))
	(activate-readline)

	(load  (string-append (getenv "HOME") "/.guile"))

	(let ((f0 (string-append (getenv "HOME") "/guile-prolog-scratch"))
	      (f1 (string-append (getenv "HOME") "/guile-prolog-scratch/lan	guage"))
	      (f2 (string-append (getenv "HOME")
			  "/guile-prolog-scratch/language/prolog"))
              (f3 (string-append (getenv "HOME")
			  "/guile-prolog-scratch/language/prolog/modules")))

          (if (not (file-exists? f0))
              (mkdir f0))
          (if (not (file-exists? f1))
              (mkdir f1))
          (if (not (file-exists? f2))
              (mkdir f2))
          (if (not (file-exists? f3))
              (mkdir f3))

          (set! %load-path (cons f0 %load-path))
          (let ()
            (define (f)
              (set-current-module
                (resolve-module '(prolog-user)))
              (let ((lang (lookup-language 'prolog)))
                (let lp ()
                  (catch #t
	            (lambda ()
	              (current-language lang)
	              (let ((status (start-repl lang)))
	                (if (null? status)
		            (run-hook exit-hook)
		            (lp))))
   	            (lambda x (lp))))))
              (f))
          (let ((do (lambda (r/w)
	        (if (memq 'history-file ((@@ (ice-9 readline)
                                           readline-options-interface)))
		     (r/w
                       (or (getenv (@@ (ice-9 readline) history-variable))
		           (@@ (ice-9 readline) history-file)))))))
             (do (@ (ice-9 readline) write-history)))))

    (_ (format (current-error-port)
               "usage: ~a file.pl~%" this))))
