#!/bin/bash

cd $1

A=$(ps -a | grep octave | cut -d" " -f3)
kill -9 $A

octave --eval "runguile()" &
