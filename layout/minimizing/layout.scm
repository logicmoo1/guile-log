(define-module (layout minimizing layout)
  #:use-module (oop goops)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-9 gnu)
  #:use-module (language python module copy)
  #:use-module (language python persist)
  #:export (*draw-rect* *draw-ellipse* *optimize* *draw-ret* *geos*
			scale
			weaker stronger hulk
			left right up down
			rect quad circ ellipse width height
			vstack vstack= hstack hstack=
			size
			clone nclone
			htags htags= vtags vtags= vboxes hboxes tag
			m-pos left-m-pos right-m-pos up-m-pos down-m-pos
			link hardlink prop-link hasr-prop-link less greater
			layout draw

                        left-part right-part up-part down-part

                        combine
			))

#|
LGPL v.2 or later, 

Copyright: Stefan Israelsson Tampe 2019

This is a layout engine through a quadratic programming algorithm. This enables 
intelligent scaling  e.g. when the overall constraints get's smaller we will 
compress less important sizes befor more important sizes. Also this system will 
solve over constrained systems in an intelligent way.

You will need python-on-guile installed and also supply drawing routines and
an SQL optimizer


INTERFACE:
*draw-ret*     : (lambda (x l) ...) handles the list of all retruns l for 
                 individual draw routins and x is the object beeing drawn.
                                  draw objects.
*draw-rect*    : (lambda (o) ...) the function to draw a rectangle from a box.

*draw-ellipse* : (lambda (o) ...) the function to draw a elipse from a box.

*optimize*     : (lambda (n ar av leq) ...) will solve a quadratic problem 
                 with the square
                 matrix defined by nxn array ar, and liner part defined by 
                 vector av. The inequalities
                 is define in the list leq = ((lhs . rhs) ...) where 
                 lhs <= rhs. It outputs a list
                 ((i . val) ...) wher i is variable i in 0,1,...,n-1 and val 
                 is the optimized value.

scale          : a variable that can direct the scale of the output

weaker         : (weaker   x) will execure x restulting in weaker equal 
                 constraints

stronger       : (stronger x) will execure x restulting in stronger equal 
                 constraints

hulk           : (hulk     x) will execure x restulting in very stringer equal
                 constraints

left           : the leftmost position of the object
right          : the rightmost posiiton of the object
up             : the upmost position of the object
down           : the downmost position of the object

rect           : (rect w h #:optional b vb) : makes a rectangle object with 
                 width w and height h optionally we can define a horrizontal 
                 alignment value of b and vertical as vb if these values is 
                 not supplied the result will be the midpoints

ellipse        : (circ w h #:optinal b vb) same as rect but will be drawn as 
                 an ellipse in stead

quad           : (quad r #:optional b vb) same as rect but the same height as 
                 width

circ           : (circ r #:optional b vb) same as ellipse but same width and 
                 height

vstack         : (vstack o ...) stack o ... vertically
vstack=        : (vstack= o ...) stack o ... vertically constraining the 
                 heghts to be the same
hstack         : (hstack o ...) stack o ... horrizontally
hstack=        : (hstack= o ...) stack o ... horrizontally constraining the 
                 widths to be the same

size           : (size o w h) constrain o to the rect (0 .. w) (0 .. h)

clone          : (clone  o) make an independent copy of o
nclone         : (nclone o n) make a list of n induvidualy independent coopies 
                 of o

htags          : (htags n) make n horrizontal tags that can be evenly 
                 distributed if total size is constrained

htags=         : the same as htags but the maximal and minimal positions will 
                 get a tag

vtags          : (vtags n) make n vertical tags that can be evenly distributed 
                 if total size is constrained

vtags=         : the same as vtags but the maximal and minimal positions will 
                 get a tag

vboxes         : (vboxes w h d), as vtags but in stead of a tag there will be 
                 a box sized w h and space between them is d.

hboxes         : same as vboxes but horrizontal

tag            : (tag o n) will get the n+1:th tag object or box object in 
                 the combined object


m-pos          : (m-pos o):     will return the mean (x . y) position
left-m-pos     : (left-m-pos o) will return the mean y-dir leftmost (x . y) 
                 position
right-m-pos    : as left-m-pos bit rightmost
up-m-pos       : as left-m-pos but upmost
down-m-pos     : as left-m-pos but downmost

layout         : (layout obj) : will optimize a layout for obj
draw           : (draw obj) : will draw an object that is layed out

link           : (link v1 v2 diff) : add constraint v1 = v2 + diff + 
                 minimized  val 
               : (link v1 v2)      : add constraint v1 = v2        + 
                 minnimized val
                 note that the degree that the constraint will be minimized is 
                 controlled by weaker stronger and hulk.

|#


(define *draw-ret*     (lambda (x l) #f))
(define *draw-rect*    (lambda o     #f))
(define *draw-ellipse* (lambda o     (values)))
(define *optimize*     (lambda x     '()))
  
(define scale    1)
(define strength 1)
(define *defining* #f)
(define-syntax-rule (defining x ...)
  (begin
    (set! *defining* #t)
    (let ((ret (begin x ...)))
      (set! *defining* #f)
      ret)))

(define (weaker x)
  (begin
    (set! strength 0.1)
    (let ((r x))
      (set! strength 1)
      r)))

(define (stronger x)
  (begin
    (set! strength 10)
    (let ((r x))
      (set! strength 1)
      r)))

(define (hulk x)
  (begin
    (set! strength 100)
    (let ((r x))
      (set! strength 1)
      r)))


(define-class <Value> () constraints x x0 xr draw)
(define-class <Item> () left right up down base y-base draw x y)
(define-class <VerticalStack> (<Item>) list)
(define-class <HorizontalStack> (<Item>) list)
(define-class <Combine> () l draw)

(name-object <Value>)
(name-object <Item>)
(name-object <VerticalStack>)
(name-object <HorizontalStack>)
(name-object <Combine>)

(define (combine . l)
  (let ((o (make <Combine>)))
    (slot-set! o 'l l)
    (slot-set! o 'draw
               (lambda (o)
                 (let lp ((l (slot-ref o 'l)))
                   (if (pair? l)
                       (cons (draw (car l)) (lp (cdr l)))
                       '()))))
    o))

    
(define-method (write (o <Value>) port)
  (format port "~a(~a)" (slot-ref o 'xr) (slot-ref o 'x)))

(define-method (write (o <Item>) port)
  (format port "[~a -- ~a, ~a | ~a, ~a m ~a]"
	  (slot-ref o 'left)
	  (slot-ref o 'right)
	  (slot-ref o 'down)
	  (slot-ref o 'up)
	  (slot-ref o 'y-base)
	  (slot-ref o 'base)))

(define-method (write (o <HorizontalStack>) port)
  (format port "H:~a" (slot-ref o 'list)))

(define-method (write (o <VerticalStack>) port)
  (format port "H:~a" (slot-ref o 'list)))

(define (left   x) (slot-ref x 'left))
(define (right  x) (slot-ref x 'right))
(define (up     x) (slot-ref x 'up))
(define (down   x) (slot-ref x 'down))
(define (base   x) (slot-ref x 'base))
(define (y-base x) (slot-ref x 'y-base))

(define (make-value val)
  (let ((o (make <Value>)))
    (slot-set! o 'draw draw-nothing)
    (slot-set! o 'x  val)
    (slot-set! o 'xr val)
    (slot-set! o 'x0 0)
    (slot-set! o 'constraints '())
    o))

(define (add o x) (slot-set! o 'constraints (cons (cons x strength)
                                                  (slot-ref o 'constraints))))

(define-method (link (o1 <Value>) (o2 <Value>))
  (link '= o1 o2 0))
(define-method (link (o1 <Value>) (o2 <Value>) num)
  (link '= o1 o2 num))
(define-method (hardlink (o1 <Value>) (o2 <Value>))
  (link '== o1 o2 0))
(define-method (hardlink (o1 <Value>) (o2 <Value>) num)
  (link '== o1 o2 num))
(define-method (link kind (o1 <Value>) (o2 <Value>))
  (link kind o1 o2 0))
(define-method (link kind (o1 <Value>) (o2 <Value>))
  (link kind o1 o2 0))
(define-method (link kind (o1 <Value>) (o2 <Value>) diff)
  (if *defining*
      (begin
        (slot-set! o1 'x  (+ (slot-ref o2 'x ) diff))
        (slot-set! o1 'xr (+ (slot-ref o2 'xr) diff))))
  (add o1 (list kind (list o1) (list o2 diff))))

(define-method (link (o1 <Value>) num)
  (link '= o1 num))
(define-method (hardlink (o1 <Value>) num)
  (link '== o1 num))

(define-method (link kind (o1 <Value>) diff)
  (if *defining*
      (begin
        (slot-set! o1 'x  diff))
        (slot-set! o1 'xr diff))
  (add o1 (list kind o1 diff)))

(define-method (less (o1 <Value>) num)
  (link '<= o1 num))
(define-method (greater (o1 <Value>) num)
  (link '>= o1 num))

(define-method (link kind (o1 <Value>) diff)
  (if *defining*
      (begin
        (slot-set! o1 'x  diff))
        (slot-set! o1 'xr diff))
  (add o1 (list kind o1 diff)))

(define-method (prop-link y-base o1 o2 yb)
  (prop-link '= y-base o1 o2 yb))

(define-method (hard-prop-link y-base o1 o2 yb)
  (prop-link '== y-base o1 o2 yb))

(define-method (prop-link kind y-base o1 o2 yb)
  (if *defining*
      (begin
        (slot-set! y-base 'x (+ (* (slot-ref o1 'x) (- 1 yb))
                                (* (slot-ref o2 'x) yb)))
        (slot-set! y-base 'xr (+ (* (slot-ref o1 'x) (- 1 yb))
                                 (* (slot-ref o2 'xr) yb)))))
  (add y-base (list kind y-base (list (cons o1 (- 1 yb)) (cons o2 yb)))))
  
(define (link-x . l) (apply link 'x l))
(define (link-y . l) (apply link 'y l))


(define (list->set . l)
  (let ((ret (make-hash-table)))
    (let lp ((l l))
      (if (pair? l)
	  (begin
	    (hashq-set! ret (car l) #t)
	    (lp (cdr l)))
	  ret))))

(define (union x y)
  (hash-for-each
   (lambda (k v)
     (hashq-set! x k v))
   y)
  x)

(define (draw-rect o)
  (*draw-rect* o))

(define (draw-nothing o) #f)

(name-object draw-nothing)
(name-object draw-rect)

(define-method (vals x0 y0 ret o) ret)
(define-method (vals x0 y0 ret (o <Value>))
  (if (not (hashq-ref ret o))
      (let lp ((l (slot-ref o 'constraints)))
        (hashq-set! ret o #t)
	(match l
	  ((x . l)
	   (lp x)
	   (lp l))
	  (x
	   (if (instance? x)
	       (vals 0 0 ret x))))))
  ret)
	   	
(define (dx o x0)
  (slot-set! o 'xr  (+ x0 (slot-ref o 'x)))
  (slot-set! o 'x0  (+ x0 (slot-ref o 'x))))

(define-method (vals x0 y0 ret (o <Item>))  
  (if (not (hashq-ref ret o))
      (begin
        (hashq-set! ret o #t)
        (if *defining*
            (let ((x0 (+ (slot-ref o 'x) x0))
                  (y0 (+ (slot-ref o 'y) y0)))
              (dx (slot-ref o 'left  ) x0)
              (dx (slot-ref o 'right ) x0)
              (dx (slot-ref o 'y-base) x0)
              (dx (slot-ref o 'up    ) y0)
              (dx (slot-ref o 'down  ) y0)
              (dx (slot-ref o 'base  ) y0)))

        (for-each
         (lambda (x)
           (vals x0 y0 ret x))
         (list	
          (slot-ref o 'left)
          (slot-ref o 'right)
          (slot-ref o 'base)
          (slot-ref o 'right)
          (slot-ref o 'up)
          (slot-ref o 'down)
          (slot-ref o 'y-base)))))
  ret)

(define-method (vals x0 y0 ret (o <VerticalStack>))
  (if (not (hashq-ref ret o))
      (let ((ret (next-method)))
        (hashq-set! ret o #t)
	(let lp ((ret ret) (l (slot-ref o 'list)))
	  (if (pair? l)
	      (lp (union ret (vals x0 y0 ret (car l))) (cdr l))
	      ret)))
      ret))

(define-method (vals x0 y0 ret (o <HorizontalStack>))
  (if (not (hashq-ref ret o))
      (let ((ret (next-method)))
        (hashq-set! ret o #t)
	(let lp ((ret ret) (l (slot-ref o 'list)))
	  (if (pair? l)
	      (lp (union ret (vals x0 y0 ret (car l))) (cdr l))
	      ret)))
      ret))

(define-method (vals x0 y0 ret (o <Combine>))
  (let lp ((ret ret) (l (slot-ref o 'l)))
    (if (pair? l)
        (lp (union ret (vals x0 y0 ret (car l))) (cdr l))
        ret)))

(define (item left right up down base y-base)
  (let ((o (make <Item>)))
    (slot-set! o 'x       0)
    (slot-set! o 'y       0)
    (slot-set! o 'left    left)
    (slot-set! o 'right   right)
    (slot-set! o 'up      up)
    (slot-set! o 'down    down)
    (slot-set! o 'base    base)
    (slot-set! o 'y-base  y-base)
    (slot-set! o 'draw    draw-rect)
    o))

(define rect
  (case-lambda
    ((width height)
     (rect width height (/ 1 2.0) (/ 1 2.0)))
    
    ((width height yb b)
     (let ((left   (make-value 0))
	   (right  (make-value 0))
	   (up     (make-value 0))
	   (down   (make-value 0))
	   (base   (make-value 0))
	   (y-base (make-value 0)))
       (defining
	 (link   right  left   width)
	 (link   up     down   height)
	 (prop-link y-base left right yb)
	 (prop-link base   down up    b )
	 (item left right up down base y-base))))))

(define (c-rect w h) (rect w h (/ 1 2.0) (/ 1 2.0)))
(define (w-rect w h) (rect w h 1         (/ 1 2.0)))

(define quad
  (case-lambda
    ((width)
     (quad width (/ width 2.0)))
    
    ((width b)
     (let* ((o (rect width width b b))
	    (l (left  o))
	    (r (right o))
	    (u (up    o))
	    (d (down  o)))
       (add l (list '==
		    (list (cons l -1) (cons r 1))
		    (list (cons d -1) (cons u 1))
		    #f))))))
   

(define width
  (case-lambda
    ((w)
     (width w (/ 1 2.0)))
    ((w yb)
     (let* ((left   (make-value 0))
	    (right  (make-value 0))
	    (up     (make-value 0))
	    (down   up)
	    (base   up)
	    (y-base (make-value 0)))
       (defining
	 (link right left w)
         (prop-link y-base left right yb))
       (greater right left)
       (let ((o (item left right up down base y-base)))
	 (slot-set! o 'draw draw-nothing)
         o)))))

(define height
  (case-lambda
    ((h)
     (height h (/ 1 2.0)))
    ((h b)
     (let* ((left   (make-value 0))
	    (right  right)
	    (up     (make-value 0))
	    (down   (make-value 0))
	    (base   (make-value 0))
	    (y-base right))
       (defining
	 (link  up   down     height)
	 (prop-link  base down up b))
       (let ((o (item left right up down base y-base)))
	 (slot-set! o 'draw draw-nothing)
         o)))))

(define (link-lr i1 i2)
  (let ((r1 (slot-ref i1 'right))
	(b1 (slot-ref i1 'base))
	(l2 (slot-ref i2 'left))
	(b2 (slot-ref i2 'base)))
    (hardlink r1 l2)
    (hardlink b1 b2)))

(define (link-du i1 i2)
  (let ((u1 (slot-ref i1 'up))
	(b1 (slot-ref i1 'y-base))
	(d2 (slot-ref i2 'down))
	(b2 (slot-ref i2 'y-base)))
    (hardlink u1 d2)
    (hardlink b1 b2)))
    

(define (hstack . l)
  (define (leftmost l)  (car l))
  (define (rightmost l) (car (reverse l)))
  
  (let ((o     (make <HorizontalStack>))
	(left  (leftmost l))
	(right (rightmost l)))
    (slot-set! o 'draw    draw-nothing)
    (slot-set! o 'list    l)
    (slot-set! o 'left    (slot-ref left  'left))
    (slot-set! o 'right   (slot-ref right 'right))
    (slot-set! o 'base    (slot-ref left  'base))
    (slot-set! o 'y-base  (slot-ref left  'y-base))
    (slot-set! o 'x 0)
    (slot-set! o 'y 0)

    (slot-set! o 'up    (apply maxit0 (map (lambda (x) (slot-ref x 'up))   l)))
    (slot-set! o 'down  (apply minit0 (map (lambda (x) (slot-ref x 'down)) l)))
    (defining
      (link (up o) (down o)
            (- (apply max (map (lambda (x) (slot-ref (up   x) 'x)) l))
               (apply min (map (lambda (x) (slot-ref (down x) 'x)) l)))))
    (let lp ((left left) (l (cdr l)) (x 0)
	     (b (slot-ref (slot-ref left 'base) 'x)))
      (if (pair? l)
	  (let* ((o (car l))
		 (d (slot-ref (down o) 'x))
		 (a (slot-ref (slot-ref o 'base) 'x))
		 (y (- b (- a d))))
	    (link-lr left o)
	    (slot-set! o 'y y)
	    (let ((x (+ x (slot-ref (slot-ref left 'right) 'x))))
	      (slot-set! o 'x x)
	      (lp o (cdr l) x b)))))
    o))

(define (hstack= . l)
  (define (leftmost l)  (car l))
  (define (rightmost l) (car (reverse l)))
  
  (let ((o     (make <HorizontalStack>))
	(left  (leftmost l))
	(right (rightmost l)))
    (slot-set! o 'draw draw-nothing)
    (slot-set! o 'list l)
    (slot-set! o 'left    (slot-ref left  'left))
    (slot-set! o 'right   (slot-ref right 'right))
    (slot-set! o 'base    (slot-ref left  'base))
    (slot-set! o 'y-base  (slot-ref left  'y-base))
    (slot-set! o 'up      (slot-ref left  'up))
    (slot-set! o 'down    (slot-ref left  'down))
    (slot-set! o 'x 0)
    (slot-set! o 'y 0)

    (let lp ((left left) (l (cdr l)) (x 0)
	     (b (slot-ref (slot-ref left 'base) 'x)))
      (if (pair? l)
	  (let* ((o (car l))
		 (d (slot-ref (down o) 'x))
		 (a (slot-ref (slot-ref o 'base) 'x))
		 (y (- b (- a d))))
	    (link (up   left) (up   right))
	    (link (down left) (down right))
	    (link-lr left o)
	    (slot-set! o 'y y)
	    (let ((x (+ x (slot-ref (slot-ref left 'right) 'x))))
	      (slot-set! o 'x x)
	      (lp o (cdr l) x b)))))
    o))

(define (maxit v x)
  (slot-set! v 'constraints
             (cons (cons (list '>= v x) #f)
                   (slot-ref v 'constraints))))

(define (minit v x)
  (slot-set! v 'constraints (cons (cons (list '<= v x) #f) (slot-ref v 'constraints))))

(define (maxit0 . l) (apply red maxit l))
(define (minit0 . l) (apply red minit l))

(define (red f . l)
  (let lp ((v (make-value 0)) (l l))
    (if (pair? l)
	(begin
	  (f v (car l))
	  (lp v (cdr l)))
	v)))

	

(define (vstack . l)
  (define (downmost l)  (car l))
  (define (upmost l) (car (reverse l)))
  
  (let ((o     (make <VerticalStack>))
	(down  (downmost l))
	(up    (upmost l)))
    (slot-set! o 'draw draw-nothing)
    (slot-set! o 'list l)
    (slot-set! o 'up     (slot-ref up   'up))
    (slot-set! o 'down   (slot-ref down 'down))
    (slot-set! o 'y-base (slot-ref down  'y-base))
    (slot-set! o 'base   (slot-ref down  'base))
    (slot-set! o 'right  (apply maxit0
                                (map (lambda (x) (slot-ref x 'right)) l)))
    (slot-set! o 'left   (apply minit0
                                (map (lambda (x) (slot-ref x 'left )) l)))

    (slot-set! o 'x 0)
    (slot-set! o 'y 0)

    (defining
      (link (right o) (left o) 
            (- (apply max (map (lambda (x) (slot-ref (right x) 'x)) l))
               (apply min (map (lambda (x) (slot-ref (left  x) 'x)) l)))))

    (let lp ((down down) (l (cdr l)) (y 0)
	     (b (slot-ref (slot-ref down 'y-base) 'x)))
      (if (pair? l)
	  (let* ((o (car l))
		 (d (slot-ref (left o) 'x))
		 (a (slot-ref (slot-ref o 'y-base) 'x))
		 (x (- b (- a d))))
	    (link-du down o)
	    (slot-set! o 'x x)
	    (let ((y (+ y (slot-ref (slot-ref down 'up) 'x))))
	      (slot-set! o 'y y)
	      (lp o (cdr l) y b)))))
    
    o))

(define (vstack= . l)
  (define (downmost l)  (car l))
  (define (upmost l) (car (reverse l)))
  
  (let ((o     (make <VerticalStack>))
	(down  (downmost l))
	(up    (upmost l)))
    (slot-set! o 'draw draw-nothing)
    (slot-set! o 'list l)
    (slot-set! o 'up     (slot-ref left  'up))
    (slot-set! o 'down   (slot-ref right 'down))
    (slot-set! o 'y-base (slot-ref down  'y-base))
    (slot-set! o 'base   (slot-ref down  'base))
    (slot-set! o 'right  (right down))
    (slot-set! o 'left   (left down))
    (slot-set! o 'x 0)
    (slot-set! o 'y 0)

    (let lp ((down down) (l (cdr l)) (y 0)
	     (b (slot-ref (slot-ref left 'base) 'x)))
      (if (pair? l)
	  (let* ((o (car l))
		 (d (slot-ref (left o) 'x))
		 (a (slot-ref (slot-ref o 'y-base) 'x))
		 (x (- b (- a d))))
	    (link-du down o)
	    (link (left  down) (left  (car l)))
	    (link (right down) (right (car l)))

	    (slot-set! o 'x x)
	    (let ((y (+ y (slot-ref (slot-ref down 'up) 'x))))
	      (slot-set! o 'y y)
	      (lp o (cdr l) y b)))))
    o))

(define (clone  x) (deepcopy x))
(define (nclone x n) (map (lambda (u) (deepcopy x)) (iota n))) 

(define (htags n)
  (let ((l1 (nclone (stronger (width 0)) n))
	(l2 (nclone (append (weaker (width 10)) (- n 1)) (list (width 10)))))
    (apply hstack (let lp ((l (list (width 10))) (l1 l1) (l2 l2))
		    (if (pair? l1)
			(lp (cons* (car l2) (car l1) l) (cdr l1) (cdr l2))
			(reverse l))))))

(define (htags= n)
  (let ((l1 (nclone (stronger (width 0)) n))
	(l2 (nclone (append (weaker (width 10)) (- n 1)) (list (stronger
                                                                (width 0))))))
    (apply hstack (let lp ((l (stronger (width 0))) (l1 l1) (l2 l2))
		    (if (pair? l1)
			(lp (cons* (car l2) (car l1) l) (cdr l1) (cdr l2))
			(reverse l))))))

(define (vtags n)
  (let ((l1 (nclone (stronger (height 0)) n))
	(l2 (nclone (append (weaker (height 10)) (- n 1)) (list (height 10)))))
    (apply vstack (let lp ((l (list (height 10))) (l1 l1) (l2 l2))
		    (if (pair? l1)
			(lp (cons* (car l2) (car l1) l) (cdr l1) (cdr l2))
			(reverse l))))))

(define (vtags= n)
  (let ((l1 (nclone (stronger (height 0)) n))
	(l2 (nclone (append (weaker (height 10)) (- n 1)) (list (stronger
                                                                 (height 0))))))
    (apply vstack (let lp ((l (list (stronger (height 0)))) (l1 l1) (l2 l2))
		    (if (pair? l1)
			(lp (cons* (car l2) (car l1) l) (cdr l1) (cdr l2))
			(reverse l))))))



(define (tag x n) (list-ref (slot-ref x 'list) (- (* 2 n) 1)))

(define (e x) (exact->inexact (floor (slot-ref x 'xr))))

(define (m-pos x)
  (cons (/ (+ (e (left x)) (e (right x))) 2.0)
        (/ (+ (e (up   x)) (e (down  x))) 2.0)))

(define (left-m-pos x)
  (cons (e (left x)) (/ (+ (e (up x)) (e (down x))) 2.0)))

(define (right-m-pos x)
  (cons (e (right x)) (/ (+ (e (up x)) (e (down x))) 2.0)))

(define (up-m-pos x)
  (cons (/ (+ (e (left x)) (e (right x))) 2.0) (e (up x))))

(define (down-m-pos x)
  (cons (/ (+ (e (left x)) (e (right x))) 2.0) (e (down x))))

(define (vboxes n w h s)
  (let ((l1 (nclone (rect w h) n))
	(l2 (append (nclone (width s) (- n 1)) (list (stronger
                                                           (width 0))))))
    (apply vstack (let lp ((l (list (stronger (width 0)))) (l1 l1) (l2 l2))
		    (if (pair? l1)
			(lp (cons* (car l2) (car l1) l) (cdr l1) (cdr l2))
			(reverse l))))))

(define (hboxes n w h s)
  (let ((l1 (nclone (rect w h) n))
	(l2 (append (nclone (height s) (- n 1)) (list (stronger
                                                            (height 0))))))
    (apply vstack (let lp ((l (list (stronger (height 0))))
                           (l1 l1) (l2 l2))
		    (if (pair? l1)
			(lp (cons* (car l2) (car l1) l) (cdr l1) (cdr l2))
			(reverse l))))))

(define *geos* (lambda () '()))
(define (draw x)
  (let ((ret (make-hash-table))
        (l   (*geos*)))
    (vals 0 0 ret x)
    (hash-for-each
     (lambda (k v)
       (let ((r ((slot-ref k 'draw) k)))
         (if r (set! l (cons r l)))))
     ret)
    (*draw-ret* x l)))

(define (get-vals x)
  (let ((h (make-hash-table)))
    (vals 0 0 h x)
    (let ((ret '()))
      (hash-for-each
       (lambda (k v)
	 (if (is-a? k <Value>)
	     (set! ret (cons k ret))))
       h)
      ret)))

(define (make-eq ar1 ar2 h x y s)
  (define (combine x y sgn)    
    (let ((nx (length x))
          (ny (length y)))
      (let lp1 ((i 0))
	(if (< i nx)
	    (let lp1r ((xx (list-ref x i)))
	      (match xx
		((ox . xx)
		 (let ((ix (hashq-ref h ox)))
		   (let lp2 ((j 0))
		     (if (< j ny)
			 (begin
			   (let lp2r ((xxx (list-ref y j)))
			     (match xxx
			       ((oy . xy)
				(let ((iy (hashq-ref h oy)))
				  (array-set! ar1
					     (+
					      (* 2 (if s s 0.1) sgn xx xy)
					      (array-ref ar1 ix iy))
                                             ix iy)))
			       (xy
				(if (instance? xy)
				    (lp2r (cons xy 1))
				    (vector-set! ar2
                                                 ix (+ (* (if s s 0.1) sgn xx xy)
                                                       (vector-ref ar2 ix)))))))
			   (lp2 (+ j 1)))
			 (lp1 (+ i 1))))))
		(xx
		 (if (instance? xx)
		     (lp1r (cons xx 1))
		     (let lp2 ((j 0))
		       (if (< j ny)
			   (begin
			     (let lp2r ((xxx (list-ref y j)))
			       (match xxx
				 ((oy . xy)
				  (let ((iy (hashq-ref h oy)))
				    (vector-set! ar2
						iy
						(+
						 (* (if s s 0.1) sgn xx xy)
						 (vector-ref ar2 iy)))))
				 (xy
				  (if (instance? xy)
				      (lp2r (cons xy 1))
				      (values)))))
			     (lp2 (+ j 1)))
			   (lp1 (+ i 1))))))))))))
  (let ((nx (if (pair? x) (length x) (begin (set! x (list x)) 1)))
        (ny (if (pair? y) (length y) (begin (set! y (list y)) 1))))
    (combine x x  1)
    (combine y y  1)
    (combine x y -1)
    (combine y x -1)))
		  

(define (make-leq h x y)
  (if (not (pair? x)) (set! x (list x)))
  (if (not (pair? y)) (set! y (list y)))
  
  (let ((hh (make-hash-table))
	(c 0))
    (define (add x sgn)
      (for-each
       (lambda (x)
	 (let lp ((x x))
	   (match x
	     ((o . x)
	      (hashq-set! hh o (+ (* sgn x)
                                  (hashq-ref hh o 0))))
	     (x
	      (if (instance? x)
		  (lp (cons x 1))
		  (set! c (+ c (* -1 sgn x))))))))
       x))

    (add x   1)
    (add y  -1)

    (let ((l '()))
      (hash-for-each
       (lambda (k x)
	 (set! l (cons (cons (hashq-ref h k) x) l)))
       hh)
      (cons l c))))
		

(define (layout x)
  (let ((h   (make-hash-table))
	(n   0)
	(eq  '())
	(leq '()))

    (for-each
     (lambda (x)
       (hashq-set! h x n)
       (set! n (+ n 1)))
     (defining (get-vals x)))

    (let ((ar (make-array 0 n n))
	  (x0 (make-vector n 0))
          (av (make-vector n 0)))
              
      (hash-for-each
       (lambda (k n)
	 (for-each
	  (lambda (c)
	    (match c
	      ((((or '= '== '<= '>=)  x y) . strength)
	       (make-eq ar av h x y strength))
	      ((('<= x y) . strength)
	       (set! leq (cons (make-leq h x y) leq)))
	      ((('>= x y) . strength)
	       (set! leq (cons (make-leq h y x) leq)))
	      ((('== x y) . strength)
	       (set! eq (cons (make-leq h y x) eq)))))
	  (slot-ref k 'constraints)))
       h)
      
      (let* ((m1  (length eq))
	     (m2  (length leq))
             (A1  (make-array 0 m1 n))
             (c1  (make-vector m1 0))
             (A2  (make-array 0 m2 n))
             (c2  (make-vector m2 0))
             (v
              (begin
                (let lp ((i 0) (l eq))
                  (if (< i m1)
                      (begin
                        (match (car l)
                          ((xx . cc)
                           (vector-set! c1 i cc)
                           (for-each
                            (lambda (xx)
                              (match xx
                                ((j . v)
                                 (array-set! A1 v i j))))
                            xx)))
                        (lp (+ i 1) (cdr l)))))
		
                (let lp ((i 0) (l leq))
                  (if (< i m2)
                      (begin
                        (match (car l)
                          ((xx . cc)
                           (vector-set! c2 i cc)
                           (for-each
                            (lambda (xx)
                              (match xx
                                ((j . v)
                                 (array-set! A2 v i j))))
                            xx)))
                        (lp (+ i 1) (cdr l)))))

		(hash-for-each
		 (lambda (v n)
		   (vector-set! x0 n (slot-ref v 'x0)))
		 h)
		
                (*optimize* n m1 m2 x0 ar av A1 c1 A2 c2)))
             (vn (make-vector n)))

        (hash-for-each
	 (lambda (k n)
	   (vector-set! vn n k))
	 h)

	(let lp ((v v) (i 0))
          (if (< i n)
              (begin
                (slot-set! (vector-ref vn i) 'xr (car v))
                (lp (cdr v) (+ i 1)))))
          
        (values)))))

(define (size o w h)
  (let ((l (left o))
	(r (right o))
	(u (up o))
	(d (down o)))
    (link l 0)
    (link d 0)
    (link r w)
    (link u h)))

(define (circle r)
  (let ((o (quad r)))
    (slot-set! o 'draw *draw-ellipse*)))

(define (circle w h)
  (let ((o (rect w h)))
    (slot-set! o 'draw *draw-ellipse*)))

(define (left-part o)
  (let ((obj (item  (left o) (left o) (up o) (down o) (base o) (left o))))
    (slot-set! obj 'draw draw-nothing)
    obj))

(define (right-part o)
  (let ((obj (item  (right o) (right o) (up o) (down o) (base o) (right o))))
    (slot-set! obj 'draw draw-nothing)
    obj))

(define (up-part o)
  (let ((obj (item  (left o) (right o) (up o) (up o) (up o) (y-base o))))
    (slot-set! obj 'draw draw-nothing)
    obj))

(define (down-part o)
  (let ((obj (item  (left o) (right o) (down o) (down o) (down o) (y-base o))))
    (slot-set! obj 'draw draw-nothing)
    obj))

#;
(begin ;;; EXAMPLE
  (define r0 (rect 100 10))
  (define rn (hboxes 5 100 10 20))
  (define tn (vtags 5))
  (define zn (hstack= tn r0))
  (define xn (hstack rn (width 50) zn))

  (stronger (size xn 200 200))

  (layout xn)
  (draw xn)

  (let lp ((i 0))
    (if (< i 5)
	(let ((b (tag rn i))
	      (t (tag tn i)))
	  (draw-line (m-pos t) (left-m-pos b))
	  (lp (+ i 1))))))

  
