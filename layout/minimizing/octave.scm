(define-module (layout minimizing octave)
  #:use-module (layout minimizing layout)
  #:export ())

#|
A recent version of octave installed can be used to do the quadratic optmization needed for the layout. We start a demon that listen to a pipe and returns to a pipe

In order to activate this optimisation do

  (use-modules (layout minimizing octave))

|#

(define (frap x)
  (list->string
   (reverse
    (cddddr
     (reverse
      (string->list x))))))
      
(define path        (pk (frap (%search-load-path "layout/minimizing/octave.scm"))))
(define run.sh      (pk (string-append path (string-append "/run.sh " path))))
(define setup.sh    (string-append path (string-append "/setup.sh " path)))
(define to-pipe     (string-append path "/to-octave"))
(define from-pipe   (string-append path "/to-guile"))

(system (string-append "cd " path))
(system (string-append "bash " setup.sh))
(sleep 1)
(system (string-append (string-append "bash " run.sh) "" #;" @"))

(define (optimize n m1 m2 x0 H v A1 c1 A2 c2)
  (define fout (open-file to-pipe   "w"))
  (define fin  (open-file from-pipe "r"))
  (define (w x)
    (format fout "~a " x))

  (format #t "~%")
  (format #t "H:~%")
  (let lp ((i 0))
    (if (< i n)
        (begin
          (let lp ((j 0))
            (if (< j n)
                (begin
                  (format #t "~04f " (array-ref H i j))
                  (lp (+ j 1)))))
          (format #t "~%")
          (lp (+ i 1)))))

  (format #t "~%")
  (format #t "v:~%")
  (let lp ((j 0))
    (if (< j n)
        (begin
          (format #t "~f " (vector-ref v j))
          (lp (+ j 1)))))

  (format #t "~%")
  (format #t "A1:~%")
  (let lp ((i 0))
    (if (< i m1)
        (begin
          (let lp ((j 0))
            (if (< j n)
                (begin
                  (format #t "~04f " (array-ref A1 i j))
                  (lp (+ j 1)))))
          (format #t "~%")
          (lp (+ i 1)))))

  (format #t "~%")
  (format #t "A2:~%")
  (let lp ((i 0))
    (if (< i m2)
        (begin
          (let lp ((j 0))
            (if (< j n)
                (begin
                  (format #t "~04f " (array-ref A2 i j))
                  (lp (+ j 1)))))
          (format #t "~%")
          (lp (+ i 1)))))

  
  (pk x0)
  (w n)
  (w m1)
  (w m2)
  (let lp ((i 0))
    (if (< i n)
        (begin
          (w (vector-ref x0 i))
          (lp (+ i 1)))))

  (let lp ((i 0))
    (if (< i n)
        (let lp2 ((j 0))
          (if (< j n)
              (begin
                (w (array-ref H i j))
                (lp2 (+ j 1)))
              (lp (+ i 1))))))
  
  (let lp ((i 0))
    (if (< i n)
        (begin
          (w (vector-ref v i))
          (lp (+ i 1)))))

  (let lp ((i 0))
    (if (< i m1)
        (let lp2 ((j 0))
          (if (< j n)
              (begin
                (w (array-ref A1 i j))
                (lp2 (+ j 1)))
              (lp (+ i 1))))))
  
  (let lp ((i 0))
    (if (< i m1)
        (begin
          (w (vector-ref c1 i))
          (lp (+ i 1)))))

  (let lp ((i 0))
    (if (< i m2)
        (let lp2 ((j 0))
          (if (< j n)
              (begin
                (w (array-ref A2 i j))
                (lp2 (+ j 1)))
              (lp (+ i 1))))))
  
  (let lp ((i 0))
    (if (< i m2)
        (begin
          (w (vector-ref c2 i))
          (lp (+ i 1)))))

  (close fout)

  (let ((ret (make-vector n 0)))
    (let lp ((i 0))
      (if (< i n)
          (begin
            (vector-set! ret i (read fin))
            (lp (+ i 1)))
          (close fin)))
    (pk 'returns (vector->list ret))))

(set! *optimize* optimize)
