(define-module (layout minimizing sequence)
  #:use-module (oop goops)
  #:use-module (layout minimizing layout)
  #:use-module (layout minimizing pict)
  #:export (init items -> end))

(define *items*  (make-hash-table))
(define *litems* '())
(define *hitems* #f)
(define *vitems* #f)
(define *arrows* '())
(define (init)
  (set! *items*  (make-hash-table))
  (set! *litems* '())
  (set! *arrows* '())
  (set! *hitems* #f)
  (set! *vitems* (make-hash-table)))


(define (arrow text)
  (let* ((text (if (symbol? text) (symbol->string text) text))
         (w    (inexact->exact (floor (* 1.5 (string-length text) 12))))
         (h    20)
         (obj  (rect w h)))
    (slot-set! obj 'draw
               (lambda (o)
                 (let* ((x1.y1 (left-m-pos o))
                        (x2.y2 (right-m-pos o))
                        (x1    (car x1.y1))
                        (y1    (cdr x1.y1))
                        (x2    (car x2.y2))
                        (y2    (cdr x2.y2)))
                   (draw-arrow x1 y1 x2 y2 text))))
    (set! *arrows* (cons obj *arrows*))
    obj))

(define (d o) ((slot-ref o 'draw) o))
(set! *geos* (lambda () (map d *arrows*)))

(define (textbox text)
  (let* ((text (if (symbol? text) (symbol->string text) text))
         (w (inexact->exact (floor (* 1.3 (string-length text) 12))))
         (h 30)
         (obj (rect w h)))
    (slot-set! obj 'draw
               (lambda (o)
                 (draw-textbox o text)))
    obj))

    
  
(define (items . l)
  (let ((ll '())
        (f  #t))
    (for-each
     (lambda (x)
       (if (symbol? x) (set! x (symbol->string x)))
       (let ((o (textbox x)))
         (hash-set! *items* x o)
         (hashq-set! *vitems* o (list o)) 
         (set! ll (if f
                      (begin
                        (set! f #f)
                        (cons o ll))
                      (cons* o (weaker (width 50)) ll)))))
     l)
    (set! *litems* (reverse ll))
    (set! *hitems* (apply hstack *litems*))))

(define (lr l r)
  (let lp ((ll *litems*))
    (if (pair? ll)
        (cond
         ((eq? (car ll) l)
          #t)
         ((eq? (car ll) r)
          #f)
         (else
          (lp (cdr ll)))))))
          

(define *arrows* '())
(define (-> a b text)
  (if (symbol? a) (set! a (symbol->string a)))
  (if (symbol? b) (set! b (symbol->string b)))
  
  (let* ((aa (hash-ref *items* a))
         (bb (hash-ref *items* b))
         (ar (arrow text)))
    (if (lr aa bb)
        (begin
          (hashq-set! *vitems* aa
                      (cons (left-part ar) (hashq-ref *vitems* aa '())))
          (hashq-set! *vitems* bb
                      (cons (right-part ar) (hashq-ref *vitems* bb '()))))
        (begin
          (hashq-set! *vitems* aa
                      (cons (right-part ar) (hashq-ref *vitems* aa '())))
          (hashq-set! *vitems* bb
                      (cons (left-part ar) (hashq-ref *vitems* bb '())))))))

(define (end width height)
  (set! top 0)
  (set! bot height)
  (let lp ((l *litems*))
    (if (pair? l)
        (begin
          (hardlink (up (car l)) height)
          (lp (cdr l)))))

  (hash-for-each
   (lambda (k l)
     (greater (down (car l)) 0))
   *vitems*)
  
  (let ((ll (list *hitems*)))
    (hash-for-each
     (lambda (o l)
       (set! ll (cons (apply vstack (reverse l)) ll)))
     *vitems*)
    (let* ((o (apply combine ll)))
      (layout o)
      (draw   o))))
      
#;
(begin
  (init)
  (items "A" "B" "C")
  (-> "A" "C" "ac")
  (-> "C" "B" "cb")
  (-> "C" "A" "ca")
  (-> "B" "A" "ba")
  (end))
