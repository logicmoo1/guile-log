(define-module (ice-9 set complement)
  #:use-module (logic guile-log procedure-properties)
  #:use-module (oop goops)
  #:use-module (ice-9 match)
  #:use-module (ice-9 set set)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-9 gnu)
  #:export (make-complementable-set 
	    make-complementable-set-mac 
            advanced-set-printer
	    <set> cset? get-set-hash
	    make-set-

            ;;;Example complementable oredered set builed out of srfi-1 listsets
	    *set-equality-predicate*
	    with-lset-operators
	    with-tex-lset-operators
	    
	    *set-map-equality-predicate* 
	    with-lassoc-operators
 
	    set-as-complement	    
            complement-as-set as-finite-set finite-as-set))

#|
FUNCTIONAL COMPLEMENTABLE SETS AND ORDERED SETS AND SETMAPS

This is a higher order library that will based on a programmable
set datatype without complement construct a library for doing set operations 
by taking complement without a universe defined.

This library treat complements without defining a world. Mathematically that
leads to nondecidability issues, but if one construct all such sets from 
factual discrete elements no technical problems will appear. To actually get 
a set define a world as the underlying set lib and intersect it with the 
complemented set, that results in the underlying set. Bitsets has lognot and
in guile (gmp) manages bitset complements with lognot and the minus sign. One can define complement for bitsets as well with this library but that's not 
nessesary the gmp has all the magic for this. 

All nessessry set operators are defined with complement feature included
The working code is using the fact that every complemented set that is 
constructed from atoms and set operations can be defined as A ⊔ Bᶜ. 
This is not unique definition, to make it unique one could specify that 
B ∖ A = B that is ̧A⊆Bᶜ and this is also what we constrain the complements 
to be. FOr unordered sets one can assume that A=∅. But when the order is 
importane one need to be more carefule and a more comple solution results.
Ordering is defined through the ordering of operands in X ∪ Y and X ∩ Y, 
that is in a serialisation of X op Y, then elements in sets in X comes befor 
elements if sets in Y. Ordered sets has a well defined order and can can seen 
as an assoc list e.g. we can consider mapings from key elements to values in 
which case lookup of mappings is through the ordered list and hence defines a 
more generalised lookup. complements of an ordered map sets have no map defined
for them, any such maps need to be taken from the world at instantiation of 
a complemented set into that world, or the real sets it is intersecting with.

(make-complementable-set ∅ union intersection difference triple) 
   -> (values #:world Ω #:u uSS #:n nSS #:c cS #:+ ⊕SS #:- ∖SS)) 
Input
union          A,B   -> {x|x∈A or  x∈B}               (A∪B)    
intersection   A,B   -> {x|x∈A and x∈B}               (A∩B)
difference     A,B   -> {x|x∈A and x∉B}               (A∖B)
triple         A,B,C -> {x|x∈A and (x∈B or x∉C)}      (A∩(B∪Cᶜ))
∅              the empty set
Ω = ∅⊔∅ᶜ == (make-set ∅ ∅) defines the world.

Output:
#:u    : ∪  - union for complemented sets         0,1,... arguments
#:n    : ∩  - intersection for complemented sets  0,1,... arguments
#:c    : *ᶜ  - set complement                     1 argument
#:-    : ∖  - set difference =  A∩Bᶜ              2 arguments
#:o    : ⊕  - set adition    = (A∖B ∪ B∖A)        0,1,.... arguments

We also export a set struct according to
($ <set> set complement)

An example complemented set is implemented this is the API
*set-equality-predicate*      

default equal?, a syntax parameter of the equality
predicate, a change of this will expand the code
using that predicate

(with-lset-operators code ...)
Execute the code ... with the following bounded functions in code ...
(lambda* (#:u ∪ #:n ∩ #:c c #:+ ⊕ #: ∖) code ...)

*set-map-equality-predicate* 
default equal?, a syntax parameter of the equality
predicate, a change of this will expand the code
using that predicate

(with-lassoc-operators code ...)

|#

(define advanced-set-printer #t)

#;
(define-record-type <set>
  (make-set- map map-complement set set-complement? meta)
  cset?
  (map             set-map)
  (map-complement  set-map-complement)
  (set             set-set)
  (set-complement? set-complement?)
  (meta            set-meta))

(define-class <set> ()
  (map             #:getter        set-map 
                   #:init-keyword  #:map)
  (map-complement  #:getter        set-map-complement
                   #:init-keyword  #:map-complement)
  (set             #:getter        set-set
                   #:init-keyword  #:set)
  (set-complement? #:getter        set-complement?
                   #:init-keyword  #:complement?)
  (meta            #:getter        set-meta
                   #:init-keyword  #:meta))

(define (make-set- map map-complement set set-complement? meta)
  (make <set> #:map map #:map-complement map-complement #:set set
              #:complement? set-complement? #:meta meta))

(define (cset? x)
  (eq? (class-of x) <set>))

(define (get-set-hash x)
  (define set-hash (@@ (ice-9 set set) set-hash))
  
  (cond
   ((set?  x) (set-hash x))
   
   ((cset? x)
    (let ((a0 (set-hash (set-map x)))
          (a2 (set-hash (set-set x)))
          (a3 (set-complement? x)))
      (if (not a3)
          (logxor a0 a2)
          (cons* a2 a3))))
   
   (else
    (error "wrong type of argument in get-set-hash"))))


(define (set-printer vl port)
  (let ((c (set-set vl))
        (a (set-map vl))
        (b (set-map-complement vl))
        (e (set-meta vl)))
    (if e 
        (format port "Ω")
        (cond
	 ((and (= (set-n a) 0) (= (set-n b) 0))
	  (if (= (set-n c) 0)
	      (if (set-complement? vl)
		  (format port "Ω")
		  (format port "∅"))
	      (if (set-complement? vl)
		  (format port "~aᶜ"  c)
		  (format port "~a"   c))))

	 ((= (set-n b) 0)
	  (cond
	   ((= (set-n c) 0)
	    (if (set-complement? vl)
		(format port "~a ∪ Ω " a)
		(format port "~a" a)))
	   (else
	    (if (set-complement? vl)
		(format port "(~a ∪ ~aᶜ)" a c)
		(format port "(~a ∪ ~a)"  a c)))))

	 ((= (set-n c) 0)
	  (if (set-complement? vl)
	      (format port "(~a∖~a ∪ Ω)" a b)
	      (format port "(~a∖~a)"  a b)))

	 (else
	  (if (set-complement? vl)
	      (format port "(~a∖~a ∪ ~aᶜ)" a b c)
	      (format port "(~a∖~a ∪ ~a)"  a b c)))))))

(define (complement-as-set x empty)
  (values
   (set-complement? x)
   (if (set-complement? x)
       (make-set- (set-map-complement x) empty (set-set x) #f (set-meta x))
       (make-set- (set-map-complement x) empty empty       #f (set-meta x)))))
       

(define (set-as-complement x empty pred)
  (if pred
      (make-set- empty       (set-map x)
                 (set-set x) #t (set-meta x))
      (make-set- empty       (set-map x)
                 empty       #f (set-meta x))))

(define (as-finite-set x empty)
  (values
   (set-complement? x)
   (if (set-complement? x)
       (make-set- (set-map x)  empty
                  empty       #f (set-meta x))
       (make-set- (set-map x)             empty
                  (set-set x) #f (set-meta x)))))

(define (finite-as-set x empty pred)
  (if pred
      (make-set- (set-map x)  empty       empty       #t (set-meta x))
      (make-set- (set-map x)  empty       (set-set x) #f (set-meta x))))



(define *equal?* (make-fluid (lambda (x y) (eq? x y))))

(define-method (equal? (x <set>) (y <set>))
  ((fluid-ref *equal?*) x y))

(define-method (write (x <set>) port)
  (set-printer x port))


#;
(set-record-type-printer! <set> set-printer)


  
                          
(set-object-property! <set> 'prolog-printer
  (lambda (lp vl advanced?) 
    (if advanced-set-printer (set! advanced? #t))
    (let ((c (set-set vl))
          (a (set-map vl))
	  (b (set-map-complement vl))
	  (e (set-meta vl)))
      (if e 
	  (format #f "Ω")
          (if (and (= (set-n a) 0) (= (set-n b) 0))
              (if (set-complement? vl)
                  (format #f "~aᶜ"  (lp c))
                  (format #f "~a"   (lp c)))
              (if advanced?
                  (if (set-complement? vl)
                      (format #f "(~a∖~a ∪ ~aᶜ)"  (lp a) (lp b) (lp c))
                      (format #f "(~a∖~a ∪ ~a)"   (lp a) (lp b) (lp c)))
                  (if (set-complement? vl)
                      (if (= (set-n a) 0)
                          (format #f "~aᶜ" c)
                          (if (= (set-n c) 0)
                              (format #f "~a∪Ω"(lp a))
                              (format #f "~a∪~aᶜ"  (lp a) (lp c))))
                      (if (= (set-n a) 0)
                          (format #f "~a" c)
                          (if (= (set-n c) 0)
                              (format #f "~a" (lp a))
                              (format #f "~a∪~a" (lp a) (lp c)))))))))))

(define-syntax-rule (define-tool 
		      make-complementable-set 
		      make-complementable-set-mac
		      (args ...) code ...)
  (begin
    (define             (make-complementable-set     args ...) code ...)
    (define-syntax-rule (make-complementable-set-mac args ...) 
      (begin code ...))))

(define-tool make-complementable-set make-complementable-set-mac
  (smember ∅ union intersection difference tripple equiv? equiv4? 
             set-size set? make-one)
  
  (define Ω (make-set- ∅ ∅ ∅ #t #t))
  
  (define (addition x y) 
    (union
     (difference x y)
     (difference y x)))

  (define (make-set x y z c)
    (make-set- x y z c #f))

  (define (ifΩ x)
    (match x 
      (($ <set> a b c pred)
       (if (and (= (set-size a) 0) (= (set-size b) 0) (= (set-size c) 0))
	   (if pred
	       Ω
	       ∅)
           x))
      (_ x)))

  (define (u2 f ff)
    (lambda x
      (match x
        ((a b) (if (or (cset? a) (cset? b))
                   (f  a b)
                   (ff a b)))
        (x (apply f x)))))

  (define (u1 f ff)
    (lambda (a)
      (if (set? a)
          (f  a) 
          (ff  a))))

  (define (empty? x) (eq? x ∅))
  (define uSSo
    (u2
    (case-lambda
     (()  ∅)
     ((x) (if (cset? x) x (union x)))
     ((x y)      
      (match (list x y)
	;;; (A⊔Bᶜ)∪(C⊔Dᶜ) = (A∪C)⊔(B∩D)ᶜ = X⊔Yᶜ, Y∖X=Y.
        (((? empty?) _)
         (uSSo y))

        ((_ (? empty?))
         (uSSo x))

	((($ <set> a1 b1 c1 #t) ($ <set> a2 b2 c2 #t))
	 (ifΩ 
          (let ((a (union a1 a2)))
            (make-set 
             a
             (union (difference b1 a2) (difference b2 a1))
             (difference (intersection c1 c2) a)
             #t))))

	((($ <set> a1 b1 c1 #t) ($ <set> a2 b2 c2 #f))
	 (ifΩ 
          (let ((a (union a1 a2)))
            (make-set 
             a
             (union (difference b1 a2) (difference b2 a1))
             (difference c1 c2 a)
             #t))))

	((($ <set> a1 b1 c1 #f) ($ <set> a2 b2 c2 #t))
	 (ifΩ 
          (let ((a (union a1 a2)))
            (make-set 
             a
             (union (difference b1 a2) (difference b2 a1))
             (difference c2 c1 a)
             #t))))

	((($ <set> a1 b1 c1 #f) ($ <set> a2 b2 c2 #f))
	 (ifΩ
          (let ((a (union a1 a2)))
            (make-set 
             a
             (union (difference b1 a2) (difference b2 a1))
             (difference (union c1 c2) a)
             #f))))


	
        ;;; (A⊔Bᶜ)∪C = (A∪C)⊔(B∖C)ᶜ = X⊔Yᶜ, Y∖X=Y. 
	((($ <set>) c)
	 (uSSo x (make-set ∅ ∅ (union c) #f)))

	((c ($ <set>))
	 (uSSo (make-set ∅ ∅ (union c) #f) y))
	
	;;  A∪C
	((a c)
	 (union a c))))

     ((x y . l)
      (uSSo x (apply uSSo y l))))
    union))

  (define uSS
    (u2
    (case-lambda
     (()  ∅)
     ((x) (if (cset? x) x (union x)))
     ((x y)
      (match (list x y)
        (((? empty?) x)
         (uSSo x))
        ((x (? empty?))
         (uSSo x))
	((($ <set> a1 b1 c1 #t) ($ <set> a2 b2 c2 #t))
	 (ifΩ 
          (let ((a (union a1 a2)))
            (make-set
             a
             ∅
             (difference (intersection c1 c2) a)
             #t))))

	((($ <set> a1 b1 c1 #f) ($ <set> a2 b2 c2 #f))
         (ifΩ           
          (let ((a (union a1 a2)))
            (make-set
             a
             ∅
             (difference (union c1 c2) a)
             #f))))

	((($ <set> a1 b1 c1 #f) ($ <set> a2 b2 c2 #t))
         (ifΩ 
          (let ((a (union a1 a2)))
            (make-set
             a
             ∅
             (difference c2 c1 a)
             #t))))
	 

	((($ <set> a1 b1 c1 #t) ($ <set> a2 b2 c2 #f))
         (ifΩ 
          (let ((a (union a1 a2)))
            (make-set
             a
             ∅
             (difference c1 c2 a)
             #t))))

	((($ <set> _ _ _ _) c)
	 (uSS x (make-set ∅ ∅ (union c) #f)))
        
	((c ($ <set> _ _ _ _))
	 (uSS (make-set ∅ ∅ (union c) #f) y))
	
	((a c)
	 (union a c))))

    ((x y . l)
     (uSS x (apply uSS y l))))

    union))

  (define nSSo
    (u2
    (case-lambda
     (()  Ω)
     ((x) (if (cset? x) x (union x)))
     ((x y)
      (match (list x y)
        ((($ <set> a1 b1 c1 #f)  ($ <set> a2 b2 c2 #f))
	 (ifΩ 
          (let ((i1 (intersection a1 c2))
                (i2 (intersection a2 c1)))
            (let ((a (union (intersection a1 a2) i1 i2)))
              (make-set
               a
               (difference
                (difference (union b1 (addition a1 a2) b2) i1)
                i2)
               (difference (intersection c1 c2) a)
               #f)))))

        ((($ <set> a1 b1 c1 #f)  ($ <set> a2 b2 c2 #t))
	 (ifΩ 
          (let ((i1 (difference   a1 c2))
                (i2 (intersection a2 c1)))
            (let ((a (union (intersection a1 a2) i1 i2)))
              (make-set
               a
               (difference
                (difference (union b1 (addition a1 a2) b2) i1)
                i2)
               (difference c1 c2 a)
               #f)))))

        ((($ <set> a1 b1 c1 #t)  ($ <set> a2 b2 c2 #f))
	 (ifΩ 
          (let ((i1 (intersection a1 c2))
                (i2 (difference   a2 c1)))
            (let ((a (union (intersection a1 a2) i1 i2)))
              (make-set
               a
               (difference
                (difference (union b1 (addition a1 a2) b2) i1)
                i2)
               (difference c2 c1 a)
               #f)))))

        ((($ <set> a1 b1 c1 #t)  ($ <set> a2 b2 c2 #t))
	 (ifΩ 
          (let* ((i1 (difference a1 c2))
                 (i2 (difference a2 c1))
                 (a  (union (intersection a1 a2) i1 i2)))
            (make-set
             a
             (difference
              (difference (union b1 (addition a1 a2) b2) i1)
              i2)
             (difference (union c1 c2) a)
             #t))))

	((($ <set> _ _ _ _)  c)
	 (nSSo x (make-set ∅ ∅ (union c) #f)))

	((c ($ <set> _ _ _ _))
	 (nSSo (make-set ∅ ∅ (union c) #f) y))

	((a b)
	 (intersection a b))))

     ((x y . l)
      (nSS x (apply nSS y l))))
    
    intersection))

  (define nSS
    (u2
    (case-lambda
     (()  Ω)
     ((x) (if (cset? x) x (union x)))
     ((x y)
      (match (list x y)
        (((? empty?) x)
         ∅)
        ((x (? empty?))
         ∅)
        ((($ <set> a1 b1 c1 #f)  ($ <set> a2 b2 c2 #f))
	 (ifΩ 
          (let ((i1 (intersection a1 c2))
                (i2 (intersection a2 c1)))
            (let ((a (union (intersection a1 a2) i1 i2)))
              (make-set
               a
               ∅
               (difference (intersection c1 c2) a)
               #f)))))

        ((($ <set> a1 b1 c1 #f)  ($ <set> a2 b2 c2 #t))
	 (ifΩ 
          (let ((i1 (difference   a1 c2))
                (i2 (intersection a2 c1)))
            (let ((a (union (intersection a1 a2) i1 i2)))
              (make-set         
               a
               ∅
               (difference (difference c1 c2) a)
               #f)))))

        ((($ <set> a1 b1 c1 #t)  ($ <set> a2 b2 c2 #f))
	 (ifΩ 
          (let ((i1 (intersection a1 c2))
                (i2 (difference   a2 c1)))
            (let ((a (union (intersection a1 a2) i1 i2)))
              (make-set
               a
               ∅
               (difference (difference c2 c1) a)
               #f)))))

        ((($ <set> a1 b1 c1 #t)  ($ <set> a2 b2 c2 #t))
	 (ifΩ 
          (let* ((i1 (difference a1 c2))
                 (i2 (difference a2 c1))
                 (a  (union (intersection a1 a2) i1 i2)))
            (make-set
             a
             ∅
             (difference (union c1 c2) a)
             #t))))
        
	((($ <set> _ _ _ _)  c)
	 (nSS x (make-set ∅ ∅ (union c) #f)))

	((c ($ <set> _ _ _ _))
	 (nSS (make-set ∅ ∅ (union c) #f) y))
        
	((a b)
	 (intersection a b))))


     ((x y . l)
      (nSS x (apply nSS y l))))
    intersection))

  (define cS
    (lambda (x)
      (match x
        (($ <set> a b c #t)
         (ifΩ
          (let ((aa (intersection b c)))
            (make-set 
             aa
             (union a (difference b c))
             (difference c a aa)
             #f))))

        (($ <set> a b c #f)
         (ifΩ
          (let ((aa (difference b c)))
            (make-set 
             aa
             (union a (intersection b c))
             (difference (union a c) aa)
             #t))))

        (x
         (cond
          ((eq? x ∅)
           Ω)
          ((eq? x Ω)
           ∅)
          (else
           (ifΩ (make-set ∅ ∅ (union x) #t))))))))

   (define ∖SS 
     (u2
     (case-lambda
       ((a) (uSS a))
       ((a b)    
        (nSS a (cS b)))
       ((a b . l)    
        (apply ∖SS (∖SS a b) l)))
     difference))

  (define ∖SSo 
    (u2
    (case-lambda
      ((a) (uSS a))
      ((a b)
       (nSSo a (cS b)))
      ((a b  . l)
       (apply ∖SSo (∖SSo a b) l)))
    difference))

  (define ⊕SS
    (u2
    (case-lambda
     (()    ∅)
     ((a)   (uSS a))
     ;; A ⊕ B = A∖B ∪ B∖A
     ((a b)
      (let ((a (make-one a))
	    (b (make-one b)))
	(uSS (∖SS a b) (∖SS b a))))
     ((a b . l)
      (let ((a (make-one a)))
	(⊕SS a (apply ⊕SS b l)))))
    addition))

  (define ⊕SSo
    (u2
    (case-lambda
     (()    ∅)
     ((a)   (uSS a))
     ;; A ⊕ B = A∖B ∪ B∖A
     ((a b)
      (let ((a (make-one a))
	    (b (make-one b)))
	(uSSo (∖SSo a b) (∖SSo b a))))
     ((a b . l)
      (let ((a (make-one a)))
	(⊕SSo a (apply ⊕SSo b l)))))
    addition))

  (define (≡SS x y)
    (match (list x y)
      ((($ <set> a1 b1 c1 p1) ($ <set> a2 b2 c2 p2))
	(and (eq? p1 p2)
             (if p1
                 (equiv? c1 c2)
                 (equiv4? a1 c1 a2 c2))))
                     
       ((_ ($ <set>))
	(≡SS (make-set ∅ ∅ x #f) y))
       ((($ <set>) _)
	(≡SS x (make-set ∅ ∅ y #f)))
       ((x y) (equiv? x y))))

  (define (⊆SS x y)
    (≡SS (nSS x y) (uSS x)))

  (define (⊂SS x y)
    (and (not (≡SS x y)) (≡SS (nSS x y) (uSS x))))

  (define (->set x)
    (match (ifΩ x)
      (($ <set> a b c #f)
       (union a c))
       
      (($ <set> a b c #t)
       (if (eq? c ∅)
           a
           (error (format #f "~a is not a pure set contains complement" x))))
      (_ x)))

  (define (ssmember x s)
    (match (ifΩ s)
      (($ <set> a b c #f)
       (let ((r (smember x a)))
	 (if r
	     r
	     (smember x c))))

      (($ <set> a b c #t)
       (let ((r (smember x a)))
	 (if r
	     r
	     (if (smember x c)
		 #f
		 (cons x #f)))))

      (else
       (smember x s))))
       
  (values #:u  uSS  #:n  nSS  #:c  cS #:+  ⊕SS  #:-  ∖SS  ; unordered operations
	  #:ou uSSo #:on nSSo #:oc cS #:o+ ⊕SSo #:o- ∖SSo ; order     operations
	  #:world Ω #:= ≡SS #:< ⊂SS #:<= ⊆SS #:->set ->set #:memb ssmember))


(define (lunion e x y)
  (append x
	  (let lp ((l '()) (y y))
	    (if (pair? y)
		(let ((xx (car y)))
		  (if (member xx x e)
		      (lp l (cdr y))
		      (lp (cons xx l) (cdr y))))
		(reverse l)))))

(define (ltripple e x y z)
  (append x
	  (let lp ((l '()) (x x))
	    (if (pair? x)
		(let ((xx (car x)))
		  (if (or (member xx y e) (not (member xx z e)))
		      (lp (cons xx l) (cdr y))
		      (lp l (cdr y))))
		(reverse l)))))
;; example
(define-syntax-parameter *set-equality-predicate* (lambda (x) #'equal?))
(define-syntax-parameter *set-empty*              (lambda (x) #''()))
(define-syntax-rule (m f) (lambda x (apply f *set-equality-predicate* x)))
(define skunk (lambda x #f))
(define-syntax with-tex-lset-operators
  (lambda (x)
    (syntax-case x ()
      ((name #:eq eq code ...)
       #'(syntax-parameterize ((*set-equality-predicate*
				(lambda (x) #'eq)))
	   (with-lset-operators code ...)))
       
      ((name code ...)       
       (with-syntax ((∪ (datum->syntax #'name '∪))
		     (∩ (datum->syntax #'name '∩))
		     (c (datum->syntax #'name 'c))
		     (⊕ (datum->syntax #'name '⊕))
		     (∖ (datum->syntax #'name '∖))
		     (≡ (datum->syntax #'name '≡))
		     (⊂ (datum->syntax #'name '⊂))
		     (⊆ (datum->syntax #'name '⊆)))
	 #'(call-with-values (lambda ()
			       (make-complementable-set *set-empty* 
							(m lunion)
							(m lset-intersection) 
							(m lset-difference)
							(m ltripple)
							equal? length
							))
	     (lambda* (#:keys uo no co c +o -o = < <= )
		(define ∪ uo)
		(define ∩ no)
		(define c co)
		(define ⊕ +o)
		(define ∖ -o)
		(define ≡  =)
		(define ⊂  <)
		(define ⊆  <=)
		code ...)))))))

(define-syntax with-lset-operators
  (lambda (x)
    (syntax-case x ()
      ((name #:eq eq code ...)
       #'(syntax-parameterize ((*set-equality-predicate*
				(lambda (x) #'eq)))
	   (with-lset-operators code ...)))
       
      ((name code ...)       
       (with-syntax ((u  (datum->syntax #'name  'u))
		     (n  (datum->syntax #'name  'n))
		     (c  (datum->syntax #'name  'c))
		     (+  (datum->syntax #'name  '+))
		     (-  (datum->syntax #'name  '-))
		     (=  (datum->syntax #'name  '=))
		     (<  (datum->syntax #'name  '<))
		     (<= (datum->syntax #'name '<=)))
	 #'(call-with-values (lambda ()
			       (make-complementable-set *set-empty* 
							(m lunion)
							(m lset-intersection) 
							(m lset-difference)
							(m ltripple)
							equal? length
							(lambda (x)
							  (if (list? x) 
							      x
							      (list x)))))
							
	     (lambda* (#:key uo no co +o -o = < <= #:allow-other-keys)
		(define u uo)
		(define n no)
		(define c co)
		(define + +o)
		(define - -o)
		code ...)))))))

(define asseq (lambda (eq) (lambda (x y) (eq (car x) (car y)))))

(define-syntax-parameter *set-map-equality-predicate* 
  (lambda (x) #'equal?))

(define-syntax-rule (with-lassoc-operators code ...)
  (syntax-parameterize ((*set-equality-predicate*
			 (lambda (x) #'(asseq *set-map-equality-predicate* ))))
    (with-lset-operators code ...)))

;;Bitsets are already complementable.
