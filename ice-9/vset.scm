(define-module (ice-9 vset)
  #:use-module (ice-9 vlist)
  #:use-module (ice-9 set set)
  #:use-module (ice-9 set vhashx)
  #:use-module (ice-9 set weak-vhashx)
  #:use-module (ice-9 set complement)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-9 gnu)
  #:export 
  (mk-kv mk-kvx mk-kx empty world

   vset-equal?
   vset-union
   vset-intersection
   vset-difference
   vset-addition
   vset-complement
   vset-subset<
   vset-subset<=

   voset-equal?
   voset-union
   voset-intersection
   voset-difference
   voset-addition
   voset-complement
   voset-subset<
   voset-subset<=
   
   vset-fold
   vset-map
   vset-for-each
   vset-empty
   vset-world
   vset->list
   vset->assoc
   vset->kvlist

   vsetq-equal?
   vsetq-union
   vsetq-intersection
   vsetq-difference
   vsetq-addition
   vsetq-complement
   vsetq-subset<
   vsetq-subset<=

   vosetq-equal?
   vosetq-union
   vosetq-intersection
   vosetq-difference
   vosetq-addition
   vosetq-complement
   vosetq-subset<
   vosetq-subset<=
   
   vsetq-fold
   vsetq-map
   vsetq-for-each
   vsetq->list   
   vsetq->assoc
   vsetq->kvlist

   vsetv-equal?
   vsetv-union
   vsetv-intersection
   vsetv-difference
   vsetv-addition
   vsetv-complement
   vsetv-subset<
   vsetv-subset<=

   vosetv-equal?
   vosetv-union
   vosetv-intersection
   vosetv-difference
   vosetv-addition
   vosetv-complement
   vosetv-subset<
   vosetv-subset<=
   
   vsetv-fold
   vsetv-map
   vsetv-for-each
   vsetv->list
   vsetv->assoc
   vsetv->kvlist

   vsetx-equal?
   vsetx-union
   vsetx-intersection
   vsetx-difference
   vsetx-addition
   vsetx-complement
   vsetx-subset<
   vsetx-subset<=

   vosetx-equal?
   vosetx-union
   vosetx-intersection
   vosetx-difference
   vosetx-addition
   vosetx-complement
   vosetx-subset<
   vosetx-subset<=
   
   vsetx-fold
   vsetx-map
   vsetx-for-each
   vsetx-empty
   vsetx->list
   vsetx->assoc
   vsetx->kvlist

   wsetx-equal?
   wsetx-union
   wsetx-intersection
   wsetx-difference
   wsetx-addition
   wsetx-complement
   wsetx-subset<
   wsetx-subset<=

   wosetx-equal?
   wosetx-union
   wosetx-intersection
   wosetx-difference
   wosetx-addition
   wosetx-complement
   wosetx-subset<
   wosetx-subset<=
   
   wsetx-fold
   wsetx-map
   wsetx-for-each
   wsetx-empty
   wsetx->list
   wsetx->assoc
   wsetx->kvlist))


(define-inlinable (id x) x)
(define-record-type <kv>
  (mk-kv k v)
  kv?
  (k kv-k)
  (v kv-v))


(set-record-type-printer! <kv>
  (lambda (vl port) 
    (format port "#<~a -> ~a>" (kv-k vl) (kv-v vl))))


(define-syntax-rule (mk-set . l)
  (call-with-values (lambda () (make-set-from-assoc . l))
    (lambda* (#:key n-
	      =  u  n  -  +  <  <=
	      o= ou on o- o+ o< o<=
	      in fold map for-each empty set->list set->assoc set->kvlist
	      make-one)
       (call-with-values (lambda ()
			   (make-complementable-set 
			    empty u n - n- = set-size set? make-one))
	 (lambda* (#:key world u n c + - = < <= #:allow-other-keys ) 
	   (define c<  < )
	   (define c<= <=)
	   (define c=  = )

	    (call-with-values (lambda ()
				(make-complementable-set 
				 empty ou on o- n- o= set-size set? make-one))
	      (lambda* (#:key ou on oc o+ o- = < <= #:allow-other-keys )
		(define o<  < )
		(define o<= <=)
		(define o=  = )
		(values c= u  n  -  +  c  c< c<=
			o= ou on o- o+ oc o< o<=
			fold map for-each empty world
			set->list set->assoc set->kvlist))))))))

(define del (list 'deleted))
(define ele (list 'nonvalue))

(define-inlinable (mk-kv* x)
  (match x
    (($ <kv> k v)
     (cons k v))
    (_
     (cons x ele))))

(define-inlinable (v-acons  kv s) (vhash-cons (car kv) (cdr kv) s))
(define-inlinable (v-delete kv s) (vhash-cons (car kv) del      s))
(define-inlinable (v-assoc  kv s) 
  (let ((kv (vhash-assoc (car kv) s)))
    (if kv
	(if (eq? (cdr kv) del)
	    #f
	    kv)
	#f)))

(define-inlinable (v-hash   kv size) (hash   (car kv) size))
(define-inlinable (v-equal? x  y)    (equal? (car x) (car y)))
(define-inlinable (v-value? kv)      (not (eq? (cdr kv) ele)))
	    
(define-values
  (vset-equal?
   vset-union
   vset-intersection
   vset-difference
   vset-addition
   vset-complement
   vset-subset<
   vset-subset<=

   voset-equal?
   voset-union
   voset-intersection
   voset-difference
   voset-addition
   voset-complement
   voset-subset<
   voset-subset<=
   
   vset-fold
   vset-map
   vset-for-each
   vset-empty
   vset-world
   vset->list
   vset->assoc
   vset->kvlist)
  
  (mk-set vlist-null v-assoc v-acons 
	  v-delete v-hash mk-kv* kv? car cdr vlist-length 
	  v-value? (lambda (x) #t)
	  v-equal? id id))


(define empty vset-empty)
(define world (vset-complement empty))

(define-inlinable (vq-acons  kv s) (vhash-consq (car kv) (cdr kv) s))
(define-inlinable (vq-delete kv s) (vhash-consq (car kv) del      s))
(define-inlinable (vq-assoc  kv s) 
  (let ((kv (vhash-assq (car kv) s)))
    (if kv
	(if (eq? (cdr kv) del)
	    #f
	    kv)
	#f)))

(define-inlinable (vq-hash   kv size) (hashq   (car kv) size))
(define-inlinable (vq-equal? x  y)    (eq? (car x) (car y)))
	    
(define-values
  (vsetq-equal?
   vsetq-union
   vsetq-intersection
   vsetq-difference
   vsetq-addition
   vsetq-complement
   vsetq-subset<
   vsetq-subset<=

   vosetq-equal?
   vosetq-union
   vosetq-intersection
   vosetq-differenc
   vosetq-addition
   vosetq-complement
   vosetq-subset<
   vosetq-subset<=
   
   vsetq-fold
   vsetq-map
   vsetq-for-each
   vsetq-empty
   vsetq-world
   vsetq->list
   vsetq->assoc
   vsetq->kvlist)
  
  (mk-set vlist-null vq-assoc vq-acons 
	  vq-delete vq-hash mk-kv* kv? car cdr vlist-length 
	  v-value? (lambda (x) #t)
	  vq-equal? id id))


(define-inlinable (vv-acons  kv s) (vhash-consv (car kv) (cdr kv) s))
(define-inlinable (vv-delete kv s) (vhash-consv (car kv) del      s))
(define-inlinable (vv-assoc  kv s) 
  (let ((kv (vhash-assv (car kv) s)))
    (if kv
	(if (eq? (cdr kv) del)
	    #f
	    kv)
	#f)))

(define-inlinable (vv-hash   kv size) (hashv   (car kv) size))
(define-inlinable (vv-equal? x  y)    (eqv? (car x) (car y)))
	    
(define-values
  (vsetv-equal?
   vsetv-union
   vsetv-intersection
   vsetv-difference
   vsetv-addition
   vsetv-complement
   vsetv-subset<
   vsetv-subset<=

   vosetv-equal?
   vosetv-union
   vosetv-intersection
   vosetv-difference
   vosetv-addition
   vosetv-complement
   vosetv-subset<
   vosetv-subset<=
   
   vsetv-fold
   vsetv-map
   vsetv-for-each
   vsetv-empty
   vsetv-world
   vsetv->list
   vsetv->assoc
   vset->kvlist)
  
  (mk-set vlist-null vv-assoc vv-acons 
	  vv-delete vv-hash mk-kv* kv? car cdr vlist-length 
	  v-value? (lambda (x) #t)
	  vv-equal? id id))


(define-record-type <kvx->
  (mk-kvx- k v data)
  kvx-?
  (k    kvx--k)
  (v    kvx--v)
  (data kvx--data))

(define (mk-kvx k v . l)
  (mk-kvx- k v l))

(set-record-type-printer! <kvx->
  (lambda (vl port) 
    (format port "#<~a -> ~a>" (kvx--k vl) (kvx--v vl))))

(define-record-type <kvx>
  (mk-kvx+ k v e? h v? o?)
  kvx?
  (k    get-k)
  (v    get-v)
  (e?   get-e?)
  (h    get-h)
  (v?   get-v?)
  (o?   get-o?))


(set-record-type-printer! <kvx>
  (lambda (vl port) 
    (format port "#<~a -> ~a>" (get-k vl) (get-v vl))))

(define-record-type <kx>
  (mk-kx- k data)
  kx?
  (k    kx-k)
  (data kx-data))

(define (mk-kx k . l)
  (mk-kx- k l))

(set-record-type-printer! <kx>
  (lambda (vl port) 
    (format port "#<k:~a>" (kx-k vl))))

 
(define-inlinable (x-equal?  kx1 kx2) 
  (and ((get-e? kx1) (get-k kx1) (get-k kx2))
       (eq? (get-e? kx1) (get-e? kx2))
       (eq? (get-o? kx1) (get-o? kx2))))
  
(define-inlinable (x-hash kx s) ((get-h kx) (get-k kx) s))


;; A new thread should spur a new hastable here
(define *weak-references* (make-fluid (make-weak-key-hash-table)))

(define (k-w->val k val)
  (hash-set! (fluid-ref *weak-references*) k val))

(define-inlinable (mk-kvx* x)
  (match x
    (($ <kvx-> k v data)
     (call-with-values (lambda () (apply values data))
       (lambda* (#:key (equal? equal?) 
		       (hash   hash) 
		       (value? #t) 
		       (order? #t))		      
		(mk-kvx+ k v equal? hash value? order?))))
 	 
    (($ <kx> k data)
     (call-with-values (lambda () (apply values data))
       (lambda* (#:key (equal? equal?) 
		       (hash   hash) 
		       (value? #f) 
		       (order? #t))
		(mk-kvx+ k ele equal? hash value? order?))))

    (x
     (mk-kvx+ x ele equal? hash #f #t))))

(define (del-kv kv)
  (match kv
   (($ <kvx> k _ e h v? o?)
    (mk-kvx+ k ele e h v? o?))))

(define-inlinable (x-acons  kv s) (vhashx-cons kv          s x-hash))
(define-inlinable (x-delete kv s) (vhashx-cons (del-kv kv) s x-hash))
(define-inlinable (x-assoc  kv s) 
  (let ((kv (vhashx-assoc kv s x-equal? x-hash)))
    (if kv
	(if (eq? (get-v kv) del)
	    #f
	    kv)
	#f)))
	    
(define-values
  (vsetx-equal?
   vsetx-union
   vsetx-intersection
   vsetx-difference
   vsetx-addition
   vsetx-complement
   vsetx-subset<
   vsetx-subset<=

   vosetx-equal?
   vosetx-union
   vosetx-intersection
   vosetx-difference
   vosetx-addition
   vosetx-complement
   vosetx-subset<
   vosetx-subset<=
   
   vsetx-fold
   vsetx-map
   vsetx-for-each
   vsetx-empty
   vsetx-world
   vsetx->list
   vsetx->assoc
   vsetx->kvlist)
  
  (mk-set vhashx-null x-assoc x-acons 
	  x-delete x-hash mk-kvx* kv? get-k get-v vhashx-length 
	  get-v? get-o?
	  x-equal? id id))


(define-record-type <wkvx>
  (mk-wkvx++ e? h v? o?)
  kvx?
  (e?   get-we?-)
  (h    get-wh-)
  (v?   get-wv?-)
  (o?   get-wo?-))

(define (mk-wkvx+ k v e? h v? o?)
  (cons* k v (mk-wkvx++ e? h v? o?)))

(define-inlinable (get-wk  x) (car x))
(define-inlinable (get-wv  x) (cadr x))
(define-inlinable (get-we? x) (get-we?- (cddr x)))
(define-inlinable (get-wh  x) (get-wh-  (cddr x)))
(define-inlinable (get-wv? x) (get-wv?- (cddr x)))
(define-inlinable (get-wo? x) (get-wo?- (cddr x)))

(define-inlinable (wx-equal?  kx1 kx2) 
  (and ((get-we? kx1) (get-wk kx1) (get-wk kx2))
       (eq? (get-we? kx1) (get-we? kx2))
       (eq? (get-wo? kx1) (get-wo? kx2))))
  
(define-inlinable (wx-hash kx s) ((get-wh kx) (get-wk kx) s))


(define (del-wkv kv)
  (match kv
   ((k v . ($ <wkvx> e h v? o?))
    (mk-wkvx+ k ele e h v? o?))))

(define-inlinable (wx-acons  kv s) (whashx-cons kv           s x-hash))
(define-inlinable (wx-delete kv s) (whashx-cons (del-wkv kv) s x-hash))
(define-inlinable (wx-assoc  kv s) 
  (let ((kv (whashx-assoc kv s x-equal? x-hash)))
    (if kv
	(if (eq? (get-v kv) del)
	    #f
	    kv)
	#f)))

(define-inlinable (mk-wkvx* x)
  (match x
    (($ <kvx-> k v data)
     (call-with-values (lambda () (apply values data))
       (lambda* (#:key (equal? equal?) 
		       (hash   hash) 
		       (value? #t) 
		       (order? #t)
		       (weak   'k))
	 (let ((ret (mk-wkvx+ k v equal? hash value? order?))) 
	    (when weak
	       (case weak
                 ((k key)
		  (k-w->val k v)
		  (k-w->val k (cddr ret)))
                 ((v val)
		  (k-w->val v k)
		  (k-w->val v (cddr ret)))
		 ((kv keyval key-val)
		  (k-w->val k v)
		  (k-w->val v k)
		  (k-w->val k (cddr ret)))))
	    ret))))
	 
    (($ <kx> k data)
     (call-with-values (lambda () (apply values data))
       (lambda* (#:key (equal? equal?) 
		       (hash   hash) 
		       (value? #f) 
		       (order? #t)
		       (weak   #t))
		(let ((ret (mk-wkvx+ k ele equal? hash value? order?)))
		  (when weak
		    (k-w->val k (cddr ret)))
		  ret))))
    (_
     (let ((ret (mk-wkvx+ x ele equal? hash #f #t)))
       (k-w->val x (cddr ret))
       ret))))

(define-values
  (wsetx-equal?
   wsetx-union
   wsetx-intersection
   wsetx-difference
   wsetx-addition   
   wsetx-complement
   wsetx-subset<
   wsetx-subset<=

   wosetx-equal?
   wosetx-union
   wosetx-intersection
   wosetx-difference
   wosetx-addition
   wosetx-complement
   wosetx-subset<
   wosetx-subset<=
   
   wsetx-fold
   wsetx-map
   wsetx-for-each
   wsetx-empty
   wsetx-world
   wsetx->list
   wsetx->assoc
   wsetx->kvlist)
  
  (mk-set whashx-null wx-assoc wx-acons 
	  wx-delete wx-hash mk-wkvx* kv? get-wk get-wv whashx-length 
	  get-wv? get-wo?
	  wx-equal?
	  wlist->list list->wlist))

